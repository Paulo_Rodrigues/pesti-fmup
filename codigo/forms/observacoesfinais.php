<?php
echo "<div id='content28' style='display: inline;'>";
 	echo "<fieldset class='normal'>\n";
	echo "<legend>Comentários Finais/<i>Final Remarks</i></legend>\n";

	
			
		
	/*
	echo "<table id='box-table-a'>";
		echo "<tr><th colspan='2'>";
		echo "<p class='ppthelp'>Aqui é a sua oportunidade de resumir o resto do material constante no seu CV que acha que deveria:</p>";			

		echo "<p class='penhelp'><i>Here you have the chance to sum up all remaining material from your CV that should:</i></p>";
		echo "</th></tr>";	
	
	echo "<tr><td><label for='tempo' class='float'>i) ter sido incluído no Levantamento de forma mais detalhada	:<p><i>i) have been included in the Survey in more detail:</i></p></label></td><td><textarea name='incluidomaisdetalhe'>".$questionario->observacoesfinais->incluidomaisdetalhe."</textarea></td><tr>";
	echo "<tr><td><label for='tempo' class='float'>ii) ter sido incluído no Levantamento :<p><i>ii) have been included in the Survey:</i></p></label></td><td><textarea name='incluido' >".$questionario->observacoesfinais->incluido."</textarea></td></tr>";
	echo "</table>";
	
	*/
	
	
	echo "<table class='box-table-a'>";
	echo "<tr><th colspan='2'>";
	
	
	
	echo "<p class='ppthelp'>Classifique de 1 a 5 (1=mau; 2=medíocre; 3=suficiente; 4=bom; 5=muito bom) este Levantamento no que respeita a:";
	echo "<p class='penhelp'><i>Please classify from 1 to 5 (1=bad; 2=mediocre; 3=fair; 4=good; 5= very good) this Survey as regards:</i></p>";
	
	echo "</th></tr>";	

	echo "<tr><td><label for='tempo' class='float'>Tempo que demorou a preencher:<p><i>Time that it took to be filled:</i></p></label></td><td>";
	getRadio($questionario->observacoesfinais->tempo,"tempo","Tempo que demorou a preencher:");
	echo "</td></tr>";
	echo "<tr><td><label for='tempo' class='float'>Automatização do preenchimento:<p><i>Filling Automation:</i></p></label></td><td>";
	getRadio($questionario->observacoesfinais->automatizacao,"automatizacao","Automatização do preenchimento:");
	echo"</td></tr>";
	echo "<tr><td><label for='tempo' class='float'>Completividade:<p><i>Completeness:</i></p></label></td><td>";
	getRadio($questionario->observacoesfinais->completividade,"completividade","Completividade:");
	echo "</td></tr>";	
	echo "<tr><td><label for='tempo' class='float'>Organização:<p><i>Organization:</i></p></label></td><td>";
	getRadio($questionario->observacoesfinais->organizacao,"organizacao","Organização:");
	echo "</td></tr>";
	echo "<tr><td><label for='tempo' class='float'>Apresentação:<p><i>Presentation:</i></p></label></td><td>";
	getRadio($questionario->observacoesfinais->apresentacao,"apresentacao","Apresentação:");
	echo "</td></tr>";
	echo "</table>";
	echo "<table class='box-table-a'>";
		echo "<tr><th>Teça comentários gerais que ache relevantes para a melhoria do Levantamento em anos posteriores; ainda, inclua informação extra do seu CV que ache que deva constar aqui:<p><i>Please make general comments that you find useful to improve the Survey in coming years; please also include any extra information from your CV that you believe should appear here:</i></p></th></tr>";	
	
	echo "<tr><td><textarea style='width: 800px; height: 150px;' name='observacoes'>".$questionario->observacoesfinais->observacoes."</textarea></td></tr>";

	echo "</table>";
	
	echo "</fieldset>";
	echo "</div>";

function getRadio($valor,$desc,$texto){
	
	
	for($i=1;$i<6;$i++){
		if($valor==$i){
			echo "<input class='inp-textAuto' type='radio' name='".$desc."' checked value='".$i."'>".$i;			
		}else{
			echo "<input class='inp-textAuto' type='radio' name='".$desc."' value='".$i."'>".$i;
		}
	}
	
		
	
}

?>