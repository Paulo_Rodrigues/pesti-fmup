<?php

echo "<p><fieldset class='normal'>\n";
			echo "<legend>Colaboradores Atuais na FMUP/<i>Current FMUP Collaborators</i></legend>\n";

			echo "<p class='ppthelp'>Colaborador é todo/a aquele/a que faz investigação de forma ativa em colaboração consigo, e.g., tendo artigos científicos comuns em preparação. No que segue preencha o(s) nome(s) científico(s) do(s) seu(s) colaborador(es) em noutro Departamento/Serviço da FMUP exatamente no formato indicado, um por linha. Não utilize acentos ou cedilhas! Escolha um Serviço/Departamento da lista.</p>";			

echo "<p class='penhelp'><i>A collaborator is an individual that does active research in cooperation with you namely by having common scientific papers in preparation. In what follows please fill the scientific name(s) of your scientific collaborator(s) at other Departament/Serviço of FMUP exactly in the format shown, one per line. Please do not use any accent or other marks on the letters. Please also pick a Service/Department from the list that appears.</i></p>";
			
			
			    echo "
    <table id='colintfmup' class='box-table-b'>
  <tbody>
    <!-- Results table headers -->
    <tr>
    <th></th>
	  <th>Nome <p><i>Name</i></p></th>
      <th>Nome Científico<p><i>Scientific Name with Initials (e.g. Guimaraes, LP)</i></p></th>
      <th>Departmento<p><i>Department</i></p></th>
    </tr>";
			    
    foreach ($questionario->colaboradoresinternos as $i => $value){
    	
			
			echo "<tr>";
			echo "<td>";
			echo "<input type='image' src='../images/icon_delete.png' name='navOption' value='Apagar Colaborador' onclick='if(formSubmited==0){return false;};document.questionario.apagaRegColaboradorInterno.value=".$questionario->colaboradoresinternos[$i]->id.";document.questionario.operacao.value=53;' >";
			echo "</td>";
			echo "<td>";
			echo "<input class='inp-textAuto' type='text' name='cinome".$i."' value='".$questionario->colaboradoresinternos[$i]->nome."'>";
			echo "</td>";
			echo "<td>";
			echo "<input class='inp-textAuto' type='text' name='cinomecient".$i."' value='".$questionario->colaboradoresinternos[$i]->nomecient."'>";
    		echo "</td>";

			echo "<td>";
			getDepartamentos($i);
			echo "</td>";
			

	    	echo "</tr>";
	    	
	    	
    }
         echo "<tr><td><input input type='image' src='../images/icon_new.png' name='navOption' value='Novo Colaborador Interno' onclick='if(formSubmited==0){return false;};document.questionario.operacao.value=52;'></td></tr>";
	
    echo "</tbody></table>";
    echo "<br /><p>";

	echo "<input type='hidden' name='apagaRegColaboradorInterno' />";
	echo "</fieldset>";
	
	
	function getDepartamentos($i){
	
	$db = new Database();
	$lValues =$db->getLookupValues("lista_servicosfmup");
	
	echo "<SELECT name='cidepartamento".$i."'>\n";
	echo "<option></option>\n";
		
	while ($row = mysql_fetch_assoc($lValues)) {	
		echo "<option value='".$row["ID"]."'".checkDepartamento($row["ID"],$i).">".$row["DESCRICAO"]."</option>\n";
	}
	echo "</SELECT>\n";
	$db->disconnect();	
}	


function checkDepartamento($id,$i){
	global $questionario;
	if($questionario->colaboradoresinternos[$i]->departamento==$id)
		return " SELECTED";
	else 
		return "";
}

	?>