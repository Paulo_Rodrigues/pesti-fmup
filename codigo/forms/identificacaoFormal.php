<?php

echo "<div id='content13' style='display: inline;'>";


echo "<p><fieldset class='normal'>\n";
			echo "<legend>Identificação Científica/<i>Scientific Identification</i></legend>\n";

			echo "<p class='ppthelp'>No que segue preencha o(s) nome(s) científico(s) que utiliza exatamente no formato indicado, um por linha. Não utilize acentos ou cedilhas! Conforme o caso, quer o nome com iniciais quer o nome completo devem estar exatamente como constam nas publicações inseridas; caso contrário, estas não serão validadas!</p>";			

echo "<p class='penhelp'><i>In what follows please fill the scientific name(s) you use exactly in the format shown, one per line. Please do not use any accent or other marks on the letters.Depending on each case, both the name with initials and the full name must be exactly as they appear in the listed publications; otherwise, these will not be validated!</i></p>";
			
			
			    echo "
    <table id='identificacaoc' class='box-table-b'>
  <tbody>
    <!-- Results table headers -->
    <tr>
      <th></th>
      <th>Nome Científico com Iniciais<p><i>Scientific Name with Initials (e.g. Guimaraes, LP)</i></p></th>
      <th>Nome Científico Completo<p><i>Full Scientific Name (e.g. Guimaraes, Luis Pacos)</i></p></th>
      </tr>";
			    
    foreach ($questionario->identificacaoformal as $i => $value){
 			echo "<tr>";   	
    		echo "<td>";
			echo "<input type='image' src='../images/icon_delete.png' name='navOption' value='Apagar Nome' onclick='if(formSubmited==0){return false;};document.questionario.apagaRegNome.value=".$questionario->identificacaoformal[$i]->id.";document.questionario.operacao.value=15;' >";
			echo "</td>";
			

			echo "<td>";
			echo "<input class='inp-textAuto' type='text' name='nomecientini".$i."' value='".$questionario->identificacaoformal[$i]->nomecientini."'>";
    		echo "</td>";
    					
			echo "<td>";
			echo "<input class='inp-textAuto' type='text' name='nomecientcom".$i."' value='".$questionario->identificacaoformal[$i]->nomecientcom."'></p>\n";
			echo "</td>";
			
			

	    	echo "</tr>";
	    	
	    	
    }
    echo "<tr><td><input input type='image' src='../images/icon_new.png' name='navOption' value='Novo Nome' onclick='if(formSubmited==0){return false;};document.questionario.operacao.value=7;'></td></tr>";
	
    echo "</tbody></table>";
    echo "<br /><p>";

	echo "<input type='hidden' name='apagaRegNome'/>";
	echo "</fieldset>";
	
	echo "</div";
	
	?>