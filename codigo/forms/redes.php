<?php
echo "<div id='content23' style='display: inline;'>";
echo "<p><fieldset class='normal'>\n";
echo "<legend>Redes Científicas/<i>Research Networks</i></legend>\n";

echo "<p class='ppthelp'>Preencha a tabela seguinte caso tenha integrado rede(s) de investigação em ".$anoactual.". Devem estar protocoladas entre instituições, mas não têm obrigatoriamente de ser financiadas.</p>";			

echo "<p class='penhelp'><i>Please fill the following table in case you have been in (a) research network(s) in ".$anoactual.". These should be formalized in a written agreement between institutions, although not necessarily funded.</i></p>";


echo "<table id='redescient' class='box-table-b'>
    					<tr><th></th>
    					<th>Nome<p><i>Name</i></p></th>
      					<th>Tipo<p><i>Type</i></p></th>
      					<th>Data Início<p><i>Begin Date</i></p></th>
      					<th>Data Fim<p><i>End Date</i></p></th>
      					<th>Link<p><i>Link</i></p></th>
						</tr>";
			    
    foreach ($questionario->redes as $i => $value){
			echo "<tr>";
		   	echo "<td><input type='image' src='../images/icon_delete.png' name='navOption' value='Apagar Rede' onclick='if(formSubmited==0){return false;};document.questionario.apagaRegRede.value=".$i.";document.questionario.operacao.value=75;' ></td>";
			//echo "<td><input class='inp-textAuto' type='text' id='redenome_".$i."' name='redenome_".$i."' value='".$questionario->redes[$i]->nome."' onkeyup='mostraRede(this.value,".$i.");'><div class='divlookup' id='livesearchrede_".$i."'></div></td>";
		   	echo "<td><input class='inp-textAuto' type='text' id='redenome_".$i."' name='redenome_".$i."' value='".$questionario->redes[$i]->nome."'></td>";
			echo "<td>";
			getTipoRede($i);
			echo "</td>";
			echo "<td><input class='inp-textAuto' type='text' id='rededatainicio_".$i."' size='11' name='rededatainicio_".$i."' value='".$questionario->redes[$i]->datainicio."' onfocus='calendario(\"rededatainicio_".$i."\");' onkeypress='validateCal(event);'></td>";	
			echo "<td><input class='inp-textAuto' type='text' id='rededatafim_".$i."' size='11' name='rededatafim_".$i."' value='".$questionario->redes[$i]->datafim."' onfocus='calendario(\"rededatafim_".$i."\");' onkeypress='validateCal(event);'></td>";	
			echo "<td><textarea rows='2' cols='20' name='redelink_".$i."'>".$questionario->redes[$i]->link."</textarea></td>";
	    	echo "</tr>";	    	
    } 
    echo "<tr><td><input input type='image' src='../images/icon_new.png' name='navOption' value='Nova Sociedade' onclick='if(formSubmited==0){return false;};document.questionario.operacao.value=76;'></td></tr>";
    
    echo "</table>";
    echo "<br />";


	echo "<input type='hidden' name='apagaRegRede'/>";
	echo "</fieldset>";
	echo "</div>";
	function getTipoRede($i) {
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_tiporedes");
	
		echo "<SELECT id='redetipo_".$i."' name='redetipo_".$i."' >\n";
		echo "<option></option>\n";
		
		while ($row = mysql_fetch_assoc($lValues)) {	
			echo "<option value='".$row["ID"]."'".checkTipoRede($row["ID"],$i).">".$row["DESCRICAO"]."</option>\n";
		}
		echo "</SELECT><br />\n";
		$db->disconnect();
				
	}	
				
	function checkTipoRede($id,$i){
		global $questionario;
		if($questionario->redes[$i]->tipo==$id)
			return " SELECTED";
		else 
			return "";
	}
		
	?>