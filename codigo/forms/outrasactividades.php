<?php

echo "<div id='content25' style='display: inline;'>";
echo "<p><fieldset class='normal'>\n";
			echo "<legend>Outras Atividades/<i>Other Activities</i></legend>\n";

echo "<p class='ppthelp'>Aqui deve contabilizar-se todo o material escrito publicado/registado publicamente e ações orais ou técnicas destinadas a alunos, doentes, ao público em geral ou às escolas.</p>";			

echo "<p class='penhelp'><i>Here, the total number of all published/registred written material must be given, as well as of all oral or technical actions targeting students, patients, the public at large or schools.</i></p>";
			
			
			
echo " <table id='oact' class='box-table-a'>
  <tbody>
    <!-- Results table headers -->
    <tr>
      <th></th>
      <th>Atividades/<i>Activities</i></th>
    </tr>";
		
echo "<tr><td>Monografias de apoio às aulas ou a trabalho clínico: obras completas registadas em biblioteca ou e-platform pública<p><i>Published Lecture Notes or Clinical Reports</i></p></td><td><input class='inp-textAuto' type='text' name='texapoio' maxlength='3' size='3' onkeypress='validate(event)' value='".$questionario->outrasactividades->texapoio."'></td></tr>";
echo "<tr><td>Livros Didáticos<p><i>Textbooks</i></p></td><td><input class='inp-textAuto' type='text' name='livdidaticos' maxlength='3' size='3' onkeypress='validate(event)' value='".$questionario->outrasactividades->livdidaticos."'></td></tr>";
echo "<tr><td>Livros de Divulgação<p><i>Scientific books for the public</i></p></td><td><input class='inp-textAuto' type='text' name='livdivulgacao' maxlength='3' size='3' onkeypress='validate(event)' value='".$questionario->outrasactividades->livdivulgacao."'></td></tr>";
echo "<tr><td>Vídeos, jogos, aplicações<p><i>Videos, games, applications</i></p></td><td><input class='inp-textAuto' type='text' name='app' maxlength='3' size='3' onkeypress='validate(event)' value='".$questionario->outrasactividades->app."'></td></tr>";
echo "<tr><td>Páginas Internet<p><i>Webpages</i></p></td><td><input class='inp-textAuto' type='text' name='webp' maxlength='3' size='3' onkeypress='validate(event)' value='".$questionario->outrasactividades->webp."'></td></tr>";
echo "<tr><td>Aparições na Televisão, Rádio ou Imprensa<p><i>Tv, Radio or Press</i></p></td><td><input class='inp-textAuto' type='text' name='tvradiopress' maxlength='3' size='3' onkeypress='validate(event)' value='".$questionario->outrasactividades->tvradiopress."'></td></tr>";

//echo "<tr><td>Ações de Divulgação em Escolas<p><i>Activities in schools</i></p></td><td><input class='inp-textAuto' type='text' name='acoesdivulgacaoescolas' maxlength='3' size='3' onkeypress='validate(event)' value='".$questionario->outrasactividades->acoesdivulgacaoescolas."'></td></tr>";
//echo "<tr><td>Ações de Divulgação para público em geral<p><i>Activities for the general public</i></p></td><td><input class='inp-textAuto' type='text' name='acoesdivulgacaogeral' maxlength='3' size='3' onkeypress='validate(event)' value='".$questionario->outrasactividades->acoesdivulgacaogeral."'></td></tr>";
   
    echo "</tbody></table>";
    echo "<br /><p>";
     //echo "<input type='submit' name='navOption' value='Nova Entidade'></p>";
	
	//echo "<input type='hidden' name='apagaRegEntidade' value='".$i."'/>";
	echo "</fieldset>";
	
	
	echo "<fieldset class='normal'>\n";
	echo "<legend>Ações de Divulgacao/<i>Outreach activities</i></legend>\n";
	

	echo "<p class='ppthelp'>Aqui devem registar-se todas as iniciativas orais, práticas ou outras destinadas ao público ou às escolas. Mesmo sendo a mesma iniciativa, deve ser contada como diferente sempre que levada a cabo em dia/hora diferente.</p>";
	
	echo "<p class='penhelp'><i>Here, a detailed account  should be given of all oral, practical or other iniciatives targeting the public or schools. Even if the iniciative is the same, it must be added as different everytime the day/time is different.</i></p>";
		
	
	
	echo "
	<table id='oactdivukg' class='box-table-a'>
	<!-- Results table headers -->
	<tr>
	<th></th>
	<th>Título<p><i> Title</i></p></th>
	<th>Data<p><i>Date</i></p></th>
	<th>Local<p><i>Location</i></p></th>
			<th>Nº Participantes<p><i>Number of Participants</i></p></th>
	</tr>";
	
	foreach ($questionario->acoesdivulgacao as $i => $value){
			
			
		echo "<tr>";
			
		echo "<td>";
		echo "<input type='image' src='../images/icon_delete_s.png' name='navOption' value='Apagar Divulgacao' onclick='if(formSubmited==0){return false;};document.questionario.apagaRegDivulgacao.value=".$questionario->acoesdivulgacao[$i]->id.";document.questionario.operacao.value=71;' >";
		echo "</td>";
	
		echo "<td>";
		echo "<input class='inp-textAuto' type='text' name='divulgacao_titulo_".$i."'   value='".$questionario->acoesdivulgacao[$i]->titulo."'>";
		echo "</td>";
	
		echo "<td>";
		echo "<input class='inp-textAuto' type='text' maxlength='11' size='11' id='divulgacao_data_".$i."' name='divulgacao_data_".$i."' onfocus='calendario(\"divulgacao_data_".$i."\");' onkeypress='validateCal(event);' value='".$questionario->acoesdivulgacao[$i]->data."'>";
		echo "</td>";
	
		echo "<td>";
		echo "<input class='inp-textAuto' type='text'  id='divulgacao_local_".$i."' name='divulgacao_local_".$i."'  value='".$questionario->acoesdivulgacao[$i]->local."'>";
		echo "</td>";
		echo "<td>";
		echo "<input class='inp-textAuto' type='text'  id='npart_".$i."' name='npart_".$i."'  value='".$questionario->acoesdivulgacao[$i]->npart."'>";
		echo "</td>";
		echo "</tr>";
	
	}
	echo "<tr><td><input input type='image' src='../images/icon_new_s.png' name='navOption' value='Nova Divulgacao' onclick='if(formSubmited==0){return false;};document.questionario.operacao.value=72;'></td></tr>";
	
	echo "</table>";
	
	
	echo "<input type='hidden' name='apagaRegDivulgacao' />";
	echo "</fieldset>";
	
	
	echo "</div>";
	
	
	echo "</div>";
	
?>