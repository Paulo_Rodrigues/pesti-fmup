<?php

echo "<div id='content19' style='display: inline;'>";
echo "<p><fieldset class='normal'>\n";
echo "<legend>Editor/Árbitro <a href='../helpfiles/EdArb.JPG' style='color: #FFFF00' target='_blank'>Ajuda</a> / <i>Editor/Referee <a href='../helpfiles/EdArb.JPG' style='color: #FFFF00' target='_blank'>Help</a></i></legend>\n";
			
echo "<p class='ppthelp'>Comece por, em cada linha, escolher o tipo de serviço (Editor ou Árbitro). Depois, inicie a escrita do nome da revista e escolha-o da lista que surge automaticamente: o respetivo ISSN também aparece. Quando for o caso, indique um link para o “editorial board” da revista onde conste o seu nome.</p>";			

echo "<p class='penhelp'><i>Please start by picking the type of service (Editor or Referee), one per line. Then, please start writing the name of the journal and pick it from the list that automatically appears: its ISSN will also show up. When applicable provide the link to the editorial board of the journal where your name appears.</i></p>";

echo "  
<table id='arbrev' class='box-table-b'>        
    <thead> 
	    <tr>
	    				<th><u></u></th>
      					<th><u>Tipo<p><i>Type</i></p></u></th>
      					<th><u>ISSN<p><i></i></p></u></th>
      					<th><u>Revista<p><i>Journal</i></p></u></th>
						<th><u>Link<p><i>Link</i></p></u></th>
		</tr>
    </thead>       
<tbody>    
";
      					
      				//<th>Nº de Artigos<p><i>Articles Nr.</i></p></th>
			    
    foreach ($questionario->arbitragensRevistas as $i => $value){
    	
    	
			/*$lValues =$db->getPubNameByISSN($questionario->arbitragensRevistas[$i]->issn);
			
			if($row = mysql_fetch_assoc($lValues)){
					do {
					
			    		$nomerevista=$row['TITULO'];
			
					}while ($row = mysql_fetch_assoc($lValues));
			}else{
				
				$nomerevista="ISSN Não Registado";
				
			}*/
    	
    	
			echo "<tr>";
		   	echo "<td><input type='image' src='../images/icon_delete.png' name='navOption' value='Apagar Arbitagem' onclick='if(formSubmited==0){return false;};document.questionario.apagaArbitagensRevistas.value=".$questionario->arbitragensRevistas[$i]->id.";document.questionario.operacao.value=56;' ></td>";
			echo "<td>";
			getTipoArbitragem($i);
			echo "</td>";
			echo "<td><input class='inp-textAuto' type='text' size='11' id='issnAR".$i."' name='issnAR".$i."' value='".$questionario->arbitragensRevistas[$i]->issn."' onkeyup='mostraNomeRevistaArbitragens(this.value,".$i.")'><div class='divlookup' id='livesearchdar".$i."'></div></td>";
			echo "<td><input class='inp-textAuto' type='text'  size='100' id='tituloRevistaAR".$i."' name='tituloRevistaAR".$i."' value=\"".$questionario->arbitragensRevistas[$i]->tituloRevista."\" onkeyup='mostraNomeRevistaTituloArbitragens(this.value,".$i.")'><div class='divlookup' id='livesearchdarn".$i."'></div></td>";
			//echo "<td><input class='inp-textAuto' type='text' size='11' name='numeroAR".$i."' value='".$questionario->arbitragensRevistas[$i]->numero."'></td>";
			echo "<td><textarea rows='2' cols='20' name='linkRevistaAR".$i."'>".$questionario->arbitragensRevistas[$i]->link."</textarea></td>";
	    	echo "</tr>";	    	
    } 
    echo "</tbody><tfoot><tr><td><input type='image' src='../images/icon_new.png' name='navOption' value='Nova Arbitagem' onclick='if(formSubmited==0){return false;};document.questionario.operacao.value=55;'></td></tr>";
    
    echo "</table>";
    echo "<br />";


	echo "<input type='hidden' name='apagaArbitagensRevistas'/>";
	echo "</fieldset>";
	echo "</div>";
	
	function getTipoArbitragem($i){
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_tipoarbitragens");
	
		echo "<SELECT name='tipoAR".$i."' >\n";
		echo "<option></option>\n";
		
		while ($row = mysql_fetch_assoc($lValues)) {	
			echo "<option value='".$row["ID"]."'".checkTipoArbitragem($row["ID"],$i).">".$row["DESCRICAO"]."</option>\n";
		}
		echo "</SELECT><br />\n";
		$db->disconnect();
				
	}	
				
	function checkTipoArbitragem($id,$i){
		global $questionario;
		if($questionario->arbitragensRevistas[$i]->tipo==$id)
			return " SELECTED";
		else 
			return "";
	}
	
	

	
	?>