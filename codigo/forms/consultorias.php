<?php

//ALTERACAO
//Paulo Rodrigues (prodrigues@med.up.pt)
//
//Ordenação da tabela com o clique. Foi utilizado o plugin tablesorter de jQuery, 
//e criado um parser para fazer as ordenações das datas, dado que o texto dentro
//dos td's tem de ser filtrado para que a ordenação seja feita com sucesso.



echo "<div id='content5' style='display: inline;'>";
echo "<p><fieldset class='normal'>\n";

//Cabeçalho de informação da página. Foi criado a imagem de help.
echo "<legend>Consultorias <a href='../helpfiles/Consultorias.jpg' style='color: #FFFF00' target='_blank'>Ajuda</a> /<i>Consultancy <a href='../helpfiles/Consultorias.jpg' style='color: #FFFF00' target='_blank'>Help</a></i></legend>\n";

//Cabeçalho da tabela
echo "  
<table id='consultorias' class='box-table-b'>        
    <thead> 
        <tr>
            <th></th>
            <th><u>Instituição<p><i>Organization</i></p></u></th>
            <th><u>Orgão<p><i>Body</i></p></u></th>      
            <th><u>Data Início<p><i>Start Date</i></p></u></th>
            <th><u>Data Fim<p><i>End Date</i></p></u></th>
        </tr>
    </thead>       
	<tbody>    
";

//Preenchimento da tabela com a informação contida no objecto questionario 
//referente às consultorias.

foreach ($questionario->consultorias as $i => $value) {
    echo "<tr>";
    echo "<td><input type='image' src='../images/icon_delete.png' name='navOption' value='Apagar Consultoria' onclick='if(formSubmited==0){return false;};document.questionario.apagaRegConsultoria.value=" . $questionario->consultorias[$i]->id . ";document.questionario.operacao.value=77;' /></td>";

    echo "<td><input class='inp-textAuto' type='text' id='consinstituicao_" . $i . "' size='40' name='consinstituicao_" . $i . "' value='" . $questionario->consultorias[$i]->instituicao . "' onkeyup='mostraInstituicaoConsultoria(this.value," . $i . ");'><div class='divlookup' id='livesearchinstituicaoconsultoria" . $i . "' onmouseover='showDiv(this)' onmouseout='hideDiv(this)'/></div></td>";
    echo "<td><input class='inp-textAuto' type='text' id='consorgao_" . $i . "' size='40' name='consorgao_" . $i . "' value='" . $questionario->consultorias[$i]->orgao . "'><div class='divlookup' id='livesearchcargoup" . $i . "'/></div></td>";

    echo "<td><input class='inp-textAuto' type='text' id='consdatainicio_" . $i . "' name='consdatainicio_" . $i . "' size='11' value='" . $questionario->consultorias[$i]->datainicio . "' onfocus='calendario(\"consdatainicio_" . $i . "\");' onkeypress='validateCal(event);'/></td>";
    echo "<td><input class='inp-textAuto' type='text' id='consdatafim_" . $i . "' name='consdatafim_" . $i . "' size='11' value='" . $questionario->consultorias[$i]->datafim . "' onfocus='calendario(\"consdatafim_" . $i . "\");' onkeypress='validateCal(event);'/></td>";

    echo "</tr>";
}

echo "</tbody>";

//Botão de adicionar uma nova consultoria.
echo "<tfoot><tr><td><input input type='image' src='../images/icon_new.png' name='navOption' value='Nova Consultoria' onclick='if(formSubmited==0){return false;};document.questionario.operacao.value=78;'></td></tr></tfoot>";

echo "</table>";

//POST
echo "<input type='hidden' name='apagaRegConsultoria'/>";
echo "</fieldset>";
echo "</div>";
?>