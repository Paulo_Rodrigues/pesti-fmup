<?php


/**
* Autor: Paulo Rodrigues (prodrigues@med.up.pt/paulomiguelarodrigues@gmail.com)
* Data: 14/04/2014
* Alterações: Adicionada a capacidade de poder editar e remover registos. Alterada a parte de front-end, usando um plugin de jQuery (Accordion). 
*/

require_once '../../classlib/QuestionarioDepartamento.class.inc';
require_once '../../classlib/Database.class.inc';

if(isset($_SESSION['departamento']))
	$dadosDep=new QuestionarioDepartamento($_SESSION['departamento']);
else
	header("Location: ../../index.php?erro=404");

	echo "<link rel=\"stylesheet\" href=\"../../js/jQuery/themes/base/jquery.ui.all.css\">
	<script>";
	echo "$(function () {   
			$( \"#accordion\" ).accordion({collapsible: true});
		});";
	echo "</script>";

	echo "<style>
			body { font-size: 70%; }
			label, input { display:block; }
			input.text { margin-bottom:12px; width:95%; padding: .4em; }
			fieldset#edit { padding:0; border:0; margin-top:25px; }
			h1 { font-size: 1.2em; margin: .6em 0; }
			div#users-contain { width: 350px; margin: 20px 0; }
			div#users-contain table { margin: 1em 0; border-collapse: collapse; width: 100%; }
			div#users-contain table td, div#users-contain table th { border: 1px solid #eee; padding: .6em 10px; text-align: left; }
			.ui-dialog .ui-state-error { padding: .3em; }
			.validateTips { border: 1px solid transparent; padding: 0.3em; }
			thead 
			{ 
				font-size: 95%;
			}
			tbody 
			{ 
				font-size: 85%;
			}
		</style>";

	echo "<div id='content3' style='display: inline;'>";
	echo "<fieldset class='normal'>\n";
	
	echo "<legend>Dados de Submissão</legend>";
	echo "<p>Nº de investigadores registados: ".sizeof($dadosDep->investigadores) . "</p>";

	echo "</fieldset></div>";

	include("camposEdicao.php");

	echo "<div id=\"accordion\" style='margin-left: 180px;  margin-right: 370px;'>"; 	
		
		include('../../reports/diretordep/investigadoresDEP.php');	

		include('../../reports/diretordep/habilitacoes.php');	

		include('../../reports/diretordep/agregacoes.php');	
		
		include('../../reports/diretordep/experienciaProfissional.php');	
		
		include('../../reports/diretordep/direcoesCurso.php');	
		
		include('../../reports/diretordep/colaboradores.php');	
			
		include('../../reports/diretordep/unidadesInvestigacao.php');	
			
		include('../../reports/diretordep/publicacoesISI.php');	
			
		include('../../reports/diretordep/publicacoesPubmed.php');
			
		include('../../reports/diretordep/publicacoesManualPatentes.php');	

		include('../../reports/diretordep/publicacoesManualInternacional.php');
			
		include('../../reports/diretordep/publicacoesManualNacional.php');
		
		include('../../reports/diretordep/projectos.php');
		
		include('../../reports/diretordep/conferencias.php');	
		
		include('../../reports/diretordep/apresentacoes.php');	
		
		include('../../reports/diretordep/arbitragensRevistas.php');	
		
		include('../../reports/diretordep/premios.php');	
		
		include('../../reports/diretordep/sc.php');	
			
		include('../../reports/diretordep/redes.php');
		
		include('../../reports/diretordep/missoes.php');	
		
		include('../../reports/diretordep/orientacoes.php');

		include('../../reports/diretordep/juris.php');
		
		include('../../reports/diretordep/outrasactividades.php');
		
		include('../../reports/diretordep/acoesdivulgacao.php');	
		
		include('../../reports/diretordep/produtos.php');

	echo "</div>";

	

?>