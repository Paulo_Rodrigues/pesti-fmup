<?php

echo "<div id='content1' style='display: inline;'>";
echo "<fieldset class='normal'>\n";
echo "<legend>Identificação Civil/<i>Civil Identification</i></legend>\n";

echo "<table id='investigadores' class='box-table-a'>";

echo "<tr><td><label for='nome' class='float'>Nome Completo:<p><i>Full Name:</i></p></label></td>\n";
echo "<td><input class='inp-textAuto' type='text' name='nome' size='50' value='" . $questionario->investigador->nome . "'></td></tr>\n";

echo "<tr><td><label for='nummec' class='float'>Número Mecanográfico <a href='../helpfiles/Mec2.jpg' target='_blank'>Ajuda</a>:<p><i>Mecanographical Number <a href='../helpfiles/Mec2.jpg' target='_blank'>Help</a>:</i></p></label></td>\n";
echo "<td><input class='inp-textAuto' type='text' name='nummec' size='50' value='" . $questionario->investigador->nummec . "' onkeypress='validate(event)'></td></tr>\n";

echo "<tr><td><label for='numidentificacao' class='float'>Nº de B. Identidade / Passaporte :<p><i>ID / Passaport Number :</i></p></label></td>\n";
echo "<td><input class='inp-textAuto' type='text' name='numidentificacao' size='50' value='" . $questionario->investigador->numidentificacao . "' onkeypress='validate(event)'></td></tr>\n";

getSexo();
getPaisesNacionalidade();
getPaisesNaturalidade();
getConcelhos();
getFreguesias();
getServicos();

echo "<tr><td><label for='datanasc' class='float'>Data de Nascimento:<p><i>Date of Birth:</i></p></label></td>\n";
echo "<td><input class='inp-textAuto' type='text' id='dn' name='datanasc' size='11' value='" . $questionario->investigador->datanasc . "' onfocus='calendario(\"dn\");' onkeypress='validateCal(event)'></td></tr>";

echo "<tr><td><label for='morada' class='float'>Morada da residência (incluindo código postal):<p><i>Full home address, including postcode:</i></p></label></td>\n";
echo "<td><textarea textarea rows='3' cols='50' class='inp-textAuto' name='morada'>" . $questionario->investigador->morada . "</textarea></td></tr>\n";

echo "<tr><td><label for='email' class='float'>Email Institucional:<p><i>Institutional Email:</i></p></label></td>\n";
echo "<td><input class='inp-textAuto' type='text' name='email' size='50' value='" . $questionario->investigador->email . "' onblur='validateMail();'></td></tr>\n";

echo "<tr><td><label for='email_alternativo' class='float'>Email Alternativo:<p><i>Other Email:</i></p></label></td>\n";
echo "<td><input class='inp-textAuto' type='text' name='email_alternativo' size='50' value='" . $questionario->investigador->email_alternativo . "' onblur='validateMailAlt();'></td></tr>\n";

echo "<tr><td><label for='telefone' class='float'>Telefone:<p><i>Phone:</i></p></label></td>\n";
echo "<td><input class='inp-textAuto' type='text' name='telefone' size='20' maxlength='20' value='" . $questionario->investigador->telefone . "' onkeypress='validatephone(event)' > (ex:+351221234567)</td></tr>\n";

echo "<tr><td><label for='telefone' class='float'>Telemóvel:<p><i>Mobile:</i></p></label></td>\n";
echo "<td><input class='inp-textAuto' type='text' name='telemovel' size='20' maxlength='20' value='" . $questionario->investigador->telemovel . "' onkeypress='validatephone(event)' > (ex:+351912345678)</td></tr>\n";

echo "<tr><td><label for='telefone' class='float'>Extensão Interna:<p><i>Extension Number:</i></p></label></td>\n";
echo "<td><input class='inp-textAuto' type='text' name='extensao' size='6' onkeypress='validate(event)' value='" . $questionario->investigador->extensao . "'></td></tr>\n";

echo "</table>";
echo "</fieldset>";
echo "</div>";

function getSexo() {

    echo "<tr><td><label for='sexo' class='float'>Género:<p><i>Gender:</i></p></label></td>\n";
    echo "<td><SELECT id='sexo' name='sexo'>\n";
    echo "<option value='-1'></option>\n";
    echo "<option value='0'" . checkSexo(0) . ">Feminino</option>\n";
    echo "<option value='1'" . checkSexo(1) . ">Masculino</option>\n";
    echo "</SELECT></td></tr>\n";
}

function getPaisesNaturalidade() {
    $db = new Database();
    $lValues = $db->getLookupValues("lista_paises");
    echo "<tr><td><label for='paisnat' class='float'>País de Naturalidade:<p><i>Country of Birth:</i></p></label></td>\n";
    echo "<td><SELECT id='paisnat' name='paisnat' onchange='changeCF()'>\n";
    echo "<option></option>\n";
    while ($row = mysql_fetch_assoc($lValues)) {
        echo "<option value='" . $row["ID"] . "'" . checkPaisNaturalidade($row["ID"]) . ">" . $row["DESCRICAO"] . "</option>\n";
    }
    echo "</SELECT></td></tr>\n";
    $db->disconnect();
}

function getPaisesNacionalidade() {
    $db = new Database();
    $lValues = $db->getLookupValues("lista_paises");
    echo "<tr><td><label for='paisnac' class='float'>País de Nacionalidade:<p><i>Citizenship:</i></p></label></td>\n";
    echo "<td><SELECT id='paisnac' name='paisnac' onchange='changeCF()'>\n";
    echo "<option></option>\n";
    while ($row = mysql_fetch_assoc($lValues)) {
        echo "<option value='" . $row["ID"] . "'" . checkPaisNacionalidade($row["ID"]) . ">" . $row["DESCRICAO"] . "</option>\n";
    }
    echo "</SELECT></td></tr>\n";
    $db->disconnect();
}

function getConcelhos() {
    $db = new Database();
    $lValues = $db->getLookupValues("lista_concelhos");
    echo "<tr><td><label for='concelhonat' class='float'>Concelho de Naturalidade:</label></td>\n";
    echo "<td><SELECT id='concelhonat' name='concelhonat' >\n";
    echo "<option></option>\n";
    while ($row = mysql_fetch_assoc($lValues)) {
        echo "<option value='" . $row["ID"] . "'" . checkConcelho($row["ID"]) . ">" . $row["DESCRICAO"] . "</option>\n";
    }
    echo "</SELECT></td></tr>\n";
    $db->disconnect();
}

function getFreguesias() {

    $db = new Database();
    $lValues = $db->getLookupValues("lista_freguesias");

    echo "<tr><td><label for='freguesianat' class='float'>Freguesia de Naturalidade:</label></td>\n";
    echo "<td><SELECT id='freguesiasnat' name='freguesianat'>\n";
    echo "<option></option>\n";

    while ($row = mysql_fetch_assoc($lValues)) {
        echo "<option value='" . $row["ID"] . "'" . checkFreguesia($row["ID"]) . ">" . $row["DESCRICAO"] . "</option>\n";
    }
    echo "</SELECT></td></tr>\n";
    $db->disconnect();
}

function getServicos() {

    $db = new Database();
    $lValues = $db->getLookupValues("lista_servicosfmup");

    echo "<tr><td><label for='servico' class='float'>Serviço/Departamento:<p><i>Service/Department:</i></p></label></td>\n";
    echo "<td><SELECT name='servico'>\n";
    echo "<option></option>\n";

    while ($row = mysql_fetch_assoc($lValues)) {
        echo "<option value='" . $row["ID"] . "'" . checkServico($row["ID"]) . ">" . $row["DESCRICAO"] . "</option>\n";
    }
    echo "</SELECT></td></tr>\n";
    $db->disconnect();
}

function checkServico($id) {
    global $questionario;
    if ($questionario->investigador->servico == $id)
        return " SELECTED";
    else
        return "";
}

function checkSexo($id) {
    global $questionario;
    if ($questionario->investigador->sexo == $id)
        return " SELECTED";
    else
        return "";
}

function checkFreguesia($id) {
    global $questionario;
    if ($questionario->investigador->freguesianat == $id)
        return " SELECTED";
    else
        return "";
}

function checkConcelho($id) {
    global $questionario;
    if ($questionario->investigador->concelhonat == $id)
        return " SELECTED";
    else
        return "";
}

function checkPaisNaturalidade($id) {
    global $questionario;
    if ($questionario->investigador->paisnat == $id)
        return " SELECTED";
    else
        return "";
}

function checkPaisNacionalidade($id) {
    global $questionario;
    if ($questionario->investigador->paisnac == $id)
        return " SELECTED";
    else
        return "";
}

?>