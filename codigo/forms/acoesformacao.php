<?php

echo "<div id='content27' style='display: inline;'>";
echo "<fieldset class='normal'>\n";
			echo "<legend>Ações de Formação dadas <a href='../helpfiles/Acoes.JPG' style='color: #FFFF00' target='_blank'>Ajuda</a> /<i>Training conducted <a href='../helpfiles/Acoes.JPG' style='color: #FFFF00' target='_blank'>Help</a></i></legend>\n";


echo "<p class='ppthelp'>Para cada ação de formação conduzida indicar o título, o nº total de horas, o custo por formando, o tipo de formandos e o seu número total. Ainda, o período em que decorreu, o local e o seu objetivo.</p>";			

echo "<p class='penhelp'><i>For each training session conducted please give the title, the total number of hours, the cost per trainee, their type and their total number. Finally, also indicate the dates, the place and its objective..</i></p>";
	

			
			
echo "
    <table id='accform' class='box-table-b'>
    <!-- Results table headers -->
    <tr>
      <th></th>
	  <th>Tipo<p><i>Type</i></p></th>
      <th>Título<p><i>Title</i></p></th>
      <th>Nº Horas<p><i>Nr. Hours</i></p></th>
      <th>Preço por Formando(euros)<p><i>Cost by Trainee(euros)</i></p></th>
      <th>Tipo de Formandos<p><i>Type of Trainees</i></p></th>
      <th>Nº de Formandos<p><i>Number of Trainees</i></p></th>
      <th>Data Início<p><i>Begin Date</i></p></th>
      <th>Data Fim<p><i>End Date</i></p></th>
       <th>Cidade, País<p><i>City, Country</i></p></th>
       <th>Objetivo<p><i>Objective</i></p></th>
    </tr>";
			    
    foreach ($questionario->acoesformacao as $i => $value){
    	
			
			echo "<tr>";
			
		   	echo "<td>";
			echo "<input type='image' src='../images/icon_delete_s.png' name='navOption' value='Apagar Unidade' onclick='if(formSubmited==0){return false;};document.questionario.apagaRegAcaoFormacao.value=".$questionario->acoesformacao[$i]->id.";document.questionario.operacao.value=48;' >";
			echo "</td>";

			echo "<td>";
			//echo "<input class='inp-textAuto' type='text' name='_formandos_".$i."' value='".$questionario->acoesformacao[$i]->formandos."'>".getTipoFormando($i);
			getTipoAcao($i);
			echo "</td>";
			
			echo "<td>";
			echo "<input class='inp-textAuto' type='text' name='titulo_".$i."' value='".$questionario->acoesformacao[$i]->titulo."'>";			
			echo "</td>";
    					
			echo "<td>";
			echo "<input class='inp-textAuto' type='text' name='horas_".$i."' maxlength='4' size='4' onkeypress='validate(event)' value='".$questionario->acoesformacao[$i]->horas."'>";			
			echo "</td>";

			echo "<td>";
			echo "<input class='inp-textAuto' type='text' name='preco_".$i."'  size='8' onkeypress='validate(event)' value='".$questionario->acoesformacao[$i]->preco."'>";			
			echo "</td>";

			echo "<td>";
			//echo "<input class='inp-textAuto' type='text' name='_formandos_".$i."' value='".$questionario->acoesformacao[$i]->formandos."'>".getTipoFormando($i);	
			getTipoFormando($i);				
			echo "</td>";
	    	
			echo "<td>";
			echo "<input class='inp-textAuto' type='text' name='nformandos_".$i."'  size='8' onkeypress='validate(event)' value='".$questionario->acoesformacao[$i]->nformandos."'>";
			echo "</td>";

			
			echo "<td>";
			echo "<input class='inp-textAuto' type='text' id='diaf_".$i."' maxlength='11' size='11' name='afdataini_".$i."' onfocus='calendario(\"diaf_".$i."\");' onkeypress='validateCal(event);' value='".$questionario->acoesformacao[$i]->dataini."'>";			
			echo "</td>";			

			echo "<td>";
			echo "<input class='inp-textAuto' type='text' id='dfaf_".$i."' maxlength='11' size='11' name='afdatafim_".$i."' onfocus='calendario(\"dfaf_".$i."\");' onkeypress='validateCal(event);' value='".$questionario->acoesformacao[$i]->datafim."'>";			
			echo "</td>";	
			
			echo "<td>";
			echo "<input class='inp-textAuto' type='text' name='local_".$i."' value='".$questionario->acoesformacao[$i]->local."'>";			
			echo "</td>";

			echo "<td>";
			echo "<input class='inp-textAuto' type='text' name='objectivo_".$i."' value='".$questionario->acoesformacao[$i]->objectivo."'>";			
			echo "</td>";
			
			echo "</tr>";

    }
   	echo "<tr><td><input input type='image' src='../images/icon_new_s.png' name='navOption' value='Nova Acão' onclick='if(formSubmited==0){return false;};document.questionario.operacao.value=49;'></td></tr>";
	
    echo "</table>";

     
	echo "<input type='hidden' name='apagaRegAcaoFormacao' />";
	echo "</fieldset>";
	
	echo "<fieldset class='normal'>\n";
	echo "<legend>Acreditação como Formador /<i>Accreditation</i></legend>\n";
	
	/*
	 echo "<p class='ppthelp'>Indique o(s) tipo(s) de Unidade de Investigação a que pertence, o(s) seu(s) nome(s) da lista (quando é “Outra” tem de preencher à mão) e a percentagem de tempo a ela(s) oficialmente adstrito. Se foi o Responsável pela Unidade, clicar na caixa respetiva. Finalmente, confirme a Avaliação 2007 da sua Unidade, não fazendo nada na última caixa. Caso contrário, escreva aí a avaliação e algum comentário.</p>";
	
	echo "<p class='penhelp'><i>Indicate the type(s) of Research Unit(s) to which you belong, pick the name(s) from the list (when “Outra” the name must be filled by hand), and fill the percentage of time officially applicable (to each one). If you were the Unit’s Head please click in the corresponding box. Finally, please confirm the 2007 Evaluation by doing nothing on the last box. Otherwise, please write there the correct Evaluation and a commentary.</i></p>";
	*/
		
	echo "
	<table id='acred' class='box-table-b'>
	<!-- Results table headers -->
	<tr>
		<th></th>
		<th>Intituicao<p><i>Institution</i></p></th>
		<th>Título da Acreditação<p><i>Accreditation Title</i></p></th>
		<th>Data<p><i>Date</i></p></th>
	</tr>";
	 
	foreach ($questionario->acreditacoes as $i => $value){
		 
			
		echo "<tr>";
			
		echo "<td>";
		echo "<input type='image' src='../images/icon_delete_s.png' name='navOption' value='Apagar Acreditacao' onclick='if(formSubmited==0){return false;};document.questionario.apagaRegAcreditacao.value=".$questionario->acreditacoes[$i]->id.";document.questionario.operacao.value=69;' >";
		echo "</td>";
	
		echo "<td>";
		echo "<input class='inp-textAuto' type='text' name='acreditacao_instituicao_".$i."'  size='50' value='".$questionario->acreditacoes[$i]->instituicao."'>";
		echo "</td>";
	
		echo "<td>";
		echo "<input class='inp-textAuto' type='text' name='acreditacao_tipo_".$i."'  size='20' value='".$questionario->acreditacoes[$i]->tipo."'>";
		echo "</td>";
	
		echo "<td>";
		echo "<input class='inp-textAuto' type='text' maxlength='11' size='11' id='acreditacao_data_".$i."' name='acreditacao_data_".$i."' onfocus='calendario(\"acreditacao_data_".$i."\");' onkeypress='validateCal(event);' value='".$questionario->acreditacoes[$i]->data."'>";
		echo "</td>";
		
		echo "</tr>";
	
	}
	echo "<tr><td><input input type='image' src='../images/icon_new_s.png' name='navOption' value='Nova Acreditacao' onclick='if(formSubmited==0){return false;};document.questionario.operacao.value=70;'></td></tr>";
	
	echo "</table>";
	
	 
	echo "<input type='hidden' name='apagaRegAcreditacao' />";
	echo "</fieldset>";
	
	

	
	function getTipoFormando($i) {

		$db = new Database();
		$lValues =$db->getLookupValues("lista_tipoformandos");
	
		echo "<SELECT name='formandos_".$i."' >\n";
		echo "<option></option>\n";		
		while ($row = mysql_fetch_assoc($lValues)) {	
			echo "<option value='".$row["ID"]."'".checkFormando($row["ID"],$i).">".$row["DESCRICAO"]."</option>\n";
		}
		echo "</SELECT>";
		$db->disconnect();
				
	}	
				
	function checkFormando($id,$i){
		global $questionario;
		if($questionario->acoesformacao[$i]->formandos==$id)
			return "SELECTED";
		else 
			return "";
	}
	
	function getTipoAcao($i) {
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_tipoacaoformacao");
	
		echo "<SELECT name='tipo_".$i."' >\n";
		echo "<option></option>\n";
		while ($row = mysql_fetch_assoc($lValues)) {
			echo "<option value='".$row["ID"]."'".checkTipoAcao($row["ID"],$i).">".$row["DESCRICAO"]."</option>\n";
		}
		echo "</SELECT>";
		$db->disconnect();
	
	}
	
	function checkTipoAcao($id,$i){
		global $questionario;
		if($questionario->acoesformacao[$i]->tipo==$id)
			return "SELECTED";
		else
			return "";
	}
	
	
	
?>