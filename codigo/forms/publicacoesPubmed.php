<?php
echo "<div id='content9' style='display: inline;'>";
echo "<p><fieldset class='normal'>\n";
			echo "<legend>Publicações PubMed/<i>PubMed Publications</i></legend>\n";

echo "<h3>Atenção: Só introduzir aqui artigos que ainda não estejam na listagem do ISI </h3><br>";			
echo "<p class='ppthelp'>Seguir-se-á um processo automático de listagem de todas as suas publicações no PubMed. Verifique a tabela item a item e remova algum, se necessário, clicando no respetivo botão à esquerda. Uma vez que será necessária a produção e upload de um ficheiro de texto produzido no PubMed, seguem-se instruções detalhadas a esse respeito, que devem ser lidas antes da ligação ao PubMed. A procura e upload do ficheiro é, então, feita, de forma à produção automática da lista.</p>";			
echo "<p class='ppthelp'>Os artigos são separados em dois grupos: i) “Artigo”; ii) “Journal Abstract”: os Artigos que têm a página inicial subtraída da página final ≤1. <a href='../helpfiles/PubMed.JPG' target='_blank'>Ajuda/Help</a><p>";

echo "<br><p class='penhelp'><i>An automatic process will follow listing all your PubMed publications. Please check the table item by item and remove some, if necessary, by clicking on the respective button at left. Since the upload of a text file produced at the PubMed site will be needed, detailed instructions follow, with this regard, which must be read before connecting to PubMed. The search (“procurar”) and upload (“Enviar Ficheiro PubMed”) of the file is then made, in order to produce the list automatically.</i></p>";
echo "<br><p class='penhelp'><i>The articles are split into two groups: i) “Artigo”; ii) “Journal Abstract”, the papers with the first page subtracted from the last ≤1.</i></p>";
			

echo "<p><img src='../images/icon_help.png'><a href='../helpfiles/Ajuda_PubMed_bilingue.pdf' target='_blank'>INSTRUÇÕES DETALHADAS/<i>DETAILED INSTRUCTIONS</i></a>";
echo "&nbsp;&nbsp;&nbsp;&nbsp;<a href='http://www.ncbi.nlm.nih.gov/pubmed/' target='_blank'>LIGAR AO PubMed/<i>Connect to PubMed</i></a></p>";			

echo "<table  id='pubpubmed' class='box-table-a'><tr><td>";
echo "<input type='radio' name='append_pub_pubmed' value='1' onclick='document.getElementById(\"uploadisi\").style.display = \"block\"'> Acrescentar publicações à lista<br>";
echo "<input type='radio' name='append_pub_pubmed' value='0' onclick='document.getElementById(\"uploadisi\").style.display = \"block\"'> Substituir a lista de publicações";
echo "</td></tr><tr><td>";
echo "<div id='uploadisi' class='hidden'>";
echo "<input type='file' name='uploadedfilePUBMED'>";
echo "<input type='submit' name='navOption' value='Enviar Ficheiro PUBMED / Send PubMed File' onclick='if(document.questionario.uploadedfilePUBMED.value==\"\"){alert(\"Deve indicar um ficheiro/You must provide a file\");return false;}else{document.questionario.operacao.value=19;document.questionario.submit();}'>";
echo "</div>";
echo "</td></tr></table><br /><br />";


$outJPUBMED="";;
$outJAPUBMED="";
$outPRPPUBMED="";
$outPRPNPUBMED="";
$outCPPUBMED="";
$outCPNPUBMED="";
$outOAPUBMED="";
$outSPUBMED="";
$outJNPUBMED="";
$outSNPUBMED="";
$outLNPUBMED="";
$outCLNPUBMED="";
$outLPUBMED="";
$outCLPUBMED="";
$outPPUBMED="";
$outAPUBMED="";

$outRestoPUBMED="";




$tcols="
    <tr><th></th>
    	<th>ID</th>
		<th>ANO</th>
		<th>TIPO</th>
		<th>NOMEPUBLICACAO</th>
		<th>ISSN</th>
		<th>ISSN_LINKING</th>
		<th>ISSN_ELECTRONIC</th>
		<th>ISBN</th>
		<th>TITULO</th>
		<th>AUTORES</th>
		<th>VOLUME</th>
		<th>ISSUE</th>
		<th>AR</th>
		<th>COLECAO</th>
		<th>PRIMPAGINA</th>
		<th>ULTPAGINA</th>
		<th>IFACTOR</th>
		<th>CITACOES</th>
		<th>NPATENTE</th>
		<th>DATAPATENTE</th>
		<th>IPC</th>
		<th>AREA</th>
		<th>ESTADO</th>
		</tr>";					

$tcolsJPUBMED="
    <tr><th></th>
		<th>Revista (Journal)</th>
		<th>ISSN</th>
		<th>Título</th>
		<th>Autores</th>
		<th>Volume</th>
		<th>Issue</th>
		<th>1ª Pág.</th>
		<th>Última Pág.</th>
		<th>Estado</th>
		</tr>";

$tcolsCONFPUBMED="
    <tr><th></th>
		<th>ISBN</th>
		<th>Nome Publicação</th>
		<th>Editores</th>
		<th>Colecção</th>
		<th>Volume</th>
		<th>Título do Artigo</th>
		<th>Autores</th>
		<th>1ª Pág.</th>
		<th>Últ- Página</th>
		<th>Estado</th>
		</tr>";

$tcolsCLIVPUBMED="
    <tr><th></th>
		<th>ISBN</th>
		<th>NOME PUBLICACAO</th>
		<th>EDITORA</th>		
		<th>AUTORES</th>
		<th>TÍTULO ARTIGO</th>
		<th>PRIMEIRA PÁG.</th>
		<th>ÚLTIMA PÁG.</th>
		<th>Estado</th>
		</tr>";

$tcolsPATPUBMED="
    <tr><th></th>
		<th>Nº Patente</th>
		<th>IPC</th>
		<th>Título</th>
		<th>Autores</th>		
		<th>Data da Patente</th>
		<th>Estado</th>
		</tr>";

$tcolsLIVPUBMED="
    <tr><th></th>
		<th>ISBN</th>
		<th>TÍTULO</th>
		<th>AUTORES</th>		
		<th>EDITOR (Publisher)</th>
		<th>Estado</th>
		</tr>";
			    
	foreach ($questionario->publicacoesPUBMED as $i => $value){
			
			$tipo=$questionario->publicacoesPUBMED[$i]->tipofmup;
			
			//echo $tipo;
			
	switch($tipo){
				case 'J': { // Opção de entrada
					/*if($l==1){
						$l=0;
					}else{
						$l=1;
					}*/
		    		$outJPUBMED=$outJPUBMED."<tr>";
		    		$outJPUBMED=$outJPUBMED."<td id='icon_delete_s'>";
					$outJPUBMED=$outJPUBMED."<input type='image' src='../images/icon_delete.png' name='navOption' value='Apagar Publicacao PUBMED' onclick='if(formSubmited==0){return false;};document.questionario.apagaPublicacaoPUBMED.value=".$questionario->publicacoesPUBMED[$i]->id.";document.questionario.operacao.value=13;' >";
		    		$outJPUBMED=$outJPUBMED."</td>";
					//$outJPUBMED=$outJPUBMED."<td>";
					//$outJPUBMED=$outJPUBMED.$questionario->publicacoesPUBMED[$i]->id;
		    		//$outJPUBMED=$outJPUBMED."</td>";
		    		//$outJPUBMED=$outJPUBMED."<td>";
					//$outJPUBMED=$outJPUBMED.$questionario->publicacoesPUBMED[$i]->ano;
		    		//$outJPUBMED=$outJPUBMED."</td>";
/*		    		$outJPUBMED=$outJPUBMED."<td>";
					$outJPUBMED=$outJPUBMED.$questionario->publicacoesPUBMED[$i]->tipo;
		    		$outJPUBMED=$outJPUBMED."</td>";*/
		    		$outJPUBMED=$outJPUBMED."<td>";
					$outJPUBMED=$outJPUBMED.$questionario->publicacoesPUBMED[$i]->nomepublicacao;
		    		$outJPUBMED=$outJPUBMED."</td>";
		    		$outJPUBMED=$outJPUBMED."<td>";
					$outJPUBMED=$outJPUBMED.$questionario->publicacoesPUBMED[$i]->issn;
		    		$outJPUBMED=$outJPUBMED."</td>";
		    		/*$outJPUBMED=$outJPUBMED."<td>";
					$outJPUBMED=$outJPUBMED.$questionario->publicacoesPUBMED[$i]->issn_linking;
		    		$outJPUBMED=$outJPUBMED."</td>";
		    		$outJPUBMED=$outJPUBMED."<td>";
					$outJPUBMED=$outJPUBMED.$questionario->publicacoesPUBMED[$i]->issn_electronic;
		    		$outJPUBMED=$outJPUBMED."</td>";*/
		    		//$outJPUBMED=$outJPUBMED."<td>";
					//$outJPUBMED=$outJPUBMED.$questionario->publicacoesPUBMED[$i]->isbn;
		    		//$outJPUBMED=$outJPUBMED."</td>";
		    		$outJPUBMED=$outJPUBMED."<td>";
					$outJPUBMED=$outJPUBMED.$questionario->publicacoesPUBMED[$i]->titulo;
		    		$outJPUBMED=$outJPUBMED."</td>";
		    		$outJPUBMED=$outJPUBMED."<td>";
					$outJPUBMED=$outJPUBMED.$questionario->publicacoesPUBMED[$i]->autores;
		    		$outJPUBMED=$outJPUBMED."</td>";
					$outJPUBMED=$outJPUBMED."<td>";
					$outJPUBMED=$outJPUBMED.$questionario->publicacoesPUBMED[$i]->volume;
		    		$outJPUBMED=$outJPUBMED."</td>";
					$outJPUBMED=$outJPUBMED."<td>";
					$outJPUBMED=$outJPUBMED.$questionario->publicacoesPUBMED[$i]->issue;
		    		$outJPUBMED=$outJPUBMED."</td>";
					//$outJPUBMED=$outJPUBMED."<td>";
					//$outJPUBMED=$outJPUBMED.$questionario->publicacoesPUBMED[$i]->ar;
		    		//$outJPUBMED=$outJPUBMED."</td>";
					//$outJPUBMED=$outJPUBMED."<td>";
					//$outJPUBMED=$outJPUBMED.$questionario->publicacoesPUBMED[$i]->colecao;
		    		//$outJPUBMED=$outJPUBMED."</td>";
					$outJPUBMED=$outJPUBMED."<td>";
					$outJPUBMED=$outJPUBMED.$questionario->publicacoesPUBMED[$i]->primpagina;
		    		$outJPUBMED=$outJPUBMED."</td>";			
					$outJPUBMED=$outJPUBMED."<td>";
					$outJPUBMED=$outJPUBMED.$questionario->publicacoesPUBMED[$i]->ultpagina;
		    		$outJPUBMED=$outJPUBMED."</td>";
		    		/*$outJPUBMED=$outJPUBMED."<td>";
					$outJPUBMED=$outJPUBMED.$questionario->publicacoesPUBMED[$i]->ifactor;
		    		$outJPUBMED=$outJPUBMED."</td>";
		    		$outJPUBMED=$outJPUBMED."<td>";
					$outJPUBMED=$outJPUBMED.$questionario->publicacoesPUBMED[$i]->citacoes;
		    		$outJPUBMED=$outJPUBMED."</td>";*/

		    		//$outJPUBMED=$outJPUBMED."<td>";
					//$outJPUBMED=$outJPUBMED.$questionario->publicacoesPUBMED[$i]->conftitle;
		    		//$outJPUBMED=$outJPUBMED."</td>";
		    		//$outJPUBMED=$outJPUBMED."<td>";
					//$outJPUBMED=$outJPUBMED.$questionario->publicacoesPUBMED[$i]->language;
		    		//$outJPUBMED=$outJPUBMED."</td>";
		    		//$outJPUBMED=$outJPUBMED."<td>";
					//$outJPUBMED=$outJPUBMED.$questionario->publicacoesPUBMED[$i]->editor;
		    		//$outJPUBMED=$outJPUBMED."</td>";
		    		$outJPUBMED=$outJPUBMED."<td>";
					$outJPUBMED=$outJPUBMED.getTipoEstado($i);
		    		$outJPUBMED=$outJPUBMED."</td>";		    				    		
		    		$outJPUBMED=$outJPUBMED."</tr>";
				}
				break;
				case 'PRP': { // Opção de entrada
		    		$outPRPPUBMED=$outPRPPUBMED."<tr>";
		    		$outPRPPUBMED=$outPRPPUBMED."<td id='delete'>";
					$outPRPPUBMED=$outPRPPUBMED."<input type='image' src='../images/icon_delete_s.png' name='navOption' value='Apagar Publicacao PUBMED' onclick='if(formSubmited==0){return false;};document.questionario.apagaPublicacaoPUBMED.value=".$questionario->publicacoesPUBMED[$i]->id.";document.questionario.operacao.value=13;' >";
		    		$outPRPPUBMED=$outPRPPUBMED."</td>";
					//$outPRPPUBMED=$outPRPPUBMED."<td>";
					//$outPRPPUBMED=$outPRPPUBMED.$questionario->publicacoesPUBMED[$i]->id;
		    		//$outPRPPUBMED=$outPRPPUBMED."</td>";
		    		//$outPRPPUBMED=$outPRPPUBMED."<td>";
					//$outPRPPUBMED=$outPRPPUBMED.$questionario->publicacoesPUBMED[$i]->ano;
		    		//$outPRPPUBMED=$outPRPPUBMED."</td>";
/*		    		$outPRPPUBMED=$outPRPPUBMED."<td>";
					$outPRPPUBMED=$outPRPPUBMED.$questionario->publicacoesPUBMED[$i]->tipo;
		    		$outPRPPUBMED=$outPRPPUBMED."</td>";*/
		    		$outPRPPUBMED=$outPRPPUBMED."<td>";
					$outPRPPUBMED=$outPRPPUBMED.$questionario->publicacoesPUBMED[$i]->nomepublicacao;
		    		$outPRPPUBMED=$outPRPPUBMED."</td>";
		    		$outPRPPUBMED=$outPRPPUBMED."<td>";
					$outPRPPUBMED=$outPRPPUBMED.$questionario->publicacoesPUBMED[$i]->issn;
		    		$outPRPPUBMED=$outPRPPUBMED."</td>";
		    		/*$outPRPPUBMED=$outPRPPUBMED."<td>";
					$outPRPPUBMED=$outPRPPUBMED.$questionario->publicacoesPUBMED[$i]->issn_linking;
		    		$outPRPPUBMED=$outPRPPUBMED."</td>";
		    		$outPRPPUBMED=$outPRPPUBMED."<td>";
					$outPRPPUBMED=$outPRPPUBMED.$questionario->publicacoesPUBMED[$i]->issn_electronic;
		    		$outPRPPUBMED=$outPRPPUBMED."</td>";*/
		    		//$outPRPPUBMED=$outPRPPUBMED."<td>";
					//$outPRPPUBMED=$outPRPPUBMED.$questionario->publicacoesPUBMED[$i]->isbn;
		    		//$outPRPPUBMED=$outPRPPUBMED."</td>";
		    		$outPRPPUBMED=$outPRPPUBMED."<td>";
					$outPRPPUBMED=$outPRPPUBMED.$questionario->publicacoesPUBMED[$i]->titulo;
		    		$outPRPPUBMED=$outPRPPUBMED."</td>";
		    		$outPRPPUBMED=$outPRPPUBMED."<td>";
					$outPRPPUBMED=$outPRPPUBMED.$questionario->publicacoesPUBMED[$i]->autores;
		    		$outPRPPUBMED=$outPRPPUBMED."</td>";
					$outPRPPUBMED=$outPRPPUBMED."<td>";
					$outPRPPUBMED=$outPRPPUBMED.$questionario->publicacoesPUBMED[$i]->volume;
		    		$outPRPPUBMED=$outPRPPUBMED."</td>";
					$outPRPPUBMED=$outPRPPUBMED."<td>";
					$outPRPPUBMED=$outPRPPUBMED.$questionario->publicacoesPUBMED[$i]->issue;
		    		$outPRPPUBMED=$outPRPPUBMED."</td>";
					//$outPRPPUBMED=$outPRPPUBMED."<td>";
					//$outPRPPUBMED=$outPRPPUBMED.$questionario->publicacoesPUBMED[$i]->ar;
		    		//$outPRPPUBMED=$outPRPPUBMED."</td>";
					//$outPRPPUBMED=$outPRPPUBMED."<td>";
					//$outPRPPUBMED=$outPRPPUBMED.$questionario->publicacoesPUBMED[$i]->colecao;
		    		//$outPRPPUBMED=$outPRPPUBMED."</td>";
					$outPRPPUBMED=$outPRPPUBMED."<td>";
					$outPRPPUBMED=$outPRPPUBMED.$questionario->publicacoesPUBMED[$i]->primpagina;
		    		$outPRPPUBMED=$outPRPPUBMED."</td>";			
					$outPRPPUBMED=$outPRPPUBMED."<td>";
					$outPRPPUBMED=$outPRPPUBMED.$questionario->publicacoesPUBMED[$i]->ultpagina;
		    		$outPRPPUBMED=$outPRPPUBMED."</td>";
		    		/*$outPRPPUBMED=$outPRPPUBMED."<td>";
					$outPRPPUBMED=$outPRPPUBMED.$questionario->publicacoesPUBMED[$i]->ifactor;
		    		$outPRPPUBMED=$outPRPPUBMED."</td>";
		    		$outPRPPUBMED=$outPRPPUBMED."<td>";
					$outPRPPUBMED=$outPRPPUBMED.$questionario->publicacoesPUBMED[$i]->citacoes;
		    		$outPRPPUBMED=$outPRPPUBMED."</td>";*/

		    		//$outPRPPUBMED=$outPRPPUBMED."<td>";
					//$outPRPPUBMED=$outPRPPUBMED.$questionario->publicacoesPUBMED[$i]->conftitle;
		    		//$outPRPPUBMED=$outPRPPUBMED."</td>";
		    		//$outPRPPUBMED=$outPRPPUBMED."<td>";
					//$outPRPPUBMED=$outPRPPUBMED.$questionario->publicacoesPUBMED[$i]->language;
		    		//$outPRPPUBMED=$outPRPPUBMED."</td>";
		    		//$outPRPPUBMED=$outPRPPUBMED."<td>";
					//$outPRPPUBMED=$outPRPPUBMED.$questionario->publicacoesPUBMED[$i]->editor;
		    		//$outPRPPUBMED=$outPRPPUBMED."</td>";
		    		$outPRPPUBMED=$outPRPPUBMED."<td>";
					$outPRPPUBMED=$outPRPPUBMED.getTipoEstado($i);
		    		$outPRPPUBMED=$outPRPPUBMED."</td>";		    				    		
		    		$outPRPPUBMED=$outPRPPUBMED."</tr>";
				}
				break;
				case 'JA': { // Opção de entrada
		    		$outJAPUBMED=$outJAPUBMED."<tr>";
		    		$outJAPUBMED=$outJAPUBMED."<td id='delete'>";
					$outJAPUBMED=$outJAPUBMED."<input type='image' src='../images/icon_delete_s.png' name='navOption' value='Apagar Publicacao PUBMED' onclick='if(formSubmited==0){return false;};document.questionario.apagaPublicacaoPUBMED.value=".$questionario->publicacoesPUBMED[$i]->id.";document.questionario.operacao.value=13;' >";
		    		$outJAPUBMED=$outJAPUBMED."</td>";
					//$outJAPUBMED=$outJAPUBMED."<td>";
					//$outJAPUBMED=$outJAPUBMED.$questionario->publicacoesPUBMED[$i]->id;
		    		//$outJAPUBMED=$outJAPUBMED."</td>";
		    		//$outJAPUBMED=$outJAPUBMED."<td>";
					//$outJAPUBMED=$outJAPUBMED.$questionario->publicacoesPUBMED[$i]->ano;
		    		//$outJAPUBMED=$outJAPUBMED."</td>";
/*		    		$outJAPUBMED=$outJAPUBMED."<td>";
					$outJAPUBMED=$outJAPUBMED.$questionario->publicacoesPUBMED[$i]->tipo;
		    		$outJAPUBMED=$outJAPUBMED."</td>";*/
		    		$outJAPUBMED=$outJAPUBMED."<td>";
					$outJAPUBMED=$outJAPUBMED.$questionario->publicacoesPUBMED[$i]->nomepublicacao;
		    		$outJAPUBMED=$outJAPUBMED."</td>";
		    		$outJAPUBMED=$outJAPUBMED."<td>";
					$outJAPUBMED=$outJAPUBMED.$questionario->publicacoesPUBMED[$i]->issn;
		    		$outJAPUBMED=$outJAPUBMED."</td>";
		    		/*$outJAPUBMED=$outJAPUBMED."<td>";
					$outJAPUBMED=$outJAPUBMED.$questionario->publicacoesPUBMED[$i]->issn_linking;
		    		$outJAPUBMED=$outJAPUBMED."</td>";
		    		$outJAPUBMED=$outJAPUBMED."<td>";
					$outJAPUBMED=$outJAPUBMED.$questionario->publicacoesPUBMED[$i]->issn_electronic;
		    		$outJAPUBMED=$outJAPUBMED."</td>";*/
		    		//$outJAPUBMED=$outJAPUBMED."<td>";
					//$outJAPUBMED=$outJAPUBMED.$questionario->publicacoesPUBMED[$i]->isbn;
		    		//$outJAPUBMED=$outJAPUBMED."</td>";
		    		$outJAPUBMED=$outJAPUBMED."<td>";
					$outJAPUBMED=$outJAPUBMED.$questionario->publicacoesPUBMED[$i]->titulo;
		    		$outJAPUBMED=$outJAPUBMED."</td>";
		    		$outJAPUBMED=$outJAPUBMED."<td>";
					$outJAPUBMED=$outJAPUBMED.$questionario->publicacoesPUBMED[$i]->autores;
		    		$outJAPUBMED=$outJAPUBMED."</td>";
					$outJAPUBMED=$outJAPUBMED."<td>";
					$outJAPUBMED=$outJAPUBMED.$questionario->publicacoesPUBMED[$i]->volume;
		    		$outJAPUBMED=$outJAPUBMED."</td>";
					$outJAPUBMED=$outJAPUBMED."<td>";
					$outJAPUBMED=$outJAPUBMED.$questionario->publicacoesPUBMED[$i]->issue;
		    		$outJAPUBMED=$outJAPUBMED."</td>";
					//$outJAPUBMED=$outJAPUBMED."<td>";
					//$outJAPUBMED=$outJAPUBMED.$questionario->publicacoesPUBMED[$i]->ar;
		    		//$outJAPUBMED=$outJAPUBMED."</td>";
					//$outJAPUBMED=$outJAPUBMED."<td>";
					//$outJAPUBMED=$outJAPUBMED.$questionario->publicacoesPUBMED[$i]->colecao;
		    		//$outJAPUBMED=$outJAPUBMED."</td>";
					$outJAPUBMED=$outJAPUBMED."<td>";
					$outJAPUBMED=$outJAPUBMED.$questionario->publicacoesPUBMED[$i]->primpagina;
		    		$outJAPUBMED=$outJAPUBMED."</td>";			
					$outJAPUBMED=$outJAPUBMED."<td>";
					$outJAPUBMED=$outJAPUBMED.$questionario->publicacoesPUBMED[$i]->ultpagina;
		    		$outJAPUBMED=$outJAPUBMED."</td>";
		    		/*$outJAPUBMED=$outJAPUBMED."<td>";
					$outJAPUBMED=$outJAPUBMED.$questionario->publicacoesPUBMED[$i]->ifactor;
		    		$outJAPUBMED=$outJAPUBMED."</td>";
		    		$outJAPUBMED=$outJAPUBMED."<td>";
					$outJAPUBMED=$outJAPUBMED.$questionario->publicacoesPUBMED[$i]->citacoes;
		    		$outJAPUBMED=$outJAPUBMED."</td>";*/

		    		//$outJAPUBMED=$outJAPUBMED."<td>";
					//$outJAPUBMED=$outJAPUBMED.$questionario->publicacoesPUBMED[$i]->conftitle;
		    		//$outJAPUBMED=$outJAPUBMED."</td>";
		    		//$outJAPUBMED=$outJAPUBMED."<td>";
					//$outJAPUBMED=$outJAPUBMED.$questionario->publicacoesPUBMED[$i]->language;
		    		//$outJAPUBMED=$outJAPUBMED."</td>";
		    		//$outJAPUBMED=$outJAPUBMED."<td>";
					//$outJAPUBMED=$outJAPUBMED.$questionario->publicacoesPUBMED[$i]->editor;
		    		//$outJAPUBMED=$outJAPUBMED."</td>";
		    		$outJAPUBMED=$outJAPUBMED."<td>";
					$outJAPUBMED=$outJAPUBMED.getTipoEstado($i);
		    		$outJAPUBMED=$outJAPUBMED."</td>";		    				    		
		    		$outJAPUBMED=$outJAPUBMED."</tr>";
				}
				break;
				case 'PRPN': { // Opção de entrada
		    		$outJAPUBMED=$outPRPNPUBMED."<tr>";
		    		$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED."<input type='image' src='../images/icon_delete_s.png' name='navOption' value='Apagar Publicacao PUBMED' onclick='if(formSubmited==0){return false;};document.questionario.apagaPublicacaoPUBMED.value=".$questionario->publicacoesPUBMED[$i]->id.";document.questionario.operacao.value=13;' >";
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";
					$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$questionario->publicacoesPUBMED[$i]->id;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";
		    		$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$questionario->publicacoesPUBMED[$i]->ano;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";
		    		/*$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$questionario->publicacoesPUBMED[$i]->tipo;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";*/
		    		$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$questionario->publicacoesPUBMED[$i]->nomepublicacao;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";
		    		$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$questionario->publicacoesPUBMED[$i]->issn;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";
		    		/*$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$questionario->publicacoesPUBMED[$i]->issn_linking;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";
		    		$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$questionario->publicacoesPUBMED[$i]->issn_electronic;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";*/
		    		$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$questionario->publicacoesPUBMED[$i]->isbn;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";
		    		$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$questionario->publicacoesPUBMED[$i]->titulo;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";
		    		$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$questionario->publicacoesPUBMED[$i]->autores;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";
					$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$questionario->publicacoesPUBMED[$i]->volume;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";
					$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$questionario->publicacoesPUBMED[$i]->issue;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";
					$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$questionario->publicacoesPUBMED[$i]->ar;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";
					$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$questionario->publicacoesPUBMED[$i]->colecao;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";
					$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$questionario->publicacoesPUBMED[$i]->primpagina;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";			
					$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$questionario->publicacoesPUBMED[$i]->ultpagina;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";
		    		$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$questionario->publicacoesPUBMED[$i]->ifactor;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";
		    		$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$questionario->publicacoesPUBMED[$i]->citacoes;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";
		    		$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$questionario->publicacoesPUBMED[$i]->conftitle;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";
		    		$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$questionario->publicacoesPUBMED[$i]->language;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";
		    		$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$questionario->publicacoesPUBMED[$i]->editor;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";
		    		$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.getTipoEstado($i);
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";		    				    		
		    		$outPRPNPUBMED=$outPRPNPUBMED."</tr>";
				}
				break;
				case 'CP': { // Opção de entrada
		    		$outCPPUBMED=$outCPPUBMED."<tr>";
		    		$outCPPUBMED=$outCPPUBMED."<td>";
					$outCPPUBMED=$outCPPUBMED."<input type='image' src='../images/icon_delete_s.png' name='navOption' value='Apagar Publicacao PUBMED' onclick='if(formSubmited==0){return false;};document.questionario.apagaPublicacaoPUBMED.value=".$questionario->publicacoesPUBMED[$i]->id.";document.questionario.operacao.value=13;' >";
		    		$outCPPUBMED=$outCPPUBMED."</td>";

		    		$outCPPUBMED=$outCPPUBMED."<td>";
					$outCPPUBMED=$outCPPUBMED.$questionario->publicacoesPUBMED[$i]->nomepublicacao;
		    		$outCPPUBMED=$outCPPUBMED."</td>";		    		    		
		    		$outCPPUBMED=$outCPPUBMED."<td>";
					$outCPPUBMED=$outCPPUBMED.$questionario->publicacoesPUBMED[$i]->isbn;
		    		$outCPPUBMED=$outCPPUBMED."</td>";		    		
		    		$outCPPUBMED=$outCPPUBMED."<td>";
					$outCPPUBMED=$outCPPUBMED.$questionario->publicacoesPUBMED[$i]->editor;
		    		$outCPPUBMED=$outCPPUBMED."</td>";		    		
					$outCPPUBMED=$outCPPUBMED."<td>";
					$outCPPUBMED=$outCPPUBMED.$questionario->publicacoesPUBMED[$i]->colecao;
		    		$outCPPUBMED=$outCPPUBMED."</td>";
					$outCPPUBMED=$outCPPUBMED."<td>";
					$outCPPUBMED=$outCPPUBMED.$questionario->publicacoesPUBMED[$i]->volume;
		    		$outCPPUBMED=$outCPPUBMED."</td>";
		    		$outCPPUBMED=$outCPPUBMED."<td>";
					$outCPPUBMED=$outCPPUBMED.$questionario->publicacoesPUBMED[$i]->titulo;
		    		$outCPPUBMED=$outCPPUBMED."</td>";		    		
		    		$outCPPUBMED=$outCPPUBMED."<td>";
					$outCPPUBMED=$outCPPUBMED.$questionario->publicacoesPUBMED[$i]->autores;
		    		$outCPPUBMED=$outCPPUBMED."</td>";		    		
					$outCPPUBMED=$outCPPUBMED."<td>";
					$outCPPUBMED=$outCPPUBMED.$questionario->publicacoesPUBMED[$i]->primpagina;
		    		$outCPPUBMED=$outCPPUBMED."</td>";			
					$outCPPUBMED=$outCPPUBMED."<td>";
					$outCPPUBMED=$outCPPUBMED.$questionario->publicacoesPUBMED[$i]->ultpagina;
		    		$outCPPUBMED=$outCPPUBMED."</td>";
		    		
		    		
/*					$outCPPUBMED=$outCPPUBMED."<td>";
					$outCPPUBMED=$outCPPUBMED.$questionario->publicacoesPUBMED[$i]->id;
		    		$outCPPUBMED=$outCPPUBMED."</td>";
		    		$outCPPUBMED=$outCPPUBMED."<td>";
					$outCPPUBMED=$outCPPUBMED.$questionario->publicacoesPUBMED[$i]->ano;
		    		$outCPPUBMED=$outCPPUBMED."</td>";
		    		$outCPPUBMED=$outCPPUBMED."<td>";
					$outCPPUBMED=$outCPPUBMED.$questionario->publicacoesPUBMED[$i]->tipo;
		    		$outCPPUBMED=$outCPPUBMED."</td>";
		    		$outCPPUBMED=$outCPPUBMED."<td>";
					$outCPPUBMED=$outCPPUBMED.$questionario->publicacoesPUBMED[$i]->issn;
		    		$outCPPUBMED=$outCPPUBMED."</td>";
		    		$outCPPUBMED=$outCPPUBMED."<td>";
					$outCPPUBMED=$outCPPUBMED.$questionario->publicacoesPUBMED[$i]->issn_linking;
		    		$outCPPUBMED=$outCPPUBMED."</td>";
		    		$outCPPUBMED=$outCPPUBMED."<td>";
					$outCPPUBMED=$outCPPUBMED.$questionario->publicacoesPUBMED[$i]->issn_electronic;
		    		$outCPPUBMED=$outCPPUBMED."</td>";
					$outCPPUBMED=$outCPPUBMED."<td>";
					$outCPPUBMED=$outCPPUBMED.$questionario->publicacoesPUBMED[$i]->issue;
		    		$outCPPUBMED=$outCPPUBMED."</td>";
					$outCPPUBMED=$outCPPUBMED."<td>";
					$outCPPUBMED=$outCPPUBMED.$questionario->publicacoesPUBMED[$i]->ar;
		    		$outCPPUBMED=$outCPPUBMED."</td>";
		    		$outCPPUBMED=$outCPPUBMED."<td>";
					$outCPPUBMED=$outCPPUBMED.$questionario->publicacoesPUBMED[$i]->citacoes;
		    		$outCPPUBMED=$outCPPUBMED."</td>";
		    		$outCPPUBMED=$outCPPUBMED."<td>";
					$outCPPUBMED=$outCPPUBMED.$questionario->publicacoesPUBMED[$i]->ifactor;
		    		$outCPPUBMED=$outCPPUBMED."</td>";
		    		$outCPPUBMED=$outCPPUBMED."<td>";
					$outCPPUBMED=$outCPPUBMED.$questionario->publicacoesPUBMED[$i]->conftitle;
		    		$outCPPUBMED=$outCPPUBMED."</td>";
		    		$outCPPUBMED=$outCPPUBMED."<td>";
					$outCPPUBMED=$outCPPUBMED.$questionario->publicacoesPUBMED[$i]->language;
		    		$outCPPUBMED=$outCPPUBMED."</td>";
		    		*/
		    		$outCPPUBMED=$outCPPUBMED."<td>";
					$outCPPUBMED=$outCPPUBMED.getTipoEstado($i);
		    		$outCPPUBMED=$outCPPUBMED."</td>";		    				    		
		    		$outCPPUBMED=$outCPPUBMED."</tr>";
				}
				break;
				case 'CPN': { // Opção de entrada
		    		$outCPNPUBMED=$outCPNPUBMED."<tr>";
		    		$outCPNPUBMED=$outCPNPUBMED."<td>";
					$outCPNPUBMED=$outCPNPUBMED."<input type='image' src='../images/icon_delete_s.png' name='navOption' value='Apagar Publicacao PUBMED' onclick='if(formSubmited==0){return false;};document.questionario.apagaPublicacaoPUBMED.value=".$questionario->publicacoesPUBMED[$i]->id.";document.questionario.operacao.value=13;' >";
		    		$outCPNPUBMED=$outCPNPUBMED."</td>";

		    		$outCPNPUBMED=$outCPNPUBMED."<td>";
					$outCPNPUBMED=$outCPNPUBMED.$questionario->publicacoesPUBMED[$i]->nomepublicacao;
		    		$outCPNPUBMED=$outCPNPUBMED."</td>";		    		    		
		    		$outCPNPUBMED=$outCPNPUBMED."<td>";
					$outCPNPUBMED=$outCPNPUBMED.$questionario->publicacoesPUBMED[$i]->isbn;
		    		$outCPNPUBMED=$outCPNPUBMED."</td>";		    		
		    		$outCPNPUBMED=$outCPNPUBMED."<td>";
					$outCPNPUBMED=$outCPNPUBMED.$questionario->publicacoesPUBMED[$i]->editor;
		    		$outCPNPUBMED=$outCPNPUBMED."</td>";		    		
					$outCPNPUBMED=$outCPNPUBMED."<td>";
					$outCPNPUBMED=$outCPNPUBMED.$questionario->publicacoesPUBMED[$i]->colecao;
		    		$outCPNPUBMED=$outCPNPUBMED."</td>";
					$outCPNPUBMED=$outCPNPUBMED."<td>";
					$outCPNPUBMED=$outCPNPUBMED.$questionario->publicacoesPUBMED[$i]->volume;
		    		$outCPNPUBMED=$outCPNPUBMED."</td>";
		    		$outCPNPUBMED=$outCPNPUBMED."<td>";
					$outCPNPUBMED=$outCPNPUBMED.$questionario->publicacoesPUBMED[$i]->titulo;
		    		$outCPNPUBMED=$outCPNPUBMED."</td>";		    		
		    		$outCPNPUBMED=$outCPNPUBMED."<td>";
					$outCPNPUBMED=$outCPNPUBMED.$questionario->publicacoesPUBMED[$i]->autores;
		    		$outCPNPUBMED=$outCPNPUBMED."</td>";		    		
					$outCPNPUBMED=$outCPNPUBMED."<td>";
					$outCPNPUBMED=$outCPNPUBMED.$questionario->publicacoesPUBMED[$i]->primpagina;
		    		$outCPNPUBMED=$outCPNPUBMED."</td>";			
					$outCPNPUBMED=$outCPNPUBMED."<td>";
					$outCPNPUBMED=$outCPNPUBMED.$questionario->publicacoesPUBMED[$i]->ultpagina;
		    		$outCPNPUBMED=$outCPNPUBMED."</td>";
		    		
		    		
/*					$outCPNPUBMED=$outCPNPUBMED."<td>";
					$outCPNPUBMED=$outCPNPUBMED.$questionario->publicacoesPUBMED[$i]->id;
		    		$outCPNPUBMED=$outCPNPUBMED."</td>";
		    		$outCPNPUBMED=$outCPNPUBMED."<td>";
					$outCPNPUBMED=$outCPNPUBMED.$questionario->publicacoesPUBMED[$i]->ano;
		    		$outCPNPUBMED=$outCPNPUBMED."</td>";
		    		$outCPNPUBMED=$outCPNPUBMED."<td>";
					$outCPNPUBMED=$outCPNPUBMED.$questionario->publicacoesPUBMED[$i]->tipo;
		    		$outCPNPUBMED=$outCPNPUBMED."</td>";
		    		$outCPNPUBMED=$outCPNPUBMED."<td>";
					$outCPNPUBMED=$outCPNPUBMED.$questionario->publicacoesPUBMED[$i]->issn;
		    		$outCPNPUBMED=$outCPNPUBMED."</td>";
		    		$outCPNPUBMED=$outCPNPUBMED."<td>";
					$outCPNPUBMED=$outCPNPUBMED.$questionario->publicacoesPUBMED[$i]->issn_linking;
		    		$outCPNPUBMED=$outCPNPUBMED."</td>";
		    		$outCPNPUBMED=$outCPNPUBMED."<td>";
					$outCPNPUBMED=$outCPNPUBMED.$questionario->publicacoesPUBMED[$i]->issn_electronic;
		    		$outCPNPUBMED=$outCPNPUBMED."</td>";
					$outCPNPUBMED=$outCPNPUBMED."<td>";
					$outCPNPUBMED=$outCPNPUBMED.$questionario->publicacoesPUBMED[$i]->issue;
		    		$outCPNPUBMED=$outCPNPUBMED."</td>";
					$outCPNPUBMED=$outCPNPUBMED."<td>";
					$outCPNPUBMED=$outCPNPUBMED.$questionario->publicacoesPUBMED[$i]->ar;
		    		$outCPNPUBMED=$outCPNPUBMED."</td>";
		    		$outCPNPUBMED=$outCPNPUBMED."<td>";
					$outCPNPUBMED=$outCPNPUBMED.$questionario->publicacoesPUBMED[$i]->citacoes;
		    		$outCPNPUBMED=$outCPNPUBMED."</td>";
		    		$outCPNPUBMED=$outCPNPUBMED."<td>";
					$outCPNPUBMED=$outCPNPUBMED.$questionario->publicacoesPUBMED[$i]->ifactor;
		    		$outCPNPUBMED=$outCPNPUBMED."</td>";
		    		$outCPNPUBMED=$outCPNPUBMED."<td>";
					$outCPNPUBMED=$outCPNPUBMED.$questionario->publicacoesPUBMED[$i]->conftitle;
		    		$outCPNPUBMED=$outCPNPUBMED."</td>";
		    		$outCPNPUBMED=$outCPNPUBMED."<td>";
					$outCPNPUBMED=$outCPNPUBMED.$questionario->publicacoesPUBMED[$i]->language;
		    		$outCPNPUBMED=$outCPNPUBMED."</td>";
		    		*/$outCPNPUBMED=$outCPNPUBMED."<td>";
					$outCPNPUBMED=$outCPNPUBMED.getTipoEstado($i);
		    		$outCPNPUBMED=$outCPNPUBMED."</td>";		    				    		
		    		$outCPNPUBMED=$outCPNPUBMED."</tr>";
				}
				break;
				case 'OA': { // Opção de entrada
		    		$outOAPUBMED=$outOAPUBMED."<tr>";
		    		$outOAPUBMED=$outOAPUBMED."<td>";
					$outOAPUBMED=$outOAPUBMED."<input type='image' src='../images/icon_delete_s.png' name='navOption' value='Apagar Publicacao PUBMED' onclick='if(formSubmited==0){return false;};document.questionario.apagaPublicacaoPUBMED.value=".$questionario->publicacoesPUBMED[$i]->id.";document.questionario.operacao.value=13;' >";
		    		$outOAPUBMED=$outOAPUBMED."</td>";

		    		$outOAPUBMED=$outOAPUBMED."<td>";
					$outOAPUBMED=$outOAPUBMED.$questionario->publicacoesPUBMED[$i]->nomepublicacao;
		    		$outOAPUBMED=$outOAPUBMED."</td>";		    		    		
		    		$outOAPUBMED=$outOAPUBMED."<td>";
					$outOAPUBMED=$outOAPUBMED.$questionario->publicacoesPUBMED[$i]->isbn;
		    		$outOAPUBMED=$outOAPUBMED."</td>";		    		
		    		$outOAPUBMED=$outOAPUBMED."<td>";
					$outOAPUBMED=$outOAPUBMED.$questionario->publicacoesPUBMED[$i]->editor;
		    		$outOAPUBMED=$outOAPUBMED."</td>";		    		
					$outOAPUBMED=$outOAPUBMED."<td>";
					$outOAPUBMED=$outOAPUBMED.$questionario->publicacoesPUBMED[$i]->colecao;
		    		$outOAPUBMED=$outOAPUBMED."</td>";
					$outOAPUBMED=$outOAPUBMED."<td>";
					$outOAPUBMED=$outOAPUBMED.$questionario->publicacoesPUBMED[$i]->volume;
		    		$outOAPUBMED=$outOAPUBMED."</td>";
		    		$outOAPUBMED=$outOAPUBMED."<td>";
					$outOAPUBMED=$outOAPUBMED.$questionario->publicacoesPUBMED[$i]->titulo;
		    		$outOAPUBMED=$outOAPUBMED."</td>";		    		
		    		$outOAPUBMED=$outOAPUBMED."<td>";
					$outOAPUBMED=$outOAPUBMED.$questionario->publicacoesPUBMED[$i]->autores;
		    		$outOAPUBMED=$outOAPUBMED."</td>";		    		
					$outOAPUBMED=$outOAPUBMED."<td>";
					$outOAPUBMED=$outOAPUBMED.$questionario->publicacoesPUBMED[$i]->primpagina;
		    		$outOAPUBMED=$outOAPUBMED."</td>";			
					$outOAPUBMED=$outOAPUBMED."<td>";
					$outOAPUBMED=$outOAPUBMED.$questionario->publicacoesPUBMED[$i]->ultpagina;
		    		$outOAPUBMED=$outOAPUBMED."</td>";
		    		
		    		
/*					$outOAPUBMED=$outOAPUBMED."<td>";
					$outOAPUBMED=$outOAPUBMED.$questionario->publicacoesPUBMED[$i]->id;
		    		$outOAPUBMED=$outOAPUBMED."</td>";
		    		$outOAPUBMED=$outOAPUBMED."<td>";
					$outOAPUBMED=$outOAPUBMED.$questionario->publicacoesPUBMED[$i]->ano;
		    		$outOAPUBMED=$outOAPUBMED."</td>";
		    		$outOAPUBMED=$outOAPUBMED."<td>";
					$outOAPUBMED=$outOAPUBMED.$questionario->publicacoesPUBMED[$i]->tipo;
		    		$outOAPUBMED=$outOAPUBMED."</td>";
		    		$outOAPUBMED=$outOAPUBMED."<td>";
					$outOAPUBMED=$outOAPUBMED.$questionario->publicacoesPUBMED[$i]->issn;
		    		$outOAPUBMED=$outOAPUBMED."</td>";
		    		$outOAPUBMED=$outOAPUBMED."<td>";
					$outOAPUBMED=$outOAPUBMED.$questionario->publicacoesPUBMED[$i]->issn_linking;
		    		$outOAPUBMED=$outOAPUBMED."</td>";
		    		$outOAPUBMED=$outOAPUBMED."<td>";
					$outOAPUBMED=$outOAPUBMED.$questionario->publicacoesPUBMED[$i]->issn_electronic;
		    		$outOAPUBMED=$outOAPUBMED."</td>";
					$outOAPUBMED=$outOAPUBMED."<td>";
					$outOAPUBMED=$outOAPUBMED.$questionario->publicacoesPUBMED[$i]->issue;
		    		$outOAPUBMED=$outOAPUBMED."</td>";
					$outOAPUBMED=$outOAPUBMED."<td>";
					$outOAPUBMED=$outOAPUBMED.$questionario->publicacoesPUBMED[$i]->ar;
		    		$outOAPUBMED=$outOAPUBMED."</td>";
		    		$outOAPUBMED=$outOAPUBMED."<td>";
					$outOAPUBMED=$outOAPUBMED.$questionario->publicacoesPUBMED[$i]->citacoes;
		    		$outOAPUBMED=$outOAPUBMED."</td>";
		    		$outOAPUBMED=$outOAPUBMED."<td>";
					$outOAPUBMED=$outOAPUBMED.$questionario->publicacoesPUBMED[$i]->ifactor;
		    		$outOAPUBMED=$outOAPUBMED."</td>";
		    		$outOAPUBMED=$outOAPUBMED."<td>";
					$outOAPUBMED=$outOAPUBMED.$questionario->publicacoesPUBMED[$i]->conftitle;
		    		$outOAPUBMED=$outOAPUBMED."</td>";
		    		$outOAPUBMED=$outOAPUBMED."<td>";
					$outOAPUBMED=$outOAPUBMED.$questionario->publicacoesPUBMED[$i]->language;
		    		$outOAPUBMED=$outOAPUBMED."</td>";
		    		*/$outOAPUBMED=$outOAPUBMED."<td>";
					$outOAPUBMED=$outOAPUBMED.getTipoEstado($i);
		    		$outOAPUBMED=$outOAPUBMED."</td>";		    				    		
		    		$outOAPUBMED=$outOAPUBMED."</tr>";
				}
				break;
				case 'S': { // Opção de entrada
		    		$outSPUBMED=$outSPUBMED."<tr>";
		    		$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED."<input type='image' src='../images/icon_delete_s.png' name='navOption' value='Apagar Publicacao PUBMED' onclick='if(formSubmited==0){return false;};document.questionario.apagaPublicacaoPUBMED.value=".$questionario->publicacoesPUBMED[$i]->id.";document.questionario.operacao.value=13;' >";
		    		$outSPUBMED=$outSPUBMED."</td>";
					$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$questionario->publicacoesPUBMED[$i]->id;
		    		$outSPUBMED=$outSPUBMED."</td>";
		    		$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$questionario->publicacoesPUBMED[$i]->ano;
		    		$outSPUBMED=$outSPUBMED."</td>";
		    		/*$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$questionario->publicacoesPUBMED[$i]->tipo;
		    		$outSPUBMED=$outSPUBMED."</td>";*/
		    		$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$questionario->publicacoesPUBMED[$i]->nomepublicacao;
		    		$outSPUBMED=$outSPUBMED."</td>";
		    		$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$questionario->publicacoesPUBMED[$i]->issn;
		    		$outSPUBMED=$outSPUBMED."</td>";
		    		/*$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$questionario->publicacoesPUBMED[$i]->issn_linking;
		    		$outSPUBMED=$outSPUBMED."</td>";
		    		$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$questionario->publicacoesPUBMED[$i]->issn_electronic;
		    		$outSPUBMED=$outSPUBMED."</td>";*/
		    		$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$questionario->publicacoesPUBMED[$i]->isbn;
		    		$outSPUBMED=$outSPUBMED."</td>";
		    		$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$questionario->publicacoesPUBMED[$i]->titulo;
		    		$outSPUBMED=$outSPUBMED."</td>";
		    		$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$questionario->publicacoesPUBMED[$i]->autores;
		    		$outSPUBMED=$outSPUBMED."</td>";
					$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$questionario->publicacoesPUBMED[$i]->volume;
		    		$outSPUBMED=$outSPUBMED."</td>";
					$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$questionario->publicacoesPUBMED[$i]->issue;
		    		$outSPUBMED=$outSPUBMED."</td>";
					$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$questionario->publicacoesPUBMED[$i]->ar;
		    		$outSPUBMED=$outSPUBMED."</td>";
					$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$questionario->publicacoesPUBMED[$i]->colecao;
		    		$outSPUBMED=$outSPUBMED."</td>";
					$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$questionario->publicacoesPUBMED[$i]->primpagina;
		    		$outSPUBMED=$outSPUBMED."</td>";			
					$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$questionario->publicacoesPUBMED[$i]->ultpagina;
		    		$outSPUBMED=$outSPUBMED."</td>";
		    		$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$questionario->publicacoesPUBMED[$i]->ifactor;
		    		$outSPUBMED=$outSPUBMED."</td>";
		    		$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$questionario->publicacoesPUBMED[$i]->citacoes;
		    		$outSPUBMED=$outSPUBMED."</td>";
		    		$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$questionario->publicacoesPUBMED[$i]->conftitle;
		    		$outSPUBMED=$outSPUBMED."</td>";
		    		$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$questionario->publicacoesPUBMED[$i]->language;
		    		$outSPUBMED=$outSPUBMED."</td>";
		    		$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$questionario->publicacoesPUBMED[$i]->editor;
		    		$outSPUBMED=$outSPUBMED."</td>";
		    		$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.getTipoEstado($i);
		    		$outSPUBMED=$outSPUBMED."</td>";		    				    		
		    		$outSPUBMED=$outSPUBMED."</tr>";
				}
				break;
				case 'JN': { // Opção de entrada
		    		$outJNPUBMED=$outJNPUBMED."<tr>";
		    		$outJNPUBMED=$outJNPUBMED."<td id='delete'>";
					$outJNPUBMED=$outJNPUBMED."<input type='image' src='../images/icon_delete_s.png' name='navOption' value='Apagar Publicacao PUBMED' onclick='if(formSubmited==0){return false;};document.questionario.apagaPublicacaoPUBMED.value=".$questionario->publicacoesPUBMED[$i]->id.";document.questionario.operacao.value=13;' >";
		    		$outJNPUBMED=$outJNPUBMED."</td>";
					//$outJNPUBMED=$outJNPUBMED."<td>";
					//$outJNPUBMED=$outJNPUBMED.$questionario->publicacoesPUBMED[$i]->id;
		    		//$outJNPUBMED=$outJNPUBMED."</td>";
		    		//$outJNPUBMED=$outJNPUBMED."<td>";
					//$outJNPUBMED=$outJNPUBMED.$questionario->publicacoesPUBMED[$i]->ano;
		    		//$outJNPUBMED=$outJNPUBMED."</td>";
/*		    		$outJNPUBMED=$outJNPUBMED."<td>";
					$outJNPUBMED=$outJNPUBMED.$questionario->publicacoesPUBMED[$i]->tipo;
		    		$outJNPUBMED=$outJNPUBMED."</td>";*/
		    		$outJNPUBMED=$outJNPUBMED."<td>";
					$outJNPUBMED=$outJNPUBMED.$questionario->publicacoesPUBMED[$i]->nomepublicacao;
		    		$outJNPUBMED=$outJNPUBMED."</td>";
		    		$outJNPUBMED=$outJNPUBMED."<td>";
					$outJNPUBMED=$outJNPUBMED.$questionario->publicacoesPUBMED[$i]->issn;
		    		$outJNPUBMED=$outJNPUBMED."</td>";
		    		/*$outJNPUBMED=$outJNPUBMED."<td>";
					$outJNPUBMED=$outJNPUBMED.$questionario->publicacoesPUBMED[$i]->issn_linking;
		    		$outJNPUBMED=$outJNPUBMED."</td>";
		    		$outJNPUBMED=$outJNPUBMED."<td>";
					$outJNPUBMED=$outJNPUBMED.$questionario->publicacoesPUBMED[$i]->issn_electronic;
		    		$outJNPUBMED=$outJNPUBMED."</td>";*/
		    		//$outJNPUBMED=$outJNPUBMED."<td>";
					//$outJNPUBMED=$outJNPUBMED.$questionario->publicacoesPUBMED[$i]->isbn;
		    		//$outJNPUBMED=$outJNPUBMED."</td>";
		    		$outJNPUBMED=$outJNPUBMED."<td>";
					$outJNPUBMED=$outJNPUBMED.$questionario->publicacoesPUBMED[$i]->titulo;
		    		$outJNPUBMED=$outJNPUBMED."</td>";
		    		$outJNPUBMED=$outJNPUBMED."<td>";
					$outJNPUBMED=$outJNPUBMED.$questionario->publicacoesPUBMED[$i]->autores;
		    		$outJNPUBMED=$outJNPUBMED."</td>";
					$outJNPUBMED=$outJNPUBMED."<td>";
					$outJNPUBMED=$outJNPUBMED.$questionario->publicacoesPUBMED[$i]->volume;
		    		$outJNPUBMED=$outJNPUBMED."</td>";
					$outJNPUBMED=$outJNPUBMED."<td>";
					$outJNPUBMED=$outJNPUBMED.$questionario->publicacoesPUBMED[$i]->issue;
		    		$outJNPUBMED=$outJNPUBMED."</td>";
					//$outJNPUBMED=$outJNPUBMED."<td>";
					//$outJNPUBMED=$outJNPUBMED.$questionario->publicacoesPUBMED[$i]->ar;
		    		//$outJNPUBMED=$outJNPUBMED."</td>";
					//$outJNPUBMED=$outJNPUBMED."<td>";
					//$outJNPUBMED=$outJNPUBMED.$questionario->publicacoesPUBMED[$i]->colecao;
		    		//$outJNPUBMED=$outJNPUBMED."</td>";
					$outJNPUBMED=$outJNPUBMED."<td>";
					$outJNPUBMED=$outJNPUBMED.$questionario->publicacoesPUBMED[$i]->primpagina;
		    		$outJNPUBMED=$outJNPUBMED."</td>";			
					$outJNPUBMED=$outJNPUBMED."<td>";
					$outJNPUBMED=$outJNPUBMED.$questionario->publicacoesPUBMED[$i]->ultpagina;
		    		$outJNPUBMED=$outJNPUBMED."</td>";
		    		/*$outJNPUBMED=$outJNPUBMED."<td>";
					$outJNPUBMED=$outJNPUBMED.$questionario->publicacoesPUBMED[$i]->ifactor;
		    		$outJNPUBMED=$outJNPUBMED."</td>";
		    		$outJNPUBMED=$outJNPUBMED."<td>";
					$outJNPUBMED=$outJNPUBMED.$questionario->publicacoesPUBMED[$i]->citacoes;
		    		$outJNPUBMED=$outJNPUBMED."</td>";*/

		    		//$outJNPUBMED=$outJNPUBMED."<td>";
					//$outJNPUBMED=$outJNPUBMED.$questionario->publicacoesPUBMED[$i]->conftitle;
		    		//$outJNPUBMED=$outJNPUBMED."</td>";
		    		//$outJNPUBMED=$outJNPUBMED."<td>";
					//$outJNPUBMED=$outJNPUBMED.$questionario->publicacoesPUBMED[$i]->language;
		    		//$outJNPUBMED=$outJNPUBMED."</td>";
		    		//$outJNPUBMED=$outJNPUBMED."<td>";
					//$outJNPUBMED=$outJNPUBMED.$questionario->publicacoesPUBMED[$i]->editor;
		    		//$outJNPUBMED=$outJNPUBMED."</td>";
		    		$outJNPUBMED=$outJNPUBMED."<td>";
					$outJNPUBMED=$outJNPUBMED.getTipoEstado($i);
		    		$outJNPUBMED=$outJNPUBMED."</td>";		    				    		
		    		$outJNPUBMED=$outJNPUBMED."</tr>";
				}
				break;
				case 'SN': { // Opção de entrada
		    		$outSNPUBMED=$outSNPUBMED."<tr>";
		    		$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED."<input type='image' src='../images/icon_delete_s.png' name='navOption' value='Apagar Publicacao PUBMED' onclick='if(formSubmited==0){return false;};document.questionario.apagaPublicacaoPUBMED.value=".$questionario->publicacoesPUBMED[$i]->id.";document.questionario.operacao.value=13;' >";
		    		$outSNPUBMED=$outSNPUBMED."</td>";
					$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$questionario->publicacoesPUBMED[$i]->id;
		    		$outSNPUBMED=$outSNPUBMED."</td>";
		    		$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$questionario->publicacoesPUBMED[$i]->ano;
		    		$outSNPUBMED=$outSNPUBMED."</td>";
		    		/*$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$questionario->publicacoesPUBMED[$i]->tipo;
		    		$outSNPUBMED=$outSNPUBMED."</td>";*/
		    		$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$questionario->publicacoesPUBMED[$i]->nomepublicacao;
		    		$outSNPUBMED=$outSNPUBMED."</td>";
		    		$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$questionario->publicacoesPUBMED[$i]->issn;
		    		$outSNPUBMED=$outSNPUBMED."</td>";
		    		/*$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$questionario->publicacoesPUBMED[$i]->issn_linking;
		    		$outSNPUBMED=$outSNPUBMED."</td>";
		    		$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$questionario->publicacoesPUBMED[$i]->issn_electronic;
		    		$outSNPUBMED=$outSNPUBMED."</td>";*/
		    		$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$questionario->publicacoesPUBMED[$i]->isbn;
		    		$outSNPUBMED=$outSNPUBMED."</td>";
		    		$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$questionario->publicacoesPUBMED[$i]->titulo;
		    		$outSNPUBMED=$outSNPUBMED."</td>";
		    		$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$questionario->publicacoesPUBMED[$i]->autores;
		    		$outSNPUBMED=$outSNPUBMED."</td>";
					$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$questionario->publicacoesPUBMED[$i]->volume;
		    		$outSNPUBMED=$outSNPUBMED."</td>";
					$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$questionario->publicacoesPUBMED[$i]->issue;
		    		$outSNPUBMED=$outSNPUBMED."</td>";
					$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$questionario->publicacoesPUBMED[$i]->ar;
		    		$outSNPUBMED=$outSNPUBMED."</td>";
					$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$questionario->publicacoesPUBMED[$i]->colecao;
		    		$outSNPUBMED=$outSNPUBMED."</td>";
					$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$questionario->publicacoesPUBMED[$i]->primpagina;
		    		$outSNPUBMED=$outSNPUBMED."</td>";			
					$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$questionario->publicacoesPUBMED[$i]->ultpagina;
		    		$outSNPUBMED=$outSNPUBMED."</td>";
		    		$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$questionario->publicacoesPUBMED[$i]->ifactor;
		    		$outSNPUBMED=$outSNPUBMED."</td>";
		    		$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$questionario->publicacoesPUBMED[$i]->citacoes;
		    		$outSNPUBMED=$outSNPUBMED."</td>";

		    		$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$questionario->publicacoesPUBMED[$i]->conftitle;
		    		$outSNPUBMED=$outSNPUBMED."</td>";
		    		$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$questionario->publicacoesPUBMED[$i]->language;
		    		$outSNPUBMED=$outSNPUBMED."</td>";
		    		$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$questionario->publicacoesPUBMED[$i]->editor;
		    		$outSNPUBMED=$outSNPUBMED."</td>";
		    		$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$questionario->publicacoesPUBMED[$i]->tipofmup;
		    		$outSNPUBMED=$outSNPUBMED."</td>";
		    		$outSNPUBMED=$outSNPUBMED."<td>";
		    		$outSNPUBMED=$outSNPUBMED.getTipoEstado($i);
		    		$outSNPUBMED=$outSNPUBMED."</td>";
		    		$outSNPUBMED=$outSNPUBMED."</tr>";
				}
				break;
				case 'LN': { // Opção de entrada
		    		$outLNPUBMED=$outLNPUBMED."<tr>";
		    		$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED."<input type='image' src='../images/icon_delete_s.png' name='navOption' value='Apagar Publicacao PUBMED' onclick='if(formSubmited==0){return false;};document.questionario.apagaPublicacaoPUBMED.value=".$questionario->publicacoesPUBMED[$i]->id.";document.questionario.operacao.value=13;' >";
		    		$outLNPUBMED=$outLNPUBMED."</td>";
					$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$questionario->publicacoesPUBMED[$i]->id;
		    		$outLNPUBMED=$outLNPUBMED."</td>";
		    		$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$questionario->publicacoesPUBMED[$i]->ano;
		    		$outLNPUBMED=$outLNPUBMED."</td>";
		    		/*$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$questionario->publicacoesPUBMED[$i]->tipo;
		    		$outLNPUBMED=$outLNPUBMED."</td>";*/
		    		$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$questionario->publicacoesPUBMED[$i]->nomepublicacao;
		    		$outLNPUBMED=$outLNPUBMED."</td>";
		    		$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$questionario->publicacoesPUBMED[$i]->issn;
		    		$outLNPUBMED=$outLNPUBMED."</td>";
		    		/*$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$questionario->publicacoesPUBMED[$i]->issn_linking;
		    		$outLNPUBMED=$outLNPUBMED."</td>";
		    		$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$questionario->publicacoesPUBMED[$i]->issn_electronic;
		    		$outLNPUBMED=$outLNPUBMED."</td>";*/
		    		$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$questionario->publicacoesPUBMED[$i]->isbn;
		    		$outLNPUBMED=$outLNPUBMED."</td>";
		    		$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$questionario->publicacoesPUBMED[$i]->titulo;
		    		$outLNPUBMED=$outLNPUBMED."</td>";
		    		$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$questionario->publicacoesPUBMED[$i]->autores;
		    		$outLNPUBMED=$outLNPUBMED."</td>";
					$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$questionario->publicacoesPUBMED[$i]->volume;
		    		$outLNPUBMED=$outLNPUBMED."</td>";
					$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$questionario->publicacoesPUBMED[$i]->issue;
		    		$outLNPUBMED=$outLNPUBMED."</td>";
					$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$questionario->publicacoesPUBMED[$i]->ar;
		    		$outLNPUBMED=$outLNPUBMED."</td>";
					$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$questionario->publicacoesPUBMED[$i]->colecao;
		    		$outLNPUBMED=$outLNPUBMED."</td>";
					$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$questionario->publicacoesPUBMED[$i]->primpagina;
		    		$outLNPUBMED=$outLNPUBMED."</td>";			
					$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$questionario->publicacoesPUBMED[$i]->ultpagina;
		    		$outLNPUBMED=$outLNPUBMED."</td>";
		    		$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$questionario->publicacoesPUBMED[$i]->citacoes;
		    		$outLNPUBMED=$outLNPUBMED."</td>";
		    		$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$questionario->publicacoesPUBMED[$i]->ifactor;
		    		$outLNPUBMED=$outLNPUBMED."</td>";
		    		$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$questionario->publicacoesPUBMED[$i]->conftitle;
		    		$outLNPUBMED=$outLNPUBMED."</td>";
		    		$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$questionario->publicacoesPUBMED[$i]->language;
		    		$outLNPUBMED=$outLNPUBMED."</td>";
		    		$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$questionario->publicacoesPUBMED[$i]->editor;
		    		$outLNPUBMED=$outLNPUBMED."</td>";
		    		$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.getTipoEstado($i);
		    		$outLNPUBMED=$outLNPUBMED."</td>";		    				    		
		    		$outLNPUBMED=$outLNPUBMED."</tr>";
				}
				break;
				case 'CLN': { // Opção de entrada
		    		$outCLNPUBMED=$outCLNPUBMED."<tr>";
		    		$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED."<input type='image' src='../images/icon_delete_s.png' name='navOption' value='Apagar Publicacao PUBMED' onclick='if(formSubmited==0){return false;};document.questionario.apagaPublicacaoPUBMED.value=".$questionario->publicacoesPUBMED[$i]->id.";document.questionario.operacao.value=13;' >";
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";
					$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$questionario->publicacoesPUBMED[$i]->id;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";
		    		$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$questionario->publicacoesPUBMED[$i]->ano;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";
		    		/*$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$questionario->publicacoesPUBMED[$i]->tipo;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";*/
		    		$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$questionario->publicacoesPUBMED[$i]->nomepublicacao;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";
		    		$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$questionario->publicacoesPUBMED[$i]->issn;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";
		    		/*$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$questionario->publicacoesPUBMED[$i]->issn_linking;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";
		    		$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$questionario->publicacoesPUBMED[$i]->issn_electronic;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";*/
		    		$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$questionario->publicacoesPUBMED[$i]->isbn;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";
		    		$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$questionario->publicacoesPUBMED[$i]->titulo;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";
		    		$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$questionario->publicacoesPUBMED[$i]->autores;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";
					$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$questionario->publicacoesPUBMED[$i]->volume;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";
					$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$questionario->publicacoesPUBMED[$i]->issue;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";
					$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$questionario->publicacoesPUBMED[$i]->ar;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";
					$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$questionario->publicacoesPUBMED[$i]->colecao;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";
					$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$questionario->publicacoesPUBMED[$i]->primpagina;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";			
					$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$questionario->publicacoesPUBMED[$i]->ultpagina;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";
		    		$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$questionario->publicacoesPUBMED[$i]->citacoes;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";
		    		$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$questionario->publicacoesPUBMED[$i]->ifactor;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";
		    		$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$questionario->publicacoesPUBMED[$i]->conftitle;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";
		    		$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$questionario->publicacoesPUBMED[$i]->language;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";
		    		$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$questionario->publicacoesPUBMED[$i]->editor;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";
		    		$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.getTipoEstado($i);
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";		    				    		
		    		$outCLNPUBMED=$outCLNPUBMED."</tr>";
				}
				break;
				case 'L': { // Opção de entrada
		    		$outLPUBMED=$outLPUBMED."<tr>";
		    		$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED."<input type='image' src='../images/icon_delete_s.png' name='navOption' value='Apagar Publicacao PUBMED' onclick='if(formSubmited==0){return false;};document.questionario.apagaPublicacaoPUBMED.value=".$questionario->publicacoesPUBMED[$i]->id.";document.questionario.operacao.value=13;' >";
		    		$outLPUBMED=$outLPUBMED."</td>";
					$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$questionario->publicacoesPUBMED[$i]->id;
		    		$outLPUBMED=$outLPUBMED."</td>";
		    		$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$questionario->publicacoesPUBMED[$i]->ano;
		    		$outLPUBMED=$outLPUBMED."</td>";
		    		/*$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$questionario->publicacoesPUBMED[$i]->tipo;
		    		$outLPUBMED=$outLPUBMED."</td>";*/
		    		$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$questionario->publicacoesPUBMED[$i]->nomepublicacao;
		    		$outLPUBMED=$outLPUBMED."</td>";
		    		$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$questionario->publicacoesPUBMED[$i]->issn;
		    		$outLPUBMED=$outLPUBMED."</td>";
		    		/*$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$questionario->publicacoesPUBMED[$i]->issn_linking;
		    		$outLPUBMED=$outLPUBMED."</td>";
		    		$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$questionario->publicacoesPUBMED[$i]->issn_electronic;
		    		$outLPUBMED=$outLPUBMED."</td>";*/
		    		$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$questionario->publicacoesPUBMED[$i]->isbn;
		    		$outLPUBMED=$outLPUBMED."</td>";
		    		$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$questionario->publicacoesPUBMED[$i]->titulo;
		    		$outLPUBMED=$outLPUBMED."</td>";
		    		$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$questionario->publicacoesPUBMED[$i]->autores;
		    		$outLPUBMED=$outLPUBMED."</td>";
					$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$questionario->publicacoesPUBMED[$i]->volume;
		    		$outLPUBMED=$outLPUBMED."</td>";
					$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$questionario->publicacoesPUBMED[$i]->issue;
		    		$outLPUBMED=$outLPUBMED."</td>";
					$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$questionario->publicacoesPUBMED[$i]->ar;
		    		$outLPUBMED=$outLPUBMED."</td>";
					$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$questionario->publicacoesPUBMED[$i]->colecao;
		    		$outLPUBMED=$outLPUBMED."</td>";
					$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$questionario->publicacoesPUBMED[$i]->primpagina;
		    		$outLPUBMED=$outLPUBMED."</td>";			
					$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$questionario->publicacoesPUBMED[$i]->ultpagina;
		    		$outLPUBMED=$outLPUBMED."</td>";
		    		$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$questionario->publicacoesPUBMED[$i]->citacoes;
		    		$outLPUBMED=$outLPUBMED."</td>";
		    		$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$questionario->publicacoesPUBMED[$i]->ifactor;
		    		$outLPUBMED=$outLPUBMED."</td>";
		    		$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$questionario->publicacoesPUBMED[$i]->conftitle;
		    		$outLPUBMED=$outLPUBMED."</td>";
		    		$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$questionario->publicacoesPUBMED[$i]->language;
		    		$outLPUBMED=$outLPUBMED."</td>";
		    		$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$questionario->publicacoesPUBMED[$i]->editor;
		    		$outLPUBMED=$outLPUBMED."</td>";
		    		$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.getTipoEstado($i);
		    		$outLPUBMED=$outLPUBMED."</td>";		    				    		
		    		$outLPUBMED=$outLPUBMED."</tr>";
				}
				break;
				case 'CL': { // Opção de entrada
		    		$outCLPUBMED=$outCLPUBMED."<tr>";
		    		$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED."<input type='image' src='../images/icon_delete_s.png' name='navOption' value='Apagar Publicacao PUBMED' onclick='if(formSubmited==0){return false;};document.questionario.apagaPublicacaoPUBMED.value=".$questionario->publicacoesPUBMED[$i]->id.";document.questionario.operacao.value=13;' >";
		    		$outCLPUBMED=$outCLPUBMED."</td>";
					$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$questionario->publicacoesPUBMED[$i]->id;
		    		$outCLPUBMED=$outCLPUBMED."</td>";
		    		$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$questionario->publicacoesPUBMED[$i]->ano;
		    		$outCLPUBMED=$outCLPUBMED."</td>";
		    		/*$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$questionario->publicacoesPUBMED[$i]->tipo;
		    		$outCLPUBMED=$outCLPUBMED."</td>";*/
		    		$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$questionario->publicacoesPUBMED[$i]->nomepublicacao;
		    		$outCLPUBMED=$outCLPUBMED."</td>";
		    		$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$questionario->publicacoesPUBMED[$i]->issn;
		    		$outCLPUBMED=$outCLPUBMED."</td>";
		    		/*$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$questionario->publicacoesPUBMED[$i]->issn_linking;
		    		$outCLPUBMED=$outCLPUBMED."</td>";
		    		$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$questionario->publicacoesPUBMED[$i]->issn_electronic;
		    		$outCLPUBMED=$outCLPUBMED."</td>";*/
		    		$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$questionario->publicacoesPUBMED[$i]->isbn;
		    		$outCLPUBMED=$outCLPUBMED."</td>";
		    		$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$questionario->publicacoesPUBMED[$i]->titulo;
		    		$outCLPUBMED=$outCLPUBMED."</td>";
		    		$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$questionario->publicacoesPUBMED[$i]->autores;
		    		$outCLPUBMED=$outCLPUBMED."</td>";
					$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$questionario->publicacoesPUBMED[$i]->volume;
		    		$outCLPUBMED=$outCLPUBMED."</td>";
					$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$questionario->publicacoesPUBMED[$i]->issue;
		    		$outCLPUBMED=$outCLPUBMED."</td>";
					$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$questionario->publicacoesPUBMED[$i]->ar;
		    		$outCLPUBMED=$outCLPUBMED."</td>";
					$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$questionario->publicacoesPUBMED[$i]->colecao;
		    		$outCLPUBMED=$outCLPUBMED."</td>";
					$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$questionario->publicacoesPUBMED[$i]->primpagina;
		    		$outCLPUBMED=$outCLPUBMED."</td>";			
					$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$questionario->publicacoesPUBMED[$i]->ultpagina;
		    		$outCLPUBMED=$outCLPUBMED."</td>";
		    		$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$questionario->publicacoesPUBMED[$i]->citacoes;
		    		$outCLPUBMED=$outCLPUBMED."</td>";
		    		$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$questionario->publicacoesPUBMED[$i]->ifactor;
		    		$outCLPUBMED=$outCLPUBMED."</td>";
		    		$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$questionario->publicacoesPUBMED[$i]->conftitle;
		    		$outCLPUBMED=$outCLPUBMED."</td>";
		    		$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$questionario->publicacoesPUBMED[$i]->language;
		    		$outCLPUBMED=$outCLPUBMED."</td>";
		    		$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$questionario->publicacoesPUBMED[$i]->editor;
		    		$outCLPUBMED=$outCLPUBMED."</td>";
		    		$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.getTipoEstado($i);
		    		$outCLPUBMED=$outCLPUBMED."</td>";		    				    		
		    		$outCLPUBMED=$outCLPUBMED."</tr>";
				}
				break;
				case 'P': { // Opção de entrada
		    		$outPPUBMED=$outPPUBMED."<tr>";
		    		$outPPUBMED=$outPPUBMED."<td>";
					$outPPUBMED=$outPPUBMED."<input type='image' src='../images/icon_delete_s.png' name='navOption' value='Apagar Publicacao PUBMED' onclick='if(formSubmited==0){return false;};document.questionario.apagaPublicacaoPUBMED.value=".$questionario->publicacoesPUBMED[$i]->id.";document.questionario.operacao.value=13;' >";
		    		$outPPUBMED=$outPPUBMED."</td>";
		    		
		    		$outPPUBMED=$outPPUBMED."<td>";
					$outPPUBMED=$outPPUBMED.$questionario->publicacoesPUBMED[$i]->npatente;
		    		$outPPUBMED=$outPPUBMED."</td>";
		    		$outPPUBMED=$outPPUBMED."<td>";
					$outPPUBMED=$outPPUBMED.$questionario->publicacoesPUBMED[$i]->ipc;
		    		$outPPUBMED=$outPPUBMED."</td>";		    		
		    		$outPPUBMED=$outPPUBMED."<td>";
					$outPPUBMED=$outPPUBMED.$questionario->publicacoesPUBMED[$i]->titulo;
		    		$outPPUBMED=$outPPUBMED."</td>";		    		
		    		$outPPUBMED=$outPPUBMED."<td>";
					$outPPUBMED=$outPPUBMED.$questionario->publicacoesPUBMED[$i]->autores;
		    		$outPPUBMED=$outPPUBMED."</td>";
		    		$outPPUBMED=$outPPUBMED."<td>";
					$outPPUBMED=$outPPUBMED.$questionario->publicacoesPUBMED[$i]->datapatente;
		    		$outPPUBMED=$outPPUBMED."</td>";		    		
		    		
					/*$outPPUBMED=$outPPUBMED."<td>";
					$outPPUBMED=$outPPUBMED.$questionario->publicacoesPUBMED[$i]->id;
		    		$outPPUBMED=$outPPUBMED."</td>";
		    		$outPPUBMED=$outPPUBMED."<td>";
					$outPPUBMED=$outPPUBMED.$questionario->publicacoesPUBMED[$i]->ano;
		    		$outPPUBMED=$outPPUBMED."</td>";
		    		$outPPUBMED=$outPPUBMED."<td>";
					$outPPUBMED=$outPPUBMED.$questionario->publicacoesPUBMED[$i]->tipo;
		    		$outPPUBMED=$outPPUBMED."</td>";
		    		$outPPUBMED=$outPPUBMED."<td>";
					$outPPUBMED=$outPPUBMED.$questionario->publicacoesPUBMED[$i]->nomepublicacao;
		    		$outPPUBMED=$outPPUBMED."</td>";
		    		$outPPUBMED=$outPPUBMED."<td>";
					$outPPUBMED=$outPPUBMED.$questionario->publicacoesPUBMED[$i]->issn;
		    		$outPPUBMED=$outPPUBMED."</td>";
		    		$outPPUBMED=$outPPUBMED."<td>";
					$outPPUBMED=$outPPUBMED.$questionario->publicacoesPUBMED[$i]->issn_linking;
		    		$outPPUBMED=$outPPUBMED."</td>";
		    		$outPPUBMED=$outPPUBMED."<td>";
					$outPPUBMED=$outPPUBMED.$questionario->publicacoesPUBMED[$i]->issn_electronic;
		    		$outPPUBMED=$outPPUBMED."</td>"
		    		$outPPUBMED=$outPPUBMED."<td>";
					$outPPUBMED=$outPPUBMED.$questionario->publicacoesPUBMED[$i]->isbn;
		    		$outPPUBMED=$outPPUBMED."</td>";


					$outPPUBMED=$outPPUBMED."<td>";
					$outPPUBMED=$outPPUBMED.$questionario->publicacoesPUBMED[$i]->volume;
		    		$outPPUBMED=$outPPUBMED."</td>";
					$outPPUBMED=$outPPUBMED."<td>";
					$outPPUBMED=$outPPUBMED.$questionario->publicacoesPUBMED[$i]->issue;
		    		$outPPUBMED=$outPPUBMED."</td>";
					$outPPUBMED=$outPPUBMED."<td>";
					$outPPUBMED=$outPPUBMED.$questionario->publicacoesPUBMED[$i]->ar;
		    		$outPPUBMED=$outPPUBMED."</td>";
					$outPPUBMED=$outPPUBMED."<td>";
					$outPPUBMED=$outPPUBMED.$questionario->publicacoesPUBMED[$i]->colecao;
		    		$outPPUBMED=$outPPUBMED."</td>";
					$outPPUBMED=$outPPUBMED."<td>";
					$outPPUBMED=$outPPUBMED.$questionario->publicacoesPUBMED[$i]->primpagina;
		    		$outPPUBMED=$outPPUBMED."</td>";			
					$outPPUBMED=$outPPUBMED."<td>";
					$outPPUBMED=$outPPUBMED.$questionario->publicacoesPUBMED[$i]->ultpagina;
		    		$outPPUBMED=$outPPUBMED."</td>";
		    		$outPPUBMED=$outPPUBMED."<td>";
					$outPPUBMED=$outPPUBMED.$questionario->publicacoesPUBMED[$i]->citacoes;
		    		$outPPUBMED=$outPPUBMED."</td>";
		    		$outPPUBMED=$outPPUBMED."<td>";
					$outPPUBMED=$outPPUBMED.$questionario->publicacoesPUBMED[$i]->ifactor;
		    		$outPPUBMED=$outPPUBMED."</td>";
		    		$outPPUBMED=$outPPUBMED."<td>";
					$outPPUBMED=$outPPUBMED.$questionario->publicacoesPUBMED[$i]->conftitle;
		    		$outPPUBMED=$outPPUBMED."</td>";
		    		$outPPUBMED=$outPPUBMED."<td>";
					$outPPUBMED=$outPPUBMED.$questionario->publicacoesPUBMED[$i]->language;
		    		$outPPUBMED=$outPPUBMED."</td>";
		    		$outPPUBMED=$outPPUBMED."<td>";
					$outPPUBMED=$outPPUBMED.$questionario->publicacoesPUBMED[$i]->editor;
		    		$outPPUBMED=$outPPUBMED."</td>";
		    		*/$outPPUBMED=$outPPUBMED."<td>";
					$outPPUBMED=$outPPUBMED.getTipoEstado($i);
		    		$outPPUBMED=$outPPUBMED."</td>";		    				    		
		    		$outPPUBMED=$outPPUBMED."</tr>";
				}
				break;
				case 'A': { // Opção de entrada
		    		$outAPUBMED=$outAPUBMED."<tr>";
		    		$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED."<input type='image' src='../images/icon_delete_s.png' name='navOption' value='Apagar Publicacao PUBMED' onclick='if(formSubmited==0){return false;};document.questionario.apagaPublicacaoPUBMED.value=".$questionario->publicacoesPUBMED[$i]->id.";document.questionario.operacao.value=13;' >";
		    		$outAPUBMED=$outAPUBMED."</td>";
					$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$questionario->publicacoesPUBMED[$i]->id;
		    		$outAPUBMED=$outAPUBMED."</td>";
		    		$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$questionario->publicacoesPUBMED[$i]->ano;
		    		$outAPUBMED=$outAPUBMED."</td>";
		    		/*$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$questionario->publicacoesPUBMED[$i]->tipo;
		    		$outAPUBMED=$outAPUBMED."</td>";*/
		    		$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$questionario->publicacoesPUBMED[$i]->nomepublicacao;
		    		$outAPUBMED=$outAPUBMED."</td>";
		    		$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$questionario->publicacoesPUBMED[$i]->issn;
		    		$outAPUBMED=$outAPUBMED."</td>";
		    		/*$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$questionario->publicacoesPUBMED[$i]->issn_linking;
		    		$outAPUBMED=$outAPUBMED."</td>";
		    		$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$questionario->publicacoesPUBMED[$i]->issn_electronic;
		    		$outAPUBMED=$outAPUBMED."</td>";*/
		    		$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$questionario->publicacoesPUBMED[$i]->isbn;
		    		$outAPUBMED=$outAPUBMED."</td>";
		    		$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$questionario->publicacoesPUBMED[$i]->titulo;
		    		$outAPUBMED=$outAPUBMED."</td>";
		    		$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$questionario->publicacoesPUBMED[$i]->autores;
		    		$outAPUBMED=$outAPUBMED."</td>";
					$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$questionario->publicacoesPUBMED[$i]->volume;
		    		$outAPUBMED=$outAPUBMED."</td>";
					$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$questionario->publicacoesPUBMED[$i]->issue;
		    		$outAPUBMED=$outAPUBMED."</td>";
					$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$questionario->publicacoesPUBMED[$i]->ar;
		    		$outAPUBMED=$outAPUBMED."</td>";
					$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$questionario->publicacoesPUBMED[$i]->colecao;
		    		$outAPUBMED=$outAPUBMED."</td>";
					$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$questionario->publicacoesPUBMED[$i]->primpagina;
		    		$outAPUBMED=$outAPUBMED."</td>";			
					$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$questionario->publicacoesPUBMED[$i]->ultpagina;
		    		$outAPUBMED=$outAPUBMED."</td>";
		    		$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$questionario->publicacoesPUBMED[$i]->citacoes;
		    		$outAPUBMED=$outAPUBMED."</td>";
		    		$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$questionario->publicacoesPUBMED[$i]->ifactor;
		    		$outAPUBMED=$outAPUBMED."</td>";
		    		$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$questionario->publicacoesPUBMED[$i]->conftitle;
		    		$outAPUBMED=$outAPUBMED."</td>";
		    		$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$questionario->publicacoesPUBMED[$i]->language;
		    		$outAPUBMED=$outAPUBMED."</td>";
		    		$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$questionario->publicacoesPUBMED[$i]->editor;
		    		$outAPUBMED=$outAPUBMED."</td>";
		    		$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.getTipoEstado($i);
		    		$outAPUBMED=$outAPUBMED."</td>";		    				    		
		    		$outAPUBMED=$outAPUBMED."</tr>";
				}
				break;
				default:{ //echo "DEFAULT";
			        $outRestoPUBMED=$outRestoPUBMED."<tr>";
		    		$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED."<input type='image' src='../images/icon_delete_s.png' name='navOption' value='Apagar Publicacao PUBMED' onclick='if(formSubmited==0){return false;};document.questionario.apagaPublicacaoPUBMED.value=".$questionario->publicacoesPUBMED[$i]->id.";document.questionario.operacao.value=13;' >";
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";
					$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$questionario->publicacoesPUBMED[$i]->id;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";
		    		$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$questionario->publicacoesPUBMED[$i]->ano;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";
		    		$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$questionario->publicacoesPUBMED[$i]->tipo;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";
		    		$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$questionario->publicacoesPUBMED[$i]->nomepublicacao;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";
		    		$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$questionario->publicacoesPUBMED[$i]->issn;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";
		    		$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$questionario->publicacoesPUBMED[$i]->titulo;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";
		    		$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$questionario->publicacoesPUBMED[$i]->autores;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";
					$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$questionario->publicacoesPUBMED[$i]->volume;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";
					$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$questionario->publicacoesPUBMED[$i]->issue;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";
					$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$questionario->publicacoesPUBMED[$i]->ar;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";
					$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$questionario->publicacoesPUBMED[$i]->primpagina;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";			
					$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$questionario->publicacoesPUBMED[$i]->ultpagina;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";
		    		$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$questionario->publicacoesPUBMED[$i]->citacoes;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";
		    		$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$questionario->publicacoesPUBMED[$i]->ifactor;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";
		    		$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$questionario->publicacoesPUBMED[$i]->conftitle;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";
		    		$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$questionario->publicacoesPUBMED[$i]->language;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";
		    		$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$questionario->publicacoesPUBMED[$i]->editor;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";
		    		$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.getTipoEstado($i);
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";
		    		$outRestoPUBMED=$outRestoPUBMED."</tr>";
			    }
				break;
			}
	}

	
	
	if($outJPUBMED!=""){echo"<table  class='box-table-b'><caption>ARTIGOS</caption>";echo $tcolsJPUBMED;echo $outJPUBMED;echo "</table><br />";}
	if($outPRPPUBMED!=""){echo"<table  class='box-table-b'><caption>PEER REVIEW PROCEEDINGS</caption>";echo $tcolsJPUBMED;echo $outPRPPUBMED;echo "</table><br />";}
	if($outJAPUBMED!=""){echo"<table  class='box-table-b'><caption>JOURNAL ABSTRACTS</caption>";echo $tcolsJPUBMED;echo $outJAPUBMED;echo "</table><br />";}

	if($outCPPUBMED!=""){echo"<table  class='box-table-b'><caption>CONFERENCE PROCEEDINGS</caption>";echo $tcolsCONFPUBMED;echo $outCPPUBMED;echo "</table><br />";}
	if($outOAPUBMED!=""){echo"<table  class='box-table-b'><caption>OTHER ABSTRACTS</caption>";echo $tcolsCONFPUBMED;echo $outOAPUBMED;echo "</table><br />";}
	if($outLPUBMED!=""){echo"<table  class='box-table-b'><caption>LIVROS</caption>";echo $tcols;echo $outLPUBMED;echo "</table><br />";}
	if($outCLPUBMED!=""){echo"<table  class='box-table-b'><caption>CAPITULOS DE LIVROS</caption>";echo $tcols;echo $outCLPUBMED;echo "</table><br />";}
	if($outSPUBMED!=""){echo"<table  class='box-table-b'><caption>SUMÁRIOS</caption>";echo $tcols;echo $outSPUBMED;echo "</table><br />";}

	
	// NACIONAIS
	
	//if($outJNPUBMED!=""){echo"<table  id='box-table-a'><caption>ARTIGOS - NACIONAL</caption>";echo $tcolsJPUBMED;echo $outJNPUBMED;echo "</table><br />";}
	if($outJNPUBMED!=""){echo"<table  class='box-table-b'><caption>ARTIGOS - NÃO INGLÊS</caption>";echo $tcolsJPUBMED;echo $outJNPUBMED;echo "</table><br />";}
	//if($outPRPNPUBMED!=""){echo"<table  id='box-table-a'><caption>PEER REVIEW PROCEEDINGS - NACIONAL</caption>";echo $tcolsJPUBMED;echo $outPRPNPUBMED;echo "</table><br />";}
	if($outPRPNPUBMED!=""){echo"<table  class='box-table-b'><caption>PEER REVIEW PROCEEDINGS - NÃO INGLÊS</caption>";echo $tcolsJPUBMED;echo $outPRPNPUBMED;echo "</table><br />";}
	// FALTA JAN
	
	//if($outCPNPUBMED!=""){echo"<table  id='box-table-a'><caption>CONFERENCE PROCEEDINGS NACIONAIS</caption>";echo $tcolsCONFPUBMED;echo $outCPNPUBMED;echo "</table><br />";}
	if($outCPNPUBMED!=""){echo"<table  class='box-table-b'><caption>CONFERENCE PROCEEDINGS NÃO INGLÊS</caption>";echo $tcolsCONFPUBMED;echo $outCPNPUBMED;echo "</table><br />";}
	//if($outLNPUBMED!=""){echo"<table  id='box-table-a'><caption>LIVROS - NACIONAL</caption>";echo $tcols;echo $outLPUBMED;echo "</table><br />";}
	if($outLNPUBMED!=""){echo"<table  class='box-table-b'><caption>LIVROS - NÃO INGLÊS</caption>";echo $tcols;echo $outLPUBMED;echo "</table><br />";}
	//if($outCLNPUBMED!=""){echo"<table  id='box-table-a'><caption>CAPITULOS DE LIVROS - NACIONAL</caption>";echo $tcols;echo $outCLNPUBMED;echo "</table><br />";}
	if($outCLNPUBMED!=""){echo"<table  class='box-table-b'><caption>CAPITULOS DE LIVROS - NÃO INGLÊS</caption>";echo $tcols;echo $outCLNPUBMED;echo "</table><br />";}
	//if($outSNPUBMED!=""){echo"<table  id='box-table-a'><caption>SUMÁRIO - NACIONAL</caption>";echo $tcols;echo $outSNPUBMED;echo "</table><br />";}
	if($outSNPUBMED!=""){echo"<table  class='box-table-b'><caption>SUMÁRIO - NÃO INGLÊS</caption>";echo $tcols;echo $outSNPUBMED;echo "</table><br />";}

	
	if($outPPUBMED!=""){echo"<table  class='box-table-b'><caption>PATENTES</caption>";echo $tcols;echo $outPPUBMED;echo "</table><br />";}
	if($outAPUBMED!=""){echo"<table  class='box-table-b'><caption>AGRADECIMENTOS</caption>";echo $tcols;echo $outAPUBMED;echo "</table><br />";}	
	

	echo "<input type='hidden' name='apagaPublicacaoPUBMED'/>";
	
	echo "</fieldset>";
	echo "</div>";
	
	
	function getTipoEstado($i) {
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_estadoPublicacoes");
		
		while ($row = mysql_fetch_assoc($lValues)) {
			if(checkTipoEstado($row["ID"],$i))
				$resp = $row["DESCRICAO"];
		}
	
		$db->disconnect();
		return $resp;
	}
	
	function checkTipoEstado($id,$i){
		global $questionario;
		if($questionario->publicacoesPUBMED[$i]->estado==$id)
			return true;
		else
			return false;
	}
?>