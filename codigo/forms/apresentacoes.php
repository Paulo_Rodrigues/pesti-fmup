<?php

echo "<div id='content18' style='display: inline;'>";
echo "<p><fieldset class='normal'>\n";
echo "<legend>Apresentações em Conferências <a href='../helpfiles/Confs.JPG' style='color: #FFFF00' target='_blank'>Ajuda</a> /<i>Conferences <a href='../helpfiles/Confs.JPG' style='color: #FFFF00' target='_blank'>Help</a></i></legend>\n";

// echo "<p class='ppthelp'>Preencha a tabela seguinte notando que se para alguma conferência fez parte do Comitê Organizador Local (LOC) e do Comitê Organizador Científico (SOC) deve repetir tudo numa outra linha (à exceção dessa diferença). No final, colocar o link da página oficial da conferência,</p>";

// echo "<p class='penhelp'><i>Please fill the following table and note that if you were a member of both the Local Organizing Committee (LOC) and the Scientific Organizing Committee (SOC) for a given conference, you must repeat all information (except that one) in a new line. At the end, please provide the link of the official page of the conference.</i></p>";


echo "  
<table id='apresentacaoConferencia' class='box-table-b'>        
    <thead> 
	    <tr>
	    	<th></th>
    					<th><u>Âmbito<p><i>Type</i></p></u></th>
      					<th><u>Comunicação como<p><i>Contribution as</i></p></u></th>
      					<th><u>Data Início<p><i>Begin Date</i></p></u></th>
      					<th><u>Data Fim<p><i>End Date</i></p></u></th>
      					<th><u>Título da Conferência/Seminário<p><i>Conference/Seminar Title</i></p></u></th>
      					<th><u>Local (cidade,país)<p><i>Location (town,country)</i></p></u></th>
      					<th><u>Nº Participantes<p><i>Participant Nr.</i></p></u></th>
      					<th><u>Link<p><i>Link</i></p></u></th>
	    </tr>
    </thead>       
<tbody>    
";
 
foreach ($questionario->apresentacoesConferencias as $i => $value){
	echo "<tr>";
	echo "<td><input type='image' src='../images/icon_delete.png' name='navOption' value='Apagar Apresentacao Conferencia' onclick='if(formSubmited==0){return false;};document.questionario.apagaRegApresentacaoConferencia.value=".$questionario->apresentacoesConferencias[$i]->id.";document.questionario.operacao.value=84;' ></td>";
	echo "<td>";
	getAmbitoApresentacaoConf($i);
	echo "</td>";
	echo "<td>";
	getTipoApresentacaoConf($i);
	echo "</td>";
	echo "<td><input class='inp-textAuto' type='text' id='apresconfdatainicio_".$i."' name='apresconfdatainicio_".$i."' size='11' value='".$questionario->apresentacoesConferencias[$i]->datainicio."' onfocus='calendario(\"apresconfdatainicio_".$i."\");' onkeypress='validateCal(event);'></td>";
	echo "<td><input class='inp-textAuto' type='text' id='apresconfdatafim_".$i."' name='apresconfdatafim_".$i."' size='11' value='".$questionario->apresentacoesConferencias[$i]->datafim."' onfocus='calendario(\"apresconfdatafim_".$i."\");' onkeypress='validateCal(event);'></td>";
	echo "<td><input class='inp-textAuto' type='text' name='apresconftitulo_".$i."' value='".$questionario->apresentacoesConferencias[$i]->titulo."'></td>";
	echo "<td><input class='inp-textAuto' type='text' name='apresconflocal_".$i."' value='".$questionario->apresentacoesConferencias[$i]->local."'></td>";
	echo "<td><input class='inp-textAuto' type='text' size='5' name='apresconfparticipantes_".$i."' onkeypress='validate(event)' value='".$questionario->apresentacoesConferencias[$i]->participantes."'></p>\n</td>";
	echo "<td><textarea rows='2' cols='20' name='apresconflink_".$i."'>".$questionario->apresentacoesConferencias[$i]->link."</textarea></td>";
	echo "</tr>";
}
echo "</tbody><tfoot><tr><td><input input type='image' src='../images/icon_new.png' name='navOption' value='Nova Conferência' onclick='if(formSubmited==0){return false;};document.questionario.operacao.value=83;'></td></tr></tfoot>";

echo "</table>";
echo "<br />";


echo "<input type='hidden' name='apagaRegApresentacaoConferencia'/>";
echo "</fieldset>";


echo "<p><fieldset class='normal'>\n";
echo "<legend>Seminários/<i>Seminars</i></legend>\n";

echo "<p class='ppthelp'>“Seminário” é uma palestra isolada de curta duração (e.g. duas horas) apresentada fora do âmbito de qualquer congresso, por convite.</p>";
echo "<p class='penhelp'><i>“Seminar” is a short duration isolated talk (e.g. two hours) presented out of the context of any congress, by invitation.</i></p>";
			
echo "<table id='semin' class='box-table-a'>
    					<tr><th></th>
    					<th>Internacional</th>
      					<th>Nacional</th>
						</tr>";
			    
/*echo "<tr><td>Número de trabalhos apresentados em congressos/simpósios/workshops, por convite<p><i>Number of presented works in congresses/symposia/workshops, by invitation</i></p></td><td><input class='inp-textAuto' type='text' name='iconfconv' maxlength='3' size='3' onkeypress='validate(event)' value='".$questionario->apresentacoes->iconfconv."'></td>";
echo "<td><input class='inp-textAuto' type='text' name='nconfconv' maxlength='3' size='3' onkeypress='validate(event)' value='".$questionario->apresentacoes->nconfconv."'></td></tr>";

echo "<tr><td>Número de trabalhos apresentados em congressos/simpósios/workshops, como participante<p><i>Number of presented works in congresses/symposia/workshops, as participant</i></p></td><td><input class='inp-textAuto' type='text' name='iconfpart' maxlength='3' size='3' onkeypress='validate(event)' value='".$questionario->apresentacoes->iconfpart."'></td>";
echo "<td><input class='inp-textAuto' type='text' name='nconfpart' maxlength='3' size='3' onkeypress='validate(event)' value='".$questionario->apresentacoes->nconfpart."'></td></tr>";
*/

echo "<tr><td>Número de seminários apresentados<p><i>Number of seminars presented</i></p></td><td><input class='inp-textAuto' type='text' name='isem'maxlength='3' size='3' onkeypress='validate(event)'  value='".$questionario->apresentacoes->isem."'></td>";
echo "<td><input class='inp-textAuto' type='text' name='nsem' maxlength='3' size='3' onkeypress='validate(event)' value='".$questionario->apresentacoes->nsem."'></td></tr>";



echo "</table>";

	echo "<input type='hidden' name='apagaRegOrientacao'/>";
	echo "</fieldset>";
	
	echo "</div>";
	
	
	
	function getTipoApresentacaoConf($i) {
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_tipoApresConf");
	
		echo "<SELECT name='apresconftipo_".$i."' >\n";
		echo "<option></option>\n";
	
		while ($row = mysql_fetch_assoc($lValues)) {
			echo "<option value='".$row["ID"]."'".checkTipoApresentacaoConf($row["ID"],$i).">".$row["DESCRICAO"]."</option>\n";
		}
		echo "</SELECT><br />\n";
		$db->disconnect();
	
	}
	
	function checkTipoApresentacaoConf($id,$i){
		global $questionario;
		if($questionario->apresentacoesConferencias[$i]->tipo==$id)
			return " SELECTED";
		else
			return "";
	}
	
	function getAmbitoApresentacaoConf($i) {
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_ambitoconf");
	
		echo "<SELECT name='apresconfambito_".$i."' >\n";
		echo "<option></option>\n";
	
		while ($row = mysql_fetch_assoc($lValues)) {
			echo "<option value='".$row["ID"]."'".checkAmbApresentacaoConf($row["ID"],$i).">".$row["DESCRICAO"]."</option>\n";
		}
		echo "</SELECT><br />\n";
		$db->disconnect();
	
	}
	
	function checkAmbApresentacaoConf($id,$i){
		global $questionario;
		if($questionario->apresentacoesConferencias[$i]->ambito==$id)
			return " SELECTED";
		else
			return "";
	}
		
	?>