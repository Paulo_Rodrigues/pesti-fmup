<?php
echo "<div id='content21' style='display: inline;'>";
echo "<p><fieldset class='normal'>\n";
echo "<legend>Organizações e Sociedades Científicas/<i>Scientific Organizations and Societies</i></legend>\n";

echo "<p class='ppthelp'>Preencha a tabela seguinte apenas para as Organizações ou Sociedades Científicas de que fez parte da Direção. Comece a escrever o nome da Organização ou Sociedade Científica por extenso: o seu nome completo deve aparecer numa lista (selecione-o); caso contrário terá de o prencher manualmente. Na versão automática aparece também o tipo e deve escolher o cargo, mais uma vez iniciando a escrita da palavra (e escolhendo-o da lista que aparece depois). Indique um link para a página oficial da Organização ou Sociedade onde consta o seu nome como diretor.</p>";			

echo "<p class='penhelp'><i>Fill the following table only for the Scientific Organizations or Societies of which you were a member of the Executive Board. Please start by writing the Organization or Society’s name in full: its complete name should appear in a list (please pick it from there); otherwise you will have to fill it manually. On the automatic version the “type” also automatically appears and you must choose the Position, once again starting by keying (and picking up the correct position from the list that appears). Please provide the link to the official Organization or Society’s page where your name is listed as director.</i></p>";


echo "<table id='soccien' class='box-table-b'>
    					<tr><th></th>
    					<th>Nome<p><i>Name</i></p></th>
      					<th>Tipo<p><i>Type</i></p></th>
      					<th>Data Início<p><i>Begin Date</i></p></th>
      					<th>Data Fim<p><i>End Date</i></p></th>
      					<th>Cargo<p><i>Position</i></p></th>
      					<th>Link<p><i>Link</i></p></th>
						</tr>";
			    
    foreach ($questionario->sc as $i => $value){
			echo "<tr>";
		   	echo "<td><input type='image' src='../images/icon_delete.png' name='navOption' value='Apagar SC' onclick='if(formSubmited==0){return false;};document.questionario.apagaRegSC.value=".$i.";document.questionario.operacao.value=30;' ></td>";
			echo "<td><input class='inp-textAuto' type='text' id='scnome_".$i."' name='scnome_".$i."' value='".$questionario->sc[$i]->nome."' onkeyup='mostraSC(this.value,".$i.");'><div class='divlookup' id='livesearchsc_".$i."'></div></td>";
			echo "<td>";
			getTipoSC($i);
			echo "</td>";
			echo "<td><input class='inp-textAuto' type='text' id='scdatainicio_".$i."' size='11' name='scdatainicio_".$i."' value='".$questionario->sc[$i]->datainicio."' onfocus='calendario(\"scdatainicio_".$i."\");' onkeypress='validateCal(event);'></td>";	
			echo "<td><input class='inp-textAuto' type='text' id='scdatafim_".$i."' size='11' name='scdatafim_".$i."' value='".$questionario->sc[$i]->datafim."' onfocus='calendario(\"scdatafim_".$i."\");' onkeypress='validateCal(event);'></td>";	
			echo "<td><input class='inp-textAuto' type='text' id='sccargo_".$i."' name='sccargo_".$i."' value='".$questionario->sc[$i]->cargo."' onkeyup='mostraSCCargo(this.value,".$i.");'><div class='divlookup' id='livesearchsccargo_".$i."'></div></td>";
			echo "<td><textarea rows='2' cols='20' name='sclink_".$i."'>".$questionario->sc[$i]->link."</textarea></td>";
	    	echo "</tr>";	    	
    } 
    echo "<tr><td><input input type='image' src='../images/icon_new.png' name='navOption' value='Nova Sociedade' onclick='if(formSubmited==0){return false;};document.questionario.operacao.value=31;'></td></tr>";
    
    echo "</table>";
    echo "<br />";


	echo "<input type='hidden' name='apagaRegSC'/>";
	echo "</fieldset>";
	echo "</div>";
	function getTipoSC($i) {
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_tiposociedadecientifica");
	
		echo "<SELECT id='sctipo_".$i."' name='sctipo_".$i."' >\n";
		echo "<option></option>\n";
		
		while ($row = mysql_fetch_assoc($lValues)) {	
			echo "<option value='".$row["ID"]."'".checkTipoSC($row["ID"],$i).">".$row["DESCRICAO"]."</option>\n";
		}
		echo "</SELECT><br />\n";
		$db->disconnect();
				
	}	
				
	function checkTipoSC($id,$i){
		global $questionario;
		if($questionario->sc[$i]->tipo==$id)
			return " SELECTED";
		else 
			return "";
	}
		
	?>