<?php
echo "<div id='content26' style='display: inline;'>";
echo "<fieldset class='normal'>\n";
echo "<legend>Produtos e Serviços/<i>Products and Services</i></legend>\n";

echo "<p class='ppthelp'>Indique na Tabela seguinte, se aplicável, os produtos/serviços que vendeu em ".$anoactual." com vantagens económicas para a FMUP, preenchendo o seu Tipo e Valor:</p>";			

echo "<p class='penhelp'><i>Please indicate in the following table the products/services that you sold in ".$anoactual." with economical advantages for FMUP:</i></p>";
			
		
			
echo "<table id='prod' class='box-table-b'>
    					<tr><th></th>
    					<th>Tipo<p><i>Type</i></p></th>
      					<th>Valor<p><i>Amount</i></p></th>
						</tr>";
			    
    foreach ($questionario->produtos as $i => $value){
			echo "<tr>";
		   	echo "<td><input type='image' src='../images/icon_delete.png' name='navOption' value='Apagar Produtos' onclick='if(formSubmited==0){return false;};document.questionario.apagaRegProduto.value=".$questionario->produtos[$i]->id.";document.questionario.operacao.value=50;' ></td>";
			echo "<td><input class='inp-textAuto' type='text' name='tipop_".$i."' maxlength='500' size='40'  value='".$questionario->produtos[$i]->tipop."'></td>";
			echo "<td><input class='inp-textAuto' type='text' name='valorp_".$i."' onkeypress='validate(event)' value='".$questionario->produtos[$i]->valorp."'> €</td>";
	    	echo "</tr>";	    	
    } 
    
    	echo "<tr><td><input input type='image' src='../images/icon_new.png' name='navOption' value='Novo Produto' onclick='if(formSubmited==0){return false;};document.questionario.operacao.value=51;'></td></tr>";
    
    echo "</table>";
    echo "<br />";


	echo "<input type='hidden' name='apagaRegProduto'/>";
	echo "</fieldset>";
		echo "</div>";
	?>