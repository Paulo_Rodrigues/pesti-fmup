<?php 
$a = session_id();
if(empty($a)){
	echo "<br>Acesso Impedido<br>";
	echo "Usar Link: <a href='https://lgdf.med.up.pt/'> LGDF </a>";
	break("erro de sessão");
}

require_once '../../classlib/Database.class.inc';

$db= new Database();

$auditLogin=@$_POST['auditLogin'];
$auditId=@$_POST['auditId'];
$auditDataIni=@$_POST['auditDataIni'];
$auditDataFim=@$_POST['auditDataFim'];
$auditAcao=@$_POST['auditAcao'];
$auditSoErros=@$_POST['auditSoErros'];
$checkedSE="";
$checkedAU="";
if($auditSoErros==1){
	$checkedSE="CHECKED";
}
$auditAgrupaU=@$_POST['auditAgrupaU'];
if($auditAgrupaU==1){
	$checkedAU="CHECKED";
}

$filtrar=@$_POST['filtrar'];

echo "<input type='hidden' id='filtrar' name='filtrar' value='".$filtrar."'/>";

echo "<fieldset class='normal'>\n";
	echo "
	<table class='box-table-c'>
	 <caption>Auditoria</caption>
	<tr>
	<th>Nº de Utilizadores que já entraram este ano no sistema</th>
	</tr>
	<tr>
	<td>".$db->adminGetNUtilizadoresComActividade()."/".$db->adminGetNRegistadosTotal()."</td>
	</tr>";
	
	echo "
	<table  class='box-table-c'>
	<tr>
	<th>Login</th>
	<th>Id Investigador</th>
	<th>Data Inicial</th>
	<th>Data Final</th>
	<th>Ação</th>
	<th>Só erros</th>
	<th>Agrupar por Utilizador</th>
	<th rowspan='2'><input type='image' src='../../images/icon_spyglass.jpg' name='navOption' value='filtrarauditoria' onclick='document.adminform.operacao.value=7;document.adminform.filtrar.value=1;document.adminform.submit();'></th>
	</tr>
	<tr>
	<td><input class='inp-textAuto' type='text' id='auditLogin' name='auditLogin' value='".$auditLogin."'></td>
	<td><input class='inp-textAuto' type='text' id='auditId' name='auditId' value='".$auditId."'></td>
	<td><input class='inp-textAuto' type='text' id='auditDataIni' name='auditDataIni' value='".$auditDataIni."' onfocus='calendario(\"auditDataIni\");' onkeypress='validateCal(event);'></td>	
	<td><input class='inp-textAuto' type='text' id='auditDataFim' name='auditDataFim' value='".$auditDataFim."' onfocus='calendario(\"auditDataFim\");' onkeypress='validateCal(event);'></td>		
	<td><input class='inp-textAuto' type='text' name='auditAcao' name='auditAcao' value='".$auditAcao."'></td>
	<td><input class='inp-textAuto' type='checkbox' id='auditSoErros' name='auditSoErros' value='1'".$checkedSE."></td>
	<td><input class='inp-textAuto' type='checkbox' id='auditAgrupaU' name='auditAgrupaU' value='1'".$checkedAU."></td>
			
	</tr>
	</table>";
	
	
	if($filtrar==1){
		
		
		
		$mysqli=$db->connect_mysqli();
		
		$where=" where 1";
		
		if($auditLogin!="")
			$where=$where." and user='".$auditLogin."'";
		if($auditId!="")
			$where=$where." and idinv='".$auditId."'";
		if($auditDataIni!="")
			$where=$where." and STR_TO_DATE(data,'%Y-%m-%d')>=STR_TO_DATE('".$auditDataIni."','%d-%m-%Y')";
		if($auditDataFim!="")
			$where=$where." and STR_TO_DATE(data,'%Y-%m-%d')<=STR_TO_DATE('".$auditDataFim."','%d-%m-%Y')";
		if($auditAcao!="")
			$where=$where." and acao like '%".$auditAcao."%'";
		if($auditSoErros==1)
			$where=$where." and erros!=''";
		
		$agrupa="";
		
		if($auditAgrupaU==1){
			$agrupa=" user ASC, ";	
		}
		
		$query="select * from auditoria".$where." order by ".$agrupa."ID DESC";
		
		//echo $query;
				
		echo "
		<table class='box-table-c'>
		<!-- Results table headers -->
		<tr>
		<th>Sessão</th>
		<th>Login</th>
		<th>Id Investigador</th>
		<th>Data do Registo</th>
		<th>Ação</th>
		<th>Erro</th>
		<th>Query</th>
		</tr>";
		
		if($result=$mysqli->query($query)){
			while($row = $result->fetch_assoc())
			{
			
				echo "<tr><td>";
				print_r($row['SESSAO']);
				echo "</td><td>";
				print_r($row['USER']);
				echo "</td><td>";
				print_r($row['IDINV']);
				echo "</td><td>";
				print_r($row['DATA']);
				echo "</td><td>";
				print_r($row['ACAO']);
				echo "</td><td>";
				print_r($row['ERROS']);
				echo "</td><td>";
				print_r($row['QUERY']);
			
				echo "</td></tr>";
			}
		}
		
		echo "</table>";
		
		$result->free();
		
		$mysqli->close();

	}
	
	echo "</fieldset>";



?>