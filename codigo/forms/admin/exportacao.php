<?php 
	session_start();
	//ini_set('session.gc_maxlifetime', 1*60);
	$a = session_id();
	if(empty($a)){
		header("Location: ../../index.php?erro=404");
	}
	/*$a = session_id();
	if(empty($a)){
		echo "<br>Acesso Impedido<br>";
		echo "Usar Link: <a href='https://lgdf.med.up.pt/'> LGDF </a>";
		break("erro de sessão");
	}*/
	
	$login=$_SESSION['login'];
	
	if(empty($login)){
		header("Location: ../../index.php?erro=404");
	}
	
	require_once '../../classlib/Database.class.inc';
	
	//verificar se o utilizador logado tem permissoes para estar nesta página (diretor ou admin)
	$db = new Database();
	$db->connect();
	$user = $db->checkDirDepLogin($login);
	if(!$user || !strcmp($login,'sciman') || !strcmp($login, 'admin') || !strcmp($login, 'dirdep') ){
		$dados = mysql_fetch_assoc($user);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt" xml:lang="pt">

<head>
<link rel="icon" href="../../images/up.png" />
<meta name="description" content=""/>

<meta name="keywords" content=""/>

<meta name="author" content=""/>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<link rel="stylesheet" type="text/css" href="../../styles/style_screen2.css" media="screen"/>

<link rel="stylesheet" type="text/css" media="all" href="../../styles/jsDatePick_ltr.min.css"/>
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">

<title>Levantamento Geral de Dados da FMUP 2013</title>

<script type="text/javascript" src="../../js/jquery.1.4.2.js"></script>
<script type="text/javascript" src="../../js/funcoes.js"></script>
<script type="text/javascript" src='../../js/jQuery/jquery-1.10.2.js'></script>
</head>
<body>
	<fieldset class='head'>
		<table style="padding: 10px 10px 10px 190px;">
			<tr><td align="right"><h2>Levantamento Geral de Dados da FMUP 2014 - ADMINISTRAÇÃO</h2></td></tr>
			<tr><td align="right"><h2><i>General Survey of FMUP 2014</i></h2></td></tr>
		</table>
	</fieldset>
	
	<form name='exportacao' id='exportacao' action='./../../functions/exportaRelatorio.php' method='post' enctype='multipart/form-data'>
		<fieldset  class='normal'><legend>Exportação da informação levantada para Excel:</legend>
			<label>Ano de Levantamento: <select id='anoLevantamento' name='anoLevantamento'></select></label>
			<select id='departamento' name='departamento'>
				<optgroup label = "Escolha Departamento">
			<?php 
			$options='';
			if(!strcmp($login, 'sciman') || !strcmp($login, 'admin')){
				$departamentos = $db->getDepartamentos();
				$options .= "<option value=0 selected='selected'>Relatório Completo</option>";
				
				while($row = mysql_fetch_assoc($departamentos)){
					if($row["id"] != 100){
						$options.="<option value=".$row['id'].">".$row['Departamento']."</option>"; 
					}
				}
			}else{
				$options .= "<option value=".$dados['ID']." selected='selected'>".$dados['DESCRICAO']."</option>";
			}	
				echo $options;
			?>
				</optgroup>
			</select>
			<input type="hidden" value="<?php echo $login ?>" name="login" id="login">
			<input type="image" src="../../images/fileDownload.png" alt="Gerar Relatório" 
					style="height: 20px; width: 20px; padding: 0px; margin: 0px 0px 0px 10px;" onclick="document.form.submit();"/>
	</fieldset>
	</form>
	<div id='Download' style="margin: 0px 0px 0px 175px"></div>
	<br/>
	<input type="image" src="../../images/Back-Arrow-Button.png" alt="Voltar à página anterior"  style="margin: 0px 0px 0px 175px"
				width="24" height="24" onclick="window.location.assign(document.referrer);"/> 
	
	<fieldset class='footer'>
		<legend></legend>
		FACULDADE DE MEDICINA DA UNIVERSIDADE DO PORTO<br/>
		Serviço de Informática e Departamento de Apoio à Investigação e Pós-Graduação
	</fieldset>
	</body>
	<script type="text/javascript">
		$(document).ready(anos());
		$('#exportacao').submit(function(event){
				//event.preventDefault();
				$('form').hide("slow",function(){
					$('#Download').append('<p>Por favor aguarde enquanto o documento é formado...</p>');
					$('#Download').show("slow");
					});
			});
	</script>
</html>
<?php 	
}else{
	header("Location: ../../index.php?erro=404");
}
