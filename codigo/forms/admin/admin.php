<?php 
ob_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt" xml:lang="pt">

<head>

<meta name="description" content=""/>

<meta name="keywords" content=""/>

<meta name="author" content=""/>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<link rel="stylesheet" type="text/css" href="../../styles/style_screen2.css" media="screen"/>

<link rel="stylesheet" type="text/css" media="all" href="../../styles/jsDatePick_ltr.min.css"/>

<title>Levantamento Geral de Dados da FMUP 2013</title>

<script type="text/javascript" src="../../js/jsDatePick.jquery.min.1.3.js"></script>

<script type="text/javascript" src="../../js/funcoes.js"></script>
<script type="text/javascript" src='../../js/jQuery/jquery-1.10.2.js'></script>
<script type="text/javascript" src='../../js/jQuery/jquery-1.10.2.js'></script>
<script type="text/javascript" src='../../js/jQuery/ui/jquery.ui.core.js'></script>
<script type="text/javascript" src='../../js/jQuery/ui/jquery.ui.widget.js'></script>
<script type="text/javascript" src='../../js/jQuery/ui/jquery.ui.mouse.js'></script>
<script type="text/javascript" src='../../js/jQuery/ui/jquery.ui.button.js'></script>
<script type="text/javascript" src='../../js/jQuery/ui/jquery.ui.draggable.js'></script>
<script type="text/javascript" src='../../js/jQuery/ui/jquery.ui.position.js'></script>
<script type="text/javascript" src='../../js/jQuery/ui/jquery.ui.resizable.js'></script>
<script type="text/javascript" src='../../js/jQuery/ui/jquery.ui.button.js'></script>
<script type="text/javascript" src='../../js/jQuery/ui/jquery.ui.dialog.js'></script>
<script type="text/javascript" src='../../js/jQuery/ui/jquery.ui.effect.js'></script>
<script type="text/javascript" src='../../js/jQuery/ui/jquery.ui.accordion.js'></script>
<script type="text/javascript" src='../../js/jQuery/ui/i18n/jquery.ui.datepicker-pt.js'></script>
<script type="text/javascript" src="../../js/adminFuncs.js"></script>
<script type="text/javascript" src="../../js/stickyTableHeaders.js"></script>
<link rel="stylesheet" href="../../js/jQuery/themes/base/jquery.ui.all.css">
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

<script src='../../js/pace.js'></script>
<link href='../../js/pace-theme.css' rel='stylesheet' />

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>
        <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css" media="all" />

        <script type="text/javascript">
            $(document).ready(function() {

                $('#wrapper').dialog({
                    autoOpen: false,
                    title: 'Basic Dialog'
                });
                $('#opener').click(function() {
                    $('#wrapper').dialog('open');
//                  return false;
                });
            });
        </script>
        
<style>					
    body { font-size: 62.5%; }
    input.text { margin-bottom:12px; width:95%; padding: .4em; }
	fieldset#edit { padding:0; border:0; margin-top:25px; }
	h1 { font-size: 1.2em; margin: .6em 0; }
	div#dialog-form-projectos { width: 350px; margin: 20px 0; font-size: 0.9em;}
	div#dialog-form-projectos table { margin: 1em 0; border-collapse: collapse; width: 100%; }
	div#dialog-form-projectos table td, div#users-contain table th { border: 1px solid #eee; padding: .6em 10px; text-align: left; }
	.ui-dialog .ui-state-error { padding: .3em; }
	.validateTips { border: 1px solid transparent; padding: 0.3em; }
	
	th.header.headerSortDown { 
		background-image: url(../../images/asc.gif); 
		background-color: #3399FF; 
	}
	
	th.header.headerSortUp { 
		background-image: url(../../images/desc.gif);
		background-color: #3399FF; 
	} 
	
	th.header { 		
		background-image: url(../../images/bg.gif);
		cursor: pointer; 
		font-weight: bold; 
		background-repeat: no-repeat; 
		background-position: center left; 
		padding-left: 20px; 
		border-right: 1px solid #dad9c7; 
		margin-left: -1px; 
	} 
</style>


</head>

<?php 
session_start();
/*
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);
/**/	
//ini_set('session.gc_maxlifetime', 1*60);
$a = session_id();
if(empty($a)){
	header("Location: ../../index.php?erro=404");
}

$operacao=@$_POST['operacao'];

//echo "<br/>session status: <br/>session name: ".session_name()." <br/> SID: ".SID."<br/>session_id(): ".session_id()."<br/>COOKIE: ".$_COOKIE["PHPSESSID"];

$login=$_SESSION['login'];

if(empty($login)){
	header("Location: ../../index.php?erro=404");
}

require_once '../../classlib/Database.class.inc';
require_once '../../classlib/ValidaRepetidos.class.inc';

echo "<body>";

echo "<form name='adminform' action='admin.php' method='post' enctype='multipart/form-data'>";

echo "<input type='hidden' id='operacao'  name='operacao' value='0'>";

echo "<fieldset class='head'>";

echo "<table>";
echo "<td>Está autenticado como <i><span class='redcolor'>".$login."</span></i><input class='op-button' type='image' src='../../images/icon_off.png' name='navOption'  value='Sair' 
	onclick='var r=confirm(\"Tem a certeza!!!?Are you sure?\");
	if (r==true){
		document.adminform.operacao.value=1;
		document.adminform.submit();		
	}else{
		return false;
	}'>Sair</td>";

echo "<td align='right'><h2>Levantamento Geral de Dados da FMUP 2013 - ADMINISTRAÇÃO</h2><br/><h2><i>General Survey of FMUP 2013</i></h2></td>";
echo "</tr></table>";
echo "</fieldset>";


verificaOperacao($operacao);

echo "<fieldset class='footer'>";
echo "<legend></legend>
FACULDADE DE MEDICINA DA UNIVERSIDADE DO PORTO<br/>";
echo "Serviço de Informática e Departamento de Apoio à Investigação e Pós-Graduação";
echo "</fieldset>";
echo "</form>";
echo "</body></html>\n";

function verificaOperacao($operacao){
	global $iddep;
	
	switch($operacao){
		case '0': { // 
				echo "Nenhuma opção";
				mostraOpcoes();
		}
		break;
		case '1': {
			
			session_unset();
            $_SESSION = array();
            setcookie(session_name(), "", 0, "/");
            echo $operacao;
            header("Location: ../../index.php");
		}
		break;
		case '2': { // Lista Lacrados
			mostraOpcoes();
			include('lacrados.php');
		}
		break;
		case '3': { // Lista Lacrados
			$idinv=@$_POST['investigador'];
			$data_antiga=@$_POST['data_antiga'];
			deslacrarInvestigador($idinv,$data_antiga);
			mostraOpcoes();
			include('lacrados.php');
		}
		break;
		case '4': { // Lista Lacrados
			mostraOpcoes();
			include('naolacrados.php');
		}
		break;
		case '5': { // Lista Lacrados
			$idinv=@$_POST['investigador'];
			lacrarInvestigador($idinv);
			mostraOpcoes();
			include('lacrados.php');
		}
		break;
		case '6': { // Lista Lacrados
			mostraOpcoes();
			include('auditoria.php');
		}
		break;
		case '7': { // Auditoria Erros
			mostraOpcoes();
			include('auditoria.php');
		}
		break;
		case '8': { // Auditoria Erros
			mostraOpcoes();
			include('validacaoNome.php');
		}
		break;
		case '9': { // Auditoria Erros
			mostraOpcoes();
			include('exportaDados.php');
		}
		break;
		case '10': { // Auditoria Erros
			mostraOpcoes();
			include('dadosDep.php');
		}
		break;		
		case '11': { // Auditoria Erros
			mostraOpcoes();			
			$iddep=@$_POST['departamento'];	
			$_SESSION['departamento']=$iddep;
			include('dadosDep.php');	
			include('../questionarioFinalDepartamentoAdmin.php');
		}
		break;
		case '12': {
			mostraOpcoes();
			include('validacaoRepetidos/validacaoRepetidos.php');	
		}	
		break;
		case '13': {			
			mostraOpcoes();
			$iddep=@$_POST['departamento'];			
			$_SESSION['departamento']=$iddep;
			$tipoVal = @$_POST['tipoValidacao'];
			$_SESSION['tipoValidacao']= $tipoVal;
			include('validacaoRepetidos/validacaoRepetidos.php');
			if($iddep != '51')
			{
				include('validacaoRepetidos/validaRepetidos.php');
			}			
		}
		break;
		case '14': {
			mostraOpcoes();
			include('validacaoDados/validacaoDados.php');	
		}	
		break;
		case '15': {
			$login=$_SESSION['login'];
			mostraOpcoes();
			$iddep=@$_POST['departamento'];
			$_SESSION['departamento']=$iddep;
			if($login == 'admin' || $login == 'sciman')
			{
				include('validacaoDados/validacaoDados.php');
			}
			else 
			{
				//buscar id do departamento do director				
				$id_dep = getIdDepFromDir($login);
				if($id_dep == -1)
				{
					header("Location: ../../index.php?erro=404");
				}
				//dar set do id do departamento na session
				$_SESSION['departamento'] = $id_dep;
			}
			if($iddep !== '51')
			{				
				include('validacaoDados/validaDados.php');
			}
		}			
		break;
		case '16': {
			mostraOpcoes();
			include('validacaoAcoes/validacaoAcoes.php');	
		}
		break;
		case '17': {
			mostraOpcoes();
			$iddep=@$_POST['departamento'];			
			$_SESSION['departamento']=$iddep;
			include('validacaoAcoes/validacaoAcoes.php');
			if($iddep != '51')
			{
				include('validacaoAcoes/validaAcoes.php');
			}
		}
		break;
		case '18': { // Classificacao de Publicacoes
			mostraOpcoes();
			
			include('pubClassificacoes_tipofmup.php');
			
		}
		break;
		case '19': { // Exportação de Relatório Completo para excel
				
			header("Location:exportacao.php");
				
		} 
		break;
		case '20': { // Transição de dados
			//mostraOpcoes();
			header("Location:transicaoBD.php");
		}
		break;
		default:{ //echo "DEFAULT";
			mostraOpcoes();
			//echo "Nenhuma opção por defeito";
		}
		break;
	}
}

function mostraOpcoes(){
	echo "<style>p:hover{ text-decoration: underline;}</style>
	<fieldset class='normal'>
		<table class='box-table-c'>
			<tr>
				<td><p><input type='image' src='../../images/icon_spyglass.jpg' name='navOption' value='lacrados' onclick='document.adminform.operacao.value=2;document.adminform.submit();'>Listar lacrados ".getEstadoLacrados()."</p></td>
				<td><p><input type='image' src='../../images/icon_spyglass.jpg' name='navOption' value='naolacrados' onclick='document.adminform.operacao.value=4;document.adminform.submit();'>Listar não lacrados ".getEstadoNaoLacrados()."</p></td>
				<td><p><input type='image' src='../../images/icon_spyglass.jpg' name='navOption' value='auditoria' onclick='document.adminform.operacao.value=6;document.adminform.submit();'>Auditoria</p></td>
				<td><p><input type='image' src='../../images/icon_spyglass.jpg' name='navOption' value='nome' onclick='document.adminform.operacao.value=9;document.adminform.submit();'>Exportação</p></td>
				<td><p><input type='image' src='../../images/icon_spyglass.jpg' name='navOption' value='nome' onclick='document.adminform.operacao.value=10;document.adminform.submit();'>Relatórios Departamentos</p></td>
				<td><p><input type='image' src='../../images/icon_spyglass.jpg' name='navOption' value='nome' onclick='document.adminform.operacao.value=8;document.adminform.submit();'>Validacao Nomes</p></td>
			</tr>
		</table>	
		<br>
		<table class='box-table-b'>			
			<caption><u><h3>Tratamento dos Dados Recolhidos</h3></u></caption>
			<tbody>
				<tr>
					<th colspan='4'>Correção de Dados</th>
					<th colspan='2'>Produção de Relatório e Transição de dados</th>
				</tr>	
				<tr>
					<td><h3><b>1.</b></h3><p onclick='document.adminform.operacao.value=12;document.adminform.submit();'>Validação de Repetidos/Duplicados</p></td>
					<td><h3><b>2.</b></h3><p onclick='document.adminform.operacao.value=14;document.adminform.submit();'>Validação/Edição de Dados</p></td>
					<td><h3><b>3.</b></h3><p onclick='document.adminform.operacao.value=16;document.adminform.submit();'>Validação de Ações (" .getNumAcoes(). ")</p></td>
					<td><h3><b>4.</b></h3><p onclick='document.adminform.operacao.value=18;document.adminform.submit();'>Validação de Publicações</p></td>
					<td><h3><b>A.</b></h3><p onclick='document.adminform.operacao.value=19;document.adminform.submit();'>Exportação de Relatório</p></td>
					<td><h3><b>B.</b></h3><p onclick='document.adminform.operacao.value=20;document.adminform.submit();'>Transição de Dados para o Ano Letivo seguinte</p></td>					
				</tr>
			</tbody>
		</table>
	</fieldset>";
}

function deslacrarInvestigador($idInv,$data_antiga){
	$db = new Database();
	$db->adminDeslacraInvestigador($idInv,$data_antiga);
}

function lacrarInvestigador($idInv){
	$db = new Database();
	$db->adminLacraInvestigador($idInv);
}

function getEstadoLacrados(){
	$nr=0;
	$nl=0;
	$db=new Database();	
	$nl=$db->adminGetNLacradosTotal();	
	$nlogs=$db->adminGetNLLoginsTotal();	
	$nr=$db->adminGetNRegistadosTotal();
	return "<br>Com login ".$nl."/".$nlogs."<br> Registados :".$nr;
}

function getEstadoNaoLacrados(){
	$nr=0;
	$nnl=0;
	$db=new Database();
	$nr=$db->adminGetNRegistadosTotal();
	$nlogs=$db->adminGetNLLoginsTotal();	
	$nnl=$db->adminGetNNaoLacradosTotal();
	return "<br>Com login ".$nnl."/".$nlogs."<br> Registados :".$nr;
}

function getNumAcoes(){
	$db = new Database();
	$nr = $db->getNumAcoes();
	return $nr;
}

ob_flush();
?>