<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt" xml:lang="pt">

<head>

<meta name="description" content=""/>

<meta name="keywords" content=""/>

<meta name="author" content=""/>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<link rel="stylesheet" type="text/css" href="../../styles/style_screen2.css" media="screen"/>

<link rel="stylesheet" type="text/css" media="all" href="../../styles/jsDatePick_ltr.min.css"/>

<title>Levantamento Geral de Dados da FMUP 2013</title>

<script type="text/javascript" src="../../js/jsDatePick.jquery.min.1.3.js"></script>

<script type="text/javascript" src="../../js/funcoes.js"></script>
<script type="text/javascript" src='../../js/jQuery/jquery-1.10.2.js'></script>
<script type="text/javascript" src='../../js/jQuery/jquery-1.10.2.js'></script>
<script type="text/javascript" src='../../js/jQuery/ui/jquery.ui.core.js'></script>
<script type="text/javascript" src='../../js/jQuery/ui/jquery.ui.widget.js'></script>
<script type="text/javascript" src='../../js/jQuery/ui/jquery.ui.mouse.js'></script>
<script type="text/javascript" src='../../js/jQuery/ui/jquery.ui.button.js'></script>
<script type="text/javascript" src='../../js/jQuery/ui/jquery.ui.draggable.js'></script>
<script type="text/javascript" src='../../js/jQuery/ui/jquery.ui.position.js'></script>
<script type="text/javascript" src='../../js/jQuery/ui/jquery.ui.resizable.js'></script>
<script type="text/javascript" src='../../js/jQuery/ui/jquery.ui.button.js'></script>
<script type="text/javascript" src='../../js/jQuery/ui/jquery.ui.dialog.js'></script>
<script type="text/javascript" src='../../js/jQuery/ui/jquery.ui.effect.js'></script>
<script type="text/javascript" src='../../js/jQuery/ui/jquery.ui.accordion.js'></script>
<script type="text/javascript" src='../../js/jQuery/ui/i18n/jquery.ui.datepicker-pt.js'></script>
<script type="text/javascript" src="../../js/adminFuncs.js"></script>
<link rel="stylesheet" href="../../js/jQuery/themes/base/jquery.ui.all.css">
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

<style>				
	input.text { margin-bottom:12px; width:95%; padding: .4em; }
	fieldset#edit { padding:0; border:0; margin-top:25px; }
	h1 { font-size: 1.2em; margin: .6em 0; }
	div#dialog-form-projectos { width: 350px; margin: 20px 0; font-size: 0.9em;}
	div#dialog-form-projectos table { margin: 1em 0; border-collapse: collapse; width: 100%; }
	div#dialog-form-projectos table td, div#users-contain table th { border: 1px solid #eee; padding: .6em 10px; text-align: left; }
	.ui-dialog .ui-state-error { padding: .3em; }
	.validateTips { border: 1px solid transparent; padding: 0.3em; }
</style>

</head>

<?php 
	session_start();
	define('SCRIPT_DEBUG', true);
	ini_set('display_errors',1);
	ini_set('display_startup_errors',1);
	error_reporting(-1);
	
//ini_set('session.gc_maxlifetime', 1*60);
$a = session_id();
if(empty($a)){
	header("Location: ../../index.php?erro=404");
}

require_once '../../classlib/Database.class.inc';
require_once '../../classlib/ValidaRepetidos.class.inc';
require_once '../../classlib/QuestionarioDepartamento.class.inc';
require_once '../../classlib/Projecto.class.inc';


require_once 'validacaoDados/validaDadosInvestigadores.php';
require_once 'validacaoDados/validaDadosHabilitacoes.php';
require_once 'validacaoDados/validaDadosAgregacoes.php';
require_once 'validacaoDados/validaDadosExperienciaProfissional.php';
require_once 'validacaoDados/validaDadosDirecoesCurso.php';
require_once 'validacaoDados/validaDadosColaboradores.php';
require_once 'validacaoDados/validaDadosUnidadesInvestigacao.php';
require_once 'validacaoDados/validaDadosPublicacoesISI.php';
require_once 'validacaoDados/validaDadosPublicacoesPUBMED.php';
require_once 'validacaoDados/validaDadosPublicacoesManualPatentes.php';
require_once 'validacaoDados/validaDadosPublicacoesManualInternacional.php';
require_once 'validacaoDados/validaDadosPublicacoesManualNacional.php';
require_once 'validacaoDados/validaDadosProjectos.php';
require_once 'validacaoDados/validaDadosConferencias.php';
require_once 'validacaoDados/validaDadosApresentacoes.php';
require_once 'validacaoDados/validaDadosArbitragensRevistas.php';
require_once 'validacaoDados/validaDadosPremios.php';
require_once 'validacaoDados/validaDadosSociedadesCientificas.php';
require_once 'validacaoDados/validaDadosRedes.php';
require_once 'validacaoDados/validaDadosMissoes.php';
require_once 'validacaoDados/validaDadosOrientacoes.php';
require_once 'validacaoDados/validaDadosJuris.php';
require_once 'validacaoDados/validaDadosOutrasActividades.php';
require_once 'validacaoDados/validaDadosAcoesDivulgacao.php';
require_once 'validacaoDados/validaDadosProdutos.php';

$operacao=@$_POST['operacao'];

$login=$_SESSION['login'];

$dep = checkDepInv($login);

$_SESSION['departamento'] = $dep;

if(empty($login) || !$dep){
	header("Location: ../../index.php?erro=404");
}

echo "<body>";

echo "<form name='adminform' action='dirDep.php' method='post' enctype='multipart/form-data'>";

echo "<input type='hidden' id='operacao'  name='operacao' value='0'>";

echo "<fieldset class='head'>";

echo "<table>";
echo "<td>Está autenticado como <i><span class='redcolor'>".$login."</span></i><input class='op-button' type='image' src='../../images/icon_off.png' name='navOption'  value='Sair' 
	onclick='var r=confirm(\"Tem a certeza?Are you sure?\");
	if (r==true){
		document.adminform.operacao.value=1;
		document.adminform.submit();		
	}else{
		return false;
	}'>Sair</td>";

echo "<td align='right'><h2>Levantamento Geral de Dados da FMUP 2014- Edição/Validação de Dados</h2><br/><h2><i>General Survey of FMUP 2013</i></h2></td>";
echo "</tr></table>";
echo "</fieldset>";

echo "
	<fieldset class='normal'>\n
		<table class='box-table-c'>
			<tr>
				<th align='center'>
					<u><h2>Validação/Edição de Dados</h2></u>
				</th>
				<th></th>
			</tr>
			<tr>
				<th align='center'>
				<u>";getNomeDepById($dep);echo"</u></th>
				<th></th>
			</tr>
		</table>
		<br>
		<input type='button' id='showhidebutton' onclick='ShowHideDivs();' value='Mostrar todos os separadores'/>
		<input type='hidden' id='showhidevalue' value='1'/><br>";
		

verificaOperacao($operacao);

if($dep)
{	
	//Informação de um departamento específico
		$dadosDep = new QuestionarioDepartamento($dep);
		$dadosDepRep = new ValidaRepetidosDepartamento($dep);			
		echo "<link rel=\"stylesheet\" href=\"../../js/jQuery/themes/base/jquery.ui.all.css\">
		<script>";
		echo "$(function () {   
				$( \"#accordion\" ).accordion({
					collapsible: true,
					heightStyle: 'content'
				});
			});
		
		
		function ShowHideDivs(){
			if($( \"#showhidevalue\" ).val()=='1'){
				$( \"#investigadores\" ).css(\"display\", \"block\");
				$( \"#habilitacoes\" ).css(\"display\", \"block\");
				$( \"#agregacoes\" ).css(\"display\", \"block\");
				$( \"#caracterizacaoDocentes\" ).css(\"display\", \"block\");
				$( \"#caracterizacaoPos\" ).css(\"display\", \"block\");
				$( \"#direcaoCurso\" ).css(\"display\", \"block\");
				$( \"#colaboradoresInternos\" ).css(\"display\", \"block\");
				$( \"#colaboradoresExternos\" ).css(\"display\", \"block\");
				$( \"#unidadesInvestigacao\" ).css(\"display\", \"block\");
				$( \"#publicacoesISI\" ).css(\"display\", \"block\");
				$( \"#publicacoesPUBMED\" ).css(\"display\", \"block\");
				$( \"#pubManualPatentes\" ).css(\"display\", \"block\");
				$( \"#pubManualInternacional\" ).css(\"display\", \"block\");
				$( \"#pubManualNacional\" ).css(\"display\", \"block\");
				$( \"#projectos\" ).css(\"display\", \"block\");
				$( \"#conferencias\" ).css(\"display\", \"block\");
				$( \"#apresentacoes\" ).css(\"display\", \"block\");
				$( \"#arbitragensRevistas\" ).css(\"display\", \"block\");
				$( \"#premios\" ).css(\"display\", \"block\");
				$( \"#sc\" ).css(\"display\", \"block\");
				$( \"#redes\" ).css(\"display\", \"block\");
				$( \"#missoes\" ).css(\"display\", \"block\");
				$( \"#orientacoes\" ).css(\"display\", \"block\");
				$( \"#juris\" ).css(\"display\", \"block\");
				$( \"#outact\" ).css(\"display\", \"block\");
				$( \"#acaodivulgacao\" ).css(\"display\", \"block\");
				$( \"#produtos\" ).css(\"display\", \"block\");
		
				$( \"#showhidevalue\" ).val(0);
				$( \"#showhidebutton\" ).val('Esconder todos os separadores');
			}else{
				$( \"#investigadores\" ).css(\"display\", \"none\");
				$( \"#habilitacoes\" ).css(\"display\", \"none\");
				$( \"#agregacoes\" ).css(\"display\", \"none\");
				$( \"#caracterizacaoDocentes\" ).css(\"display\", \"none\");
				$( \"#caracterizacaoPos\" ).css(\"display\", \"none\");
				$( \"#direcaoCurso\" ).css(\"display\", \"none\");
				$( \"#colaboradoresInternos\" ).css(\"display\", \"none\");
				$( \"#colaboradoresExternos\" ).css(\"display\", \"none\");
				$( \"#unidadesInvestigacao\" ).css(\"display\", \"none\");
				$( \"#publicacoesISI\" ).css(\"display\", \"none\");
				$( \"#publicacoesPUBMED\" ).css(\"display\", \"none\");
				$( \"#pubManualPatentes\" ).css(\"display\", \"none\");
				$( \"#pubManualInternacional\" ).css(\"display\", \"none\");
				$( \"#pubManualNacional\" ).css(\"display\", \"none\");
				$( \"#projectos\" ).css(\"display\", \"none\");
				$( \"#conferencias\" ).css(\"display\", \"none\");
				$( \"#apresentacoes\" ).css(\"display\", \"none\");
				$( \"#arbitragensRevistas\" ).css(\"display\", \"none\");
				$( \"#premios\" ).css(\"display\", \"none\");
				$( \"#sc\" ).css(\"display\", \"none\");
				$( \"#redes\" ).css(\"display\", \"none\");
				$( \"#missoes\" ).css(\"display\", \"none\");
				$( \"#orientacoes\" ).css(\"display\", \"none\");
				$( \"#juris\" ).css(\"display\", \"none\");
				$( \"#outact\" ).css(\"display\", \"none\");
				$( \"#acaodivulgacao\" ).css(\"display\", \"none\");
				$( \"#produtos\" ).css(\"display\", \"none\");
				$( \"#showhidevalue\" ).val(1);
				$( \"#showhidebutton\" ).val('Mostrar todos os separadores');
			}
		}
		
		";
		echo "</script>";
		echo "<style>
			body { font-size: 70%; }
			label, input { display:block; }
			input.text { margin-bottom:12px; width:95%; padding: .4em; }
			fieldset#edit { padding:0; border:0; margin-top:25px; }
			h1 { font-size: 1.2em; margin: .6em 0; }
			div#users-contain { width: 350px; margin: 20px 0; }
			div#users-contain table { margin: 1em 0; border-collapse: collapse; width: 100%; }
			div#users-contain table td, div#users-contain table th { border: 1px solid #eee; padding: .6em 10px; text-align: left; }
			.ui-dialog .ui-state-error { padding: .3em; }
			.validateTips { border: 1px solid transparent; padding: 0.3em; }
			thead 
			{ 
				font-size: 95%;
			}
			tbody 
			{ 
				font-size: 85%;
			}
		</style>";
		
		
		echo "<div id='wrapper' style='width: 2300px'>
				<div id=\"accordion\" >"; 	
					showEditInvestigadores($dadosDep->investigadores);
					showEditHabilitacoes($dadosDep->habilitacoes);
					showEditAgregacoes($dadosDep->agregacoes);
					showEditExperienciaProfissional($dadosDep->ocupacaoprofissional);
					showEditDirecoesCurso($dadosDep->dirCursos);
					showEditColaboradores($dadosDep);
					showEditUnidadesInvestigacao($dadosDepRep->unidadesInvestigacao);
					showEditPublicacoesISI($dadosDepRep->publicacoesISI);
					showEditPublicacoesPUBMED($dadosDepRep->publicacoesPUBMED);
					showEditPublicacoesManualPatentes($dadosDepRep->publicacoesMANUALPatentes);					
					showEditPublicacoesManualInternacional($dadosDepRep->publicacoesMANUALInternacional);						
					showEditPublicacoesManualNacional($dadosDepRep->publicacoesMANUALNacional);
					showEditProjectos($dadosDepRep->projectos);	
					showEditConferencias($dadosDepRep->conferencias);
					showEditApresentacoes($dadosDep->apresentacoes);
					showEditArbitragensRevistas($dadosDepRep->arbitragensRevistas);
					showEditPremios($dadosDepRep->premios);
					showEditSociedadesCientificas($dadosDep->sc);
					showEditRedes($dadosDep->redes);
					showEditMissoes($dadosDep->missoes);
					showEditOrientacoes($dadosDep->orientacoes);	
					showEditJuris($dadosDep->juris);
					showEditOutrasActividades($dadosDep->outrasactividades);
					showEditAcoesDivulgacao($dadosDepRep->acoesDivulgacao);
					showEditProdutos($dadosDep->produtos);
		echo "</div></div>
		";
		
echo "
	  <script type='text/javascript' src='../../js/jquery.tablesorter.js'></script>
	  <script>
		$(document).ready(function() {	
		
			//Investigadores
			$('#users').tablesorter();
			$('#habs').tablesorter();
			$('#agreg').tablesorter();
			$('#caracDoc').tablesorter();
			$('#caracPos').tablesorter();
			$('#dircurso').tablesorter();
			$('#colIntAct').tablesorter();
			$('#colExtAct').tablesorter();
			$('#uniInv').tablesorter();
			
			//ISI			
			$('#AISI').tablesorter();
			$('#PISI').tablesorter();
			$('#CPNISI').tablesorter();
			$('#CLNISI').tablesorter();
			$('#LNISI').tablesorter();
			$('#SNISI').tablesorter();
			$('#PRPNISI').tablesorter();
			$('#JNISI').tablesorter();
			$('#SISI').tablesorter();
			$('#CLISI').tablesorter();
			$('#LISI').tablesorter();
			$('#OAISI').tablesorter();
			$('#CPISI').tablesorter();
			$('#JAISI').tablesorter();
			$('#PRPISI').tablesorter();
			$('#JISI').tablesorter();
			
			//PUBMED
			$('#JPUBMED').tablesorter();
			$('#PRPPUBMED').tablesorter();
			$('#JAPUBMED').tablesorter();
			$('#CPPUBMED').tablesorter();
			$('#OAPUBMED').tablesorter();
			$('#CLPUBMED').tablesorter();
			$('#SPUBMED').tablesorter();
			$('#JNPUBMED').tablesorter();
			$('#PRPNPUBMED').tablesorter();
			$('#CPNPUBMED').tablesorter();
			$('#LPUBMED').tablesorter();
			$('#CLNPUBMED').tablesorter();
			$('#SNPUBMED').tablesorter();
			$('#PPUBMED').tablesorter();
			$('#APUBMED').tablesorter();	
			
			//Patentes
			$('#pubManPat').tablesorter();
			
			//Publicações Manual Internacional			
 			$('#pubManInt-Art').tablesorter();
			$('#pubManInt-Rev').tablesorter();
			$('#pubManInt-Sum').tablesorter();
			$('#pubManInt-Liv').tablesorter();
			$('#pubManInt-CLiv').tablesorter();
			$('#pubManInt-Con').tablesorter();
			$('#pubManInt-Oth').tablesorter();  
			
			
			//Publicacoes Manual Nacional
			$('#pubManNac-Con').tablesorter();
			$('#pubManNac-Oth').tablesorter();
			$('#pubManNac-CLiv').tablesorter();
			$('#pubManNac-Liv').tablesorter();
			$('#pubManNac-Sum').tablesorter();
			$('#pubManNac-Rev').tablesorter();
			$('#pubManNac-Art').tablesorter();			
										
			$('#prj').tablesorter();		
			$('#cnf').tablesorter();			
			$('#apr').tablesorter();
			$('#arbRevistas').tablesorter();
			$('#premio').tablesorter();
			$('#socCient').tablesorter();
			$('#rede').tablesorter();
			$('#missao').tablesorter();
			$('#orientacao').tablesorter();
			$('#juri').tablesorter();
			$('#outrasAct').tablesorter();
			$('#prdts').tablesorter();
			$('#acaoDiv').tablesorter();			
		});
	</script>";
}
echo "</fieldset>";
echo "<fieldset class='footer'>";
echo "<legend></legend>
FACULDADE DE MEDICINA DA UNIVERSIDADE DO PORTO<br/>";
echo "Serviço de Informática e Departamento de Apoio à Investigação e Pós-Graduação";
echo "</fieldset>";
echo "</form>";

include("../camposEdicao.php");
echo "</body></html>\n";

function verificaOperacao($operacao){
	global $iddep;
	
	switch($operacao){
		case '0': { // 
		}
		break;
		case '1': { //Logout
			header("Location: ../../index.php");
		}
		break;		
		default:{ //echo "DEFAULT";			
		}
		break;
	}
}

function checkDepInv($login) {

	$db = new Database();
	$valid = $db->getIdDepFromDir($login);
	
	if($valid == -1) {
		return false;
	} else {		
		return $valid;
	}	
}

function getNomeDepById($dep) {
	$db = new Database();
	$valid = $db->getNomeDepById($dep);		
	echo $valid;
}


?>