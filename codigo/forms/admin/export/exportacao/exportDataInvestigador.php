<?php

require_once('../../../../classlib/Database.class.inc');

$db=new Database();


$invData=$db->getAllInvestigadores();


$data=array();

while($row=mysql_fetch_assoc($invData)){


	$morada=str_replace("\r\n"," ",$row["MORADA"]);
	//$morada2=str_replace("\r", "",$morada);
	//$morada=$morada_2;
	
	
	array_push($data, array(
			"ID"=>iconv('UTF-8', 'windows-1252',$row["ID"]),
			"IDINV"=>iconv('UTF-8', 'windows-1252',$row["IDINV"]),
			"SERVICO"=>iconv('UTF-8', 'windows-1252',$row["SERVICO"]),
			"SERVICODESCR"=>iconv('UTF-8', 'windows-1252',$row["SERVICODESCR"]),
			"NOME"=>iconv('UTF-8', 'windows-1252',$row["NOME"]),
			"NUMMEC"=>iconv('UTF-8', 'windows-1252',$row["NUMMEC"]),
			"FREGUESIANAT"=>iconv('UTF-8', 'windows-1252',$row["FREGUESIANAT"]),
			"CONCELHONAT"=>iconv('UTF-8', 'windows-1252',$row["CONCELHONAT"]),
			"PAISNAC"=>iconv('UTF-8', 'windows-1252',$row["PAISNAC"]),
			"PAISNAT"=>iconv('UTF-8', 'windows-1252',$row["PAISNAT"]),
			"DATANASC"=>iconv('UTF-8', 'windows-1252',$row["DATANASC"]),
			"MORADA"=>iconv('UTF-8', 'windows-1252',$morada),
			"EMAIL"=>iconv('UTF-8', 'windows-1252',$row["EMAIL"]),
			"EMAIL_ALTERNATIVO"=>iconv('UTF-8', 'windows-1252',$row["EMAIL_ALTERNATIVO"]),
			"TELEFONE"=>iconv('UTF-8', 'windows-1252',$row["TELEFONE"]),
			"TELEMOVEL"=>iconv('UTF-8', 'windows-1252',$row["TELEMOVEL"]),
			"EXTENSAO"=>iconv('UTF-8', 'windows-1252',$row["EXTENSAO"]),
			"LOGIN"=>iconv('UTF-8', 'windows-1252',$row["LOGIN"]),
			"SERVICO"=>iconv('UTF-8', 'windows-1252',$row["SERVICO"]),
			"ESTADO"=>iconv('UTF-8', 'windows-1252',$row["ESTADO"]),
			"DATASUBMISSAO"=>iconv('UTF-8', 'windows-1252',$row["DATASUBMISSAO"]),
			"QUEST"=>iconv('UTF-8', 'windows-1252',$row["QUEST"]),
			"NUMIDENTIFICACAO	"=>iconv('UTF-8', 'windows-1252',$row["NUMIDENTIFICACAO"]),
			"SEXO"=>iconv('UTF-8', 'windows-1252',$row["SEXO"])
	
		));
	//"MORADA"=>iconv('UTF-8', 'ISO-8859-15//TRANSLIT',"\"".str_replace($novalinha, "", $row["MORADA"])."\""),
	
}




$csvTitle = "Tabela Investigadores em ".date('d/m/Y');
 
/* We know the keys of each sub-array are the same, so
 * extract them from the first sub-array and set them
 * to be our column titles */
$titleArray = array_keys($data[0]);
 
/* Set your desired delimiter. You can make this a true
 * .csv and set $delimiter = ","; but I find that tabs
 * work better as commas can also be present in your data.
 * Note that you must use the .tsv or .xls file extension for Excel
 * to correctly interpret tabs. Otherwise if you are using commas
 * for your delimiter, use .csv for your file extension. */
$delimiter = "\t";
 
//Set target filename - see above comment on file extension.
$filename="TabelaInvestigadores".date('d_m_Y_mis').".xls";
 
//Send headers
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$filename");
header("Pragma: no-cache");
header("Expires: 0");
 
//print the title to the first cell
print $csvTitle . "\r\n";
 
//Separate each column title name with the delimiter
$titleString = implode($delimiter, $titleArray);
print $titleString . "\r\n";





//Loop through each subarray, which are our data sets
foreach ($data as $subArrayKey => $subArray) {
	//Separate each datapoint in the row with the delimiter
	$dataRowString = implode($delimiter, $subArray);
	print $dataRowString . "\r\n";
}
?>