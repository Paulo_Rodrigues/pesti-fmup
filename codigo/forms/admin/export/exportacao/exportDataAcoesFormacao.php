<?php

require_once('../../../../classlib/Database.class.inc');

$db=new Database();


$invData=$db->getAllData(acoesformacao);


$data=array();

while($row=mysql_fetch_assoc($invData)){


	array_push($data, array(
		"IDINV"=>iconv('UTF-8', 'windows-1252',$row["IDINV"]),
		"SERVICO"=>iconv('UTF-8', 'windows-1252',$row["SERVICO"]),
		"SERVICODESCR"=>iconv('UTF-8', 'windows-1252',$row["SERVICODESCR"]),
		"TITULO"=>iconv('UTF-8', 'windows-1252',$row["TITULO"]),
		"HORAS"=>iconv('UTF-8', 'windows-1252',$row["HORAS"]),
		"PRECO"=>iconv('UTF-8', 'windows-1252',$row["PRECO"]),
		"FORMANDOS"=>iconv('UTF-8', 'windows-1252',$row["FORMANDOS"]),
		"NFORMANDOS"=>iconv('UTF-8', 'windows-1252',$row["NFORMANDOS"]),
		"DATAINI"=>iconv('UTF-8', 'windows-1252',$row["DATAINI"]),
		"DATAFIM"=>iconv('UTF-8', 'windows-1252',$row["DATAFIM"]),
		"LOCAL"=>iconv('UTF-8', 'windows-1252',$row["LOCAL"]),
		"OBJECTIVO"=>iconv('UTF-8', 'windows-1252',$row["OBJECTIVO"]),
	
	
		));
		
	
}




$csvTitle = "Tabela Dados Acoes Formacao em ".date('d/m/Y');
 
/* We know the keys of each sub-array are the same, so
 * extract them from the first sub-array and set them
 * to be our column titles */
$titleArray = array_keys($data[0]);
 
/* Set your desired delimiter. You can make this a true
 * .csv and set $delimiter = ","; but I find that tabs
 * work better as commas can also be present in your data.
 * Note that you must use the .tsv or .xls file extension for Excel
 * to correctly interpret tabs. Otherwise if you are using commas
 * for your delimiter, use .csv for your file extension. */
$delimiter = "\t";
 
//Set target filename - see above comment on file extension.
$filename="TabelaDadosAcoesFormacao".date('d_m_Y_mis').".xls";
 
//Send headers
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$filename");
header("Pragma: no-cache");
header("Expires: 0");
 
//print the title to the first cell
print $csvTitle . "\r\n";
 
//Separate each column title name with the delimiter
$titleString = implode($delimiter, $titleArray);
print $titleString . "\r\n";





//Loop through each subarray, which are our data sets
foreach ($data as $subArrayKey => $subArray) {
	//Separate each datapoint in the row with the delimiter
	$dataRowString = implode($delimiter, $subArray);
	print $dataRowString . "\r\n";
}
?>