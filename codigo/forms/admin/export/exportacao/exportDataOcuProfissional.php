<?php

require_once('../../../../classlib/Database.class.inc');


$db=new Database();


$invData=$db->getAllData(ocupacaoprofissional);


$data=array();

while($row=mysql_fetch_assoc($invData)){


	array_push($data, array(
			"ID"=>iconv('UTF-8', 'windows-1252',$row["ID"]),
			"IDINV"=>iconv('UTF-8', 'windows-1252',$row["IDINV"]),
			"SERVICO"=>iconv('UTF-8', 'windows-1252',$row["SERVICO"]),
		"SERVICODESCR"=>iconv('UTF-8', 'windows-1252',$row["SERVICODESCR"]),
			"DOCENTEPER"=>iconv('UTF-8', 'windows-1252',$row["DOCENTEPER"]),
			"DOCENTEMES"=>iconv('UTF-8', 'windows-1252',$row["DOCENTEMES"]),
			"DOCENTECAT"=>iconv('UTF-8', 'windows-1252',$row["DOCENTECAT"]),
			"DOCENTEOUTRADESIG"=>iconv('UTF-8', 'windows-1252',$row["DOCENTEOUTRADESIG"]),
			"INVESTIGADORPER"=>iconv('UTF-8', 'windows-1252',$row["INVESTIGADORPER"]),
			"INVESTIGADORMES"=>iconv('UTF-8', 'windows-1252',$row["INVESTIGADORMES"]),
			"INVESTIGADORCAT"=>iconv('UTF-8', 'windows-1252',$row["INVESTIGADORCAT"]),
			"INVESTIGADOROUTRADESIG"=>iconv('UTF-8', 'windows-1252',$row["INVESTIGADOROUTRADESIG"]),
			"PCICLOPER"=>iconv('UTF-8', 'windows-1252',$row["PCICLOPER"]),
			"PCICLOMES"=>iconv('UTF-8', 'windows-1252',$row["PCICLOMES"]),
			"SCICLOPER"=>iconv('UTF-8', 'windows-1252',$row["SCICLOPER"]),
			"SCICLOMES"=>iconv('UTF-8', 'windows-1252',$row["SCICLOMES"]),
			"TCICLOPER"=>iconv('UTF-8', 'windows-1252',$row["TCICLOPER"]),
			"TCICLOMES"=>iconv('UTF-8', 'windows-1252',$row["TCICLOMES"]),
			"OCICLOPER"=>iconv('UTF-8', 'windows-1252',$row["OCICLOPER"]),
			"OCICLOMES"=>iconv('UTF-8', 'windows-1252',$row["OCICLOMES"]),
			"OCICLODESIG"=>iconv('UTF-8', 'windows-1252',$row["OCICLODESIG"]),
			"TECPER"=>iconv('UTF-8', 'windows-1252',$row["TECPER"]),
			"TECMES"=>iconv('UTF-8', 'windows-1252',$row["TECMES"]),
			"OUTRAPER"=>iconv('UTF-8', 'windows-1252',$row["OUTRAPER"]),
			"OUTRAMES"=>iconv('UTF-8', 'windows-1252',$row["OUTRAMES"]),
			"OUTRADESIG"=>iconv('UTF-8', 'windows-1252',$row["OUTRADESIG"]),
			"FMUPINV"=>iconv('UTF-8', 'windows-1252',$row["FMUPINV"]),
			"FMUPENS"=>iconv('UTF-8', 'windows-1252',$row["FMUPENS"]),
			"FMUPGEST"=>iconv('UTF-8', 'windows-1252',$row["FMUPGEST"]),
			"FMUPDC"=>iconv('UTF-8', 'windows-1252',$row["FMUPDC"]),
			"INVESTIGADORBOLSA"=>iconv('UTF-8', 'windows-1252',$row["INVESTIGADORBOLSA"])
	
		));
		
	
}




$csvTitle = "Tabela Ocupacao Profissional em ".date('d/m/Y');
 
/* We know the keys of each sub-array are the same, so
 * extract them from the first sub-array and set them
 * to be our column titles */
$titleArray = array_keys($data[0]);
 
/* Set your desired delimiter. You can make this a true
 * .csv and set $delimiter = ","; but I find that tabs
 * work better as commas can also be present in your data.
 * Note that you must use the .tsv or .xls file extension for Excel
 * to correctly interpret tabs. Otherwise if you are using commas
 * for your delimiter, use .csv for your file extension. */
$delimiter = "\t";
 
//Set target filename - see above comment on file extension.
$filename="TabelaOcupacaoProfissional".date('d_m_Y_mis').".xls";
 
//Send headers
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$filename");
header("Pragma: no-cache");
header("Expires: 0");
 
//print the title to the first cell
print $csvTitle . "\r\n";
 
//Separate each column title name with the delimiter
$titleString = implode($delimiter, $titleArray);
print $titleString . "\r\n";





//Loop through each subarray, which are our data sets
foreach ($data as $subArrayKey => $subArray) {
	//Separate each datapoint in the row with the delimiter
	$dataRowString = implode($delimiter, $subArray);
	print $dataRowString . "\r\n";
}
?>