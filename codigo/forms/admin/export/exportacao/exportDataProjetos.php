<?php

require_once('../../../../classlib/Database.class.inc');

$db=new Database();


$invData=$db->getAllData(projectosinv);


$data=array();

while($row=mysql_fetch_assoc($invData)){

		$cod=str_replace("\"", "", $row["CODIGO"]);

	array_push($data, array(
		"ID"=>iconv('UTF-8', 'windows-1252',$row["ID"]),
		"IDINV"=>iconv('UTF-8', 'windows-1252',$row["IDINV"]),
		"SERVICO"=>iconv('UTF-8', 'windows-1252',$row["SERVICO"]),
		"SERVICODESCR"=>iconv('UTF-8', 'windows-1252',$row["SERVICODESCR"]),
		"ENTIDADE"=>iconv('UTF-8', 'windows-1252',$row["ENTIDADE"]),
		"MONTANTE"=>iconv('UTF-8', 'windows-1252',$row["MONTANTE"]),
		"MONTANTEA"=>iconv('UTF-8', 'windows-1252',$row["MONTANTEA"]),
		"MONTANTEFMUP"=>iconv('UTF-8', 'windows-1252',$row["MONTANTEFMUP"]),
		"ANO"=>iconv('UTF-8', 'windows-1252',$row["ANO"]),
		"INVRESPONSAVEL"=>iconv('UTF-8', 'windows-1252',$row["INVRESPONSAVEL"]),
		"CODIGO"=>iconv('UTF-8', 'windows-1252',str_replace("\r\n",'', $cod)),
		"TITULO"=>iconv('UTF-8', 'windows-1252',str_replace("\r\n",'', $row["TITULO"])),
		"DATAINICIO"=>iconv('UTF-8', 'windows-1252',$row["DATAINICIO"]),
		"DATAFIM"=>iconv('UTF-8', 'windows-1252',$row["DATAFIM"]),
		"ENTIDADEFINANCIADORA"=>iconv('UTF-8', 'windows-1252',$row["ENTIDADEFINANCIADORA"]),
		"ACOLHIMENTO"=>iconv('UTF-8', 'windows-1252',$row["ACOLHIMENTO"]),
		"LINK"=>iconv('UTF-8', 'windows-1252',str_replace("\r\n",'', $row["LINK"])),
		"ESTADO"=>iconv('UTF-8', 'windows-1252',$row["ESTADO"])
	
		));
		
	
}




$csvTitle = "Tabela Dados Projetos 2011 em ".date('d/m/Y');
 
/* We know the keys of each sub-array are the same, so
 * extract them from the first sub-array and set them
 * to be our column titles */
$titleArray = array_keys($data[0]);
 
/* Set your desired delimiter. You can make this a true
 * .csv and set $delimiter = ","; but I find that tabs
 * work better as commas can also be present in your data.
 * Note that you must use the .tsv or .xls file extension for Excel
 * to correctly interpret tabs. Otherwise if you are using commas
 * for your delimiter, use .csv for your file extension. */
$delimiter = "\t";
 
//Set target filename - see above comment on file extension.
$filename="TabelaDadosProjetos2011".date('d_m_Y_mis').".xls";
 
//Send headers
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$filename");
header("Pragma: no-cache");
header("Expires: 0");
 
//print the title to the first cell
print $csvTitle . "\r\n";
 
//Separate each column title name with the delimiter
$titleString = implode($delimiter, $titleArray);
print $titleString . "\r\n";





//Loop through each subarray, which are our data sets
foreach ($data as $subArrayKey => $subArray) {
	//Separate each datapoint in the row with the delimiter
	$dataRowString = implode($delimiter, $subArray);
	print $dataRowString . "\r\n";
}
?>