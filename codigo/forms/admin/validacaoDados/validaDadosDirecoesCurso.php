<?php 

function showEditDirecoesCurso($dadosDep)
{
	$db = new Database();
	echo "<h3>Direções de Curso</h3>\n";
	echo "<div id='direcaoCurso'>\n";

	echo "<table id='dircurso' class='box-table-b'>
			<thead>
				<tr>
					<th>ID Inv</th>
					<th>Nome</th>
					<th>Data de Inicio</th>
					<th>Data de Fim</th>
					<th>Curso</th>
					<th>Grau</th>
					<th colspan='3'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewDirCurso();return false;'></center></th>
				</tr>
			</thead>
			<tbody>";
			
	foreach ($dadosDep as $i => $value){ 
		$checkAcao = $db->checkAcaoExists($dadosDep[$i]->id, $_SESSION['login'], 5);		
		if ($checkAcao) {
			$acao = transformIntoAcaoDirDepObject($dadosDep[$i]->id, $dadosDep[$i]->idinv, $checkAcao);
			echo "<tr>";
				echo "<td>".$dadosDep[$i]->idinv."</td>";
				echo "<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
				echo "<td>".$dadosDep[$i]->datainicio."</td>";
				echo "<td>".$dadosDep[$i]->datafim."</td>";
				echo "<td>".$dadosDep[$i]->nome."</td>";
				echo "<td>";
				getGrauDirCursos($dadosDep[$i]->grau);	
				echo "</td>";
				echo "<td></td>";
				echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-dircurso').text('" . $dadosDep[$i]->id . "');setObservacaoDirCurso();return false;\" ></center></td>";
				echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-dircurso').text('" . $dadosDep[$i]->id . "');apagarDirCurso();return false;\" ></center></td>";   
			echo "</tr>";	
			
			echo "<tr>";
				echo "<td style='background:#FFFF33; overflow:hidden;'><img src=\"../../images/arrow_return_down_right.png\" name='navOption'></td>";
				echo "<td style='background:#FFFF33; overflow:hidden;'>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_dircurso_dataini_" . $dadosDep[$i]->id . "'>".$acao['datainicio']."</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_dircurso_datafim_" . $dadosDep[$i]->id . "'>".$acao['datafim']."</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_dircurso_curso_" . $dadosDep[$i]->id . "'>".$acao['nome']."</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_dircurso_grau_" . $dadosDep[$i]->id . "'>";
				getGrauDirCursos($acao['grau']);	
				echo "</td>";
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-dircurso').text('" . $dadosDep[$i]->id . "');\"></td>";
				echo "<td></td>";
				echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"apagarAcao('" . $dadosDep[$i]->id . "',5);return false;\" ></center></td>";   
			echo "</tr>";	
		} else {			
			echo "<tr>";
				echo "<td>".$dadosDep[$i]->idinv."</td>";
				echo "<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
				echo "<td id='td_dircurso_dataini_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->datainicio."</td>";
				echo "<td id='td_dircurso_datafim_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->datafim."</td>";
				echo "<td id='td_dircurso_curso_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->nome."</td>";
				echo "<td id='td_dircurso_grau_" . $dadosDep[$i]->id . "'>";
				getGrauDirCursos($dadosDep[$i]->grau);	
				echo "</td>";
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-dircurso').text('" . $dadosDep[$i]->id . "');\"></td>";
				echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-dircurso').text('" . $dadosDep[$i]->id . "');setObservacaoDirCurso();return false;\" ></center></td>";
				echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-dircurso').text('" . $dadosDep[$i]->id . "');apagarDirCurso();return false;\" ></center></td>";   
			echo "</tr>";	
		}		
	}
	
	echo "</tbody></table>";
    echo "<p id='chave-dircurso' hidden></p>";
    echo "</div>";	
}

function transformIntoAcaoDirDepObject($id, $idinv, $query)
{
	$acao = array();
	$cena = explode("DATAINICIO='",$query);
				
	$cena1 = explode("', DATAFIM='",$cena[1]);
	$acao['datainicio'] = $cena1[0];
	
	$cena2 = explode("', NOME='",$cena1[1]);
	$acao['datafim'] = $cena2[0];
	
	$cena3 = explode("', GRAU='",$cena2[1]);
	$acao['nome'] = $cena3[0];
	
	$cena4 = explode("' where",$cena3[1]);
	$acao['grau'] = $cena4[0];
			
	return $acao;
}

function getGrauDirCursos($i) {
	$db = new Database();
	$lValues =$db->getLookupValues("lista_graucursos");
	while ($row = mysql_fetch_assoc($lValues)) {	
		if($i==$row["ID"])
			echo $row["DESCRICAO"];
	}
	$db->disconnect();			
}	
				
function checkGrauDirCursos($id,$i){
	global $dadosDep;
	if($dadosDep[$i]->grau==$id)
		return true;
	else 
		return false;
}

?>