<?php

function showEditAgregacoes($dadosDep)
{
	$db = new Database();
	echo "<h3>Agregações</h3> 
		<div id='agregacoes'>";
		
	echo "<table id='agreg' class='box-table-b'>
			<thead>
				<tr>
					<th>ID Investigador</th>
					<th>Nome</th>
					<th>Unanimidade</th>
					<th colspan='3'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewAgregacao();return false;'></center></th>
				</tr>
			</thead>
			<tbody>";
			
	 foreach ($dadosDep as $i => $value){            
		$checkAcao = $db->checkAcaoExists($dadosDep[$i]->id, $_SESSION['login'], 3);		
		if ($checkAcao) {
			$acao = transformIntoAgregacaoProjectoObject($dadosDep[$i]->id, $dadosDep[$i]->idinv, $checkAcao);
			echo "<tr>
				<td>".$dadosDep[$i]->idinv."</td>";
				echo "<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
				if($dadosDep[$i]->unanimidade == 1)
					echo "<td>Sim</td>";
				else
					echo "<td>Não</td>";
				echo "<td></td><td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-agr').text('" . $dadosDep[$i]->id . "');setObservacaoAgregacao();return false;\" ></center></td>";
				echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-agr').text('" . $dadosDep[$i]->id . "');apagarAgregacao();return false;\" ></center></td>     
			</tr>";
			
			echo "<tr>
				<td style='background:#FFFF33; overflow:hidden;'><img src=\"../../images/arrow_return_down_right.png\" name='navOption'></td>";
				echo "<td style='background:#FFFF33; overflow:hidden;'>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
				if($acao == 1)
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_unanimidade_agr_" .$dadosDep[$i]->id. "'>Sim</td>";
				else
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_unanimidade_agr_" .$dadosDep[$i]->id. "'>Não</td>";
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-agr').text('" . $dadosDep[$i]->id . "');\"></td>
				<td></td>				
				<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"apagarAcao('" . $dadosDep[$i]->id . "',3);return false;\" ></center></td>     
			</tr>";			
		} else {
			echo "<tr>
				<td>".$dadosDep[$i]->idinv."</td>";
				echo "<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
				if($dadosDep[$i]->unanimidade == 1)
					echo "<td id='td_unanimidade_agr_" .$dadosDep[$i]->id. "'>Sim</td>";
				else
					echo "<td id='td_unanimidade_agr_" .$dadosDep[$i]->id. "'>Não</td>";
				
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-agr').text('" . $dadosDep[$i]->id . "');\"></td>";
				echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-agr').text('" . $dadosDep[$i]->id . "');setObservacaoAgregacao();return false;\" ></center></td>";
				echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-agr').text('" . $dadosDep[$i]->id . "');apagarAgregacao();return false;\" ></center></td>     
			</tr>";
		}
	}
	
	echo "</tbody></table>
	<p id='chave-agr' hidden></p>
	</div>";
}

function transformIntoAgregacaoProjectoObject($id, $idinv, $query)
{	
	$cena = explode("UNANIMIDADE=",$query);			

	$cena1 = explode(" where",$cena[1]);
	$unanimidade = $cena1[0];
			
	return $unanimidade;
}

?>
