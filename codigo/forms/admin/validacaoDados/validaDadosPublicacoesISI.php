<?php

function showEditPublicacoesISI($dadosDep) {
		
	echo "<h3>Publicações ISI</h3>\n";
	echo "<div id='publicacoesISI'>";

	$outJISI="";;
	$outJAISI="";
	$outPRPISI="";
	$outPRPNISI="";
	$outCPISI="";
	$outCPNISI="";
	$outOAISI="";
	$outSISI="";
	$outJNISI="";
	$outSNISI="";
	$outLNISI="";
	$outCLNISI="";
	$outLISI="";
	$outCLISI="";
	$outPISI="";
	$outAISI="";
	$outRestoISI="";

	$tcols="<thead>
				<tr>
					<th>IDINV</th>						
					<th>Nome Investigador</th>
					<th>NOMEPUBLICACAO</th>
					<th>ISSN</th>
					<th>ISBN</th>
					<th>TITULO</th>
					<th>AUTORES</th>
					<th>VOLUME</th>
					<th>ISSUE</th>
					<th>AR</th>
					<th>COLECAO</th>
					<th>PRIMPAGINA</th>
					<th>ULTPAGINA</th>
					<th>IFACTOR</th>
					<th>CITACOES</th>
					<th>NPATENTE</th>
					<th>DATAPATENTE</th>
					<th>IPC</th>
					<th>AREA</th>
					<th>ESTADO</th>";

	$tcols_all="<thead>
					<tr>
						<th>ID</th>
						<th>Tipo</th>
						<th>Nome Publicação</th>
						<th>ISSN</th>
						<th>ISSN_LINKING</th>
						<th>ISSN_ELECTRONIC</th>
						<th>ISBN</th>
						<th>Título</th>
						<th>Autores</th>
						<th>VOLUME</th>
						<th>ISSUE</th>
						<th>AR</th>
						<th>Coleção</th>
						<th>1ª Pág.</th>
						<th>Últ. Pág.</th>
						<th>Factor Impacto</th>
						<th>Citações</th>
						<th>Patentes</th>
						<th>Data Patente</th>
						<th>IPC</th>
						<th>Area</th>
						<th>Estado</th>";

	$tcolsJISI="<thead>
					<tr>
						<th>IDINV</th>						
						<th>Nome Investigador</th>
						<th>Revista</th>
						<th>ISSN</th>
						<th>Título</th>
						<th>Autores</th>
						<th>Volume</th>
						<th>Issue</th>
						<th>1ª Pág.</th>
						<th>Última Pág.</th>
						<th>Citações</th>
						<th>Estado</th>";

	$tcolsCONFISI="<thead>
					<tr>
						<th>IDINV</th>	
						<th>Nome Investigador</th>
						<th>ISBN</th>
						<th>Nome Publicação</th>
						<th>Editores</th>
						<th>Coleção</th>
						<th>Volume</th>
						<th>Título do Artigo</th>
						<th>Autores</th>
						<th>1ª Pág.</th>
						<th>Última Página</th>
						<th>Estado</th>";

	$tcolsCLIVISI="<thead>
					<tr>
						<th>ISBN</th>
						<th>Nome Publicação</th>
						<th>Editora</th>		
						<th>Autores</th>
						<th>Título Artigo</th>
						<th>1ª Pág.</th>
						<th>Última Pág.</th>
						<th>Estado</th>";

	$tcolsPATISI="<thead>
					<tr>
						<th>IDINV</th>	
						<th>Nome Investigador</th>
						<th>Nº Patente</th>
						<th>IPC</th>
						<th>Título</th>
						<th>Autores</th>		
						<th>Data da Patente</th>
						<th>Estado</th>";

	$tcolsLIVISI="<thead>
					<tr>
						<th>IDINV</th>	
						<th>Nome Investigador</th>
						<th>ISBN</th>
						<th>Título</th>
						<th>Autores</th>		
						<th>Editor</th>
						<th>Estado</th>";

	foreach ($dadosDep as $i => $value){
			$db = new Database();
			$copia=0;
		
			$tipo=$dadosDep[$i]->tipofmup;
			
			if($dadosDep[$i]->estado==0)
				$estado="Publicado/Published";
			else
				$estado="Em Revisão ou no Prelo/In Review or In Press";
						
			switch($tipo){
				case 'J': { 

					$outJISI=$outJISI."<tr>";
						$outJISI=$outJISI."<td>";
						$outJISI=$outJISI.$dadosDep[$i]->idinv;
						$outJISI=$outJISI."</td>";

						$outJISI=$outJISI."<td>";
						$outJISI=$outJISI.$db->getNomeInvById($dadosDep[$i]->idinv);
						$outJISI=$outJISI."</td>";
						
						$outJISI=$outJISI."<td>";
						$outJISI=$outJISI.$dadosDep[$i]->nomepublicacao;
						$outJISI=$outJISI."</td>";

						$outJISI=$outJISI."<td>";
						$outJISI=$outJISI.$dadosDep[$i]->issn;
						$outJISI=$outJISI."</td>";	

						$outJISI=$outJISI."<td>";
						$outJISI=$outJISI.$dadosDep[$i]->titulo;
						$outJISI=$outJISI."</td>";

						$outJISI=$outJISI."<td>";
						$outJISI=$outJISI.$dadosDep[$i]->autores;
						$outJISI=$outJISI."</td>";

						$outJISI=$outJISI."<td>";
						$outJISI=$outJISI.$dadosDep[$i]->volume;
						$outJISI=$outJISI."</td>";

						$outJISI=$outJISI."<td>";
						$outJISI=$outJISI.$dadosDep[$i]->issue;
						$outJISI=$outJISI."</td>";

						$outJISI=$outJISI."<td>";
						$outJISI=$outJISI.$dadosDep[$i]->primpagina;
						$outJISI=$outJISI."</td>";

						$outJISI=$outJISI."<td>";
						$outJISI=$outJISI.$dadosDep[$i]->ultpagina;
						$outJISI=$outJISI."</td>";
						
						$outJISI=$outJISI."<td>";
						$outJISI=$outJISI.$dadosDep[$i]->citacoes;
						$outJISI=$outJISI."</td>";

						$outJISI=$outJISI."<td>";
						$outJISI=$outJISI.$estado;
						$outJISI=$outJISI."</td>";
						
						$outJISI=$outJISI."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');setObservacaoPublicacaoISI();return false;\" ></center></td>";
						$outJISI=$outJISI."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('9');apagarPublicacao();return false;\" ></center></td>";   
				
					$outJISI=$outJISI."</tr>";
				}
				break;
				case 'PRP': { // Opção de entrada
					$outPRPISI=$outPRPISI."<tr>";
						$outPRPISI=$outPRPISI."<td>";
						$outPRPISI=$outPRPISI.$dadosDep[$i]->idinv;
						$outPRPISI=$outPRPISI."</td>";

						$outPRPISI=$outPRPISI."<td>";
						$outPRPISI=$outPRPISI.$db->getNomeInvById($dadosDep[$i]->idinv);
						$outPRPISI=$outPRPISI."</td>";	
							
						$outPRPISI=$outPRPISI."<td>";
						$outPRPISI=$outPRPISI.$dadosDep[$i]->nomepublicacao;
						$outPRPISI=$outPRPISI."</td>";

						$outPRPISI=$outPRPISI."<td>";
						$outPRPISI=$outPRPISI.$dadosDep[$i]->issn;
						$outPRPISI=$outPRPISI."</td>";

						$outPRPISI=$outPRPISI."<td>";
						$outPRPISI=$outPRPISI.$dadosDep[$i]->titulo;
						$outPRPISI=$outPRPISI."</td>";

						$outPRPISI=$outPRPISI."<td>";
						$outPRPISI=$outPRPISI.$dadosDep[$i]->autores;
						$outPRPISI=$outPRPISI."</td>";

						$outPRPISI=$outPRPISI."<td>";
						$outPRPISI=$outPRPISI.$dadosDep[$i]->volume;
						$outPRPISI=$outPRPISI."</td>";

						$outPRPISI=$outPRPISI."<td>";
						$outPRPISI=$outPRPISI.$dadosDep[$i]->issue;
						$outPRPISI=$outPRPISI."</td>";

						$outPRPISI=$outPRPISI."<td>";
						$outPRPISI=$outPRPISI.$dadosDep[$i]->primpagina;
						$outPRPISI=$outPRPISI."</td>";	

						$outPRPISI=$outPRPISI."<td>";
						$outPRPISI=$outPRPISI.$dadosDep[$i]->ultpagina;
						$outPRPISI=$outPRPISI."</td>";

						$outPRPISI=$outPRPISI."<td>";
						$outPRPISI=$outPRPISI.$dadosDep[$i]->citacoes;
						$outPRPISI=$outPRPISI."</td>";

						$outPRPISI=$outPRPISI."<td>";
						$outPRPISI=$outPRPISI.$estado;
						$outPRPISI=$outPRPISI."</td>";
						
						$outPRPISI=$outPRPISI."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');setObservacaoPublicacaoISI();return false;\" ></center></td>";
						$outPRPISI=$outPRPISI."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('9');apagarPublicacao();return false;\" ></center></td>";   
				
					$outPRPISI=$outPRPISI."</tr>";
				}
				break;
				case 'JA': { // Opção de entrada
					$outJAISI=$outJAISI."<tr>";
						$outJAISI=$outJAISI."<td>";
						$outJAISI=$outJAISI.$dadosDep[$i]->idinv;
						$outJAISI=$outJAISI."</td>";
						
						$outJAISI=$outJAISI."<td>";
						$outJAISI=$outJAISI.$db->getNomeInvById($dadosDep[$i]->idinv);
						$outJAISI=$outJAISI."</td>";	
						
						$outJAISI=$outJAISI."<td>";
						$outJAISI=$outJAISI.$dadosDep[$i]->nomepublicacao;
						$outJAISI=$outJAISI."</td>";

						$outJAISI=$outJAISI."<td>";
						$outJAISI=$outJAISI.$dadosDep[$i]->issn;
						$outJAISI=$outJAISI."</td>";

						$outJAISI=$outJAISI."<td>";
						$outJAISI=$outJAISI.$dadosDep[$i]->titulo;
						$outJAISI=$outJAISI."</td>";

						$outJAISI=$outJAISI."<td>";
						$outJAISI=$outJAISI.$dadosDep[$i]->autores;
						$outJAISI=$outJAISI."</td>";

						$outJAISI=$outJAISI."<td>";
						$outJAISI=$outJAISI.$dadosDep[$i]->volume;
						$outJAISI=$outJAISI."</td>";

						$outJAISI=$outJAISI."<td>";
						$outJAISI=$outJAISI.$dadosDep[$i]->issue;
						$outJAISI=$outJAISI."</td>";

						$outJAISI=$outJAISI."<td>";
						$outJAISI=$outJAISI.$dadosDep[$i]->primpagina;
						$outJAISI=$outJAISI."</td>";

						$outJAISI=$outJAISI."<td>";
						$outJAISI=$outJAISI.$dadosDep[$i]->ultpagina;
						$outJAISI=$outJAISI."</td>";

						$outJAISI=$outJAISI."<td>";
						$outJAISI=$outJAISI.$dadosDep[$i]->citacoes;
						$outJAISI=$outJAISI."</td>";

						$outJAISI=$outJAISI."<td>";
						$outJAISI=$outJAISI.$estado;
						$outJAISI=$outJAISI."</td>";
						
						$outJAISI=$outJAISI."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');setObservacaoPublicacaoISI();return false;\" ></center></td>";
						$outJAISI=$outJAISI."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('9');apagarPublicacao();return false;\" ></center></td>";   
				
					$outJAISI=$outJAISI."</tr>";
				}
				break;
				case 'PRPN': { // Opção de entrada
					$outPRPNISI=$outPRPNISI."<tr>";

						$outPRPNISI=$outPRPNISI."<td>";
						$outPRPNISI=$outPRPNISI.$dadosDep[$i]->idinv;
						$outPRPNISI=$outPRPNISI."</td>";
						
						$outPRPNISI=$outPRPNISI."<td>";
						$outPRPNISI=$outPRPNISI.$db->getNomeInvById($dadosDep[$i]->idinv);
						$outPRPNISI=$outPRPNISI."</td>";	
						
						$outPRPNISI=$outPRPNISI."<td>";
						$outPRPNISI=$outPRPNISI.$dadosDep[$i]->nomepublicacao;
						$outPRPNISI=$outPRPNISI."</td>";

						$outPRPNISI=$outPRPNISI."<td>";
						$outPRPNISI=$outPRPNISI.$dadosDep[$i]->issn;
						$outPRPNISI=$outPRPNISI."</td>";

						$outPRPNISI=$outPRPNISI."<td>";
						$outPRPNISI=$outPRPNISI.$dadosDep[$i]->isbn;
						$outPRPNISI=$outPRPNISI."</td>";

						$outPRPNISI=$outPRPNISI."<td>";
						$outPRPNISI=$outPRPNISI.$dadosDep[$i]->titulo;
						$outPRPNISI=$outPRPNISI."</td>";

						$outPRPNISI=$outPRPNISI."<td>";
						$outPRPNISI=$outPRPNISI.$dadosDep[$i]->autores;
						$outPRPNISI=$outPRPNISI."</td>";

						$outPRPNISI=$outPRPNISI."<td>";
						$outPRPNISI=$outPRPNISI.$dadosDep[$i]->volume;
						$outPRPNISI=$outPRPNISI."</td>";

						$outPRPNISI=$outPRPNISI."<td>";
						$outPRPNISI=$outPRPNISI.$dadosDep[$i]->issue;
						$outPRPNISI=$outPRPNISI."</td>";

						$outPRPNISI=$outPRPNISI."<td>";
						$outPRPNISI=$outPRPNISI.$dadosDep[$i]->ar;
						$outPRPNISI=$outPRPNISI."</td>";

						$outPRPNISI=$outPRPNISI."<td>";
						$outPRPNISI=$outPRPNISI.$dadosDep[$i]->colecao;
						$outPRPNISI=$outPRPNISI."</td>";

						$outPRPNISI=$outPRPNISI."<td>";
						$outPRPNISI=$outPRPNISI.$dadosDep[$i]->primpagina;
						$outPRPNISI=$outPRPNISI."</td>";	

						$outPRPNISI=$outPRPNISI."<td>";
						$outPRPNISI=$outPRPNISI.$dadosDep[$i]->ultpagina;
						$outPRPNISI=$outPRPNISI."</td>";

						$outPRPNISI=$outPRPNISI."<td>";
						$outPRPNISI=$outPRPNISI.$dadosDep[$i]->citacoes;
						$outPRPNISI=$outPRPNISI."</td>";

						$outPRPNISI=$outPRPNISI."<td>";
						$outPRPNISI=$outPRPNISI.$dadosDep[$i]->conftitle;
						$outPRPNISI=$outPRPNISI."</td>";

						$outPRPNISI=$outPRPNISI."<td>";
						$outPRPNISI=$outPRPNISI.$dadosDep[$i]->language;
						$outPRPNISI=$outPRPNISI."</td>";

						$outPRPNISI=$outPRPNISI."<td>";
						$outPRPNISI=$outPRPNISI.$dadosDep[$i]->editor;
						$outPRPNISI=$outPRPNISI."</td>";

						$outPRPNISI=$outPRPNISI."<td>";
						$outPRPNISI=$outPRPNISI.$estado;
						$outPRPNISI=$outPRPNISI."</td>";
						
						$outPRPNISI=$outPRPNISI."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');setObservacaoPublicacaoISI();return false;\" ></center></td>";
						$outPRPNISI=$outPRPNISI."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('9');apagarPublicacao();return false;\" ></center></td>";   
				
					$outPRPNISI=$outPRPNISI."</tr>";
				}
				break;
				case 'CP': { // Opção de entrada
					$outCPISI=$outCPISI."<tr>";
						$outCPISI=$outCPISI."<td>";
						$outCPISI=$outCPISI.$dadosDep[$i]->idinv;
						$outCPISI=$outCPISI."</td>";
						
						$outCPISI=$outCPISI."<td>";
						$outCPISI=$outCPISI.$db->getNomeInvById($dadosDep[$i]->idinv);
						$outCPISI=$outCPISI."</td>";	

						$outCPISI=$outCPISI."<td>";
						$outCPISI=$outCPISI.$dadosDep[$i]->isbn;
						$outCPISI=$outCPISI."</td>";

						$outCPISI=$outCPISI."<td>";
						$outCPISI=$outCPISI.$dadosDep[$i]->nomepublicacao;
						$outCPISI=$outCPISI."</td>";

						$outCPISI=$outCPISI."<td>";
						$outCPISI=$outCPISI.$dadosDep[$i]->editor;
						$outCPISI=$outCPISI."</td>";

						$outCPISI=$outCPISI."<td>";
						$outCPISI=$outCPISI.$dadosDep[$i]->colecao;
						$outCPISI=$outCPISI."</td>";

						$outCPISI=$outCPISI."<td>";
						$outCPISI=$outCPISI.$dadosDep[$i]->volume;
						$outCPISI=$outCPISI."</td>";

						$outCPISI=$outCPISI."<td>";
						$outCPISI=$outCPISI.$dadosDep[$i]->titulo;
						$outCPISI=$outCPISI."</td>";

						$outCPISI=$outCPISI."<td>";
						$outCPISI=$outCPISI.$dadosDep[$i]->autores;
						$outCPISI=$outCPISI."</td>";

						$outCPISI=$outCPISI."<td>";
						$outCPISI=$outCPISI.$dadosDep[$i]->primpagina;
						$outCPISI=$outCPISI."</td>";

						$outCPISI=$outCPISI."<td>";
						$outCPISI=$outCPISI.$dadosDep[$i]->ultpagina;
						$outCPISI=$outCPISI."</td>";			    		
						
						$outCPISI=$outCPISI."<td>";
						$outCPISI=$outCPISI.$estado;
						$outCPISI=$outCPISI."</td>";
						
						$outCPISI=$outCPISI."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');setObservacaoPublicacaoISI();return false;\" ></center></td>";
						$outCPISI=$outCPISI."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('9');apagarPublicacao();return false;\" ></center></td>";   
						
					$outCPISI=$outCPISI."</tr>";
				}
				break;
				case 'CPN': { // Opção de entrada
					$outCPNISI=$outCPNISI."<tr>";
						$outCPNISI=$outCPNISI."<td>";
						$outCPNISI=$outCPNISI.$dadosDep[$i]->idinv;
						$outCPNISI=$outCPNISI."</td>";
						
						$outCPNISI=$outCPNISI."<td>";
						$outCPNISI=$outCPNISI.$db->getNomeInvById($dadosDep[$i]->idinv);
						$outCPNISI=$outCPNISI."</td>";	

						$outCPNISI=$outCPNISI."<td>";
						$outCPNISI=$outCPNISI.$dadosDep[$i]->nomepublicacao;
						$outCPNISI=$outCPNISI."</td>";

						$outCPNISI=$outCPNISI."<td>";
						$outCPNISI=$outCPNISI.$dadosDep[$i]->issn;
						$outCPNISI=$outCPNISI."</td>";
						
						$outCPNISI=$outCPNISI."<td>";
						$outCPNISI=$outCPNISI.$dadosDep[$i]->isbn;
						$outCPNISI=$outCPNISI."</td>";

						$outCPNISI=$outCPNISI."<td>";
						$outCPNISI=$outCPNISI.$dadosDep[$i]->titulo;
						$outCPNISI=$outCPNISI."</td>";

						$outCPNISI=$outCPNISI."<td>";
						$outCPNISI=$outCPNISI.$dadosDep[$i]->autores;
						$outCPNISI=$outCPNISI."</td>";

						$outCPNISI=$outCPNISI."<td>";
						$outCPNISI=$outCPNISI.$dadosDep[$i]->volume;
						$outCPNISI=$outCPNISI."</td>";

						$outCPNISI=$outCPNISI."<td>";
						$outCPNISI=$outCPNISI.$dadosDep[$i]->issue;
						$outCPNISI=$outCPNISI."</td>";

						$outCPNISI=$outCPNISI."<td>";
						$outCPNISI=$outCPNISI.$dadosDep[$i]->ar;
						$outCPNISI=$outCPNISI."</td>";

						$outCPNISI=$outCPNISI."<td>";
						$outCPNISI=$outCPNISI.$dadosDep[$i]->colecao;
						$outCPNISI=$outCPNISI."</td>";

						$outCPNISI=$outCPNISI."<td>";
						$outCPNISI=$outCPNISI.$dadosDep[$i]->primpagina;
						$outCPNISI=$outCPNISI."</td>";	

						$outCPNISI=$outCPNISI."<td>";
						$outCPNISI=$outCPNISI.$dadosDep[$i]->ultpagina;
						$outCPNISI=$outCPNISI."</td>";
						
						$outCPNISI=$outCPNISI."<td>";
						$outCPNISI=$outCPNISI.$dadosDep[$i]->citacoes;
						$outCPNISI=$outCPNISI."</td>";

						$outCPNISI=$outCPNISI."<td>";
						$outCPNISI=$outCPNISI.$dadosDep[$i]->conftitle;
						$outCPNISI=$outCPNISI."</td>";

						$outCPNISI=$outCPNISI."<td>";
						$outCPNISI=$outCPNISI.$dadosDep[$i]->language;
						$outCPNISI=$outCPNISI."</td>";

						$outCPNISI=$outCPNISI."<td>";
						$outCPNISI=$outCPNISI.$dadosDep[$i]->editor;
						$outCPNISI=$outCPNISI."</td>";	

						$outCPNISI=$outCPNISI."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');setObservacaoPublicacaoISI();return false;\" ></center></td>";
						$outCPNISI=$outCPNISI."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('9');apagarPublicacao();return false;\" ></center></td>";   
						
					$outCPNISI=$outCPNISI."</tr>";
				}
				break;
				case 'OA': { // Opção de entrada
					$outOAISI=$outOAISI."<tr>";
						$outOAISI=$outOAISI."<td>";
						$outOAISI=$outOAISI.$dadosDep[$i]->idinv;
						$outOAISI=$outOAISI."</td>";
						
						$outOAISI=$outOAISI."<td>";
						$outOAISI=$outOAISI.$db->getNomeInvById($dadosDep[$i]->idinv);
						$outOAISI=$outOAISI."</td>";	

						$outOAISI=$outOAISI."<td>";
						$outOAISI=$outOAISI.$dadosDep[$i]->nomepublicacao;
						$outOAISI=$outOAISI."</td>";

						$outOAISI=$outOAISI."<td>";
						$outOAISI=$outOAISI.$dadosDep[$i]->isbn;
						$outOAISI=$outOAISI."</td>";

						$outOAISI=$outOAISI."<td>";
						$outOAISI=$outOAISI.$dadosDep[$i]->editor;
						$outOAISI=$outOAISI."</td>";

						$outOAISI=$outOAISI."<td>";
						$outOAISI=$outOAISI.$dadosDep[$i]->colecao;
						$outOAISI=$outOAISI."</td>";

						$outOAISI=$outOAISI."<td>";
						$outOAISI=$outOAISI.$dadosDep[$i]->volume;
						$outOAISI=$outOAISI."</td>";

						$outOAISI=$outOAISI."<td>";
						$outOAISI=$outOAISI.$dadosDep[$i]->titulo;
						$outOAISI=$outOAISI."</td>";

						$outOAISI=$outOAISI."<td>";
						$outOAISI=$outOAISI.$dadosDep[$i]->autores;
						$outOAISI=$outOAISI."</td>";

						$outOAISI=$outOAISI."<td>";
						$outOAISI=$outOAISI.$dadosDep[$i]->primpagina;
						$outOAISI=$outOAISI."</td>";

						$outOAISI=$outOAISI."<td>";
						$outOAISI=$outOAISI.$dadosDep[$i]->ultpagina;
						$outOAISI=$outOAISI."</td>";

						$outOAISI=$outOAISI."<td>";
						$outOAISI=$outOAISI.$estado;
						$outOAISI=$outOAISI."</td>";	

						$outOAISI=$outOAISI."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');setObservacaoPublicacaoISI();return false;\" ></center></td>";
						$outOAISI=$outOAISI."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('9');apagarPublicacao();return false;\" ></center></td>";   
												
					$outOAISI=$outOAISI."</tr>";
				}
				break;
				case 'S': { // Opção de entrada
					$outSISI=$outSISI."<tr>";
						$outSISI=$outSISI."<td>";
						$outSISI=$outSISI.$dadosDep[$i]->idinv;
						$outSISI=$outSISI."</td>";
						
						$outSISI=$outSISI."<td>";
						$outSISI=$outSISI.$db->getNomeInvById($dadosDep[$i]->idinv);
						$outSISI=$outSISI."</td>";	
						
						$outSISI=$outSISI."<td>";
						$outSISI=$outSISI.$dadosDep[$i]->nomepublicacao;
						$outSISI=$outSISI."</td>";

						$outSISI=$outSISI."<td>";
						$outSISI=$outSISI.$dadosDep[$i]->issn;
						$outSISI=$outSISI."</td>";
						
						$outSISI=$outSISI."<td>";
						$outSISI=$outSISI.$dadosDep[$i]->isbn;
						$outSISI=$outSISI."</td>";

						$outSISI=$outSISI."<td>";
						$outSISI=$outSISI.$dadosDep[$i]->titulo;
						$outSISI=$outSISI."</td>";

						$outSISI=$outSISI."<td>";
						$outSISI=$outSISI.$dadosDep[$i]->autores;
						$outSISI=$outSISI."</td>";

						$outSISI=$outSISI."<td>";
						$outSISI=$outSISI.$dadosDep[$i]->volume;
						$outSISI=$outSISI."</td>";

						$outSISI=$outSISI."<td>";
						$outSISI=$outSISI.$dadosDep[$i]->issue;
						$outSISI=$outSISI."</td>";

						$outSISI=$outSISI."<td>";
						$outSISI=$outSISI.$dadosDep[$i]->ar;
						$outSISI=$outSISI."</td>";

						$outSISI=$outSISI."<td>";
						$outSISI=$outSISI.$dadosDep[$i]->colecao;
						$outSISI=$outSISI."</td>";

						$outSISI=$outSISI."<td>";
						$outSISI=$outSISI.$dadosDep[$i]->primpagina;
						$outSISI=$outSISI."</td>";	

						$outSISI=$outSISI."<td>";
						$outSISI=$outSISI.$dadosDep[$i]->ultpagina;
						$outSISI=$outSISI."</td>";
						
						$outSISI=$outSISI."<td>";
						$outSISI=$outSISI.$dadosDep[$i]->citacoes;
						$outSISI=$outSISI."</td>";

						$outSISI=$outSISI."<td>";
						$outSISI=$outSISI.$dadosDep[$i]->conftitle;
						$outSISI=$outSISI."</td>";

						$outSISI=$outSISI."<td>";
						$outSISI=$outSISI.$dadosDep[$i]->language;
						$outSISI=$outSISI."</td>";

						$outSISI=$outSISI."<td>";
						$outSISI=$outSISI.$dadosDep[$i]->editor;
						$outSISI=$outSISI."</td>";	

						$outSISI=$outSISI."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');setObservacaoPublicacaoISI();return false;\" ></center></td>";
						$outSISI=$outSISI."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('9');apagarPublicacao();return false;\" ></center></td>";   
						
					$outSISI=$outSISI."</tr>";
				}
				break;
				case 'JN': { // Opção de entrada
					$outJNISI=$outJNISI."<tr>";
						$outJNISI=$outJNISI."<td>";
						$outJNISI=$outJNISI.$dadosDep[$i]->idinv;
						$outJNISI=$outJNISI."</td>";
						
						$outJNISI=$outJNISI."<td>";
						$outJNISI=$outJNISI.$db->getNomeInvById($dadosDep[$i]->idinv);
						$outJNISI=$outJNISI."</td>";	
						
						$outJNISI=$outJNISI."<td>";
						$outJNISI=$outJNISI.$dadosDep[$i]->nomepublicacao;
						$outJNISI=$outJNISI."</td>";

						$outJNISI=$outJNISI."<td>";
						$outJNISI=$outJNISI.$dadosDep[$i]->issn;
						$outJNISI=$outJNISI."</td>";
						
						$outJNISI=$outJNISI."<td>";
						$outJNISI=$outJNISI.$dadosDep[$i]->isbn;
						$outJNISI=$outJNISI."</td>";

						$outJNISI=$outJNISI."<td>";
						$outJNISI=$outJNISI.$dadosDep[$i]->titulo;
						$outJNISI=$outJNISI."</td>";

						$outJNISI=$outJNISI."<td>";
						$outJNISI=$outJNISI.$dadosDep[$i]->autores;
						$outJNISI=$outJNISI."</td>";

						$outJNISI=$outJNISI."<td>";
						$outJNISI=$outJNISI.$dadosDep[$i]->volume;
						$outJNISI=$outJNISI."</td>";

						$outJNISI=$outJNISI."<td>";
						$outJNISI=$outJNISI.$dadosDep[$i]->issue;
						$outJNISI=$outJNISI."</td>";

						$outJNISI=$outJNISI."<td>";
						$outJNISI=$outJNISI.$dadosDep[$i]->ar;
						$outJNISI=$outJNISI."</td>";

						$outJNISI=$outJNISI."<td>";
						$outJNISI=$outJNISI.$dadosDep[$i]->colecao;
						$outJNISI=$outJNISI."</td>";

						$outJNISI=$outJNISI."<td>";
						$outJNISI=$outJNISI.$dadosDep[$i]->primpagina;
						$outJNISI=$outJNISI."</td>";

						$outJNISI=$outJNISI."<td>";
						$outJNISI=$outJNISI.$dadosDep[$i]->ultpagina;
						$outJNISI=$outJNISI."</td>";			    		

						$outJNISI=$outJNISI."<td>";
						$outJNISI=$outJNISI.$dadosDep[$i]->citacoes;
						$outJNISI=$outJNISI."</td>";

						$outJNISI=$outJNISI."<td>";
						$outJNISI=$outJNISI.$dadosDep[$i]->conftitle;
						$outJNISI=$outJNISI."</td>";

						$outJNISI=$outJNISI."<td>";
						$outJNISI=$outJNISI.$dadosDep[$i]->language;
						$outJNISI=$outJNISI."</td>";

						$outJNISI=$outJNISI."<td>";
						$outJNISI=$outJNISI.$dadosDep[$i]->editor;
						$outJNISI=$outJNISI."</td>";

						$outJNISI=$outJNISI."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');setObservacaoPublicacaoISI();return false;\" ></center></td>";
						$outJNISI=$outJNISI."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('9');apagarPublicacao();return false;\" ></center></td>";   
						
					$outJNISI=$outJNISI."</tr>";
				}
				break;
				case 'SN': { // Opção de entrada
					$outSNISI=$outSNISI."<tr>";
						$outSNISI=$outSNISI."<td>";
						$outSNISI=$outSNISI.$dadosDep[$i]->idinv;
						$outSNISI=$outSNISI."</td>";
						
						$outSNISI=$outSNISI."<td>";
						$outSNISI=$outSNISI.$db->getNomeInvById($dadosDep[$i]->idinv);
						$outSNISI=$outSNISI."</td>";	

						$outSNISI=$outSNISI."<td>";
						$outSNISI=$outSNISI.$dadosDep[$i]->nomepublicacao;
						$outSNISI=$outSNISI."</td>";

						$outSNISI=$outSNISI."<td>";
						$outSNISI=$outSNISI.$dadosDep[$i]->issn;
						$outSNISI=$outSNISI."</td>";
						
						$outSNISI=$outSNISI."<td>";
						$outSNISI=$outSNISI.$dadosDep[$i]->isbn;
						$outSNISI=$outSNISI."</td>";

						$outSNISI=$outSNISI."<td>";
						$outSNISI=$outSNISI.$dadosDep[$i]->titulo;
						$outSNISI=$outSNISI."</td>";

						$outSNISI=$outSNISI."<td>";
						$outSNISI=$outSNISI.$dadosDep[$i]->autores;
						$outSNISI=$outSNISI."</td>";

						$outSNISI=$outSNISI."<td>";
						$outSNISI=$outSNISI.$dadosDep[$i]->volume;
						$outSNISI=$outSNISI."</td>";

						$outSNISI=$outSNISI."<td>";
						$outSNISI=$outSNISI.$dadosDep[$i]->issue;
						$outSNISI=$outSNISI."</td>";

						$outSNISI=$outSNISI."<td>";
						$outSNISI=$outSNISI.$dadosDep[$i]->ar;
						$outSNISI=$outSNISI."</td>";

						$outSNISI=$outSNISI."<td>";
						$outSNISI=$outSNISI.$dadosDep[$i]->colecao;
						$outSNISI=$outSNISI."</td>";

						$outSNISI=$outSNISI."<td>";
						$outSNISI=$outSNISI.$dadosDep[$i]->primpagina;
						$outSNISI=$outSNISI."</td>";	

						$outSNISI=$outSNISI."<td>";
						$outSNISI=$outSNISI.$dadosDep[$i]->ultpagina;
						$outSNISI=$outSNISI."</td>";
						
						$outSNISI=$outSNISI."<td>";
						$outSNISI=$outSNISI.$dadosDep[$i]->citacoes;
						$outSNISI=$outSNISI."</td>";

						$outSNISI=$outSNISI."<td>";
						$outSNISI=$outSNISI.$dadosDep[$i]->conftitle;
						$outSNISI=$outSNISI."</td>";

						$outSNISI=$outSNISI."<td>";
						$outSNISI=$outSNISI.$dadosDep[$i]->language;
						$outSNISI=$outSNISI."</td>";

						$outSNISI=$outSNISI."<td>";
						$outSNISI=$outSNISI.$dadosDep[$i]->editor;
						$outSNISI=$outSNISI."</td>";

						$outSNISI=$outSNISI."<td>";
						$outSNISI=$outSNISI.$dadosDep[$i]->tipofmup;
						$outSNISI=$outSNISI."</td>";	

						$outSNISI=$outSNISI."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');setObservacaoPublicacaoISI();return false;\" ></center></td>";
						$outSNISI=$outSNISI."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('9');apagarPublicacao();return false;\" ></center></td>";   
						
					$outSNISI=$outSNISI."</tr>";
				}
				break;
				case 'LN': { // Opção de entrada
					$outLNISI=$outLNISI."<tr>";
						$outLNISI=$outLNISI."<td>";
						$outLNISI=$outLNISI.$dadosDep[$i]->idinv;
						$outLNISI=$outLNISI."</td>";
						
						$outLNISI=$outLNISI."<td>";
						$outLNISI=$outLNISI.$db->getNomeInvById($dadosDep[$i]->idinv);
						$outLNISI=$outLNISI."</td>";

						$outLNISI=$outLNISI."<td>";
						$outLNISI=$outLNISI.$dadosDep[$i]->nomepublicacao;
						$outLNISI=$outLNISI."</td>";

						$outLNISI=$outLNISI."<td>";
						$outLNISI=$outLNISI.$dadosDep[$i]->issn;
						$outLNISI=$outLNISI."</td>";

						$outLNISI=$outLNISI."<td>";
						$outLNISI=$outLNISI.$dadosDep[$i]->isbn;
						$outLNISI=$outLNISI."</td>";

						$outLNISI=$outLNISI."<td>";
						$outLNISI=$outLNISI.$dadosDep[$i]->titulo;
						$outLNISI=$outLNISI."</td>";

						$outLNISI=$outLNISI."<td>";
						$outLNISI=$outLNISI.$dadosDep[$i]->autores;
						$outLNISI=$outLNISI."</td>";

						$outLNISI=$outLNISI."<td>";
						$outLNISI=$outLNISI.$dadosDep[$i]->volume;
						$outLNISI=$outLNISI."</td>";

						$outLNISI=$outLNISI."<td>";
						$outLNISI=$outLNISI.$dadosDep[$i]->issue;
						$outLNISI=$outLNISI."</td>";

						$outLNISI=$outLNISI."<td>";
						$outLNISI=$outLNISI.$dadosDep[$i]->ar;
						$outLNISI=$outLNISI."</td>";

						$outLNISI=$outLNISI."<td>";
						$outLNISI=$outLNISI.$dadosDep[$i]->colecao;
						$outLNISI=$outLNISI."</td>";

						$outLNISI=$outLNISI."<td>";
						$outLNISI=$outLNISI.$dadosDep[$i]->primpagina;
						$outLNISI=$outLNISI."</td>";

						$outLNISI=$outLNISI."<td>";
						$outLNISI=$outLNISI.$dadosDep[$i]->ultpagina;
						$outLNISI=$outLNISI."</td>";

						$outLNISI=$outLNISI."<td>";
						$outLNISI=$outLNISI.$dadosDep[$i]->citacoes;
						$outLNISI=$outLNISI."</td>";
						
						$outLNISI=$outLNISI."<td>";
						$outLNISI=$outLNISI.$dadosDep[$i]->conftitle;
						$outLNISI=$outLNISI."</td>";

						$outLNISI=$outLNISI."<td>";
						$outLNISI=$outLNISI.$dadosDep[$i]->language;
						$outLNISI=$outLNISI."</td>";

						$outLNISI=$outLNISI."<td>";
						$outLNISI=$outLNISI.$dadosDep[$i]->editor;
						$outLNISI=$outLNISI."</td>";

						$outLNISI=$outLNISI."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');setObservacaoPublicacaoISI();return false;\" ></center></td>";
						$outLNISI=$outLNISI."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('9');apagarPublicacao();return false;\" ></center></td>";   
						
					$outLNISI=$outLNISI."</tr>";
				}
				break;
				case 'CLN': { // Opção de entrada
					$outCLNISI=$outCLNISI."<tr>";
						$outCLNISI=$outCLNISI."<td>";
						$outCLNISI=$outCLNISI.$dadosDep[$i]->idinv;
						$outCLNISI=$outCLNISI."</td>";
						
						$outCLNISI=$outCLNISI."<td>";
						$outCLNISI=$outCLNISI.$db->getNomeInvById($dadosDep[$i]->idinv);
						$outCLNISI=$outCLNISI."</td>";

						$outCLNISI=$outCLNISI."<td>";
						$outCLNISI=$outCLNISI.$dadosDep[$i]->nomepublicacao;
						$outCLNISI=$outCLNISI."</td>";

						$outCLNISI=$outCLNISI."<td>";
						$outCLNISI=$outCLNISI.$dadosDep[$i]->issn;
						$outCLNISI=$outCLNISI."</td>";
						
						$outCLNISI=$outCLNISI."<td>";
						$outCLNISI=$outCLNISI.$dadosDep[$i]->isbn;
						$outCLNISI=$outCLNISI."</td>";

						$outCLNISI=$outCLNISI."<td>";
						$outCLNISI=$outCLNISI.$dadosDep[$i]->titulo;
						$outCLNISI=$outCLNISI."</td>";

						$outCLNISI=$outCLNISI."<td>";
						$outCLNISI=$outCLNISI.$dadosDep[$i]->autores;
						$outCLNISI=$outCLNISI."</td>";

						$outCLNISI=$outCLNISI."<td>";
						$outCLNISI=$outCLNISI.$dadosDep[$i]->volume;
						$outCLNISI=$outCLNISI."</td>";

						$outCLNISI=$outCLNISI."<td>";
						$outCLNISI=$outCLNISI.$dadosDep[$i]->issue;
						$outCLNISI=$outCLNISI."</td>";

						$outCLNISI=$outCLNISI."<td>";
						$outCLNISI=$outCLNISI.$dadosDep[$i]->ar;
						$outCLNISI=$outCLNISI."</td>";

						$outCLNISI=$outCLNISI."<td>";
						$outCLNISI=$outCLNISI.$dadosDep[$i]->colecao;
						$outCLNISI=$outCLNISI."</td>";

						$outCLNISI=$outCLNISI."<td>";
						$outCLNISI=$outCLNISI.$dadosDep[$i]->primpagina;
						$outCLNISI=$outCLNISI."</td>";	

						$outCLNISI=$outCLNISI."<td>";
						$outCLNISI=$outCLNISI.$dadosDep[$i]->ultpagina;
						$outCLNISI=$outCLNISI."</td>";

						$outCLNISI=$outCLNISI."<td>";
						$outCLNISI=$outCLNISI.$dadosDep[$i]->citacoes;
						$outCLNISI=$outCLNISI."</td>";
						
						$outCLNISI=$outCLNISI."<td>";
						$outCLNISI=$outCLNISI.$dadosDep[$i]->conftitle;
						$outCLNISI=$outCLNISI."</td>";

						$outCLNISI=$outCLNISI."<td>";
						$outCLNISI=$outCLNISI.$dadosDep[$i]->language;
						$outCLNISI=$outCLNISI."</td>";

						$outCLNISI=$outCLNISI."<td>";
						$outCLNISI=$outCLNISI.$dadosDep[$i]->editor;
						$outCLNISI=$outCLNISI."</td>";	

						$outCLNISI=$outCLNISI."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');setObservacaoPublicacaoISI();return false;\" ></center></td>";
						$outCLNISI=$outCLNISI."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('9');apagarPublicacao();return false;\" ></center></td>";   
												
					$outCLNISI=$outCLNISI."</tr>";
				}
				break;
				case 'L': { // Opção de entrada
					$outLISI=$outLISI."<tr>";

						$outLISI=$outLISI."<td>";
						$outLISI=$outLISI.$dadosDep[$i]->idinv;
						$outLISI=$outLISI."</td>";
						
						$outLISI=$outLISI."<td>";
						$outLISI=$outLISI.$db->getNomeInvById($dadosDep[$i]->idinv);
						$outLISI=$outLISI."</td>";

						$outLISI=$outLISI."<td>";
						$outLISI=$outLISI.$dadosDep[$i]->nomepublicacao;
						$outLISI=$outLISI."</td>";

						$outLISI=$outLISI."<td>";
						$outLISI=$outLISI.$dadosDep[$i]->issn;
						$outLISI=$outLISI."</td>";
						
						$outLISI=$outLISI."<td>";
						$outLISI=$outLISI.$dadosDep[$i]->isbn;
						$outLISI=$outLISI."</td>";

						$outLISI=$outLISI."<td>";
						$outLISI=$outLISI.$dadosDep[$i]->titulo;
						$outLISI=$outLISI."</td>";

						$outLISI=$outLISI."<td>";
						$outLISI=$outLISI.$dadosDep[$i]->autores;
						$outLISI=$outLISI."</td>";

						$outLISI=$outLISI."<td>";
						$outLISI=$outLISI.$dadosDep[$i]->volume;
						$outLISI=$outLISI."</td>";

						$outLISI=$outLISI."<td>";
						$outLISI=$outLISI.$dadosDep[$i]->issue;
						$outLISI=$outLISI."</td>";

						$outLISI=$outLISI."<td>";
						$outLISI=$outLISI.$dadosDep[$i]->ar;
						$outLISI=$outLISI."</td>";

						$outLISI=$outLISI."<td>";
						$outLISI=$outLISI.$dadosDep[$i]->colecao;
						$outLISI=$outLISI."</td>";

						$outLISI=$outLISI."<td>";
						$outLISI=$outLISI.$dadosDep[$i]->primpagina;
						$outLISI=$outLISI."</td>";	

						$outLISI=$outLISI."<td>";
						$outLISI=$outLISI.$dadosDep[$i]->ultpagina;
						$outLISI=$outLISI."</td>";

						$outLISI=$outLISI."<td>";
						$outLISI=$outLISI.$dadosDep[$i]->citacoes;
						$outLISI=$outLISI."</td>";
						
						$outLISI=$outLISI."<td>";
						$outLISI=$outLISI.$dadosDep[$i]->conftitle;
						$outLISI=$outLISI."</td>";

						$outLISI=$outLISI."<td>";
						$outLISI=$outLISI.$dadosDep[$i]->language;
						$outLISI=$outLISI."</td>";

						$outLISI=$outLISI."<td>";
						$outLISI=$outLISI.$dadosDep[$i]->editor;
						$outLISI=$outLISI."</td>";
									
						$outLISI=$outLISI."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');setObservacaoPublicacaoISI();return false;\" ></center></td>";
						$outLISI=$outLISI."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('9');apagarPublicacao();return false;\" ></center></td>";   
									
					$outLISI=$outLISI."</tr>";
				}
				break;
				case 'CL': { // Opção de entrada
					$outCLISI=$outCLISI."<tr>";

						$outCLISI=$outCLISI."<td>";
						$outCLISI=$outCLISI.$dadosDep[$i]->idinv;
						$outCLISI=$outCLISI."</td>";
						
						$outCLISI=$outCLISI."<td>";
						$outCLISI=$outCLISI.$db->getNomeInvById($dadosDep[$i]->idinv);
						$outCLISI=$outCLISI."</td>";	
													
						$outCLISI=$outCLISI."<td>";
						$outCLISI=$outCLISI.$dadosDep[$i]->nomepublicacao;
						$outCLISI=$outCLISI."</td>";

						$outCLISI=$outCLISI."<td>";
						$outCLISI=$outCLISI.$dadosDep[$i]->issn;
						$outCLISI=$outCLISI."</td>";

						$outCLISI=$outCLISI."<td>";
						$outCLISI=$outCLISI.$dadosDep[$i]->isbn;
						$outCLISI=$outCLISI."</td>";

						$outCLISI=$outCLISI."<td>";
						$outCLISI=$outCLISI.$dadosDep[$i]->titulo;
						$outCLISI=$outCLISI."</td>";

						$outCLISI=$outCLISI."<td>";
						$outCLISI=$outCLISI.$dadosDep[$i]->autores;
						$outCLISI=$outCLISI."</td>";

						$outCLISI=$outCLISI."<td>";
						$outCLISI=$outCLISI.$dadosDep[$i]->volume;
						$outCLISI=$outCLISI."</td>";

						$outCLISI=$outCLISI."<td>";
						$outCLISI=$outCLISI.$dadosDep[$i]->issue;
						$outCLISI=$outCLISI."</td>";

						$outCLISI=$outCLISI."<td>";
						$outCLISI=$outCLISI.$dadosDep[$i]->ar;
						$outCLISI=$outCLISI."</td>";

						$outCLISI=$outCLISI."<td>";
						$outCLISI=$outCLISI.$dadosDep[$i]->colecao;
						$outCLISI=$outCLISI."</td>";

						$outCLISI=$outCLISI."<td>";
						$outCLISI=$outCLISI.$dadosDep[$i]->primpagina;
						$outCLISI=$outCLISI."</td>";	

						$outCLISI=$outCLISI."<td>";
						$outCLISI=$outCLISI.$dadosDep[$i]->ultpagina;
						$outCLISI=$outCLISI."</td>";

						$outCLISI=$outCLISI."<td>";
						$outCLISI=$outCLISI.$dadosDep[$i]->citacoes;
						$outCLISI=$outCLISI."</td>";

						$outCLISI=$outCLISI."<td>";
						$outCLISI=$outCLISI.$dadosDep[$i]->ifactor;
						$outCLISI=$outCLISI."</td>";

						$outCLISI=$outCLISI."<td>";
						$outCLISI=$outCLISI.$dadosDep[$i]->conftitle;
						$outCLISI=$outCLISI."</td>";

						$outCLISI=$outCLISI."<td>";
						$outCLISI=$outCLISI.$dadosDep[$i]->language;
						$outCLISI=$outCLISI."</td>";

						$outCLISI=$outCLISI."<td>";
						$outCLISI=$outCLISI.$dadosDep[$i]->editor;
						$outCLISI=$outCLISI."</td>";

						$outCLISI=$outCLISI."<td>";
						$outCLISI=$outCLISI.getTipoEstado($dadosDep[$i]->estado);
						$outCLISI=$outCLISI."</td>";	
						
						$outCLISI=$outCLISI."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');setObservacaoPublicacaoISI();return false;\" ></center></td>";
						$outCLISI=$outCLISI."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('9');apagarPublicacao();return false;\" ></center></td>";   

					$outCLISI=$outCLISI."</tr>";
				}
				break;
				case 'P': { // Opção de entrada
					$outPISI=$outPISI."<tr>";
						$outPISI=$outPISI."<td>";
						$outPISI=$outPISI.$dadosDep[$i]->idinv;
						$outPISI=$outPISI."</td>";
						
						$outPISI=$outPISI."<td>";
						$outPISI=$outPISI.$db->getNomeInvById($dadosDep[$i]->idinv);
						$outPISI=$outPISI."</td>";

						$outPISI=$outPISI."<td>";
						$outPISI=$outPISI.$dadosDep[$i]->npatente;
						$outPISI=$outPISI."</td>";

						$outPISI=$outPISI."<td>";
						$outPISI=$outPISI.$dadosDep[$i]->ipc;
						$outPISI=$outPISI."</td>";	

						$outPISI=$outPISI."<td>";
						$outPISI=$outPISI.$dadosDep[$i]->titulo;
						$outPISI=$outPISI."</td>";	

						$outPISI=$outPISI."<td>";
						$outPISI=$outPISI.$dadosDep[$i]->autores;
						$outPISI=$outPISI."</td>";

						$outPISI=$outPISI."<td>";
						$outPISI=$outPISI.$dadosDep[$i]->datapatente;
						$outPISI=$outPISI."</td>";	

						$outPISI=$outPISI."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');setObservacaoPublicacaoISI();return false;\" ></center></td>";
						$outPISI=$outPISI."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('9');apagarPublicacao();return false;\" ></center></td>";   
	
					$outPISI=$outPISI."</tr>";
				}
				break;
				case 'A': { // Opção de entrada
					$outAISI=$outAISI."<tr>";
						$outAISI=$outAISI."<td>";
						$outAISI=$outAISI.$dadosDep[$i]->idinv;
						$outAISI=$outAISI."</td>";
						
						$outAISI=$outAISI."<td>";
						$outAISI=$outAISI.$db->getNomeInvById($dadosDep[$i]->idinv);
						$outAISI=$outAISI."</td>";
						
						$outAISI=$outAISI."<td>";
						$outAISI=$outAISI.$dadosDep[$i]->nomepublicacao;
						$outAISI=$outAISI."</td>";

						$outAISI=$outAISI."<td>";
						$outAISI=$outAISI.$dadosDep[$i]->issn;
						$outAISI=$outAISI."</td>";
						
						$outAISI=$outAISI."<td>";
						$outAISI=$outAISI.$dadosDep[$i]->isbn;
						$outAISI=$outAISI."</td>";

						$outAISI=$outAISI."<td>";
						$outAISI=$outAISI.$dadosDep[$i]->titulo;
						$outAISI=$outAISI."</td>";

						$outAISI=$outAISI."<td>";
						$outAISI=$outAISI.$dadosDep[$i]->autores;
						$outAISI=$outAISI."</td>";

						$outAISI=$outAISI."<td>";
						$outAISI=$outAISI.$dadosDep[$i]->volume;
						$outAISI=$outAISI."</td>";

						$outAISI=$outAISI."<td>";
						$outAISI=$outAISI.$dadosDep[$i]->issue;
						$outAISI=$outAISI."</td>";

						$outAISI=$outAISI."<td>";
						$outAISI=$outAISI.$dadosDep[$i]->ar;
						$outAISI=$outAISI."</td>";

						$outAISI=$outAISI."<td>";
						$outAISI=$outAISI.$dadosDep[$i]->colecao;
						$outAISI=$outAISI."</td>";

						$outAISI=$outAISI."<td>";
						$outAISI=$outAISI.$dadosDep[$i]->primpagina;
						$outAISI=$outAISI."</td>";	

						$outAISI=$outAISI."<td>";
						$outAISI=$outAISI.$dadosDep[$i]->ultpagina;
						$outAISI=$outAISI."</td>";

						$outAISI=$outAISI."<td>";
						$outAISI=$outAISI.$dadosDep[$i]->citacoes;
						$outAISI=$outAISI."</td>";
						
						$outAISI=$outAISI."<td>";
						$outAISI=$outAISI.$dadosDep[$i]->conftitle;
						$outAISI=$outAISI."</td>";

						$outAISI=$outAISI."<td>";
						$outAISI=$outAISI.$dadosDep[$i]->language;
						$outAISI=$outAISI."</td>";

						$outAISI=$outAISI."<td>";
						$outAISI=$outAISI.$dadosDep[$i]->editor;
						$outAISI=$outAISI."</td>";   
	
						$outAISI=$outAISI."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');setObservacaoPublicacaoISI();return false;\" ></center></td>";
						$outAISI=$outAISI."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('9');apagarPublicacao();return false;\" ></center></td>";   
			
					$outAISI=$outAISI."</tr>";
				}
				break;
				default:{ //echo "DEFAULT";
					$outRestoISI=$outRestoISI."<tr>";
						$outRestoISI=$outRestoISI."<td>";
						$outRestoISI=$outRestoISI.$dadosDep[$i]->idinv;
						$outRestoISI=$outRestoISI."</td>";
						
						$outRestoISI=$outRestoISI."<td>";
						$outRestoISI=$v.$db->getNomeInvById($dadosDep[$i]->idinv);
						$outRestoISI=$outRestoISI."</td>";

						$outRestoISI=$outRestoISI."<td>";
						$outRestoISI=$outRestoISI.$dadosDep[$i]->tipo;
						$outRestoISI=$outRestoISI."</td>";

						$outRestoISI=$outRestoISI."<td>";
						$outRestoISI=$outRestoISI.$dadosDep[$i]->nomepublicacao;
						$outRestoISI=$outRestoISI."</td>";

						$outRestoISI=$outRestoISI."<td>";
						$outRestoISI=$outRestoISI.$dadosDep[$i]->issn;
						$outRestoISI=$outRestoISI."</td>";

						$outRestoISI=$outRestoISI."<td>";
						$outRestoISI=$outRestoISI.$dadosDep[$i]->titulo;
						$outRestoISI=$outRestoISI."</td>";

						$outRestoISI=$outRestoISI."<td>";
						$outRestoISI=$outRestoISI.$dadosDep[$i]->autores;
						$outRestoISI=$outRestoISI."</td>";

						$outRestoISI=$outRestoISI."<td>";
						$outRestoISI=$outRestoISI.$dadosDep[$i]->volume;
						$outRestoISI=$outRestoISI."</td>";

						$outRestoISI=$outRestoISI."<td>";
						$outRestoISI=$outRestoISI.$dadosDep[$i]->issue;
						$outRestoISI=$outRestoISI."</td>";

						$outRestoISI=$outRestoISI."<td>";
						$outRestoISI=$outRestoISI.$dadosDep[$i]->ar;
						$outRestoISI=$outRestoISI."</td>";

						$outRestoISI=$outRestoISI."<td>";
						$outRestoISI=$outRestoISI.$dadosDep[$i]->primpagina;
						$outRestoISI=$outRestoISI."</td>";	

						$outRestoISI=$outRestoISI."<td>";
						$outRestoISI=$outRestoISI.$dadosDep[$i]->ultpagina;
						$outRestoISI=$outRestoISI."</td>";

						$outRestoISI=$outRestoISI."<td>";
						$outRestoISI=$outRestoISI.$dadosDep[$i]->citacoes;
						$outRestoISI=$outRestoISI."</td>";

						$outRestoISI=$outRestoISI."<td>";
						$outRestoISI=$outRestoISI.$dadosDep[$i]->conftitle;
						$outRestoISI=$outRestoISI."</td>";

						$outRestoISI=$outRestoISI."<td>";
						$outRestoISI=$outRestoISI.$dadosDep[$i]->language;
						$outRestoISI=$outRestoISI."</td>";

						$outRestoISI=$outRestoISI."<td>";
						$outRestoISI=$outRestoISI.$dadosDep[$i]->editor;
						$outRestoISI=$outRestoISI."</td>";

						$outRestoISI=$outRestoISI."<td>";
						$outRestoISI=$outRestoISI.$dadosDep[$i]->tipofmup;
						$outRestoISI=$outRestoISI."</td>";
						
						$outRestoISI=$outRestoISI."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');setObservacaoPublicacaoISI();return false;\" ></center></td>";
						$outRestoISI=$outRestoISI."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('9');apagarPublicacao();return false;\" ></center></td>";   
			
					$outRestoISI=$outRestoISI."</tr>";
				}
				break;
			}
	}
	
	echo"<div id='outJISI'><table id='JISI' class='box-table-b'><caption>ARTIGOS</caption>";echo $tcolsJISI;echo "<th colspan='2'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPub(\"J\",\"ISI\");return false;'></center></th></tr></thead><tbody>";echo $outJISI;echo "</tbody>";echo "</table></p></div><br /><br>";
	echo"<div id='outPRPISI'><table id='PRPISI' class='box-table-b'><caption>PEER REVIEW PROCEEDINGS</caption>";echo $tcolsJISI;echo "<th colspan='2'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPub(\"PRP\",\"ISI\");return false;'></center></th></tr></thead><tbody>";echo $outPRPISI;echo "</tbody>";echo "</table></div><br /><br>";
	echo"<div id='outJAISI'><table id='JAISI' class='box-table-b'><caption>JOURNAL ABSTRACTS</caption>";echo $tcolsJISI;echo "<th colspan='2'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPub(\"JA\",\"ISI\");return false;'></center></th></tr></thead><tbody>";echo $outJAISI;echo "</tbody>";echo "</table></div><br /><br>";
	echo"<div id='outCPISI'><table id='CPISI' class='box-table-b'><caption>CONFERENCE PROCEEDINGS</caption>";echo $tcolsCONFISI;echo "<th colspan='2'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPub(\"CP\",\"ISI\");return false;'></center></th></tr></thead><tbody>";echo $outCPISI;echo "</tbody>";echo "</table></div><br /><br>";
	echo"<div id='outOAISI'><table id='OAISI' class='box-table-b'><caption>OTHER ABSTRACTS</caption>";echo $tcolsCONFISI;echo "<th colspan='2'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPub(\"OA\",\"ISI\");return false;'></center></th></tr></thead><tbody>";echo $outOAISI;echo "</tbody>";echo "</table></div><br /><br>";	
	echo"<div id='outLISI'><table id='LISI' class='box-table-b'><caption>LIVROS</caption>";echo $tcols;echo "<th colspan='2'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPub(\"L\",\"ISI\");return false;'></center></th></tr></thead><tbody>";echo $outLISI;echo "</tbody>";echo "</table></div><br /><br>";
	echo"<div id='outCLISI'><table id='CLISI' class='box-table-b'><caption>CAPITULOS DE LIVROS</caption>";echo $tcols;echo "<th colspan='2'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPub(\"CL\",\"ISI\");return false;'></center></th></tr></thead><tbody>";echo $outCLISI;echo "</tbody>";echo "</table></div><br /><br>";
	echo"<div id='outSISI'><table id='SISI' class='box-table-b'><caption>SUMÁRIOS</caption>";echo $tcols;echo "<th colspan='2'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPub(\"S\",\"ISI\");return false;'></center></th></tr></thead><tbody>";echo $outSISI;echo "</tbody>";echo "</table></div><br /><br>";																														
	echo"<div id='outJNISI'><table id='JNISI' class='box-table-b'><caption>ARTIGOS - NÃO INGLÊS</caption>";echo $tcolsJISI;echo "<th colspan='2'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPub(\"JN\",\"ISI\");return false;'></center></th></tr></thead><tbody>";echo $outJNISI;echo "</tbody>";echo "</table></div><br><br>";
	echo"<div id='outPRPNISI'><table id='PRPNISI' class='box-table-b'><caption>PEER REVIEW PROCEEDINGS - NÃO INGLÊS</caption>";echo $tcolsJISI;echo "<th colspan='2'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPub(\"PRPN\",\"ISI\");return false;'></center></th></tr></thead><tbody>";echo $outPRPNISI;echo "</tbody>";echo "</table></div><br /><br>";
	echo"<div id='outSNISI'><table id='SNISI' class='box-table-b'><caption>SUMÁRIO - NÃO INGLÊS</caption>";echo $tcols;echo "<th colspan='2'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPub(\"SN\",\"ISI\");return false;'></center></th></tr></thead><tbody>";echo $outSNISI;echo "</tbody>";echo "</table></div><br /><br>";
	echo"<div id='outLNISI'><table id='LNISI' class='box-table-b'><caption>LIVROS - NÃO INGLÊS</caption>";echo $tcols;echo "<th colspan='2'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPub(\"LN\",\"ISI\");return false;'></center></th></tr></thead><tbody>";echo $outLNISI;echo "</tbody>";echo "</table></div><br /><br>";
	echo"<div id='outCLNISI'><table id='CLNISI' class='box-table-b'><caption>CAPITULOS DE LIVROS - NÃO INGLÊS</caption>";echo $tcols;echo "<th colspan='2'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPub(\"CLN\",\"ISI\");return false;'></center></th></tr></thead><tbody>";echo $outCLNISI;echo "</tbody>";echo "</table></div><br /><br>";
	echo"<div id='outCPNISI'><table id='CPNISI' class='box-table-b'><caption>CONFERENCE PROCEEDINGS NÃO INGLÊS</caption>";echo $tcolsCONFISI;echo "<th colspan='2'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPub(\"CPN\",\"ISI\");return false;'></center></th></tr></thead><tbody>";echo $outCPNISI;echo "</tbody>";echo "</table></div><br /><br>";	
	echo"<div id='outPISI'><table id='PISI' class='box-table-b'><caption>PATENTES</caption>";echo $tcols;echo "<th colspan='2'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPub(\"P\",\"ISI\");return false;'></center></th></tr></thead><tbody>";echo $outPISI;echo "</tbody>";echo "</table></div><br/><br>";
	echo"<div id='outAISI'><table id='AISI' class='box-table-b'><caption>AGRADECIMENTOS</caption>";echo $tcols;echo "<th colspan='2'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPub(\"A\",\"ISI\");return false;'></center></th></tr></thead><tbody>";echo $outAISI;echo "</tbody>";echo "</table></div><br /><br>";
	
	echo "<p id='chave-publicacoes' ></p>";
	echo "</div>";
}

?>