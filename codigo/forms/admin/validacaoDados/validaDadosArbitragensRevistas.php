<?php

	function showEditArbitragensRevistas($dadosDep) 
	{
		$db = new Database();
		echo "<h3>Edição e Arbitragens de Revistas</h3>\n";
		echo"<div id='arbitragensRevistas'>";
					
		echo "<table id='arbRevistas' class='box-table-b'>
				<thead>
					<tr>
						<th>IDINV</th>
						<th>Nome</th>
						<th>Tipo</th>
						<th>ISSN</th>
						<th>Revista</th>
						<th>Link</th>						
						<th colspan='3'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewArbRevista();return false;'></center></th>
					</tr>
				</thead>
				<tbody>";
				
		foreach ($dadosDep as $i => $value){
			$checkAcao = $db->checkAcaoExists($dadosDep[$i]->id, $_SESSION['login'], 17);		
			if ($checkAcao) {
				$acao = transformIntoAcaoArbRevistaObject($dadosDep[$i]->id, $dadosDep[$i]->idinv, $checkAcao);	
				echo "<tr>";
					echo "<td>".$dadosDep[$i]->idinv."</td>";
					echo "<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
					echo "<td>";getTipoArbitragem($dadosDep[$i]->tipo);echo "</td>";
					echo "<td>".$dadosDep[$i]->issn."</td>";
					echo "<td>".$dadosDep[$i]->tituloRevista."</td>";
					echo "<td><a href='".$dadosDep[$i]->link."' target='_blank'>".$dadosDep[$i]->link."</a></td>";
					echo "<td></td>";
					echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-arb-revistas').text('" . $dadosDep[$i]->id . "');setObservacaoArbRevista();return false;\" ></center></td>";				
					echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-arb-revistas').text('" . $dadosDep[$i]->id . "');apagarArbRevista();return false;\" ></center></td>";
				echo "</tr>";	 
				
				echo "<tr>";
					echo "<td style='background:#FFFF33; overflow:hidden;'><img src=\"../../images/arrow_return_down_right.png\" name='navOption'></td>";
					echo "<td style='background:#FFFF33; overflow:hidden;'>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_arbrevistas_tipo_" . $dadosDep[$i]->id ."'>";getTipoArbitragem($acao['tipo']);echo "</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_arbrevistas_issn_" . $dadosDep[$i]->id ."'>".$acao['issn']."</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_arbrevistas_titulo_" . $dadosDep[$i]->id ."'>".$acao['tituloRevista']."</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_arbrevistas_link_" . $dadosDep[$i]->id ."'><a href='".$acao['link']."' target='_blank'>".$acao['link']."</a></td>";
					echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-arb-revistas').text('" . $dadosDep[$i]->id . "');\"></td>";
					echo "<td></td>";
					echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"apagarAcao('" . $dadosDep[$i]->id . "',17);return false;\" ></center></td>";
				echo "</tr>";	 
				
			} else {
				echo "<tr>";
					echo "<td>".$dadosDep[$i]->idinv."</td>";
					echo "<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
					echo "<td id='td_arbrevistas_tipo_" . $dadosDep[$i]->id ."'>";
					getTipoArbitragem($dadosDep[$i]->tipo);
					echo "</td>";
					echo "<td id='td_arbrevistas_issn_" . $dadosDep[$i]->id ."'>".$dadosDep[$i]->issn."</td>";
					echo "<td id='td_arbrevistas_titulo_" . $dadosDep[$i]->id ."'>".$dadosDep[$i]->tituloRevista."</td>";
					echo "<td id='td_arbrevistas_link_" . $dadosDep[$i]->id ."'><a href='".$dadosDep[$i]->link."' target='_blank'>".$dadosDep[$i]->link."</a></td>";
					echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-arb-revistas').text('" . $dadosDep[$i]->id . "');\"></td>";
					echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-arb-revistas').text('" . $dadosDep[$i]->id . "');setObservacaoArbRevista();return false;\" ></center></td>";				
					echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-arb-revistas').text('" . $dadosDep[$i]->id . "');apagarArbRevista();return false;\" ></center></td>";
				echo "</tr>";	    	
			}			
		}	
		
		 echo "</tbody>
		</table>
		<p id='chave-arb-revistas'></p>";

		echo "</div>";
	}
	
	function transformIntoAcaoArbRevistaObject($id, $idinv, $query) {
		$acao = array();
	
		$cena = explode("TIPO=",$query);
					
		$cena1 = explode(", ISSN='",$cena[1]);
		$acao['tipo'] = $cena1[0];
		
		$cena2 = explode("', TITULOREVISTA='",$cena1[1]);
		$acao['issn']= $cena2[0];
		
		$cena3 = explode("', LINK='",$cena2[1]);
		$acao['tituloRevista'] = $cena3[0];	
								
		$cena4 = explode("' where",$cena3[1]);
		$acao['link'] = $cena4[0];
		
		return $acao;
	}

	function getTipoArbitragem($i)
	{		
		$db = new Database();
		$lValues =$db->getLookupValues("lista_tipoarbitragens");		
		while ($row = mysql_fetch_assoc($lValues)) {	
			if($i==$row["ID"])
				echo $row["DESCRICAO"];
		}
		$db->disconnect();				
	}		
?>