<?php

function showEditColaboradores($dadosDep) {
	$db = new Database();
	
	echo "<h3>Colaboradores Atuais na FMUP</h3>";
	echo "<div id='colaboradoresInternos'>

	<table id='colIntAct' class='box-table-b'>
		<thead>
			<tr>
				<th>ID Inv</th>
				<th>Nome </th>
				<th>Nome Colaborador</th>
				<th>Nome Científico</th>
				<th>Departmento</th>
				<th colspan='3'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewColAct();return false;'></center></th>
			</tr>
		</thead>
		<tbody>";
	 
	foreach ($dadosDep->colaboradoresinternos as $i => $value){ 
		$checkAcao = $db->checkAcaoExists($dadosDep->colaboradoresinternos[$i]->id, $_SESSION['login'], 6);		
		if ($checkAcao) {
			$acao = transformIntoAcaoColIntObject($dadosDep->colaboradoresinternos[$i]->id, $dadosDep->colaboradoresinternos[$i]->idinv, $checkAcao);
			echo "<tr>";
				echo "<td>";echo $dadosDep->colaboradoresinternos[$i]->idinv;echo "</td>";		
				echo "<td>".$db->getNomeInvById($dadosDep->colaboradoresinternos[$i]->idinv). "</td>";		
				echo "<td>";echo $dadosDep->colaboradoresinternos[$i]->nome;echo "</td>";
				echo "<td>";echo $dadosDep->colaboradoresinternos[$i]->nomecient;echo "</td>";
				echo "<td>";getDepartamentos($dadosDep->colaboradoresinternos[$i]->departamento);echo "</td>";
				echo "<td></td>";
				echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-colIntAct').text('" . $dadosDep->colaboradoresinternos[$i]->id . "');setObservacaoColIntAct();return false;\" ></center></td>";				
				echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep->colaboradoresinternos[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-colIntAct').text('" . $dadosDep->colaboradoresinternos[$i]->id . "');apagarColActInt();return false;\" ></center></td>";
			echo "</tr>";
			
			echo "<tr>";
				echo "<td style='background:#FFFF33; overflow:hidden;'><img src=\"../../images/arrow_return_down_right.png\" name='navOption'></td>";			
				echo "<td style='background:#FFFF33; overflow:hidden;'>".$db->getNomeInvById($dadosDep->colaboradoresinternos[$i]->idinv). "</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_colIntAct_nome_" . $dadosDep->colaboradoresinternos[$i]->id . "'>";echo$acao['nome'];echo "</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_colIntAct_nomecient_" . $dadosDep->colaboradoresinternos[$i]->id . "'>";echo$acao['nomecient'];echo "</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_colIntAct_dep_" . $dadosDep->colaboradoresinternos[$i]->id . "'>";getDepartamentos($acao['dep']);echo "</td>";
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-colIntAct').text('" . $dadosDep->colaboradoresinternos[$i]->id . "');\"></td>";
				echo "<td></td>";
				echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep->colaboradoresinternos[$i]->id . "' value='apagar' name='navOption' onclick=\"apagarAcao('" . $dadosDep->colaboradoresinternos[$i]->id . "',6);return false;\" ></center></td>";
			echo "</tr>";
		} else {
			echo "<tr>";
				echo "<td>";echo $dadosDep->colaboradoresinternos[$i]->idinv;echo "</td>";		
				echo "<td>".$db->getNomeInvById($dadosDep->colaboradoresinternos[$i]->idinv). "</td>";
				echo "<td id='td_colIntAct_nome_" . $dadosDep->colaboradoresinternos[$i]->id . "'>";echo $dadosDep->colaboradoresinternos[$i]->nome;echo "</td>";
				echo "<td id='td_colIntAct_nomecient_" . $dadosDep->colaboradoresinternos[$i]->id . "'>";echo $dadosDep->colaboradoresinternos[$i]->nomecient;echo "</td>";
				echo "<td id='td_colIntAct_dep_" . $dadosDep->colaboradoresinternos[$i]->id . "'>";getDepartamentos($dadosDep->colaboradoresinternos[$i]->departamento);echo "</td>";
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-colIntAct').text('" . $dadosDep->colaboradoresinternos[$i]->id . "');\"></td>";
				echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-colIntAct').text('" . $dadosDep->colaboradoresinternos[$i]->id . "');setObservacaoColIntAct();return false;\" ></center></td>";				
				echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep->colaboradoresinternos[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-colIntAct').text('" . $dadosDep->colaboradoresinternos[$i]->id . "');apagarColActInt();return false;\" ></center></td>";
			echo "</tr>";
		}			
	}

	echo "</tbody>
	</table>
	<p id='chave-colIntAct' hidden></p>
	</div>";
	
	echo "<h3>Colaboradores Externos Atuais</h3>\n";

	echo "<div id='colaboradoresExternos'>";

	echo "<table id='colExtAct' class='box-table-b'>
			<thead>		
				<tr>
					<th>ID Inv</th>
					<th>Nome</th>
					<th>Nome Científico com Iniciais</th>
					<th>Instituição</th>
					<th>País</th>
					<th colspan='3'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewColExt();return false;'></center></th>
				</tr>
			</thead>
			<tbody>";
					
	foreach ($dadosDep->colaboradores as $i => $value){ 
		$checkAcao = $db->checkAcaoExists($dadosDep->colaboradores[$i]->id, $_SESSION['login'], 7);		
		if ($checkAcao) {
			$acao = transformIntoAcaoColExtObject($dadosDep->colaboradores[$i]->id, $dadosDep->colaboradores[$i]->idinv, $checkAcao);
			echo "<tr>";
				echo "<td>";echo $dadosDep->colaboradores[$i]->idinv;echo "</td>";
				echo "<td>".$db->getNomeInvById($dadosDep->colaboradores[$i]->idinv). "</td>";	
				echo "<td>";echo $dadosDep->colaboradores[$i]->nomecient;echo "</td>";							
				echo "<td>";echo $dadosDep->colaboradores[$i]->instituicao;echo "</td>";				
				echo "<td>";getPaisesCol($dadosDep->colaboradores[$i]->pais);echo "</td>";
				echo "<td></td>";
				echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-colExtAct').text('" . $dadosDep->colaboradores[$i]->id . "');setObservacaoColExtAct();return false;\" ></center></td>";				
				echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep->colaboradores[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-colExtAct').text('" . $dadosDep->colaboradores[$i]->id . "');apagarColActExt();return false;\" ></center></td>";
			echo "</tr>";
			
			echo "<tr>";
				echo "<td style='background:#FFFF33; overflow:hidden;'><img src=\"../../images/arrow_return_down_right.png\" name='navOption'></td>";	
				echo "<td style='background:#FFFF33; overflow:hidden;'>".$db->getNomeInvById($dadosDep->colaboradores[$i]->idinv). "</td>";				
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_colExtAct_nomecient_" . $dadosDep->colaboradores[$i]->id . "'>";echo $acao['nomecient'];echo "</td>";							
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_colExtAct_instituicao_" . $dadosDep->colaboradores[$i]->id . "'>";echo $acao['ins'];echo "</td>";				
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_colExtAct_pais_" . $dadosDep->colaboradores[$i]->id . "'>";getPaisesCol($acao['pais']);echo "</td>";
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-colExtAct').text('" . $dadosDep->colaboradores[$i]->id . "');\"></td>";				
				echo "<td></td>";
				echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep->colaboradores[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-colExtAct').text('" . $dadosDep->colaboradores[$i]->id . "');apagarColActExt();return false;\" ></center></td>";
			echo "</tr>";
			
		} else {
			echo "<tr>";
				echo "<td>";echo $dadosDep->colaboradores[$i]->idinv;echo "</td>";
				echo "<td>".$db->getNomeInvById($dadosDep->colaboradores[$i]->idinv). "</td>";	
				echo "<td id='td_colExtAct_nomecient_" . $dadosDep->colaboradores[$i]->id . "'>";echo $dadosDep->colaboradores[$i]->nomecient;echo "</td>";							
				echo "<td id='td_colExtAct_instituicao_" . $dadosDep->colaboradores[$i]->id . "'>";echo $dadosDep->colaboradores[$i]->instituicao;echo "</td>";				
				echo "<td id='td_colExtAct_pais_" . $dadosDep->colaboradores[$i]->id . "'>";getPaisesCol($dadosDep->colaboradores[$i]->pais);echo "</td>";
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-colExtAct').text('" . $dadosDep->colaboradores[$i]->id . "');\"></td>";
				echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-colExtAct').text('" . $dadosDep->colaboradores[$i]->id . "');setObservacaoColExtAct();return false;\" ></center></td>";				
				echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep->colaboradores[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-colExtAct').text('" . $dadosDep->colaboradores[$i]->id . "');apagarColActExt();return false;\" ></center></td>";
			echo "</tr>";
		}
	}

	echo "</tbody>
	</table> <p id='chave-colExtAct' hidden></p>
   </div>";

}
		
function transformIntoAcaoColExtObject($id, $idinv, $query) {
	
	$acao = array();
	$cena = explode("NOMECIENT='",$query);
				
	$cena1 = explode("', INSTITUICAO='",$cena[1]);
	$acao['nomecient'] = $cena1[0];
	
	$cena2 = explode("', PAIS='",$cena1[1]);
	$acao['ins'] = $cena2[0];
		
	$cena3 = explode("' where",$cena2[1]);
	$acao['pais'] = $cena3[0];
			
	return $acao;
}

function transformIntoAcaoColIntObject($id, $idinv, $query) {

	$acao = array();
	$cena = explode("NOMECIENT='",$query);
				
	$cena1 = explode("', DEPARTAMENTO=",$cena[1]);
	$acao['nomecient'] = $cena1[0];
		
	$cena2 = explode(" where",$cena1[1]);
	$acao['dep'] = $cena2[0];
			
	return $acao;
}
		
function getPaisesCol($i){
	$db = new Database();
	$lValues =$db->getLookupValues("lista_paises");
	while ($row = mysql_fetch_assoc($lValues)) {	
		if($i==$row["ID"])
			echo $row["DESCRICAO"];
	}
	$db->disconnect();			
}	
	
function checkPaisCol($id,$i){
	global $dadosDep;
	if($dadosDep->colaboradores[$i]->pais==$id)
		return true;
	else 
		return false;
}
function getDepartamentos($i){
	$db = new Database();
	$lValues =$db->getLookupValues("lista_servicosfmup");
	while ($row = mysql_fetch_assoc($lValues)) {
		if($row["ID"]==$i)
			echo $row["DESCRICAO"];
	}
	$db->disconnect();
}

function checkDepartamento($id,$i){
	global $dadosDep;
	if($dadosDep->colaboradoresinternos[$i]->departamento==$id)
		return true;
	else
		return false;
}

?>