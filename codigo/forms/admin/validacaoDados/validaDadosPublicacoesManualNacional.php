<?php

function showEditPublicacoesManualNacional($dadosDep) {
	
	$db = new Database();
$outJNMAN="";
$outJANMAN="";
$outPRPNMAN="";
$outCPNMAN="";
$outOANMAN="";
$outLNMAN="";
$outCLNMAN="";

$tcols="<thead>
            <tr>
                <th>IDINV</th>
				<th>NOME</th>
                <th>TIPO</th>
                <th>TÍTULO DA OBRA</th>
                <th>ISSN</th>
                <th>ISSN_LINKING</th>
                <th>ISSN_ELECTRONIC</th>
                <th>ISBN</th>
                <th>TITULO</th>
                <th>AUTORES</th>
                <th>VOLUME</th>
                <th>ISSUE</th>
                <th>AR</th>
                <th>COLECAO</th>
                <th>PRIMPAGINA</th>
                <th>ULTPAGINA</th>
                <th>IFACTOR</th>
                <th>ANO</th>
                <th>CITACOES</th>
                <th>NPATENTE</th>
                <th>DATAPATENTE</th>
                <th>IPC</th>
                <th>AREA</th>
                <th>LINK</th>
                <th>ESTADO</th>";

$tcolsJ="<thead>
            <tr>
                <th>IDINV</th>
				<th>NOME</th>
                <th>ISSN</th>
                <th>REVISTA</th>
                <th>TÍTULO</th>
                <th>AUTORES</th>
                <th>VOLUME</th>
                <th>ISSUE</th>
                <th>PRIMEIRA PÁG.</th>
                <th>ÚLTIMA PÁG.</th>
                <th>LINK</th>
                <th>ESTADO</th>";

$tcolsCONF="<thead>
                <tr>
                    <th>IDINV</th>
					<th>NOME</th>
                    <th>ISBN</th>
                    <th>TÍTULO DA OBRA</th>
                    <th>EDITORES</th>
                    <th>TÍTULO ARTIGO</th>
                    <th>AUTORES</th>
                    <th>PRIMEIRA PÁG.</th>
                    <th>ÚLTIMA PÁG.</th>
                    <th>LINK</th>
                    <th>ESTADO</th>";

$tcolsPRP="<thead>
                <tr>
                    <th>IDINV</th>
					<th>NOME</th>
                    <th>ISSN</th>
                    <th>TÍTULO DA OBRA</th>
                    <th>EDITORES</th>
                    <th>TÍTULO ARTIGO</th>
                    <th>AUTORES</th>
                    <th>PRIMEIRA PÁ</th>
                    <th>LINK</th>
                    <th>ESTADO</th>";

$tcolsCLIV="<thead>
                <tr>
                    <th>IDINV</th>  
					<th>NOME</th>           
                    <th>ISBN</th>
                    <th>TÍTULO DA OBRA</th>
                    <th>EDITORA</th>
                    <th>EDITORES</th>
                    <th>TÍTULO ARTIGO</th>
                    <th>AUTORES</th>
                    <th>PRIMEIRA PÁG.</th>
                    <th>ÚLTIMA PÁG.</th>
                    <th>LINK</th>
                    <th>ESTADO</th>";

$tcolsPAT="<thead>
                <tr>
                    <th>IDINV</th>
					<th>NOME</th>
                    <th>Nº PATENTE</th>
                    <th>IPC</th>
                    <th>TÍTULO</th>
                    <th>AUTORES</th>
                    <th>DATA de PUBLICAÇÃO</th>
                    <th>LINK</th>
                    <th>ESTADO</th>";

$tcolsLIV="<thead>
                <tr>
                    <th>IDINV</th>
					<th>NOME</th>
                    <th>ISBN</th>
                    <th>TÍTULO</th>
                    <th>AUTORES</th>
                    <th>EDITOR</th>
                    <th>EDITORA</th>
                    <th>LINK</th>
                    <th>ESTADO</th>";
		    
	foreach ($dadosDep as $i => $value){
			
        $tipo=$dadosDep[$i]->tipofmup;

        switch($tipo){
            case 'CPN': {
				$checkAcao = $db->checkAcaoExists($dadosDep[$i]->id, $_SESSION['login'], 13);		
				if ($checkAcao) {
					$acao = transformIntoAcaoPubCPObject($dadosDep[$i]->id, $dadosDep[$i]->idinv, $checkAcao);	
					$outCPNMAN=$outCPNMAN."<tr>";
						$outCPNMAN=$outCPNMAN."<td>".$dadosDep[$i]->idinv."</td>";						
						$outCPNMAN=$outCPNMAN."<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
						$outCPNMAN=$outCPNMAN."<td>".$dadosDep[$i]->isbn."</td>";
						$outCPNMAN=$outCPNMAN."<td>".$dadosDep[$i]->nomepublicacao."</td>";
						$outCPNMAN=$outCPNMAN."<td>".$dadosDep[$i]->editor."</td>";
						$outCPNMAN=$outCPNMAN."<td>".$dadosDep[$i]->titulo."</td>";
						$outCPNMAN=$outCPNMAN."<td>".$dadosDep[$i]->autores."</td>";
						$outCPNMAN=$outCPNMAN."<td>".$dadosDep[$i]->primpagina."</td>";
						$outCPNMAN=$outCPNMAN."<td>".$dadosDep[$i]->ultpagina."</td>";
						$outCPNMAN=$outCPNMAN."<td><a href ='".$dadosDep[$i]->link."' target='_blank'>".$dadosDep[$i]->link."</a></td>";
						$outCPNMAN=$outCPNMAN."<td>".getEstadoPublicacao($dadosDep[$i]->estado)."</td>";
						$outCPNMAN=$outCPNMAN."<td></td>";
						$outCPNMAN=$outCPNMAN."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-pubNacMan-Con').text('" . $dadosDep[$i]->id . "');setObservacaoPubManNacCP();return false;\" ></center></td>";
						$outCPNMAN=$outCPNMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('12');apagarPublicacao();return false;\" ></center></td>";    		
					$outCPNMAN=$outCPNMAN."</tr>";
					
					$outCPNMAN=$outCPNMAN."<tr>";
						$outCPNMAN=$outCPNMAN."<td style='background:#FFFF33; overflow:hidden;'><img src=\"../../images/arrow_return_down_right.png\" name='navOption'></td>";
						$outCPNMAN=$outCPNMAN."<td style='background:#FFFF33; overflow:hidden;'>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
						$outCPNMAN=$outCPNMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outCPNMAN_isbn_" . $dadosDep[$i]->id . "'>".$acao['isbn']."</td>";
						$outCPNMAN=$outCPNMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outCPNMAN_nomepub_" . $dadosDep[$i]->id . "'>".$acao['nomepublicacao']."</td>";
						$outCPNMAN=$outCPNMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outCPNMAN_editor_" . $dadosDep[$i]->id . "'>".$acao['editor']."</td>";
						$outCPNMAN=$outCPNMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outCPNMAN_titulo_" . $dadosDep[$i]->id . "'>".$acao['titulo']."</td>";
						$outCPNMAN=$outCPNMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outCPNMAN_autores_" . $dadosDep[$i]->id . "'>".$acao['autores']."</td>";
						$outCPNMAN=$outCPNMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outCPNMAN_prim_" . $dadosDep[$i]->id . "'>".$acao['primpagina']."</td>";
						$outCPNMAN=$outCPNMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outCPNMAN_ult_" . $dadosDep[$i]->id . "'>".$acao['ultpagina']."</td>";
						$outCPNMAN=$outCPNMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outCPNMAN_link_" . $dadosDep[$i]->id . "'><a href ='".$acao['link']."' target='_blank'>".$acao['link']."</a></td>";
						$outCPNMAN=$outCPNMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outCPNMAN_estado_" . $dadosDep[$i]->id . "'>".getEstadoPublicacao($acao['estado'])."</td>";
						$outCPNMAN=$outCPNMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubNacMan-Con').text('" . $dadosDep[$i]->id . "');$('#tabela').text('13');\"></td>";
						$outCPNMAN=$outCPNMAN."<td></td>";
						$outCPNMAN=$outCPNMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"apagarAcao('" . $dadosDep[$i]->id . "',13);return false;\" ></center></td>";    		
					$outCPNMAN=$outCPNMAN."</tr>";
				} else {
					$outCPNMAN=$outCPNMAN."<tr>";
						$outCPNMAN=$outCPNMAN."<td>".$dadosDep[$i]->idinv."</td>";
						$outCPNMAN=$outCPNMAN."<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
						$outCPNMAN=$outCPNMAN."<td id='td_outCPNMAN_isbn_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->isbn."</td>";
						$outCPNMAN=$outCPNMAN."<td id='td_outCPNMAN_nomepub_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->nomepublicacao."</td>";
						$outCPNMAN=$outCPNMAN."<td id='td_outCPNMAN_editor_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->editor."</td>";
						$outCPNMAN=$outCPNMAN."<td id='td_outCPNMAN_titulo_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->titulo."</td>";
						$outCPNMAN=$outCPNMAN."<td id='td_outCPNMAN_autores_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->autores."</td>";
						$outCPNMAN=$outCPNMAN."<td id='td_outCPNMAN_prim_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->primpagina."</td>";
						$outCPNMAN=$outCPNMAN."<td id='td_outCPNMAN_ult_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->ultpagina."</td>";
						$outCPNMAN=$outCPNMAN."<td><a href ='".$dadosDep[$i]->link."' target='_blank'>".$dadosDep[$i]->link."</a></td>";
						$outCPNMAN=$outCPNMAN."<td id='td_outCPNMAN_estado_" . $dadosDep[$i]->id . "'>".getEstadoPublicacao($dadosDep[$i]->estado)."</td>";
						$outCPNMAN=$outCPNMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubNacMan-Con').text('" . $dadosDep[$i]->id . "');$('#tabela').text('13');\"></td>";
						$outCPNMAN=$outCPNMAN."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-pubNacMan-Con').text('" . $dadosDep[$i]->id . "');setObservacaoPubManNacCP();return false;\" ></center></td>";
						$outCPNMAN=$outCPNMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('12');apagarPublicacao();return false;\" ></center></td>";    		
					$outCPNMAN=$outCPNMAN."</tr>";
				}
            }
            break;
            case 'JN': {
				$checkAcao = $db->checkAcaoExists($dadosDep[$i]->id, $_SESSION['login'], 13);		
				if ($checkAcao) {
					$acao = transformIntoAcaoPubArtObject($dadosDep[$i]->id, $dadosDep[$i]->idinv, $checkAcao);	
					$outJNMAN=$outJNMAN."<tr>";
						$outJNMAN=$outJNMAN."<td>".$dadosDep[$i]->idinv."</td>";
						$outJNMAN=$outJNMAN."<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
						$outJNMAN=$outJNMAN."<td>".$dadosDep[$i]->issn."</td>";
						$outJNMAN=$outJNMAN."<td>".$dadosDep[$i]->nomepublicacao."</td>";
						$outJNMAN=$outJNMAN."<td>".$dadosDep[$i]->titulo."</td>";
						$outJNMAN=$outJNMAN."<td>".$dadosDep[$i]->autores."</td>";
						$outJNMAN=$outJNMAN."<td>".$dadosDep[$i]->volume."</td>";
						$outJNMAN=$outJNMAN."<td>".$dadosDep[$i]->issue."</td>";
						$outJNMAN=$outJNMAN."<td>".$dadosDep[$i]->primpagina."</td>";
						$outJNMAN=$outJNMAN."<td>".$dadosDep[$i]->ultpagina."</td>";
						$outJNMAN=$outJNMAN."<td><a href='".$dadosDep[$i]->link."' target='_blank'>".$dadosDep[$i]->link."</a></td>";
						$outJNMAN=$outJNMAN."<td>".getEstadoPublicacao($dadosDep[$i]->estado)."</td>";
						$outJNMAN=$outJNMAN."<td></td>";
						$outJNMAN=$outJNMAN."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-pubNacMan-Art').text('" . $dadosDep[$i]->id . "');setObservacaoPubManNacArt();return false;\" ></center></td>";
						$outJNMAN=$outJNMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');apagarPublicacao();return false;\" ></center></td>";    		
					$outJNMAN=$outJNMAN."</tr>";
					
					$outJNMAN=$outJNMAN."<tr>";
						$outJNMAN=$outJNMAN."<td style='background:#FFFF33; overflow:hidden;'><img src=\"../../images/arrow_return_down_right.png\" name='navOption'></td>";
						$outJNMAN=$outJNMAN."<td style='background:#FFFF33; overflow:hidden;'>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
						$outJNMAN=$outJNMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outJNMAN_issn_" . $dadosDep[$i]->id . "'>".$acao['issn']."</td>";
						$outJNMAN=$outJNMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outJNMAN_nomepub_" . $dadosDep[$i]->id . "'>".$acao['nomepublicacao']."</td>";
						$outJNMAN=$outJNMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outJNMAN_titulo_" . $dadosDep[$i]->id . "'>".$acao['titulo']."</td>";
						$outJNMAN=$outJNMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outJNMAN_autores_" . $dadosDep[$i]->id . "'>".$acao['autores']."</td>";
						$outJNMAN=$outJNMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outJNMAN_volume_" . $dadosDep[$i]->id . "'>".$acao['volume']."</td>";
						$outJNMAN=$outJNMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outJNMAN_issue_" . $dadosDep[$i]->id . "'>".$acao['issue']."</td>";
						$outJNMAN=$outJNMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outJNMAN_prim_" . $dadosDep[$i]->id . "'>".$acao['primpagina']."</td>";
						$outJNMAN=$outJNMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outJNMAN_ult_" . $dadosDep[$i]->id . "'>".$acao['ultpagina']."</td>";
						$outJNMAN=$outJNMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outJNMAN_link_" . $dadosDep[$i]->id . "'><a href='".$acao['link']."' target='_blank'>".$acao['link']."</td>";
						$outJNMAN=$outJNMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outJNMAN_estado_" . $dadosDep[$i]->id . "'>".getEstadoPublicacao($acao['estado'])."</td>";
						$outJNMAN=$outJNMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubNacMan-Art').text('" . $dadosDep[$i]->id . "');$('#tabela').text('13');\"></td>";
						$outJNMAN=$outJNMAN."<td></td>";
						$outJNMAN=$outJNMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"apagarAcao('" . $dadosDep[$i]->id . "',13);return false;\" ></center></td>";    		
					$outJNMAN=$outJNMAN."</tr>";
				} else {
					$outJNMAN=$outJNMAN."<tr>";
						$outJNMAN=$outJNMAN."<td>".$dadosDep[$i]->idinv."</td>";
						$outJNMAN=$outJNMAN."<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
						$outJNMAN=$outJNMAN."<td id='td_outJNMAN_issn_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->issn."</td>";
						$outJNMAN=$outJNMAN."<td id='td_outJNMAN_nomepub_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->nomepublicacao."</td>";
						$outJNMAN=$outJNMAN."<td id='td_outJNMAN_titulo_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->titulo."</td>";
						$outJNMAN=$outJNMAN."<td id='td_outJNMAN_autores_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->autores."</td>";
						$outJNMAN=$outJNMAN."<td id='td_outJNMAN_volume_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->volume."</td>";
						$outJNMAN=$outJNMAN."<td id='td_outJNMAN_issue_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->issue."</td>";
						$outJNMAN=$outJNMAN."<td id='td_outJNMAN_prim_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->primpagina."</td>";
						$outJNMAN=$outJNMAN."<td id='td_outJNMAN_ult_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->ultpagina."</td>";
						$outJNMAN=$outJNMAN."<td id='td_outJNMAN_link_" . $dadosDep[$i]->id . "'><a href='".$dadosDep[$i]->link."' target='_blank'>".$dadosDep[$i]->link."</td>";
						$outJNMAN=$outJNMAN."<td id='td_outJNMAN_estado_" . $dadosDep[$i]->id . "'>".getEstadoPublicacao($dadosDep[$i]->estado)."</td>";
						$outJNMAN=$outJNMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubNacMan-Art').text('" . $dadosDep[$i]->id . "');$('#tabela').text('13');\"></td>";
						$outJNMAN=$outJNMAN."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-pubNacMan-Art').text('" . $dadosDep[$i]->id . "');setObservacaoPubManNacArt();return false;\" ></center></td>";
						$outJNMAN=$outJNMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');apagarPublicacao();return false;\" ></center></td>";    		
					$outJNMAN=$outJNMAN."</tr>";
				}
            }
            break;			
            case 'PRPN': {
				$checkAcao = $db->checkAcaoExists($dadosDep[$i]->id, $_SESSION['login'], 13);		
				if ($checkAcao) {
					$acao = transformIntoAcaoPubArtObject($dadosDep[$i]->id, $dadosDep[$i]->idinv, $checkAcao);	
					$outPRPNMAN=$outPRPNMAN."<tr>";
						$outPRPNMAN=$outPRPNMAN."<td>".$dadosDep[$i]->idinv."</td>";
						$outPRPNMAN=$outPRPNMAN."<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
						$outPRPNMAN=$outPRPNMAN."<td>".$dadosDep[$i]->issn."</td>";
						$outPRPNMAN=$outPRPNMAN."<td>".$dadosDep[$i]->nomepublicacao."</td>";
						$outPRPNMAN=$outPRPNMAN."<td>".$dadosDep[$i]->titulo."</td>";
						$outPRPNMAN=$outPRPNMAN."<td>".$dadosDep[$i]->autores."</td>";
						$outPRPNMAN=$outPRPNMAN."<td>".$dadosDep[$i]->volume."</td>";
						$outPRPNMAN=$outPRPNMAN."<td>".$dadosDep[$i]->issue."</td>";
						$outPRPNMAN=$outPRPNMAN."<td>".$dadosDep[$i]->primpagina."</td>";
						$outPRPNMAN=$outPRPNMAN."<td>".$dadosDep[$i]->ultpagina."</td>";
						$outPRPNMAN=$outPRPNMAN."<td><a href='".$dadosDep[$i]->link."' target='_blank'>".$dadosDep[$i]->link."</a></td>";
						$outPRPNMAN=$outPRPNMAN."<td>".getEstadoPublicacao($dadosDep[$i]->estado)."</td>";
						$outPRPNMAN=$outPRPNMAN."<td></td>";
						$outPRPNMAN=$outPRPNMAN."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-pubNacMan-Rev').text('" . $dadosDep[$i]->id . "');setObservacaoPubManNacRev();return false;\" ></center></td>";
						$outPRPNMAN=$outPRPNMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');apagarPublicacao();return false;\" ></center></td>";    		
					$outPRPNMAN=$outPRPNMAN."</tr>";
					
					$outPRPNMAN=$outPRPNMAN."<tr>";
						$outPRPNMAN=$outPRPNMAN."<td style='background:#FFFF33; overflow:hidden;'><img src=\"../../images/arrow_return_down_right.png\" name='navOption'></td>";
						$outPRPNMAN=$outPRPNMAN."<td style='background:#FFFF33; overflow:hidden;'>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
						$outPRPNMAN=$outPRPNMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outPRPNMAN_issn_" . $dadosDep[$i]->id . "'>".$acao['issn']."</td>";
						$outPRPNMAN=$outPRPNMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outPRPNMAN_nomepub_" . $dadosDep[$i]->id . "'>".$acao['nomepublicacao']."</td>";
						$outPRPNMAN=$outPRPNMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outPRPNMAN_titulo_" . $dadosDep[$i]->id . "'>".$acao['titulo']."</td>";
						$outPRPNMAN=$outPRPNMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outPRPNMAN_autores_" . $dadosDep[$i]->id . "'>".$acao['autores']."</td>";
						$outPRPNMAN=$outPRPNMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outPRPNMAN_volume_" . $dadosDep[$i]->id . "'>".$acao['volume']."</td>";
						$outPRPNMAN=$outPRPNMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outPRPNMAN_issue_" . $dadosDep[$i]->id . "'>".$acao['issue']."</td>";
						$outPRPNMAN=$outPRPNMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outPRPNMAN_prim_" . $dadosDep[$i]->id . "'>".$acao['primpagina']."</td>";
						$outPRPNMAN=$outPRPNMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outPRPNMAN_ult_" . $dadosDep[$i]->id . "'>".$acao['ultpagina']."</td>";
						$outPRPNMAN=$outPRPNMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outPRPNMAN_link_" . $dadosDep[$i]->id . "'><a href='".$acao['link']."' target='_blank'>".$acao['link']."</a></td>";
						$outPRPNMAN=$outPRPNMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outPRPNMAN_estado_" . $dadosDep[$i]->id . "'>".getEstadoPublicacao($dadosDep[$i]->estado)."</td>";
						$outPRPNMAN=$outPRPNMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubNacMan-Rev').text('" . $dadosDep[$i]->id . "');$('#tabela').text('13');\"></td>";
						$outPRPNMAN=$outPRPNMAN."<td></td>";
						$outPRPNMAN=$outPRPNMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"apagarAcao('" . $dadosDep[$i]->id . "',13);return false;\" ></center></td>";    		
					$outPRPNMAN=$outPRPNMAN."</tr>";
				} else {
					$outPRPNMAN=$outPRPNMAN."<tr>";
						$outPRPNMAN=$outPRPNMAN."<td>".$dadosDep[$i]->idinv."</td>";
						$outPRPNMAN=$outPRPNMAN."<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
						$outPRPNMAN=$outPRPNMAN."<td id='td_outPRPNMAN_issn_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->issn."</td>";
						$outPRPNMAN=$outPRPNMAN."<td id='td_outPRPNMAN_nomepub_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->nomepublicacao."</td>";
						$outPRPNMAN=$outPRPNMAN."<td id='td_outPRPNMAN_titulo_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->titulo."</td>";
						$outPRPNMAN=$outPRPNMAN."<td id='td_outPRPNMAN_autores_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->autores."</td>";
						$outPRPNMAN=$outPRPNMAN."<td id='td_outPRPNMAN_volume_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->volume."</td>";
						$outPRPNMAN=$outPRPNMAN."<td id='td_outPRPNMAN_issue_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->issue."</td>";
						$outPRPNMAN=$outPRPNMAN."<td id='td_outPRPNMAN_prim_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->primpagina."</td>";
						$outPRPNMAN=$outPRPNMAN."<td id='td_outPRPNMAN_ult_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->ultpagina."</td>";
						$outPRPNMAN=$outPRPNMAN."<td id='td_outPRPNMAN_link_" . $dadosDep[$i]->id . "'><a href='".$dadosDep[$i]->link."' target='_blank'>".$dadosDep[$i]->link."</a></td>";
						$outPRPNMAN=$outPRPNMAN."<td id='td_outPRPNMAN_estado_" . $dadosDep[$i]->id . "'>".getEstadoPublicacao($dadosDep[$i]->estado)."</td>";
						$outPRPNMAN=$outPRPNMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubNacMan-Rev').text('" . $dadosDep[$i]->id . "');$('#tabela').text('13');\"></td>";
						$outPRPNMAN=$outPRPNMAN."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-pubNacMan-Rev').text('" . $dadosDep[$i]->id . "');setObservacaoPubManNacRev();return false;\" ></center></td>";
						$outPRPNMAN=$outPRPNMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');apagarPublicacao();return false;\" ></center></td>";    		
					$outPRPNMAN=$outPRPNMAN."</tr>";
				}
            }
            break;
            case 'JAN': {
				$checkAcao = $db->checkAcaoExists($dadosDep[$i]->id, $_SESSION['login'], 13);		
				if ($checkAcao) {
					$acao = transformIntoAcaoPubArtObject($dadosDep[$i]->id, $dadosDep[$i]->idinv, $checkAcao);	
					$outJANMAN=$outJANMAN."<tr>";
						$outJANMAN=$outJANMAN."<td>".$dadosDep[$i]->idinv."</td>";
						$outJANMAN=$outJANMAN."<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
						$outJANMAN=$outJANMAN."<td>".$dadosDep[$i]->issn."</td>";
						$outJANMAN=$outJANMAN."<td>".$dadosDep[$i]->nomepublicacao."</td>";
						$outJANMAN=$outJANMAN."<td>".$dadosDep[$i]->titulo."</td>";
						$outJANMAN=$outJANMAN."<td>".$dadosDep[$i]->autores."</td>";
						$outJANMAN=$outJANMAN."<td>".$dadosDep[$i]->volume."</td>";
						$outJANMAN=$outJANMAN."<td>".$dadosDep[$i]->issue."</td>";
						$outJANMAN=$outJANMAN."<td>".$dadosDep[$i]->primpagina."</td>";
						$outJANMAN=$outJANMAN."<td>".$dadosDep[$i]->ultpagina."</td>";
						$outJANMAN=$outJANMAN."<td><a href='".$dadosDep[$i]->link."' target='_blank'>".$dadosDep[$i]->link."</a></td>";
						$outJANMAN=$outJANMAN."<td>".getEstadoPublicacao($dadosDep[$i]->estado)."</td>";
						$outJANMAN=$outJANMAN."<td></td>";
						$outJANMAN=$outJANMAN."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-pubNacMan-Sum').text('" . $dadosDep[$i]->id . "');setObservacaoPubManNacSum();return false;\" ></center></td>";
						$outJANMAN=$outJANMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');apagarPublicacao();return false;\" ></center></td>";    		
					$outJANMAN=$outJANMAN."</tr>";
					
					$outJANMAN=$outJANMAN."<tr>";
						$outJANMAN=$outJANMAN."<td style='background:#FFFF33; overflow:hidden;'><img src=\"../../images/arrow_return_down_right.png\" name='navOption'></td>";
						$outJANMAN=$outJANMAN."<td style='background:#FFFF33; overflow:hidden;'>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
						$outJANMAN=$outJANMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outJANMAN_issn_" . $dadosDep[$i]->id . "'>".$acao['issn']."</td>";
						$outJANMAN=$outJANMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outJANMAN_nomepub_" . $dadosDep[$i]->id . "'>".$acao['nomepublicacao']."</td>";
						$outJANMAN=$outJANMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outJANMAN_titulo_" . $dadosDep[$i]->id . "'>".$acao['titulo']."</td>";
						$outJANMAN=$outJANMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outJANMAN_autores_" . $dadosDep[$i]->id . "'>".$acao['autores']."</td>";
						$outJANMAN=$outJANMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outJANMAN_volume_" . $dadosDep[$i]->id . "'>".$acao['volume']."</td>";
						$outJANMAN=$outJANMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outJANMAN_issue_" . $dadosDep[$i]->id . "'>".$acao['issue']."</td>";
						$outJANMAN=$outJANMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outJANMAN_prim_" . $dadosDep[$i]->id . "'>".$acao['primpagina']."</td>";
						$outJANMAN=$outJANMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outJANMAN_ult_" . $dadosDep[$i]->id . "'>".$acao['ultpagina']."</td>";
						$outJANMAN=$outJANMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outJANMAN_link_" . $dadosDep[$i]->id . "'><a href='".$acao['link']."' target='_blank'>".$acao['link']."</a></td>";
						$outJANMAN=$outJANMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outJANMAN_estado_" . $dadosDep[$i]->id . "'>".getEstadoPublicacao($acao['estado'])."</td>";
						$outJANMAN=$outJANMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubNacMan-Sum').text('" . $dadosDep[$i]->id . "');$('#tabela').text('13');\"></td>";
						$outJANMAN=$outJANMAN."<td></td>";
						$outJANMAN=$outJANMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"apagarAcao('" . $dadosDep[$i]->id . "',13);return false;\" ></center></td>";    		
					$outJANMAN=$outJANMAN."</tr>";

				} else {
					$outJANMAN=$outJANMAN."<tr>";
						$outJANMAN=$outJANMAN."<td>".$dadosDep[$i]->idinv."</td>";
						$outJANMAN=$outJANMAN."<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
						$outJANMAN=$outJANMAN."<td id='td_outJANMAN_issn_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->issn."</td>";
						$outJANMAN=$outJANMAN."<td id='td_outJANMAN_nomepub_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->nomepublicacao."</td>";
						$outJANMAN=$outJANMAN."<td id='td_outJANMAN_titulo_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->titulo."</td>";
						$outJANMAN=$outJANMAN."<td id='td_outJANMAN_autores_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->autores."</td>";
						$outJANMAN=$outJANMAN."<td id='td_outJANMAN_volume_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->volume."</td>";
						$outJANMAN=$outJANMAN."<td id='td_outJANMAN_issue_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->issue."</td>";
						$outJANMAN=$outJANMAN."<td id='td_outJANMAN_prim_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->primpagina."</td>";
						$outJANMAN=$outJANMAN."<td id='td_outJANMAN_ult_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->ultpagina."</td>";
						$outJANMAN=$outJANMAN."<td id='td_outJANMAN_link_" . $dadosDep[$i]->id . "'><a href='".$dadosDep[$i]->link."' target='_blank'>".$dadosDep[$i]->link."</a></td>";
						$outJANMAN=$outJANMAN."<td id='td_outJANMAN_estado_" . $dadosDep[$i]->id . "'>".getEstadoPublicacao($dadosDep[$i]->estado)."</td>";
						$outJANMAN=$outJANMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubNacMan-Sum').text('" . $dadosDep[$i]->id . "');$('#tabela').text('13');\"></td>";
						$outJANMAN=$outJANMAN."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-pubNacMan-Sum').text('" . $dadosDep[$i]->id . "');setObservacaoPubManNacSum();return false;\" ></center></td>";
						$outJANMAN=$outJANMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');apagarPublicacao();return false;\" ></center></td>";    		
					$outJANMAN=$outJANMAN."</tr>";

				}
            }
            break;			
            case 'OAN': {
				$checkAcao = $db->checkAcaoExists($dadosDep[$i]->id, $_SESSION['login'], 13);		
				if ($checkAcao) {
					$acao = transformIntoAcaoPubCPObject($dadosDep[$i]->id, $dadosDep[$i]->idinv, $checkAcao);	
					$outOANMAN=$outOANMAN."<tr>";
						$outOANMAN=$outOANMAN."<td>".$dadosDep[$i]->idinv."</td>";
						$outOANMAN=$outOANMAN."<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
						$outOANMAN=$outOANMAN."<td>".$dadosDep[$i]->isbn."</td>";
						$outOANMAN=$outOANMAN."<td>".$dadosDep[$i]->nomepublicacao."</td>";
						$outOANMAN=$outOANMAN."<td>".$dadosDep[$i]->editor."</td>";
						$outOANMAN=$outOANMAN."<td>".$dadosDep[$i]->titulo."</td>";
						$outOANMAN=$outOANMAN."<td>".$dadosDep[$i]->autores."</td>";
						$outOANMAN=$outOANMAN."<td>".$dadosDep[$i]->primpagina."</td>";
						$outOANMAN=$outOANMAN."<td>".$dadosDep[$i]->ultpagina."</td>";
						$outOANMAN=$outOANMAN."<td><a href='".$dadosDep[$i]->link."' target='_blank'>".$dadosDep[$i]->link."</a></td>";
						$outOANMAN=$outOANMAN."<td>".getEstadoPublicacao($dadosDep[$i]->estado)."</td>";
						$outOANMAN=$outOANMAN."<td></td>";
						$outOANMAN=$outOANMAN."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-pubNacMan-Oth').text('" . $dadosDep[$i]->id . "');setObservacaoPubManNacOth();return false;\" ></center></td>";
						$outOANMAN=$outOANMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');apagarPublicacao();return false;\" ></center></td>";    		
					$outOANMAN=$outOANMAN."</tr>";
					
					$outOANMAN=$outOANMAN."<tr>";
						$outOANMAN=$outOANMAN."<td style='background:#FFFF33; overflow:hidden;'><img src=\"../../images/arrow_return_down_right.png\" name='navOption'></td>";
						$outOANMAN=$outOANMAN."<td style='background:#FFFF33; overflow:hidden;'>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
						$outOANMAN=$outOANMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outOANMAN_isbn_" . $dadosDep[$i]->id . "'>".$acao['isbn']."</td>";
						$outOANMAN=$outOANMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outOANMAN_nomepub_" . $dadosDep[$i]->id . "'>".$acao['nomepublicacao']."</td>";
						$outOANMAN=$outOANMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outOANMAN_editor_" . $dadosDep[$i]->id . "'>".$acao['editor']."</td>";
						$outOANMAN=$outOANMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outOANMAN_titulo_" . $dadosDep[$i]->id . "'>".$acao['titulo']."</td>";
						$outOANMAN=$outOANMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outOANMAN_autores_" . $dadosDep[$i]->id . "'>".$acao['autores']."</td>";
						$outOANMAN=$outOANMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outOANMAN_prim_" . $dadosDep[$i]->id. "'>".$acao['primpagina']."</td>";
						$outOANMAN=$outOANMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outOANMAN_ult_" . $dadosDep[$i]->id . "'>".$acao['ultpagina']."</td>";
						$outOANMAN=$outOANMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outOANMAN_link_" . $dadosDep[$i]->id . "'><a href='".$acao['link']."' target='_blank'>".$acao['link']."</a></td>";
						$outOANMAN=$outOANMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outOANMAN_estado_" . $dadosDep[$i]->id . "'>".getEstadoPublicacao($acao['estado'])."</td>";
						$outOANMAN=$outOANMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubNacMan-Oth').text('" . $dadosDep[$i]->id . "');$('#tabela').text('13');\"></td>";
						$outOANMAN=$outOANMAN."<td></td>";
						$outOANMAN=$outOANMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"apagarAcao('" . $dadosDep[$i]->id . "',13);return false;\" ></center></td>";    		
					$outOANMAN=$outOANMAN."</tr>";
					
				} else { 
					$outOANMAN=$outOANMAN."<tr>";
						$outOANMAN=$outOANMAN."<td>".$dadosDep[$i]->idinv."</td>";
						$outOANMAN=$outOANMAN."<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
						$outOANMAN=$outOANMAN."<td id='td_outOANMAN_isbn_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->isbn."</td>";
						$outOANMAN=$outOANMAN."<td id='td_outOANMAN_nomepub_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->nomepublicacao."</td>";
						$outOANMAN=$outOANMAN."<td id='td_outOANMAN_editor_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->editor."</td>";
						$outOANMAN=$outOANMAN."<td id='td_outOANMAN_titulo_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->titulo."</td>";
						$outOANMAN=$outOANMAN."<td id='td_outOANMAN_autores_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->autores."</td>";
						$outOANMAN=$outOANMAN."<td id='td_outOANMAN_prim_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->primpagina."</td>";
						$outOANMAN=$outOANMAN."<td id='td_outOANMAN_ult_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->ultpagina."</td>";
						$outOANMAN=$outOANMAN."<td id='td_outOANMAN_link_" . $dadosDep[$i]->id . "'><a href='".$dadosDep[$i]->link."' target='_blank'>".$dadosDep[$i]->link."</a></td>";
						$outOANMAN=$outOANMAN."<td id='td_outOANMAN_estado_" . $dadosDep[$i]->id . "'>".getEstadoPublicacao($dadosDep[$i]->estado)."</td>";
						$outOANMAN=$outOANMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubNacMan-Oth').text('" . $dadosDep[$i]->id . "');$('#tabela').text('13');\"></td>";
						$outOANMAN=$outOANMAN."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-pubNacMan-Oth').text('" . $dadosDep[$i]->id . "');setObservacaoPubManNacOth();return false;\" ></center></td>";
						$outOANMAN=$outOANMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');apagarPublicacao();return false;\" ></center></td>";    		
					$outOANMAN=$outOANMAN."</tr>";
				}	
            }
            break;
            case 'LN': {				
				$checkAcao = $db->checkAcaoExists($dadosDep[$i]->id, $_SESSION['login'], 13);		
				if ($checkAcao) {
					$acao = transformIntoAcaoPubLivObject($dadosDep[$i]->id, $dadosDep[$i]->idinv, $checkAcao);	
					$outLNMAN=$outLNMAN."<tr>";
						$outLNMAN=$outLNMAN."<td>".$dadosDep[$i]->idinv."</td>";
						$outLNMAN=$outLNMAN."<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
						$outLNMAN=$outLNMAN."<td>".$dadosDep[$i]->isbn."</td>";
						$outLNMAN=$outLNMAN."<td>".$dadosDep[$i]->titulo."</td>";
						$outLNMAN=$outLNMAN."<td>".$dadosDep[$i]->autores."</td>";
						$outLNMAN=$outLNMAN."<td>".$dadosDep[$i]->editor."</td>";
						$outLNMAN=$outLNMAN."<td>".$dadosDep[$i]->editora."</td>";
						$outLNMAN=$outLNMAN."<td><a href='".$dadosDep[$i]->link."' target='_blank'>".$dadosDep[$i]->link."</a></td>";
						$outLNMAN=$outLNMAN."<td>".getEstadoPublicacao($dadosDep[$i]->estado)."</td>";
						$outLNMAN=$outLNMAN."<td></td>";
						$outLNMAN=$outLNMAN."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-pubNacMan-Liv').text('" . $dadosDep[$i]->id . "');setObservacaoPubManNacLiv();return false;\" ></center></td>";
						$outLNMAN=$outLNMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');apagarPublicacao();return false;\" ></center></td>";    		
					$outLNMAN=$outLNMAN."</tr>";
					
					$outLNMAN=$outLNMAN."<tr>";
						$outLNMAN=$outLNMAN."<td style='background:#FFFF33; overflow:hidden;'><img src=\"../../images/arrow_return_down_right.png\" name='navOption'></td>";
						$outLNMAN=$outLNMAN."<td style='background:#FFFF33; overflow:hidden;'>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
						$outLNMAN=$outLNMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outLNMAN_isbn_" . $dadosDep[$i]->id . "'>".$acao['isbn']."</td>";
						$outLNMAN=$outLNMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outLNMAN_titulo_" . $dadosDep[$i]->id . "'>".$acao['titulo']."</td>";
						$outLNMAN=$outLNMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outLNMAN_autores_" . $dadosDep[$i]->id . "'>".$acao['autores']."</td>";
						$outLNMAN=$outLNMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outLNMAN_editor_" . $dadosDep[$i]->id . "'>".$acao['editor']."</td>";
						$outLNMAN=$outLNMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outLNMAN_editora_" . $dadosDep[$i]->id . "'>".$acao['editora']."</td>";
						$outLNMAN=$outLNMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outLNMAN_link_" . $dadosDep[$i]->id . "'><a href='".$acao['link']."' target='_blank'>".$acao['link']."</a></td>";
						$outLNMAN=$outLNMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outLNMAN_estado_" . $dadosDep[$i]->id . "'>".getEstadoPublicacao($acao['estado'])."</td>";
						$outLNMAN=$outLNMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubNacMan-Liv').text('" . $dadosDep[$i]->id . "');$('#tabela').text('13');\"></td>";
						$outLNMAN=$outLNMAN."<td></td>";
						$outLNMAN=$outLNMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"apagarAcao('" . $dadosDep[$i]->id . "',13);return false;\" ></center></td>";    		
					$outLNMAN=$outLNMAN."</tr>";
				} else {
					$outLNMAN=$outLNMAN."<tr>";
						$outLNMAN=$outLNMAN."<td>".$dadosDep[$i]->idinv."</td>";
						$outLNMAN=$outLNMAN."<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
						$outLNMAN=$outLNMAN."<td id='td_outLNMAN_isbn_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->isbn."</td>";
						$outLNMAN=$outLNMAN."<td id='td_outLNMAN_titulo_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->titulo."</td>";
						$outLNMAN=$outLNMAN."<td id='td_outLNMAN_autores_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->autores."</td>";
						$outLNMAN=$outLNMAN."<td id='td_outLNMAN_editor_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->editor."</td>";
						$outLNMAN=$outLNMAN."<td id='td_outLNMAN_editora_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->editora."</td>";
						$outLNMAN=$outLNMAN."<td id='td_outLNMAN_link_" . $dadosDep[$i]->id . "'><a href='".$dadosDep[$i]->link."' target='_blank'>".$dadosDep[$i]->link."</a></td>";
						$outLNMAN=$outLNMAN."<td id='td_outLNMAN_estado_" . $dadosDep[$i]->id . "'>".getEstadoPublicacao($dadosDep[$i]->estado)."</td>";
						$outLNMAN=$outLNMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubNacMan-Liv').text('" . $dadosDep[$i]->id . "');$('#tabela').text('13');\"></td>";
						$outLNMAN=$outLNMAN."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-pubNacMan-Liv').text('" . $dadosDep[$i]->id . "');setObservacaoPubManNacLiv();return false;\" ></center></td>";
						$outLNMAN=$outLNMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');apagarPublicacao();return false;\" ></center></td>";    		
					$outLNMAN=$outLNMAN."</tr>";
				}
            }
            break;
            case 'CLN': {
			$checkAcao = $db->checkAcaoExists($dadosDep[$i]->id, $_SESSION['login'], 13);		
				if ($checkAcao) {
					$acao = transformIntoAcaoPubCLivObject($dadosDep[$i]->id, $dadosDep[$i]->idinv, $checkAcao);	
					$outCLNMAN=$outCLNMAN."<tr>";
						$outCLNMAN=$outCLNMAN."<td>".$dadosDep[$i]->idinv."</td>";
						$outCLNMAN=$outCLNMAN."<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
						$outCLNMAN=$outCLNMAN."<td>".$dadosDep[$i]->isbn."</td>";
						$outCLNMAN=$outCLNMAN."<td>".$dadosDep[$i]->nomepublicacao."</td>";
						$outCLNMAN=$outCLNMAN."<td>".$dadosDep[$i]->editora."</td>";
						$outCLNMAN=$outCLNMAN."<td>".$dadosDep[$i]->editor."</td>";
						$outCLNMAN=$outCLNMAN."<td>".$dadosDep[$i]->titulo."</td>";
						$outCLNMAN=$outCLNMAN."<td>".$dadosDep[$i]->autores."</td>";
						$outCLNMAN=$outCLNMAN."<td>".$dadosDep[$i]->primpagina."</td>";
						$outCLNMAN=$outCLNMAN."<td>".$dadosDep[$i]->ultpagina."</td>";
						$outCLNMAN=$outCLNMAN."<td><a href='".$dadosDep[$i]->link."' target='_blank'>".$dadosDep[$i]->link."</a></td>";
						$outCLNMAN=$outCLNMAN."<td>".getEstadoPublicacao($dadosDep[$i]->estado)."</td>";
						$outCLNMAN=$outCLNMAN."<td></td>";
						$outCLNMAN=$outCLNMAN."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-pubNacMan-CLiv').text('" . $dadosDep[$i]->id . "');setObservacaoPubManNacCLiv();return false;\" ></center></td>";
						$outCLNMAN=$outCLNMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');apagarPublicacao();return false;\" ></center></td>";    		
					$outCLNMAN=$outCLNMAN."</tr>";
					
					$outCLNMAN=$outCLNMAN."<tr>";
						$outCLNMAN=$outCLNMAN."<td style='background:#FFFF33; overflow:hidden;'><img src=\"../../images/arrow_return_down_right.png\" name='navOption'></td>";
						$outCLNMAN=$outCLNMAN."<td style='background:#FFFF33; overflow:hidden;'>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
						$outCLNMAN=$outCLNMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outCLNMAN_isbn_" . $dadosDep[$i]->id . "'>".$acao['isbn']."</td>";
						$outCLNMAN=$outCLNMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outCLNMAN_nomepub_" . $dadosDep[$i]->id . "'>".$acao['nomepublicacao']."</td>";
						$outCLNMAN=$outCLNMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outCLNMAN_editora_" . $dadosDep[$i]->id . "'>".$acao['editora']."</td>";
						$outCLNMAN=$outCLNMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outCLNMAN_editor_" . $dadosDep[$i]->id . "'>".$acao['editor']."</td>";
						$outCLNMAN=$outCLNMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outCLNMAN_titulo_" . $dadosDep[$i]->id . "'>".$acao['titulo']."</td>";
						$outCLNMAN=$outCLNMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outCLNMAN_autores_" . $dadosDep[$i]->id . "'>".$acao['autores']."</td>";
						$outCLNMAN=$outCLNMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outCLNMAN_prim_" . $dadosDep[$i]->id . "'>".$acao['primpagina']."</td>";
						$outCLNMAN=$outCLNMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outCLNMAN_ult_" . $dadosDep[$i]->id . "'>".$acao['ultpagina']."</td>";
						$outCLNMAN=$outCLNMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outCLNMAN_link_" . $dadosDep[$i]->id . "'><a href='".$acao['link']."' target='_blank'>".$acao['link']."</a></td>";
						$outCLNMAN=$outCLNMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outCLNMAN_estado_" . $dadosDep[$i]->id . "'>".getEstadoPublicacao($acao['estado'])."</td>";
						$outCLNMAN=$outCLNMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubNacMan-CLiv').text('" . $dadosDep[$i]->id . "');$('#tabela').text('13');\"></td>";
						$outCLNMAN=$outCLNMAN."<td></td>";
						$outCLNMAN=$outCLNMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"apagarAcao('" . $dadosDep[$i]->id . "',13);return false;\" ></center></td>";    		
					$outCLNMAN=$outCLNMAN."</tr>";
				} else {
					$outCLNMAN=$outCLNMAN."<tr>";
						$outCLNMAN=$outCLNMAN."<td>".$dadosDep[$i]->idinv."</td>";
						$outCLNMAN=$outCLNMAN."<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
						$outCLNMAN=$outCLNMAN."<td id='td_outCLNMAN_isbn_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->isbn."</td>";
						$outCLNMAN=$outCLNMAN."<td id='td_outCLNMAN_nomepub_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->nomepublicacao."</td>";
						$outCLNMAN=$outCLNMAN."<td id='td_outCLNMAN_editora_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->editora."</td>";
						$outCLNMAN=$outCLNMAN."<td id='td_outCLNMAN_editor_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->editor."</td>";
						$outCLNMAN=$outCLNMAN."<td id='td_outCLNMAN_titulo_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->titulo."</td>";
						$outCLNMAN=$outCLNMAN."<td id='td_outCLNMAN_autores_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->autores."</td>";
						$outCLNMAN=$outCLNMAN."<td id='td_outCLNMAN_prim_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->primpagina."</td>";
						$outCLNMAN=$outCLNMAN."<td id='td_outCLNMAN_ult_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->ultpagina."</td>";
						$outCLNMAN=$outCLNMAN."<td id='td_outCLNMAN_link_" . $dadosDep[$i]->id . "'><a href='".$dadosDep[$i]->link."' target='_blank'>".$dadosDep[$i]->link."</a></td>";
						$outCLNMAN=$outCLNMAN."<td id='td_outCLNMAN_estado_" . $dadosDep[$i]->id . "'>".getEstadoPublicacao($dadosDep[$i]->estado)."</td>";
						$outCLNMAN=$outCLNMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubNacMan-CLiv').text('" . $dadosDep[$i]->id . "');$('#tabela').text('13');\"></td>";						
						$outCLNMAN=$outCLNMAN."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-pubNacMan-CLiv').text('" . $dadosDep[$i]->id . "');setObservacaoPubManNacCLiv();return false;\" ></center></td>";
						$outCLNMAN=$outCLNMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');apagarPublicacao();return false;\" ></center></td>";    		
					$outCLNMAN=$outCLNMAN."</tr>";
				}
            }
            break;
        }
	}

	echo "<h3>Publicações Manuais Não Inglês</h3>\n";
    echo "<div id='pubManualNacional'>";
        echo"<div id='pubManualNac-Art'><table id='pubManNac-Art' class='box-table-b'><caption >ARTIGOS</caption>";echo $tcolsJ;echo "<th colspan='3'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPub(\"J\",\"NAC\");return false;'></center></th></tr></thead><tbody>";echo $outJNMAN; echo "</body></table><p id='chave-pubNacMan-Art' hidden></p></div><br>";
        echo"<div id='pubManualNac-Rev'><table id='pubManNac-Rev' class='box-table-b'><caption >PEER REVIEW PROCEEDINGS</caption>";echo $tcolsJ;echo "<th colspan='3'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPub(\"PRP\",\"NAC\");return false;'></center></th></tr></thead><tbody>";echo $outPRPNMAN;echo "</body></table><p id='chave-pubNacMan-Rev' hidden></div><br>";
        echo"<div id='pubManualNac-Sum'><table id='pubManNac-Sum' class='box-table-b'><caption >SUMÁRIOS<p><i>JOURNAL ABSTRACTS</i></p></caption>";echo $tcolsJ;echo "<th colspan='3'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPub(\"S\",\"NAC\");return false;'></center></th></tr></thead><tbody>";echo $outJANMAN;echo "</body></table><p id='chave-pubNacMan-Sum' hidden></div><br>";
        echo"<div id='pubManualNac-Liv'><table id='pubManNac-Liv' class='box-table-b'><caption >LIVROS<p><i>BOOKS</i></p></caption>";echo $tcolsLIV;echo "<th colspan='3'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPub(\"L\",\"NAC\");return false;'></center></th></tr></thead><tbody>";echo $outLNMAN;echo "</body></table><p id='chave-pubNacMan-Liv' hidden></div><br>";
        echo"<div id='pubManualNac-CLiv'><table id='pubManNac-CLiv' class='box-table-b'><caption >CAPITULOS DE LIVROS<p><i>BOOK CHAPTERS</i></p></caption>";echo $tcolsCLIV;echo "<th colspan='3'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPub(\"CL\",\"NAC\");return false;'></center></th></tr></thead><tbody>";echo $outCLNMAN;echo "</body></table><p id='chave-pubNacMan-CLiv' hidden></div><br>";
        echo"<div id='pubManualNac-Con'><table id='pubManNac-Con' class='box-table-b'><caption >CONFERENCE PROCEEDINGS</caption>";echo $tcolsCONF;echo "<th colspan='3'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPub(\"CP\",\"NAC\");return false;'></center></th></tr></thead><tbody>";echo $outCPNMAN;echo "</body></table><p id='chave-pubNacMan-Con' hidden></div><br>";
        echo"<div id='pubManualNac-Oth'><table id='pubManNac-Oth' class='box-table-b'><caption >OUTROS SUMÁRIOS<br>OTHER ABSTRACTS</caption>";echo $tcolsCONF;echo "<th colspan='3'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPub(\"OA\",\"NAC\");return false;'></center></th></tr></thead><tbody>";echo $outOANMAN;echo "</body></table><p id='chave-pubNacMan-Oth' hidden></div><br>";
    echo "</div>";
}

function getEstadoPublicacao($i) {
	$db = new Database();
	$lValues =$db->getLookupValues("lista_estadoPublicacoes");
	$texto="";
	while ($row = mysql_fetch_assoc($lValues)) {
		if($i==$row["ID"])
			$texto=$row["DESCRICAO"];
	}
	
	$db->disconnect();
	return $texto;
}

?>