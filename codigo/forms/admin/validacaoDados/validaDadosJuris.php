<?php

function showEditJuris($dadosDep) {
	$db = new Database();
	
	echo "<h3>Júris</h3>\n
		  <div id='juris'>";		
				
	echo "<table id='juri' class='box-table-b'>
			<thead>
				<tr>
					<th colspan='2'></th>
					<th colspan='4'>Agregacao</th>
					<th colspan='4'>Doutoramento</i></th>
					<th colspan='4'>Mestrado Científico</i></th>
					<th colspan='4'>Mestrado Integrado</i></th>
				</tr>
				<tr>
					<td colspan='2'></td>
					<td colspan='2'>FMUP</td>
					<td colspan='2'>Exterior</td>
					<td colspan='2'>FMUP</td>
					<td colspan='2'>Exterior</td>
					<td colspan='2'>FMUP</td>
					<td colspan='2'>Exterior</td>
					<td colspan='2'>FMUP</td>
					<td colspan='2'>Exterior</td>
				</tr>
				<tr>
					<td>IDINV</td>
					<td>Nome</td>
					<td>Arguente</td>
					<td>Outro</td>
					<td>Arguente</td>
					<td>Outro</td>
					<td>Arguente</td>
					<td>Outro</td>
					<td>Arguente</td>
					<td>Outro</td>
					<td>Arguente</td>
					<td>Outro</td>
					<td>Arguente</td>
					<td>Outro</td>
					<td>Arguente</td>
					<td>Outro</td>
					<td>Arguente</td>
					<td>Outro</td>					
					<th colspan='3'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewJuri();return false;'></center></th>
				</tr>
			</thead>
			<tbody>";

	foreach ($dadosDep as $i => $value) {
		if($dadosDep[$i]->agregacaoargfmup!=0 ||
			$dadosDep[$i]->agregacaovogalfmup!=0 ||
			$dadosDep[$i]->agregacaoargext!=0 ||
			$dadosDep[$i]->agregacaovogalext!=0 ||
			$dadosDep[$i]->doutoramentoargfmup!=0 ||
			$dadosDep[$i]->doutoramentovogalfmup!=0 ||
			$dadosDep[$i]->doutoramentoargext!=0 ||
			$dadosDep[$i]->doutoramentovogalext!=0 ||
			$dadosDep[$i]->mestradoargfmup!=0 ||
			$dadosDep[$i]->mestradovogalfmup!=0 ||
			$dadosDep[$i]->mestradoargext!=0 ||
			$dadosDep[$i]->mestradovogalext!=0 ||
			$dadosDep[$i]->mestradointargfmup!=0 ||
			$dadosDep[$i]->mestradointvogalfmup!=0 ||
			$dadosDep[$i]->mestradointargext!=0 ||
			$dadosDep[$i]->mestradointvogalext!=0){
			$checkAcao = $db->checkAcaoExists($dadosDep[$i]->id, $_SESSION['login'], 23);
			if ($checkAcao) {
				$acao = transformIntoJuriObject($dadosDep[$i]->id, $dadosDep[$i]->idinv, $checkAcao);
				echo "<tr>";
					echo "<td>".$dadosDep[$i]->idinv."</td>";					
					echo "<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
					echo "<td>".$dadosDep[$i]->agregacaoargfmup."</td>";
					echo "<td>".$dadosDep[$i]->agregacaovogalfmup."</td>";
					echo "<td>".$dadosDep[$i]->agregacaoargext."</td>";
					echo "<td>".$dadosDep[$i]->agregacaovogalext."</td>";
					echo "<td>".$dadosDep[$i]->doutoramentoargfmup."</td>";
					echo "<td>".$dadosDep[$i]->doutoramentovogalfmup."</td>";
					echo "<td>".$dadosDep[$i]->doutoramentoargext."</td>";
					echo "<td>".$dadosDep[$i]->doutoramentovogalext."</td>";
					echo "<td>".$dadosDep[$i]->mestradoargfmup."</td>";
					echo "<td>".$dadosDep[$i]->mestradovogalfmup."</td>";
					echo "<td>".$dadosDep[$i]->mestradoargext."</td>";
					echo "<td>".$dadosDep[$i]->mestradovogalext."</td>";
					echo "<td>".$dadosDep[$i]->mestradointargfmup."</td>";
					echo "<td>".$dadosDep[$i]->mestradointvogalfmup."</td>";
					echo "<td>".$dadosDep[$i]->mestradointargext."</td>";
					echo "<td>".$dadosDep[$i]->mestradointvogalext."</td>";
					echo "<td></td>";
					echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-juris').text('" . $dadosDep[$i]->id . "');setObservacaoJuri();return false;\" ></center></td>";
					echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-juris').text('" . $dadosDep[$i]->id . "');apagarJuri();return false;\" ></center></td>";   
				echo "</tr>";
				
				echo "<tr>";
					echo "<td style='background:#FFFF33; overflow:hidden;'><img src=\"../../images/arrow_return_down_right.png\" name='navOption'></td>";
					echo "<td style='background:#FFFF33; overflow:hidden;'>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_juris_agrargfmup_" . $dadosDep[$i]->id . "'>".$acao['agregacaoargfmup']."</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_juris_agrvogalfmup_" . $dadosDep[$i]->id . "'>".$acao['agregacaovogalfmup']."</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_juris_agrargext_" . $dadosDep[$i]->id . "'>".$acao['agregacaoargext']."</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_juris_agrvogalext_" . $dadosDep[$i]->id . "'>".$acao['agregacaovogalext']."</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_juris_doutargfmup_" . $dadosDep[$i]->id . "'>".$acao['doutoramentoargfmup']."</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_juris_doutvogalfmup_" . $dadosDep[$i]->id . "'>".$acao['doutoramentovogalfmup']."</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_juris_doutargext_" . $dadosDep[$i]->id . "'>".$acao['doutoramentoargext']."</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_juris_doutvogalext_" . $dadosDep[$i]->id . "'>".$acao['doutoramentovogalext']."</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_juris_mesargfmup_" . $dadosDep[$i]->id . "'>".$acao['mestradoargfmup']."</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_juris_mesvogalfmup_" . $dadosDep[$i]->id . "'>".$acao['mestradovogalfmup']."</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_juris_mesargext_" . $dadosDep[$i]->id . "'>".$acao['mestradoargext']."</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_juris_mesvogalext_" . $dadosDep[$i]->id . "'>".$acao['mestradovogalext']."</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_juris_mesintfmup_" . $dadosDep[$i]->id . "'>".$acao['mestradointargfmup']."</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_juris_mesintvogalfmup_" . $dadosDep[$i]->id . "'>".$acao['mestradointvogalfmup']."</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_juris_mesintargext_" . $dadosDep[$i]->id . "'>".$acao['mestradointargext']."</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_juris_mesintvogalext_" . $dadosDep[$i]->id . "'>".$acao['mestradointvogalext']."</td>";
					echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-juris').text('" . $dadosDep[$i]->id . "');\"></td>";
					echo "<td></td>";
					echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"apagarAcao('" . $dadosDep[$i]->id . "',23);return false;\" ></center></td>";   
				echo "</tr>";
				
			} else {
				echo "<tr>";
					echo "<td>".$dadosDep[$i]->idinv."</td>";
					echo "<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
					echo "<td id='td_juris_agrargfmup_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->agregacaoargfmup."</td>";
					echo "<td id='td_juris_agrvogalfmup_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->agregacaovogalfmup."</td>";
					echo "<td id='td_juris_agrargext_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->agregacaoargext."</td>";
					echo "<td id='td_juris_agrvogalext_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->agregacaovogalext."</td>";
					echo "<td id='td_juris_doutargfmup_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->doutoramentoargfmup."</td>";
					echo "<td id='td_juris_doutvogalfmup_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->doutoramentovogalfmup."</td>";
					echo "<td id='td_juris_doutargext_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->doutoramentoargext."</td>";
					echo "<td id='td_juris_doutvogalext_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->doutoramentovogalext."</td>";
					echo "<td id='td_juris_mesargfmup_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->mestradoargfmup."</td>";
					echo "<td id='td_juris_mesvogalfmup_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->mestradovogalfmup."</td>";
					echo "<td id='td_juris_mesargext_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->mestradoargext."</td>";
					echo "<td id='td_juris_mesvogalext_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->mestradovogalext."</td>";
					echo "<td id='td_juris_mesintfmup_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->mestradointargfmup."</td>";
					echo "<td id='td_juris_mesintvogalfmup_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->mestradointvogalfmup."</td>";
					echo "<td id='td_juris_mesintargext_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->mestradointargext."</td>";
					echo "<td id='td_juris_mesintvogalext_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->mestradointvogalext."</td>";
					echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-juris').text('" . $dadosDep[$i]->id . "');\"></td>";
					echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-juris').text('" . $dadosDep[$i]->id . "');setObservacaoJuri();return false;\" ></center></td>";
					echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-juris').text('" . $dadosDep[$i]->id . "');apagarJuri();return false;\" ></center></td>";   
				echo "</tr>";
			}	
		}
	}
	
	echo "</tbody>
	</table>
	<p id='chave-juris' hidden ></p>
</div>";
		
}

function transformIntoJuriObject($id, $idinv, $query) {
	$acao = array();
	
	$cena = explode("AGREGACAOARGFMUP=",$query);
				
	$cena1 = explode(", AGREGACAOVOGALFMUP=",$cena[1]);
	$acao['agregacaoargfmup'] = $cena1[0];
	
	$cena2 = explode(", AGREGACAOARGEXT=",$cena1[1]);
	$acao['agregacaovogalfmup'] = $cena2[0];
	
	$cena3 = explode(", AGREGACAOVOGALEXT=",$cena2[1]);
	$acao['agregacaoargext']= $cena3[0];
	
	$cena4 = explode(", DOUTORAMENTOARGFMUP=",$cena3[1]);
	$acao['agregacaovogalext']= $cena4[0];
	
	$cena5 = explode(", DOUTORAMENTOVOGALFMUP=",$cena4[1]);
	$acao['doutoramentoargfmup']= $cena5[0];
	
	$cena6 = explode(", DOUTORAMENTOARGEXT=",$cena5[1]);
	$acao['doutoramentovogalfmup']= $cena6[0];
			
	$cena7 = explode(", DOUTORAMENTOVOGALEXT=",$cena6[1]);
	$acao['doutoramentoargext']= $cena7[0];
	
	$cena8 = explode(", MESTRADOARGFMUP=",$cena7[1]);
	$acao['doutoramentovogalext'] = $cena8[0];
	
	$cena9 = explode(", MESTRADOVOGALFMUP=",$cena8[1]);
	$acao['mestradoargfmup'] = $cena9[0];
	
	$cena10 = explode(", MESTRADOARGEXT=",$cena9[1]);
	$acao['mestradovogalfmup'] = $cena10[0];
	
	$cena11 = explode(", MESTRADOVOGALEXT=",$cena10[1]);
	$acao['mestradoargext'] = $cena11[0];
	
	$cena12 = explode(", MESTRADOINTARGFMUP=",$cena11[1]);
	$acao['mestradovogalext'] = $cena12[0];
	
	$cena13 = explode(", MESTRADOINTVOGALFMUP=",$cena12[1]);
	$acao['mestradointargfmup'] = $cena13[0];
	
	$cena14 = explode(", MESTRADOINTARGEXT=",$cena13[1]);
	$acao['mestradointvogalfmup'] = $cena14[0];
	
	$cena15 = explode(", MESTRADOINTVOGALEXT=",$cena14[1]);
	$acao['mestradointargext'] = $cena15[0];
	
	$cena16 = explode(" where",$cena15[1]);
	$acao['mestradointvogalext'] = $cena16[0];
			
	return $acao;
}

?>