<?php

function showEditMissoes($dadosDep)
{
    $db = new Database();
    
    echo "<h3>Missões</h3>\n
          <div id='missoes'>";
    
    echo "<table id='missao' class='box-table-b'>
            <thead>
                <tr>
                    <th>ID Inv</th>
					<th>Nome</th>
                    <th>Data de Inicio</th>
                    <th>Data de Fim</th>
                    <th>Motivacao</th>
                    <th>Instituição de Acolhimento</th>
                    <th>Pais</th>
                    <th>Âmbito de Tese</th>					
					<th colspan='3'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewMissao();return false;'></center></th>
                </tr>
            </thead>
            <tbody>";
    
    
    foreach ($dadosDep as $i => $value) {
        $checkAcao = $db->checkAcaoExists($dadosDep[$i]->id, $_SESSION['login'], 21);
        if ($checkAcao) {
            $acao = transformIntoAcaoMissaoObject($dadosDep[$i]->id, $dadosDep[$i]->idinv, $checkAcao);
            echo "<tr>";
				echo "<td>" . $dadosDep[$i]->idinv . "</td>";
				echo "<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
				echo "<td>" . $dadosDep[$i]->datainicio . "</td>";
				echo "<td>" . $dadosDep[$i]->datafim . "</td>";
				echo "<td>" . $dadosDep[$i]->motivacao . "</td>";
				echo "<td>" . $dadosDep[$i]->instituicao . "</td>";
				echo "<td>";
				getPaisesMissoes($dadosDep[$i]->pais);
				echo "</td>";
				echo "<td>" . checkAmbTese($dadosDep[$i]->ambtese) . "</td>";
				echo "<td></td>";
				echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-missoes').text('" . $dadosDep[$i]->id . "');setObservacaoMissao();return false;\" ></center></td>";
				echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-missoes').text('" . $dadosDep[$i]->id . "');apagarMissao();return false;\" ></center></td>";
            echo "</tr>";
            
            echo "<tr>";
				echo "<td style='background:#FFFF33; overflow:hidden;'><img src=\"../../images/arrow_return_down_right.png\" name='navOption'></td>";				
				echo "<td style='background:#FFFF33; overflow:hidden;'>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_missoes_dataini_" . $dadosDep[$i]->id . "'>" . $acao['datainicio'] . "</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_missoes_datafim_" . $dadosDep[$i]->id . "'>" . $acao['datafim'] . "</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_missoes_motivacao_" . $dadosDep[$i]->id . "'>" . $acao['motivacao'] . "</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_missoes_instituicao_" . $dadosDep[$i]->id . "'>" . $acao['instituicao'] . "</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_missoes_pais_" . $dadosDep[$i]->id . "'>";
				getPaisesMissoes($acao['pais']);
				echo "</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_missoes_ambtese_" . $dadosDep[$i]->id . "'>" . checkAmbTese($acao['ambtese']) . "</td>";
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-missoes').text('" . $dadosDep[$i]->id . "');\"></td>";
				echo "<td></td>";
				echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"apagarAcao('" . $dadosDep[$i]->id . "',21);return false;\" ></center></td>";
            echo "</tr>";
            
        } else {
            
            echo "<tr>";
				echo "<td>" . $dadosDep[$i]->idinv . "</td>";
				echo "<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
				echo "<td id='td_missoes_dataini_" . $dadosDep[$i]->id . "'>" . $dadosDep[$i]->datainicio . "</td>";
				echo "<td id='td_missoes_datafim_" . $dadosDep[$i]->id . "'>" . $dadosDep[$i]->datafim . "</td>";
				echo "<td id='td_missoes_motivacao_" . $dadosDep[$i]->id . "'>" . $dadosDep[$i]->motivacao . "</td>";
				echo "<td id='td_missoes_instituicao_" . $dadosDep[$i]->id . "'>" . $dadosDep[$i]->instituicao . "</td>";
				echo "<td id='td_missoes_pais_" . $dadosDep[$i]->id . "'>";
				getPaisesMissoes($dadosDep[$i]->pais);
				echo "</td>";
				echo "<td id='td_missoes_ambtese_" . $dadosDep[$i]->id . "'>" . checkAmbTese($dadosDep[$i]->ambtese) . "</td>";
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-missoes').text('" . $dadosDep[$i]->id . "');\"></td>";
				echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-missoes').text('" . $dadosDep[$i]->id . "');setObservacaoMissao();return false;\" ></center></td>";
				echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-missoes').text('" . $dadosDep[$i]->id . "');apagarMissao();return false;\" ></center></td>";
            echo "</tr>";
        }
    }
    
    echo "</tbody>
    </table>
    <p id='chave-missoes' hidden></p>
    </div>";
}

function transformIntoAcaoMissaoObject($id, $idinv, $query)
{
    $acao = array();
        
    $cena = explode("PAIS=", $query);
    
    $cena1 = explode(", DATAINICIO='", $cena[1]);
    $acao['pais'] = $cena1[0];
    
    $cena2 = explode("', DATAFIM='", $cena1[1]);
    $acao['datainicio'] = $cena2[0];
    
    $cena3 = explode("', INSTITUICAO='", $cena2[1]);
    $acao['datafim'] = $cena3[0];
    
    $cena4 = explode("', MOTIVACAO='", $cena3[1]);
    $acao['instituicao'] = $cena4[0];
    
    $cena5 = explode("', AMBTESE='", $cena4[1]);
    $acao['motivacao'] = $cena5[0];
    
    $cena6 = explode("' where ", $cena5[1]);
    $acao['ambtese'] = $cena6[0];
    
    return $acao;
}

function getPaisesMissoes($i)
{
    $db      = new Database();
    $lValues = $db->getLookupValues("lista_paises");
    while ($row = mysql_fetch_assoc($lValues)) {
        if ($i == $row["ID"])
            echo $row["DESCRICAO"];
    }
    $db->disconnect();
}

function checkAmbTese($i)
{
    global $dadosDep;
    if ($i == 1)
        return "Sim";
    else
        return "Não";
}

?>