<?php

function showEditAcoesDivulgacao($dadosDep) {
	$db = new Database();
		
	echo "<h3>Ações de Divulgação</h3>\n";
			
	echo "<div id='acaodivulgacao'>
		  <table id='acaoDiv' class='box-table-b'>
		  <thead>
			<tr>
				<th>IDINV</th>
				<th>Nome</th>
				<th>Título</th>
				<th>Data</th>
				<th>Local</th>
				<th>Nº de Participantes</th>
				<th colspan='3'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewAcaoDiv();return false;'></center></th>
			</tr>
		</thead>
		<tbody>";
		
	foreach ($dadosDep as $i => $value) {				
		$checkAcao = $db->checkAcaoExists($dadosDep[$i]->id, $_SESSION['login'], 25);
		if ($checkAcao) {		
			$acao = transformIntoAcaoDivObject($dadosDep[$i]->id, $dadosDep[$i]->idinv, $checkAcao);
			echo "<tr>";
				echo "<td>". $dadosDep[$i]->idinv. "</td>";
				echo "<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
				echo "<td>". $dadosDep[$i]->titulo ."</td>";
				echo "<td>". $dadosDep[$i]->data ."</td>";
				echo "<td>". $dadosDep[$i]->local ."</td>";
				echo "<td>". $dadosDep[$i]->npart ."</td>";
				echo "<td></td>";
				echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-acaodivulgacao').text('" . $dadosDep[$i]->id . "');setObservacaoAcaoDiv();return false;\" ></center></td>";
				echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-acaodivulgacao').text('" . $dadosDep[$i]->id . "');apagarAcaoDivulgacao();return false;\" ></center></td>";   
			echo "</tr>";
			
			echo "<tr>";
				echo "<td style='background:#FFFF33; overflow:hidden;'><img src=\"../../images/arrow_return_down_right.png\" name='navOption'></td>";
				echo "<td style='background:#FFFF33; overflow:hidden;'>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_acaodivulgacao_titulo_" .$dadosDep[$i]->id. "'>". $acao['titulo'] ."</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_acaodivulgacao_data_" .$dadosDep[$i]->id. "'>". $acao['data'] ."</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_acaodivulgacao_local_" .$dadosDep[$i]->id. "'>". $acao['local'] ."</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_acaodivulgacao_npart_" .$dadosDep[$i]->id. "'>". $acao['npart'] ."</td>";
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-acaodivulgacao').text('" . $dadosDep[$i]->id . "');\"></td>";
				echo "<td></td>";
				echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"apagarAcao('" . $dadosDep[$i]->id . "',25);return false;\" ></center></td>";   
			echo "</tr>";
			
		} else {
		
			echo "<tr>";
				echo "<td>". $dadosDep[$i]->idinv. "</td>";
				echo "<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
				echo "<td id='td_acaodivulgacao_titulo_" .$dadosDep[$i]->id. "'>". $dadosDep[$i]->titulo ."</td>";
				echo "<td id='td_acaodivulgacao_data_" .$dadosDep[$i]->id. "'>". $dadosDep[$i]->data ."</td>";
				echo "<td id='td_acaodivulgacao_local_" .$dadosDep[$i]->id. "'>". $dadosDep[$i]->local ."</td>";
				echo "<td id='td_acaodivulgacao_npart_" .$dadosDep[$i]->id. "'>". $dadosDep[$i]->npart ."</td>";
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-acaodivulgacao').text('" . $dadosDep[$i]->id . "');\"></td>";
				echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-acaodivulgacao').text('" . $dadosDep[$i]->id . "');setObservacaoAcaoDiv();return false;\" ></center></td>";
				echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-acaodivulgacao').text('" . $dadosDep[$i]->id . "');apagarAcaoDivulgacao();return false;\" ></center></td>";   
			echo "</tr>";
		}
	}	
	
    echo "</tbody>
    </table>
    <p id='chave-acaodivulgacao' hidden></p>
</div>";	
}

function transformIntoAcaoDivObject($id, $idinv, $query) {
	$acao = array();
	
	$cena = explode("TITULO='",$query);
				
	$cena1 = explode("', DATA='",$cena[1]);
	$acao['titulo'] = $cena1[0];
	
	$cena2 = explode("', LOCAL='",$cena1[1]);
	$acao['data'] = $cena2[0];
	
	$cena3 = explode("', NPART=",$cena2[1]);
	$acao['local']= $cena3[0];
	
	$cena4 = explode(" where",$cena3[1]);
	$acao['npart']= $cena4[0];	
			
	return $acao;
}
?>