<?php

function showEditRedes($dadosDep) {
	$db = new Database();
	echo "<h3>Redes Científicas</h3>\n";
	echo "<div id='redes'>";
	echo "<table id='rede' class='box-table-b'>
			<thead>
				<tr>
					<th>ID Inv</th>		
					<th>Nome Investigador</th>
					<th>Nome Rede</th>
					<th>Tipo</th>
					<th>Data Início</th>
					<th>Data Fim</th>
					<th>Link</th>
					<th colspan='3'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewRede();return false;'></center></th>
				</tr>
			</thead>
			<tbody>";
			
	foreach ($dadosDep as $i => $value){
		$checkAcao = $db->checkAcaoExists($dadosDep[$i]->id, $_SESSION['login'], 20);		
		if ($checkAcao) {
			$acao = transformIntoAcaoRedesObject($dadosDep[$i]->id, $dadosDep[$i]->idinv, $checkAcao);	
			echo "<tr>";
				echo "<td>".$dadosDep[$i]->idinv."</td>";
				echo "<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
				echo "<td>".$dadosDep[$i]->nome."</td>";
				echo "<td>";getTipoRedes($dadosDep[$i]->tipo);echo "</td>";
				echo "<td>".$dadosDep[$i]->datainicio."</td>";	
				echo "<td>".$dadosDep[$i]->datafim."</td>";	
				echo "<td><a href='".$dadosDep[$i]->link."' target='_blank' >".$dadosDep[$i]->link."</a></td>";
				echo "<td></td>";
				echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-redes').text('" . $dadosDep[$i]->id . "');setObservacaoRede();return false;\" ></center></td>";				
				echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-redes').text('" . $dadosDep[$i]->id . "');apagarRede();return false;\" ></center></td>";
			echo "</tr>";	
			
			echo "<tr>";
				echo "<td style='background:#FFFF33; overflow:hidden;'><img src=\"../../images/arrow_return_down_right.png\" name='navOption'></td>";	
				echo "<td style='background:#FFFF33; overflow:hidden;'>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_redes_nome_" .$dadosDep[$i]->id. "'>".$acao['nome']."</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_redes_tipo_" .$dadosDep[$i]->id. "'>";getTipoRedes($acao['tipo']);echo "</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_redes_datainicio_" .$dadosDep[$i]->id. "'>".$acao['datainicio']."</td>";	
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_redes_datafim_" .$dadosDep[$i]->id. "'>".$acao['datafim']."</td>";	
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_redes_link_" .$dadosDep[$i]->id. "'><a href='".$acao['link']."' target='_blank' >".$acao['link']."</a></td>";
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-redes').text('" . $dadosDep[$i]->id . "');\"></td>";
				echo "<td></td>";
				echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"apagarAcao('" . $dadosDep[$i]->id . "',20);return false;\" ></center></td>";
			echo "</tr>";	 
			
		} else {
		
			echo "<tr>";
				echo "<td>".$dadosDep[$i]->idinv."</td>";
				echo "<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
				echo "<td id='td_redes_nome_" .$dadosDep[$i]->id. "'>".$dadosDep[$i]->nome."</td>";
				echo "<td id='td_redes_tipo_" .$dadosDep[$i]->id. "'>";getTipoRedes($dadosDep[$i]->tipo);	echo "</td>";
				echo "<td id='td_redes_datainicio_" .$dadosDep[$i]->id. "'>".$dadosDep[$i]->datainicio."</td>";	
				echo "<td id='td_redes_datafim_" .$dadosDep[$i]->id. "'>".$dadosDep[$i]->datafim."</td>";	
				echo "<td id='td_redes_link_" .$dadosDep[$i]->id. "'><a href='".$dadosDep[$i]->link."' target='_blank' >".$dadosDep[$i]->link."</a></td>";
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-redes').text('" . $dadosDep[$i]->id . "');\"></td>";
				echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-redes').text('" . $dadosDep[$i]->id . "');setObservacaoRede();return false;\" ></center></td>";				
				echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-redes').text('" . $dadosDep[$i]->id . "');apagarRede();return false;\" ></center></td>";
			echo "</tr>";	 
			
		}		
	}
	
	echo "</tbody>
    </table>
    <p id='chave-redes' hidden></p>
</div>";
}

function transformIntoAcaoRedesObject($id, $idinv, $query) {
	$acao = array();	
	
	$cena = explode("TIPO='",$query);
				
	$cena1 = explode("', DATAINICIO='",$cena[1]);
	$acao['tipo'] = $cena1[0];
	
	$cena2 = explode("', DATAFIM='",$cena1[1]);
	$acao['datainicio']= $cena2[0];
	
	$cena3 = explode("', LINK='",$cena2[1]);
	$acao['datafim'] = $cena3[0];	
	
	$cena4 = explode("', NOME='",$cena3[1]);
	$acao['link'] = $cena4[0];	
	
	$cena5 = explode("' where ",$cena4[1]);
	$acao['nome'] = $cena5[0];	
		
	return $acao;
}

function getTipoRedes($i) {	
	$db = new Database();
	$lValues =$db->getLookupValues("lista_tiporedes");

	while ($row = mysql_fetch_assoc($lValues)) {
		if($i==$row["ID"])
			echo $row["DESCRICAO"];
	}
	$db->disconnect();				
}	
?>