<?php
//PASSO 1 PMARQUES e 5
function showEditHabilitacoes($dadosDep) {
	$db = new Database();

	echo "<h4>Curso(s) a frequentar</h4>\n";

	echo "<div id='habilitacoes'>\n";

	//echo "<legend>Curso(s) superior(es) a frequentar ou concluído(s)/<i>Graduate course(s) attended or finished</i></legend>\n";
				

	echo "<table class='box-table-b' id='habs'>
			<thead>
				<tr>
					<th>ID Inv</th>		
					<th>Instituição</th>			
					<th>Grau/Título</th>					
					<th>Curso</th>
					<th>Nome</th>
					<th>Ano de Início</th>
					<th>Ano de Fim</th>
					<th>Distinção</th>
					<th>Bolsa</th>
					<th>Título da Tese em Português</th>
					<th>Percentagem</th>
					<th>Orientador<p><i>Supervisor</i></p></th>
					<th>Instituição do Orientador<p><i>Supervisor Institution</i></p></th>
					<th>COrientador<p><i>COSupervisor</i></p></th>
					<th>Instituição do Coorientador<p><i>CoSupervisor Institution</i></p></th>
					<th colspan='3'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewHabilitacao();return false;' value='Inserir novo Habilitacao'></center></th>
				</tr>
			</thead>
			<tbody>";
	
	foreach ($dadosDep as $i => $value){
		$checkAcao = $db->checkAcaoExists($dadosDep[$i]->id, $_SESSION['login'], 2);
		if ($checkAcao) {
			$acao = transformIntoAcaoHabilitacaoObject($dadosDep[$i]->id,$dadosDep[$i]->idinv,$checkAcao);
			if($dadosDep[$i]->anofim!='94'){
				echo "<tr>";					
					echo "<td>".$dadosDep[$i]->id."/".$dadosDep[$i]->idinv . "</td>";					
					echo "<td>".$dadosDep[$i]->instituicao."</td>";
					echo "<td>";
					getGrau($dadosDep[$i]->grau);	
					echo "</td>";					
					echo "<td>".$dadosDep[$i]->curso."</td>";
					echo "<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
					echo "<td>";	
					getAnoInicio($dadosDep[$i]->anoinicio);
					echo "</td>";
					echo "<td>";
					getAnoFim($dadosDep[$i]->anofim);
					echo "</td>";			
					echo "<td>";
					getDistincao($dadosDep[$i]->distincao);			
					echo "</td>";
					echo "<td>";
					getBolsa($dadosDep[$i]->bolsa);	
					echo "</td>";
					echo "<td>".$dadosDep[$i]->titulo."</td>";
					echo "<td>".$dadosDep[$i]->percentagem."</td>";
					echo "<td>".$dadosDep[$i]->orientador."</td>";
					echo "<td>".$dadosDep[$i]->oinstituicao."</td>";
					echo "<td>".$dadosDep[$i]->corientador."</td>";
					echo "<td>".$dadosDep[$i]->coinstituicao."</td>";
					echo "<td></td>";
					echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-hab').text('" . $dadosDep[$i]->id . "');setObservacaoHabilitacoes();return false;\" ></center></td>";
					echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-hab').text('" . $dadosDep[$i]->id . "');apagarHabilitacao();return false;\" ></center></td>";
				echo "</tr>";
				
				echo "<tr>";
					echo "<td style='background:#FFFF33; overflow:hidden;'><img src=\"../../images/arrow_return_down_right.png\" name='navOption'></td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_instituicao_hab_" . $dadosDep[$i]->id . "''>".$acao->instituicao."</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_grau_hab_" . $dadosDep[$i]->id . "'>";
					getGrau($acao->grau);	
					echo "</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_curso_hab_" . $dadosDep[$i]->id . "'>".$acao->curso."</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;'>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_ano_ini_hab_" . $dadosDep[$i]->id . "'>";	
					getAnoInicio($acao->anoinicio);
					echo "</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_ano_fim_hab_" . $dadosDep[$i]->id . "'>";
					getAnoFim($acao->anofim);
					echo "</td>";	
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_dist_hab_" . $dadosDep[$i]->id . "'>";
					getDistincao($acao->distincao);			
					echo "</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_bolsa_hab_" . $dadosDep[$i]->id . "'>";
					getBolsa($acao->bolsa);	
					echo "</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_tit_hab_" . $dadosDep[$i]->id . "'>".$acao->titulo."</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_perc_hab_" . $dadosDep[$i]->id . "'>".$acao->percentagem."</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_ori_hab_" . $dadosDep[$i]->id . "'>".$acao->orientador."</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_ori_inst_hab_" . $dadosDep[$i]->id . "'>".$acao->oinstituicao."</td>"; //TODO lookup Instituições
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_cori_hab_" . $dadosDep[$i]->id . "'>".$acao->corientador."</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_cori_inst_hab_" . $dadosDep[$i]->id . "'>".$acao->coinstituicao."</td>";
					echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-hab').text('". $dadosDep[$i]->id ."');\"></td>";					
					echo "<td></td>";
					echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"apagarAcao('" . $dadosDep[$i]->id . "',2);return false;\" ></center></td>";
				echo "</tr>";
			}	 
		} else {
			if($dadosDep[$i]->anofim!='94'){
				echo "<tr>";
					echo "<td>".$dadosDep[$i]->idinv. "</td>";
					echo "<td id='td_instituicao_hab_" . $dadosDep[$i]->id . "''>".$dadosDep[$i]->instituicao."</td>";
					echo "<td id='td_grau_hab_" . $dadosDep[$i]->id . "'>";
					getGrau($dadosDep[$i]->grau);	
					echo "</td>";
					echo "<td id='td_curso_hab_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->curso."</td>";
					echo "<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
					echo "<td id='td_ano_ini_hab_" . $dadosDep[$i]->id . "'>";	
					getAnoInicio($dadosDep[$i]->anoinicio);
					echo "</td>";
					echo "<td id='td_ano_fim_hab_" . $dadosDep[$i]->id . "'>";
					getAnoFim($dadosDep[$i]->anofim);
					echo "</td>";	
					echo "<td id='td_dist_hab_" . $dadosDep[$i]->id . "'>";
					getDistincao($dadosDep[$i]->distincao);			
					echo "</td>";
					echo "<td id='td_bolsa_hab_" . $dadosDep[$i]->id . "'>";
					getBolsa($dadosDep[$i]->bolsa);	
					echo "</td>";
					echo "<td id='td_tit_hab_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->titulo."</td>";
					echo "<td id='td_perc_hab_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->percentagem."</td>";
					echo "<td id='td_ori_hab_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->orientador."</td>";
					echo "<td id='td_ori_inst_hab_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->oinstituicao."</td>"; //TODO lookup Instituições
					echo "<td id='td_cori_hab_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->corientador."</td>";
					echo "<td id='td_cori_inst_hab_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->coinstituicao."</td>";
					echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-hab').text('". $dadosDep[$i]->id ."');\"></td>";
					echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-hab').text('" . $dadosDep[$i]->id . "');setObservacaoHabilitacoes();return false;\" ></center></td>";
					echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-hab').text('" . $dadosDep[$i]->id . "');apagarHabilitacao();return false;\" ></center></td>";
				echo "</tr>";
			}
		}
	}
    echo "<tr><td colspan='17' style='background-color: #FFFFCC;'><h1>Concluídos</h1></td></tr>";
	foreach ($dadosDep as $i => $value) {
		if($dadosDep[$i]->anofim=='94') { //TODO REFERENCIA AO ANO
			$checkAcao = $db->checkAcaoExists($dadosDep[$i]->id, $_SESSION['login'], 2);
			
			if ($checkAcao) {
				$acao = transformIntoAcaoHabilitacaoObject($dadosDep[$i]->id,$dadosDep[$i]->idinv,$checkAcao);
			
				echo "<tr>";
					echo "<td>".$dadosDep[$i]->idinv . "</td>";					
					echo "<td id='td_instituicao_hab_" . $dadosDep[$i]->id . "''>".$dadosDep[$i]->instituicao."</td>";
					echo "<td id='td_grau_hab_" . $dadosDep[$i]->id . "'>";
					getGrau($dadosDep[$i]->grau);	
					echo "</td>";					
					echo "<td id='td_curso_hab_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->curso."</td>";
					echo "<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
					echo "<td id='td_ano_ini_hab_" . $dadosDep[$i]->id . "'>";	
					getAnoInicio($dadosDep[$i]->anoinicio);
					echo "</td>";
					echo "<td id='td_ano_fim_hab_" . $dadosDep[$i]->id . "'>";
					getAnoFim($dadosDep[$i]->anofim);
					echo "</td>";			
					echo "<td id='td_dist_hab_" . $dadosDep[$i]->id . "'>";
					getDistincao($dadosDep[$i]->distincao);			
					echo "</td>";
					echo "<td id='td_bolsa_hab_" . $dadosDep[$i]->id . "'>";
					getBolsa($dadosDep[$i]->bolsa);	
					echo "</td>";
					echo "<td id='td_tit_hab_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->titulo."</td>";
					echo "<td>".$dadosDep[$i]->percentagem."</td>";
					echo "<td>".$dadosDep[$i]->orientador."</td>";
					echo "<td>".$dadosDep[$i]->oinstituicao."</td>";
					echo "<td>".$dadosDep[$i]->corientador."</td>";
					echo "<td>".$dadosDep[$i]->coinstituicao."</td>";
					echo "<td></td>";
					echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-inv').text('" . $dadosDep[$i]->id . "');setObservacaoInvestigadores();return false;\" ></center></td>";
					echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-hab').text('" . $dadosDep[$i]->id . "');apagarHabilitacao();return false;\" ></center></td>";
				echo "</tr>";
				
				echo "<tr>";
					echo "<td style='background:#FFFF33; overflow:hidden;'><img src=\"../../images/arrow_return_down_right.png\" name='navOption'></td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_instituicao_hab_" . $dadosDep[$i]->id . "''>".$acao->instituicao."</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_grau_hab_" . $dadosDep[$i]->id . "'>";
					getGrau($acao->grau);	
					echo "</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_curso_hab_" . $dadosDep[$i]->id . "'>".$acao->curso."</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;'>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_ano_ini_hab_" . $dadosDep[$i]->id . "'>";	
					getAnoInicio($acao->anoinicio);
					echo "</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_ano_fim_hab_" . $dadosDep[$i]->id . "'>";
					getAnoFim($acao->anofim);
					echo "</td>";			
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_dist_hab_" . $dadosDep[$i]->id . "'>";
					getDistincao($acao->distincao);			
					echo "</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_bolsa_hab_" . $dadosDep[$i]->id . "'>";
					getBolsa($acao->bolsa);	
					echo "</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_tit_hab_" . $dadosDep[$i]->id . "'>".$acao->titulo."</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_perc_hab_" . $dadosDep[$i]->id . "'>".$acao->percentagem."</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_ori_hab_" . $dadosDep[$i]->id . "'>".$acao->orientador."</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_ori_inst_hab_" . $dadosDep[$i]->id . "'>".$acao->oinstituicao."</td>"; //TODO lookup Instituições
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_cori_hab_" . $dadosDep[$i]->id . "'>".$acao->corientador."</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_cori_inst_hab_" . $dadosDep[$i]->id . "'>".$acao->coinstituicao."</td>";
					echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-hab').text('". $dadosDep[$i]->id ."');\"></td>";					
					echo "<td></td>";
					echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"apagarAcao('" . $dadosDep[$i]->id . "',2);return false;\" ></center></td>";
				echo "</tr>";
			} else {
				echo "<tr>";
					echo "<td>".$dadosDep[$i]->idinv . "</td>";					
					echo "<td id='td_instituicao_hab_" . $dadosDep[$i]->id . "''>".$dadosDep[$i]->instituicao."</td>";
					echo "<td id='td_grau_hab_" . $dadosDep[$i]->id . "'>";
					getGrau($dadosDep[$i]->grau);	
					echo "</td>";					
					echo "<td id='td_curso_hab_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->curso."</td>";
					echo "<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
					echo "<td id='td_ano_ini_hab_" . $dadosDep[$i]->id . "'>";	
					getAnoInicio($dadosDep[$i]->anoinicio);
					echo "</td>";
					echo "<td id='td_ano_fim_hab_" . $dadosDep[$i]->id . "'>";
					getAnoFim($dadosDep[$i]->anofim);
					echo "</td>";			
					echo "<td id='td_dist_hab_" . $dadosDep[$i]->id . "'>";
					getDistincao($dadosDep[$i]->distincao);			
					echo "</td>";
					echo "<td id='td_bolsa_hab_" . $dadosDep[$i]->id . "'>";
					getBolsa($dadosDep[$i]->bolsa);	
					echo "</td>";
					echo "<td id='td_tit_hab_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->titulo."</td>";
					echo "<td id='td_perc_hab_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->percentagem."</td>";
					echo "<td id='td_ori_hab_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->orientador."</td>";
					echo "<td id='td_ori_inst_hab_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->oinstituicao."</td>"; //TODO lookup Instituições
					echo "<td id='td_cori_hab_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->corientador."</td>";
					echo "<td id='td_cori_inst_hab_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->coinstituicao."</td>";
					echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-hab').text('". $dadosDep[$i]->id ."');\"></td>";
					echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-inv').text('" . $dadosDep[$i]->id . "');setObservacaoHabilitacoes();return false;\" ></center></td>";
					echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-hab').text('" . $dadosDep[$i]->id . "');apagarHabilitacao();return false;\" ></center></td>";
				echo "</tr>";
			}
		}
	}
	
		echo "</tbody>
		</table>
		<p id='chave-hab' hidden></p>
	</div>";
	
}

function transformIntoAcaoHabilitacaoObject($id, $idinv, $query)
{

	$cena = explode("anoinicio=",$query);
			
	$cena1 = explode(", anofim=",$cena[1]);
	$anoinicio = $cena1[0];
	
	$cena2 = explode(", curso='",$cena1[1]);
	$anofim = $cena2[0];
	
	$cena3 = explode("', instituicao='",$cena2[1]);
	$curso= $cena3[0];
	
	$cena4 = explode("', grau = ",$cena3[1]);
	$instituicao= $cena4[0];
	
	$cena5 = explode(", distincao = '",$cena4[1]);
	$grau= $cena5[0];
	
	$cena6 = explode("', bolsa= '",$cena5[1]);
	$distincao= $cena6[0];
				
	$cena7 = explode("', titulo = '",$cena6[1]);
	$bolsa= $cena7[0];
	
	$cena8 = explode("', percentagem = ",$cena7[1]);
	$titulo = $cena8[0];
	
	$cena9 = explode(", ORIENTADOR='",$cena8[1]);
	$percentagem = $cena9[0];

	$cena10 = explode("', OINSTITUICAO='",$cena9[1]);
	$orientador = $cena10[0];
	
	$cena11 = explode("', CORIENTADOR='",$cena10[1]);
	$oinstituicao = $cena11[0];
	
	$cena12 = explode("',COINSTITUICAO='",$cena11[1]);
	$corientador = $cena12[0];
	
	$cena13 = explode("' where id='",$cena12[1]);
	$coinstituicao = $cena13[0];
	
	$acao = new Habilitacoes(
		$id, $idinv, $anoinicio, $anofim, $curso, $instituicao, $grau , $distincao, $bolsa, $titulo, $percentagem,$orientador,$oinstituicao,$corientador,$coinstituicao
	);
	
	return $acao;
}	

function getAnoInicio($i){	
	$db = new Database();
	$lValues =$db->getLookupValuesAnoInicio("lista_anoslectivos");
	while ($row = mysql_fetch_assoc($lValues)) {	
		if($i==$row["ID"])
			echo $row["DESCRICAO"];
	}	
	$db->disconnect();
}
			
function checkAnoInicio($id,$i){
	global $dadosDep;
	if($dadosDep[$i]->anoinicio==$id)
		return true;
	else 
		return false;
}

function getAnoFim($i) {
	$db = new Database();
	$lValues =$db->getLookupValues("lista_anoslectivos");
	while ($row = mysql_fetch_assoc($lValues)) {	
		if($i==$row["ID"]){
			echo $row["DESCRICAO"];
		}
	}
	$db->disconnect();	
}
			
function checkAnofim($id,$i){
	global $dadosDep;
	if($dadosDep[$i]->anofim==$id)
		return true;
	else 
		return false;
}

function getGrau($i) {
	$db = new Database();
	$lValues =$db->getLookupValues("lista_graucursos");	
	while ($row = mysql_fetch_assoc($lValues)) {	
		if($i==$row["ID"])
			echo $row["DESCRICAO"];
	}
	echo "</SELECT><br />\n";
	$db->disconnect();			
}	
			
function checkGrau($id,$i){
	global $dadosDep;
	if($dadosDep[$i]->grau==$id)
		return true;
	else 
		return false;
}

function getDistincao($i) {
	$db = new Database();
	$lValues =$db->getLookupValues("lista_distincaoValores");
	while ($row = mysql_fetch_assoc($lValues)) {
		if($i==$row["ID"])
			echo $row["DESCRICAO"];
	}
	$db->disconnect();
}

function checkDistincao($id,$i){
	global $dadosDep;
	if($dadosDep[$i]->distincao==$id)
		return true;
	else
		return false;
}

function getBolsa($i) {
	$db = new Database();
	$lValues =$db->getLookupValues("lista_TipoBolsas");
	while ($row = mysql_fetch_assoc($lValues)) {
		if($i==$row["ID"])
			echo $row["DESCRICAO"];
	}
	$db->disconnect();
}

function checkBolsa($id,$i){
	global $dadosDep;
	if($dadosDep[$i]->bolsa==$id)
		return true;
	else
		return false;
}

?>