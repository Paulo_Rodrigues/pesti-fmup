<?php

function showEditPublicacoesPUBMED($dadosDep) {
	
echo "<h3>Publicações PubMed</h3>\n";
echo "<div id='publicacoesPUBMED'>";

$outJPUBMED="";
$outJAPUBMED="";
$outPRPPUBMED="";
$outPRPNPUBMED="";
$outCPPUBMED="";
$outCPNPUBMED="";
$outOAPUBMED="";
$outSPUBMED="";
$outJNPUBMED="";
$outSNPUBMED="";
$outLNPUBMED="";
$outCLNPUBMED="";
$outLPUBMED="";
$outCLPUBMED="";
$outPPUBMED="";
$outAPUBMED="";

$outRestoPUBMED="";

$tcols="<thead>
				<tr>
					<th>IDINV</th>						
					<th>Nome Investigador</th>
					<th>NOMEPUBLICACAO</th>
					<th>ISSN</th>
					<th>ISBN</th>
					<th>TITULO</th>
					<th>AUTORES</th>
					<th>VOLUME</th>
					<th>ISSUE</th>
					<th>AR</th>
					<th>COLECAO</th>
					<th>PRIMPAGINA</th>
					<th>ULTPAGINA</th>
					<th>IFACTOR</th>
					<th>CITACOES</th>
					<th>NPATENTE</th>
					<th>DATAPATENTE</th>
					<th>IPC</th>
					<th>AREA</th>
					<th>ESTADO</th>";
					
$tcolsJPUBMED="<thead>
				    <tr>
						<th>IDINV</th>
						<th>Revista (Journal)</th>
						<th>ISSN</th>
						<th>Título</th>
						<th>Autores</th>
						<th>Volume</th>
						<th>Issue</th>
						<th>1ª Pág.</th>
						<th>Última Pág.</th>
						<th>Estado</th>";

$tcolsCONFPUBMED="<thead>
				    <tr>
						<th>IDINV</th>
						<th>ISBN</th>
						<th>Nome Publicação</th>
						<th>Editores</th>
						<th>Colecção</th>
						<th>Volume</th>
						<th>Título do Artigo</th>
						<th>Autores</th>
						<th>1ª Pág.</th>
						<th>Últ- Página</th>
						<th>Estado</th>";

$tcolsCLIVPUBMED="<thead>
				    <tr>
						<th>IDINV</th>
						<th>ISBN</th>
						<th>NOME PUBLICACAO</th>
						<th>EDITORA</th>		
						<th>AUTORES</th>
						<th>TÍTULO ARTIGO</th>
						<th>PRIMEIRA PÁG.</th>
						<th>ÚLTIMA PÁG.</th>
						<th>Estado</th>";

$tcolsPATPUBMED="<thead>
				    <tr>
						<th>IDINV</th>
						<th>Nº Patente</th>
						<th>IPC</th>
						<th>Título</th>
						<th>Autores</th>		
						<th>Data da Patente</th>
						<th>Estado</th>";

$tcolsLIVPUBMED="<thead>
				    <tr>
						<th>IDINV</th>
						<th>ISBN</th>
						<th>TÍTULO</th>
						<th>AUTORES</th>		
						<th>EDITOR (Publisher)</th>
						<th>Estado</th>";
			    
	foreach ($dadosDep as $i => $value){
			
			$tipo=$dadosDep[$i]->tipofmup;
			
	switch($tipo){
			case 'J': { 
	    		$outJPUBMED=$outJPUBMED."<tr>";

					$outJPUBMED=$outJPUBMED."<td>";
					$outJPUBMED=$outJPUBMED.$dadosDep[$i]->idinv;
		    		$outJPUBMED=$outJPUBMED."</td>";    		
		    		
		    		$outJPUBMED=$outJPUBMED."<td>";
					$outJPUBMED=$outJPUBMED.$dadosDep[$i]->nomepublicacao;
		    		$outJPUBMED=$outJPUBMED."</td>";

		    		$outJPUBMED=$outJPUBMED."<td>";
					$outJPUBMED=$outJPUBMED.$dadosDep[$i]->issn;
		    		$outJPUBMED=$outJPUBMED."</td>";
		    		
		    		$outJPUBMED=$outJPUBMED."<td>";
					$outJPUBMED=$outJPUBMED.$dadosDep[$i]->titulo;
		    		$outJPUBMED=$outJPUBMED."</td>";

		    		$outJPUBMED=$outJPUBMED."<td>";
					$outJPUBMED=$outJPUBMED.$dadosDep[$i]->autores;
		    		$outJPUBMED=$outJPUBMED."</td>";

					$outJPUBMED=$outJPUBMED."<td>";
					$outJPUBMED=$outJPUBMED.$dadosDep[$i]->volume;
		    		$outJPUBMED=$outJPUBMED."</td>";

					$outJPUBMED=$outJPUBMED."<td>";
					$outJPUBMED=$outJPUBMED.$dadosDep[$i]->issue;
		    		$outJPUBMED=$outJPUBMED."</td>";

					$outJPUBMED=$outJPUBMED."<td>";
					$outJPUBMED=$outJPUBMED.$dadosDep[$i]->primpagina;
		    		$outJPUBMED=$outJPUBMED."</td>";	

					$outJPUBMED=$outJPUBMED."<td>";
					$outJPUBMED=$outJPUBMED.$dadosDep[$i]->ultpagina;
		    		$outJPUBMED=$outJPUBMED."</td>";

		    		$outJPUBMED=$outJPUBMED."<td>";
					$outJPUBMED=$outJPUBMED.getTipoEstado($dadosDep[$i]->estado);
		    		$outJPUBMED=$outJPUBMED."</td>";	

					$outJPUBMED=$outJPUBMED."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');setObservacaoPublicacaoPUBMED();return false;\" ></center></td>";
					$outJPUBMED=$outJPUBMED."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('9');apagarPublicacao();return false;\" ></center></td>";   
						
	    		$outJPUBMED=$outJPUBMED."</tr>";
			}
			break;
			case 'PRP': { 
	    		$outPRPPUBMED=$outPRPPUBMED."<tr>";

					$outPRPPUBMED=$outPRPPUBMED."<td>";
					$outPRPPUBMED=$outPRPPUBMED.$dadosDep[$i]->idinv;
		    		$outPRPPUBMED=$outPRPPUBMED."</td>";
					
		    		$outPRPPUBMED=$outPRPPUBMED."<td>";
					$outPRPPUBMED=$outPRPPUBMED.$dadosDep[$i]->nomepublicacao;
		    		$outPRPPUBMED=$outPRPPUBMED."</td>";

		    		$outPRPPUBMED=$outPRPPUBMED."<td>";
					$outPRPPUBMED=$outPRPPUBMED.$dadosDep[$i]->issn;
		    		$outPRPPUBMED=$outPRPPUBMED."</td>";
		    		
		    		$outPRPPUBMED=$outPRPPUBMED."<td>";
					$outPRPPUBMED=$outPRPPUBMED.$dadosDep[$i]->titulo;
		    		$outPRPPUBMED=$outPRPPUBMED."</td>";

		    		$outPRPPUBMED=$outPRPPUBMED."<td>";
					$outPRPPUBMED=$outPRPPUBMED.$dadosDep[$i]->autores;
		    		$outPRPPUBMED=$outPRPPUBMED."</td>";

					$outPRPPUBMED=$outPRPPUBMED."<td>";
					$outPRPPUBMED=$outPRPPUBMED.$dadosDep[$i]->volume;
		    		$outPRPPUBMED=$outPRPPUBMED."</td>";

					$outPRPPUBMED=$outPRPPUBMED."<td>";
					$outPRPPUBMED=$outPRPPUBMED.$dadosDep[$i]->issue;
		    		$outPRPPUBMED=$outPRPPUBMED."</td>";
					
					$outPRPPUBMED=$outPRPPUBMED."<td>";
					$outPRPPUBMED=$outPRPPUBMED.$dadosDep[$i]->primpagina;
		    		$outPRPPUBMED=$outPRPPUBMED."</td>";

					$outPRPPUBMED=$outPRPPUBMED."<td>";
					$outPRPPUBMED=$outPRPPUBMED.$dadosDep[$i]->ultpagina;
		    		$outPRPPUBMED=$outPRPPUBMED."</td>";
		    		
		    		$outPRPPUBMED=$outPRPPUBMED."<td>";
					$outPRPPUBMED=$outPRPPUBMED.getTipoEstado($dadosDep[$i]->estado);
		    		$outPRPPUBMED=$outPRPPUBMED."</td>";

					$outPRPPUBMED=$outPRPPUBMED."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');setObservacaoPublicacaoPUBMED();return false;\" ></center></td>";
					$outPRPPUBMED=$outPRPPUBMED."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('9');apagarPublicacao();return false;\" ></center></td>";   
											
	    		$outPRPPUBMED=$outPRPPUBMED."</tr>";
			}
			break;
			case 'JA': { 
	    		$outJAPUBMED=$outJAPUBMED."<tr>";

					$outJAPUBMED=$outJAPUBMED."<td>";
					$outJAPUBMED=$outJAPUBMED.$dadosDep[$i]->idinv;
		    		$outJAPUBMED=$outJAPUBMED."</td>";

		    		$outJAPUBMED=$outJAPUBMED."<td>";
					$outJAPUBMED=$outJAPUBMED.$dadosDep[$i]->nomepublicacao;
		    		$outJAPUBMED=$outJAPUBMED."</td>";

		    		$outJAPUBMED=$outJAPUBMED."<td>";
					$outJAPUBMED=$outJAPUBMED.$dadosDep[$i]->issn;
		    		$outJAPUBMED=$outJAPUBMED."</td>";

		    		$outJAPUBMED=$outJAPUBMED."<td>";
					$outJAPUBMED=$outJAPUBMED.$dadosDep[$i]->titulo;
		    		$outJAPUBMED=$outJAPUBMED."</td>";

		    		$outJAPUBMED=$outJAPUBMED."<td>";
					$outJAPUBMED=$outJAPUBMED.$dadosDep[$i]->autores;
		    		$outJAPUBMED=$outJAPUBMED."</td>";

					$outJAPUBMED=$outJAPUBMED."<td>";
					$outJAPUBMED=$outJAPUBMED.$dadosDep[$i]->volume;
		    		$outJAPUBMED=$outJAPUBMED."</td>";

					$outJAPUBMED=$outJAPUBMED."<td>";
					$outJAPUBMED=$outJAPUBMED.$dadosDep[$i]->issue;
		    		$outJAPUBMED=$outJAPUBMED."</td>";

					$outJAPUBMED=$outJAPUBMED."<td>";
					$outJAPUBMED=$outJAPUBMED.$dadosDep[$i]->primpagina;
		    		$outJAPUBMED=$outJAPUBMED."</td>";			
					$outJAPUBMED=$outJAPUBMED."<td>";
					$outJAPUBMED=$outJAPUBMED.$dadosDep[$i]->ultpagina;
		    		$outJAPUBMED=$outJAPUBMED."</td>";

		    		$outJAPUBMED=$outJAPUBMED."<td>";
					$outJAPUBMED=$outJAPUBMED.getTipoEstado($dadosDep[$i]->estado);
		    		$outJAPUBMED=$outJAPUBMED."</td>";	

					$outJAPUBMED=$outJAPUBMED."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');setObservacaoPublicacaoPUBMED();return false;\" ></center></td>";
					$outJAPUBMED=$outJAPUBMED."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('9');apagarPublicacao();return false;\" ></center></td>";   
						
	    		$outJAPUBMED=$outJAPUBMED."</tr>";
			}
			break;
			case 'PRPN': { // Opção de entrada
	    		$outPRPNPUBMED=$outPRPNPUBMED."<tr>";

					$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$dadosDep[$i]->idinv;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";

		    		$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$dadosDep[$i]->ano;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";

		    		$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$dadosDep[$i]->nomepublicacao;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";

		    		$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$dadosDep[$i]->issn;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";

		    		$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$dadosDep[$i]->isbn;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";

		    		$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$dadosDep[$i]->titulo;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";

		    		$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$dadosDep[$i]->autores;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";

					$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$dadosDep[$i]->volume;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";

					$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$dadosDep[$i]->issue;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";

					$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$dadosDep[$i]->ar;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";

					$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$dadosDep[$i]->colecao;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";

					$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$dadosDep[$i]->primpagina;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";	

					$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$dadosDep[$i]->ultpagina;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";

		    		$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$dadosDep[$i]->ifactor;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";

		    		$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$dadosDep[$i]->citacoes;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";

		    		$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$dadosDep[$i]->conftitle;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";

		    		$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$dadosDep[$i]->language;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";

		    		$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$dadosDep[$i]->editor;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";

		    		$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.getTipoEstado($dadosDep[$i]->estado);
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";	

					$outPRPNPUBMED=$outPRPNPUBMED."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');setObservacaoPublicacaoPUBMED();return false;\" ></center></td>";
					$outPRPNPUBMED=$outPRPNPUBMED."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('9');apagarPublicacao();return false;\" ></center></td>";   
											
	    		$outPRPNPUBMED=$outPRPNPUBMED."</tr>";
			}
			break;
			case 'CP': { 
	    		$outCPPUBMED=$outCPPUBMED."<tr>";

		    		$outCPPUBMED=$outCPPUBMED."<td>";
		    		$outCPPUBMED=$outCPPUBMED.$dadosDep[$i]->idinv;
		    		$outCPPUBMED=$outCPPUBMED."</td>";		    		

		    		$outCPPUBMED=$outCPPUBMED."<td>";
					$outCPPUBMED=$outCPPUBMED.$dadosDep[$i]->isbn;
		    		$outCPPUBMED=$outCPPUBMED."</td>";	
					
					$outCPPUBMED=$outCPPUBMED."<td>";
					$outCPPUBMED=$outCPPUBMED.$dadosDep[$i]->nomepublicacao;
		    		$outCPPUBMED=$outCPPUBMED."</td>";	

		    		$outCPPUBMED=$outCPPUBMED."<td>";
					$outCPPUBMED=$outCPPUBMED.$dadosDep[$i]->editor;
		    		$outCPPUBMED=$outCPPUBMED."</td>";	

					$outCPPUBMED=$outCPPUBMED."<td>";
					$outCPPUBMED=$outCPPUBMED.$dadosDep[$i]->colecao;
		    		$outCPPUBMED=$outCPPUBMED."</td>";

					$outCPPUBMED=$outCPPUBMED."<td>";
					$outCPPUBMED=$outCPPUBMED.$dadosDep[$i]->volume;
		    		$outCPPUBMED=$outCPPUBMED."</td>";

		    		$outCPPUBMED=$outCPPUBMED."<td>";
					$outCPPUBMED=$outCPPUBMED.$dadosDep[$i]->titulo;
		    		$outCPPUBMED=$outCPPUBMED."</td>";	

		    		$outCPPUBMED=$outCPPUBMED."<td>";
					$outCPPUBMED=$outCPPUBMED.$dadosDep[$i]->autores;
		    		$outCPPUBMED=$outCPPUBMED."</td>";	

					$outCPPUBMED=$outCPPUBMED."<td>";
					$outCPPUBMED=$outCPPUBMED.$dadosDep[$i]->primpagina;
		    		$outCPPUBMED=$outCPPUBMED."</td>";	

					$outCPPUBMED=$outCPPUBMED."<td>";
					$outCPPUBMED=$outCPPUBMED.$dadosDep[$i]->ultpagina;
		    		$outCPPUBMED=$outCPPUBMED."</td>";
		    		
		    		$outCPPUBMED=$outCPPUBMED."<td>";
					$outCPPUBMED=$outCPPUBMED.getTipoEstado($dadosDep[$i]->estado);
		    		$outCPPUBMED=$outCPPUBMED."</td>";	

					$outCPPUBMED=$outCPPUBMED."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');setObservacaoPublicacaoPUBMED();return false;\" ></center></td>";
					$outCPPUBMED=$outCPPUBMED."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('9');apagarPublicacao();return false;\" ></center></td>";   
										
	    		$outCPPUBMED=$outCPPUBMED."</tr>";
			}
			break;
			case 'CPN': { 
	    		$outCPNPUBMED=$outCPNPUBMED."<tr>";

		    		$outCPNPUBMED=$outCPNPUBMED."<td>";
		    		$outCPNPUBMED=$outCPNPUBMED.$dadosDep[$i]->idinv;
		    		$outCPNPUBMED=$outCPNPUBMED."</td>";

					$outCPNPUBMED=$outCPNPUBMED."<td>";
					$outCPNPUBMED=$outCPNPUBMED.$dadosDep[$i]->isbn;
		    		$outCPNPUBMED=$outCPNPUBMED."</td>";	

		    		$outCPNPUBMED=$outCPNPUBMED."<td>";
					$outCPNPUBMED=$outCPNPUBMED.$dadosDep[$i]->nomepublicacao;
		    		$outCPNPUBMED=$outCPNPUBMED."</td>";
		    		
		    		$outCPNPUBMED=$outCPNPUBMED."<td>";
					$outCPNPUBMED=$outCPNPUBMED.$dadosDep[$i]->editor;
		    		$outCPNPUBMED=$outCPNPUBMED."</td>";

					$outCPNPUBMED=$outCPNPUBMED."<td>";
					$outCPNPUBMED=$outCPNPUBMED.$dadosDep[$i]->colecao;
		    		$outCPNPUBMED=$outCPNPUBMED."</td>";

					$outCPNPUBMED=$outCPNPUBMED."<td>";
					$outCPNPUBMED=$outCPNPUBMED.$dadosDep[$i]->volume;
		    		$outCPNPUBMED=$outCPNPUBMED."</td>";

		    		$outCPNPUBMED=$outCPNPUBMED."<td>";
					$outCPNPUBMED=$outCPNPUBMED.$dadosDep[$i]->titulo;
		    		$outCPNPUBMED=$outCPNPUBMED."</td>";

		    		$outCPNPUBMED=$outCPNPUBMED."<td>";
					$outCPNPUBMED=$outCPNPUBMED.$dadosDep[$i]->autores;
		    		$outCPNPUBMED=$outCPNPUBMED."</td>";

					$outCPNPUBMED=$outCPNPUBMED."<td>";
					$outCPNPUBMED=$outCPNPUBMED.$dadosDep[$i]->primpagina;
		    		$outCPNPUBMED=$outCPNPUBMED."</td>";

					$outCPNPUBMED=$outCPNPUBMED."<td>";
					$outCPNPUBMED=$outCPNPUBMED.$dadosDep[$i]->ultpagina;
		    		$outCPNPUBMED=$outCPNPUBMED."</td>";

		    		$outCPNPUBMED=$outCPNPUBMED."<td>";
					$outCPNPUBMED=$outCPNPUBMED.getTipoEstado($dadosDep[$i]->estado);
		    		$outCPNPUBMED=$outCPNPUBMED."</td>";	

					$outCPNPUBMED=$outCPNPUBMED."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');setObservacaoPublicacaoPUBMED();return false;\" ></center></td>";
					$outCPNPUBMED=$outCPNPUBMED."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('9');apagarPublicacao();return false;\" ></center></td>";   
																
	    		$outCPNPUBMED=$outCPNPUBMED."</tr>";
			}
			break;
			case 'OA': { 
	    		$outOAPUBMED=$outOAPUBMED."<tr>";
		    		$outOAPUBMED=$outOAPUBMED."<td>";
		    		$outOAPUBMED=$outOAPUBMED.$dadosDep[$i]->idinv;
		    		$outOAPUBMED=$outOAPUBMED."</td>";

		    		$outOAPUBMED=$outOAPUBMED."<td>";
					$outOAPUBMED=$outOAPUBMED.$dadosDep[$i]->nomepublicacao;
		    		$outOAPUBMED=$outOAPUBMED."</td>";		

		    		$outOAPUBMED=$outOAPUBMED."<td>";
					$outOAPUBMED=$outOAPUBMED.$dadosDep[$i]->isbn;
		    		$outOAPUBMED=$outOAPUBMED."</td>";	

		    		$outOAPUBMED=$outOAPUBMED."<td>";
					$outOAPUBMED=$outOAPUBMED.$dadosDep[$i]->editor;
		    		$outOAPUBMED=$outOAPUBMED."</td>";	

					$outOAPUBMED=$outOAPUBMED."<td>";
					$outOAPUBMED=$outOAPUBMED.$dadosDep[$i]->colecao;
		    		$outOAPUBMED=$outOAPUBMED."</td>";

					$outOAPUBMED=$outOAPUBMED."<td>";
					$outOAPUBMED=$outOAPUBMED.$dadosDep[$i]->volume;
		    		$outOAPUBMED=$outOAPUBMED."</td>";

		    		$outOAPUBMED=$outOAPUBMED."<td>";
					$outOAPUBMED=$outOAPUBMED.$dadosDep[$i]->titulo;
		    		$outOAPUBMED=$outOAPUBMED."</td>";	

		    		$outOAPUBMED=$outOAPUBMED."<td>";
					$outOAPUBMED=$outOAPUBMED.$dadosDep[$i]->autores;
		    		$outOAPUBMED=$outOAPUBMED."</td>";	

					$outOAPUBMED=$outOAPUBMED."<td>";
					$outOAPUBMED=$outOAPUBMED.$dadosDep[$i]->primpagina;
		    		$outOAPUBMED=$outOAPUBMED."</td>";	

					$outOAPUBMED=$outOAPUBMED."<td>";
					$outOAPUBMED=$outOAPUBMED.$dadosDep[$i]->ultpagina;
		    		$outOAPUBMED=$outOAPUBMED."</td>";

		    		$outOAPUBMED=$outOAPUBMED."<td>";
					$outOAPUBMED=$outOAPUBMED.getTipoEstado($dadosDep[$i]->estado);
		    		$outOAPUBMED=$outOAPUBMED."</td>";	

					$outOAPUBMED=$outOAPUBMED."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');setObservacaoPublicacaoPUBMED();return false;\" ></center></td>";
					$outOAPUBMED=$outOAPUBMED."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('9');apagarPublicacao();return false;\" ></center></td>";   
											
	    		$outOAPUBMED=$outOAPUBMED."</tr>";
			}
			break;
			case 'S': { 
		    		$outSPUBMED=$outSPUBMED."<tr>";

					$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$dadosDep[$i]->idinv;
		    		$outSPUBMED=$outSPUBMED."</td>";

		    		$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$dadosDep[$i]->ano;
		    		$outSPUBMED=$outSPUBMED."</td>";

		    		$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$dadosDep[$i]->nomepublicacao;
		    		$outSPUBMED=$outSPUBMED."</td>";

		    		$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$dadosDep[$i]->issn;
		    		$outSPUBMED=$outSPUBMED."</td>";

		    		$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$dadosDep[$i]->isbn;
		    		$outSPUBMED=$outSPUBMED."</td>";

		    		$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$dadosDep[$i]->titulo;
		    		$outSPUBMED=$outSPUBMED."</td>";

		    		$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$dadosDep[$i]->autores;
		    		$outSPUBMED=$outSPUBMED."</td>";

					$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$dadosDep[$i]->volume;
		    		$outSPUBMED=$outSPUBMED."</td>";

					$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$dadosDep[$i]->issue;
		    		$outSPUBMED=$outSPUBMED."</td>";

					$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$dadosDep[$i]->ar;
		    		$outSPUBMED=$outSPUBMED."</td>";

					$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$dadosDep[$i]->colecao;
		    		$outSPUBMED=$outSPUBMED."</td>";

					$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$dadosDep[$i]->primpagina;
		    		$outSPUBMED=$outSPUBMED."</td>";

					$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$dadosDep[$i]->ultpagina;
		    		$outSPUBMED=$outSPUBMED."</td>";

		    		$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$dadosDep[$i]->ifactor;
		    		$outSPUBMED=$outSPUBMED."</td>";

		    		$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$dadosDep[$i]->citacoes;
		    		$outSPUBMED=$outSPUBMED."</td>";

		    		$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$dadosDep[$i]->conftitle;
		    		$outSPUBMED=$outSPUBMED."</td>";

		    		$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$dadosDep[$i]->language;
		    		$outSPUBMED=$outSPUBMED."</td>";

		    		$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$dadosDep[$i]->editor;
		    		$outSPUBMED=$outSPUBMED."</td>";

		    		$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.getTipoEstado($dadosDep[$i]->estado);
		    		$outSPUBMED=$outSPUBMED."</td>";	

					$outSPUBMED=$outSPUBMED."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');setObservacaoPublicacaoPUBMED();return false;\" ></center></td>";
					$outSPUBMED=$outSPUBMED."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('9');apagarPublicacao();return false;\" ></center></td>";   
																
	    		$outSPUBMED=$outSPUBMED."</tr>";
			}
			break;
			case 'JN': {
	    		$outJNPUBMED=$outJNPUBMED."<tr>";
	    
					$outJNPUBMED=$outJNPUBMED."<td>";
					$outJNPUBMED=$outJNPUBMED.$dadosDep[$i]->idinv;
		    		$outJNPUBMED=$outJNPUBMED."</td>";

		    		$outJNPUBMED=$outJNPUBMED."<td>";
					$outJNPUBMED=$outJNPUBMED.$dadosDep[$i]->nomepublicacao;
		    		$outJNPUBMED=$outJNPUBMED."</td>";

		    		$outJNPUBMED=$outJNPUBMED."<td>";
					$outJNPUBMED=$outJNPUBMED.$dadosDep[$i]->issn;
		    		$outJNPUBMED=$outJNPUBMED."</td>";
		    		
		    		$outJNPUBMED=$outJNPUBMED."<td>";
					$outJNPUBMED=$outJNPUBMED.$dadosDep[$i]->titulo;
		    		$outJNPUBMED=$outJNPUBMED."</td>";

		    		$outJNPUBMED=$outJNPUBMED."<td>";
					$outJNPUBMED=$outJNPUBMED.$dadosDep[$i]->autores;
		    		$outJNPUBMED=$outJNPUBMED."</td>";

					$outJNPUBMED=$outJNPUBMED."<td>";
					$outJNPUBMED=$outJNPUBMED.$dadosDep[$i]->volume;
		    		$outJNPUBMED=$outJNPUBMED."</td>";

					$outJNPUBMED=$outJNPUBMED."<td>";
					$outJNPUBMED=$outJNPUBMED.$dadosDep[$i]->issue;
		    		$outJNPUBMED=$outJNPUBMED."</td>";

					$outJNPUBMED=$outJNPUBMED."<td>";
					$outJNPUBMED=$outJNPUBMED.$dadosDep[$i]->primpagina;
		    		$outJNPUBMED=$outJNPUBMED."</td>";	

					$outJNPUBMED=$outJNPUBMED."<td>";
					$outJNPUBMED=$outJNPUBMED.$dadosDep[$i]->ultpagina;
		    		$outJNPUBMED=$outJNPUBMED."</td>";

		    		$outJNPUBMED=$outJNPUBMED."<td>";
					$outJNPUBMED=$outJNPUBMED.getTipoEstado($dadosDep[$i]->estado);
		    		$outJNPUBMED=$outJNPUBMED."</td>";

					$outJNPUBMED=$outJNPUBMED."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');setObservacaoPublicacaoPUBMED();return false;\" ></center></td>";
					$outJNPUBMED=$outJNPUBMED."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('9');apagarPublicacao();return false;\" ></center></td>";   
											
	    		$outJNPUBMED=$outJNPUBMED."</tr>";
			}
			break;
			case 'SN': { 
	    		$outSNPUBMED=$outSNPUBMED."<tr>";

					$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$dadosDep[$i]->idinv;
		    		$outSNPUBMED=$outSNPUBMED."</td>";

		    		$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$dadosDep[$i]->ano;
		    		$outSNPUBMED=$outSNPUBMED."</td>";

		    		$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$dadosDep[$i]->nomepublicacao;
		    		$outSNPUBMED=$outSNPUBMED."</td>";

		    		$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$dadosDep[$i]->issn;
		    		$outSNPUBMED=$outSNPUBMED."</td>";

		    		$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$dadosDep[$i]->isbn;
		    		$outSNPUBMED=$outSNPUBMED."</td>";

		    		$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$dadosDep[$i]->titulo;
		    		$outSNPUBMED=$outSNPUBMED."</td>";

		    		$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$dadosDep[$i]->autores;
		    		$outSNPUBMED=$outSNPUBMED."</td>";

					$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$dadosDep[$i]->volume;
		    		$outSNPUBMED=$outSNPUBMED."</td>";

					$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$dadosDep[$i]->issue;
		    		$outSNPUBMED=$outSNPUBMED."</td>";

					$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$dadosDep[$i]->ar;
		    		$outSNPUBMED=$outSNPUBMED."</td>";

					$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$dadosDep[$i]->colecao;
		    		$outSNPUBMED=$outSNPUBMED."</td>";

					$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$dadosDep[$i]->primpagina;
		    		$outSNPUBMED=$outSNPUBMED."</td>";

					$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$dadosDep[$i]->ultpagina;
		    		$outSNPUBMED=$outSNPUBMED."</td>";

		    		$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$dadosDep[$i]->ifactor;
		    		$outSNPUBMED=$outSNPUBMED."</td>";

		    		$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$dadosDep[$i]->citacoes;
		    		$outSNPUBMED=$outSNPUBMED."</td>";

		    		$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$dadosDep[$i]->conftitle;
		    		$outSNPUBMED=$outSNPUBMED."</td>";

		    		$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$dadosDep[$i]->language;
		    		$outSNPUBMED=$outSNPUBMED."</td>";

		    		$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$dadosDep[$i]->editor;
		    		$outSNPUBMED=$outSNPUBMED."</td>";

		    		$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$dadosDep[$i]->tipofmup;
		    		$outSNPUBMED=$outSNPUBMED."</td>";

		    		$outSNPUBMED=$outSNPUBMED."<td>";
		    		$outSNPUBMED=$outSNPUBMED.getTipoEstado($dadosDep[$i]->estado);
		    		$outSNPUBMED=$outSNPUBMED."</td>";
					
					$outSNPUBMED=$outSNPUBMED."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');setObservacaoPublicacaoPUBMED();return false;\" ></center></td>";
					$outSNPUBMED=$outSNPUBMED."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('9');apagarPublicacao();return false;\" ></center></td>";   
						
	    		$outSNPUBMED=$outSNPUBMED."</tr>";
			}
			break;
			case 'LN': { 
	    		$outLNPUBMED=$outLNPUBMED."<tr>";

					$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$dadosDep[$i]->idinv;
		    		$outLNPUBMED=$outLNPUBMED."</td>";

		    		$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$dadosDep[$i]->ano;
		    		$outLNPUBMED=$outLNPUBMED."</td>";

		    		$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$dadosDep[$i]->nomepublicacao;
		    		$outLNPUBMED=$outLNPUBMED."</td>";

		    		$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$dadosDep[$i]->issn;
		    		$outLNPUBMED=$outLNPUBMED."</td>";

		    		$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$dadosDep[$i]->isbn;
		    		$outLNPUBMED=$outLNPUBMED."</td>";

		    		$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$dadosDep[$i]->titulo;
		    		$outLNPUBMED=$outLNPUBMED."</td>";

		    		$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$dadosDep[$i]->autores;
		    		$outLNPUBMED=$outLNPUBMED."</td>";

					$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$dadosDep[$i]->volume;
		    		$outLNPUBMED=$outLNPUBMED."</td>";

					$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$dadosDep[$i]->issue;
		    		$outLNPUBMED=$outLNPUBMED."</td>";

					$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$dadosDep[$i]->ar;
		    		$outLNPUBMED=$outLNPUBMED."</td>";

					$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$dadosDep[$i]->colecao;
		    		$outLNPUBMED=$outLNPUBMED."</td>";

					$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$dadosDep[$i]->primpagina;
		    		$outLNPUBMED=$outLNPUBMED."</td>";	

					$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$dadosDep[$i]->ultpagina;
		    		$outLNPUBMED=$outLNPUBMED."</td>";

		    		$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$dadosDep[$i]->citacoes;
		    		$outLNPUBMED=$outLNPUBMED."</td>";

		    		$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$dadosDep[$i]->ifactor;
		    		$outLNPUBMED=$outLNPUBMED."</td>";

		    		$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$dadosDep[$i]->conftitle;
		    		$outLNPUBMED=$outLNPUBMED."</td>";

		    		$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$dadosDep[$i]->language;
		    		$outLNPUBMED=$outLNPUBMED."</td>";

		    		$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$dadosDep[$i]->editor;
		    		$outLNPUBMED=$outLNPUBMED."</td>";

		    		$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.getTipoEstado($dadosDep[$i]->estado);
		    		$outLNPUBMED=$outLNPUBMED."</td>";	

					$outLNPUBMED=$outLNPUBMED."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');setObservacaoPublicacaoPUBMED();return false;\" ></center></td>";
					$outLNPUBMED=$outLNPUBMED."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('9');apagarPublicacao();return false;\" ></center></td>";   
										
	    		$outLNPUBMED=$outLNPUBMED."</tr>";
			}
			break;
			case 'CLN': { 
		    		$outCLNPUBMED=$outCLNPUBMED."<tr>";

					$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$dadosDep[$i]->idinv;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";

		    		$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$dadosDep[$i]->ano;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";

		    		$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$dadosDep[$i]->nomepublicacao;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";

		    		$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$dadosDep[$i]->issn;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";

		    		$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$dadosDep[$i]->isbn;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";

		    		$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$dadosDep[$i]->titulo;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";

		    		$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$dadosDep[$i]->autores;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";

					$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$dadosDep[$i]->volume;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";

					$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$dadosDep[$i]->issue;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";

					$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$dadosDep[$i]->ar;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";

					$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$dadosDep[$i]->colecao;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";

					$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$dadosDep[$i]->primpagina;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";	

					$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$dadosDep[$i]->ultpagina;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";

		    		$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$dadosDep[$i]->citacoes;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";

		    		$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$dadosDep[$i]->ifactor;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";

		    		$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$dadosDep[$i]->conftitle;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";

		    		$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$dadosDep[$i]->language;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";

		    		$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$dadosDep[$i]->editor;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";

		    		$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.getTipoEstado($dadosDep[$i]->estado);
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";	

					$outCLNPUBMED=$outCLNPUBMED."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');setObservacaoPublicacaoPUBMED();return false;\" ></center></td>";
					$outCLNPUBMED=$outCLNPUBMED."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('9');apagarPublicacao();return false;\" ></center></td>";   
										
	    		$outCLNPUBMED=$outCLNPUBMED."</tr>";
			}
			break;
			case 'L': { 
	    		$outLPUBMED=$outLPUBMED."<tr>";

					$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$dadosDep[$i]->idinv;
		    		$outLPUBMED=$outLPUBMED."</td>";

		    		$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$dadosDep[$i]->ano;
		    		$outLPUBMED=$outLPUBMED."</td>";

		    		$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$dadosDep[$i]->nomepublicacao;
		    		$outLPUBMED=$outLPUBMED."</td>";

		    		$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$dadosDep[$i]->issn;
		    		$outLPUBMED=$outLPUBMED."</td>";

		    		$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$dadosDep[$i]->isbn;
		    		$outLPUBMED=$outLPUBMED."</td>";

		    		$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$dadosDep[$i]->titulo;
		    		$outLPUBMED=$outLPUBMED."</td>";

		    		$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$dadosDep[$i]->autores;
		    		$outLPUBMED=$outLPUBMED."</td>";

					$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$dadosDep[$i]->volume;
		    		$outLPUBMED=$outLPUBMED."</td>";

					$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$dadosDep[$i]->issue;
		    		$outLPUBMED=$outLPUBMED."</td>";

					$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$dadosDep[$i]->ar;
		    		$outLPUBMED=$outLPUBMED."</td>";

					$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$dadosDep[$i]->colecao;
		    		$outLPUBMED=$outLPUBMED."</td>";

					$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$dadosDep[$i]->primpagina;
		    		$outLPUBMED=$outLPUBMED."</td>";

					$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$dadosDep[$i]->ultpagina;
		    		$outLPUBMED=$outLPUBMED."</td>";

		    		$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$dadosDep[$i]->citacoes;
		    		$outLPUBMED=$outLPUBMED."</td>";

		    		$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$dadosDep[$i]->ifactor;
		    		$outLPUBMED=$outLPUBMED."</td>";

		    		$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$dadosDep[$i]->conftitle;
		    		$outLPUBMED=$outLPUBMED."</td>";

		    		$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$dadosDep[$i]->language;
		    		$outLPUBMED=$outLPUBMED."</td>";

		    		$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$dadosDep[$i]->editor;
		    		$outLPUBMED=$outLPUBMED."</td>";

		    		$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.getTipoEstado($dadosDep[$i]->estado);
		    		$outLPUBMED=$outLPUBMED."</td>";

					$outLPUBMED=$outLPUBMED."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');setObservacaoPublicacaoPUBMED();return false;\" ></center></td>";
					$outLPUBMED=$outLPUBMED."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('9');apagarPublicacao();return false;\" ></center></td>";   
										
	    		$outLPUBMED=$outLPUBMED."</tr>";
			}
			break;
			case 'CL': { 
		    		$outCLPUBMED=$outCLPUBMED."<tr>";

					$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$dadosDep[$i]->idinv;
		    		$outCLPUBMED=$outCLPUBMED."</td>";
					
		    		$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$dadosDep[$i]->ano;
		    		$outCLPUBMED=$outCLPUBMED."</td>";

		    		$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$dadosDep[$i]->nomepublicacao;
		    		$outCLPUBMED=$outCLPUBMED."</td>";

		    		$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$dadosDep[$i]->issn;
		    		$outCLPUBMED=$outCLPUBMED."</td>";

		    		$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$dadosDep[$i]->isbn;
		    		$outCLPUBMED=$outCLPUBMED."</td>";

		    		$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$dadosDep[$i]->titulo;
		    		$outCLPUBMED=$outCLPUBMED."</td>";

		    		$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$dadosDep[$i]->autores;
		    		$outCLPUBMED=$outCLPUBMED."</td>";

					$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$dadosDep[$i]->volume;
		    		$outCLPUBMED=$outCLPUBMED."</td>";

					$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$dadosDep[$i]->issue;
		    		$outCLPUBMED=$outCLPUBMED."</td>";

					$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$dadosDep[$i]->ar;
		    		$outCLPUBMED=$outCLPUBMED."</td>";

					$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$dadosDep[$i]->colecao;
		    		$outCLPUBMED=$outCLPUBMED."</td>";

					$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$dadosDep[$i]->primpagina;
		    		$outCLPUBMED=$outCLPUBMED."</td>";	

					$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$dadosDep[$i]->ultpagina;
		    		$outCLPUBMED=$outCLPUBMED."</td>";

		    		$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$dadosDep[$i]->citacoes;
		    		$outCLPUBMED=$outCLPUBMED."</td>";

		    		$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$dadosDep[$i]->ifactor;
		    		$outCLPUBMED=$outCLPUBMED."</td>";

		    		$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$dadosDep[$i]->conftitle;
		    		$outCLPUBMED=$outCLPUBMED."</td>";

		    		$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$dadosDep[$i]->language;
		    		$outCLPUBMED=$outCLPUBMED."</td>";

		    		$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$dadosDep[$i]->editor;
		    		$outCLPUBMED=$outCLPUBMED."</td>";

		    		$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.getTipoEstado($dadosDep[$i]->estado);
		    		$outCLPUBMED=$outCLPUBMED."</td>";	

					$outCLPUBMED=$outCLPUBMED."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');setObservacaoPublicacaoPUBMED();return false;\" ></center></td>";
					$outCLPUBMED=$outCLPUBMED."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('9');apagarPublicacao();return false;\" ></center></td>";   
										
	    		$outCLPUBMED=$outCLPUBMED."</tr>";
			}
			break;
			case 'P': { 
	    		$outPPUBMED=$outPPUBMED."<tr>";

		    		$outPPUBMED=$outPPUBMED."<td>";
		    		$outPPUBMED=$outPPUBMED.$dadosDep[$i]->idinv;
		    		$outPPUBMED=$outPPUBMED."</td>";

		    		$outPPUBMED=$outPPUBMED."<td>";
					$outPPUBMED=$outPPUBMED.$dadosDep[$i]->npatente;
		    		$outPPUBMED=$outPPUBMED."</td>";

		    		$outPPUBMED=$outPPUBMED."<td>";
					$outPPUBMED=$outPPUBMED.$dadosDep[$i]->ipc;
		    		$outPPUBMED=$outPPUBMED."</td>";	

		    		$outPPUBMED=$outPPUBMED."<td>";
					$outPPUBMED=$outPPUBMED.$dadosDep[$i]->titulo;
		    		$outPPUBMED=$outPPUBMED."</td>";	

		    		$outPPUBMED=$outPPUBMED."<td>";
					$outPPUBMED=$outPPUBMED.$dadosDep[$i]->autores;
		    		$outPPUBMED=$outPPUBMED."</td>";

		    		$outPPUBMED=$outPPUBMED."<td>";
					$outPPUBMED=$outPPUBMED.$dadosDep[$i]->datapatente;
		    		$outPPUBMED=$outPPUBMED."</td>";		    		
		    		
					$outPPUBMED=$outPPUBMED."<td>";
					$outPPUBMED=$outPPUBMED.getTipoEstado($dadosDep[$i]->estado);
		    		$outPPUBMED=$outPPUBMED."</td>";	

					$outPPUBMED=$outPPUBMED."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');setObservacaoPublicacaoPUBMED();return false;\" ></center></td>";
					$outPPUBMED=$outPPUBMED."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('9');apagarPublicacao();return false;\" ></center></td>";   
					
	    		$outPPUBMED=$outPPUBMED."</tr>";
			}
			break;
			case 'A': { 
	    		$outAPUBMED=$outAPUBMED."<tr>";

					$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$dadosDep[$i]->idinv;
		    		$outAPUBMED=$outAPUBMED."</td>";

		    		$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$dadosDep[$i]->ano;
		    		$outAPUBMED=$outAPUBMED."</td>";

		    		$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$dadosDep[$i]->nomepublicacao;
		    		$outAPUBMED=$outAPUBMED."</td>";

		    		$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$dadosDep[$i]->issn;
		    		$outAPUBMED=$outAPUBMED."</td>";

		    		$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$dadosDep[$i]->isbn;
		    		$outAPUBMED=$outAPUBMED."</td>";

		    		$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$dadosDep[$i]->titulo;
		    		$outAPUBMED=$outAPUBMED."</td>";

		    		$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$dadosDep[$i]->autores;
		    		$outAPUBMED=$outAPUBMED."</td>";

					$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$dadosDep[$i]->volume;
		    		$outAPUBMED=$outAPUBMED."</td>";

					$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$dadosDep[$i]->issue;
		    		$outAPUBMED=$outAPUBMED."</td>";

					$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$dadosDep[$i]->ar;
		    		$outAPUBMED=$outAPUBMED."</td>";

					$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$dadosDep[$i]->colecao;
		    		$outAPUBMED=$outAPUBMED."</td>";

					$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$dadosDep[$i]->primpagina;
		    		$outAPUBMED=$outAPUBMED."</td>";

					$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$dadosDep[$i]->ultpagina;
		    		$outAPUBMED=$outAPUBMED."</td>";

		    		$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$dadosDep[$i]->citacoes;
		    		$outAPUBMED=$outAPUBMED."</td>";

		    		$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$dadosDep[$i]->ifactor;
		    		$outAPUBMED=$outAPUBMED."</td>";

		    		$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$dadosDep[$i]->conftitle;
		    		$outAPUBMED=$outAPUBMED."</td>";

		    		$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$dadosDep[$i]->language;
		    		$outAPUBMED=$outAPUBMED."</td>";

		    		$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$dadosDep[$i]->editor;
		    		$outAPUBMED=$outAPUBMED."</td>";

		    		$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.getTipoEstado($dadosDep[$i]->estado);
		    		$outAPUBMED=$outAPUBMED."</td>";	

					$outAPUBMED=$outAPUBMED."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');setObservacaoPublicacaoPUBMED();return false;\" ></center></td>";
					$outAPUBMED=$outAPUBMED."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('9');apagarPublicacao();return false;\" ></center></td>";   
								
	    		$outAPUBMED=$outAPUBMED."</tr>";
			}
			break;
			default:{ 
		        $outRestoPUBMED=$outRestoPUBMED."<tr>";

					$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$dadosDep[$i]->idinv;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";

		    		$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$dadosDep[$i]->ano;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";

		    		$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$dadosDep[$i]->tipo;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";

		    		$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$dadosDep[$i]->nomepublicacao;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";

		    		$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$dadosDep[$i]->issn;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";

		    		$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$dadosDep[$i]->titulo;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";

		    		$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$dadosDep[$i]->autores;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";

					$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$dadosDep[$i]->volume;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";

					$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$dadosDep[$i]->issue;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";

					$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$dadosDep[$i]->ar;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";

					$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$dadosDep[$i]->primpagina;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";

					$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$dadosDep[$i]->ultpagina;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";

		    		$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$dadosDep[$i]->citacoes;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";

		    		$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$dadosDep[$i]->ifactor;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";

		    		$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$dadosDep[$i]->conftitle;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";

		    		$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$dadosDep[$i]->language;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";

		    		$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$dadosDep[$i]->editor;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";

		    		$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$dadosDep[$i]->tipofmup;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";
					
					$outRestoPUBMED=$outRestoPUBMED."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');setObservacaoPublicacaoPUBMED();return false;\" ></center></td>";
					$outRestoPUBMED=$outRestoPUBMED."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('9');apagarPublicacao();return false;\" ></center></td>";   
			
	    		$outRestoPUBMED=$outRestoPUBMED."</tr>";
		    }
			break;
		}
	}
	
	if($outJPUBMED!=""){echo"<table id='JPUBMED' class='box-table-b'><caption>ARTIGOS</caption>";echo $tcolsJPUBMED;echo "<th colspan='2'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPub(\"J\",\"PUBMED\");return false;'></center></th></tr></thead><tbody>";echo $outJPUBMED;echo "</tbody></table><br />";}
	if($outPRPPUBMED!=""){echo"<table id='PRPPUBMED' class='box-table-b'><caption>PEER REVIEW PROCEEDINGS</caption>";echo $tcolsJPUBMED;echo "<th colspan='2'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPub(\"PRP\",\"PUBMED\");return false;'></center></th></tr></thead><tbody>";echo $outPRPPUBMED;echo "</tbody></table><br />";}
	if($outJAPUBMED!=""){echo"<table id='JAPUBMED' class='box-table-b'><caption>JOURNAL ABSTRACTS</caption>";echo $tcolsJPUBMED;echo "<th colspan='2'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPub(\"JA\",\"PUBMED\");return false;'></center></th></tr></thead><tbody>";echo $outJAPUBMED;echo "</tbody></table><br />";}
	if($outCPPUBMED!=""){echo"<table id='CPPUBMED' class='box-table-b'><caption>CONFERENCE PROCEEDINGS</caption>";echo $tcolsCONFPUBMED;echo "<th colspan='2'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPub(\"CP\",\"PUBMED\");return false;'></center></th></tr></thead><tbody>";echo $outCPPUBMED;echo "</tbody></table><br />";}
	if($outOAPUBMED!=""){echo"<table id='OAPUBMED' class='box-table-b'><caption>OTHER ABSTRACTS</caption>";echo $tcolsCONFPUBMED;echo "<th colspan='2'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPub(\"OA\",\"PUBMED\");return false;'></center></th></tr></thead><tbody>";echo $outOAPUBMED;echo "</tbody></table><br />";}
	if($outLPUBMED!=""){echo"<table id='LPUBMED' class='box-table-b'><caption>LIVROS</caption>";echo $tcols;echo "<th colspan='2'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPub(\"L\",\"PUBMED\");return false;'></center></th></tr></thead><tbody>";echo $outLPUBMED;echo "</tbody></table><br />";}
	if($outCLPUBMED!=""){echo"<table id='CLPUBMED' class='box-table-b'><caption>CAPITULOS DE LIVROS</caption>";echo $tcols;echo "<th colspan='2'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPub(\"CL\",\"PUBMED\");return false;'></center></th></tr></thead><tbody>";echo $outCLPUBMED;echo "</tbody></table><br />";}
	if($outSPUBMED!=""){echo"<table id='SPUBMED' class='box-table-b'><caption>SUMÁRIOS</caption>";echo $tcols;echo "<th colspan='2'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPub(\"S\",\"PUBMED\");return false;'></center></th></tr></thead><tbody>";echo $outSPUBMED;echo "</tbody></table><br />";}	
	if($outJNPUBMED!=""){echo"<table id='JNPUBMED' class='box-table-b'><caption>ARTIGOS - NÃO INGLÊS</caption>";echo $tcolsJPUBMED;echo "<th colspan='2'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPub(\"JN\",\"PUBMED\");return false;'></center></th></tr></thead><tbody>";echo $outJNPUBMED;echo "</tbody></table><br />";}
	if($outPRPNPUBMED!=""){echo"<table id='PRPNPUBMED' class='box-table-b'><caption>PEER REVIEW PROCEEDINGS - NÃO INGLÊS</caption>";echo $tcolsJPUBMED;echo "<th colspan='2'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPub(\"PRPN\",\"PUBMED\");return false;'></center></th></tr></thead><tbody>";echo $outPRPNPUBMED;echo "</tbody></table><br />";}
	if($outSNPUBMED!=""){echo"<table id='SNPUBMED' class='box-table-b'><caption>SUMÁRIO - NÃO INGLÊS</caption>";echo $tcols;echo "<th colspan='2'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPub(\"SN\",\"PUBMED\");return false;'></center></th></tr></thead><tbody>";echo $outSNPUBMED;echo "</tbody></table><br />";}
	if($outLNPUBMED!=""){echo"<table id='LPUBMED' class='box-table-b'><caption>LIVROS - NÃO INGLÊS</caption>";echo $tcols;echo "<th colspan='2'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPub(\"LN\",\"PUBMED\");return false;'></center></th></tr></thead><tbody>";echo $outLNPUBMED;echo "</tbody></table><br />";}
	if($outCLNPUBMED!=""){echo"<table id='CLNPUBMED' class='box-table-b'><caption>CAPITULOS DE LIVROS - NÃO INGLÊS</caption>";echo $tcols;echo "<th colspan='2'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPub(\"CLN\",\"PUBMED\");return false;'></center></th></tr></thead><tbody>";echo $outCLNPUBMED;echo "</tbody></table><br />";}
	if($outCPNPUBMED!=""){echo"<table id='CPNPUBMED' class='box-table-b'><caption>CONFERENCE PROCEEDINGS NÃO INGLÊS</caption>";echo $tcolsCONFPUBMED;echo "<th colspan='2'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPub(\"CPN\",\"PUBMED\");return false;'></center></th></tr></thead><tbody>";echo $outCPNPUBMED;echo "</tbody></table><br />";}
	if($outPPUBMED!=""){echo"<table id='PPUBMED' class='box-table-b'><caption>PATENTES</caption>";echo $tcols;echo "<th colspan='2'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPub(\"P\",\"PUBMED\");return false;'></center></th></tr></thead><tbody>";echo $outPPUBMED;echo "</tbody></table><br />";}
	if($outAPUBMED!=""){echo"<table id='APUBMED' class='box-table-b'><caption>AGRADECIMENTOS</caption>";echo $tcols;echo "<th colspan='2'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPub(\"A\",\"PUBMED\");return false;'></center></th></tr></thead><tbody>";echo $outAPUBMED;echo "</tbody></table><br />";}	
	
	echo "</div>";
	
}

	function getTipoEstado($i) {	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_estadoPublicacoes");	
		while ($row = mysql_fetch_assoc($lValues)) {
			if($i==$row["ID"])
				$resp = $row["DESCRICAO"];
		}	
		$db->disconnect();
		return $resp;
	}
	
	function checkTipoEstado($id,$i){
		global $dadosDep;
		if($dadosDep[$i]->estado==$id)
			return true;
		else
			return false;
	}

?>