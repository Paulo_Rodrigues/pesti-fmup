<?php
function showEditProjectos($dadosDep)
{
	$db = new Database();
	echo "<h4>Projetos / Ensaios Clínicos 2013</h4>
	<div id='projectos'>
		<table id='prj' class='box-table-b'>
			<thead>
				<tr>
					<th>IDINV</th>
					<th>Nome</th>
					<th>Tipo/Entidade</th>
					<th>Entidade Financiadora</th>
					<th>Instituição de Acolhimento</th>
					<th>Montante total solicitado</th>
					<th>Montante total aprovado</th>
					<th>Montante atribuído à FMUP</th>
					<th>É Investigador Responsável?</th>
					<th>Código/ Referência</th>
					<th>Título</th>
					<th>Data Início</th>
					<th>Data Fim</th>
					<th>Link</th>
					<th>Estado</th>
					<th colspan='3'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewProjeto();return false;'></center></th>
				</tr>
			</thead>
			<tbody>";

    foreach ($dadosDep as $i => $value){            
		$checkAcao = $db->checkAcaoExists($dadosDep[$i]->id, $_SESSION['login'], 14);		
		if ($checkAcao) {
			$acao = transformIntoAcaoProjectoObject($dadosDep[$i]->id, $dadosDep[$i]->idinv, $checkAcao);
			echo "<tr>";
				echo "<td style='overflow:hidden;'>".$dadosDep[$i]->idinv."</td>";
				echo "<td style='overflow:hidden;'>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
				echo "<td style='overflow:hidden;'>";
				getTipoEntidade($dadosDep[$i]->entidade);
				echo "</td>";
				echo "<td style='overflow:hidden;'>".$dadosDep[$i]->entidadefinanciadora."</td>";
				echo "<td style='overflow:hidden;'>".$dadosDep[$i]->acolhimento."</td>";
				echo "<td style='overflow:hidden;'>".$dadosDep[$i]->montante." €</td>";
				echo "<td style='overflow:hidden;'>".$dadosDep[$i]->montantea." €</td>";
				echo "<td style='overflow:hidden;'>".$dadosDep[$i]->montantefmup." €</td>";
				echo "<td style='overflow:hidden;'>".checkInv($dadosDep[$i]->invresponsavel)."</td>";
				echo "<td style='overflow:hidden;'>".$dadosDep[$i]->codigo."</td>";
				echo "<td style='overflow:hidden;'>".$dadosDep[$i]->titulo."</td>";
				echo "<td style='overflow:hidden;'>".$dadosDep[$i]->datainicio."</td>";
				echo "<td style='overflow:hidden;'>".$dadosDep[$i]->datafim."</td>";
				echo "<td style='overflow:hidden;'><a href='".$dadosDep[$i]->link."' target='_blank'>".$dadosDep[$i]->link."</a></td>";
				echo "<td style='overflow:hidden;'>".getEstadoProjeto($dadosDep[$i]->estado)."</td>";
				echo "<td style='overflow:hidden;'></td>";
				echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-projectos').text('" . $dadosDep[$i]->id . "');setObservacao();return false;\" ></center></td>";
				echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-projectos').text('" . $dadosDep[$i]->id . "');apagarProjecto();return false;\" ></center></td>";
			echo "</tr>";
			
			echo "<tr>";
				echo "<td style='background:#FFFF33; overflow:hidden;'><img src=\"../../images/arrow_return_down_right.png\" name='navOption'></td>";
				echo "<td style='background:#FFFF33; overflow:hidden;'>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_projectos_tipoentidade_". $acao->id ."'>";
				getTipoEntidade($acao->entidade);
				echo "</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_projectos_entidade_". $acao->id ."'>".$acao->entidadefinanciadora."</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_projectos_acolhimento_". $acao->id ."'>".$acao->acolhimento."</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_projectos_montante_". $acao->id ."'>".$acao->montante." €</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_projectos_montantea_". $acao->id ."'>".$acao->montantea." €</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_projectos_montantefmup_". $acao->id ."'>".$acao->montantefmup." €</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_projectos_invres_". $acao->id ."'>".checkInv($acao->invresponsavel)."</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_projectos_codigo_". $acao->id ."'>".$acao->codigo."</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_projectos_titulo_". $acao->id ."'>".$acao->titulo."</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_projectos_dataini_". $acao->id ."'>".$acao->datainicio."</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_projectos_datafim_". $acao->id ."'>".$acao->datafim."</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_projectos_link_". $acao->id ."'><a href='".$acao->link."' target='_blank'>".$acao->link."</a></td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_projectos_estado_". $acao->id ."'>".getEstadoProjeto($acao->estado)."</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;'><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-projectos').text('" . $acao->id. "');\"></td>";
				echo "<td style='overflow:hidden;'></td>";
				echo "<td style='background:#FFFF33; overflow:hidden;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"apagarAcao('" . $dadosDep[$i]->id . "',14);return false;\" ></center></td>";
			echo "</tr>";
			
		} else {
			echo "<tr>";
				echo "<td style='overflow:hidden;'>".$dadosDep[$i]->idinv."</td>";
				echo "<td style='overflow:hidden;'>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
				echo "<td style='overflow:hidden;' id='td_projectos_tipoentidade_". $dadosDep[$i]->id ."'>";
				getTipoEntidade($dadosDep[$i]->entidade);
				echo "</td>";
				echo "<td style='overflow:hidden;' id='td_projectos_entidade_". $dadosDep[$i]->id ."'>".$dadosDep[$i]->entidadefinanciadora."</td>";
				echo "<td style='overflow:hidden;' id='td_projectos_acolhimento_". $dadosDep[$i]->id ."'>".$dadosDep[$i]->acolhimento."</td>";
				echo "<td style='overflow:hidden;' id='td_projectos_montante_". $dadosDep[$i]->id ."'>".$dadosDep[$i]->montante." €</td>";
				echo "<td style='overflow:hidden;' id='td_projectos_montantea_". $dadosDep[$i]->id ."'>".$dadosDep[$i]->montantea." €</td>";
				echo "<td style='overflow:hidden;' id='td_projectos_montantefmup_". $dadosDep[$i]->id ."'>".$dadosDep[$i]->montantefmup." €</td>";
				echo "<td style='overflow:hidden;' id='td_projectos_invres_". $dadosDep[$i]->id ."'>".checkInv($dadosDep[$i]->invresponsavel)."</td>";
				echo "<td style='overflow:hidden;' id='td_projectos_codigo_". $dadosDep[$i]->id ."'>".$dadosDep[$i]->codigo."</td>";
				echo "<td style='overflow:hidden;' id='td_projectos_titulo_". $dadosDep[$i]->id ."'>".$dadosDep[$i]->titulo."</td>";
				echo "<td style='overflow:hidden;' id='td_projectos_dataini_". $dadosDep[$i]->id ."'>".$dadosDep[$i]->datainicio."</td>";
				echo "<td style='overflow:hidden;' id='td_projectos_datafim_". $dadosDep[$i]->id ."'>".$dadosDep[$i]->datafim."</td>";
				echo "<td style='overflow:hidden;' id='td_projectos_link_". $dadosDep[$i]->id ."'><a href='".$dadosDep[$i]->link."' target='_blank'>".$dadosDep[$i]->link."</a></td>";
				echo "<td style='overflow:hidden;' id='td_projectos_estado_". $dadosDep[$i]->id ."'>".getEstadoProjeto($dadosDep[$i]->estado)."</td>";
				echo "<td style='overflow:hidden;'><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-projectos').text('" . $dadosDep[$i]->id. "');\"></td>";
				echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-projectos').text('" . $dadosDep[$i]->id . "');apagarProjecto();return false;\" ></center></td>";
				echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-projectos').text('" . $dadosDep[$i]->id . "');setObservacaoProjectos();return false;\" ></center></td>";
			echo "</tr>";
		}			
    }

    echo "</tbody>
    </table>
    <p id='chave-projectos' hidden></p>
	<p id='login' hidden>" . $_SESSION['login'] . "</p>
	<p id='dep' hidden>" .$_SESSION['departamento'] . "</p>
</div>
";

}
function getTipoEntidade($i) {
		$db = new Database();
		$lValues =$db->getLookupValues("lista_tipoentidadefinanciadora");
		while ($row = mysql_fetch_assoc($lValues)) {	
			if($i==$row["ID"])
				echo $row["DESCRICAO"];
		}
		$db->disconnect();
	}
	
	function checkInv($i){
		global $dadosNewDep;
		if($i==1)
			return "Sim";
		else 
			return "Não";
	}
	
	function getEstadoProjeto($i) {
		global $dadosNewDep;
		$db = new Database();
		$lValues =$db->getLookupValues("lista_estadoProjetos");
		$texto="";
		while ($row = mysql_fetch_assoc($lValues)) {
			if($i==$row["ID"])
				$texto=$row["DESCRICAO"];
		}
		$db->disconnect();
		return $texto;
	}

	function transformIntoAcaoProjectoObject($id, $idinv, $query)
	{
		$cena = explode("ENTIDADE=",$query);
				
		$cena1 = explode(", MONTANTE=",$cena[1]);
		$entidade = $cena1[0];
		
		$cena2 = explode(", MONTANTEA=",$cena1[1]);
		$montante = $cena2[0];
		
		$cena3 = explode(", MONTANTEFMUP=",$cena2[1]);
		$montantea= $cena3[0];
		
		$cena4 = explode(", INVRESPONSAVEL=",$cena3[1]);
		$montantefmup= $cena4[0];
		
		$cena5 = explode(", CODIGO='",$cena4[1]);
		$invres= $cena5[0];
		
		$cena6 = explode("', TITULO='",$cena5[1]);
		$codigo= $cena6[0];
				
		$cena7 = explode("', DATAINICIO='",$cena6[1]);
		$titulo= $cena7[0];
		
		$cena8 = explode("', DATAFIM='",$cena7[1]);
		$datainicio = $cena8[0];
		
		$cena9 = explode("', ENTIDADEFINANCIADORA='",$cena8[1]);
		$datafim = $cena9[0];
		
		$cena10 = explode("', ACOLHIMENTO='",$cena9[1]);
		$entidadefinanciadora = $cena10[0];
		
		$cena11 = explode("', LINK='",$cena10[1]);
		$acolhimento = $cena11[0];
		
		$cena12 = explode("', ESTADO=",$cena11[1]);
		$link = $cena12[0];
		
		$cena13 = explode(" where",$cena12[1]);
		$estado = $cena13[0];
		
		$acao = new Projecto(
			$id, $idinv, $entidade, $montante, $montantea, $montantefmup, $invres, $codigo, $titulo, $datainicio, $datafim, $entidadefinanciadora, $acolhimento, $link, $estado
		);
		
		return $acao;
	}
	
?>