<?php

function showEditUnidadesInvestigacao($dadosDep) {
	$db = new Database();
	
	echo "<h3>Unidades de Investigação</h3>\n";
			
	echo "<div id='unidadesInvestigacao'>";		
	echo "<table id='uniInv' class='box-table-b'>
			<thead>
				<tr>
					<th>Id Inv</th>
					<th>Nome</th>
					<th>Unidade de Investigação</th>
					<th>Avaliação 2007</th>
					<th>Percentagem</th>
					<th>Responsável pela Unidade</th>
					<th colspan='3'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewUniInv();return false;'></center></th>
				</tr>
			</thead>
		<tbody>";
		
	foreach ($dadosDep as $i => $value){ 
		$checkAcao = $db->checkAcaoExists($dadosDep[$i]->id, $_SESSION['login'], 8);		
		if ($checkAcao) {
			$acao = transformIntoAcaoUniInvObject($dadosDep[$i]->id, $dadosDep[$i]->idinv, $checkAcao);
			echo "<tr>";
				echo "<td>";echo $dadosDep[$i]->idinv;echo "</td>";			
				echo "<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";	
				echo "<td>"; echo $dadosDep[$i]->unidade; echo "</td>";				
				echo "<td>"; echo $dadosDep[$i]->ava2007; echo "</td>";				
				echo "<td>"; echo $dadosDep[$i]->percentagem; echo "</td>";	
				if ($dadosDep[$i]->responsavel==1) {
					echo "<td>Sim</td>";
				}else {
					echo "<td>Não</td>";
				}
				echo "<td></td>";
				echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-uniInv').text('" . $dadosDep[$i]->id . "');setObservacaoUniInv();return false;\" ></center></td>";				
				echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-uniInv').text('" . $dadosDep[$i]->id . "');apagarUniInv();return false;\" ></center></td>";
			echo "</tr>";
			
			echo "<tr>";
				echo "<td style='background:#FFFF33; overflow:hidden;'><img src=\"../../images/arrow_return_down_right.png\" name='navOption'></td>";		
				echo "<td style='background:#FFFF33; overflow:hidden;'>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";		
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_uni_inv_unidade_" . $dadosDep[$i]->id . "'>"; echo $acao['unidade']; echo "</td>";				
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_uni_inv_ava2007_" . $dadosDep[$i]->id . "'>"; echo $acao['ava2007']; echo "</td>";				
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_uni_inv_perc_" . $dadosDep[$i]->id . "'>"; echo $acao['percentagem']; echo "</td>";				
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_uni_inv_resp_" . $dadosDep[$i]->id . "'>";
				if ($acao['responsavel']==1) {
					echo "Sim";
				}else {
					echo "Não";
				}
				echo "</td>";
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-uniInv').text('" . $dadosDep[$i]->id . "');\"></td>";				
				echo "<td></td>";
				echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"apagarAcao('" . $dadosDep[$i]->id . "',8);return false;\" ></center></td>";
			echo "</tr>";
		
		} else {
			echo "<tr>";
				echo "<td>";echo $dadosDep[$i]->idinv;echo "</td>";	
				echo "<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";				
				echo "<td id='td_uni_inv_unidade_" . $dadosDep[$i]->id . "'>"; echo $dadosDep[$i]->unidade; echo "</td>";				
				echo "<td id='td_uni_inv_ava2007_" . $dadosDep[$i]->id . "'>"; echo $dadosDep[$i]->ava2007; echo "</td>";				
				echo "<td id='td_uni_inv_perc_" . $dadosDep[$i]->id . "'>"; echo $dadosDep[$i]->percentagem; echo "</td>";				
				echo "<td id='td_uni_inv_resp_" . $dadosDep[$i]->id . "'>";
				if ($dadosDep[$i]->responsavel==1) {
					echo "Sim";
				}else {
					echo "Não";
				}
				echo "</td>";
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-uniInv').text('" . $dadosDep[$i]->id . "');\"></td>";				
				echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-uniInv').text('" . $dadosDep[$i]->id . "');setObservacaoUniInv();return false;\" ></center></td>";				
				echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-uniInv').text('" . $dadosDep[$i]->id . "');apagarUniInv();return false;\" ></center></td>";
			echo "</tr>";
		}
	}
	
	echo "</tbody></table>
    <p id='chave-uniInv' hidden></p>";
	echo "</div>";
}

function transformIntoAcaoUniInvObject($id, $idinv, $query)
{
	$acao = array();
	
	$cena = explode("UNIDADE='",$query);
			
	$cena1 = explode("', AVA2007='",$cena[1]);
	$acao['unidade'] = $cena1[0];

	$cena2 = explode("', PERCENTAGEM=",$cena1[1]);
	$acao['ava2007'] = $cena2[0];
	
	$cena3 = explode(", RESPONSAVEL=",$cena2[1]);
	$acao['percentagem'] = $cena3[0];

	$cena4 = explode(" where",$cena3[1]);
	$acao['responsavel'] = $cena4[0];

	return $acao;
}
?>