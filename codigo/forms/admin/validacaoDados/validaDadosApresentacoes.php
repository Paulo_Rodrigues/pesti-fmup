<?php

function showEditApresentacoes($dadosDep) {
	$db = new Database();
	
	echo "<h3>Apresentações</h3>\n";
	echo "<div id='apresentacoes'>";
		echo "<table id='apr' class='box-table-b'>		
				<thead>	
					<tr>
						<td colspan='2'></td>	
						<td colspan='3'><h3>Internacionais</h3></td>
						<td colspan='3'><h3>Nacionais</h3></td>
					</tr>
					<tr>
						<th>IDINV</th>						
						<th>Nome</th>		
						<th>Apresentações por convite</th>
						<th>Apresentações como participante</th>
						<th>Seminários Apresentados</th>
						<th>Apresentações por convite</th>
						<th>Apresentações como participante</th>
						<th>Seminários Apresentados</th>
						<th colspan='3'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewApresentacao();return false;'></center></th>
					</tr>
				</thead>							
				<tbody>";
	
	foreach ($dadosDep as $i => $value){ 
		if($dadosDep[$i]->iconfconv!=0 || 
				$dadosDep[$i]->nconfconv!=0 || 
				$dadosDep[$i]->iconfpart!=0 ||
				$dadosDep[$i]->nconfpart!=0 ||
				$dadosDep[$i]->isem!=0 ||
				$dadosDep[$i]->nsem!=0){
				
			$checkAcao = $db->checkAcaoExists($dadosDep[$i]->id, $_SESSION['login'], 16);		
			if ($checkAcao) {
				$acao = transformIntoAcaoApresentacaoObject($dadosDep[$i]->id, $dadosDep[$i]->idinv, $checkAcao);
				echo "<tr>";
					echo "<td>".$dadosDep[$i]->idinv."</td>";
					echo "<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
					echo "<td>".$dadosDep[$i]->iconfconv."</td>";
					echo "<td>".$dadosDep[$i]->iconfpart."</td>";
					echo "<td>".$dadosDep[$i]->isem."</td>";
					echo "<td>".$dadosDep[$i]->nconfconv."</td>";
					echo "<td>".$dadosDep[$i]->nconfpart."</td>";
					echo "<td>".$dadosDep[$i]->nsem."</td>";
					echo "<td></td>";
					echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-apresentacoes').text('" . $dadosDep[$i]->id . "');setObservacaoApresentacao();return false;\" ></center></td>";				
					echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-apresentacoes').text('" . $dadosDep[$i]->id . "');apagarApresentacao();return false;\" ></center></td>";
				echo "</tr>";
				
				echo "<tr>";
					echo "<td style='background:#FFFF33; overflow:hidden;'><img src=\"../../images/arrow_return_down_right.png\" name='navOption'></td>";
					echo "<td style='background:#FFFF33; overflow:hidden;'>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_apresentacoes_iconfconv_" . $dadosDep[$i]->id . "'>".$acao['iconfconv']."</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_apresentacoes_iconfpart_" . $dadosDep[$i]->id . "'>".$acao['iconfpart']."</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_apresentacoes_isem_" . $dadosDep[$i]->id . "'>".$acao['isem']."</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_apresentacoes_nconfconv_" . $dadosDep[$i]->id . "'>".$acao['nconfconv']."</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_apresentacoes_nconfpart_" . $dadosDep[$i]->id . "'>".$acao['nconfpart']."</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_apresentacoes_nsem_" . $dadosDep[$i]->id . "'>".$acao['nsem']."</td>";
					echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-apresentacoes').text('" . $dadosDep[$i]->id . "');\"></td>";
					echo "<td></td>";
					echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"apagarAcao('" . $dadosDep[$i]->id . "',16);return false;\" ></center></td>";
				echo "</tr>";
				
			} else {
				echo "<tr>";
					echo "<td>".$dadosDep[$i]->idinv."</td>";
					echo "<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
					echo "<td id='td_apresentacoes_iconfconv_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->iconfconv."</td>";
					echo "<td id='td_apresentacoes_iconfpart_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->iconfpart."</td>";
					echo "<td id='td_apresentacoes_isem_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->isem."</td>";
					echo "<td id='td_apresentacoes_nconfconv_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->nconfconv."</td>";
					echo "<td id='td_apresentacoes_nconfpart_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->nconfpart."</td>";
					echo "<td id='td_apresentacoes_nsem_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->nsem."</td>";
					echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-apresentacoes').text('" . $dadosDep[$i]->id . "');\"></td>";
					echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-apresentacoes').text('" . $dadosDep[$i]->id . "');setObservacaoApresentacao();return false;\" ></center></td>";				
					echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-apresentacoes').text('" . $dadosDep[$i]->id . "');apagarApresentacao();return false;\" ></center></td>";
				echo "</tr>";
			}
		}
	}
	
	echo "<tbody>
	</table>
	<p id='chave-apresentacoes' hidden></p>
</div>";
}

function transformIntoAcaoApresentacaoObject($id, $idinv, $query) {
	$acao = array();
	
	$cena = explode("ICONFCONV=",$query);
				
	$cena1 = explode(", ICONFPART=",$cena[1]);
	$acao['iconfconv'] = $cena1[0];
	
	$cena2 = explode(", ISEM=",$cena1[1]);
	$acao['iconfpart']= $cena2[0];
	
	$cena3 = explode(", NCONFCONV=",$cena2[1]);
	$acao['isem'] = $cena3[0];	
	
	$cena4 = explode(", NCONFPART=",$cena3[1]);
	$acao['nconfconv']= $cena4[0];	
	
	$cena5 = explode(", NSEM=",$cena4[1]);
	$acao['nconfpart']= $cena5[0];
				
	$cena6 = explode(" where",$cena5[1]);
	$acao['nsem'] = $cena6[0];
	
	return $acao;
}

?>