<?php

function showEditOrientacoes($dadosDep) {
	$db = new Database();
		
	echo "<h3>Orientações</h3>
		  <div id='orientacoes'>\n";

	echo "<table id='orientacao' class='box-table-b'>
			<thead>
				<tr>
					<th>IDINV</th>
					<th>Nome</th>
					<th>Tipo de Orientação</th>
					<th>Tipo de Orientador</th>
					<th>Estado</th>
					<th>Título da Tese em Português</th>
					<th colspan='3'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewOrientacao();return false;'></center></th>
				</tr>
			</thead>
			<tbody>";
    foreach ($dadosDep as $i => $value) {
        $checkAcao = $db->checkAcaoExists($dadosDep[$i]->id, $_SESSION['login'], 22);
        if ($checkAcao) {
            $acao = transformIntoAcaoOrientacaoObject($dadosDep[$i]->id, $dadosDep[$i]->idinv, $checkAcao);
				
			echo "<tr>";
				echo "<td>".$dadosDep[$i]->idinv."</td>";
				echo "<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
				echo "<td>";getTipoOrientacao($dadosDep[$i]->tipo_orientacao);echo "</td>";
				echo "<td>";	getTipoOrientador($dadosDep[$i]->tipo_orientador);echo "</td>";
				echo "<td>";getOrientacaoEstado($dadosDep[$i]->tipo_orientacao);echo "</td>";
				echo "<td>".$dadosDep[$i]->titulo."</td>";
				echo "<td></td>";
				echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-orientacoes').text('" . $dadosDep[$i]->id . "');setObservacaoOrientacao();return false;\" ></center></td>";
				echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-orientacoes').text('" . $dadosDep[$i]->id . "');apagarOrientacao();return false;\" ></center></td>";    
			echo "</tr>";	   
		
			echo "<tr>";
				echo "<td style='background:#FFFF33; overflow:hidden;'><img src=\"../../images/arrow_return_down_right.png\" name='navOption'></td>";
				echo "<td style='background:#FFFF33; overflow:hidden;'>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_orientacoes_tipo_" . $dadosDep[$i]->id . "'>";getTipoOrientacao($acao['tipo_orientacao']);echo "</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_orientacoes_tipoori_" . $dadosDep[$i]->id . "'>";	getTipoOrientador($acao['tipo_orientador']);echo "</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_orientacoes_estado_" . $dadosDep[$i]->id . "'>";getOrientacaoEstado($acao['tipo_orientacao']);echo "</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_orientacoes_titulo_" . $dadosDep[$i]->id . "'>".$acao['titulo']."</td>";
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-orientacoes').text('" . $dadosDep[$i]->id . "');\"></td>";
				echo "<td></td>";
				echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"apagarAcao('" . $dadosDep[$i]->id . "',22);return false;\" ></center></td>";    
			echo "</tr>";	
		
		} else {
		
			echo "<tr>";
				echo "<td>".$dadosDep[$i]->idinv."</td>";
				echo "<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
				echo "<td id='td_orientacoes_tipo_" . $dadosDep[$i]->id . "'>";getTipoOrientacao($dadosDep[$i]->tipo_orientacao);	echo "</td>";
				echo "<td id='td_orientacoes_tipoori_" . $dadosDep[$i]->id . "'>";getTipoOrientador($dadosDep[$i]->tipo_orientador);echo "</td>";
				echo "<td id='td_orientacoes_estado_" . $dadosDep[$i]->id . "'>";	getOrientacaoEstado($dadosDep[$i]->tipo_orientacao);echo "</td>";
				echo "<td id='td_orientacoes_titulo_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->titulo."</td>";
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-orientacoes').text('" . $dadosDep[$i]->id . "');\"></td>";
				echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-orientacoes').text('" . $dadosDep[$i]->id . "');setObservacaoOrientacao();return false;\" ></center></td>";
				echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-orientacoes').text('" . $dadosDep[$i]->id . "');apagarOrientacao();return false;\" ></center></td>";    
			echo "</tr>";	   
		}
	}
	
	 echo "</tbody>
    </table>
    <p id='chave-orientacoes' hidden></p>
</div>";   
}	

function transformIntoAcaoOrientacaoObject($id, $indinv, $query) {

	$acao = array();
        
    $cena = explode("TIPO_ORIENTACAO=", $query);
    
    $cena1 = explode(", TIPO_ORIENTADOR=", $cena[1]);
    $acao['tipo_orientacao'] = $cena1[0];
    
    $cena2 = explode(", ESTADO=", $cena1[1]);
    $acao['tipo_orientador'] = $cena2[0];
    
    $cena3 = explode(", TITULO='", $cena2[1]);
    $acao['estado'] = $cena3[0];
    
    $cena4 = explode("' where", $cena3[1]);
    $acao['titulo'] = $cena4[0];      
    
    return $acao;
}


function getTipoOrientacao($i) {
	global $dadosDep;
	$db = new Database();
	$lValues =$db->getLookupValues("lista_tipoOrientacao");
	while ($row = mysql_fetch_assoc($lValues)) {	
		if($i==$row["ID"])
			echo $row["DESCRICAO"];
	}
	$db->disconnect();				
}	

function getTipoOrientador($i) {	
	$db = new Database();
	$lValues =$db->getLookupValues("lista_tipoOrientador");	
	while ($row = mysql_fetch_assoc($lValues)) {	
		if($i==$row["ID"])
			echo $row["DESCRICAO"];
	}
	$db->disconnect();				
}	

function getOrientacaoEstado($i) {	
	$db = new Database();
	$lValues =$db->getLookupValues("lista_estadosOrientacao");
	while ($row = mysql_fetch_assoc($lValues)) {
		if($i==$row["ID"])
			echo $row["DESCRICAO"];
	}
	$db->disconnect();	
}
?>