<?php

function showEditConferencias($dadosDep) {
	$db = new Database();
	echo "<h3>Conferências Organizadas (LOC)</h3>\n
		  <div id='conferencias'> ";

	echo "<table id='cnf' class='box-table-b'>
			<thead>
				<tr>
					<th>IDINV</th>
					<th>Nome</th>	
					<th>Âmbito</th>
					<th>Título</th>
					<th>Data Início</th>
					<th>Data Fim</th>
					<th>Local (cidade,país)</th>
					<th>Nº Participantes</th>
					<th>Link</th>
					<th colspan='3'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewConferencia();return false;'></center></th>
				</tr>
			</thead>
			<tbody>";
			
	foreach ($dadosDep as $i => $value){ 
		$checkAcao = $db->checkAcaoExists($dadosDep[$i]->id, $_SESSION['login'], 15);		
		if ($checkAcao) {
			$acao = transformIntoAcaoConfObject($dadosDep[$i]->id, $dadosDep[$i]->idinv, $checkAcao);
			echo "<tr>";
				echo "<td>".$dadosDep[$i]->idinv."</td>";
				echo "<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";				
				echo "<td>";
				getAmbitoConf($dadosDep[$i]->ambito);
				echo "</td>";									
				echo "<td>".$dadosDep[$i]->titulo."</td>";				
				echo "<td>".$dadosDep[$i]->datainicio."</td>";	
				echo "<td>".$dadosDep[$i]->datafim."</td>";	
				echo "<td>".$dadosDep[$i]->local."</td>";
				echo "<td>".$dadosDep[$i]->participantes."</td>";
				echo "<td><a href='".$dadosDep[$i]->link."' target='_blank'>".$dadosDep[$i]->link."</a></td>";
				echo "<td></td>";
				echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-conf').text('" . $dadosDep[$i]->id . "');setObservacaoConf();return false;\" ></center></td>";				
				echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-conf').text('" . $dadosDep[$i]->id . "');apagarConferencia();return false;\" ></center></td>";
			echo "</tr>";	 

			echo "<tr>";
				echo "<td style='background:#FFFF33; overflow:hidden;'><img src=\"../../images/arrow_return_down_right.png\" name='navOption'></td>";
				echo "<td style='background:#FFFF33; overflow:hidden;'>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";	
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_conferencias_ambito_" . $dadosDep[$i]->id . "'>";
				getAmbitoConf($acao['ambito']);
				echo "</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_conferencias_titulo_" . $dadosDep[$i]->id . "'>".$acao['titulo']."</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_conferencias_dataini_" . $dadosDep[$i]->id . "'>".$acao['datainicio']."</td>";	
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_conferencias_datafim_" . $dadosDep[$i]->id . "'>".$acao['datafim']."</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_conferencias_local_" . $dadosDep[$i]->id . "'>".$acao['local']."</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_conferencias_npart_" . $dadosDep[$i]->id . "'>".$acao['participantes']."</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_conferencias_link_" . $dadosDep[$i]->id . "'><a href='".$acao['link']."' target='_blank'>".$acao['link']."</a></td>";
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-conf').text('" . $dadosDep[$i]->id. "');\"></td>";
            	echo "<td></td>";
				echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"apagarAcao('" . $dadosDep[$i]->id . "',15);return false;\" ></center></td>";
			echo "</tr>";	    
			
		} else {
			echo "<tr>";
				echo "<td>".$dadosDep[$i]->idinv."</td>";
				echo "<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
			   	echo "<td id='td_conferencias_ambito_" . $dadosDep[$i]->id . "'>";
				getAmbitoConf($dadosDep[$i]->ambito);
				echo "</td>";
				echo "<td id='td_conferencias_titulo_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->titulo."</td>";
				echo "<td id='td_conferencias_dataini_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->datainicio."</td>";	
				echo "<td id='td_conferencias_datafim_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->datafim."</td>";	
				echo "<td id='td_conferencias_local_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->local."</td>";
				echo "<td id='td_conferencias_npart_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->participantes."</td>";
				echo "<td id='td_conferencias_link_" . $dadosDep[$i]->id . "'><a href='".$dadosDep[$i]->link."' target='_blank'>".$dadosDep[$i]->link."</a></td>";
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-conf').text('" . $dadosDep[$i]->id. "');\"></td>";
            	echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-conf').text('" . $dadosDep[$i]->id . "');setObservacaoConf();return false;\" ></center></td>";				
				echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-conf').text('" . $dadosDep[$i]->id . "');apagarConferencia();return false;\" ></center></td>";
			echo "</tr>";	    	
		}
	}
		
    echo "</tbody>
    </table>
    <p id='chave-conf' hidden></p>
    </div>";		
}	

function transformIntoAcaoConfObject($id, $idinv, $query) {
	$acao = array();
	
	$cena = explode("AMBITO=",$query);
				
	$cena1 = explode(", DATAINICIO='",$cena[1]);
	$acao['ambito'] = $cena1[0];	
	
	$cena2 = explode("', DATAFIM='",$cena1[1]);
	$acao['datainicio']= $cena2[0];
	
	$cena3 = explode("', TITULO='",$cena2[1]);
	$acao['datafim']= $cena3[0];
	
	$cena4 = explode("', LOCAL='",$cena3[1]);
	$acao['titulo']= $cena4[0];
	
	$cena5 = explode("', PARTICIPANTES=",$cena4[1]);
	$acao['local']= $cena5[0];
			
	$cena6 = explode(", LINK='",$cena5[1]);
	$acao['participantes']= $cena6[0];
		
	$cena7 = explode("' where",$cena6[1]);
	$acao['link'] = $cena7[0];
	
	return $acao;
}

function getTipoConf($i) {	
	$db = new Database();
	$lValues =$db->getLookupValues("lista_tipoconf");
	while ($row = mysql_fetch_assoc($lValues)) {	
		if($i==$row["ID"])
			echo $row["DESCRICAO"];
	}
	$db->disconnect();				
}	

function getAmbitoConf($i) {
	$db = new Database();
	$lValues =$db->getLookupValues("lista_ambitoconf");	
	while ($row = mysql_fetch_assoc($lValues)) {	
		if($i==$row["ID"])
			echo $row["DESCRICAO"];
	}
	$db->disconnect();				
}	

?>