<?php

function showEditPublicacoesManualPatentes($dadosDep) {
	$db = new Database();
	echo "<h3>Patentes Manuais</h3>\n";
	echo "<div id='pubManualPatentes'>";		
		echo"<table id='pubManPat' class='box-table-b'><caption>PATENTES</caption>
				<thead>
					<tr>
						<th>ID INV</th>
						<th>NOME</th>
						<th>Nº PATENTE</th>
						<th>IPCs</th>
						<th>TÍTULO</th>
						<th>AUTORES</th>		
						<th>DATA de PUBLICAÇÃO</th>
						<th>Link</th>
						<th>Estado</th>
						<th colspan='2'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPub(\"Pat\",\"\");return false;'></center></th>
					</tr>
				</thead>
				<tbody>";
				
	 foreach ($dadosDep as $i => $value){            
		$checkAcao = $db->checkAcaoExists($dadosDep[$i]->id, $_SESSION['login'], 11);		
		if ($checkAcao) {
			$acao = transformIntoAcaoPatenteObject($dadosDep[$i]->id, $dadosDep[$i]->idinv, $checkAcao);			
			echo "<tr>";
				echo "<td>" .$dadosDep[$i]->idinv. "</td>";
				echo "<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";	
				echo "<td>".$dadosDep[$i]->npatente."</td>";
				echo "<td>".$dadosDep[$i]->ipc."</td>";
				echo "<td>".$dadosDep[$i]->titulo."</td>";
				echo "<td>".$dadosDep[$i]->autores."</td>";										
				echo "<td>".$dadosDep[$i]->datapatente."</td>";
				echo "<td><a href='".$dadosDep[$i]->link."' target='_blank'>".$dadosDep[$i]->link."</a></td>";
				echo "<td>".getEstadoPublicacaoPatente($dadosDep[$i]->estado)."</td>";	
				echo "<td></td>";
				echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-pubManPatentes').text('" . $dadosDep[$i]->id . "');setObservacaoPubManPat();return false;\" ></center></td>";
				echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('11');apagarPublicacao();return false;\" ></center></td>";    		
			echo "</tr>";	
			
			echo "<tr>";			
				echo "<td style='background:#FFFF33; overflow:hidden;'><img src=\"../../images/arrow_return_down_right.png\" name='navOption'></td>";
				echo "<td style='background:#FFFF33; overflow:hidden;'>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";	
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_pubManualPatentes_npatente_" . $dadosDep[$i]->id . "'>".$acao['npatente']."</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_pubManualPatentes_ipc_" . $dadosDep[$i]->id . "'>".$acao['ipc']."</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_pubManualPatentes_titulo_" . $dadosDep[$i]->id . "'>".$acao['titulo']."</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_pubManualPatentes_autores_" . $dadosDep[$i]->id . "'>".$acao['autores']."</td>";										
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_pubManualPatentes_datapatente_" . $dadosDep[$i]->id . "'>".$acao['datapatente']."</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_pubManualPatentes_link_" . $dadosDep[$i]->id . "'><a href='".$acao['link']."' target='_blank'>".$acao['link']."</a></td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_pubManualPatentes_estado_" . $dadosDep[$i]->id . "'>".getEstadoPublicacaoPatente($acao['estado'])."</td>";	
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubManPatentes').text('" . $dadosDep[$i]->id . "');$('#chave-pubManPatentes').text('" . $dadosDep[$i]->id . "');\"></td>";
				echo "<td></td>";
				echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"apagarAcao('" . $dadosDep[$i]->id . "',11);return false;\" ></center></td>";    		
			echo "</tr>";
		} else {
		
			echo "<tr>";			
				echo "<td>" . $dadosDep[$i]->idinv. "</td>";
				echo "<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";	
				echo "<td id='td_pubManualPatentes_npatente_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->npatente."</td>";
				echo "<td id='td_pubManualPatentes_ipc_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->ipc."</td>";
				echo "<td id='td_pubManualPatentes_titulo_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->titulo."</td>";
				echo "<td id='td_pubManualPatentes_autores_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->autores."</td>";										
				echo "<td id='td_pubManualPatentes_datapatente_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->datapatente."</td>";
				echo "<td id='td_pubManualPatentes_link_" . $dadosDep[$i]->id . "'><a href='".$dadosDep[$i]->link."' target='_blank'>".$dadosDep[$i]->link."</a></td>";
				echo "<td id='td_pubManualPatentes_estado_" . $dadosDep[$i]->id . "'>".getEstadoPublicacaoPatente($dadosDep[$i]->estado)."</td>";	
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubManPatentes').text('" . $dadosDep[$i]->id . "');\"></td>";
				echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-pubManPatentes').text('" . $dadosDep[$i]->id . "');setObservacaoPubManPat();return false;\" ></center></td>";
				echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('11');apagarPublicacao();return false;\" ></center></td>";    		
			echo "</tr>";		
		}		
	}
	echo "</tbody>";
	echo "</table>";
	echo "<p id='chave-pubManPatentes' hidden></p>";
	echo "<p id='tabela' hidden></p>";
echo "</div>";
}

function transformIntoAcaoPatenteObject($id, $idinv, $query) {
	$acao = array();
	
	$cena = explode("TITULO='",$query);
				
	$cena1 = explode("', AUTORES='",$cena[1]);
	$acao['titulo'] = $cena1[0];
	
	$cena2 = explode("',IPC='",$cena1[1]);
	$acao['autores'] = $cena2[0];
	
	$cena3 = explode("', NPATENTE='",$cena2[1]);
	$acao['ipc']= $cena3[0];
	
	$cena4 = explode("', DATAPATENTE='",$cena3[1]);
	$acao['npatente']= $cena4[0];	
	
	$cena5 = explode("', LINK='",$cena4[1]);
	$acao['datapatente']= $cena5[0];	
	
	$cena6 = explode("', ESTADO=",$cena5[1]);
	$acao['link']= $cena6[0];	
	
	$cena7 = explode(" where",$cena6[1]);
	$acao['estado']= $cena7[0];	
			
	return $acao;
}

function getEstadoPublicacaoPatente($i) {
	$db = new Database();
	$lValues =$db->getLookupValues("lista_estadoPublicacoes");
	$texto="";
	while ($row = mysql_fetch_assoc($lValues)) {
		if($i==$row["ID"])
			$texto=$texto.$row["DESCRICAO"];
	}

	$db->disconnect();
	return $texto;
} 

?>