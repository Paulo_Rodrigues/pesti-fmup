
<?php
define('SCRIPT_DEBUG', true);

require_once '../../classlib/Database.class.inc';
require_once '../../classlib/QuestionarioDepartamento.class.inc';
require_once '../../classlib/ValidaRepetidos.class.inc';

//Interfaces
require_once 'validaDadosInvestigadores.php';
require_once 'validaDadosHabilitacoes.php';
require_once 'validaDadosAgregacoes.php';
require_once 'validaDadosExperienciaProfissional.php';
require_once 'validaDadosDirecoesCurso.php';
require_once 'validaDadosColaboradores.php';
require_once 'validaDadosUnidadesInvestigacao.php';

require_once 'validaDadosPublicacoesISI.php';
require_once 'validaDadosPublicacoesPUBMED.php';
require_once 'validaDadosPublicacoesManualPatentes.php';
require_once 'validaDadosPublicacoesManualInternacional.php';
require_once 'validaDadosPublicacoesManualNacional.php';

require_once 'validaDadosProjectos.php';
require_once 'validaDadosConferencias.php';
require_once 'validaDadosApresentacoes.php';
require_once 'validaDadosArbitragensRevistas.php';
require_once 'validaDadosPremios.php';
require_once 'validaDadosSociedadesCientificas.php';
require_once 'validaDadosRedes.php';
require_once 'validaDadosMissoes.php';
require_once 'validaDadosOrientacoes.php';
require_once 'validaDadosJuris.php';
require_once 'validaDadosOutrasActividades.php';
require_once 'validaDadosAcoesDivulgacao.php';
require_once 'validaDadosProdutos.php';

if(isset($_SESSION['departamento']))
{
	if($_SESSION['departamento'] == 50)
	{
		//Informações de todos os departamentos	
		//$dadosDep = new ValidaRepetidos();		
		//showEditProjectos($dadosDep);
	}
	else
	{
		//Informação de um departamento específico
		$dadosDep = new QuestionarioDepartamento($_SESSION['departamento']);
		$dadosDepRep = new ValidaRepetidosDepartamento($_SESSION['departamento']);	
	
		
		
		echo "<link rel=\"stylesheet\" href=\"../../js/jQuery/themes/base/jquery.ui.all.css\">
		<script>
		$(function () {   
			$( \"#accordion\" ).accordion({collapsible: true,heightStyle: \"content\"});
		});
			
		";
			
		echo "</script>";
		echo "<style>
			body { font-size: 70%; }
			label, input { display:block; }
			input.text { margin-bottom:12px; width:95%; padding: .4em; }
			fieldset#edit { padding:0; border:0; margin-top:25px; }
			h1 { font-size: 1.2em; margin: .6em 0; }
			div#users-contain { width: 350px; margin: 20px 0; }
			div#users-contain table { margin: 1em 0; border-collapse: collapse; width: 100%; }
			div#users-contain table td, div#users-contain table th { border: 1px solid #eee; padding: .6em 10px; text-align: left; }
			.ui-dialog .ui-state-error { padding: .3em; }
			.validateTips { border: 1px solid transparent; padding: 0.3em; }
			thead 
			{ 
				font-size: 95%;
			}
			tbody 
			{ 
				font-size: 85%;
			}
			.ui-autocomplete-category {
				font-weight: bold;
				padding: .2em .4em;
				margin: .8em 0 .2em;
				line-height: 1.5;
			  }
		</style>";
		echo "<fieldset class='normal'>
				<div id=\"accordion\" style='margin: 0px 600px 0px 0px;'>"; 
					showEditInvestigadores($dadosDep->investigadores);
					showEditHabilitacoes($dadosDep->habilitacoes);
					showEditAgregacoes($dadosDep->agregacoes);
					showEditExperienciaProfissional($dadosDep->ocupacaoprofissional);
					showEditDirecoesCurso($dadosDep->dirCursos);
					showEditColaboradores($dadosDep);
					showEditUnidadesInvestigacao($dadosDepRep->unidadesInvestigacao);
					showEditPublicacoesISI($dadosDepRep->publicacoesISI);
					showEditPublicacoesPUBMED($dadosDepRep->publicacoesPUBMED);
					showEditPublicacoesManualPatentes($dadosDepRep->publicacoesMANUALPatentes);					
					showEditPublicacoesManualInternacional($dadosDepRep->publicacoesMANUALInternacional);						
					showEditPublicacoesManualNacional($dadosDepRep->publicacoesMANUALNacional);
					showEditProjectos($dadosDepRep->projectos);	
					showEditConferencias($dadosDepRep->conferencias);
					showEditApresentacoes($dadosDep->apresentacoes);
					showEditArbitragensRevistas($dadosDepRep->arbitragensRevistas);
					showEditPremios($dadosDepRep->premios);
					showEditSociedadesCientificas($dadosDep->sc);
					showEditRedes($dadosDep->redes);
					showEditMissoes($dadosDep->missoes);
					showEditOrientacoes($dadosDep->orientacoes);	
					showEditJuris($dadosDep->juris);
					showEditOutrasActividades($dadosDep->outrasactividades);
					showEditAcoesDivulgacao($dadosDepRep->acoesDivulgacao);
					showEditProdutos($dadosDep->produtos);
		echo "</div>
		</fieldset>";
	}	
}

include("../camposEdicao.php");
  
echo "<link rel=\"stylesheet\" href=\"../../js/jQuery/themes/base/jquery.ui.all.css\">";

echo "
	  <script type='text/javascript' src='../../js/jquery.tablesorter.js'></script>
	  <script>
		$(document).ready(function() {	
		
		$.tablesorter.addParser({ 
			'id': 'data', 
			'is': function(string) { 
				 return false; 
				}, 
			'format': function(string) { 
				if (!string) {
				   return '';
				}    
				var thedate = string.split('-');                        
				var myDate = new Date(thedate[2],thedate[1],thedate[0]);
				return myDate.getTime();
			}, 
			'type': 'numeric' 
		}); 
		
			//Investigadores
			$('#users').tablesorter({
				headers: { 
					7: { 
						sorter:false
					} 
				} 
			});
			$('#habs').tablesorter({
				headers: { 
					6: { 
						sorter:'data' 
					} ,
					7: { 
						sorter:'data' 
					} ,
					11: { 
						sorter:false 
					} 
				} 
			});
			
			$('#agreg').tablesorter({
					headers: { 
						3: { 
							sorter:false 
						}
					} 
			});
			
			$('#caracDoc').tablesorter({
					headers: { 
						4: { 
							sorter:false 
						}
					} 
			});
			$('#caracPos').tablesorter({
					headers: { 
						4: { 
							sorter:false 
						}
					} 
			});
			$('#dircurso').tablesorter({
				headers: { 
					3: { 
						sorter:'data' 
					} ,
					4: { 
						sorter:'data' 
					} ,
					6 : {
						sorter: false
					}
				} 
			});
			$('#colIntAct').tablesorter({
					headers: { 
						4: { 
							sorter:false 
						}
					} 
			});
			$('#colExtAct').tablesorter({
					headers: { 
						5: { 
							sorter:false 
						}
					} 
			});
			$('#uniInv').tablesorter({
					headers: { 
						6: { 
							sorter:false 
						}
					} 
			});
			
			//ISI			
			$('#AISI').tablesorter({
					headers: { 
						20: { 
							sorter:false 
						}
					} 
			});
			$('#PISI').tablesorter({
					headers: { 
						20: { 
							sorter:false 
						}
					} 
			});
			$('#CPNISI').tablesorter({
					headers: { 
						12: { 
							sorter:false 
						}
					} 
			});
			$('#CLNISI').tablesorter({
					headers: { 
						20: { 
							sorter:false 
						}
					} 
			});
			$('#LNISI').tablesorter({
					headers: { 
						20: { 
							sorter:false 
						}
					} 
			});
			$('#SNISI').tablesorter({
					headers: { 
						20: { 
							sorter:false 
						}
					} 
			});
			$('#PRPNISI').tablesorter({
					headers: { 
						12: { 
							sorter:false 
						}
					} 
			});
			$('#JNISI').tablesorter({
					headers: { 
						12: { 
							sorter:false 
						}
					} 
			});
			$('#SISI').tablesorter({
					headers: { 
						20: { 
							sorter:false 
						}
					} 
			});
			$('#CLISI').tablesorter({
					headers: { 
						20: { 
							sorter:false 
						}
					} 
			});
			$('#LISI').tablesorter({
					headers: { 
						20: { 
							sorter:false 
						}
					} 
			});
			$('#OAISI').tablesorter({
					headers: { 
						12: { 
							sorter:false 
						}
					} 
			});
			$('#CPISI').tablesorter({
					headers: { 
						12: { 
							sorter:false 
						}
					} 
			});
			$('#JAISI').tablesorter({
					headers: { 
						12: { 
							sorter:false 
						}
					} 
			});
			$('#PRPISI').tablesorter({
					headers: { 
						12: { 
							sorter:false 
						}
					} 
			});
			$('#JISI').tablesorter({
					headers: { 
						12: { 
							sorter:false 
						}
					} 
			});
			
			//PUBMED
			$('#JPUBMED').tablesorter({
					headers: { 
						12: { 
							sorter:false 
						}
					} 
			});
			$('#PRPPUBMED').tablesorter({
					headers: { 
						12: { 
							sorter:false 
						}
					} 
			});
			$('#JAPUBMED').tablesorter({
					headers: { 
						12: { 
							sorter:false 
						}
					} 
			});
			$('#CPPUBMED').tablesorter({
					headers: { 
						12: { 
							sorter:false 
						}
					} 
			});
			$('#OAPUBMED').tablesorter({
					headers: { 
						12: { 
							sorter:false 
						}
					} 
			});
			$('#CLPUBMED').tablesorter({
					headers: { 
						20: { 
							sorter:false 
						}
					} 
			});
			$('#SPUBMED').tablesorter({
					headers: { 
						20: { 
							sorter:false 
						}
					} 
			});
			$('#JNPUBMED').tablesorter({
					headers: { 
						12: { 
							sorter:false 
						}
					} 
			});
			$('#PRPNPUBMED').tablesorter({
					headers: { 
						12: { 
							sorter:false 
						}
					} 
			});
			$('#CPNPUBMED').tablesorter({
					headers: { 
						12: { 
							sorter:false 
						}
					} 
			});
			$('#LPUBMED').tablesorter({
					headers: { 
						20: { 
							sorter:false 
						}
					} 
			});
			$('#CLNPUBMED').tablesorter({
					headers: { 
						20: { 
							sorter:false 
						}
					} 
			});
			$('#SNPUBMED').tablesorter();
			$('#PPUBMED').tablesorter({
					headers: { 
						20: { 
							sorter:false 
						}
					} 
			});
			$('#APUBMED').tablesorter({
					headers: { 
						20: { 
							sorter:false 
						}
					} 
			});	
			
			//Patentes
			$('#pubManPat').tablesorter({
					headers: { 
						9: { 
							sorter:false 
						}
					} 
			});
			
			//Publicações Manual Internacional			
 			$('#pubManInt-Art').tablesorter({
					headers: { 
						12: { 
							sorter:false 
						}
					} 
			});
			$('#pubManInt-Rev').tablesorter({
					headers: { 
						12: { 
							sorter:false 
						}
					} 
			});
			$('#pubManInt-Sum').tablesorter({
					headers: { 
						12: { 
							sorter:false 
						}
					} 
			});
			$('#pubManInt-Liv').tablesorter({
					headers: { 
						9: { 
							sorter:false 
						}
					} 
			});
			$('#pubManInt-CLiv').tablesorter({
					headers: { 
						12: { 
							sorter:false 
						}
					} 
			});
			$('#pubManInt-Con').tablesorter({
					headers: { 
						11: { 
							sorter:false 
						}
					} 
			});
			$('#pubManInt-Oth').tablesorter({
					headers: { 
						11: { 
							sorter:false 
						}
					} 
			});  
			
			
			//Publicacoes Manual Nacional
			$('#pubManNac-Con').tablesorter({
					headers: { 
						11: { 
							sorter:false 
						}
					} 
			});
			$('#pubManNac-Oth').tablesorter({
					headers: { 
						11: { 
							sorter:false 
						}
					} 
			});
			$('#pubManNac-CLiv').tablesorter({
					headers: { 
						12: { 
							sorter:false 
						}
					} 
			});
			$('#pubManNac-Liv').tablesorter({
					headers: { 
						9: { 
							sorter:false 
						}
					} 
			});
			$('#pubManNac-Sum').tablesorter({
					headers: { 
						12: { 
							sorter:false 
						}
					} 
			});
			$('#pubManNac-Rev').tablesorter({
					headers: { 
						12: { 
							sorter:false 
						}
					} 
			});
			$('#pubManNac-Art').tablesorter({
					headers: { 
						12: { 
							sorter:false 
						}
					} 
			});		
										
			$('#prj').tablesorter({
				headers: { 
					11: {       // Change this to your column position
						'sorter' : 'data' 
					} ,	

					12: {       // Change this to your column position
						'sorter' : 'data' 
					} ,
					15 : { sorter : false }
					
				} 
			});		
			$('#cnf').tablesorter({
				headers: { 
					4: { 
						sorter:'data' 
					} ,
					5: { 
						sorter:'data' 
					} ,
					9 : { sorter:false }
				} 
			});		
			
			$('#apr').tablesorter({
				headers: { 
					8: { 
						sorter:false
					} 
				} 
			});	
			
			$('#arbRevistas').tablesorter({
				headers: { 
					6: { 
						sorter:false 
					} 
				} 
			});		
			$('#premio').tablesorter({
				headers: { 
					7: { 
						sorter:false 
					} 
				} 
			});		
			$('#socCient').tablesorter({
				headers: { 
					4: { 
						sorter:'data' 
					} ,
					5: { 
						sorter:'data' 
					} ,
					8 : { sorter: false }
				} 
			});
			$('#rede').tablesorter({
				headers: { 
					4: { 
						sorter:'data' 
					} ,
					5: { 
						sorter:'data' 
					} ,
					7 : { sorter: false }
				} 
			});
			$('#missao').tablesorter({
				headers: { 
					2: { 
						sorter:'data' 
					}, 
					3: { 
						sorter:'data' 
					} ,
					8 : { sorter: false }
				} 
			});
			$('#orientacao').tablesorter({
				headers: { 
					6: { 
						sorter:false 
					} 
				} 
			});		
			$('#juri').tablesorter({
				headers: { 
					0 : { sorter:false },
					5: { 
						sorter:false
					} 
				} 
			});		
			$('#outrasAct').tablesorter({
				headers: { 
					7: { 
						sorter:false 
					} 
				} 
			});		
			$('#prdts').tablesorter({
				headers: { 
					4: { 
						sorter:false 
					} 
				} 
			});		
			$('#acaoDiv').tablesorter({
				headers: { 
					3: { 
						sorter:'data' 
					} ,
					6 : { sorter: false } 
				} 
			});				
		});
	</script>";
	

?>