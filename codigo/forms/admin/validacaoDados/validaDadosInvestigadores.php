<?php

function showEditInvestigadores($dadosDep) {

	$db = new Database();
	echo "<h4>Investigadores Registados</h4> 
    <div id='investigadores' >   
      <table id='users' class='box-table-b' >
        <thead>
          <tr>
            <th>ID Inv</th>
            <th>Login</th>
			<th>Nome</th>
            <th>Nº Mecanográfico</th>
            <th>Data Nascimento</th>
            <th>Estado</th>
            <th>Data de Lacre</th>
			<th colspan='3'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewInvestigador();return false;' value='Inserir novo Investigador'></center></th>
          </tr>
      </thead>";
	  
	echo "<tbody>";
	foreach ($dadosDep as $i => $value){
		$checkAcao = $db->checkAcaoExists($dadosDep[$i]->id, $_SESSION['login'], 1);
		if ($checkAcao) {
			$acao = transformIntoAcaoInvestigadorObject($checkAcao);
			
			echo "<tr id='user" .$dadosDep[$i]->id. "'>";
				echo "<td>".$dadosDep[$i]->id."</td>";
				echo "<td>".$dadosDep[$i]->login."</td>";
				echo "<td>".$dadosDep[$i]->nome."</td>";
				echo "<td>".$dadosDep[$i]->nummec."</td>";
				echo "<td>".$dadosDep[$i]->datanasc."</td>";
				if($dadosDep[$i]->estado=="0")
					echo "<td style='color:red !important;background:#FFFF33;'>Não lacrado</td>";
				else if($dadosDep[$i]->estado=="-1")
					echo "<td style='color:green !important;background:#FFFF33;'>Novo Investigador</td>";
				else
					echo "<td>Lacrado</td>";
				echo "<td>".$dadosDep[$i]->datasubmissao."</td>";
				echo "<td></td>";
				echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-inv').text('" . $dadosDep[$i]->id . "');setObservacaoInvestigadores();return false;\" ></center></td>";
				echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-inv').text('" . $dadosDep[$i]->id . "');apagarInvestigador();return false;\" ></center></td>";
			echo "</tr>";  
			
			echo "<tr>";				
				echo "<td style='background:#FFFF33; overflow:hidden;'><img src=\"../../images/arrow_return_down_right.png\" name='navOption'></td>";
				echo "<td style='background:#FFFF33; overflow:hidden;'>".$dadosDep[$i]->login."</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td" . $dadosDep[$i]->id . "_nome'>".$acao['nome']."</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td" . $dadosDep[$i]->id . "_nummec'>".$acao['nummec']."</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td" . $dadosDep[$i]->id . "_datanasc'>".$acao['datanasc']."</td>";
				if($dadosDep[$i]->estado=="0")
					echo "<td style='color:red !important;background:#FFFF33;'>Não lacrado</td>";
				else if($dadosDep[$i]->estado=="-1")
					echo "<td style='color:green !important;background:#FFFF33;'>Novo Investigador</td>";
				else
					echo "<td style='background:#FFFF33; overflow:hidden;'>Lacrado</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;'>".$dadosDep[$i]->datasubmissao."</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;'><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-inv').text('" . $dadosDep[$i]->id. "');\"></td>";
				echo "<td></td>";
				echo "<td style='background:#FFFF33; overflow:hidden;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"apagarAcao('" . $dadosDep[$i]->id . "',1);return false;\" ></center></td>";
			echo "</tr>";
			
		} else {
		
			echo "<tr id='user" .$dadosDep[$i]->id. "'>";
				echo "<td>".$dadosDep[$i]->id."</td>";
				echo "<td>".$dadosDep[$i]->login."</td>";
				echo "<td id='td" . $dadosDep[$i]->id . "_nome'>".$dadosDep[$i]->nome."</td>";
				echo "<td id='td" . $dadosDep[$i]->id . "_nummec'>".$dadosDep[$i]->nummec."</td>";
				echo "<td id='td" . $dadosDep[$i]->id . "_datanasc'>".$dadosDep[$i]->datanasc."</td>";
				if($dadosDep[$i]->estado=="0")
					echo "<td style='color:red !important;'>Não lacrado</td>";
				else if($dadosDep[$i]->estado=="-1")
					echo "<td style='color:green !important;'>Novo Investigador</td>";
				else
					echo "<td>Lacrado</td>";
				echo "<td>".$dadosDep[$i]->datasubmissao."</td>";				
				echo "<td style='overflow:hidden;'><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-inv').text('" . $dadosDep[$i]->id. "');\"></td>";
				echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-inv').text('" . $dadosDep[$i]->id . "');setObservacaoInvestigadores();return false;\" ></center></td>";
				echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-inv').text('" . $dadosDep[$i]->id . "');apagarInvestigador();return false;\" ></center></td>";
			echo "</tr>";  
		}
	}
	echo "</tbody></table>
		<p id='chave-inv' hidden></p>
		<input type='hidden' name='apagaRegHabilitacao'/>
	</div>";
}

	function transformIntoAcaoInvestigadorObject($query)
	{
		$cena = explode("NOME='",$query);
				
		$cena1 = explode("',NUMMEC='",$cena[1]);
		$nome = $cena1[0];
		
		$cena2 = explode("',DATANASC='",$cena1[1]);
		$nummec = $cena2[0];
		
		$cena3 = explode("' where",$cena2[1]);
		$datanasc = $cena3[0];
		
		$acao = array();
		$acao['nome'] = $nome;
		$acao['nummec'] = $nummec;
		$acao['datanasc'] = $datanasc;
		
		return $acao;
	}
?>