<?php

function showEditPublicacoesManualInternacional($dadosDep) {
	$db = new Database();		
		
	$outJMAN="";
	$outPRPMAN="";
	$outJAMAN="";
	$outLMAN="";
	$outCLMAN="";
	$outCPMAN="";
	$outOAMAN="";

	$tcolsJ="<thead>
				<tr>
					<th>IDINV</th>					
					<th>NOME</th>
					<th>ISSN</th>
					<th>REVISTA</th>
					<th>TÍTULO</th>
					<th>AUTORES</th>
					<th>VOLUME</th>
					<th>ISSUE</th>
					<th>PRIMEIRA PÁG.</th>
					<th>ÚLTIMA PÁG.</th>
					<th>LINK</th>
					<th>ESTADO</th>";

	$tcolsCONF="<thead>
					<tr>
						<th>IDINV</th>				
						<th>NOME</th>
						<th>ISBN</th>
						<th>TÍTULO DA OBRA</th>
						<th>EDITORES</th>
						<th>TÍTULO ARTIGO</th>
						<th>AUTORES</th>
						<th>PRIMEIRA PÁG.</th>
						<th>ÚLTIMA PÁG.</th>
						<th>LINK</th>
						<th>ESTADO</th>";

	$tcolsPRP="<thead>
					<tr>
						<th>IDINV</th>				
						<th>NOME</th>
						<th>ISSN</th>
						<th>TÍTULO DA OBRA</th>
						<th>EDITORES</th>
						<th>TÍTULO ARTIGO</th>
						<th>AUTORES</th>
						<th>PRIMEIRA PÁG.</th>
						<th>ÚLTIMA PÁG.</th>
						<th>LINK</th>
						<th>ESTADO</th>";

	$tcolsLIV="<thead>
					<tr>
						<th>IDINV</th>				
						<th>NOME</th>
						<th>ISBN</th>
						<th>TÍTULO</th>
						<th>AUTORES</th>		
						<th>EDITOR</th>
						<th>EDITORA</th>
						<th>LINK</th>
						<th>ESTADO</th>";
				
	$tcolsCLIV="<thead>
					<tr>
						<th>IDINV</th>					
						<th>NOME</th>					
						<th>ISBN</th>
						<th>TÍTULO DA OBRA</th>
						<th>EDITORA</th>
						<th>EDITORES</th>			
						<th>TÍTULO ARTIGO</th>
						<th>AUTORES</th>
						<th>PRIMEIRA PÁG.</th>
						<th>ÚLTIMA PÁG.</th>
						<th>LINK</th>
						<th>ESTADO</th>";

	$tcolsPAT="<thead>
					<tr>
						<th>IDINV</th>				
						<th>NOME</th>
						<th>Nº PATENTE</th>
						<th>IPC</th>
						<th>TÍTULO</th>
						<th>AUTORES</th>		
						<th>DATA de PUBLICAÇÃO</th>
						<th>LINK</th>
						<th>ESTADO</th>";

	$tcolsLIV="<thead>
					<tr>
						<th>IDINV</th>				
						<th>NOME</th>
						<th>ISBN</th>
						<th>TÍTULO</th>
						<th>AUTORES</th>		
						<th>EDITOR</th>
						<th>EDITORA</th>
						<th>LINK</th>
						<th>ESTADO</th>";
				
	foreach ($dadosDep as $i => $value){   
	
		$tipo=$dadosDep[$i]->tipofmup;

		switch($tipo){
			case 'J': { 
				$checkAcao = $db->checkAcaoExists($dadosDep[$i]->id, $_SESSION['login'], 12);		
				if ($checkAcao) {
					$acao = transformIntoAcaoPubArtObject($dadosDep[$i]->id, $dadosDep[$i]->idinv, $checkAcao);	
					
					$outJMAN=$outJMAN."<tr>";
						$outJMAN=$outJMAN."<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
						$outJMAN=$outJMAN."<td>".$dadosDep[$i]->idinv."</td>";
						$outJMAN=$outJMAN."<td>".$dadosDep[$i]->issn."</td>";			
						$outJMAN=$outJMAN."<td>".$dadosDep[$i]->nomepublicacao."</td>";
						$outJMAN=$outJMAN."<td>".$dadosDep[$i]->titulo."</td>";
						$outJMAN=$outJMAN."<td>".$dadosDep[$i]->autores."</td>";
						$outJMAN=$outJMAN."<td>".$dadosDep[$i]->volume."</td>";
						$outJMAN=$outJMAN."<td>".$dadosDep[$i]->issue."</td>";
						$outJMAN=$outJMAN."<td>".$dadosDep[$i]->primpagina."</td>";
						$outJMAN=$outJMAN."<td>".$dadosDep[$i]->ultpagina."</td>";
						$outJMAN=$outJMAN."<td><a href='".$dadosDep[$i]->link."' target='_blank'>".$dadosDep[$i]->link."</a></td>";
						$outJMAN=$outJMAN."<td>".getEstadoPublicacaoMI($dadosDep[$i]->estado)."</td>";	
						$outJMAN=$outJMAN."<td></td>";
						$outJMAN=$outJMAN."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-pubIntMan-Art').text('" . $dadosDep[$i]->id . "');setObservacaoPubManIntArt();return false;\" ></center></td>";
						$outJMAN=$outJMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('12');apagarPublicacao();return false;\" ></center></td>";    		
					$outJMAN=$outJMAN."</tr>";
					
					$outJMAN=$outJMAN."<tr>";
						$outJMAN=$outJMAN."<td style='background:#FFFF33; overflow:hidden;'><img src=\"../../images/arrow_return_down_right.png\" name='navOption'></td>";
						$outJMAN=$outJMAN."<td style='background:#FFFF33; overflow:hidden;'>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
						$outJMAN=$outJMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outJMAN_issn_" . $dadosDep[$i]->id . "'>".$acao['issn']."</td>";			
						$outJMAN=$outJMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outJMAN_nomepub_" . $dadosDep[$i]->id . "'>".$acao['nomepublicacao']."</td>";
						$outJMAN=$outJMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outJMAN_titulo_" . $dadosDep[$i]->id . "'>".$acao['titulo']."</td>";
						$outJMAN=$outJMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outJMAN_autores_" . $dadosDep[$i]->id . "'>".$acao['autores']."</td>";
						$outJMAN=$outJMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outJMAN_volume_" . $dadosDep[$i]->id . "'>".$acao['volume']."</td>";
						$outJMAN=$outJMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outJMAN_issue_" . $dadosDep[$i]->id . "'>".$acao['issue']."</td>";
						$outJMAN=$outJMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outJMAN_prim_" . $dadosDep[$i]->id . "'>".$acao['primpagina']."</td>";
						$outJMAN=$outJMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outJMAN_ult_" . $dadosDep[$i]->id . "'>".$acao['ultpagina']."</td>";
						$outJMAN=$outJMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outJMAN_link_" . $dadosDep[$i]->id . "'><a href='".$acao['link']."' target='_blank'>".$acao['link']."</a></td>";
						$outJMAN=$outJMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outJMAN_estado_" . $dadosDep[$i]->id . "'>".getEstadoPublicacaoMI($acao['estado'])."</td>";						
						$outJMAN=$outJMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubIntMan-Art').text('" . $dadosDep[$i]->id . "');$('#tabela').text('12');\"></td>";
						$outJMAN=$outJMAN."<td></td>";
						$outJMAN=$outJMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"apagarAcao('" . $dadosDep[$i]->id . "',12);return false;\" ></center></td>";    		
					$outJMAN=$outJMAN."</tr>";
					
				} else {
					$outJMAN=$outJMAN."<tr>";
						$outJMAN=$outJMAN."<td>".$dadosDep[$i]->idinv."</td>";
						$outJMAN=$outJMAN."<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
						$outJMAN=$outJMAN."<td id='td_outJMAN_issn_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->issn."</td>";			
						$outJMAN=$outJMAN."<td id='td_outJMAN_nomepub_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->nomepublicacao."</td>";
						$outJMAN=$outJMAN."<td id='td_outJMAN_titulo_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->titulo."</td>";
						$outJMAN=$outJMAN."<td id='td_outJMAN_autores_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->autores."</td>";
						$outJMAN=$outJMAN."<td id='td_outJMAN_volume_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->volume."</td>";
						$outJMAN=$outJMAN."<td id='td_outJMAN_issue_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->issue."</td>";
						$outJMAN=$outJMAN."<td id='td_outJMAN_prim_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->primpagina."</td>";
						$outJMAN=$outJMAN."<td id='td_outJMAN_ult_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->ultpagina."</td>";
						$outJMAN=$outJMAN."<td id='td_outJMAN_link_" . $dadosDep[$i]->id . "'><a href='".$dadosDep[$i]->link."' target='_blank'>".$dadosDep[$i]->link."</a></td>";
						$outJMAN=$outJMAN."<td id='td_outJMAN_estado_" . $dadosDep[$i]->id . "'>".getEstadoPublicacaoMI($dadosDep[$i]->estado)."</td>";						
						$outJMAN=$outJMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubIntMan-Art').text('" . $dadosDep[$i]->id . "');$('#tabela').text('12');\"></td>";
						$outJMAN=$outJMAN."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-pubIntMan-Art').text('" . $dadosDep[$i]->id . "');setObservacaoPubManIntArt();return false;\" ></center></td>";
						$outJMAN=$outJMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('12');apagarPublicacao();return false;\" ></center></td>";    		
					$outJMAN=$outJMAN."</tr>";
				}				
			}
			break;			
			case 'PRP': { 
				$checkAcao = $db->checkAcaoExists($dadosDep[$i]->id, $_SESSION['login'], 12);		
				if ($checkAcao) {
					$acao = transformIntoAcaoPubArtObject($dadosDep[$i]->id, $dadosDep[$i]->idinv, $checkAcao);	
					
					$outPRPMAN=$outPRPMAN."<tr>";
						$outPRPMAN=$outPRPMAN."<td>".$dadosDep[$i]->idinv."</td>";
						$outPRPMAN=$outPRPMAN."<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
						$outPRPMAN=$outPRPMAN."<td>".$dadosDep[$i]->issn."</td>";
						$outPRPMAN=$outPRPMAN."<td>".$dadosDep[$i]->nomepublicacao."</td>";
						$outPRPMAN=$outPRPMAN."<td>".$dadosDep[$i]->titulo."</td>";
						$outPRPMAN=$outPRPMAN."<td>".$dadosDep[$i]->autores."</td>";
						$outPRPMAN=$outPRPMAN."<td>".$dadosDep[$i]->volume."</td>";
						$outPRPMAN=$outPRPMAN."<td>".$dadosDep[$i]->issue."</td>";
						$outPRPMAN=$outPRPMAN."<td>".$dadosDep[$i]->primpagina."</td>";
						$outPRPMAN=$outPRPMAN."<td>".$dadosDep[$i]->ultpagina."</td>";
						$outPRPMAN=$outPRPMAN."<td>".$dadosDep[$i]->link."</td>";
						$outPRPMAN=$outPRPMAN."<td>".getEstadoPublicacaoMI($dadosDep[$i]->estado)."</td>";
						$outPRPMAN=$outPRPMAN."<td></td>";
						$outPRPMAN=$outPRPMAN."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-pubIntMan-PRP').text('" . $dadosDep[$i]->id . "');setObservacaoPubManIntPRP();return false;\" ></center></td>";
						$outPRPMAN=$outPRPMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('12');apagarPublicacao();return false;\" ></center></td>";    		
					$outPRPMAN=$outPRPMAN."</tr>";
					
					$outPRPMAN=$outPRPMAN."<tr>";
						$outPRPMAN=$outPRPMAN."<td style='background:#FFFF33; overflow:hidden;'><img src=\"../../images/arrow_return_down_right.png\" name='navOption'></td>";
						$outPRPMAN=$outPRPMAN."<td style='background:#FFFF33; overflow:hidden;'>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
						$outPRPMAN=$outPRPMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outPRPMAN_issn_" . $dadosDep[$i]->id . "'>".$acao['issn']."</td>";
						$outPRPMAN=$outPRPMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outPRPMAN_numepub_" . $dadosDep[$i]->id . "'>".$acao['nomepublicacao']."</td>";
						$outPRPMAN=$outPRPMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outPRPMAN_titulo_" . $dadosDep[$i]->id . "'>".$acao['titulo']."</td>";
						$outPRPMAN=$outPRPMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outPRPMAN_autores_" . $dadosDep[$i]->id . "'>".$acao['autores']."</td>";
						$outPRPMAN=$outPRPMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outPRPMAN_volume_" . $dadosDep[$i]->id . "'>".$acao['volume']."</td>";
						$outPRPMAN=$outPRPMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outPRPMAN_issue_" . $dadosDep[$i]->id . "'>".$acao['issue']."</td>";
						$outPRPMAN=$outPRPMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outPRPMAN_prim_" . $dadosDep[$i]->id . "'>".$acao['primpagina']."</td>";
						$outPRPMAN=$outPRPMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outPRPMAN_ult_" . $dadosDep[$i]->id . "'>".$acao['ultpagina']."</td>";
						$outPRPMAN=$outPRPMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outPRPMAN_link_" . $dadosDep[$i]->id . "'>".$acao['link']."</td>";
						$outPRPMAN=$outPRPMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outPRPMAN_estado_" . $dadosDep[$i]->id . "'>".getEstadoPublicacaoMI($acao['estado'])."</td>";
						$outPRPMAN=$outPRPMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubIntMan-PRP').text('" . $dadosDep[$i]->id . "');$('#tabela').text('12');\"></td>";
						$outPRPMAN=$outPRPMAN."<td></td>";
						$outPRPMAN=$outPRPMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"apagarAcao('" . $dadosDep[$i]->id . "',12);return false;\" ></center></td>";    		
					$outPRPMAN=$outPRPMAN."</tr>";
					
				} else {
					$outPRPMAN=$outPRPMAN."<tr>";
						$outPRPMAN=$outPRPMAN."<td>".$dadosDep[$i]->idinv."</td>";
						$outPRPMAN=$outPRPMAN."<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
						$outPRPMAN=$outPRPMAN."<td id='td_outPRPMAN_issn_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->issn."</td>";
						$outPRPMAN=$outPRPMAN."<td id='td_outPRPMAN_numepub_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->nomepublicacao."</td>";
						$outPRPMAN=$outPRPMAN."<td id='td_outPRPMAN_titulo_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->titulo."</td>";
						$outPRPMAN=$outPRPMAN."<td id='td_outPRPMAN_autores_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->autores."</td>";
						$outPRPMAN=$outPRPMAN."<td id='td_outPRPMAN_volume_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->volume."</td>";
						$outPRPMAN=$outPRPMAN."<td id='td_outPRPMAN_issue_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->issue."</td>";
						$outPRPMAN=$outPRPMAN."<td id='td_outPRPMAN_prim_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->primpagina."</td>";
						$outPRPMAN=$outPRPMAN."<td id='td_outPRPMAN_ult_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->ultpagina."</td>";
						$outPRPMAN=$outPRPMAN."<td id='td_outPRPMAN_link_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->link."</td>";
						$outPRPMAN=$outPRPMAN."<td id='td_outPRPMAN_estado_" . $dadosDep[$i]->id . "'>".getEstadoPublicacaoMI($dadosDep[$i]->estado)."</td>";
						$outPRPMAN=$outPRPMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubIntMan-PRP').text('" . $dadosDep[$i]->id . "');$('#tabela').text('12');\"></td>";
						$outPRPMAN=$outPRPMAN."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-pubIntMan-PRP').text('" . $dadosDep[$i]->id . "');setObservacaoPubManIntPRP();return false;\" ></center></td>";
						$outPRPMAN=$outPRPMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('12');apagarPublicacao();return false;\" ></center></td>";    		
					$outPRPMAN=$outPRPMAN."</tr>";
				}			
			}
			break;			
			case 'JA': { 
				$checkAcao = $db->checkAcaoExists($dadosDep[$i]->id, $_SESSION['login'], 12);		
				if ($checkAcao) {
					$acao = transformIntoAcaoPubArtObject($dadosDep[$i]->id, $dadosDep[$i]->idinv, $checkAcao);	
					
					$outJAMAN=$outJAMAN."<tr>";
						$outJAMAN=$outJAMAN."<td>".$dadosDep[$i]->idinv."</td>";
						$outJAMAN=$outJAMAN."<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
						$outJAMAN=$outJAMAN."<td>".$dadosDep[$i]->issn."</td>";	
						$outJAMAN=$outJAMAN."<td>".$dadosDep[$i]->nomepublicacao."</td>";
						$outJAMAN=$outJAMAN."<td>".$dadosDep[$i]->titulo."</td>";						
						$outJAMAN=$outJAMAN."<td>".$dadosDep[$i]->autores."</td>";
						$outJAMAN=$outJAMAN."<td>".$dadosDep[$i]->volume."</td>";
						$outJAMAN=$outJAMAN."<td>".$dadosDep[$i]->issue."</td>";	
						$outJAMAN=$outJAMAN."<td>".$dadosDep[$i]->primpagina."</td>";
						$outJAMAN=$outJAMAN."<td>".$dadosDep[$i]->ultpagina."</td>";
						$outJAMAN=$outJAMAN."<td>".$dadosDep[$i]->link."</td>";
						$outJAMAN=$outJAMAN."<td>".getEstadoPublicacaoMI($dadosDep[$i]->estado)."</td>";
						$outJAMAN=$outJAMAN."<td></td>";
						$outJAMAN=$outJAMAN."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-pubIntMan-Sum').text('" . $dadosDep[$i]->id . "');setObservacaoPubManIntSum();return false;\" ></center></td>";
						$outJAMAN=$outJAMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('12');apagarPublicacao();return false;\" ></center></td>";    		
					$outJAMAN=$outJAMAN."</tr>";
				   
					$outJAMAN=$outJAMAN."<tr>";
						$outJAMAN=$outJAMAN."<td style='background:#FFFF33; overflow:hidden;'><img src=\"../../images/arrow_return_down_right.png\" name='navOption'></td>";
						$outJAMAN=$outJAMAN."<td style='background:#FFFF33; overflow:hidden;'>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
						$outJAMAN=$outJAMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outJAMAN_issn_" . $dadosDep[$i]->id . "'>".$acao['issn']."</td>";	
						$outJAMAN=$outJAMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outJAMAN_nomepub_" . $dadosDep[$i]->id . "'>".$acao['nomepublicacao']."</td>";
						$outJAMAN=$outJAMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outJAMAN_titulo_" . $dadosDep[$i]->id . "'>".$acao['titulo']."</td>";						
						$outJAMAN=$outJAMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outJAMAN_autores_" . $dadosDep[$i]->id . "'>".$acao['autores']."</td>";
						$outJAMAN=$outJAMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outJAMAN_volume_" . $dadosDep[$i]->id . "'>".$acao['volume']."</td>";
						$outJAMAN=$outJAMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outJAMAN_issue_" . $dadosDep[$i]->id . "'>".$acao['issue']."</td>";	
						$outJAMAN=$outJAMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outJAMAN_prim_" . $dadosDep[$i]->id . "'>".$acao['primpagina']."</td>";
						$outJAMAN=$outJAMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outJAMAN_ult_" . $dadosDep[$i]->id . "'>".$acao['ultpagina']."</td>";
						$outJAMAN=$outJAMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outJAMAN_link_" . $dadosDep[$i]->id . "'>".$acao['link']."</td>";
						$outJAMAN=$outJAMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outJAMAN_estado_" . $dadosDep[$i]->id . "'>".getEstadoPublicacaoMI($acao['estado'])."</td>";
						$outJAMAN=$outJAMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubIntMan-Sum').text('" . $dadosDep[$i]->id . "');$('#tabela').text('12');\"></td>";
						$outJAMAN=$outJAMAN."<td></td>";
						$outJAMAN=$outJAMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"apagarAcao('" . $dadosDep[$i]->id . "',12);return false;\" ></center></td>";    		
				    $outJAMAN=$outJAMAN."</tr>";
				   
				} else {
					$outJAMAN=$outJAMAN."<tr>";
						$outJAMAN=$outJAMAN."<td>".$dadosDep[$i]->idinv."</td>";
						$outJAMAN=$outJAMAN."<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
						$outJAMAN=$outJAMAN."<td id='td_outJAMAN_issn_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->issn."</td>";	
						$outJAMAN=$outJAMAN."<td id='td_outJAMAN_nomepub_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->nomepublicacao."</td>";
						$outJAMAN=$outJAMAN."<td id='td_outJAMAN_titulo_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->titulo."</td>";						
						$outJAMAN=$outJAMAN."<td id='td_outJAMAN_autores_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->autores."</td>";
						$outJAMAN=$outJAMAN."<td id='td_outJAMAN_volume_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->volume."</td>";
						$outJAMAN=$outJAMAN."<td id='td_outJAMAN_issue_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->issue."</td>";	
						$outJAMAN=$outJAMAN."<td id='td_outJAMAN_prim_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->primpagina."</td>";
						$outJAMAN=$outJAMAN."<td id='td_outJAMAN_ult_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->ultpagina."</td>";
						$outJAMAN=$outJAMAN."<td id='td_outJAMAN_link_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->link."</td>";
						$outJAMAN=$outJAMAN."<td id='td_outJAMAN_estado_" . $dadosDep[$i]->id . "'>".getEstadoPublicacaoMI($dadosDep[$i]->estado)."</td>";
						$outJAMAN=$outJAMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubIntMan-Sum').text('" . $dadosDep[$i]->id . "');$('#tabela').text('12');\"></td>";
						$outJAMAN=$outJAMAN."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-pubIntMan-Sum').text('" . $dadosDep[$i]->id . "');setObservacaoPubManIntSum();return false;\" ></center></td>";
						$outJAMAN=$outJAMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('12');apagarPublicacao();return false;\" ></center></td>";    		
				   $outJAMAN=$outJAMAN."</tr>";
				}
			}
			break;			
			case 'CP': { 
				$checkAcao = $db->checkAcaoExists($dadosDep[$i]->id, $_SESSION['login'], 12);		
				if ($checkAcao) {
					$acao = transformIntoAcaoPubCPObject($dadosDep[$i]->id, $dadosDep[$i]->idinv, $checkAcao);	
					$outCPMAN=$outCPMAN."<tr>";
						$outCPMAN=$outCPMAN."<td>".$dadosDep[$i]->idinv."</td>";
						$outCPMAN=$outCPMAN."<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
						$outCPMAN=$outCPMAN."<td>".$dadosDep[$i]->isbn."</td>";
						$outCPMAN=$outCPMAN."<td>".$dadosDep[$i]->nomepublicacao."</td>";
						$outCPMAN=$outCPMAN."<td>".$dadosDep[$i]->editor."</td>";
						$outCPMAN=$outCPMAN."<td>".$dadosDep[$i]->titulo."</td>";
						$outCPMAN=$outCPMAN."<td>".$dadosDep[$i]->autores."</td>";
						$outCPMAN=$outCPMAN."<td>".$dadosDep[$i]->primpagina."</td>";
						$outCPMAN=$outCPMAN."<td>".$dadosDep[$i]->ultpagina."</td>";
						$outCPMAN=$outCPMAN."<td>".$dadosDep[$i]->link."</textarea></td>";
						$outCPMAN=$outCPMAN."<td>".getEstadoPublicacaoMI($dadosDep[$i]->estado)."</td>";
						$outCPMAN=$outCPMAN."<td></td>";
						$outCPMAN=$outCPMAN."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-pubIntMan-Con').text('" . $dadosDep[$i]->id . "');setObservacaoPubManIntCP();return false;\" ></center></td>";
						$outCPMAN=$outCPMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('12');apagarPublicacao();return false;\" ></center></td>";    		
					$outCPMAN=$outCPMAN."</tr>";
					
					$outCPMAN=$outCPMAN."<tr>";
						$outCPMAN=$outCPMAN."<td style='background:#FFFF33; overflow:hidden;'><img src=\"../../images/arrow_return_down_right.png\" name='navOption'></td>";
						$outCPMAN=$outCPMAN."<td style='background:#FFFF33; overflow:hidden;'>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
						$outCPMAN=$outCPMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outCPMAN_isbn_" . $dadosDep[$i]->id . "'>".$acao['isbn']."</td>";
						$outCPMAN=$outCPMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outCPMAN_nomepub_" . $dadosDep[$i]->id . "'>".$acao['nomepublicacao']."</td>";
						$outCPMAN=$outCPMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outCPMAN_editor_" . $dadosDep[$i]->id . "'>".$acao['editor']."</td>";
						$outCPMAN=$outCPMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outCPMAN_titulo_" . $dadosDep[$i]->id . "'>".$acao['titulo']."</td>";
						$outCPMAN=$outCPMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outCPMAN_autores_" . $dadosDep[$i]->id . "'>".$acao['autores']."</td>";
						$outCPMAN=$outCPMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outCPMAN_prim_" . $dadosDep[$i]->id . "'>".$acao['primpagina']."</td>";
						$outCPMAN=$outCPMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outCPMAN_ult_" . $dadosDep[$i]->id . "'>".$acao['ultpagina']."</td>";
						$outCPMAN=$outCPMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outCPMAN_link_" . $dadosDep[$i]->id . "'>".$acao['link']."</td>";
						$outCPMAN=$outCPMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outCPMAN_estado_" . $dadosDep[$i]->id . "'>".getEstadoPublicacaoMI($acao['estado'])."</td>";
						$outCPMAN=$outCPMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubIntMan-Con').text('" . $dadosDep[$i]->id . "');$('#tabela').text('12');\"></td>";
						$outCPMAN=$outCPMAN."<td></td>";
						$outCPMAN=$outCPMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"apagarAcao('" . $dadosDep[$i]->id . "',12);return false;\" ></center></td>";
					$outCPMAN=$outCPMAN."</tr>";
					
				} else {
					$outCPMAN=$outCPMAN."<tr>";
						$outCPMAN=$outCPMAN."<td>".$dadosDep[$i]->idinv."</td>";
						$outCPMAN=$outCPMAN."<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
						$outCPMAN=$outCPMAN."<td id='td_outCPMAN_isbn_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->isbn."</td>";
						$outCPMAN=$outCPMAN."<td id='td_outCPMAN_nomepub_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->nomepublicacao."</td>";
						$outCPMAN=$outCPMAN."<td id='td_outCPMAN_editor_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->editor."</td>";
						$outCPMAN=$outCPMAN."<td id='td_outCPMAN_titulo_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->titulo."</td>";
						$outCPMAN=$outCPMAN."<td id='td_outCPMAN_autores_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->autores."</td>";
						$outCPMAN=$outCPMAN."<td id='td_outCPMAN_prim_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->primpagina."</td>";
						$outCPMAN=$outCPMAN."<td id='td_outCPMAN_ult_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->ultpagina."</td>";
						$outCPMAN=$outCPMAN."<td id='td_outCPMAN_link_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->link."</textarea></td>";
						$outCPMAN=$outCPMAN."<td id='td_outCPMAN_estado_" . $dadosDep[$i]->id . "'>".getEstadoPublicacaoMI($dadosDep[$i]->estado)."</td>";
						$outCPMAN=$outCPMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubIntMan-Con').text('" . $dadosDep[$i]->id . "');$('#tabela').text('12');\"></td>";
						$outCPMAN=$outCPMAN."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-pubIntMan-Con').text('" . $dadosDep[$i]->id . "');setObservacaoPubManIntPRP();return false;\" ></center></td>";
						$outCPMAN=$outCPMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('12');apagarPublicacao();return false;\" ></center></td>";    		
					$outCPMAN=$outCPMAN."</tr>";
				}				
			}
			break;
			case 'OA': { 
				$checkAcao = $db->checkAcaoExists($dadosDep[$i]->id, $_SESSION['login'], 12);		
				if ($checkAcao) {
					$acao = transformIntoAcaoPubCPObject($dadosDep[$i]->id, $dadosDep[$i]->idinv, $checkAcao);	
					$outOAMAN=$outOAMAN."<tr>";
						$outOAMAN=$outOAMAN."<td >".$dadosDep[$i]->idinv."</td>";
						$outOAMAN=$outOAMAN."<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
						$outOAMAN=$outOAMAN."<td>".$dadosDep[$i]->isbn."</td>";
						$outOAMAN=$outOAMAN."<td>".$dadosDep[$i]->nomepublicacao."</td>";
						$outOAMAN=$outOAMAN."<td>".$dadosDep[$i]->editor."</td>";
						$outOAMAN=$outOAMAN."<td>".$dadosDep[$i]->titulo."</td>";
						$outOAMAN=$outOAMAN."<td>".$dadosDep[$i]->autores."</td>";
						$outOAMAN=$outOAMAN."<td>".$dadosDep[$i]->primpagina."</td>";
						$outOAMAN=$outOAMAN."<td>".$dadosDep[$i]->ultpagina."</td>";
						$outOAMAN=$outOAMAN."<td>".$dadosDep[$i]->link."</td>";
						$outOAMAN=$outOAMAN."<td>".getEstadoPublicacaoMI($dadosDep[$i]->estado)."</td>";
						$outOAMAN=$outOAMAN."<td></td>";
						$outOAMAN=$outOAMAN."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-pubIntMan-Oth').text('" . $dadosDep[$i]->id . "');setObservacaoPubManIntOth();return false;\" ></center></td>";
						$outOAMAN=$outOAMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('12');apagarPublicacao();return false;\" ></center></td>";    		
					$outOAMAN=$outOAMAN."</tr>";
					
					$outOAMAN=$outOAMAN."<tr>";
						$outOAMAN=$outOAMAN."<td style='background:#FFFF33; overflow:hidden;'><img src=\"../../images/arrow_return_down_right.png\" name='navOption'></td>";
						$outOAMAN=$outOAMAN."<td style='background:#FFFF33; overflow:hidden;'>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
						$outOAMAN=$outOAMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outOAMAN_isbn_" . $dadosDep[$i]->id . "'>".$acao['isbn']."</td>";
						$outOAMAN=$outOAMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outOAMAN_nomepub_" . $dadosDep[$i]->id . "'>".$acao['nomepublicacao']."</td>";
						$outOAMAN=$outOAMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outOAMAN_editor_" . $dadosDep[$i]->id . "'>".$acao['editor']."</td>";
						$outOAMAN=$outOAMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outOAMAN_titulo_" . $dadosDep[$i]->id . "'>".$acao['titulo']."</td>";
						$outOAMAN=$outOAMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outOAMAN_autores_" . $dadosDep[$i]->id . "'>".$acao['autores']."</td>";
						$outOAMAN=$outOAMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outOAMAN_prim_" . $dadosDep[$i]->id . "'>".$acao['primpagina']."</td>";
						$outOAMAN=$outOAMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outOAMAN_ult_" . $dadosDep[$i]->id . "'>".$acao['ultpagina']."</td>";
						$outOAMAN=$outOAMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outOAMAN_link_" . $dadosDep[$i]->id . "'>".$acao['link']."</td>";
						$outOAMAN=$outOAMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outOAMAN_estado_" . $dadosDep[$i]->id . "'>".getEstadoPublicacaoMI($acao['estado'])."</td>";
						$outOAMAN=$outOAMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubIntMan-Oth').text('" . $dadosDep[$i]->id . "');$('#tabela').text('12');\"></td>";
						$outOAMAN=$outOAMAN."<td></td>";
						$outOAMAN=$outOAMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"apagarAcao('" . $dadosDep[$i]->id . "',12);return false;\" ></center></td>";    		
					$outOAMAN=$outOAMAN."</tr>";
				} else {
					$outOAMAN=$outOAMAN."<tr>";
						$outOAMAN=$outOAMAN."<td >".$dadosDep[$i]->idinv."</td>";
						$outOAMAN=$outOAMAN."<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
						$outOAMAN=$outOAMAN."<td id='td_outOAMAN_isbn_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->isbn."</td>";
						$outOAMAN=$outOAMAN."<td id='td_outOAMAN_nomepub_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->nomepublicacao."</td>";
						$outOAMAN=$outOAMAN."<td id='td_outOAMAN_editor_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->editor."</td>";
						$outOAMAN=$outOAMAN."<td id='td_outOAMAN_titulo_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->titulo."</td>";
						$outOAMAN=$outOAMAN."<td id='td_outOAMAN_autores_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->autores."</td>";
						$outOAMAN=$outOAMAN."<td id='td_outOAMAN_prim_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->primpagina."</td>";
						$outOAMAN=$outOAMAN."<td id='td_outOAMAN_ult_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->ultpagina."</td>";
						$outOAMAN=$outOAMAN."<td id='td_outOAMAN_link_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->link."</td>";
						$outOAMAN=$outOAMAN."<td id='td_outOAMAN_estado_" . $dadosDep[$i]->id . "'>".getEstadoPublicacaoMI($dadosDep[$i]->estado)."</td>";
						$outOAMAN=$outOAMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubIntMan-Oth').text('" . $dadosDep[$i]->id . "');$('#tabela').text('12');\"></td>";
						$outOAMAN=$outOAMAN."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-pubIntMan-Oth').text('" . $dadosDep[$i]->id . "');setObservacaoPubManIntOth();return false;\" ></center></td>";
						$outOAMAN=$outOAMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('12');apagarPublicacao();return false;\" ></center></td>";    		
					$outOAMAN=$outOAMAN."</tr>";
				}				
			}			
			break;			
			case 'L': { 
				$checkAcao = $db->checkAcaoExists($dadosDep[$i]->id, $_SESSION['login'], 12);		
				if ($checkAcao) {
					$acao = transformIntoAcaoPubLivObject($dadosDep[$i]->id, $dadosDep[$i]->idinv, $checkAcao);	
					$outLMAN=$outLMAN."<tr>";
						$outLMAN=$outLMAN."<td>".$dadosDep[$i]->idinv."</td>";
						$outLMAN=$outLMAN."<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
						$outLMAN=$outLMAN."<td>".$dadosDep[$i]->isbn."</td>";
						$outLMAN=$outLMAN."<td>".$dadosDep[$i]->titulo."</td>";
						$outLMAN=$outLMAN."<td>".$dadosDep[$i]->autores."</td>";
						$outLMAN=$outLMAN."<td>".$dadosDep[$i]->editor."</td>";
						$outLMAN=$outLMAN."<td>".$dadosDep[$i]->editora."</td>";
						$outLMAN=$outLMAN."<td>".$dadosDep[$i]->link."</td>";
						$outLMAN=$outLMAN."<td>".getEstadoPublicacaoMI($dadosDep[$i]->estado)."</td>";
						$outLMAN=$outLMAN."<td></td>";
						$outLMAN=$outLMAN."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-pubIntMan-Liv').text('" . $dadosDep[$i]->id . "');setObservacaoPubManIntLiv();return false;\" ></center></td>";
						$outLMAN=$outLMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('12');apagarPublicacao();return false;\" ></center></td>";    		
					$outLMAN=$outLMAN."</tr>";
					
					$outLMAN=$outLMAN."<tr>";
						$outLMAN=$outLMAN."<td style='background:#FFFF33; overflow:hidden;'><img src=\"../../images/arrow_return_down_right.png\" name='navOption'></td>";
						$outLMAN=$outLMAN."<td style='background:#FFFF33; overflow:hidden;'>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
						$outLMAN=$outLMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outLMAN_isbn_" . $dadosDep[$i]->id . "'>".$acao['isbn']."</td>";
						$outLMAN=$outLMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outLMAN_titulo_" . $dadosDep[$i]->id . "'>".$acao['titulo']."</td>";
						$outLMAN=$outLMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outLMAN_autores_" . $dadosDep[$i]->id . "'>".$acao['autores']."</td>";
						$outLMAN=$outLMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outLMAN_editor_" . $dadosDep[$i]->id . "'>".$acao['editor']."</td>";
						$outLMAN=$outLMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outLMAN_editora_" . $dadosDep[$i]->id . "'>".$acao['editora']."</td>";
						$outLMAN=$outLMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outLMAN_link_" . $dadosDep[$i]->id . "'>".$acao['link']."</td>";
						$outLMAN=$outLMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outLMAN_estado_" . $dadosDep[$i]->id . "'>".getEstadoPublicacaoMI($acao['estado'])."</td>";
						$outLMAN=$outLMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubIntMan-Liv').text('" . $dadosDep[$i]->id . "');$('#tabela').text('12');\"></td>";
						$outLMAN=$outLMAN."<td></td>";
						$outLMAN=$outLMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"apagarAcao('" . $dadosDep[$i]->id . "',12);return false;\" ></center></td>";    		
					$outLMAN=$outLMAN."</tr>";
				} else { 
					$outLMAN=$outLMAN."<tr>";
						$outLMAN=$outLMAN."<td>".$dadosDep[$i]->idinv."</td>";
						$outLMAN=$outLMAN."<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
						$outLMAN=$outLMAN."<td id='td_outLMAN_isbn_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->isbn."</td>";
						$outLMAN=$outLMAN."<td id='td_outLMAN_titulo_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->titulo."</td>";
						$outLMAN=$outLMAN."<td id='td_outLMAN_autores_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->autores."</td>";
						$outLMAN=$outLMAN."<td id='td_outLMAN_editor_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->editor."</td>";
						$outLMAN=$outLMAN."<td id='td_outLMAN_editora_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->editora."</td>";
						$outLMAN=$outLMAN."<td id='td_outLMAN_link_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->link."</td>";
						$outLMAN=$outLMAN."<td id='td_outLMAN_estado_" . $dadosDep[$i]->id . "'>".getEstadoPublicacaoMI($dadosDep[$i]->estado)."</td>";
						$outLMAN=$outLMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubIntMan-Liv').text('" . $dadosDep[$i]->id . "');$('#tabela').text('12');\"></td>";
						$outLMAN=$outLMAN."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-pubIntMan-Liv').text('" . $dadosDep[$i]->id . "');setObservacaoPubManIntLiv();return false;\" ></center></td>";
						$outLMAN=$outLMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('12');apagarPublicacao();return false;\" ></center></td>";    		
					$outLMAN=$outLMAN."</tr>";
				}
			}
			break;
			case 'CL': { 
				$checkAcao = $db->checkAcaoExists($dadosDep[$i]->id, $_SESSION['login'], 12);		
				if ($checkAcao) {
					$acao = transformIntoAcaoPubCLivObject($dadosDep[$i]->id, $dadosDep[$i]->idinv, $checkAcao);	
					
					$outCLMAN=$outCLMAN."<tr>";
						$outCLMAN=$outCLMAN."<td>".$dadosDep[$i]->idinv."</td>";
						$outCLMAN=$outCLMAN."<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
						$outCLMAN=$outCLMAN."<td>".$dadosDep[$i]->isbn."</td>";
						$outCLMAN=$outCLMAN."<td>".$dadosDep[$i]->nomepublicacao."</td>";
						$outCLMAN=$outCLMAN."<td>".$dadosDep[$i]->editora."</td>";
						$outCLMAN=$outCLMAN."<td>".$dadosDep[$i]->editor."</td>";						
						$outCLMAN=$outCLMAN."<td>".$dadosDep[$i]->titulo."</td>";
						$outCLMAN=$outCLMAN."<td>".$dadosDep[$i]->autores."</td>";
						$outCLMAN=$outCLMAN."<td>".$dadosDep[$i]->primpagina."</td>";
						$outCLMAN=$outCLMAN."<td>".$dadosDep[$i]->ultpagina."</td>";
						$outCLMAN=$outCLMAN."<td>".$dadosDep[$i]->link."</td>";
						$outCLMAN=$outCLMAN."<td>".getEstadoPublicacaoMI($dadosDep[$i]->estado)."</td>";
						$outCLMAN=$outCLMAN."<td></td>";						
						$outCLMAN=$outCLMAN."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-pubIntMan-CLiv').text('" . $dadosDep[$i]->id . "');setObservacaoPubManIntCLiv();return false;\" ></center></td>";
						$outCLMAN=$outCLMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('12');apagarPublicacao();return false;\" ></center></td>";    		
					$outCLMAN=$outCLMAN."</tr>";
					
					$outCLMAN=$outCLMAN."<tr>";
						$outCLMAN=$outCLMAN."<td style='background:#FFFF33; overflow:hidden;'><img src=\"../../images/arrow_return_down_right.png\" name='navOption'></td>";
						$outCLMAN=$outCLMAN."<td style='background:#FFFF33; overflow:hidden;'>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
						$outCLMAN=$outCLMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outCLMAN_isbn_" . $dadosDep[$i]->id . "'>".$acao['isbn']."</td>";
						$outCLMAN=$outCLMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outCLMAN_nomepub_" . $dadosDep[$i]->id . "'>".$acao['nomepublicacao']."</td>";
						$outCLMAN=$outCLMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outCLMAN_editora_" . $dadosDep[$i]->id . "'>".$acao['editora']."</td>";
						$outCLMAN=$outCLMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outCLMAN_editor_" . $dadosDep[$i]->id . "'>".$acao['editor']."</td>";						
						$outCLMAN=$outCLMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outCLMAN_titulo_" . $dadosDep[$i]->id . "'>".$acao['titulo']."</td>";
						$outCLMAN=$outCLMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outCLMAN_autores_" . $dadosDep[$i]->id . "'>".$acao['autores']."</td>";
						$outCLMAN=$outCLMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outCLMAN_prim_" . $dadosDep[$i]->id . "'>".$acao['primpagina']."</td>";
						$outCLMAN=$outCLMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outCLMAN_ult_" . $dadosDep[$i]->id . "'>".$acao['ultpagina']."</td>";
						$outCLMAN=$outCLMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outCLMAN_link_" . $dadosDep[$i]->id . "'>".$acao['link']."</td>";
						$outCLMAN=$outCLMAN."<td style='background:#FFFF33; overflow:hidden;' id='td_outCLMAN_estado_" . $dadosDep[$i]->id . "'>".getEstadoPublicacaoMI($acao['estado'])."</td>";						   	
						$outCLMAN=$outCLMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubIntMan-CLiv').text('" . $dadosDep[$i]->id . "');$('#tabela').text('12');\"></td>";
						$outCLMAN=$outCLMAN."<td></td>";
						$outCLMAN=$outCLMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"apagarAcao('" . $dadosDep[$i]->id . "',12);return false;\" ></center></td>";    		
					$outCLMAN=$outCLMAN."</tr>";
				} else {
					$outCLMAN=$outCLMAN."<tr>";
						$outCLMAN=$outCLMAN."<td>".$dadosDep[$i]->idinv."</td>";
						$outCLMAN=$outCLMAN."<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
						$outCLMAN=$outCLMAN."<td id='td_outCLMAN_isbn_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->isbn."</td>";
						$outCLMAN=$outCLMAN."<td id='td_outCLMAN_nomepub_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->nomepublicacao."</td>";
						$outCLMAN=$outCLMAN."<td id='td_outCLMAN_editora_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->editora."</td>";
						$outCLMAN=$outCLMAN."<td id='td_outCLMAN_editor_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->editor."</td>";						
						$outCLMAN=$outCLMAN."<td id='td_outCLMAN_titulo_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->titulo."</td>";
						$outCLMAN=$outCLMAN."<td id='td_outCLMAN_autores_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->autores."</td>";
						$outCLMAN=$outCLMAN."<td id='td_outCLMAN_prim_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->primpagina."</td>";
						$outCLMAN=$outCLMAN."<td id='td_outCLMAN_ult_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->ultpagina."</td>";
						$outCLMAN=$outCLMAN."<td id='td_outCLMAN_link_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->link."</td>";
						$outCLMAN=$outCLMAN."<td id='td_outCLMAN_estado_" . $dadosDep[$i]->id . "'>".getEstadoPublicacaoMI($dadosDep[$i]->estado)."</td>";						   	
						$outCLMAN=$outCLMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubIntMan-CLiv').text('" . $dadosDep[$i]->id . "');$('#tabela').text('12');\"></td>";
						$outCLMAN=$outCLMAN."<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-pubIntMan-CLiv').text('" . $dadosDep[$i]->id . "');setObservacaoPubManIntCLiv();return false;\" ></center></td>";
						$outCLMAN=$outCLMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $dadosDep[$i]->id . "');$('#tabela').text('12');apagarPublicacao();return false;\" ></center></td>";    		
					$outCLMAN=$outCLMAN."</tr>";
				}
			}
			break;	
		}
	}
	
	echo "<h3>Publicações Manuais Internacionais</h3>\n";
	echo "<div id='pubManualInternacional'>";
		echo"<div id='pubManualInt-Art'><table id='pubManInt-Art' class='box-table-b'><caption>ARTIGOS</caption>";echo $tcolsJ;echo "<th colspan='3'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPub(\"J\",\"INT\");return false;'></center></th></tr></thead><tbody>";echo $outJMAN;echo "</table><p id='chave-pubIntMan-Art' hidden></p></div><br>";
		echo"<div id='pubManualInt-Rev'><table id='pubManInt-Rev' class='box-table-b'><caption >PEER REVIEW PROCEEDINGS</caption>";echo $tcolsJ;echo "<th colspan='3'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPub(\"PRP\",\"INT\");return false;'></center></th></tr></thead><tbody>";echo $outPRPMAN;echo "</tbody></table><p id='chave-pubIntMan-PRP' hidden></p></div><br>";
		echo"<div id='pubManualInt-Sum'><table id='pubManInt-Sum' class='box-table-b'><caption >SUMÁRIOS<p><i>JOURNAL ABSTRACTS</i></p></caption>";echo $tcolsJ;echo "<th colspan='3'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPub(\"S\",\"INT\");return false;'></center></th></tr></thead><tbody>";echo $outJAMAN;echo "</tbody></table><p id='chave-pubIntMan-Sum' hidden></p></div><br>";		
		echo"<div id='pubManualInt-Liv'><table id='pubManInt-Liv' class='box-table-b'><caption >LIVROS<p><i>BOOKS</i></p></caption>";echo $tcolsLIV;echo "<th colspan='3'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPub(\"L\",\"INT\");return false;'></center></th></tr></thead><tbody>";echo $outLMAN;echo "</tbody></table><p id='chave-pubIntMan-Liv' hidden></p></div><br>";
		echo"<div id='pubManualInt-CLiv'><table id='pubManInt-CLiv' class='box-table-b'><caption >CAPITULOS DE LIVROS<p><i>BOOK CHAPTERS</i></p></caption>";echo $tcolsCLIV;echo "<th colspan='3'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPub(\"CL\",\"INT\");return false;'></center></th></tr></thead><tbody>";echo $outCLMAN;echo "</tbody></table><p id='chave-pubIntMan-CLiv' hidden></p></div><br>";
		echo"<div id='pubManualInt-Con'><table id='pubManInt-Con' class='box-table-b'><caption >CONFERENCE PROCEEDINGS</caption>";echo $tcolsCONF;echo "<th colspan='3'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPub(\"CP\",\"INT\");return false;'></center></th></tr></thead><tbody>";echo $outCPMAN;echo "</tbody></table><p id='chave-pubIntMan-Con' hidden></p></div><br>";
		echo"<div id='pubManualInt-Oth'><table id='pubManInt-Oth' class='box-table-b'><caption >OUTROS SUMÁRIOS<br>OTHER ABSTRACTS</caption>";echo $tcolsCONF;echo "<th colspan='3'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPub(\"OA\",\"INT\");return false;'></center></th></tr></thead><tbody>";echo $outOAMAN;echo "</tbody></table><p id='chave-pubIntMan-Oth' hidden></p></div><br>";
	echo "</div>";
}

function getEstadoPublicacaoMI($i) {
	$db = new Database();
	$lValues =$db->getLookupValues("lista_estadoPublicacoes");
	$texto="";
	while ($row = mysql_fetch_assoc($lValues)) {
		if($i==$row["ID"])
			$texto=$row["DESCRICAO"];
	}
	$db->disconnect();
	return $texto;
}

function transformIntoAcaoPubArtObject($id,$idinv, $query) {	
	$acao = array();
	
	$cena = explode("ISSN='",$query);
				
	$cena1 = explode("', AUTORES='",$cena[1]);
	$acao['issn'] = $cena1[0];
	
	$cena2 = explode("', NOMEPUBLICACAO='",$cena1[1]);
	$acao['autores'] = $cena2[0];
	
	$cena3 = explode("', VOLUME='",$cena2[1]);
	$acao['nomepublicacao']= $cena3[0];
	
	$cena4 = explode("', ISSUE='",$cena3[1]);
	$acao['volume']= $cena4[0];	
	
	$cena5 = explode("', PRIMPAGINA='",$cena4[1]);
	$acao['issue']= $cena5[0];	
	
	$cena6 = explode("', ULTPAGINA='",$cena5[1]);
	$acao['primpagina']= $cena6[0];	
	
	$cena7 = explode("', TITULO='",$cena6[1]);
	$acao['ultpagina']= $cena7[0];	
	
	$cena8 = explode("', LINK='",$cena7[1]);
	$acao['titulo']= $cena8[0];	
	
	$cena9 = explode("', ESTADO=",$cena8[1]);
	$acao['link']= $cena9[0];	
	
	$cena10 = explode(" where",$cena9[1]);
	$acao['estado']= $cena10[0];	
				
	return $acao;
}

function transformIntoAcaoPubCPObject($id,$idinv, $query) {	
	$acao = array();
	
	$cena = explode("ISBN='",$query);
				
	$cena1 = explode("', NOMEPUBLICACAO='",$cena[1]);
	$acao['isbn'] = $cena1[0];
	
	$cena2 = explode("', EDITOR='",$cena1[1]);
	$acao['nomepublicacao'] = $cena2[0];
	
	$cena3 = explode("', TITULO='",$cena2[1]);
	$acao['editor']= $cena3[0];
	
	$cena4 = explode("', AUTORES='",$cena3[1]);
	$acao['titulo']= $cena4[0];	
	
	$cena5 = explode("', PRIMPAGINA='",$cena4[1]);
	$acao['autores']= $cena5[0];	
	
	$cena6 = explode("', ULTPAGINA='",$cena5[1]);
	$acao['primpagina']= $cena6[0];	
	
	$cena7 = explode("', LINK='",$cena6[1]);
	$acao['ultpagina']= $cena7[0];	
	
	$cena8 = explode("', ESTADO=",$cena7[1]);
	$acao['link']= $cena8[0];	
	
	$cena9 = explode(" where",$cena8[1]);
	$acao['estado']= $cena9[0];	
				
	return $acao;
}

function transformIntoAcaoPubLivObject($id, $idinv, $query) {
	$acao = array();
	
	$cena = explode("ISBN='",$query);
				
	$cena1 = explode("', AUTORES='",$cena[1]);
	$acao['isbn'] = $cena1[0];
	
	$cena2 = explode("', EDITOR='",$cena1[1]);
	$acao['autores'] = $cena2[0];
	
	$cena3 = explode("', EDITORA='",$cena2[1]);
	$acao['editor']= $cena3[0];
	
	$cena4 = explode("', TITULO='",$cena3[1]);
	$acao['editora']= $cena4[0];	
	
	$cena5 = explode("', LINK='",$cena4[1]);
	$acao['titulo']= $cena5[0];	
	
	$cena6 = explode("', ESTADO=",$cena5[1]);
	$acao['link']= $cena6[0];	
	
	$cena7 = explode(" where",$cena6[1]);
	$acao['estado']= $cena7[0];	
	
	return $acao;	
}

function transformIntoAcaoPubCLivObject($id, $idinv, $query) {
	$acao = array();
	
	$cena = explode("ISBN='",$query);
				
	$cena1 = explode("', NOMEPUBLICACAO='",$cena[1]);
	$acao['isbn'] = $cena1[0];
	
	$cena2 = explode("', EDITORA='",$cena1[1]);
	$acao['nomepublicacao'] = $cena2[0];
	
	$cena3 = explode("', EDITOR='",$cena2[1]);
	$acao['editora']= $cena3[0];
	
	$cena4 = explode("', TITULO='",$cena3[1]);
	$acao['editor']= $cena4[0];	
	
	$cena5 = explode("', AUTORES='",$cena4[1]);
	$acao['titulo']= $cena5[0];	
	
	$cena6 = explode("', PRIMPAGINA='",$cena5[1]);
	$acao['autores']= $cena6[0];	
	
	$cena7 = explode("', ULTPAGINA='",$cena6[1]);
	$acao['primpagina']= $cena7[0];	
	
	$cena8 = explode("', LINK='",$cena7[1]);
	$acao['ultpagina']= $cena8[0];	
	
	$cena9 = explode("', ESTADO=",$cena8[1]);
	$acao['link']= $cena9[0];	
	
	$cena10 = explode(" where",$cena9[1]);
	$acao['estado']= $cena10[0];
				
	return $acao;	
}

?>