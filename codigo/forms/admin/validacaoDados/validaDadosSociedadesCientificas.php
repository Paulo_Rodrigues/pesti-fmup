<?php

	function showEditSociedadesCientificas($dadosDep) {		
		$db = new Database();
		echo "<h3>Direção de Organizações/ Sociedades Científicas</h3>\n";
		echo "<div id='sc'>";

		echo "<table id='socCient' class='box-table-b'>
				<thead>
					<tr>
						<th>IDINV</th>						
						<th>Nome Investigador</th>
						<th>Nome</th>
						<th>Tipo</th>
						<th>Data Início</th>
						<th>Data Fim</th>
						<th>Cargo</th>
						<th>Link</th>
						<th colspan='3'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewSocCientifica();return false;'></center></th>
					</tr>
				</thead>
				<tbody>";
				
	foreach ($dadosDep as $i => $value){
		$checkAcao = $db->checkAcaoExists($dadosDep[$i]->id, $_SESSION['login'], 19);		
		if ($checkAcao) {
			$acao = transformIntoAcaoSCObject($dadosDep[$i]->id, $dadosDep[$i]->idinv, $checkAcao);	
			echo "<tr>";
				echo "<td>".$dadosDep[$i]->idinv."</td>";
				echo "<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
				echo "<td>".$dadosDep[$i]->nome."</td>";
				echo "<td>";getTipoSC($dadosDep[$i]->tipo);	echo "</td>";
				echo "<td>".$dadosDep[$i]->datainicio."</td>";	
				echo "<td>".$dadosDep[$i]->datafim."</td>";	
				echo "<td>".$dadosDep[$i]->cargo."</td>";
				echo "<td><a href='".$dadosDep[$i]->link."' target='_blank'>".$dadosDep[$i]->link."</a></td>";
				echo "<td></td>";
				echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-sc').text('" . $dadosDep[$i]->id . "');setObservacaoSC();return false;\" ></center></td>";				
				echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" .$dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-sc').text('" .$dadosDep[$i]->id . "');apagarSC();return false;\" ></center></td>";
			echo "</tr>";	   
				
			echo "<tr>";
				echo "<td style='background:#FFFF33; overflow:hidden;'><img src=\"../../images/arrow_return_down_right.png\" name='navOption'></td>";
				echo "<td style='background:#FFFF33; overflow:hidden;'>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";	
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_sc_nome_" .$dadosDep[$i]->id . "'>".$acao['nome']."</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_sc_tipo_" .$dadosDep[$i]->id . "'>";getTipoSC($acao['tipo']);echo "</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_sc_datainicio_" .$dadosDep[$i]->id . "'>".$acao['datainicio']."</td>";	
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_sc_datafim_" .$dadosDep[$i]->id . "'>".$acao['datafim']."</td>";	
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_sc_cargo_" .$dadosDep[$i]->id . "'>".$acao['cargo']."</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_sc_link_" .$dadosDep[$i]->id . "'><a href='".$acao['link']."' target='_blank'>".$acao['link']."</a></td>";
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-sc').text('" .$dadosDep[$i]->id . "');\"></td>";
				echo "<td></td>";
				echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" .$dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"apagarAcao('" . $dadosDep[$i]->id . "',19);return false;\" ></center></td>";
			echo "</tr>";
			
		} else {
			echo "<tr>";
				echo "<td>".$dadosDep[$i]->idinv."</td>";
				echo "<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
				echo "<td id='td_sc_nome_" .$dadosDep[$i]->id . "'>".$dadosDep[$i]->nome."</td>";
				echo "<td id='td_sc_tipo_" .$dadosDep[$i]->id . "'>";getTipoSC($dadosDep[$i]->tipo);echo "</td>";
				echo "<td id='td_sc_datainicio_" .$dadosDep[$i]->id . "'>".$dadosDep[$i]->datainicio."</td>";	
				echo "<td id='td_sc_datafim_" .$dadosDep[$i]->id . "'>".$dadosDep[$i]->datafim."</td>";	
				echo "<td id='td_sc_cargo_" .$dadosDep[$i]->id . "'>".$dadosDep[$i]->cargo."</td>";
				echo "<td id='td_sc_link_" .$dadosDep[$i]->id . "'><a href='".$dadosDep[$i]->link."' target='_blank'>".$dadosDep[$i]->link."</a></td>";
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-sc').text('" .$dadosDep[$i]->id . "');\"></td>";
				echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-sc').text('" . $dadosDep[$i]->id . "');setObservacaoSC();return false;\" ></center></td>";				
				echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" .$dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-sc').text('" .$dadosDep[$i]->id . "');apagarSC();return false;\" ></center></td>";
			echo "</tr>";	   
		}		
	}
	
		echo "</tbody></table>
		<p id='chave-sc' hidden></p>";
		echo "</div>";
}
	
	function transformIntoAcaoSCObject($id, $idinv, $query) {
		$acao = array();	
				
		$cena = explode("TIPO='",$query);
					
		$cena1 = explode("', DATAINICIO='",$cena[1]);
		$acao['tipo'] = $cena1[0];
		
		$cena2 = explode("', DATAFIM='",$cena1[1]);
		$acao['datainicio']= $cena2[0];
		
		$cena3 = explode("', CARGO='",$cena2[1]);
		$acao['datafim'] = $cena3[0];	
		
		$cena4 = explode("', LINK='",$cena3[1]);
		$acao['cargo'] = $cena4[0];	
		
		$cena5 = explode("', NOME='",$cena4[1]);
		$acao['link'] = $cena5[0];	
								
		$cena6 = explode("' where",$cena5[1]);
		$acao['nome'] = $cena6[0];
		
		return $acao;
	}

	function getTipoSC($i) {	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_tiposociedadecientifica");
		
		while ($row = mysql_fetch_assoc($lValues)) {	
			if($i==$row["ID"])
				echo $row["DESCRICAO"];
		}

		$db->disconnect();				
	}	
?>