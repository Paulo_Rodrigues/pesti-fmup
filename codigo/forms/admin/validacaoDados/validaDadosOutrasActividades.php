<?php

function showEditOutrasActividades($dadosDep) {
	$db = new Database();	
		
	echo "<h3>Outras Atividades</h3>\n
			<div id='outact'>";
				
	echo "<table id='outrasAct' class='box-table-b'>
		  <thead>
			<tr>
				<th>IDINV</th>
				<th>Nome</th>
				<th>Monografias de apoio às aulas ou a trabalho clínico</th>
				<th>Livros Didáticos</th>
				<th>Livros de Divulgação</th>
				<th>Vídeos, jogos, aplicações</th>
				<th>Páginas Internet</th>
				<th colspan='3'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewOutAct();return false;'></center></th>
			</tr>
		  </thead>
		  <tbody>";
	
	foreach ($dadosDep as $i => $value) {	
		if($dadosDep[$i]->texapoio!=0 || 
			$dadosDep[$i]->livdidaticos!=0 || 
			$dadosDep[$i]->livdivulgacao!=0 ||
			$dadosDep[$i]->app!=0 ||
			$dadosDep[$i]->webp!=0){
			
			$checkAcao = $db->checkAcaoExists($dadosDep[$i]->id, $_SESSION['login'], 24);
			if ($checkAcao) {
			
				$acao = transformIntoOutActObject($dadosDep[$i]->id, $dadosDep[$i]->idinv, $checkAcao);
				echo "<tr>";
					echo "<td>".$dadosDep[$i]->idinv."</td>";
					echo "<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
					echo "<td>".$dadosDep[$i]->texapoio."</td>";
					echo "<td>".$dadosDep[$i]->livdidaticos."</td>";
					echo "<td>".$dadosDep[$i]->livdivulgacao."</td>";
					echo "<td>".$dadosDep[$i]->app."</td>";
					echo "<td>".$dadosDep[$i]->webp."</td>";
					echo "<td></td>";
					echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-outact').text('" . $dadosDep[$i]->id . "');setObservacaoOutAct();return false;\" ></center></td>";
					echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-outact').text('" . $dadosDep[$i]->id . "');apagarOutAct();return false;\" ></center></td>";   
				echo "</tr>";
				
				echo "<tr>";
					echo "<td style='background:#FFFF33; overflow:hidden;'><img src=\"../../images/arrow_return_down_right.png\" name='navOption'></td>";
					echo "<td style='background:#FFFF33; overflow:hidden;'>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_outact_texapoio_" . $dadosDep[$i]->id . "'>".$acao['texapoio']."</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_outact_livdidaticos_" . $dadosDep[$i]->id . "'>".$acao['livdidaticos']."</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_outact_livdivulgacao_" . $dadosDep[$i]->id . "'>".$acao['livdivulgacao']."</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_outact_app_" . $dadosDep[$i]->id . "'>".$acao['app']."</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_outact_webp_" . $dadosDep[$i]->id . "'>".$acao['webp']."</td>";
					echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-outact').text('" . $dadosDep[$i]->id . "');\"></td>";					
					echo "<td></td>";
					echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"apagarAcao('" . $dadosDep[$i]->id . "',24);return false;\" ></center></td>";   
				echo "</tr>";
			} else { 
			
				echo "<tr>";
					echo "<td>".$dadosDep[$i]->idinv."</td>";
					echo "<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
					echo "<td id='td_outact_texapoio_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->texapoio."</td>";
					echo "<td id='td_outact_livdidaticos_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->livdidaticos."</td>";
					echo "<td id='td_outact_livdivulgacao_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->livdivulgacao."</td>";
					echo "<td id='td_outact_app_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->app."</td>";
					echo "<td id='td_outact_webp_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->webp."</td>";
					echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-outact').text('" . $dadosDep[$i]->id . "');\"></td>";
					echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-outact').text('" . $dadosDep[$i]->id . "');setObservacaoOutAct();return false;\" ></center></td>";
					echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-outact').text('" . $dadosDep[$i]->id . "');apagarOutAct();return false;\" ></center></td>";   
				echo "</tr>";
			}
		}
	}
	
	echo "</tbody>
    </table>
    <p id='chave-outact' hidden></p>
</div>";
}

function transformIntoOutActObject ($id, $idinv, $query) {
	$acao = array();
	
	$cena = explode("TEXAPOIO=",$query);
				
	$cena1 = explode(", LIVDIDATICOS=",$cena[1]);
	$acao['texapoio'] = $cena1[0];
	
	$cena2 = explode(", LIVDIVULGACAO=",$cena1[1]);
	$acao['livdidaticos'] = $cena2[0];
	
	$cena3 = explode(", APP=",$cena2[1]);
	$acao['livdivulgacao']= $cena3[0];
	
	$cena4 = explode(", WEBP=",$cena3[1]);
	$acao['app']= $cena4[0];
	
	$cena5 = explode(" where",$cena4[1]);
	$acao['webp'] = $cena5[0];
			
	return $acao;
}
	
?>