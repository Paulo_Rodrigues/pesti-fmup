<?php
function showEditExperienciaProfissional($dadosDep)
{
	$db = new Database();
	echo "<h3>Caracterização Docentes</h3>\n";
	echo "<div id='caracterizacaoDocentes'>";
	echo "<table id='caracDoc' class='box-table-b'>
			<thead>
				<tr>
					<th>IDINV</th>
					<th>Nome</th>
					<th>Tipo</th>
					<th>Percentagem</th>
					<th colspan='3'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewCaracDoc();return false;'></center></th>
				</tr>
			</thead>
			<tbody>";

	foreach ($dadosDep as $i => $value){ 
		if($dadosDep[$i]->docentecat!=0 || $dadosDep[$i]->docenteper!=0){
			$checkAcao = $db->checkAcaoExists($dadosDep[$i]->id, $_SESSION['login'], 4);		
			if ($checkAcao) {
				$acao = transformIntoAcaoCarDocObject($dadosDep[$i]->id, $dadosDep[$i]->idinv, $checkAcao);
				echo "<tr>";
					echo "<td>".$dadosDep[$i]->idinv."</td>";
					echo "<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
					echo "<td>".getCat("docente",$dadosDep[$i]->docentecat)."</td>";
					echo "<td>".$dadosDep[$i]->docenteper."</td>";
					echo "<td></td>";
					echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-exp-pro-doc').text('" . $dadosDep[$i]->id . "');setObservacaoExpProDoc();return false;\" ></center></td>";
					echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-exp-pro-doc').text('" . $dadosDep[$i]->id . "');apagarCaracterizacaoDocente();return false;\" ></center></td>";
				echo "</tr>";
				
				echo "<tr>";
					echo "<td style='background:#FFFF33; overflow:hidden;'><img src=\"../../images/arrow_return_down_right.png\" name='navOption'></td>";
					echo "<td style='background:#FFFF33; overflow:hidden;'>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_exp_pro_doc_cat_" . $dadosDep[$i]->id . "'>".getCat("docente",$acao['docentecat'])."</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_exp_pro_doc_per_" . $dadosDep[$i]->id . "'>".$acao['docenteper']."</td>";
					echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-exp-pro-doc').text('" . $dadosDep[$i]->id. "');\"></td>";
					echo "<td></td>";
					echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"apagarAcao('" . $dadosDep[$i]->id . "',4);return false;\" ></center></td>";
				echo "</tr>";
			} else {
				echo "<tr>";
					echo "<td>".$dadosDep[$i]->idinv."</td>";
					echo "<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
					echo "<td id='td_exp_pro_doc_cat_" . $dadosDep[$i]->id . "'>".getCat("docente",$dadosDep[$i]->docentecat)."</td>";
					echo "<td id='td_exp_pro_doc_per_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->docenteper."</td>";
					echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-exp-pro-doc').text('" . $dadosDep[$i]->id. "');\"></td>";	
					echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-exp-pro-doc').text('" . $dadosDep[$i]->id . "');setObservacaoExpProDoc();return false;\" ></center></td>";
					echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-exp-pro-doc').text('" . $dadosDep[$i]->id . "');apagarCaracterizacaoDocente();return false;\" ></center></td>";
				echo "</tr>";
			}
		}
	}
	
	 echo "</tbody>
    </table>
	<p id='chave-exp-pro-doc' hidden></p>
</div>";

	echo "<h3>Caracterização Pós-Doc</h3>\n";
	echo "<div id='caracterizacaoPos'>";
	echo "<table id='caracPos' class='box-table-b'>
			<thead>
				<tr>
					<th>IDINV</th>
					<th>Nome</th>
					<th>Tipo Bolsa</th>
					<th>Percentagem</th>
					<th colspan='3'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewCaracPosDoc();return false;'></center></th>
				</tr>
			</thead>
			<tbody>";

	foreach ($dadosDep as $i => $value){		
		if($dadosDep[$i]->investigadorcat==1){
			$checkAcao = $db->checkAcaoExists($dadosDep[$i]->id, $_SESSION['login'], 27);		
			if ($checkAcao) {
				$acao = transformIntoAcaoCarPosDocObject($dadosDep[$i]->id, $dadosDep[$i]->idinv, $checkAcao);			
				echo "<tr>";
					echo "<td>".$dadosDep[$i]->idinv."</td>";
					echo "<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
					echo "<td>".getBolsaInvestigador($dadosDep[$i]->investigadorbolsa)."</td>";
					echo "<td>".$dadosDep[$i]->investigadorper."</td>";
					echo "<td></td>";
					echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-exp-pro-pdoc').text('" . $dadosDep[$i]->id . "');setObservacaoExpProPosDoc();return false;\" ></center></td>";
					echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-exp-pro-pdoc').text('" . $dadosDep[$i]->id . "');apagarCaracterizacaoPosDoc();return false;\" ></center></td>";
				echo "</tr>";
				
				echo "<tr>";
					echo "<td style='background:#FFFF33; overflow:hidden;'><img src=\"../../images/arrow_return_down_right.png\" name='navOption'></td>";
					echo "<td style='background:#FFFF33; overflow:hidden;'>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_exp_pro_pdoc_bol_" . $dadosDep[$i]->id . "'>".getBolsaInvestigador($acao['investigadorbolsa'])."</td>";
					echo "<td style='background:#FFFF33; overflow:hidden;' id='td_exp_pro_pdoc_per_" . $dadosDep[$i]->id . "'>".$acao['investigadorper']."</td>";
					echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-exp-pro-pdoc').text('" . $dadosDep[$i]->id. "');\"></td>";
					echo "<td></td>";
					echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"apagarAcao('" . $dadosDep[$i]->id . "',27);return false;\" ></center></td>";
				echo "</tr>";
			} else { 
				echo "<tr>";
					echo "<td>".$dadosDep[$i]->idinv."</td>";
					echo "<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";	
					echo "<td id='td_exp_pro_pdoc_bol_" . $dadosDep[$i]->id . "'>".getBolsaInvestigador($dadosDep[$i]->investigadorbolsa)."</td>";
					echo "<td id='td_exp_pro_pdoc_per_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->investigadorper."</td>";
					echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-exp-pro-pdoc').text('" . $dadosDep[$i]->id. "');\"></td>";
					echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-exp-pro-pdoc').text('" . $dadosDep[$i]->id . "');setObservacaoExpProPosDoc();return false;\" ></center></td>";
					echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-exp-pro-pdoc').text('" . $dadosDep[$i]->id . "');apagarCaracterizacaoPosDoc();return false;\" ></center></td>";
				echo "</tr>";
			}
		}
	}

	echo "</tbody>
	</table>";
	echo "<p id='chave-exp-pro-pdoc' hidden></p>";
	echo "</div>";
	
}

	function transformIntoAcaoCarDocObject($id, $idinv, $query)
	{
		$acao = array();
		$cena = explode("DOCENTECAT=",$query);
				
		$cena1 = explode(", DOCENTEPER=",$cena[1]);
		$acao['docentecat'] = $cena1[0];
		
		$cena2 = explode(" where",$cena1[1]);
		$acao['docenteper'] = $cena2[0];
				
		return $acao;
	}
	
	function transformIntoAcaoCarPosDocObject($id, $idinv, $query)
	{
		$acao = array();
		$cena = explode("INVESTIGADORBOLSA=",$query);
				
		$cena1 = explode(", INVESTIGADORPER=",$cena[1]);
		$acao['investigadorbolsa'] = $cena1[0];
		
		$cena2 = explode(" where",$cena1[1]);
		$acao['investigadorper'] = $cena2[0];
				
		return $acao;
	}

	function checkSelectedOP($i,$valor){
	
		if($i==$valor){
			return true;
		}else{
			return false;
		}
	
	}

	function getCat($tipo,$valorcat) {
		$resultado="";
		$db = new Database();
		$lValues =$db->getLookupValues("lista_tipo".$tipo);
	
	
		while ($row = mysql_fetch_assoc($lValues)) {
			if(checkSelectedOP($row["ID"],$valorcat))
				$resultado=$row["DESCRICAO"];
		}
		$db->disconnect();
		return $resultado;
	}
	
	function getCatInvestigador($tipo,$valorcat) {
		$resultado="";
		$db = new Database();
		$lValues =$db->getLookupValues("lista_tipo".$tipo);
		while ($row = mysql_fetch_assoc($lValues)) {
			if(checkSelectedOP($row["ID"],$valorcat))
				$resultado=$row["DESCRICAO"];
		}
	
		$db->disconnect();
		return $resultado;
	}
	
	function getBolsaInvestigador($bolsa) {
		$resultado="";
		$db = new Database();
		$lValues =$db->getLookupValues("lista_TipoBolsas");
	
		while ($row = mysql_fetch_assoc($lValues)) {
			if(checkSelectedOP($row["ID"],$bolsa))
				$resultado=$row["DESCRICAO"];
		}

		$db->disconnect();
		return $resultado;
	}
	

?>