<?php

function showEditPremios($dadosDep) {

	$db = new Database();
	echo "<h3>Prémios</h3>\n";
	echo "<div id='premios'>";
			
	echo "<table id='premio' class='box-table-b'>
			<thead>
				<tr>
					<th>IDINV</th>	
					<th>Nome Investigador</th>		
					<th>Tipo</th>
					<th>Nome</th>
					<th>Contexto</th>
					<th>Montante</th>
					<th>Link</th>
					<th colspan='3'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewPremio();return false;'></center></th>
				</tr>
			</thead>
			<tbody>";
			    
	foreach ($dadosDep as $i => $value){
		$checkAcao = $db->checkAcaoExists($dadosDep[$i]->id, $_SESSION['login'], 18);		
		if ($checkAcao) {
			$acao = transformIntoAcaoPremioObject($dadosDep[$i]->id, $dadosDep[$i]->idinv, $checkAcao);		
			
			echo "<tr>";
				echo "<td>".$dadosDep[$i]->idinv."</td>";	
				echo "<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";			
				echo "<td>";getTipoPremios($dadosDep[$i]->tipo);	echo "</td>";	
				echo "<td>".$dadosDep[$i]->nome."</td>";
				echo "<td>".$dadosDep[$i]->contexto."</td>";
				echo "<td>".$dadosDep[$i]->montante." €</td>";
				echo "<td><a href='".$dadosDep[$i]->link."' target='_blank'>".$dadosDep[$i]->link."</a></td>";
				echo "<td></td>";
				echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-premios').text('" . $dadosDep[$i]->id . "');setObservacaoPremio();return false;\" ></center></td>";				
				echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-premios').text('" . $dadosDep[$i]->id . "');apagarPremio();return false;\" ></center></td>";
			echo "</tr>";	   
		
			echo "<tr>";
				echo "<td style='background:#FFFF33; overflow:hidden;'><img src=\"../../images/arrow_return_down_right.png\" name='navOption'></td>";		
				echo "<td style='background:#FFFF33; overflow:hidden;'>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";			
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_premios_tipo_" . $dadosDep[$i]->id . "'>";	getTipoPremios($acao['tipo']); echo "</td>";	
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_premios_nome_" . $dadosDep[$i]->id . "'>".$acao['nome']."</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_premios_contexto_" . $dadosDep[$i]->id . "'>".$acao['contexto']."</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_premios_montante_" . $dadosDep[$i]->id . "'>".$acao['montante']." €</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_premios_link_" . $dadosDep[$i]->id . "'><a href='".$acao['link']."' target='_blank'>".$acao['link']."</a></td>";
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-premios').text('" . $dadosDep[$i]->id . "');\"></td>";
				echo "<td></td>";
				echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"apagarAcao('" . $dadosDep[$i]->id . "',18);return false;\" ></center></td>";
			echo "</tr>";
			
		} else {
			echo "<tr>";
				echo "<td>".$dadosDep[$i]->idinv."</td>";
				echo "<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";			
				echo "<td id='td_premios_tipo_" . $dadosDep[$i]->id . "'>";getTipoPremios($dadosDep[$i]->tipo);	echo "</td>";	
				echo "<td id='td_premios_nome_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->nome."</td>";
				echo "<td id='td_premios_contexto_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->contexto."</td>";
				echo "<td id='td_premios_montante_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->montante." €</td>";
				echo "<td id='td_premios_link_" . $dadosDep[$i]->id . "'><a href='".$dadosDep[$i]->link."' target='_blank'>".$dadosDep[$i]->link."</a></td>";
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-premios').text('" . $dadosDep[$i]->id . "');\"></td>";
				echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-premios').text('" . $dadosDep[$i]->id . "');setObservacaoPremio();return false;\" ></center></td>";				
				echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-premios').text('" . $dadosDep[$i]->id . "');apagarPremio();return false;\" ></center></td>";
			echo "</tr>";	   
		}
	}
	
	 echo "</tbody>
    </table>
    <p id='chave-premios' hidden></p>
  </div>";
}

function transformIntoAcaoPremioObject($id, $idinv, $query) {
	$acao = array();
	
	$cena = explode("TIPO='",$query);
				
	$cena1 = explode("', NOME='",$cena[1]);
	$acao['tipo'] = $cena1[0];
	
	$cena2 = explode("', CONTEXTO='",$cena1[1]);
	$acao['nome']= $cena2[0];
	
	$cena3 = explode("', MONTANTE='",$cena2[1]);
	$acao['contexto'] = $cena3[0];	
	
	$cena4 = explode("', LINK='",$cena3[1]);
	$acao['montante'] = $cena4[0];	
							
	$cena5 = explode("' where",$cena4[1]);
	$acao['link'] = $cena5[0];
	
	return $acao;
}

function getTipoPremios($i) {	
	$db = new Database();
	$lValues =$db->getLookupValues("lista_tipopremio");	
	while ($row = mysql_fetch_assoc($lValues)) {	
		if($i==$row["ID"])
			echo $row["DESCRICAO"];
	}
	echo "</SELECT><br />\n";
	$db->disconnect();				
}	

?>	