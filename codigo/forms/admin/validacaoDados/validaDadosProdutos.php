<?php

function showEditProdutos($dadosDep) {
	$db = new Database();	
		
	echo "<h3>Produtos e Serviços</h3>\n
		  <div id='produtos'>";
				
	echo "<table id='prdts' class='box-table-b'>
			<thead>
				<tr>
					<th>IDINV</th>
					<th>Nome</th>
					<th>Tipo</th>
					<th>Valor</th>
					<th colspan='3'><center><input type='image' src=\"../../images/icon_new.png\" onclick='insertNewProduto();return false;'></center></th>
				</tr>
			</thead>
			<tbody>";			
		
	foreach ($dadosDep as $i => $value) {				
		$checkAcao = $db->checkAcaoExists($dadosDep[$i]->id, $_SESSION['login'], 26);
		if ($checkAcao) {		
			$acao = transformIntoProdutoObject($dadosDep[$i]->id, $dadosDep[$i]->idinv, $checkAcao);
			echo "<tr>";
				echo "<td>".$dadosDep[$i]->idinv."</td>";
				echo "<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
				echo "<td>".$dadosDep[$i]->tipop."</td>";
				echo "<td>".$dadosDep[$i]->valorp." €</td>";
				echo "<td></td>";
				echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-produtos').text('" . $dadosDep[$i]->id . "');setObservacaoProduto();return false;\" ></center></td>";
				echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-produtos').text('" . $dadosDep[$i]->id . "');apagarProduto();return false;\" ></center></td>";   
			echo "</tr>";	
		
			echo "<tr>";
				echo "<td style='background:#FFFF33; overflow:hidden;'><img src=\"../../images/arrow_return_down_right.png\" name='navOption'></td>";
				echo "<td style='background:#FFFF33; overflow:hidden;'>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_produtos_tipo_" . $dadosDep[$i]->id . "'>".$acao['tipop']."</td>";
				echo "<td style='background:#FFFF33; overflow:hidden;' id='td_produtos_preco_" . $dadosDep[$i]->id . "'>".$acao['valorp']." €</td>";
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-produtos').text('" . $dadosDep[$i]->id . "');\"></td>";
				echo "<td></td>";
				echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"apagarAcao('" . $dadosDep[$i]->id . "',26);return false;\" ></center></td>";   
			echo "</tr>";	  
		} else {
			echo "<tr>";
				echo "<td>".$dadosDep[$i]->idinv."</td>";
				echo "<td>".$db->getNomeInvById($dadosDep[$i]->idinv). "</td>";
				echo "<td id='td_produtos_tipo_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->tipop."</td>";
				echo "<td id='td_produtos_preco_" . $dadosDep[$i]->id . "'>".$dadosDep[$i]->valorp." €</td>";
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-produtos').text('" . $dadosDep[$i]->id . "');\"></td>";
				echo "<td style='overflow:hidden;'><center><input type='image' src=\"../../images/comment_icon.png\" name='navOption' onclick=\"$('#chave-produtos').text('" . $dadosDep[$i]->id . "');setObservacaoProduto();return false;\" ></center></td>";
				echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-produtos').text('" . $dadosDep[$i]->id . "');apagarProduto();return false;\" ></center></td>";   
			echo "</tr>";	  
		}
	}
	
	echo "</tbody>
    </table>
    <p id='chave-produtos' hidden></p>
</div>";
}

function transformIntoProdutoObject($id, $idinv, $query) {
	$acao = array();
	
	$cena = explode("TIPOP='",$query);
				
	$cena1 = explode("', VALORP=",$cena[1]);
	$acao['tipop'] = $cena1[0];
	
	$cena2 = explode(" where",$cena1[1]);
	$acao['valorp'] = $cena2[0];
			
	return $acao;
}

?>