<?php
error_reporting(E_ALL);
ini_set("display_errors", "On");
ini_set("display_startup_errors",1);

session_start();
//ini_set('session.gc_maxlifetime', 1*60);
$a = session_id();
if(empty($a)){
	header("Location: ../../index.php?erro=404");
}

$login=$_SESSION['login'];

if(empty($login)){
	header("Location: ../../index.php?erro=404");
}

require_once '../../classlib/Database.class.inc';

//verificar se o utilizador logado tem permissoes para estar nesta página (diretor ou admin)
$db = new Database();
$db->connect();
if(!strcmp($login,'sciman') || !strcmp($login, 'admin') || !strcmp($login,'cam')){ 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt" xml:lang="pt">
	<head>
		<link rel="icon" href="../../images/up.png" />
		<meta name="description" content="Transição de Base de Dados"/>
		
		<meta name="keywords" content="Base de Dados, BD"/>
		
		<meta name="author" content="Cátia Marques"/>
		
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		
		<link rel="stylesheet" type="text/css" href="../../styles/style_screen2.css" media="screen"/>
		<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
		<style type="text/css">
			
		</style>
		
		<title>Levantamento Geral de Dados da FMUP 2013</title>
		
		<script type="text/javascript" src="../../js/jquery.1.4.2.js"></script>
		<script type="text/javascript" src="../../js/funcoes.js"></script>
		<script type="text/javascript" src='../../js/jQuery/jquery-1.10.2.js'></script>
	</head>
	<body>
		<fieldset class='head'>
			<table style="padding: 10px 10px 10px 190px;">
				<tr><td align="right"><h2>Levantamento Geral de Dados da FMUP 2013 - ADMINISTRAÇÃO</h2></td></tr>
				<tr><td align="right"><h2><i>General Survey of FMUP 2013</i></h2></td></tr>
			</table>
		</fieldset>
		
		<form action="" name='transicao' id='transicao' method='post' enctype='multipart/form-data'>
			<fieldset class='normal'>
				<legend>Transição dos dados para o ano letivo seguinte:</legend>
				<label class='label_input' for="nomeBD">Nome da nova Base de Dados:</label> 
				<input class="input_transicao" type="text" name="nomeBD" id="nomeBD" placeholder="Sem espaços" required/><br/>
				
				<label class='label_input' for="anoLevantamento">Novo Ano de Levantamento:</label> 
				<input class="input_transicao" type="text" name="anoLevantamento" id="anoLevantamento" placeholder="Número" required/><br/>
				
				<label class='label_input' for="tabelas">Tabelas a Transitar:</label> 
				<div class="div_transicao" id='lista_tabelas' style="width:550px; height:400px; overflow-y:auto;overflow-x: hidden;border:2px groove #FFCC00;padding: 2px 2px 2px 2px;">
				<?php
				try {
					$res = $db->nomesTabelas();
					$checkboxGeral="";
					$checkboxListaTipo="";
					$checkboxTrat = "";
					while ($row = mysql_fetch_row($res)) {
						if((strpos($row[0],'tipo') !== false) || (strpos($row[0],'lista') !== false)){//listas
							$checkboxListaTipo .= 
								"<label class='label_checkbox'>
									<input class='input_transicao' type='checkbox' name='tabelas[]' id='tabelas' value='$row[0]'>$row[0]</label>";
						}elseif((strpos($row[0],'repetidos_fase2') === false) && (strpos($row[0],'back') === false)
											 && (strpos($row[0],'teste_') === false)){//resto
							$checkboxGeral .= 
								"<label class='label_checkbox'>
									<input class='input_transicao' type='checkbox' name='tabelas[]' id='tabelas' value='$row[0]'>$row[0]</label>";
						}
					}  
					echo "<ul style='float: left;width: 100%;list-style-type: none;text-align:center;'>
							<li style='display: inline;width=50%'>
								<label for='outras' class='label_divisao'>
									<label>
										<input class='input_transicao' type='checkbox' id='selOutras' onclick='selecionaTodas(this.id,\"outras\")'>Outras
									</label>
									<div class='div_transicao' id ='outras' style='color: black;'>$checkboxGeral</div> 
								</label>
							</li> 
							<li style='display: inline;width=50%'>
								<label for='listas' class='label_divisao'> 
									<label>
											<input class='input_transicao' type='checkbox' id='selListas' onclick='selecionaTodas(this.id,\"listas\")'>Listas Pré-defenidas
									</label> 
									<div class='div_transicao' id ='listas' style='color: black;' >$checkboxListaTipo</div> 
								</label>
							</li>
							</ul>";
				} catch (Exception $e) {
					echo $e->getMessage(), "\n";
				}
				?>
				</div><br/>
				<input class='input_transicao' type="button" name="submit" id="submit" value="Criar" onclick="criarBD();">
			</fieldset>
		</form>
		<div class='div_transicao' id='waiting' style="visibility: inline ;margin: 0px 0px 0px 175px;"></div>
		<div class='div_transicao' id='aviso' style="visibility: inline ;margin: 0px 0px 0px 175px;color:#A80000"></div>
		<div class='div_transicao' id='sucesso' style="visibility: inline ;margin: 0px 0px 0px 175px;"></div>
		<input class='input_transicao' id="paraTras" type="image" src="../../images/Back-Arrow-Button.png" alt="Voltar à página anterior"  style="margin: 0px 0px 0px 175px"
				width="24" height="24" onclick="window.location.assign('admin.php');"/> 
		<fieldset class='footer'>
			<legend></legend>
			FACULDADE DE MEDICINA DA UNIVERSIDADE DO PORTO<br/>
			Serviço de Informática e Departamento de Apoio à Investigação e Pós-Graduação
		</fieldset>
	</body>
</html>
<?php }else{ header("Location: ../../index.php?erro=404"); }?>