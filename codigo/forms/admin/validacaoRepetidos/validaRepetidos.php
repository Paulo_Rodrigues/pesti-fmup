﻿<?php

require_once '../../classlib/ValidaRepetidos.class.inc';
require_once '../../classlib/Database.class.inc';

//Deteccao de repetidos
require_once '../../functions/validateRepetidosProjectos.php';
require_once '../../functions/validateRepetidosPublicacoes.php';
require_once '../../functions/validateRepetidosConferencias.php';
require_once '../../functions/validateRepetidosAcaoDivulgacao.php';
require_once '../../functions/validateRepetidosPremios.php';
require_once '../../functions/validateRepetidosArbitragensRevistas.php';
require_once '../../functions/validateRepetidosUnidadesInvestigacao.php';

//Interface
require_once 'showRepetidosProjectos.php';
require_once 'showRepetidosPublicacoes.php';
require_once 'showRepetidosConferencias.php';
require_once 'showRepetidosAcoesDivulgacao.php';
require_once 'showRepetidosPremios.php';
require_once 'showRepetidosArbitragensRevistas.php';
require_once 'showRepetidosUnidadesInvestigacao.php';

$listaIdsPublicacoes = '';
$listaIdsProjectos = '';
$listaIdsConferencias = '';
$listaIdsAcoesDivulgacao = '';
$listaIdsPremios = '';
$listaIdsArbitragensRevistas = '';
$listaIdsUnidadesInvestigacao = '';

define('SCRIPT_DEBUG', true);
set_time_limit(200);
if(isset($_SESSION['departamento']))
{
	if($_SESSION['departamento'] == 50)
	{
		//Informações de todos os departamentos	
		$dados = new ValidaRepetidos();
		$op =  $_SESSION['tipoValidacao'];
		switch($op) {
			case '1': {
				if(count($dados->publicacoesISI) + count($dados->publicacoesPUBMED) + count($dados->publicacoesMANUALPatentes) + count($dados->publicacoesMANUALNacional) + count($dados->publicacoesMANUALInternacional) == 0)
					echo "<fieldset class='normal'><p>Não existem publicações registados relacionados com o departamento seleccionado.<p></fieldset>";
				else
				{
					validateRepetidosPublicacoes($dados);					
					$listaIdsPublicacoes = showPublicacoes();					
				}
			}
			break;
			case '2': {
			
				if(count($dados->projectos) == 0)
					echo "<fieldset class='normal'><p>Não existem projectos registados relacionados com o departamento seleccionado.<p></fieldset>";
				else
				{
					validateRepetidosProjectos($dados->projectos);
					$listaIdsProjectos = showProjectos();
				}
			}
			break;		
			case '3': {
				if(count($dados->conferencias) == 0)
					echo "<fieldset class='normal'><p>Não existem conferências registados relacionados com o departamento seleccionado.<p></fieldset>";
				else
				{
					validateRepetidosConferencias($dados->conferencias);
					$listaIdsConferencias = showConferencias();
				}
			}
			break;
			case '4': {
				if(count($dados->acoesDivulgacao) == 0)
					echo "<fieldset class='normal'><p>Não existem ações de divulgação registadas relacionados com o departamento seleccionado.<p></fieldset>";
				else
				{
					validateRepetidosAcaoDivulgacao($dados->acoesDivulgacao);
					$listaIdsAcoesDivulgacao = showAcoesDivulgacao();
				}				
			}
			break;	
			case '5': {
				if(count($dados->premios) == 0)
					echo "<fieldset class='normal'><p>Não existem prémios registados relacionados com o departamento seleccionado.<p></fieldset>";
				else
				{
					validateRepetidosPremios($dados->premios);
					$listaIdsPremios = showPremios();
				}	
			}
			break;	
			case '6': {
				if(count($dados->arbitragensRevistas) == 0)
					echo "<fieldset class='normal'><p>Não existem arbitragens de revistas registadas relacionados com o departamento seleccionado.<p></fieldset>";
				else
				{
					validateRepetidosArbitragensRevistas($dados->arbitragensRevistas);
					$listaIdsArbitragensRevistas = showArbitragensRevistas();
				}	
			}
			break;	
			case '7': {
				if(count($dados->unidadesInvestigacao) == 0)
					echo "<fieldset class='normal'><p>Não existem unidades de investigação registadas relacionados com o departamento seleccionado.<p></fieldset>";
				else
				{
					validateRepetidosUnidadesInvestigacao($dados->unidadesInvestigacao);
					$listaIdsUnidadesInvestigacao = showUnidadesInvestigacao();
				}	
			}
			break;	
		}
	}
	else
	{
		//Informação de um departamento específico
		$dados = new ValidaRepetidosDepartamento($_SESSION['departamento']);					
		$op =  $_SESSION['tipoValidacao'];
		switch($op) {
			case '1': {
				if(count($dados->publicacoesISI) + count($dados->publicacoesPUBMED) + count($dados->publicacoesMANUALPatentes) + count($dados->publicacoesMANUALNacional) + count($dados->publicacoesMANUALInternacional) == 0)
					echo "<fieldset class='normal'><p>Não existem publicações registados relacionados com o departamento seleccionado.<p></fieldset>";
				else
				{
					validateRepetidosPublicacoes($dados);
					$listaIdsPublicacoes = showPublicacoes();
				}
			}
			break;
			case '2': {
				if(count($dados->projectos) == 0)
					echo "<fieldset class='normal'><p>Não existem projectos registados relacionados com o departamento seleccionado.<p></fieldset>";
				else
				{
					validateRepetidosProjectos($dados->projectos);
					$listaIdsProjectos = showProjectos();
				}
			}
			break;		
			case '3': {					
				if(count($dados->conferencias) == 0)
					echo "<fieldset class='normal'><p>Não existem conferências registados relacionados com o departamento seleccionado.<p></fieldset>";
				else
				{	
					validateRepetidosConferencias($dados->conferencias);					
					$listaIdsConferencias = showConferencias();
				}
			}
			break;
			case '4': {
				if(count($dados->acoesDivulgacao) == 0)
					echo "<fieldset class='normal'><p>Não existem ações de divulgação registadas relacionados com o departamento seleccionado.<p></fieldset>";
				else
				{
					validateRepetidosAcaoDivulgacao($dados->acoesDivulgacao);
					$listaIdsAcoesDivulgacao = showAcoesDivulgacao();
				}
			}
			break;	
			case '5': {
				if(count($dados->premios) == 0)
					echo "<fieldset class='normal'><p>Não existem prémios registados relacionados com o departamento seleccionado.<p></fieldset>";
				else
				{
					validateRepetidosPremios($dados->premios);
					$listaIdsPremios = showPremios();
				}	
			}
			break;	
			case '6': {
				if(count($dados->arbitragensRevistas) == 0)
					echo "<fieldset class='normal'><p>Não existem arbitragens de revistas registadas relacionados com o departamento seleccionado.<p></fieldset>";
				else
				{
					validateRepetidosArbitragensRevistas($dados->arbitragensRevistas);
					$listaIdsArbitragensRevistas = showArbitragensRevistas();
				}	
			}
			break;	
			case '7': {
				if(count($dados->unidadesInvestigacao) == 0)
					echo "<fieldset class='normal'><p>Não existem unidades de investigação registadas relacionados com o departamento seleccionado.<p></fieldset>";
				else
				{
					validateRepetidosUnidadesInvestigacao($dados->unidadesInvestigacao);
					$listaIdsUnidadesInvestigacao = showUnidadesInvestigacao();
				}
			}
			break;	
		}
		
		}		
	}	

include("../camposEdicao.php");

echo "
	<script>					
		ids = '"; echo "$listaIdsPublicacoes"; echo "$listaIdsProjectos"; echo "$listaIdsConferencias"; echo "$listaIdsAcoesDivulgacao"; echo "$listaIdsPremios"; echo "$listaIdsArbitragensRevistas"; echo "$listaIdsUnidadesInvestigacao"; echo "';
		alert('ATENÇÃO: Não ordenar nenhuma das tabelas enquanto os repetidos detectados pelo sistema não tenham sido validados. Caso contrário, a ordem com que os repetidos aparecem pode ser perdida.');
	</script>";	

	
	echo "<p id='login' hidden>" . $_SESSION['login'] . "</p>
	<p id='dep' hidden>" .$_SESSION['departamento'] . "</p>
	<p id='tabela' hidden></p>";
	
?>
