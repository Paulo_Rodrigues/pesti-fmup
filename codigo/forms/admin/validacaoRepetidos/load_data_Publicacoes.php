﻿<?php

	ini_set('display_errors',1);
	ini_set('display_startup_errors',1);
	error_reporting(-1);
	
	//include "db.php";
	
	require_once '../../../classlib/Database.class.inc';
	
	$db = new Database();
	$db->connect();
	
	if($_POST['page'] && $_POST['a'] )
	{			
		switch ($_POST['a']) {
			case 1:
				loadTableJ();
				break;	
			case 2:
				loadTableJA();
				break;
			case 3:
				loadTablePRP();
				break;
			case 4:
				loadTableCP();
				break;
			case 5:
				loadTableOA();
				break;
			case 6:
				loadTableL();
				break;
			case 7:
				loadTableCL();
				break;
			case 8:
				loadTableS();
				break;
			case 9:
				 loadTableJN();
				break;	
			case 10:
				loadTablePRPN();
				break;
			case 11:
				loadTableSN();
				break;
			case 12:
				loadTableLN();
				break;
			case 13:
				loadTableCLN();
				break;
			case 14:
				loadTableCPN();
				break;
			case 15:
				loadTableP();
				break;
			case 16:
				loadTableA();
				break;
			default:
				# code...
				break;
		}		
	}

	$db->disconnect();
			
function loadTableJ() {		
	$db = new Database();
	$msgArt = "<thead class='header'><tr><th>IDINV</th><th>Tipo Publicação</th><th>ISSN</th><th>REVISTA</th><th>TÍTULO</th><th>AUTORES</th><th>VOLUME</th><th>ISSUE</th><th>PRIMEIRA PÁG.</th><th>ÚLTIMA PÁG.</th><th width='40'>LINK</th><th width='60'>ESTADO</th><th width='50'>Repetidos</th><th width='50'>Original</th><th width='30'></th><th width='30'></th></tr></thead><tbody>";
	$msgJ ='';	
	$page = $_POST['page'];
	$page -= 1;
	$per_page = 30;
	$start = $page * $per_page;
	
	$ids = array();
	$ids_final = '';
	$originais = array();

	//Get 30 registos 
	$query_pag_data = "SELECT DISTINCT id_original from repetidos_pub WHERE tipofmup = 'J' LIMIT $start, $per_page";
	$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error() );
	while ($row = mysql_fetch_array($result_pag_data)) {
		$ids[]=$row['id_original'];
	}
	
	if(!empty($ids)) {
		$ids = array_unique($ids);

		foreach ($ids as $i => $value) {
			$ids_final .= $ids[$i] . ',';
			$query_pag_data = "SELECT id_repetido from repetidos_pub WHERE id_original = " . $ids[$i] . " AND tipofmup = 'J';";
			$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error());
			
			$originais[$i] = array();
			$originais[$i][] = $ids[$i];
			while ($row = mysql_fetch_array($result_pag_data)) {
				if($row['id_repetido'] != null)
					$originais[$i][]=$row['id_repetido'];
			}
		}
		
		foreach ($originais as $i => $value) {		
			foreach ($originais[$i] as $j => $value1) {		
				
				//Verifica se existe algum registo repetido
				if($j == 0)	{
					if (count($originais[$i])==1) {
						$style ="style=\"overflow:hidden;\"";	
					} else {		
						$style ="style=\"background-color:#99FFFF; overflow:hidden;\"";	
					}				
				} else {
					$style ="style=\"background-color:#FFFF33; overflow:hidden;\"";	
				}		
				
				$msgJ .= "<tr>";
				
				$repetidos = $db->getRepetidosPublicacoes($originais[$i][$j]);
				$idInv = $db->getIdInvByIdPub($originais[$i][$j]);
				if($idInv) {
					if ($repetidos) {	
						$style ="style=\"background-color:#99FF66; overflow:hidden;\"";		
							$msgJ .= "<td id='". $originais[$i][$j]. "' $style>".$idInv;							
						foreach ($repetidos as $x => $value2) {	
							if($idInv != $value2) {
								$msgJ .= "," . $value2 ;				
							}								
						}
						$msgJ .=  "</td>";
					} else {						
						$msgJ .=  "<td id='". $originais[$i][$j]. "' $style>".$idInv."</td>";
					}			
					
					$query_pag_data = "SELECT * FROM publicacoes WHERE id =" .  $originais[$i][$j] . ";";	
					$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error() . " dentro do repetidos $query_pag_data");
					while ($row = mysql_fetch_array($result_pag_data)) {	
						if($row['INDEXED_ISI'] == 1 && $row['INDEXED_PUBMED'] == 0) {
							//ISI
							$msgJ .= printISIArt($row,$style,'ISI','');		
						} else if ($row['INDEXED_ISI'] == 0 && $row['INDEXED_PUBMED'] == 1) {
							//PUBMED
							$msgJ .= printISIArt($row,$style,'PUBMED','');	
						} else if ($row['TIPOFMUP'] == 'J'){
							//INTERNACIONAL
							$msgJ .= printISIArt($row,$style,'outJMAN','Art');	
						}
					}
				}
			}					
		}
		
		$msgJ = "<table id='JISI' class='box-table-b'><caption>ARTIGOS</caption>" . $msgArt . $msgJ . "</tbody></table><p id='chave-pubIntMan-Art' hidden></p><br>" . drawButtons('J');	
		
		$msg = "<div id='pubManualInt-Art' class='pubISIdata' ><ul>" . $msgJ . "</ul></div><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>"; // Content for Data
		echo $msg;
	} else {
		$msg = "<p>Não existem Artigos a validar.</p>"; 
		echo $msg;	
	}
}
	
function loadTableJA() {	
	$db = new Database();
	$msgArt = "<thead class='header'><tr><th>IDINV</th><th>Tipo Publicação</th><th>ISSN</th><th>REVISTA</th><th>TÍTULO</th><th>AUTORES</th><th>VOLUME</th><th>ISSUE</th><th>PRIMEIRA PÁG.</th><th>ÚLTIMA PÁG.</th><th width='40'>LINK</th><th width='60'>ESTADO</th><th width='50'>Repetidos</th><th width='50'>Original</th><th width='30'></th><th width='30'></th></tr></thead><tbody>";
	$msgJA ='';
	$page = $_POST['page'];
	$page -= 1;
	$per_page = 30;
	$start = $page * $per_page;
	$ids_final = '';
	$ids = array();
	$originais = array();		

	//Get 30 registos 
	$query_pag_data = "SELECT DISTINCT id_original from repetidos_pub WHERE tipofmup = 'JA' LIMIT $start, $per_page";
	$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error() );
	while ($row = mysql_fetch_array($result_pag_data)) {
		$ids[]=$row['id_original'];
	}
	
	if(!empty($ids)) {
		$ids = array_unique($ids);
		
		foreach ($ids as $i => $value) {
			$ids_final .= $ids[$i] . ',';
			$query_pag_data = "SELECT id_repetido from repetidos_pub WHERE id_original = " . $ids[$i] . " AND tipofmup = 'JA';";
			$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error());
			
			$originais[$i] = array();
			$originais[$i][] = $ids[$i];
			while ($row = mysql_fetch_array($result_pag_data)) {
				if($row['id_repetido'] != null)
					$originais[$i][]=$row['id_repetido'];
			}
		}
		
		foreach ($originais as $i => $value) {		
			foreach ($originais[$i] as $j => $value1) {		
				
				//Verifica se existe algum registo repetido
				if($j == 0)	{
					if (count($originais[$i])==1) {
						$style ="style=\"overflow:hidden;\"";	
					} else {		
						$style ="style=\"background-color:#99FFFF; overflow:hidden;\"";	
					}				
				} else {
					$style ="style=\"background-color:#FFFF33; overflow:hidden;\"";	
				}		
				
				$msgJA .= "<tr>";
				
				$repetidos = $db->getRepetidosPublicacoes($originais[$i][$j]);
				$idInv = $db->getIdInvByIdPub($originais[$i][$j]);
				if($idInv) {
					if ($repetidos) {	
						$style ="style=\"background-color:#99FF66; overflow:hidden;\"";		
							$msgJA .= "<td id='". $originais[$i][$j]. "' $style>".$idInv;							
						foreach ($repetidos as $x => $value2) {	
							if($idInv != $value2) {
								$msgJA .= "," . $value2 ;				
							}								
						}
						$msgJA .=  "</td>";
					} else {						
						$msgJA .=  "<td id='". $originais[$i][$j]. "' $style>".$idInv."</td>";
					}			
					
					$query_pag_data = "SELECT * FROM publicacoes WHERE id =" .  $originais[$i][$j] . ";";	
					$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error() . " dentro do repetidos $query_pag_data");
					while ($row = mysql_fetch_array($result_pag_data)) {					
						if($row['INDEXED_ISI'] == 1 && $row['INDEXED_PUBMED'] == 0) {
							//ISI
							$msgJA .= printISIArt($row,$style,'ISI','');		
						} else if ($row['INDEXED_ISI'] == 0 && $row['INDEXED_PUBMED'] == 1) {
							//PUBMED
							$msgJA .= printISIArt($row,$style,'PUBMED','');	
						} else if ($row['TIPOFMUP'] == 'JA'){
							//INTERNACIONAL
							$msgJA .= printISIArt($row,$style,'outJAMAN','Sum');	
						} 					
					}
				}
			}					
		}
		
		$msgJA = "<table id='JAISI' class='box-table-b'><caption>JOURNAL ABSTRACTS</caption>" .$msgArt . $msgJA . "</tbody></table><p id='chave-pubIntMan-Sum' hidden></p><br>" . drawButtons('JA');	
				
		$msg = "<div class='pubISIdata' id='pubManualInt-Sum'><ul>" . $msgJA . "</ul></div><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>"; // Content for Data
		echo $msg;
	} else {
		$msg = "<p>Não existem Journal Abstracts a validar.</p>"; 
		echo $msg;	
	}
}

function loadTablePRP() {
	$db = new Database();
	$msgArt = "<thead class='header'><tr><th>IDINV</th><th>Tipo Publicação</th><th>ISSN</th><th>REVISTA</th><th>TÍTULO</th><th>AUTORES</th><th>VOLUME</th><th>ISSUE</th><th>PRIMEIRA PÁG.</th><th>ÚLTIMA PÁG.</th><th width='40'>LINK</th><th width='60'>ESTADO</th><th width='50'>Repetidos</th><th width='50'>Original</th><th width='30'></th><th width='30'></th></tr></thead><tbody>";
	$msgPRP ='';
	$page = $_POST['page'];
	$page -= 1;
	$per_page = 30;
	$start = $page * $per_page;
	$ids_final = '';
	$ids = array();
	$originais = array();		

	//Get 30 registos 
	$query_pag_data = "SELECT DISTINCT id_original from repetidos_pub WHERE tipofmup = 'PRP' LIMIT $start, $per_page";
	$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error() );
	while ($row = mysql_fetch_array($result_pag_data)) {
		$ids[]=$row['id_original'];
	}

	$ids = array_unique($ids);
	if(!empty($ids)) {
		foreach ($ids as $i => $value) {
			$ids_final .= $ids[$i] . ',';
			$query_pag_data = "SELECT id_repetido from repetidos_pub WHERE id_original = " . $ids[$i] . " AND tipofmup = 'PRP';";
			$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error());
			
			$originais[$i] = array();
			$originais[$i][] = $ids[$i];
			while ($row = mysql_fetch_array($result_pag_data)) {
				if($row['id_repetido'] != null)
					$originais[$i][]=$row['id_repetido'];
			}
		}
		
		foreach ($originais as $i => $value) {		
			foreach ($originais[$i] as $j => $value1) {		
				
				//Verifica se existe algum registo repetido
				if($j == 0)	{
					if (count($originais[$i])==1) {
						$style ="style=\"overflow:hidden;\"";	
					} else {		
						$style ="style=\"background-color:#99FFFF; overflow:hidden;\"";	
					}				
				} else {
					$style ="style=\"background-color:#FFFF33; overflow:hidden;\"";	
				}		
				
				$msgPRP .= "<tr>";
				
				$repetidos = $db->getRepetidosPublicacoes($originais[$i][$j]);
				$idInv = $db->getIdInvByIdPub($originais[$i][$j]);
				if($idInv) {
					if ($repetidos) {	
						$style ="style=\"background-color:#99FF66; overflow:hidden;\"";		
							$msgPRP .= "<td id='". $originais[$i][$j]. "' $style>".$idInv;							
						foreach ($repetidos as $x => $value2) {	
							if($idInv != $value2) {
								$msgPRP .= "," . $value2 ;				
							}								
						}
						$msgPRP .=  "</td>";
					} else {						
						$msgPRP .=  "<td id='". $originais[$i][$j]. "' $style>".$idInv."</td>";
					}			
					
					$query_pag_data = "SELECT * FROM publicacoes WHERE id =" .  $originais[$i][$j] . ";";	
					$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error() . " dentro do repetidos $query_pag_data");
					while ($row = mysql_fetch_array($result_pag_data)) {
						if($row['INDEXED_ISI'] == 1 && $row['INDEXED_PUBMED'] == 0) {
							//ISI
							$msgPRP .= printISIArt($row,$style,'ISI','');		
						} else if ($row['INDEXED_ISI'] == 0 && $row['INDEXED_PUBMED'] == 1) {
							//PUBMED
							$msgPRP .= printISIArt($row,$style,'PUBMED','');	
						} else if ($row['TIPOFMUP'] == 'PRP'){
							//INTERNACIONAL
							$msgPRP .= printISIArt($row,$style,'outPRPMAN','PRP');	
						} 							
					}
				}
			}					
		}
		
		$msgPRP = "<table id='PRPISI' class='box-table-b'><caption>PEER REVIEW PROCEEDINGS</caption>" .$msgArt . $msgPRP. "</tbody></table><p id='chave-pubIntMan-PRP' hidden><br>" . drawButtons('PRP');
				
		$msg = "<div class='pubISIdata' id='pubManualInt-Rev'><ul>" . $msgPRP . "</ul></div><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>"; // Content for Data
		echo $msg;
	} else {
		$msg = "<p>Não existem Peer Review Proceedings a validar.</p>"; 
		echo $msg;	
	}
}

function loadTableCP() {
	$db = new Database();
	$msgConfP = "<thead class='header'><tr><th>IDINV</th><th>Tipo Publicação</th><th>ISBN</th><th>TÍTULO DA OBRA</th><th>EDITORES</th><th>TÍTULO ARTIGO</th><th>AUTORES</th><th>PRIMEIRA PÁG.</th><th>ÚLTIMA PÁG.</th><th width='40'>LINK</th><th width='60'>ESTADO</th><th width='50'>Repetidos</th><th width='50'>Original</th><th width='30'></th><th width='30'></th></tr></thead><tbody>";
	$msgCP ='';
	$page = $_POST['page'];
	$page -= 1;
	$per_page = 30;
	$start = $page * $per_page;
	$ids_final = '';
	$ids = array();
	$originais = array();		

	//Get 30 registos 
	$query_pag_data = "SELECT DISTINCT id_original from repetidos_pub WHERE tipofmup = 'CP' LIMIT $start, $per_page";
	$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error() );
	while ($row = mysql_fetch_array($result_pag_data)) {
		$ids[]=$row['id_original'];
	}
	if (!empty($ids)) {
		$ids = array_unique($ids);

		foreach ($ids as $i => $value) {
			$ids_final .= $ids[$i] . ',';
			$query_pag_data = "SELECT id_repetido from repetidos_pub WHERE id_original = " . $ids[$i] . " AND 'CP';";
			$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error());
			
			$originais[$i] = array();
			$originais[$i][] = $ids[$i];
			while ($row = mysql_fetch_array($result_pag_data)) {
				if($row['id_repetido'] != null)
					$originais[$i][]=$row['id_repetido'];
			}
		}
		
		foreach ($originais as $i => $value) {		
			foreach ($originais[$i] as $j => $value1) {		
				
				//Verifica se existe algum registo repetido
				if($j == 0)	{
					if (count($originais[$i])==1) {
						$style ="style=\"overflow:hidden;\"";	
					} else {		
						$style ="style=\"background-color:#99FFFF; overflow:hidden;\"";	
					}				
				} else {
					$style ="style=\"background-color:#FFFF33; overflow:hidden;\"";	
				}		
				
				$msgCP .= "<tr>";
				
				$repetidos = $db->getRepetidosPublicacoes($originais[$i][$j]);
				$idInv = $db->getIdInvByIdPub($originais[$i][$j]);
				if($idInv) {
					if ($repetidos) {	
						$style ="style=\"background-color:#99FF66; overflow:hidden;\"";		
							$msgCP .= "<td id='". $originais[$i][$j]. "' $style>".$idInv;							
						foreach ($repetidos as $x => $value2) {	
							if($idInv != $value2) {
								$msgCP .= "," . $value2 ;				
							}								
						}
						$msgCP .=  "</td>";
					} else {						
						$msgCP .=  "<td id='". $originais[$i][$j]. "' $style>".$idInv."</td>";
					}			
					
					$query_pag_data = "SELECT * FROM publicacoes WHERE id =" .  $originais[$i][$j] . ";";	
					$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error() . " dentro do repetidos $query_pag_data");
					while ($row = mysql_fetch_array($result_pag_data)) {
						if($row['INDEXED_ISI'] == 1 && $row['INDEXED_PUBMED'] == 0) {
							//ISI
							$msgCP .= printISICP($row,$style,'ISI','');		
						} else if ($row['INDEXED_ISI'] == 0 && $row['INDEXED_PUBMED'] == 1) {
							//PUBMED
							$msgCP .= printISICP($row,$style,'PUBMED','');	
						} else if ($row['TIPOFMUP'] == 'CP'){
							//INTERNACIONAL
							$msgCP .= printISICP($row,$style,'outCPMAN','Con');	
						} 							
					}
				}
			}					
		}
		
		$msgCP = "<table id='CPISI' class='box-table-b'><caption>CONFERENCE PROCEEDINGS</caption>" .$msgConfP . $msgCP . "</tbody></table><p id='chave-pubIntMan-Con' hidden></p><br>" . drawButtons('CP');	
					
		$msg = "<div class='pubISIdata' id='pubManualInt-Con'><ul>" . $msgCP . "</ul></div><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>"; // Content for Data
		echo $msg;
	} else {
		$msg = "<p>Não existem Conference Proceedings a validar.</p>"; 
		echo $msg;	
	}
	
}

function loadTableOA() {
	$db = new Database();	
	$msgConfP = "<thead class='header'><tr><th>IDINV</th><th>Tipo Publicação</th><th>ISBN</th><th>TÍTULO DA OBRA</th><th>EDITORES</th><th>TÍTULO ARTIGO</th><th>AUTORES</th><th>PRIMEIRA PÁG.</th><th>ÚLTIMA PÁG.</th><th width='40'>LINK</th><th width='60'>ESTADO</th><th width='50'>Repetidos</th><th width='50'>Original</th><th width='30'></th><th width='30'></th></tr></thead><tbody>";
	$msgOA = '';
	$page = $_POST['page'];
	$page -= 1;
	$per_page = 30;
	$start = $page * $per_page;
	$ids_final = '';
	$ids = array();
	$originais = array();		

	//Get 30 registos 
	$query_pag_data = "SELECT DISTINCT id_original from repetidos_pub WHERE (tipofmup = 'OA' OR tipofmup = 'OAN') LIMIT $start, $per_page";
	$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error() );
	while ($row = mysql_fetch_array($result_pag_data)) {
		$ids[]=$row['id_original'];
	}
	
	if (!empty($ids)) {
		$ids = array_unique($ids);

		foreach ($ids as $i => $value) {
			$ids_final .= $ids[$i] . ',';
			$query_pag_data = "SELECT id_repetido from repetidos_pub WHERE id_original = " . $ids[$i] . " AND ( tipofmup = 'OA' OR tipofmup = 'OAN');";
			$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error());
			
			$originais[$i] = array();
			$originais[$i][] = $ids[$i];
			while ($row = mysql_fetch_array($result_pag_data)) {
				if($row['id_repetido'] != null)
					$originais[$i][]=$row['id_repetido'];
			}
		}
		
		foreach ($originais as $i => $value) {		
			foreach ($originais[$i] as $j => $value1) {		
				
				//Verifica se existe algum registo repetido
				if($j == 0)	{
					if (count($originais[$i])==1) {
						$style ="style=\"overflow:hidden;\"";	
					} else {		
						$style ="style=\"background-color:#99FFFF; overflow:hidden;\"";	
					}				
				} else {
					$style ="style=\"background-color:#FFFF33; overflow:hidden;\"";	
				}		
				
				$msgOA .= "<tr>";
				
				$repetidos = $db->getRepetidosPublicacoes($originais[$i][$j]);
				$idInv = $db->getIdInvByIdPub($originais[$i][$j]);
				if($idInv) {
					if ($repetidos) {	
						$style ="style=\"background-color:#99FF66; overflow:hidden;\"";		
							$msgOA .= "<td id='". $originais[$i][$j]. "' $style>".$idInv;							
						foreach ($repetidos as $x => $value2) {	
							if($idInv != $value2) {
								$msgOA .= "," . $value2 ;				
							}								
						}
						$msgOA .=  "</td>";
					} else {						
						$msgOA .=  "<td id='". $originais[$i][$j]. "' $style>".$idInv."</td>";
					}			
					
					$query_pag_data = "SELECT * FROM publicacoes WHERE id =" .  $originais[$i][$j] . ";";	
					$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error() . " dentro do repetidos $query_pag_data");
					while ($row = mysql_fetch_array($result_pag_data)) {	
						if($row['INDEXED_ISI'] == 1 && $row['INDEXED_PUBMED'] == 0) {
							//ISI
							$msgOA .= printISICP($row,$style,'ISI','');		
						} else if ($row['INDEXED_ISI'] == 0 && $row['INDEXED_PUBMED'] == 1) {
							//PUBMED
							$msgOA .= printISICP($row,$style,'PUBMED','');	
						} else if ($row['TIPOFMUP'] == 'OA'){
							//INTERNACIONAL
							$msgOA .= printISICP($row,$style,'outOAMAN','Oth');	
						} else if ($row['TIPOFMUP'] == 'OAN'){
							//NACIONAL
							$msgOA .= printISICP($row,$style,'outOANMAN','Oth');	
						} 					
					}
				}
			}					
		}
		
		$msgOA = "<table id='OAISI' class='box-table-b'><caption>OTHER ABSTRACTS</caption>" .$msgConfP . $msgOA . "</tbody></table><p id='chave-pubIntMan-Oth' hidden></p><br>" . drawButtons('OA');	
					
		$msg = "<div class='pubISIdata' id='pubManualInt-Oth'><ul>" . $msgOA . "</ul></div><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>"; // Content for Data
		echo $msg;
	} else {
		$msg = "<p>Não existem Other Abstracts a validar.</p>"; 
		echo $msg;	
	}
}

function loadTableL() {
	$db = new Database();
	$msgResto = "<thead><tr><th>IDINV</th><th>ISBN</th><th>Tipo Publicação</th><th>TÍTULO</th><th>AUTORES</th><th>EDITOR</th><th>EDITORA</th><th width='40'>LINK</th><th width='60'>ESTADO</th><th width='50'>Repetidos</th><th width='50'>Original</th><th width='30'></th><th width='30'></th></tr></thead><tbody>";
	$msgL ='';
	$page = $_POST['page'];
	$page -= 1;
	$per_page = 30;
	$start = $page * $per_page;
	$ids_final = '';
	$ids = array();
	$originais = array();		

	//Get 30 registos 
	$query_pag_data = "SELECT DISTINCT id_original from repetidos_pub WHERE tipofmup = 'L' LIMIT $start, $per_page";
	$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error() );
	while ($row = mysql_fetch_array($result_pag_data)) {
		$ids[]=$row['id_original'];
	}
	
	if (!empty($ids)) {
		$ids = array_unique($ids);

		foreach ($ids as $i => $value) {
			$ids_final .= $ids[$i] . ',';
			$query_pag_data = "SELECT id_repetido from repetidos_pub WHERE id_original = " . $ids[$i] . " AND tipofmup = 'L';";
			$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error());
			
			$originais[$i] = array();
			$originais[$i][] = $ids[$i];
			while ($row = mysql_fetch_array($result_pag_data)) {
				if($row['id_repetido'] != null)
					$originais[$i][]=$row['id_repetido'];
			}
		}
		
		foreach ($originais as $i => $value) {		
			foreach ($originais[$i] as $j => $value1) {		
				
				//Verifica se existe algum registo repetido
				if($j == 0)	{
					if (count($originais[$i])==1) {
						$style ="style=\"overflow:hidden;\"";	
					} else {		
						$style ="style=\"background-color:#99FFFF; overflow:hidden;\"";	
					}				
				} else {
					$style ="style=\"background-color:#FFFF33; overflow:hidden;\"";	
				}		
				
				$msgL .= "<tr>";
				
				$repetidos = $db->getRepetidosPublicacoes($originais[$i][$j]);
				$idInv = $db->getIdInvByIdPub($originais[$i][$j]);
				if($idInv) {
					if ($repetidos) {	
						$style ="style=\"background-color:#99FF66; overflow:hidden;\"";		
							$msgL .= "<td id='". $originais[$i][$j]. "' $style>".$idInv;							
						foreach ($repetidos as $x => $value2) {	
							if($idInv != $value2) {
								$msgL .= "," . $value2 ;				
							}								
						}
						$msgL .=  "</td>";
					} else {						
						$msgL .=  "<td id='". $originais[$i][$j]. "' $style>".$idInv."</td>";
					}			
					
					$query_pag_data = "SELECT * FROM publicacoes WHERE id =" .  $originais[$i][$j] . ";";	
					$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error() . " dentro do repetidos $query_pag_data");
					while ($row = mysql_fetch_array($result_pag_data)) {	
						if($row['INDEXED_ISI'] == 1 && $row['INDEXED_PUBMED'] == 0) {
							//ISI
							$msgL .= printISILiv($row,$style,'ISI','');		
						} else if ($row['INDEXED_ISI'] == 0 && $row['INDEXED_PUBMED'] == 1) {
							//PUBMED
							$msgL .= printISILiv($row,$style,'PUBMED','');	
						} else if ($row['TIPOFMUP'] == 'L'){
							//INTERNACIONAL
							$msgL .= printISILiv($row,$style,'outLMAN','Liv');	
						} 								
					}
				}
			}					
		}
		
		$msgL = "<table id='LISI' class='box-table-b'><caption>LIVROS</caption>" .$msgResto . $msgL . "</tbody></table><p id='chave-pubIntMan-Liv' hidden></p><br>" . drawButtons('L');	
					
		$msg = "<div class='pubISIdata' id='pubManualInt-Liv'><ul>" . $msgL . "</ul></div><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>"; // Content for Data
		echo $msg;
	} else {
		$msg = "<p>Não existem Livros a validar.</p>"; 
		echo $msg;	
	}
}

function loadTableCL() {
	$db = new Database();
	$msgResto = "<thead><tr><th>IDINV</th><th>ISBN</th><th>Tipo Publicação</th><th>TÍTULO DA OBRA</th><th>EDITORA</th><th>EDITORES</th><th>TÍTULO ARTIGO</th><th>AUTORES</th><th>PRIMEIRA PÁG.</th><th>ÚLTIMA PÁG.</th><th width='40'>LINK</th><th width='60'>ESTADO</th><th width='50'>Repetidos</th><th width='50'>Original</th><th width='30'></th><th width='30'></th></tr></thead><tbody>";
	$msgCL ='';
	$page = $_POST['page'];
	$page -= 1;
	$per_page = 30;
	$start = $page * $per_page;
	$ids_final = '';
	$ids = array();
	$originais = array();		

	//Get 30 registos 
	$query_pag_data = "SELECT DISTINCT id_original from repetidos_pub WHERE tipofmup = 'CL' LIMIT $start, $per_page";
	$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error() );
	while ($row = mysql_fetch_array($result_pag_data)) {
		$ids[]=$row['id_original'];
	}
	
	if (!empty($ids)) {
		$ids = array_unique($ids);

		foreach ($ids as $i => $value) {
			$ids_final .= $ids[$i] . ',';
			$query_pag_data = "SELECT id_repetido from repetidos_pub WHERE id_original = " . $ids[$i] . " AND tipofmup = 'CL';";
			$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error());
			
			$originais[$i] = array();
			$originais[$i][] = $ids[$i];
			while ($row = mysql_fetch_array($result_pag_data)) {
				if($row['id_repetido'] != null)
					$originais[$i][]=$row['id_repetido'];
			}
		}
		
		foreach ($originais as $i => $value) {		
			foreach ($originais[$i] as $j => $value1) {		
				
				//Verifica se existe algum registo repetido
				if($j == 0)	{
					if (count($originais[$i])==1) {
						$style ="style=\"overflow:hidden;\"";	
					} else {		
						$style ="style=\"background-color:#99FFFF; overflow:hidden;\"";	
					}				
				} else {
					$style ="style=\"background-color:#FFFF33; overflow:hidden;\"";	
				}		
				
				$msgCL .= "<tr>";
				
				$repetidos = $db->getRepetidosPublicacoes($originais[$i][$j]);
				$idInv = $db->getIdInvByIdPub($originais[$i][$j]);
				if($idInv) {
					if ($repetidos) {	
						$style ="style=\"background-color:#99FF66; overflow:hidden;\"";		
							$msgCL .= "<td id='". $originais[$i][$j]. "' $style>".$idInv;							
						foreach ($repetidos as $x => $value2) {	
							if($idInv != $value2) {
								$msgCL .= "," . $value2 ;				
							}								
						}
						$msgCL .=  "</td>";
					} else {						
						$msgCL .=  "<td id='". $originais[$i][$j]. "' $style>".$idInv."</td>";
					}			
					
					$query_pag_data = "SELECT * FROM publicacoes WHERE id =" .  $originais[$i][$j] . ";";	
					$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error() . " dentro do repetidos $query_pag_data");
					while ($row = mysql_fetch_array($result_pag_data)) {	
						if($row['INDEXED_ISI'] == 1 && $row['INDEXED_PUBMED'] == 0) {
							//ISI
							$msgCL .= printISICLiv($row,$style,'ISI','');		
						} else if ($row['INDEXED_ISI'] == 0 && $row['INDEXED_PUBMED'] == 1) {
							//PUBMED
							$msgCL .= printISICLiv($row,$style,'PUBMED','');	
						} else if ($row['TIPOFMUP'] == 'CL'){
							//INTERNACIONAL
							$msgCL .= printISICLiv($row,$style,'outCLMAN','CLiv');	
						} 								
					}
				}
			}					
		}
		
		$msgCL = "<table id='CLISI' class='box-table-b'><caption>CAPITULOS DE LIVROS</caption>" .$msgResto . $msgCL . "</tbody></table><p id='chave-pubIntMan-CLiv' hidden></p><br>" . drawButtons('CL');	
					
		$msg = "<div class='pubISIdata'  id='pubManualInt-CLiv'><ul>" . $msgCL . "</ul></div><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>"; // Content for Data
		echo $msg;
	} else {
		$msg = "<p>Não existem Capítulos de Livros a validar.</p>"; 
		echo $msg;	
	}
}

function loadTableS() {
	$db = new Database();
	$msgResto = "<thead><tr><th>ID</th><th>Ano</th><th>Tipo Publicação</th><th>Nome Publicação</th><th>ISSN</th><th>ISBN</th><th>Título</th><th>Autores</th><th>Volume</th><th>Issue</th><th>AR</th><th>Coleção</th><th>1ª Página</th><th>Últ. Página</th><th>Citações</th><th>Patentes</th><th>Data Patente</th><th>IPC</th><th>Estado</th><th width='30'>Repetidos</th><th width='30'>Original</th><th width='30'></th></tr></thead><tbody>";
	$msgS ='';	
	$page = $_POST['page'];
	$page -= 1;
	$per_page = 30;
	$start = $page * $per_page;
	$ids_final = '';
	$ids = array();
	$originais = array();		

	//Get 30 registos 
	$query_pag_data = "SELECT DISTINCT id_original from repetidos_pub WHERE tipofmup = 'S' LIMIT $start, $per_page";
	$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error() );
	while ($row = mysql_fetch_array($result_pag_data)) {
		$ids[]=$row['id_original'];
	}
	
	if (!empty($ids)) {
		$ids = array_unique($ids);

		foreach ($ids as $i => $value) {
			$ids_final .= $ids[$i] . ',';
			$query_pag_data = "SELECT id_repetido from repetidos_pub WHERE id_original = " . $ids[$i] . " AND tipofmup = 'S';";
			$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error());
			
			$originais[$i] = array();
			$originais[$i][] = $ids[$i];
			while ($row = mysql_fetch_array($result_pag_data)) {
				if($row['id_repetido'] != null)
					$originais[$i][]=$row['id_repetido'];
			}
		}
		
		foreach ($originais as $i => $value) {		
			foreach ($originais[$i] as $j => $value1) {		
				
				//Verifica se existe algum registo repetido
				if($j == 0)	{
					if (count($originais[$i])==1) {
						$style ="style=\"overflow:hidden;\"";	
					} else {		
						$style ="style=\"background-color:#99FFFF; overflow:hidden;\"";	
					}				
				} else {
					$style ="style=\"background-color:#FFFF33; overflow:hidden;\"";	
				}		
				
				$msgS .= "<tr>";
				
				$repetidos = $db->getRepetidosPublicacoes($originais[$i][$j]);
				$idInv = $db->getIdInvByIdPub($originais[$i][$j]);
				if($idInv) {
					if ($repetidos) {	
						$style ="style=\"background-color:#99FF66; overflow:hidden;\"";		
							$msgS .= "<td id='". $originais[$i][$j]. "' $style>".$idInv;							
						foreach ($repetidos as $x => $value2) {	
							if($idInv != $value2) {
								$msgS .= "," . $value2 ;				
							}								
						}
						$msgS .=  "</td>";
					} else {						
						$msgS .=  "<td id='". $originais[$i][$j]. "' $style>".$idInv."</td>";
					}			
					
					$query_pag_data = "SELECT * FROM publicacoes WHERE id =" .  $originais[$i][$j] . ";";	
					$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error() . " dentro do repetidos $query_pag_data");
					while ($row = mysql_fetch_array($result_pag_data)) {	
						if($row['INDEXED_ISI'] == 1 && $row['INDEXED_PUBMED'] == 0) {
							//ISI
							$msgS .= printISIResto($row,$style,'ISI');	
						} else if ($row['INDEXED_ISI'] == 0 && $row['INDEXED_PUBMED'] == 1) {
							//PUBMED
							$msgS .= printISIResto($row,$style,'PUBMED');	
						} 							
					}
				}
			}					
		}
		
		$msgS = "<table id='SISI' class='box-table-b'><caption>SUMÁRIOS</caption>" .$msgResto . $msgS . "</tbody></table><br>" . drawButtons('S');	
					
		$msg = "<div class='pubISIdata'><ul>" . $msgS . "</ul></div><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>"; // Content for Data
		echo $msg;
	} else {
		$msg = "<p>Não existem Sumários a validar.</p>"; 
		echo $msg;	
	}
}

function loadTableJN() {
	$db = new Database();
	$msgArt = "<thead class='header'><tr><th>IDINV</th><th>Tipo Publicação</th><th>ISSN</th><th>REVISTA</th><th>TÍTULO</th><th>AUTORES</th><th>VOLUME</th><th>ISSUE</th><th>PRIMEIRA PÁG.</th><th>ÚLTIMA PÁG.</th><th width='40'>LINK</th><th width='60'>ESTADO</th><th width='50'>Repetidos</th><th width='50'>Original</th><th width='30'></th><th width='30'></th></tr></thead><tbody>";
	$msgJN ='';	
	$page = $_POST['page'];
	$page -= 1;
	$per_page = 30;
	$start = $page * $per_page;
	$ids_final ='';
	$ids = array();
	$originais = array();		

	//Get 30 registos 
	$query_pag_data = "SELECT DISTINCT id_original from repetidos_pub WHERE tipofmup = 'JN' LIMIT $start, $per_page";
	$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error() );
	while ($row = mysql_fetch_array($result_pag_data)) {
		$ids[]=$row['id_original'];
	}
	
	if (!empty($ids)) {
		$ids = array_unique($ids);

		foreach ($ids as $i => $value) {
			$ids_final .= $ids[$i] . ',';
			$query_pag_data = "SELECT id_repetido from repetidos_pub WHERE id_original = " . $ids[$i] . " AND tipofmup = 'JN';";
			$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error());
			
			$originais[$i] = array();
			$originais[$i][] = $ids[$i];
			while ($row = mysql_fetch_array($result_pag_data)) {
				if($row['id_repetido'] != null)
					$originais[$i][]=$row['id_repetido'];
			}
		}
		
		foreach ($originais as $i => $value) {		
			foreach ($originais[$i] as $j => $value1) {		
				
				//Verifica se existe algum registo repetido
				if($j == 0)	{
					if (count($originais[$i])==1) {
						$style ="style=\"overflow:hidden;\"";	
					} else {		
						$style ="style=\"background-color:#99FFFF; overflow:hidden;\"";	
					}				
				} else {
					$style ="style=\"background-color:#FFFF33; overflow:hidden;\"";	
				}		
				
				$msgJN .= "<tr>";
				
				$repetidos = $db->getRepetidosPublicacoes($originais[$i][$j]);
				$idInv = $db->getIdInvByIdPub($originais[$i][$j]);
				if($idInv) {
					if ($repetidos) {	
						$style ="style=\"background-color:#99FF66; overflow:hidden;\"";		
							$msgJN .= "<td id='". $originais[$i][$j]. "' $style>".$idInv;							
						foreach ($repetidos as $x => $value2) {	
							if($idInv != $value2) {
								$msgJN .= "," . $value2 ;				
							}								
						}
						$msgJN .=  "</td>";
					} else {						
						$msgJN .=  "<td id='". $originais[$i][$j]. "' $style>".$idInv."</td>";
					}			
					
					$query_pag_data = "SELECT * FROM publicacoes WHERE id =" .  $originais[$i][$j] . ";";	
					$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error() . " dentro do repetidos $query_pag_data");
					while ($row = mysql_fetch_array($result_pag_data)) {	
						if($row['INDEXED_ISI'] == 1 && $row['INDEXED_PUBMED'] == 0) {
							//ISI
							$msgJN .= printISIArt($row,$style,'ISI','');		
						} else if ($row['INDEXED_ISI'] == 0 && $row['INDEXED_PUBMED'] == 1) {
							//PUBMED
							$msgJN .= printISIArt($row,$style,'PUBMED','');	
						} else if ($row['TIPOFMUP'] == 'JN'){
							//NACIONAL
							$msgJN .= printISIArt($row,$style,'outJNMAN','Art');	
						}								
					}
				}
			}					
		}
		
		$msgJN = "<table id='JNISI' class='box-table-b'><caption>ARTIGOS - Não Inglês</caption>" .$msgArt . $msgJN . "</tbody></table><p id='chave-pubNacMan-Art' hidden></p><br>" . drawButtons('JN');	
					
		$msg = "<div class='pubISIdata' id='pubManualNac-Art'><ul>" . $msgJN . "</ul></div><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>"; // Content for Data
		echo $msg;
	} else {
		$msg = "<p>Não existem Artigos - Não Inglês a validar.</p>"; 
		echo $msg;	
	}
}

function loadTablePRPN() {
	$db = new Database();
	$msgArt = "<thead class='header'><tr><th>IDINV</th><th>Tipo Publicação</th><th>ISSN</th><th>REVISTA</th><th>TÍTULO</th><th>AUTORES</th><th>VOLUME</th><th>ISSUE</th><th>PRIMEIRA PÁG.</th><th>ÚLTIMA PÁG.</th><th width='40'>LINK</th><th width='60'>ESTADO</th><th width='50'>Repetidos</th><th width='50'>Original</th><th width='30'></th><th width='30'></th></tr></thead><tbody>";
	$msgPRPN ='';	
	$page = $_POST['page'];
	$page -= 1;
	$per_page = 30;
	$start = $page * $per_page;
	$ids_final ='';
	$ids = array();
	$originais = array();		

	//Get 30 registos 
	$query_pag_data = "SELECT DISTINCT id_original from repetidos_pub WHERE tipofmup = 'PRPN' LIMIT $start, $per_page";
	$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error() );
	while ($row = mysql_fetch_array($result_pag_data)) {
		$ids[]=$row['id_original'];
	}
	
	if (!empty($ids)) {
		$ids = array_unique($ids);

		foreach ($ids as $i => $value) {
			$ids_final .= $ids[$i] . ',';
			$query_pag_data = "SELECT id_repetido from repetidos_pub WHERE id_original = " . $ids[$i] . " AND tipofmup = 'PRPN';";
			$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error());
			
			$originais[$i] = array();
			$originais[$i][] = $ids[$i];
			while ($row = mysql_fetch_array($result_pag_data)) {
				if($row['id_repetido'] != null)
					$originais[$i][]=$row['id_repetido'];
			}
		}
		
		foreach ($originais as $i => $value) {		
			foreach ($originais[$i] as $j => $value1) {		
				
				//Verifica se existe algum registo repetido
				if($j == 0)	{
					if (count($originais[$i])==1) {
						$style ="style=\"overflow:hidden;\"";	
					} else {		
						$style ="style=\"background-color:#99FFFF; overflow:hidden;\"";	
					}				
				} else {
					$style ="style=\"background-color:#FFFF33; overflow:hidden;\"";	
				}		
				
				$msgPRPN .= "<tr>";
				
				$repetidos = $db->getRepetidosPublicacoes($originais[$i][$j]);				
				$idInv = $db->getIdInvByIdPub($originais[$i][$j]);
				if($idInv) {
					if ($repetidos) {	
						$style ="style=\"background-color:#99FF66; overflow:hidden;\"";		
							$msgPRPN .= "<td id='". $originais[$i][$j]. "' $style>".$idInv;							
						foreach ($repetidos as $x => $value2) {	
							if($idInv != $value2) {
								$msgPRPN .= "," . $value2 ;				
							}								
						}
						$msgPRPN .=  "</td>";
					} else {						
						$msgPRPN .=  "<td id='". $originais[$i][$j]. "' $style>".$idInv."</td>";
					}			
					
					$query_pag_data = "SELECT * FROM publicacoes WHERE id =" .  $originais[$i][$j] . ";";	
					$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error() . " dentro do repetidos $query_pag_data");
					while ($row = mysql_fetch_array($result_pag_data)) {	
						if($row['INDEXED_ISI'] == 1 && $row['INDEXED_PUBMED'] == 0) {
							//ISI
							$msgPRPN .= printISIArt($row,$style,'ISI','');		
						} else if ($row['INDEXED_ISI'] == 0 && $row['INDEXED_PUBMED'] == 1) {
							//PUBMED
							$msgPRPN .= printISIArt($row,$style,'PUBMED','');							
						} else if ($row['TIPOFMUP'] == 'PRPN'){
							//NACIONAL
							$msgPRPN .= printISIArt($row,$style,'outPRPNMAN','Rev');	
						}					
					}
				}
			}					
		}
		
		$msgPRPN = "<table id='PRPNISI' class='box-table-b'><caption>PEER REVIEW PROCEEDINGS - NÃO INGLÊS</caption>" .$msgArt . $msgPRPN . "</tbody></table><p id='chave-pubNacMan-Rev' hidden></p><br>" . drawButtons('PRPN');
					
		$msg = "<div class='pubISIdata' id='pubManualNac-Rev'><ul>" . $msgPRPN . "</ul></div><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>"; // Content for Data
		echo $msg;
	} else {
		$msg = "<p>Não existem Peer Review Proceedings - Não Inglês a validar.</p>"; 
		echo $msg;	
	}
}

function loadTableSN() {
	$db = new Database();
	$msgResto = "<thead><tr><th>ID</th><th>Tipo Publicação</th><th>Ano</th><th>Nome Publicação</th><th>ISSN</th><th>ISBN</th><th>Título</th><th>Autores</th><th>Volume</th><th>Issue</th><th>AR</th><th>Coleção</th><th>1ª Página</th><th>Últ. Página</th><th>Citações</th><th>Patentes</th><th>Data Patente</th><th>IPC</th><th>Estado</th><th width='30'>Repetidos</th><th width='30'>Original</th><th width='30'></th></tr></thead><tbody>";
	$msgSN ='';	
	$page = $_POST['page'];
	$page -= 1;
	$per_page = 30;
	$start = $page * $per_page;
	$ids_final ='';
	$ids = array();
	$originais = array();		

	//Get 30 registos 
	$query_pag_data = "SELECT DISTINCT id_original from repetidos_pub WHERE tipofmup = 'SN' LIMIT $start, $per_page";
	$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error() );
	while ($row = mysql_fetch_array($result_pag_data)) {
		$ids[]=$row['id_original'];
	}
	
	if (!empty($ids)) {
		$ids = array_unique($ids);

		foreach ($ids as $i => $value) {
			$ids_final .= $ids[$i] . ',';
			$query_pag_data = "SELECT id_repetido from repetidos_pub WHERE id_original = " . $ids[$i] . " AND tipofmup = 'SN';";
			$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error());
			
			$originais[$i] = array();
			$originais[$i][] = $ids[$i];
			while ($row = mysql_fetch_array($result_pag_data)) {
				if($row['id_repetido'] != null)
					$originais[$i][]=$row['id_repetido'];
			}
		}
		
		foreach ($originais as $i => $value) {		
			foreach ($originais[$i] as $j => $value1) {		
				
				//Verifica se existe algum registo repetido
				if($j == 0)	{
					if (count($originais[$i])==1) {
						$style ="style=\"overflow:hidden;\"";	
					} else {		
						$style ="style=\"background-color:#99FFFF; overflow:hidden;\"";	
					}				
				} else {
					$style ="style=\"background-color:#FFFF33; overflow:hidden;\"";	
				}		
				
				$msgSN .= "<tr>";
				
				$repetidos = $db->getRepetidosPublicacoes($originais[$i][$j]);
				$idInv = $db->getIdInvByIdPub($originais[$i][$j]);
				if($idInv) {
					if ($repetidos) {	
						$style ="style=\"background-color:#99FF66; overflow:hidden;\"";		
							$msgSN .= "<td id='". $originais[$i][$j]. "' $style>".$idInv;							
						foreach ($repetidos as $x => $value2) {	
							if($idInv != $value2) {
								$msgSN .= "," . $value2 ;				
							}								
						}
						$msgSN .=  "</td>";
					} else {						
						$msgSN .=  "<td id='". $originais[$i][$j]. "' $style>".$idInv."</td>";
					}			
					
					$query_pag_data = "SELECT * FROM publicacoes WHERE id =" .  $originais[$i][$j] . ";";	
					$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error() . " dentro do repetidos $query_pag_data");
					while ($row = mysql_fetch_array($result_pag_data)) {	
						if($row['INDEXED_ISI'] == 1 && $row['INDEXED_PUBMED'] == 0) {
							//ISI
							$msgSN .= printISIResto($row,$style,'ISI');	
						} else if ($row['INDEXED_ISI'] == 0 && $row['INDEXED_PUBMED'] == 1) {
							//PUBMED
							$msgSN .= printISIResto($row,$style,'PUBMED');	
						} 								
					}
				}
			}					
		}
		
		$msgSN = "<table id='SNISI' class='box-table-b'><caption>SUMÁRIOS - NÃO INGLÊS</caption>" .$msgResto . $msgSN . "</tbody></table><br>" . drawButtons('SN');	
					
		$msg = "<div class='pubISIdata'><ul>" . $msgSN . "</ul></div><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>"; // Content for Data
		echo $msg;
	} else {
		$msg = "<p>Não existem Sumários - Não Inglês a validar.</p>"; 
		echo $msg;	
	}
}

function loadTableLN() {
	$db = new Database();
	$msgResto = "<thead><tr><th>IDINV</th><th>Tipo Publicação</th><th>ISBN</th><th>TÍTULO</th><th>AUTORES</th><th>EDITOR</th><th>EDITORA</th><th width='40'>LINK</th><th width='60'>ESTADO</th><th width='50'>Repetidos</th><th width='50'>Original</th><th width='30'></th><th width='30'></th></tr></thead><tbody>";
	$msgLN ='';	
	$page = $_POST['page'];
	$page -= 1;
	$per_page = 30;
	$start = $page * $per_page;
	$ids_final ='';
	$ids = array();
	$originais = array();		

	//Get 30 registos 
	$query_pag_data = "SELECT DISTINCT id_original from repetidos_pub WHERE tipofmup = 'LN' LIMIT $start, $per_page";
	$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error() );
	while ($row = mysql_fetch_array($result_pag_data)) {
		$ids[]=$row['id_original'];
	}
	
	if (!empty($ids)) {
		$ids = array_unique($ids);

		foreach ($ids as $i => $value) {
			$ids_final .= $ids[$i] . ',';
			$query_pag_data = "SELECT id_repetido from repetidos_pub WHERE id_original = " . $ids[$i] . " AND tipofmup = 'LN';";
			$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error());
			
			$originais[$i] = array();
			$originais[$i][] = $ids[$i];
			while ($row = mysql_fetch_array($result_pag_data)) {
				if($row['id_repetido'] != null)
					$originais[$i][]=$row['id_repetido'];
			}
		}
		
		foreach ($originais as $i => $value) {		
			foreach ($originais[$i] as $j => $value1) {		
				
				//Verifica se existe algum registo repetido
				if($j == 0)	{
					if (count($originais[$i])==1) {
						$style ="style=\"overflow:hidden;\"";	
					} else {		
						$style ="style=\"background-color:#99FFFF; overflow:hidden;\"";	
					}				
				} else {
					$style ="style=\"background-color:#FFFF33; overflow:hidden;\"";	
				}		
				
				$msgLN .= "<tr>";
				
				$repetidos = $db->getRepetidosPublicacoes($originais[$i][$j]);
				$idInv = $db->getIdInvByIdPub($originais[$i][$j]);
				if($idInv) {
					if ($repetidos) {	
						$style ="style=\"background-color:#99FF66; overflow:hidden;\"";		
							$msgLN .= "<td id='". $originais[$i][$j]. "' $style>".$idInv;							
						foreach ($repetidos as $x => $value2) {	
							if($idInv != $value2) {
								$msgLN .= "," . $value2 ;				
							}								
						}
						$msgLN .=  "</td>";
					} else {						
						$msgLN .=  "<td id='". $originais[$i][$j]. "' $style>".$idInv."</td>";
					}			
					
					$query_pag_data = "SELECT * FROM publicacoes WHERE id =" .  $originais[$i][$j] . ";";	
					$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error() . " dentro do repetidos $query_pag_data");
					while ($row = mysql_fetch_array($result_pag_data)) {
						if($row['INDEXED_ISI'] == 1 && $row['INDEXED_PUBMED'] == 0) {
							//ISI
							$msgLN .= printISILiv($row,$style,'ISI','');		
						} else if ($row['INDEXED_ISI'] == 0 && $row['INDEXED_PUBMED'] == 1) {
							//PUBMED
							$msgLN .= printISILiv($row,$style,'PUBMED','');							
						} else if ($row['TIPOFMUP'] == 'LN'){
							//NACIONAL
							$msgLN .= printISILiv($row,$style,'outLNMAN','Liv');	
						}		
					}
				}
			}					
		}
		
		$msgLN = "<table id='LNISI' class='box-table-b'><caption>LIVROS - NÃO INGLÊS</caption>" .$msgResto . $msgLN . "</tbody></table><p id='chave-pubNacMan-Liv' hidden></p><br>" . drawButtons('LN');
					
		$msg = "<div class='pubISIdata' id='pubManualNac-Liv'><ul>" . $msgLN . "</ul></div><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>"; // Content for Data
		echo $msg;
	} else {
		$msg = "<p>Não existem Livros - Não Inglês a validar.</p>"; 
		echo $msg;	
	}
}

function loadTableCLN() {
	$db = new Database();
	$msgResto = "<thead><tr><th>IDINV</th><th>Tipo Publicação</th><th>ISBN</th><th>TÍTULO DA OBRA</th><th>EDITORA</th><th>EDITORES</th><th>TÍTULO ARTIGO</th><th>AUTORES</th><th>PRIMEIRA PÁG.</th><th>ÚLTIMA PÁG.</th><th width='40'>LINK</th><th width='60'>ESTADO</th><th width='50'>Repetidos</th><th width='50'>Original</th><th width='30'></th><th width='30'></th></tr></thead><tbody>";
	$msgCLN ='';	
	$page = $_POST['page'];
	$page -= 1;
	$per_page = 30;
	$start = $page * $per_page;
	$ids_final ='';
	$ids = array();
	$originais = array();		

	//Get 30 registos 
	$query_pag_data = "SELECT DISTINCT id_original from repetidos_pub WHERE tipofmup = 'CLN' LIMIT $start, $per_page";
	$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error() );
	while ($row = mysql_fetch_array($result_pag_data)) {
		$ids[]=$row['id_original'];
	}
	
	if (!empty($ids)) {
		$ids = array_unique($ids);

		foreach ($ids as $i => $value) {
			$ids_final .= $ids[$i] . ',';
			$query_pag_data = "SELECT id_repetido from repetidos_pub WHERE id_original = " . $ids[$i] . " AND tipofmup = 'CLN';";
			$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error());
			
			$originais[$i] = array();
			$originais[$i][] = $ids[$i];
			while ($row = mysql_fetch_array($result_pag_data)) {
				if($row['id_repetido'] != null)
					$originais[$i][]=$row['id_repetido'];
			}
		}
		
		foreach ($originais as $i => $value) {		
			foreach ($originais[$i] as $j => $value1) {		
				
				//Verifica se existe algum registo repetido
				if($j == 0)	{
					if (count($originais[$i])==1) {
						$style ="style=\"overflow:hidden;\"";	
					} else {		
						$style ="style=\"background-color:#99FFFF; overflow:hidden;\"";	
					}				
				} else {
					$style ="style=\"background-color:#FFFF33; overflow:hidden;\"";	
				}		
				
				$msgCLN .= "<tr>";
				
				$repetidos = $db->getRepetidosPublicacoes($originais[$i][$j]);
				$idInv = $db->getIdInvByIdPub($originais[$i][$j]);
				if($idInv) {
					if ($repetidos) {	
						$style ="style=\"background-color:#99FF66; overflow:hidden;\"";		
							$msgCLN .= "<td id='". $originais[$i][$j]. "' $style>".$idInv;							
						foreach ($repetidos as $x => $value2) {	
							if($idInv != $value2) {
								$msgCLN .= "," . $value2 ;				
							}								
						}
						$msgCLN .=  "</td>";
					} else {						
						$msgCLN .=  "<td id='". $originais[$i][$j]. "' $style>".$idInv."</td>";
					}			
					
					$query_pag_data = "SELECT * FROM publicacoes WHERE id =" .  $originais[$i][$j] . ";";	
					$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error() . " dentro do repetidos $query_pag_data");
					while ($row = mysql_fetch_array($result_pag_data)) {	
						if($row['INDEXED_ISI'] == 1 && $row['INDEXED_PUBMED'] == 0) {
							//ISI
							$msgCLN .= printISICLiv($row,$style,'ISI','');		
						} else if ($row['INDEXED_ISI'] == 0 && $row['INDEXED_PUBMED'] == 1) {
							//PUBMED
							$msgCLN .= printISICLiv($row,$style,'PUBMED','');	
						} else if ($row['TIPOFMUP'] == 'CLN'){
							//INTERNACIONAL
							$msgCLN .= printISICLiv($row,$style,'outCLNMAN','CLiv');	
						} 									
					}
				}
			}					
		}
		
		$msgCLN = "<table id='CLNISI' class='box-table-b'><caption>CAPITULOS DE LIVROS - NÃO INGLÊS</caption>" .$msgResto . $msgCLN . "</tbody></table><p id='chave-pubNacMan-CLiv' hidden></p><br>" . drawButtons('CLN');
					
		$msg = "<div class='pubISIdata' id='pubManualNac-CLiv'><ul>" . $msgCLN . "</ul></div><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>"; // Content for Data
		echo $msg;
	} else {
		$msg = "<p>Não existem Capítulos de Livros - Não Inglês a validar.</p>"; 
		echo $msg;	
	}
}

function loadTableCPN() {
	$db = new Database();
	$msgConfP = "<thead class='header'><tr><th>IDINV</th><th>Tipo Publicação</th><th>ISBN</th><th>TÍTULO DA OBRA</th><th>EDITORES</th><th>TÍTULO ARTIGO</th><th>AUTORES</th><th>PRIMEIRA PÁG.</th><th>ÚLTIMA PÁG.</th><th width='40'>LINK</th><th width='60'>ESTADO</th><th width='50'>Repetidos</th><th width='50'>Original</th><th width='30'></th><th width='30'></th></tr></thead><tbody>";
	$msgCPN ='';	
	$page = $_POST['page'];
	$page -= 1;
	$per_page = 30;
	$start = $page * $per_page;
	$ids_final ='';
	$ids = array();
	$originais = array();		

	//Get 30 registos 
	$query_pag_data = "SELECT DISTINCT id_original from repetidos_pub WHERE tipofmup = 'CPN' LIMIT $start, $per_page";
	$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error() );
	while ($row = mysql_fetch_array($result_pag_data)) {
		$ids[]=$row['id_original'];
	}
	
	if (!empty($ids)) {
		$ids = array_unique($ids);

		foreach ($ids as $i => $value) {
			$ids_final .= $ids[$i] . ',';
			$query_pag_data = "SELECT id_repetido from repetidos_pub WHERE id_original = " . $ids[$i] . " AND ( (tipo = 'ISI/PUBMED' AND tipofmup = 'CPN') OR (tipo = 'NAC' AND tipofmup = 'CPN') );";
			$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error());
			
			$originais[$i] = array();
			$originais[$i][] = $ids[$i];
			while ($row = mysql_fetch_array($result_pag_data)) {
				if($row['id_repetido'] != null)
					$originais[$i][]=$row['id_repetido'];
			}
		}
		
		foreach ($originais as $i => $value) {		
			foreach ($originais[$i] as $j => $value1) {		
				
				//Verifica se existe algum registo repetido
				if($j == 0)	{
					if (count($originais[$i])==1) {
						$style ="style=\"overflow:hidden;\"";	
					} else {		
						$style ="style=\"background-color:#99FFFF; overflow:hidden;\"";	
					}				
				} else {
					$style ="style=\"background-color:#FFFF33; overflow:hidden;\"";	
				}		
				
				$msgCPN .= "<tr>";
				
				$repetidos = $db->getRepetidosPublicacoes($originais[$i][$j]);
				$idInv = $db->getIdInvByIdPub($originais[$i][$j]);
				if($idInv) {
					if ($repetidos) {	
						$style ="style=\"background-color:#99FF66; overflow:hidden;\"";		
							$msgCPN .= "<td id='". $originais[$i][$j]. "' $style>".$idInv;							
						foreach ($repetidos as $x => $value2) {	
							if($idInv != $value2) {
								$msgCPN .= "," . $value2 ;				
							}								
						}
						$msgCPN .=  "</td>";
					} else {						
						$msgCPN .=  "<td id='". $originais[$i][$j]. "' $style>".$idInv."</td>";
					}			
					
					$query_pag_data = "SELECT * FROM publicacoes WHERE id =" .  $originais[$i][$j] . ";";	
					$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error() . " dentro do repetidos $query_pag_data");
					while ($row = mysql_fetch_array($result_pag_data)) {
						if($row['INDEXED_ISI'] == 1 && $row['INDEXED_PUBMED'] == 0) {
							//ISI
							$msgCPN .= printISICP($row,$style,'ISI','');		
						} else if ($row['INDEXED_ISI'] == 0 && $row['INDEXED_PUBMED'] == 1) {
							//PUBMED
							$msgCPN .= printISICP($row,$style,'PUBMED','');							
						} else if ($row['TIPOFMUP'] == 'CPN'){
							//NACIONAL
							$msgCPN .= printISICP($row,$style,'outCPNMAN','Con');	
						}	
					}
				}
			}					
		}
		
		$msgCPN = "<table id='CPNISI' class='box-table-b'><caption>CONFERENCE PROCEEDINGS - NÃO INGLÊS</caption>" .$msgConfP . $msgCPN . "</tbody></table><p id='chave-pubNacMan-Con' hidden></p><br>" . drawButtons('CPN');	
					
		$msg = "<div class='pubISIdata' id='pubManualNac-Con'><ul>" . $msgCPN . "</ul></div><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>"; // Content for Data
		echo $msg;
	} else {
		$msg = "<p>Não existem Conference Proceedings - Não Inglês a validar.</p>"; 
		echo $msg;	
	}
}

function loadTableP() {
	$db = new Database();
	$msgResto = "<thead><tr><th>ID</th><th>Tipo Publicação</th><th>Ano</th><th>Nome Publicação</th><th>ISSN</th><th>ISBN</th><th>Título</th><th>Autores</th><th>Volume</th><th>Issue</th><th>AR</th><th>Coleção</th><th>1ª Página</th><th>Últ. Página</th><th>Citações</th><th>Patentes</th><th>Data Patente</th><th>IPC</th><th>Estado</th><th width='30'>Repetidos</th><th width='30'>Original</th><th width='30'></th></tr></thead><tbody>";
	$msgP ='';	
	$page = $_POST['page'];
	$page -= 1;
	$per_page = 30;
	$start = $page * $per_page;
	$ids_final ='';
	$ids = array();
	$ids_final = '';
	$originais = array();		

	//Get 30 registos 
	$query_pag_data = "SELECT DISTINCT id_original from repetidos_pub WHERE tipofmup = 'P' AND tipo IS NULL LIMIT $start, $per_page";
	$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error() );
	while ($row = mysql_fetch_array($result_pag_data)) {
		$ids[]=$row['id_original'];
	}
	
	if (!empty($ids)) {
		$ids = array_unique($ids);

		foreach ($ids as $i => $value) {
			$ids_final .= $ids[$i] . ',';		
			$query_pag_data = "SELECT id_repetido from repetidos_pub WHERE id_original = " . $ids[$i] . " AND	tipofmup = 'P';";
			$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error());
			
			$originais[$i] = array();
			$originais[$i][] = $ids[$i];
			while ($row = mysql_fetch_array($result_pag_data)) {
				if($row['id_repetido'] != null)
					$originais[$i][]=$row['id_repetido'];
			}
		}
		
		foreach ($originais as $i => $value) {		
			foreach ($originais[$i] as $j => $value1) {		
				
				//Verifica se existe algum registo repetido
				if($j == 0)	{
					if (count($originais[$i])==1) {
						$style ="style=\"overflow:hidden;\"";	
					} else {		
						$style ="style=\"background-color:#99FFFF; overflow:hidden;\"";	
					}				
				} else {
					$style ="style=\"background-color:#FFFF33; overflow:hidden;\"";	
				}		
				
				$msgP .= "<tr>";
				
				$repetidos = $db->getRepetidosPublicacoes($originais[$i][$j]);
				$idInv = $db->getIdInvByIdPub($originais[$i][$j]);
				if($idInv) {
					if ($repetidos) {	
						$style ="style=\"background-color:#99FF66; overflow:hidden;\"";		
							$msgP .= "<td id='". $originais[$i][$j]. "' $style>".$idInv;							
						foreach ($repetidos as $x => $value2) {	
							if($idInv != $value2) {
								$msgP .= "," . $value2 ;				
							}								
						}
						$msgP .=  "</td>";
					} else {						
						$msgP .=  "<td id='". $originais[$i][$j]. "' $style>".$idInv."</td>";
					}			
					
					$query_pag_data = "SELECT * FROM publicacoes WHERE id =" .  $originais[$i][$j] . ";";	
					$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error() . " dentro do repetidos $query_pag_data");
					while ($row = mysql_fetch_array($result_pag_data)) {	
						if($row['INDEXED_ISI'] == 1 && $row['INDEXED_PUBMED'] == 0) {
							//ISI
							$msgP .= printISIResto($row,$style,'ISI');	
						} else if ($row['INDEXED_ISI'] == 0 && $row['INDEXED_PUBMED'] == 1) {
							//PUBMED
							$msgP .= printISIResto($row,$style,'PUBMED');	
						}  else  {
							$msgP .= printISIResto($row,$style,'');	
						} 						
					}
				}
			}					
		}
		
		$msgP = "<table id='PISI' class='box-table-b'><caption>PATENTES</caption>" .$msgResto . $msgP . "</tbody></table><br>" . drawButtons('P');	
					
		$msg = "<div class='pubISIdata'><ul>" . $msgP . "</ul></div><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>"; // Content for Data
		echo $msg;
	} else {
		$msg = "<p>Não existem Patentes a validar.</p>"; 
		echo $msg;	
	}
}

function loadTableA() {
	$db = new Database();
	$msgResto = "<thead><tr><th>ID</th><th>Tipo Publicação</th><th>Ano</th><th>Nome Publicação</th><th>ISSN</th><th>ISBN</th><th>Título</th><th>Autores</th><th>Volume</th><th>Issue</th><th>AR</th><th>Coleção</th><th>1ª Página</th><th>Últ. Página</th><th>Citações</th><th>Patentes</th><th>Data Patente</th><th>IPC</th><th>Estado</th><th width='30'>Repetidos</th><th width='30'>Original</th><th width='30'></th></tr></thead><tbody>";
	$msgA ='';	
	$page = $_POST['page'];
	$page -= 1;
	$per_page = 30;
	$start = $page * $per_page;
	$ids_final ='';
	$ids = array();
	$originais = array();		

	//Get 30 registos 
	$query_pag_data = "SELECT DISTINCT id_original from repetidos_pub WHERE tipofmup = 'A' LIMIT $start, $per_page";
	$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error() );
	while ($row = mysql_fetch_array($result_pag_data)) {
		$ids[]=$row['id_original'];
	}
	
	if (!empty($ids)) {
		$ids = array_unique($ids);

		foreach ($ids as $i => $value) {
			$ids_final .= $ids[$i] . ',';
			$query_pag_data = "SELECT id_repetido from repetidos_pub WHERE id_original = " . $ids[$i] . " AND tipofmup = 'A';";
			$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error());
			
			$originais[$i] = array();
			$originais[$i][] = $ids[$i];
			while ($row = mysql_fetch_array($result_pag_data)) {
				if($row['id_repetido'] != null)
					$originais[$i][]=$row['id_repetido'];
			}
		}
		
		foreach ($originais as $i => $value) {		
			foreach ($originais[$i] as $j => $value1) {		
				
				//Verifica se existe algum registo repetido
				if($j == 0)	{
					if (count($originais[$i])==1) {
						$style ="style=\"overflow:hidden;\"";	
					} else {		
						$style ="style=\"background-color:#99FFFF; overflow:hidden;\"";	
					}				
				} else {
					$style ="style=\"background-color:#FFFF33; overflow:hidden;\"";	
				}		
				
				$msgA .= "<tr>";
				
				$repetidos = $db->getRepetidosPublicacoes($originais[$i][$j]);
				$idInv = $db->getIdInvByIdPub($originais[$i][$j]);
				if($idInv) {
					if ($repetidos) {	
						$style ="style=\"background-color:#99FF66; overflow:hidden;\"";		
							$msgA .= "<td id='". $originais[$i][$j]. "' $style>".$idInv;							
						foreach ($repetidos as $x => $value2) {	
							if($idInv != $value2) {
								$msgA .= "," . $value2 ;				
							}								
						}
						$msgA .=  "</td>";
					} else {						
						$msgA .=  "<td id='". $originais[$i][$j]. "' $style>".$idInv."</td>";
					}			
					
					$query_pag_data = "SELECT * FROM publicacoes WHERE id =" .  $originais[$i][$j] . ";";	
					$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error() . " dentro do repetidos $query_pag_data");
					while ($row = mysql_fetch_array($result_pag_data)) {	
						if($row['INDEXED_ISI'] == 1 && $row['INDEXED_PUBMED'] == 0) {
							//ISI
							$msgA .= printISIResto($row,$style,'ISI');	
						} else if ($row['INDEXED_ISI'] == 0 && $row['INDEXED_PUBMED'] == 1) {
							//PUBMED
							$msgA .= printISIResto($row,$style,'PUBMED');	
						}							
					}
				}
			}					
		}
		
		$msgA = "<table id='AISI' class='box-table-b'><caption>AGRADECIMENTOS</caption>" .$msgResto . $msgA . "</tbody></table><br>" . drawButtons('A');		
			
		$msg = "<div class='pubISIdata'><ul>" . $msgA . "</ul></div><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>"; // Content for Data
		echo $msg;
	} else {
		$msg = "<p>Não existem Agradecimentos a validar.</p>"; 
		echo $msg;	
	}
}

function drawButtons($tipofmup) {
	$db = new Database();
	$db->connect();
	$page = $_POST['page'];
	$cur_page = $page;
	$page -= 1;
	$per_page = 30;
	$previous_btn = true;
	$next_btn = true;
	$first_btn = true;
	$last_btn = true;
	$start = $page * $per_page;
	$msg = '';
	/* --------------------------------------------- */
	$query_pag_num = "SELECT COUNT(*) AS count FROM repetidos_pub WHERE tipofmup = '" . $tipofmup."';";
	$result_pag_num = mysql_query($query_pag_num,$db->conn);
	$row = mysql_fetch_array($result_pag_num);
	$count = $row['count'];
	$no_of_paginations = ceil($count / $per_page);

	/* ---------------Calculating the starting and endign values for the loop----------------------------------- */
	if ($cur_page >= 7) {
		$start_loop = $cur_page - 3;
		if ($no_of_paginations > $cur_page + 3)
			$end_loop = $cur_page + 3;
		else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
			$start_loop = $no_of_paginations - 6;
			$end_loop = $no_of_paginations;
		} else {
			$end_loop = $no_of_paginations;
		}
	} else {
		$start_loop = 1;
		if ($no_of_paginations > 7)
			$end_loop = 7;
		else
			$end_loop = $no_of_paginations;
	}
	/* ----------------------------------------------------------------------------------------------------------- */
	$msg .= "<div class='pubISI_" . $tipofmup . "_pagination'><ul>";

	for ($i = $start_loop; $i <= $end_loop; $i++) {
		if ($cur_page == $i)
			$msg .= "<li p='$i' style='color:#fff;background-color:#006699;' class='active' onclick='clickNum" . $tipofmup . "ISI(this);'>{$i}</li>";
		else
			$msg .= "<li p='$i' class='active' onclick='clickNum" . $tipofmup . "ISI(this);'>{$i}</li>";
	}
	
	$goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn_pub_" . $tipofmup . "_ISI' class='go_button' value='Procurar' onclick='procura" . $tipofmup . "ISI();'/>&nbsp;";
	$total_string = "<span class='total' a='$no_of_paginations'>P&aacutegina <b>" . $cur_page . "</b> de <b>$no_of_paginations</b></span>";
	$msg = $msg . "</ul>" . $goto . $total_string . "</div>";  // Content for pagination
	
	return $msg;
}

function printISIArt($row,$style,$tipo,$tipo1) {
	$msg = '';
	$titulo=htmlentities(utf8_decode($row['TITULO']));
	$nomepublicacao=htmlentities($row['NOMEPUBLICACAO']);
	$issn=htmlentities($row['ISSN']);
	$autores=htmlentities(utf8_decode($row['AUTORES']));
	$volume=htmlentities($row['VOLUME']);
	$issue=htmlentities($row['ISSUE']);
	$primpagina=$row['PRIMPAGINA'];
	$ultpagina=$row['ULTPAGINA'];
	$link=htmlentities($row['LINK']);
	$estado=$row['ESTADO'];		
	$id=$row['ID'];			
	 
	if($tipo == 'ISI') {
		$msg .= "<td $style>ISI</td>";		
	} else if ($tipo == 'PUBMED') {	
		$msg .= "<td $style>PUBMED</td>";
	} else if($row['TIPOFMUP'] === 'J' || $row['TIPOFMUP'] === 'JA' || $row['TIPOFMUP'] === 'PRP') {	
		$msg .= "<td $style>Manual Internacional</td>";			
	} else if($row['TIPOFMUP'] === 'JN' || $row['TIPOFMUP'] === 'JAN' || $row['TIPOFMUP'] === 'PRPN') {		
		$msg .= "<td $style>Manual Nacional</td>";					
	} 
					
	$msg .= "<td id='td_".$tipo."_issn_" . $id . "' $style>".$issn."</td>";			
	$msg .= "<td id='td_".$tipo."_nomepub_" . $id . "' $style>".$nomepublicacao."</td>";
	$msg .= "<td id='td_".$tipo."_titulo_" . $id . "' $style>".$titulo."</td>";
	$msg .= "<td id='td_".$tipo."_autores_" . $id . "' $style>".$autores."</td>";
	$msg .= "<td id='td_".$tipo."_volume_" . $id . "' $style>".$volume."</td>";
	$msg .= "<td id='td_".$tipo."_issue_" . $id . "' $style>".$issue."</td>";
	$msg .= "<td id='td_".$tipo."_prim_" . $id . "' $style>".$primpagina."</td>";
	$msg .= "<td id='td_".$tipo."_ult_" . $id . "' $style>".$ultpagina."</td>";
	$msg .= "<td id='td_".$tipo."_link_" . $id . "' $style><a href='".$link."' target='_blank'>".$link."</a></td>";
	$msg .= "<td id='td_".$tipo."_estado_" . $id . "' $style>".getEstadoPublicacaoMI($estado)."</td>";	
	$msg .=  "<td><center><input type='checkbox' id='rep_" . $id . "' onclick='addRepetido(" . $id . ");'></center></td>";
	$msg .=  "<td><center><input type='checkbox' id='orig_" . $id ."' onclick='addOriginal(" . $id . ");'></center></td>";	
	
	if($tipo != '') {
		if($tipo == 'ISI') {
			$msg .= "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $id . "');$('#tabela').text('9');apagarPublicacao();return false;\" ></center></td>";
		} else if ($tipo == 'PUBMED') {
			$msg .= "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $id . "');$('#tabela').text('10');apagarPublicacao();return false;\" ></center></td>";
		} else if($row['TIPOFMUP'] === 'J' ) {	
			$msg .= "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubIntMan-".$tipo1."').text('" . $id . "');$('#tabela').text('12');editArt();\"></td>";	
			$msg .= "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $id . "');$('#tabela').text('12');apagarPublicacao();return false;\" ></center></td>";
		} else if($row['TIPOFMUP'] === 'JN') {		
			$msg .= "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubNacMan-".$tipo1."').text('" . $id . "');$('#tabela').text('13');editManArt();\"></td>";
			$msg .= "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $id . "');$('#tabela').text('13');apagarPublicacao();return false;\" ></center></td>";
		} else if($row['TIPOFMUP'] === 'JA') {		
			$msg .= "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubIntMan-".$tipo1."').text('" . $id . "');$('#tabela').text('12');editSum();\"></td>";
			$msg .= "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $id . "');$('#tabela').text('12');apagarPublicacao();return false;\" ></center></td>";
		} else if($row['TIPOFMUP'] === 'JAN') {		
			$msg .= "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubNacMan-".$tipo1."').text('" . $id . "');$('#tabela').text('13');editManSum();\"></td>";
			$msg .= "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $id . "');$('#tabela').text('13');apagarPublicacao();return false;\" ></center></td>";
		} else if($row['TIPOFMUP'] === 'PRP') {		
			$msg .= "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubIntMan-PRP').text('" . $id . "');$('#tabela').text('12');editPRP();\"></td>";
			$msg .= "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $id . "');$('#tabela').text('12');apagarPublicacao();return false;\" ></center></td>";
		} else if($row['TIPOFMUP'] === 'PRPN') {		
			$msg .= "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubNacMan-Rev').text('" . $id . "');$('#tabela').text('13');editManPRP();\"></td>";
			$msg .= "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $id . "');$('#tabela').text('13');apagarPublicacao();return false;\" ></center></td>";
		}  
	}
			
	$msg .= "</tr>";
	
	return $msg;
}
	
function printISICP($row,$style, $tipo, $tipo1) {
	$msg = '';	
	
	$idinv=$row['IDINV'];	
	$isbn=htmlentities(utf8_decode($row['ISBN']));
	$nomepublicacao=htmlentities(utf8_decode($row['NOMEPUBLICACAO']));	
	$editor=htmlentities(utf8_decode($row['EDITOR']));
	$titulo=htmlentities(utf8_decode($row['TITULO']));	
	$autores=htmlentities(utf8_decode($row['AUTORES']));
	$primpagina=$row['PRIMPAGINA'];
	$ultpagina=$row['ULTPAGINA'];
	$link=htmlentities(utf8_decode($row['LINK']));
	$estado=$row['ESTADO'];	
	$id=$row['ID'];									
						
	if($tipo == 'ISI') {
		$msg .= "<td $style>ISI</td>";		
	} else if ($tipo == 'PUBMED') {	
		$msg .= "<td $style>PUBMED</td>";
	} else if($row['TIPOFMUP'] === 'CP' || $row['TIPOFMUP'] === 'OA') {			
		$msg .= "<td $style>Manual Internacional</td>";
	} else if($row['TIPOFMUP'] === 'CPN' || $row['TIPOFMUP'] === 'OAN' ) {		
		$msg .= "<td $style>Manual Nacional</td>";
			
	} 
						
	$msg .= "<td id='td_".$tipo."_isbn_" . $id . "' $style>".$isbn."</td>";
	$msg .= "<td id='td_".$tipo."_nomepub_" . $id . "' $style>".$nomepublicacao."</td>";
	$msg .= "<td id='td_".$tipo."_editor_" . $id . "' $style>".$editor."</td>";
	$msg .= "<td id='td_".$tipo."_titulo_" . $id . "' $style>".$titulo."</td>";
	$msg .= "<td id='td_".$tipo."_autores_" . $id . "' $style>".$autores."</td>";
	$msg .= "<td id='td_".$tipo."_prim_" . $id . "' $style>".$primpagina."</td>";
	$msg .= "<td id='td_".$tipo."_ult_" . $id . "' $style>".$ultpagina."</td>";
	$msg .= "<td id='td_".$tipo."_link_" . $id . "' $style><a href='".$link."' target='_blank'>".$link."</a></td>";
	$msg .= "<td id='td_".$tipo."_estado_" . $id . "' $style>".getEstadoPublicacaoMI($estado)."</td>";			
	$msg .=  "<td><center><input type='checkbox' id='rep_" . $id . "' onclick='addRepetido(" . $id . ");'></center></td>";
	$msg .=  "<td><center><input type='checkbox' id='orig_" . $id ."' onclick='addOriginal(" . $id . ");'></center></td>";	
	
	if($tipo != '') {
		if($tipo == 'ISI') {
			$msg .= "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $id . "');$('#tabela').text('9');apagarPublicacao();return false;\" ></center></td>";
		} else if ($tipo == 'PUBMED') {
			$msg .= "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $id . "');$('#tabela').text('10');apagarPublicacao();return false;\" ></center></td>";
		} else if($row['TIPOFMUP'] === 'CP' ) {	
			$msg .= "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubIntMan-".$tipo1."').text('" . $id . "');$('#tabela').text('12');editCP();\"></td>";	
			$msg .= "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $id . "');$('#tabela').text('12');apagarPublicacao();return false;\" ></center></td>";
		} else if($row['TIPOFMUP'] === 'OA' ) {	
			$msg .= "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubIntMan-".$tipo1."').text('" . $id . "');$('#tabela').text('12');editOA();\"></td>";	
			$msg .= "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $id . "');$('#tabela').text('12');apagarPublicacao();return false;\" ></center></td>";
		} else if($row['TIPOFMUP'] === 'CPN' ) {	
			$msg .= "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubNacMan-".$tipo1."').text('" . $id . "');$('#tabela').text('13');editManCP();\"></td>";	
			$msg .= "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $id . "');$('#tabela').text('13');apagarPublicacao();return false;\" ></center></td>";
		} else if($row['TIPOFMUP'] === 'OAN' ) {	
			$msg .= "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubNacMan-".$tipo1."').text('" . $id . "');$('#tabela').text('13');editManOA();\"></td>";	
			$msg .= "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $id . "');$('#tabela').text('13');apagarPublicacao();return false;\" ></center></td>";
		}  
	}
	
	$msg .= "</tr>";
	return $msg;
}

function printISILiv($row,$style, $tipo, $tipo1) {
	$msg = '';	
	$isbn=htmlentities($row['ISBN']);
	$titulo=htmlentities(utf8_decode($row['TITULO']));
	$autores=htmlentities(utf8_decode($row['AUTORES']));
	$editor=htmlentities($row['EDITOR']);
	$editora=htmlentities(utf8_decode($row['EDITORA']));	
	$link=htmlentities($row['LINK']);	
	$estado=$row['ESTADO'];			
	$id=$row['ID'];	
	
	if($tipo == 'ISI') {
		$msg .= "<td $style>ISI</td>";		
	} else if ($tipo == 'PUBMED') {	
		$msg .= "<td $style>PUBMED</td>";
	} else if($row['TIPOFMUP'] === 'L' ){
		$msg .= "<td $style>Manual Internacional</td>";
	} else if($row['TIPOFMUP'] === 'LN' ){
		$msg .= "<td $style>Manual Nacional</td>";
	}	
	
	$msg .= "<td id='td_".$tipo."_isbn_" . $id . "' $style>".$isbn."</td>";
	$msg .= "<td id='td_".$tipo."_titulo_" . $id . "' $style>".$titulo."</td>";
	$msg .= "<td id='td_".$tipo."_autores_" . $id . "' $style>".$autores."</td>";
	$msg .= "<td id='td_".$tipo."_editor_" . $id . "' $style>".$editor."</td>";
	$msg .= "<td id='td_".$tipo."_editora_" . $id . "' $style>".$editora."</td>";
	$msg .= "<td id='td_".$tipo."_link_" . $id . "' $style><a href='".$link."' target='_blank'>".$link."</a></td>";
	$msg .= "<td id='td_".$tipo."_estado_" . $id . "' $style>".getEstadoPublicacaoMI($estado)."</td>";							
	$msg .=  "<td><center><input type='checkbox' id='rep_" . $id . "' onclick='addRepetido(" . $id . ");'></center></td>";
	$msg .=  "<td><center><input type='checkbox' id='orig_" . $id ."' onclick='addOriginal(" . $id . ");'></center></td>";	
	
	if($tipo != '') {
		if($tipo == 'ISI') {
			$msg .= "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $id . "');$('#tabela').text('9');apagarPublicacao();return false;\" ></center></td>";
		} else if ($tipo == 'PUBMED') {
			$msg .= "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $id . "');$('#tabela').text('10');apagarPublicacao();return false;\" ></center></td>";
		} else if($row['TIPOFMUP'] === 'L' ) {	
			$msg .= "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubIntMan-".$tipo1."').text('" . $id . "');$('#tabela').text('12');editL();\"></td>";	
			$msg .= "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $id . "');$('#tabela').text('12');apagarPublicacao();return false;\" ></center></td>";
		} else if($row['TIPOFMUP'] === 'LN' ) {	
			$msg .= "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubNacMan-".$tipo1."').text('" . $id . "');$('#tabela').text('13');editManL();\"></td>";	
			$msg .= "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $id . "');$('#tabela').text('13');apagarPublicacao();return false;\" ></center></td>";
		} 
	}
	
	$msg .= "</tr>";
	
	return $msg;
}

function printISICLiv($row,$style, $tipo, $tipo1) {
	$msg = '';	
	$isbn=htmlentities($row['ISBN']);
	$nomepublicacao=htmlentities(utf8_decode($row['NOMEPUBLICACAO']));
	$editor=htmlentities($row['EDITOR']);
	$editora=htmlentities(utf8_decode($row['EDITORA']));	
	$titulo=htmlentities(utf8_decode($row['TITULO']));
	$autores=htmlentities(utf8_decode($row['AUTORES']));
	$primpagina=$row['PRIMPAGINA'];
	$ultpagina=$row['ULTPAGINA'];
	$link=htmlentities($row['LINK']);	
	$estado=$row['ESTADO'];			
	$id=$row['ID'];		
	
	if($tipo == 'ISI') {
		$msg .= "<td $style>ISI</td>";		
	} else if ($tipo == 'PUBMED') {	
		$msg .= "<td $style>PUBMED</td>";
	} else if($row['TIPOFMUP'] === 'CL' ) {	
		$msg .= "<td $style>Manual Internacional</td>";
	} else if($row['TIPOFMUP'] === 'CLN' ) {	
		$msg .= "<td $style>Manual Nacional</td>";
	}
	
	$msg .= "<td id='td_".$tipo."_isbn_" . $id . "' $style>".$isbn."</td>";
	$msg .= "<td id='td_".$tipo."_nomepub_" . $id . "' $style>".$nomepublicacao."</td>";
	$msg .= "<td id='td_".$tipo."_editora_" . $id . "' $style>".$editora."</td>";
	$msg .= "<td id='td_".$tipo."_editor_" . $id . "' $style>".$editor."</td>";						
	$msg .= "<td id='td_".$tipo."_titulo_" . $id . "' $style>".$titulo."</td>";
	$msg .= "<td id='td_".$tipo."_autores_" . $id . "' $style>".$autores."</td>";
	$msg .= "<td id='td_".$tipo."_prim_" . $id . "' $style>".$primpagina."</td>";
	$msg .= "<td id='td_".$tipo."_ult_" . $id . "' $style>".$ultpagina."</td>";
	$msg .= "<td id='td_".$tipo."_link_" . $id . "' $style><a href='".$link."' target='_blank'>".$link."</a></td>";
	$msg .= "<td id='td_".$tipo."_estado_" . $id . "' $style>".getEstadoPublicacaoMI($estado)."</td>";			
	$msg .=  "<td><center><input type='checkbox' id='rep_" . $id . "' onclick='addRepetido(" . $id . ");'></center></td>";
	$msg .=  "<td><center><input type='checkbox' id='orig_" . $id ."' onclick='addOriginal(" . $id . ");'></center></td>";					  

	if($tipo != '') {
		 if($tipo == 'ISI') {
			$msg .= "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $id . "');$('#tabela').text('9');apagarPublicacao();return false;\" ></center></td>";
		} else if ($tipo == 'PUBMED') {
			$msg .= "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $id . "');$('#tabela').text('10');apagarPublicacao();return false;\" ></center></td>";
		} else if($row['TIPOFMUP'] === 'CL' ) {	
			$msg .= "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubIntMan-".$tipo1."').text('" . $id . "');$('#tabela').text('12');editCL();\"></td>";	
			$msg .= "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $id . "');$('#tabela').text('12');apagarPublicacao();return false;\" ></center></td>";
		} else if($row['TIPOFMUP'] === 'CLN' ) {	
			$msg .= "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubNacMan-".$tipo1."').text('" . $id . "');$('#tabela').text('13');editManCL();\"></td>";	
			$msg .= "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $id . "');$('#tabela').text('13');apagarPublicacao();return false;\" ></center></td>";
		} 
	}

	$msg .= "</tr>";
	
	return $msg;
}

function printISIResto($row,$style, $tipo) {
	$msg = '';	
	$isbn=htmlentities($row['ISBN']);
	$ano=htmlentities($row['ANO']);
	$nomepublicacao=htmlentities(utf8_decode($row['NOMEPUBLICACAO']));	
	$issn=htmlentities($row['ISSN']);
	$isbn=htmlentities($row['ISBN']);
	$titulo=htmlentities(utf8_decode($row['TITULO']));	
	$autores=htmlentities(utf8_decode($row['AUTORES']));	
	$volume=htmlentities($row['VOLUME']);
	$issue=htmlentities($row['ISSUE']);	
	$ar=htmlentities($row['AR']);	
	$colecao=htmlentities(utf8_decode($row['COLECAO']));
	$primpagina=$row['PRIMPAGINA'];
	$ultpagina=$row['ULTPAGINA'];
	$citacoes=htmlentities(utf8_decode($row['CITACOES']));
	$npatente=$row['NPATENTE'];
	$datapatente=$row['DATAPATENTE'];		
	$ipc=$row['IPC'];				
	$estado=$row['ESTADO'];			
	$id=$row['ID'];									
	
	if($estado==0)
		$estado="Publicado/Published";
	else
		$estado="Em Revisão ou no Prelo/In Review or In Press";						
			
	if($tipo == 'ISI') {
		$msg .= "<td $style>ISI</td>";		
	} else if ($tipo == 'PUBMED') {	
		$msg .= "<td $style>PUBMED</td>";
	} 			
	
	$msg .="<td $style>";
	$msg .= $ano;
	$msg .="</td>";			
	
	$msg .="<td $style>";
	$msg .= $nomepublicacao;
	$msg .="</td>";
	
	$msg .= "<td $style>";
	$msg .= $issn;
	$msg .= "</td>";

	$msg .= "<td $style>";
	$msg .= $isbn;
	$msg .= "</td>";	
	
	$msg .= "<td $style>";
	$msg .= $titulo;
	$msg .= "</td>";
	
	$msg .= "<td $style>";
	$msg .= $autores;
	$msg .= "</td>";

	$msg .= "<td $style>";
	$msg .= $volume;
	$msg .= "</td>";

	
	$msg .= "<td $style>";
	$msg .= $issue;
	$msg .= "</td>";

	$msg .= "<td $style>";
	$msg .= $ar;
	$msg .= "</td>";

	$msg .= "<td $style>";
	$msg .= $colecao;
	$msg .= "</td>";	

	$msg .= "<td $style>";
	$msg .= $primpagina;
	$msg .= "</td>";

	$msg .= "<td $style>";
	$msg .= $ultpagina;
	$msg .= "</td>";	
		
	$msg .= "<td $style>";
	$msg .= $citacoes;
	$msg .= "</td>";

	$msg .= "<td $style>";
	$msg .= $npatente;
	$msg .= "</td>";	

	$msg .= "<td $style>";
	$msg .= $datapatente;
	$msg .= "</td>";
	
	$msg .= "<td $style>";
	$msg .= $ipc;
	$msg .= "</td>";

	$msg .= "<td $style>";
	$msg .= $estado;
	$msg .= "</td>";
	
	$msg .= "<td><center><input type='checkbox' id='rep_" . $id . "' onclick='addRepetido(" . $id . ");'></center></td>";
	$msg .= "<td><center><input type='checkbox' id='orig_" . $id ."' onclick='addOriginal(" . $id . ");'></center></td>";		

	if($tipo == 'ISI') {
		$msg .= "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $id . "');$('#tabela').text('9');apagarPublicacao();return false;\" ></center></td>";
	} else if ($tipo == 'PUBMED') {
		$msg .= "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $id . "' value='apagar' name='navOption' onclick=\"$('#chave-publicacoes').text('" . $id . "');$('#tabela').text('10');apagarPublicacao();return false;\" ></center></td>";
	}		
			
	$msg .= "</tr>";
	
	return $msg;
}
	
function getEstadoPublicacaoMI($i) {	
	$db = new Database();
	$lValues =$db->getLookupValues("lista_estadoPublicacoes");
	$texto="";
	while ($row = mysql_fetch_assoc($lValues)) {
		if($i==$row["ID"])
			$texto=$row["DESCRICAO"];
	}

	$db->disconnect();
	return $texto;	
}		