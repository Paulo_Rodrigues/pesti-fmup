﻿<?php
	ini_set('display_errors',1);
	ini_set('display_startup_errors',1);
	error_reporting(-1);
	
	//include "db.php";
	
	require_once '../../../classlib/Database.class.inc';
				
	$db = new Database();
	$db->connect();
	
	$msgArt = "<thead class='header'>
				<tr>
					<th width='40'>IDINV</th>
					<th width='60'>Tipo/ Entidade</th>
					<th width='60'>Entidade Financiadora</th>
					<th width='60'>Instituição de Acolhimento</th>
					<th width='60'>Montante total solicitado</th>
					<th width='60'>Montante total aprovado</th>
					<th width='60'>Montante  atribuído à FMUP</th>
					<th width='80'>É Investigador Responsável?</th>
					<th width='80'>Código/ Referência</th>
					<th width='120'>Título</th>
					<th width='40'>Data Início</th>
					<th width='40'>Data Fim</th>
					<th width='150'>Link</th>
					<th width='40'>Estado</th>
					<th width='60'>Repetidos</th>
					<th width='40'>Original</th>
					<th width='30'></th>
					<th width='30'></th>
				</tr>
				</thead>
				<tbody>";
	$msgJ ='';	
	$page = $_POST['page'];
	$page -= 1;
	$per_page = 30;
	$start = $page * $per_page;
	$ids = array();
	$ids_final = '';
	$originais = array();		
	//Get 30 registos 
	$query_pag_data = "SELECT id_original,id_repetido from repetidos_prj LIMIT $start, $per_page";
	$result_pag_data = mysql_query($query_pag_data,$db->conn) or die('MySql Error' . mysql_error() );
	while ($row = mysql_fetch_array($result_pag_data)) {
		$ids[]=$row['id_original'];
	}		
	$db->disconnect();
	
	if(!empty($ids)) {
		$ids = array_unique($ids);

		foreach ($ids as $i => $value) {
			$ids_final .= $ids[$i] . ',';					
			$db->connect();
			$query_pag_data = "SELECT id_repetido from repetidos_prj WHERE id_original = " . $ids[$i];
			$result_pag_data = mysql_query($query_pag_data,$db->conn) or die('MySql Error' . mysql_error());
			
			$originais[$i] = array();
			$originais[$i][] = $ids[$i];
			while ($row = mysql_fetch_array($result_pag_data)) {
				if($row['id_repetido'] != null)
					$originais[$i][]=$row['id_repetido'];
			}		
			$db->disconnect();
		}
		
		foreach ($originais as $i => $value) {		
			foreach ($originais[$i] as $j => $value1) {		
				
				//Verifica se existe algum registo repetido
				if($j == 0)	{
					if (count($originais[$i])==1) {
						$style ="style=\"overflow:hidden;\"";	
					} else {		
						$style ="style=\"background-color:#99FFFF; overflow:hidden;\"";	
					}				
				} else {
					$style ="style=\"background-color:#FFFF33; overflow:hidden;\"";	
				}		
				
				$msgJ .= "<tr>";
				
				$repetidos = $db->getRepetidosProjecto($originais[$i][$j]);		
				$idInv = $db->getIdInvByIdPrj($originais[$i][$j]);
				if($idInv) {
					if ($repetidos) {	
						$style ="style=\"background-color:#99FF66; overflow:hidden;\"";	
						$resp = checkInvPrjResponsável($originais[$i][$j], $idInv);	
						if($resp==true) 
							$msgJ .= "<td id='". $originais[$i][$j]. "' $style><b><u>".$idInv."</u></b>";	
						else 						
							$msgJ .= "<td id='". $originais[$i][$j]. "' $style>".$idInv;						
						foreach ($repetidos as $x => $value2) {	
							$resp = checkInvPrjResponsável($originais[$i][$j], $value2);	
							if($resp==true) {
								if($db->getIdInvByIdPrj($originais[$i][$j]) != $value2) {
									$msgJ .= ",<b><u>" . $value2 . "</u><b>";				
								}
							} else {
								if($db->getIdInvByIdPrj($originais[$i][$j]) != $value2) {
									$msgJ .= "," . $value2 . "";				
								}
							}		
						}
						$msgJ .=  "</td>";
					} else {			
						$resp = checkInvPrjResponsável($originais[$i][$j], $idInv);
						if($resp == true)
							$msgJ .=  "<td id='". $originais[$i][$j]. "' $style><b><u>".$idInv."</u></b></td>";
						else 
							$msgJ .=  "<td id='". $originais[$i][$j]. "' $style>".$idInv."</td>";					
					}		
						
					$db->connect();
					$query_pag_data = "SELECT * FROM projectosinv WHERE id =" .  $originais[$i][$j] . ";";					
					$result_pag_data = mysql_query($query_pag_data, $db->conn) or die('MySql Error' . mysql_error() . " dentro do repetidos $query_pag_data");
					while ($row = mysql_fetch_array($result_pag_data)) {	
						$msgJ .= printPrj($row,$style);							
					}				
				}
			}					
		}
		$msgJ = "<table id='prj' class='box-table-b'><caption><u><h2>Projetos / Ensaios Clínicos 2013</h2></u></caption>$msgArt $msgJ</tbody></table><p id='chave-projectos' hidden></p><br>" . drawButtons() . "<br>";
		
		$msg = "<div id='projectos'><ul>" . $msgJ . "</ul></div>"; // Content for Data
		echo $msg;
	} else {
		$msg = "<p>Não existem Projetos a validar.</p>"; 
		echo $msg;	
	}
	
	if($db!=null)
		$db->disconnect();

function printPrj($row,$style) {
	$msg = '';
	$id=$row['ID'];		
	$entidade=$row['ENTIDADE'];	
	$entidadefinanciadora=$row['ENTIDADEFINANCIADORA'];	
	$acolhimento=htmlentities(utf8_decode($row['ACOLHIMENTO']));
	$montante=htmlentities(utf8_decode($row['MONTANTE']));
	$montantea=htmlentities(utf8_decode($row['MONTANTEA']));
	$montantefmup=htmlentities($row['MONTANTEFMUP']);
	$invresponsavel=htmlentities($row['INVRESPONSAVEL']);	
	$codigo=$row['CODIGO'];	
	$titulo=htmlentities(utf8_decode($row['TITULO']));
	$datainicio=htmlentities(utf8_decode($row['DATAINICIO']));
	$datafim=htmlentities(utf8_decode($row['DATAFIM']));
	$link=htmlentities($row['LINK']);
	$estado=$row['ESTADO'];			
				
	$msg .= "<td " . $style . " id='td_projectos_tipoentidade_". $id ."'>";
	$msg .= getTipoEntidade($entidade);
	$msg .= "</td>";
	$msg .= "<td " . $style . " id='td_projectos_entidade_". $id ."'>".$entidadefinanciadora."</td>";
	$msg .= "<td " . $style . " id='td_projectos_acolhimento_". $id ."'>".$acolhimento."</td>";
	$msg .= "<td " . $style . " id='td_projectos_montante_". $id ."'>".$montante." €</td>";
	$msg .= "<td " . $style . " id='td_projectos_montantea_". $id ."'>".$montantea." €</td>";
	$msg .= "<td " . $style . " id='td_projectos_montantefmup_". $id ."'>".$montantefmup." €</td>";
	$msg .= "<td " . $style . " id='td_projectos_invres_". $id ."'>".checkInv($invresponsavel)."</td>";
	$msg .= "<td " . $style . " id='td_projectos_codigo_". $id ."'>".$codigo."</td>";
	$msg .= "<td " . $style . " id='td_projectos_titulo_". $id ."'>".$titulo."</td>";
	$msg .= "<td " . $style . " id='td_projectos_dataini_". $id ."'>".$datainicio."</td>";
	$msg .= "<td " . $style . " id='td_projectos_datafim_". $id ."'>".$datafim."</td>";
	$msg .= "<td " . $style . " id='td_projectos_link_". $id ."'><a href='".$link."' target='_blank'>".$link."</a></td>";
	$msg .= "<td " . $style . " id='td_projectos_estado_". $id ."'>".getEstadoProjeto($estado)."</td>";
	$msg .= "<td><input type='checkbox' id='rep_" . $id . "' onclick='addRepetido(" . $id . ");'>";
	$msg .= "<td><input type='checkbox' id='orig_" . $id ."' onclick='addOriginal(" . $id . ");'>";
	$msg .= "<td style='overflow:hidden;'><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-projectos').text('" . $id. "');\"></td>";           
	$msg .= "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $id . "' value='apagar' name='navOption' onclick=\"$('#chave-projectos').text('" . $id . "');apagarProjecto();return false;\" ></center></td>";   
	$msg .= "</tr>";	
	
	return $msg;
}
	
function drawButtons() {
	$db = new Database();
	$db->connect();
	
	$page = $_POST['page'];
	$cur_page = $page;
	$page -= 1;
	$per_page = 30;
	$previous_btn = true;
	$next_btn = true;
	$first_btn = true;
	$last_btn = true;
	$start = $page * $per_page;
	
	$msg = '';
	/* --------------------------------------------- */
	$query_pag_num = "SELECT COUNT(*) AS count FROM repetidos_prj";
	$result_pag_num = mysql_query($query_pag_num,$db->conn);
	$row = mysql_fetch_array($result_pag_num);
	$count = $row['count'];
	$no_of_paginations = ceil($count / $per_page);

	/* ---------------Calculating the starting and endign values for the loop----------------------------------- */
	if ($cur_page >= 7) {
		$start_loop = $cur_page - 3;
		if ($no_of_paginations > $cur_page + 3)
			$end_loop = $cur_page + 3;
		else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
			$start_loop = $no_of_paginations - 6;
			$end_loop = $no_of_paginations;
		} else {
			$end_loop = $no_of_paginations;
		}
	} else {
		$start_loop = 1;
		if ($no_of_paginations > 7)
			$end_loop = 7;
		else
			$end_loop = $no_of_paginations;
	}
	/* ----------------------------------------------------------------------------------------------------------- */
	$msg .= "<div class='pubPRJpagination'><ul>";

	for ($i = $start_loop; $i <= $end_loop; $i++) {
		if ($cur_page == $i)
			$msg .= "<li p='$i' style='color:#fff;background-color:#006699;' class='active' onclick='clickNumPRJ(this);'>{$i}</li>";
		else
			$msg .= "<li p='$i' class='active' onclick='clickNumPRJ(this);'>{$i}</li>";
	}
	
	$goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn_pub_PRJ' class='go_button' value='Procurar' onclick='procuraPRJ();'/>&nbsp;";
	$total_string = "<span class='total' a='$no_of_paginations'>P&aacutegina <b>" . $cur_page . "</b> de <b>$no_of_paginations</b></span>";
	$msg = $msg . "</ul>" . $goto . $total_string . "</div>";  // Content for pagination
	
	return $msg;
}

function getTipoEntidade($i) {
	$db = new Database();
	$lValues =$db->getLookupValues("lista_tipoentidadefinanciadora");
	while ($row = mysql_fetch_assoc($lValues)) {	
		if($i==$row["ID"])
			return $row["DESCRICAO"];
	}
	$db->disconnect();
}

function checkInv($i){
	if($i==1)
		return "Sim";
	else 
		return "Não";
}

function getEstadoProjeto($i) {
	$db = new Database();
	$lValues =$db->getLookupValues("lista_estadoProjetos");
	$texto="";
	while ($row = mysql_fetch_assoc($lValues)) {
		if($i==$row["ID"])
			$texto=$row["DESCRICAO"];
	}
	$db->disconnect();
	return $texto;
}


function checkInvPrjResponsável($idPrj, $idInv) {	
	$connection = new Database();
	$connection->connect();
	$sql = "SELECT INVRESPONSAVEL FROM projectosinv WHERE ID = $idPrj AND IDINV = $idInv";
	$result = mysql_query($sql,$connection->conn);
	$row = mysql_fetch_array($result);
	$resp = $row['INVRESPONSAVEL'];
	
	if($resp == 0) 	
		return false;
	else	
		return true;	
}

?>
		