﻿<?php
	ini_set('display_errors',1);
	ini_set('display_startup_errors',1);
	error_reporting(-1);
	
	//include "db.php";
	
	require_once '../../../classlib/Database.class.inc';
				
	$db = new Database();

	$db->connect();
	
	$msgArt = "<thead class='header'>
				<tr>
					<th width='50'>Id Inv</th>
					<th>Unidade de Investigação</th>
					<th>Avaliação 2007</th>
					<th>Percentagem</th>
					<th>Responsável pela Unidade</th>
					<th width='25'>Repetidos</th>
					<th width='20'>Original</th>
					<th width='15'></th>
					<th width='15'></th>
				</tr>
				</thead>
				<tbody>";
	$msgJ ='';	
	$page = $_POST['page'];
	$page -= 1;
	$per_page = 30;
	$start = $page * $per_page;
	$ids = array();
	$ids_final = '';
	$originais = array();	
	$db->connect();
	
	//Get 30 registos 
	$query_pag_data = "SELECT DISTINCT id_original from repetidos_uniInv LIMIT $start, $per_page";
	$result_pag_data = mysql_query($query_pag_data,$db->conn) or die('MySql Error' . mysql_error() );
	$db->disconnect();
	while ($row = mysql_fetch_array($result_pag_data)) {
		$ids[]=$row['id_original'];
	}
	
	if(!empty($ids)) {
		$ids = array_unique($ids);

		foreach ($ids as $i => $value) {
			$ids_final .= $ids[$i] . ',';
			$db->connect();
			$query_pag_data = "SELECT id_repetido from repetidos_uniInv WHERE id_original = " . $ids[$i];
			$result_pag_data = mysql_query($query_pag_data, $db->conn) or die('MySql Error' . mysql_error());
			
			$originais[$i] = array();
			$originais[$i][] = $ids[$i];
			while ($row = mysql_fetch_array($result_pag_data)) {
				if($row['id_repetido'] != null)
					$originais[$i][]=$row['id_repetido'];
			}
			$db->disconnect();
		}
		
		foreach ($originais as $i => $value) {		
			foreach ($originais[$i] as $j => $value1) {		
				
				//Verifica se existe algum registo repetido
				if($j == 0)	{
					if (count($originais[$i])==1) {
						$style ="style=\"overflow:hidden;\"";	
					} else {		
						$style ="style=\"background-color:#99FFFF; overflow:hidden;\"";	
					}				
				} else {
					$style ="style=\"background-color:#FFFF33; overflow:hidden;\"";	
				}		
				
				$msgJ .= "<tr>";
				
				$repetidos = $db->getRepetidosUnidadeInvestigacao($originais[$i][$j]);				
				$idInv = $db->getIdInvByIdUniInv($originais[$i][$j]);
				if($idInv) {
					if ($repetidos) {						
						$style ="style=\"background-color:#99FF66; overflow:hidden;\"";	
						$resp = checkInvUniInvResponsável($originais[$i][$j], $idInv);
						if($resp==true) 
							$msgJ .= "<td id='". $originais[$i][$j]. "' $style><b><u>".$idInv."</u></b>";	
						else 						
							$msgJ .= "<td id='". $originais[$i][$j]. "' $style>".$idInv;	
						foreach ($repetidos as $x => $value2) {	
							$resp = checkInvUniInvResponsável($originais[$i][$j], $value2);
							if($resp==true) {
								if($db->getIdInvByIdUniInv($originais[$i][$j]) != $value2) {
									$msgJ .= ",<b><u>" . $value2 . "</u><b>";				
								}
							} else {
								if($db->getIdInvByIdUniInv($originais[$i][$j]) != $value2) {
									$msgJ .= "," . $value2 . "";				
								}
							}							
						}
						$msgJ .=  "</td>";
					} else {							
						$resp = checkInvUniInvResponsável($originais[$i][$j], $idInv);
						if($resp == true)
							$msgJ .=  "<td id='". $originais[$i][$j]. "' $style><b><u>".$idInv."</u></b></td>";
						else 
							$msgJ .=  "<td id='". $originais[$i][$j]. "' $style>".$idInv."</td>";
						
					}			
					
					$query_pag_data = "SELECT * FROM unidadesinvestigacao WHERE id = " .  $originais[$i][$j] . ";";	
					$db->connect();
					$result_pag_data = mysql_query($query_pag_data,$db->conn) or die('MySql Error' . mysql_error() . " dentro do repetidos $query_pag_data");
					while ($row = mysql_fetch_array($result_pag_data)) {	
						$msgJ .= printUniInv($row,$style);							
					}
					$db->disconnect();
				}	
			}
		}
		$msgJ = "<table id='unidadesInvestigacao' class='box-table-b'><caption><u><h2>Unidades de Investigação</h2></u></caption>$msgArt $msgJ</tbody></table><p id='chave-uniInv' hidden></p></p><br>" . drawButtons() . "<br>";
		
		$msg = "<div id='uniInv'><ul>" . $msgJ . "</ul></div>"; // Content for Data
		echo $msg;
	} else {
		$msg = "<p>Não existem Unidades de Investigação a validar.</p>"; 
		echo $msg;	
	}
	

	$db->disconnect();

function printUniInv($row,$style) {
	$msg = '';
	$id=$row['ID'];		
	$unidade=$row['UNIDADE'];	
	$ava2007=htmlentities(utf8_decode($row['AVA2007']));
	$percentagem=htmlentities(utf8_decode($row['PERCENTAGEM']));
	$responsavel=htmlentities($row['RESPONSAVEL']);
	
	$msg .= "<td id='td_uni_inv_unidade_" .$id . "' $style>" .$unidade . "</td>";				
	$msg .= "<td id='td_uni_inv_ava2007_" .$id . "' $style>" .$ava2007 . "</td>";				
	$msg .= "<td id='td_uni_inv_perc_" .$id . "' $style>" .$percentagem . "</td>";				
	$msg .= "<td id='td_uni_inv_resp_" .$id . "' $style>";
	if ($responsavel==1) 
		$msg .= "Sim</td>";
	else 
		$msg .= "Não</td>";
	$msg .= "<td><input type='checkbox' id='rep_" . $id . "' onclick='addRepetido(" . $id . ");'>";
	$msg .= "<td><input type='checkbox' id='orig_" . $id ."' onclick='addOriginal(" . $id . ");'>";
	$msg .= "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-uniInv').text('" .$id . "');\"></td>";
	$msg .= "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" .$id . "' value='apagar' name='navOption' onclick=\"$('#chave-uniInv').text('" .$id . "');apagarUniInv();return false;\" ></center></td>";						
	$msg .= "</tr>";  
	
	return $msg;
}
	
function drawButtons() {
	$db = new Database();
	$db->connect();
	
	$page = $_POST['page'];
	$cur_page = $page;
	$page -= 1;
	$per_page = 30;
	$previous_btn = true;
	$next_btn = true;
	$first_btn = true;
	$last_btn = true;
	$start = $page * $per_page;
	
	$msg = '';
	/* --------------------------------------------- */
	$query_pag_num = "SELECT COUNT(*) AS count FROM repetidos_uniInv";
	$result_pag_num = mysql_query($query_pag_num,$db->conn);
	$row = mysql_fetch_array($result_pag_num);
	$count = $row['count'];
	$no_of_paginations = ceil($count / $per_page);

	/* ---------------Calculating the starting and endign values for the loop----------------------------------- */
	if ($cur_page >= 7) {
		$start_loop = $cur_page - 3;
		if ($no_of_paginations > $cur_page + 3)
			$end_loop = $cur_page + 3;
		else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
			$start_loop = $no_of_paginations - 6;
			$end_loop = $no_of_paginations;
		} else {
			$end_loop = $no_of_paginations;
		}
	} else {
		$start_loop = 1;
		if ($no_of_paginations > 7)
			$end_loop = 7;
		else
			$end_loop = $no_of_paginations;
	}
	/* ----------------------------------------------------------------------------------------------------------- */
	$msg .= "<div class='pubUniInvPagination'><ul>";

	for ($i = $start_loop; $i <= $end_loop; $i++) {
		if ($cur_page == $i)
			$msg .= "<li p='$i' style='color:#fff;background-color:#006699;' class='active' onclick='clickNumUniInv(this);'>{$i}</li>";
		else
			$msg .= "<li p='$i' class='active' onclick='clickNumUniInv(this);'>{$i}</li>";
	}

	$goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn_pub_UniInv' class='go_button' value='Procurar' onclick='procuraUniInv();'/>&nbsp;";
	$total_string = "<span class='total' a='$no_of_paginations'>P&aacutegina <b>" . $cur_page . "</b> de <b>$no_of_paginations</b></span>";
	$msg = $msg . "</ul>" . $goto . $total_string . "</div>";  // Content for pagination
	
	return $msg;
}

function checkInvUniInvResponsável($idUniInv, $idInv) {
	$db = new Database();
	$db->connect();
	
	$sql = "SELECT responsavel FROM unidadesinvestigacao WHERE ID = $idUniInv AND IDINV = $idInv";
	$result = mysql_query($sql,$db->conn);
	$row = mysql_fetch_array($result);
	$resp = $row['responsavel'];
	$db->disconnect();
	
	if($resp == 0) 	
		return false;
	else	
		return true;	
}
	
?>