<?php 
	function showPublicacoesTudo() {?>
		<meta charset="UTF-8">
		<script type='text/javascript' src='../../js/jquery.tablesorter.js'></script>
		<script type="text/javascript" src="../../js/stickyTableHeaders.js"></script>
        <script type="text/javascript"> 
			var pagina = 1;
			            
			function loadDataJISI(page){ $.ajax ({type: "POST",url: "validacaoRepetidos/load_data_Publicacoes.php", data: "a=1&page="+page, contentType: "application/x-www-form-urlencoded", dataType:"html",  success: function(msg){$("#pubISI_J").html(msg + "<br>"); sortTableJ(); ;} });}				
			function loadDataJAISI(page){ $.ajax({ type: "POST", url: "validacaoRepetidos/load_data_Publicacoes.php", data: "a=2&page="+page, contentType: "application/x-www-form-urlencoded", dataType:"html",  success: function(msg){ $("#pubISI_JA").html(msg + "<br>"); sortTableJA(); }});}
			function loadDataPRPISI(page){ $.ajax({ type: "POST", url: "validacaoRepetidos/load_data_Publicacoes.php", data: "a=3&page="+page, contentType: "application/x-www-form-urlencoded", dataType:"html",  success: function(msg){ $("#pubISI_PRP").html(msg + "<br>"); sortTablePRP();}});}
			function loadDataCPISI(page){ $.ajax({ type: "POST", url: "validacaoRepetidos/load_data_Publicacoes.php", data: "a=4&page="+page, contentType: "application/x-www-form-urlencoded", dataType:"html",  success: function(msg){ $("#pubISI_CP").html(msg + "<br>"); sortTableCP(); }});}
			function loadDataOAISI(page){ $.ajax({ type: "POST", url: "validacaoRepetidos/load_data_Publicacoes.php", data: "a=5&page="+page,contentType: "application/x-www-form-urlencoded", dataType:"html",  success: function(msg){$("#pubISI_OA").html(msg + "<br>"); sortTableOA(); }});}
			function loadDataLISI(page){ $.ajax({ type: "POST", url: "validacaoRepetidos/load_data_Publicacoes.php", data: "a=6&page="+page, contentType: "application/x-www-form-urlencoded", dataType:"html",  success: function(msg){$("#pubISI_L").html(msg + "<br>"); sortTableL(); }});}
			function loadDataCLISI(page){ $.ajax({ type: "POST", url: "validacaoRepetidos/load_data_Publicacoes.php", data: "a=7&page="+page, contentType: "application/x-www-form-urlencoded", dataType:"html",  success: function(msg){ $("#pubISI_CL").html(msg + "<br>"); sortTableCL(); }});}
			function loadDataSISI(page){ $.ajax({ type: "POST", url: "validacaoRepetidos/load_data_Publicacoes.php", data: "a=8&page="+page, contentType: "application/x-www-form-urlencoded", dataType:"html",  success: function(msg){ $("#pubISI_S").html(msg + "<br>"); sortTableS(); }});}
			function loadDataJNISI(page){ $.ajax({ type: "POST", url: "validacaoRepetidos/load_data_Publicacoes.php", data: "a=9&page="+page, contentType: "application/x-www-form-urlencoded", dataType:"html",  success: function(msg){$("#pubISI_JN").html(msg + "<br>"); sortTableJN(); }});}
			function loadDataPRPNISI(page){$.ajax({ type: "POST", url: "validacaoRepetidos/load_data_Publicacoes.php", data: "a=10&page="+page, contentType: "application/x-www-form-urlencoded", dataType:"html",  success: function(msg){ $("#pubISI_PRPN").html(msg + "<br>"); sortTablePRPN(); }});}
			function loadDataSNISI(page){ $.ajax({ type: "POST", url: "validacaoRepetidos/load_data_Publicacoes.php", data: "a=11&page="+page, contentType: "application/x-www-form-urlencoded", dataType:"html",  success: function(msg){ $("#pubISI_SN").html(msg + "<br>"); sortTableSN(); }});}
			function loadDataLNISI(page){ $.ajax({ type: "POST", url: "validacaoRepetidos/load_data_Publicacoes.php", data: "a=12&page="+page, contentType: "application/x-www-form-urlencoded", dataType:"html",  success: function(msg){ $("#pubISI_LN").html(msg + "<br>"); sortTableLN(); }});}
			function loadDataCLNISI(page){ $.ajax({ type: "POST", url: "validacaoRepetidos/load_data_Publicacoes.php", data: "a=13&page="+page, contentType: "application/x-www-form-urlencoded", dataType:"html",  success: function(msg){ $("#pubISI_CLN").html(msg + "<br>"); sortTableCLN(); }});}
			function loadDataCPNISI(page){ $.ajax({ type: "POST", url: "validacaoRepetidos/load_data_Publicacoes.php", data: "a=14&page="+page, contentType: "application/x-www-form-urlencoded", dataType:"html",  success: function(msg){$("#pubISI_CPN").html(msg + "<br>"); sortTableCPN(); }});}
			function loadDataPISI(page){ $.ajax({ type: "POST", url: "validacaoRepetidos/load_data_Publicacoes.php", data: "a=15&page="+page, contentType: "application/x-www-form-urlencoded", dataType:"html",  success: function(msg){ $("#pubISI_P").html(msg + "<br>"); sortTableP(); }});}
			function loadDataAISI(page){ $.ajax({ type: "POST", url: "validacaoRepetidos/load_data_Publicacoes.php", data: "a=16&page="+page, contentType: "application/x-www-form-urlencoded", dataType:"html",  success: function(msg){ $("#pubISI_A").html(msg + "<br>"); sortTableA(); }});}
				     					         
			loadDataJISI(1); 
			loadDataJAISI(1);		 
			loadDataPRPISI(1);	
			loadDataCPISI(1);	
			loadDataOAISI(1);
			loadDataLISI(1);
			loadDataCLISI(1);
			loadDataSISI(1);
			loadDataJNISI(1);
			loadDataPRPNISI(1); 
			loadDataSNISI(1);
			loadDataLNISI(1);
			loadDataCLNISI(1);
			loadDataCPNISI(1); 
			loadDataPISI(1);
			loadDataAISI(1);			
			
			/* -------------------------- ISI - J -----------------------------*/			
			function clickNumJISI(elem) {	
				page = $(elem).attr('p');	
				loadDataJISI(page);
				pagina = page;
				$("html, body").animate({ scrollTop: $("#pubISI_J .pubISIdata table caption").offset().top }, 600);
			}

			function procuraJISI() {
				page = parseInt($('#pubISI_J .goto').val());
				var no_of_pages = parseInt($('.total').attr('a'));
				if(page != 0 && page <= no_of_pages){
					loadDataJISI(page);
					pagina = page;
				}else{
					alert('Introduza um n�mero entre 1 e '+no_of_pages);
					$('#pubISI_J .goto').val("").focus();
					return false;
				}
				$("html, body").animate({ scrollTop: $("#pubISI_J .pubISIdata table caption").offset().top }, 600);
			}
			/*-----------------------------------------------------------------*/
			
			/* -------------------------- ISI - JA -----------------------------*/
			function clickNumJAISI(elem) {
				page = $(elem).attr('p');
				loadDataJAISI(page);
				pagina = page;
				$("html, body").animate({ scrollTop: $("#pubISI_JA .pubISIdata table caption").offset().top }, 600);
			}           
			
		   function procuraJAISI() {
				page = parseInt($('#pubISI_JA .goto').val());
				var no_of_pages = parseInt($('.total').attr('a'));
				if(page != 0 && page <= no_of_pages){
					loadDataJAISI(page);
					pagina = page;
				}else{
					alert('Introduza um n�mero entre 1 e '+no_of_pages);
					$('#pubISI_JA .goto').val("").focus();
					return false;
				}
				$("html, body").animate({ scrollTop: $("#pubISI_JA .pubISIdata table caption").offset().top }, 600);
				return false;                    
			}
			/*-----------------------------------------------------------------*/
			
			/* -------------------------- ISI - PRP -----------------------------*/
			function clickNumPRPISI(elem) {
				page = $(elem).attr('p');
				loadDataPRPISI(page);
				pagina = page;
				$("html, body").animate({ scrollTop: $("#pubISI_PRP .pubISIdata table caption").offset().top }, 600);
				return false;				
			}           
			
			function procuraPRPISI() {
				page = parseInt($('#pubISI_PRP .goto').val());
				var no_of_pages = parseInt($('.total').attr('a'));
				if(page != 0 && page <= no_of_pages){
					loadDataPRPISI(page);							
					pagina = page;
				}else{
					alert('Introduza um n�mero entre 1 e ' + no_of_pages);
					$('#pubISI_PRP .goto').val("").focus();
					return false;
				}
				$("html, body").animate({ scrollTop: $("#pubISI_PRP .pubISIdata table caption").offset().top }, 600);
				return false;                    
			}
			/*-----------------------------------------------------------------*/
			
			/* -------------------------- ISI - CP -----------------------------*/
			function clickNumCPISI(elem) {
				page = $(elem).attr('p');
				loadDataCPISI(page);
				pagina = page;
				$("html, body").animate({ scrollTop: $("#pubISI_CP .pubISIdata table caption").offset().top }, 600);
				return false;				
			}        
			
			function procuraCPISI() {
				page = parseInt($('#pubISI_CP .goto').val());
				var no_of_pages = parseInt($('.total').attr('a'));
				if(page != 0 && page <= no_of_pages){
					loadDataCPISI(page);
					pagina = page;
				}else{
					alert('Introduza um n�mero entre 1 e ' + no_of_pages);
					$('#pubISI_CP .goto').val("").focus();
					return false;
				}
				$("html, body").animate({ scrollTop: $("#pubISI_CP .pubISIdata table caption").offset().top }, 600);
				return false;                    
			}
			/*----------------------------------------------------------------------*/
			
			/* -------------------------- ISI - OA -----------------------------*/
			function clickNumOAISI(elem) {
				page = $(elem).attr('p');
				loadDataOAISI(page);
				pagina = page;
				$("html, body").animate({ scrollTop: $("#pubISI_OA .pubISIdata table caption").offset().top }, 600);
				return false;				
			}           
			
			function procuraOAISI() {
				page = parseInt($('#pubISI_OA .goto').val());
				var no_of_pages = parseInt($('.total').attr('a'));
				if(page != 0 && page <= no_of_pages){
					loadDataOAISI(page);
					pagina = page;
				}else{
					alert('Introduza um n�mero entre 1 e ' + no_of_pages);
					$('#pubISI_OA .goto').val("").focus();
					return false;
				}
				$("html, body").animate({ scrollTop: $("#pubISI_OA .pubISIdata table caption").offset().top }, 600);
				return false;                    
			}
			/*----------------------------------------------------------------------*/		
			
			/* -------------------------- ISI - L -----------------------------*/
			function clickNumLISI(elem) {
				page = $(elem).attr('p');
				loadDataLISI(page);
				pagina = page;
				$("html, body").animate({ scrollTop: $("#pubISI_L .pubISIdata table caption").offset().top }, 600);
				return false;				
			}
			
			function procuraLISI() {
				page = parseInt($('#pubISI_L .goto').val());
				var no_of_pages = parseInt($('.total').attr('a'));
				if(page != 0 && page <= no_of_pages){
					loadDataLISI(page);
					pagina = page;
				}else{
					alert('Introduza um n�mero entre 1 e ' + no_of_pages);
					$('#pubISI_L .goto').val("").focus();
					return false;
				}
				$("html, body").animate({ scrollTop: $("#pubISI_L .pubISIdata table caption").offset().top }, 600);
				return false;                    
			}
			/*----------------------------------------------------------------------*/
			
			/* -------------------------- ISI - CL -----------------------------*/
			function clickNumCLISI(elem) {
				page = $(elem).attr('p');
				loadDataCLISI(page);
				pagina = page;
				$("html, body").animate({ scrollTop: $("#pubISI_CL .pubISIdata table caption").offset().top }, 600);				
			}       
			
			function procuraCLISI() {
				page = parseInt($('#pubISI_CL .goto').val());
				var no_of_pages = parseInt($('.total').attr('a'));
				if(page != 0 && page <= no_of_pages){
					loadDataCLISI(page);
					pagina = page;
				}else{
					alert('Introduza um n�mero entre 1 e ' + no_of_pages);
					$('#pubISI_CL .goto').val("").focus();
					return false;
				}
				$("html, body").animate({ scrollTop: $("#pubISI_CL .pubISIdata table caption").offset().top }, 600);
				return false;                    
			}
			/*----------------------------------------------------------------------*/
			
			/* -------------------------- ISI - S -----------------------------*/
			function clickNumSISI(elem) {
				page = $(elem).attr('p');
				loadDataSISI(page);
				pagina = page;
				$("html, body").animate({ scrollTop: $("#pubISI_S .pubISIdata table caption").offset().top }, 600);
				return false;				
			}        
			
			function procuraSISI() {
				page = parseInt($('#pubISI_S .goto').val());
				var no_of_pages = parseInt($('.total').attr('a'));
				if(page != 0 && page <= no_of_pages){
					loadDataSISI(page);
					pagina = page;
				}else{
					alert('Introduza um n�mero entre 1 e ' + no_of_pages);
					$('#pubISI_S .goto').val("").focus();
					return false;
				}
				$("html, body").animate({ scrollTop: $("#pubISI_S .pubISIdata table caption").offset().top }, 600);
				return false;                    
			}
			/*----------------------------------------------------------------------*/
			
			/* -------------------------- ISI - JN -----------------------------*/
			function clickNumJNISI(elem) {
				page = $(elem).attr('p');
				loadDataJNISI(page);
				pagina = page;
				$("html, body").animate({ scrollTop: $("#pubISI_JN .pubISIdata table caption").offset().top }, 600);
				return false;				
			}       
			
			function procuraJNISI() {
				page = parseInt($('#pubISI_JN .goto').val());
				var no_of_pages = parseInt($('.total').attr('a'));
				if(page != 0 && page <= no_of_pages){
					loadDataJNISI(page);
					pagina = page;
				}else{
					alert('Introduza um n�mero entre 1 e ' + no_of_pages);
					$('#pubISI_JN .goto').val("").focus();
					return false;
				}
				$("html, body").animate({ scrollTop: $("#pubISI_JN .pubISIdata table caption").offset().top }, 600);
				return false;                    
			}
			/*----------------------------------------------------------------------*/
			
			/* -------------------------- ISI - PRPN -----------------------------*/
			function clickNumPRPNISI(elem) {
				page = $(elem).attr('p');
				loadDataPRPNISI(page);
				pagina = page;
				$("html, body").animate({ scrollTop: $("#pubISI_PRPN .pubISIdata table caption").offset().top }, 600);
				return false;				
			}     
			
			function procuraPRPNISI() {
				page = parseInt($('#pubISI_PRPN .goto').val());
				var no_of_pages = parseInt($('.total').attr('a'));
				if(page != 0 && page <= no_of_pages){
					loadDataPRPNISI(page);
					pagina = page;
				}else{
					alert('Introduza um n�mero entre 1 e ' + no_of_pages);
					$('#pubISI_PRPN .goto').val("").focus();
					return false;
				}
				$("html, body").animate({ scrollTop: $("#pubISI_PRPN .pubISIdata table caption").offset().top }, 600);
				return false;                    
			}
			/*----------------------------------------------------------------------*/
			
			/* -------------------------- ISI - SN -----------------------------*/
			function clickNumSNISI(elem) {
				page = $(elem).attr('p');
				loadDataSNISI(page);
				pagina = page;
				$("html, body").animate({ scrollTop: $("#pubISI_SN .pubISIdata table caption").offset().top }, 600);
				return false;
			}
			
			function procuraSNISI() {
				page = parseInt($('#pubISI_SN .goto').val());
				var no_of_pages = parseInt($('.total').attr('a'));
				if(page != 0 && page <= no_of_pages){
					loadDataSNISI(page);
					pagina = page;
				}else{
					alert('Introduza um n�mero entre 1 e ' + no_of_pages);
					$('#pubISI_SN .goto').val("").focus();
					return false;
				}
				$("html, body").animate({ scrollTop: $("#pubISI_SN .pubISIdata table caption").offset().top }, 600);
				return false;                    
			}
			/*----------------------------------------------------------------------*/
			
			/* -------------------------- ISI - LN -----------------------------*/
			function clickNumLNISI(elem) {
				page = $(elem).attr('p');
				loadDataLNISI(page);
				pagina = page;
				$("html, body").animate({ scrollTop: $("#pubISI_LN .pubISIdata table caption").offset().top }, 600);
				return false;				
			}          
			
			function procuraLNISI() {
				page = parseInt($('#pubISI_LN .goto').val());
				var no_of_pages = parseInt($('.total').attr('a'));
				if(page != 0 && page <= no_of_pages){
					loadDataLNISI(page);
					pagina = page;
				}else{
					alert('Introduza um n�mero entre 1 e ' + no_of_pages);
					$('#pubISI_LN .goto').val("").focus();
					return false;
				}
				$("html, body").animate({ scrollTop: $("#pubISI_LN .pubISIdata table caption").offset().top }, 600);
				return false;                    
			}
			/*----------------------------------------------------------------------*/
			
			/* -------------------------- ISI - CLN -----------------------------*/
			function clickNumCLNISI(elem) {
				page = $(elem).attr('p');
				loadDataCLNISI(page);
				pagina = page;
				$("html, body").animate({ scrollTop: $("#pubISI_CLN .pubISIdata table caption").offset().top }, 600);
				return false;				
			}          
			
			function procuraCLNISI() {
				page = parseInt($('#pubISI_CLN .goto').val());
				var no_of_pages = parseInt($('.total').attr('a'));
				if(page != 0 && page <= no_of_pages){
					loadDataCLNISI(page);
					pagina = page;
				}else{
					alert('Introduza um n�mero entre 1 e ' + no_of_pages);
					$('#pubISI_CLN .goto').val("").focus();
					return false;
				}
				$("html, body").animate({ scrollTop: $("#pubISI_CLN .pubISIdata table caption").offset().top }, 600);
				return false;                    
			}
			/*----------------------------------------------------------------------*/
			
			/* -------------------------- ISI - CPN -----------------------------*/
			function clickNumCPNISI(elem) {
				page = $(elem).attr('p');
				loadDataCPNISI(page);
				pagina = page;
				$("html, body").animate({ scrollTop: $("#pubISI_CPN .pubISIdata table caption").offset().top }, 600);
				return false;				
			}          
			
			function procuraCPNISI() {
				page = parseInt($('#pubISI_CPN .goto').val());
				var no_of_pages = parseInt($('.total').attr('a'));
				if(page != 0 && page <= no_of_pages){
					loadDataCPNISI(page);
					pagina = page;
				}else{
					alert('Introduza um n�mero entre 1 e ' + no_of_pages);
					$('#pubISI_CPN .goto').val("").focus();
					return false;
				}
				$("html, body").animate({ scrollTop: $("#pubISI_CPN .pubISIdata table caption").offset().top }, 600);
				return false;                    
			}
			/*----------------------------------------------------------------------*/
			
			/* -------------------------- ISI - P -----------------------------*/
			function clickNumPISI(elem) {
				page = $(elem).attr('p');
				loadDataPISI(page);
				pagina = page;
				$("html, body").animate({ scrollTop: $("#pubISI_P .pubISIdata table caption").offset().top }, 600);
				return false;				
			}   
			
			function procuraPISI() {
				page = parseInt($('#pubISI_P .goto').val());
				var no_of_pages = parseInt($('.total').attr('a'));
				if(page != 0 && page <= no_of_pages){
					loadDataPISI(page);
					pagina = page;
				}else{
					alert('Introduza um n�mero entre 1 e ' + no_of_pages);
					$('#pubISI_P .goto').val("").focus();
					return false;
				}
				$("html, body").animate({ scrollTop: $("#pubISI_P .pubISIdata table caption").offset().top }, 600);
				return false;                    
			}
			/*----------------------------------------------------------------------*/
			
			/* -------------------------- ISI - A -----------------------------*/
			function clickNumAISI(elem) {
				page = $(elem).attr('p');
				loadDataAISI(page);
				pagina = page;
				$("html, body").animate({ scrollTop: $("#pubISI_A .pubISIdata table caption").offset().top }, 600);
				return false;				
			}        
			
			function procuraAISI() {
				page = parseInt($('#pubISI_A .goto').val());
				var no_of_pages = parseInt($('.total').attr('a'));
				if(page != 0 && page <= no_of_pages){
					loadDataAISI(page);
					pagina = page;
				}else{
					alert('Introduza um n�mero entre 1 e ' + no_of_pages);
					$('#pubISI_A .goto').val("").focus();
					return false;
				}
				$("html, body").animate({ scrollTop: $("#pubISI_A .pubISIdata table caption").offset().top }, 600);
				return false;                    
			}
			/*----------------------------------------------------------------------*/		
			
			
			/* ---------------------ORDENACAO DAS TABELAS ---------------*/
			function sortTableJ() {	
				$('#JISI').tablesorter( {		
					'headers': { 	                        
						12: { sorter : false },	
						13: { sorter : false },
						14: { sorter : false },
						15: { sorter : false } 
                    } 
                });		
				$("#JISI").stickyTableHeaders();			
			}	
			
			function sortTableJA() {			
				$('#JAISI').tablesorter( {		
					'headers': { 	                        
						12: { sorter : false },	
						13: { sorter : false },
						14: { sorter : false },
						15: { sorter : false } 
                    } 
                });	
				$("#JAISI").stickyTableHeaders();					
			}
			
			function sortTablePRP() {			
				$('#PRPISI').tablesorter( {		
					'headers': { 	                        
						12: { sorter : false },	
						13: { sorter : false },
						14: { sorter : false },
						15: { sorter : false } 
                    } 
                });
				$("#PRPISI").stickyTableHeaders();						
			}

			function sortTableCP() {			
				$('#CPISI').tablesorter( {		
					'headers': { 	                        
						11: { sorter : false },	
						12: { sorter : false },
						13: { sorter : false },
						14: { sorter : false } 
                    } 
                });		
				$("#CPISI").stickyTableHeaders();					
			}

			function sortTableOA() {			
				$('#OAISI').tablesorter( {		
					'headers': { 	                        
						11: { sorter : false },	
						12: { sorter : false },
						13: { sorter : false },
						14: { sorter : false } 
                    } 
                });			
				$("#OAISI").stickyTableHeaders();					
			}

			function sortTableL() {			
				$('#LISI').tablesorter( {		
					'headers': { 	                        
						9: { sorter : false },	
						10: { sorter : false },
						11: { sorter : false },
						12: { sorter : false } 
                    } 
                });		
				$("#LISI").stickyTableHeaders();					
			}

			function sortTableCL() {			
				$('#CLISI').tablesorter({		
					'headers': { 	                        
						12: { sorter : false },	
						13: { sorter : false },
						14: { sorter : false },
						15: { sorter : false } 
                    } 
                });			
				$("#CLISI").stickyTableHeaders();										
			}

			function sortTableS() {			
				$('#SISI').tablesorter({		
					'headers': { 	                        
						19: { sorter : false },	
						20: { sorter : false },
						21: { sorter : false },
						22: { sorter : false } 
                    } 
                });	
				$("#SISI").stickyTableHeaders();				
			}

			function sortTableJN() {			
				$('#JNISI').tablesorter({		
					'headers': { 	                        
						12: { sorter : false },	
						13: { sorter : false },
						14: { sorter : false },
						15: { sorter : false } 
                    } 
                });		
				$("#JNISI").stickyTableHeaders();			
			}

			function sortTablePRPN() {			
				$('#PRPNISI').tablesorter({		
					'headers': { 	                        
						12: { sorter : false },	
						13: { sorter : false },
						14: { sorter : false },
						15: { sorter : false } 
                    } 
                });		
				$("#PRPNISI").stickyTableHeaders();				
			}

			function sortTableSN() {			
				$('#SNISI').tablesorter( {		
					'headers': { 	                        
						19: { sorter : false },	
						20: { sorter : false },
						21: { sorter : false },
						22: { sorter : false } 
                    } 
                });		
				$("#SNISI").stickyTableHeaders();				
			}

			function sortTableLN() {			
				$('#LNISI').tablesorter( {		
					'headers': { 	                        
						9: { sorter : false },	
						10: { sorter : false },
						11: { sorter : false },
						12: { sorter : false } 
                    } 
                });		
				$("#LNISI").stickyTableHeaders();							
			}

			function sortTableCLN() {			
				$('#CLNISI').tablesorter( {		
					'headers': { 	                        
						12: { sorter : false },	
						13: { sorter : false },
						14: { sorter : false },
						15: { sorter : false } 
                    } 
                });		
				$("#CLNISI").stickyTableHeaders();				
			}

			function sortTableCPN() {			
				$('#CPNISI').tablesorter( {		
					'headers': { 	                        
						11: { sorter : false },	
						12: { sorter : false },
						13: { sorter : false },
						14: { sorter : false } 
                    } 
                });
				$("#CPNISI").stickyTableHeaders();						
			}

			function sortTableP() {			
				$('#PISI').tablesorter( {		
					'headers': { 	                        
						19: { sorter : false },	
						20: { sorter : false },
						21: { sorter : false },
						22: { sorter : false } 
                    } 
                });		
				$("#PISI").stickyTableHeaders();				
			}

			function sortTableA() {			
				$('#AISI').tablesorter( {		
					'headers': { 	                        
						19: { sorter : false },	
						20: { sorter : false },
						21: { sorter : false },
						22: { sorter : false } 
                    } 
                });	
				$("#AISI").stickyTableHeaders();					
			}
			
        </script>

        <style type="text/css">     			
			#pubISI_J .pubISI_J_pagination ul li.inactive,
            #pubISI_J .pubISI_J_pagination ul li.inactive:hover,
			#pubISI_JA .pubISI_JA_pagination ul li.inactive,
            #pubISI_JA .pubISI_JA_pagination ul li.inactive:hover,
            #pubISI_PRP .pubISI_PRP_pagination ul li.inactive,
            #pubISI_PRP .pubISI_PRP_pagination ul li.inactive:hover,
            #pubISI_CP .pubISI_CP_pagination ul li.inactive,
            #pubISI_CP .pubISI_CP_pagination ul li.inactive:hover,
			#pubISI_OA .pubISI_OA_pagination ul li.inactive,
            #pubISI_OA .pubISI_OA_pagination ul li.inactive:hover,
			#pubISI_L .pubISI_L_pagination ul li.inactive,
            #pubISI_L .pubISI_L_pagination ul li.inactive:hover,
			#pubISI_CL .pubISI_CL_pagination ul li.inactive,
            #pubISI_CL .pubISI_CL_pagination ul li.inactive:hover,
			#pubISI_S .pubISI_S_pagination ul li.inactive,
            #pubISI_S .pubISI_S_pagination ul li.inactive:hover,
			#pubISI_JN .pubISI_JN_pagination ul li.inactive,
            #pubISI_JN .pubISI_JN_pagination ul li.inactive:hover,
			#pubISI_PRPN .pubISI_PRPN_pagination ul li.inactive,
            #pubISI_PRPN .pubISI_PRPN_pagination ul li.inactive:hover,
			#pubISI_SN .pubISI_SN_pagination ul li.inactive,
            #pubISI_SN .pubISI_SN_pagination ul li.inactive:hover,
			#pubISI_LN .pubISI_LN_pagination ul li.inactive,
            #pubISI_LN .pubISI_LN_pagination ul li.inactive:hover,
			#pubISI_CLN .pubISI_CLN_pagination ul li.inactive,
            #pubISI_CLN .pubISI_CLN_pagination ul li.inactive:hover,
			#pubISI_CPN .pubISI_CPN_pagination ul li.inactive,
            #pubISI_CPN .pubISI_CPN_pagination ul li.inactive:hover,
			#pubISI_P .pubISI_P_pagination ul li.inactive,
            #pubISI_P .pubISI_P_pagination ul li.inactive:hover,
			#pubISI_A .pubISI_A_pagination ul li.inactive,
            #pubISI_A .pubISI_A_pagination ul li.inactive:hover{
                background-color:#ededed;
                color:#bababa;
                border:1px solid #bababa;
                cursor: default;
            }
			
            #pubISI .pubISIdata ul li{
                list-style: none;
                font-family: verdana;
                margin: 5px 0 5px 0;
                color: #000;
                font-size: 13px;
            }
			
            #pubISI_J .pubISI_J_pagination,			
			#pubISI_JA .pubISI_JA_pagination,
            #pubISI_PRP .pubISI_PRP_pagination,
			#pubISI_CP .pubISI_CP_pagination,
			#pubISI_OA .pubISI_OA_pagination,			
			#pubISI_L .pubISI_L_pagination,
            #pubISI_CL .pubISI_CL_pagination,
			#pubISI_S .pubISI_S_pagination,
			#pubISI_JN .pubISI_JN_pagination,			
			#pubISI_PRPN .pubISI_PRPN_pagination,
            #pubISI_SN .pubISI_SN_pagination,
			#pubISI_LN .pubISI_LN_pagination,
			#pubISI_CLN .pubISI_CLN_pagination,			
			#pubISI_CPN .pubISI_CPN_pagination,
            #pubISI_P .pubISI_P_pagination,
			#pubISI_A .pubISI_A_pagination{
                width: 800px;
                height: 25px;
            }
			
            #pubISI_J .pubISI_J_pagination ul li, 
			#pubISI_JA .pubISI_JA_pagination ul li,
			#pubISI_PRP .pubISI_PRP_pagination ul li,
			#pubISI_CP .pubISI_CP_pagination ul li,
			#pubISI_OA .pubISI_OA_pagination ul li,			
			#pubISI_L .pubISI_L_pagination ul li,
            #pubISI_CL .pubISI_CL_pagination ul li,
			#pubISI_S .pubISI_S_pagination ul li,
			#pubISI_JN .pubISI_JN_pagination ul li,			
			#pubISI_PRPN .pubISI_PRPN_pagination ul li,
            #pubISI_SN .pubISI_SN_pagination ul li,
			#pubISI_LN .pubISI_LN_pagination ul li,
			#pubISI_CLN .pubISI_CLN_pagination ul li,			
			#pubISI_CPN .pubISI_CPN_pagination ul li,
            #pubISI_P .pubISI_P_pagination ul li,
			#pubISI_A .pubISI_A_pagination ul li {
                list-style: none;
                float: left;
                border: 1px solid #006699;
                padding: 2px 6px 2px 6px;
                margin: 0 3px 0 3px;
                font-family: arial;
                font-size: 14px;
                color: #006699;
                font-weight: bold;
                background-color: #f2f2f2;
            }
			
            #pubISI_J .pubISI_J_pagination ul li:hover,
			#pubISI_JA .pubISI_JA_pagination ul li:hover,
			#pubISI_PRP .pubISI_PRP_pagination ul li:hover,
			#pubISI_CP .pubISI_CP_pagination ul li:hover,
			#pubISI_OA .pubISI_OA_pagination ul li:hover,			
			#pubISI_L .pubISI_L_pagination ul li:hover,
            #pubISI_CL .pubISI_CL_pagination ul li:hover,
			#pubISI_S .pubISI_S_pagination ul li:hover,
			#pubISI_JN .pubISI_JN_pagination ul li:hover,			
			#pubISI_PRPN .pubISI_PRPN_pagination ul li:hover,
            #pubISI_SN .pubISI_SN_pagination ul li:hover,
			#pubISI_SN .pubISI_LN_pagination ul li:hover,
			#pubISI_CLN .pubISI_CLN_pagination ul li:hover,			
			#pubISI_CPN .pubISI_CPN_pagination ul li:hover,
            #pubISI_P .pubISI_P_pagination ul li:hover,
			#pubISI_A .pubISI_A_pagination ul li:hover{
                color: #fff;
                background-color: #006699;
                cursor: pointer;
            }
			.go_button
			{
				background-color:#f2f2f2;border:1px solid #006699;color:#cc0000;padding:2px 6px 2px 6px;cursor:pointer;position:absolute;margin-top:-1px;
			}
			.total
			{
				float:right;font-family:arial;color:#999;
			}
        </style>
					
				<div id="loading"></div>
		<fieldset class='normal'>			
			<div id="pubISI_J">
				<div class="pubISIdata"></div>
				<div class="pubISI_J_pagination"></div>
			</div>
			<div id="pubISI_JA">
				<div class="pubISIdata"></div>
				<div class="pubISI_JA_pagination"></div>
			</div>
			<div id="pubISI_PRP">
				<div class="pubISIdata"></div>
				<div class="pubISI_PRP_pagination"></div>
			</div>
			<div id="pubISI_CP">
				<div class="pubISIdata"></div>
				<div class="pubISI_CP_pagination"></div>
			</div>
			<div id="pubISI_OA">
				<div class="pubISIdata"></div>
				<div class="pubISI_OA_pagination"></div>
			</div>
			<div id="pubISI_L">
				<div class="pubISIdata"></div>
				<div class="pubISI_L_pagination"></div>
			</div>
			<div id="pubISI_CL">
				<div class="pubISIdata"></div>
				<div class="pubISI_CL_pagination"></div>
			</div>
			<div id="pubISI_S">
				<div class="pubISIdata"></div>
				<div class="pubISI_S_pagination"></div>
			</div>
			<div id="pubISI_JN">
				<div class="pubISIdata"></div>
				<div class="pubISI_JN_pagination"></div>
			</div>
			<div id="pubISI_PRPN">
				<div class="pubISIdata"></div>
				<div class="pubISI_PRPN_pagination"></div>
			</div>
			<div id="pubISI_SN">
				<div class="pubISIdata"></div>
				<div class="pubISI_SN_pagination"></div>
			</div>
			<div id="pubISI_LN">
				<div class="pubISIdata"></div>
				<div class="pubISI_LN_pagination"></div>
			</div>
			<div id="pubISI_CLN">
				<div class="pubISIdata"></div>
				<div class="pubISI_CLN_pagination"></div>
			</div>
			<div id="pubISI_CPN">
				<div class="pubISIdata"></div>
				<div class="pubISI_CPN_pagination"></div>
			</div>
			<div id="pubISI_P">
				<div class="pubISIdata"></div>
				<div class="pubISI_P_pagination"></div>
			</div>
			<div id="pubISI_A">
				<div class="pubISIdata"></div>
				<div class="pubISI_A_pagination"></div>
			</div>
		</fieldset>		

<?php } ?>
		