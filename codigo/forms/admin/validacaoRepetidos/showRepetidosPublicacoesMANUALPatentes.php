<?php
function showPublicacoesMANUALPatentes() { ?>
		<meta charset="UTF-8">
		<script type='text/javascript' src='../../js/jquery.tablesorter.js'></script>
		<script type="text/javascript" src="../../js/stickyTableHeaders.js"></script>
        <script type="text/javascript"> 
			var pagina = 1;
			
			function loading_show(){
				$('#loading').html("<center><img src='../../images/loading.gif'/></center>").fadeIn('fast');
			}
			function loading_hide(){
				$('#loading').fadeOut('fast');
			}     
			
			function loadDataPAT(page){ loading_show(); $.ajax ({type: "POST",url: "validacaoRepetidos/load_data_PAT.php", data: "page="+page, contentType: "application/x-www-form-urlencoded", dataType:"html",  success: function(msg){ loading_hide(); $("#pubManualPatentes").html(msg + "<br>"); sortTable(); ;} });}				
			     					         
			loadDataPAT(1); 
			
			function sortTable() {
				$('#pubManPat').tablesorter();
				$("#pubManPat").stickyTableHeaders();
			}
			
			/* -------------------------- PAT - J -----------------------------*/			
			function clickNumPAT(elem) {	
				page = $(elem).attr('p');	
				loadDataPAT(page);
				pagina = page;
				$("html, body").animate({ scrollTop: $("#pubManualPatentes .pubPATdata table caption").offset().top }, 600);
			}

			function procuraPAT() {
				page = parseInt($('#pubManualPatentes .goto').val());
				var no_of_pages = parseInt($('.total').attr('a'));
				if(page != 0 && page <= no_of_pages){
					loadDataPAT(page);
					pagina = page;
				}else{
					alert('Introduza um número entre 1 e '+no_of_pages);
					$('#pubManualPatentes .goto').val("").focus();
					return false;
				}
				$("html, body").animate({ scrollTop: $("#pubManualPatentes .pubPATdata table caption").offset().top }, 600);
			}
			/*-----------------------------------------------------------------*/
			
			
        </script>

        <style type="text/css">     			
			#pubManualPatentes .pubPATpagination ul li.inactive,
            #pubManualPatentes .pubPATpagination ul li.inactive:hover{
                background-color:#ededed;
                color:#bababa;
                border:1px solid #bababa;
                cursor: default;
            }
			
            #pubManualPatentes .pubPATdata ul li{
                list-style: none;
                font-family: verdana;
                margin: 5px 0 5px 0;
                color: #000;
                font-size: 13px;
            }
			
            #pubManualPatentes .pubPATpagination {
                width: 800px;
                height: 25px;
            }
			
            #pubManualPatentes .pubPATpagination ul li{
                list-style: none;
                float: left;
                border: 1px solid #006699;
                padding: 2px 6px 2px 6px;
                margin: 0 3px 0 3px;
                font-family: arial;
                font-size: 14px;
                color: #006699;
                font-weight: bold;
                background-color: #f2f2f2;
            }
			
            #pubManualPatentes .pubPATpagination ul li:hover{
                color: #fff;
                background-color: #006699;
                cursor: pointer;
            }
			
			.go_button
			{
				background-color:#f2f2f2;border:1px solid #006699;color:#cc0000;padding:2px 6px 2px 6px;cursor:pointer;position:absolute;margin-top:-1px;
			}
			
			.total
			{
				float:right;font-family:arial;color:#999;
			}
        </style>		
		<div id='loading' style='width:40px'>
		</div>		
		<fieldset class='normal'>						
			<div id="pubManualPatentes">
				<div class="pubPATdata"></div>
				<div class="pubPATpagination"></div>
			</div>			
		</fieldset>
	<?php 	
		
	} ?>