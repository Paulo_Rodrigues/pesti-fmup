﻿<?php

function showAcoesDivulgacao() { ?>
		<meta charset="UTF-8">
		<script type='text/javascript' src='../../js/jquery.tablesorter.js'></script>
		<script type="text/javascript" src="../../js/stickyTableHeaders.js"></script>
        <script type="text/javascript">     
			var pagina = 1;  
			
			function loading_show(){
				$('#loading').html("<center><img src='../../images/loading.gif'/></center>").fadeIn('fast');
			}
			function loading_hide(){
				$('#loading').fadeOut('fast');
			}      
				
			function loadDataAcaoDiv(page){ loading_show(); $.ajax ({type: "POST",url: "validacaoRepetidos/load_data_AcaoDiv.php", data: "page="+page, contentType: "application/x-www-form-urlencoded", dataType:"html",  success: function(msg){ loading_hide(); $("#acaodivulgacao").html(msg + "<br>"); sortTable(); ;} });}				
			     					         
			loadDataAcaoDiv(1); 			
			
			function sortTable() {
				$('#acaoDiv').tablesorter({
					'headers': { 						
                        2: {       // Change this to your column position
                            'sorter' : 'customDates' 
                        } ,						
						5: { 						
							sorter: false 
						}, 
						6: { 						
							sorter: false 
						}, 
						7: { 						
							sorter: false 
						}, 
						8: { 						
							sorter: false 
						} 				
                    } 
                });	
				$("#acaoDiv").stickyTableHeaders();
			}
			
			$.tablesorter.addParser({ 
				'id': 'customDates', 
				'is': function(string) { 
					 return false; 
					}, 
				'format': function(string) { 
					if (!string) {
					   return '';
					}    
					var thedate = string.split('-');                        
					var myDate = new Date(thedate[2],thedate[1],thedate[0]);
					return myDate.getTime();
				}, 
				'type': 'numeric' 
			}); 
			
			/* -------------------------- acaodivulgacao -----------------------------*/			
			function clickNumAcaoDiv(elem) {	
				page = $(elem).attr('p');	
				loadDataAcaoDiv(page);
				$("html, body").animate({ scrollTop: $("#load").offset().top }, 600);
				pagina = page;
			}

			function procuraAcaoDiv() {
				page = parseInt($('#acaodivulgacao .goto').val());
				var no_of_pages = parseInt($('.total').attr('a'));
				if(page != 0 && page <= no_of_pages){
					loadDataAcaoDiv(page);
					pagina = page;
				}else{
					alert('Introduza um número entre 1 e '+no_of_pages);
					$('#acaodivulgacao .goto').val("").focus();
					return false;
				}
				$("html, body").animate({ scrollTop: $("#load").offset().top }, 600);
			}
			/*-----------------------------------------------------------------*/
			
		$(document).ready(function() {
			var s = $("#cenas");
			
			var pos = s.position();					   
			$(window).scroll(function() {
				var windowpos = $(window).scrollTop();
				if (windowpos >= pos.top) {
					s.addClass('stick');
				} else {
					s.removeClass('stick');	
				}
			});			
		});
        </script>

        <style type="text/css">   
			.stick {
				position:fixed;
				top:100px;
				float:right;
				margin-left:1240px;
			}	
									
			#acaodivulgacao .pubPATpagination ul li.inactive,
            #acaodivulgacao .pubAcaoDivpagination ul li.inactive:hover{
                background-color:#ededed;
                color:#bababa;
                border:1px solid #bababa;
                cursor: default;
            }
			
            #acaodivulgacao .pubAcaoDivdata ul li{
                list-style: none;
                font-family: verdana;
                margin: 5px 0 5px 0;
                color: #000;
                font-size: 13px;
            }
			
            #acaodivulgacao .pubAcaoDivpagination {
                width: 800px;
                height: 25px;
            }
			
            #acaodivulgacao .pubAcaoDivpagination ul li{
                list-style: none;
                float: left;
                border: 1px solid #006699;
                padding: 2px 6px 2px 6px;
                margin: 0 3px 0 3px;
                font-family: arial;
                font-size: 14px;
                color: #006699;
                font-weight: bold;
                background-color: #f2f2f2;
            }
			
            #acaodivulgacao .pubAcaoDivpagination ul li:hover{
                color: #fff;
                background-color: #006699;
                cursor: pointer;
            }
			
			.go_button
			{
				background-color:#f2f2f2;border:1px solid #006699;color:#cc0000;padding:2px 6px 2px 6px;cursor:pointer;position:absolute;margin-top:-1px;
			}
			
			.total
			{
				float:right;font-family:arial;color:#999;
			}
        </style>
		
		<div id='loading' style='width:40px'>
		</div>		
			
		<div style='background: #DD6262; border-style:solid; border-color:#DD6262; width:60px; float:right; margin-right:500px;'>
			<center>
				<input type="image" src="../../images/icon_valid.png" onclick="validaTodosRepetidosAcaoDivulgacao();return false;">
				<b>
					<br>Valida Todos
					<br>Repetidos
				</b>
			</center>
		</div>	
	
		<div id='cenas' style='background: #eee8aa; border-style:solid; border-color:#FFD700; width:60px; float:right; margin-right:500px;'>
			<center>				
				<input type="image" src="../../images/icon_valid.png" onclick="validaRepetidosAcaoDivulgacao(pagina);return false;">
				<b>
					<br>Valida
					<br>Repetidos
				</b>					
			</center>			
		</div>
		
		
		<fieldset class='normal'>				
			<div id="acaodivulgacao"></div>	
			<div class="pubAcaoDivpagination"></div>
			</div>			
		</fieldset>
	<?php 			
		$db = new Database();
		$listaIds = '';
		$listaIds = $db->getIdsAcaoDivRep();
		return $listaIds;
	} ?>
