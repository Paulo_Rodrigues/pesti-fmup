<?php

echo "
	<style>
		.stick {
			position:fixed;
			top:100px;
			float:right;
			margin-left:1250px;
		}
	</style>
	
	<script>
		$(document).ready(function() {
			var s = $(\"#cenas\");
			
			var pos = s.position();					   
			$(window).scroll(function() {
				var windowpos = $(window).scrollTop();
				if (windowpos >= pos.top) {
					s.addClass(\"stick\");
				} else {
					s.removeClass(\"stick\");	
				}
			});			
		});
	</script>";
	
	
function showProjectos() { 
		echo "<div id='loading' style='width:40px'>
		</div>	
		<div style='background: #DD6262; border-style:solid; border-color:#DD6262; width:60px; float:right; margin-right:500px;'>
				<center>
					<input type=\"image\" src=\"../../images/icon_valid.png\" onclick=\"validaTodosRepetidosProjecto();return false;\">
					<b>
						<br>Valida Todos
						<br>Repetidos
					</b>
				</center>
			</div>		
		
		<div id='cenas' style='background: #eee8aa; border-style:solid; border-color:#FFD700; width:60px; float:right; margin-right:100px;'>
			<center>				
				<input type=\"image\" src=\"../../images/icon_valid.png\" onclick=\"validaRepetidosProjecto(pagina);return false;\">
				<b>
					<br>Valida
					<br>Repetidos
				</b>					
			</center>			
		</div>";?>
		<meta charset="UTF-8">
		<script type='text/javascript' src='../../js/jquery.tablesorter.js'></script>
		<script type="text/javascript" src="../../js/stickyTableHeaders.js"></script>
        <script type="text/javascript">    
			var pagina = 1;
			
			function loading_show(){
				$('#loading').html("<center><img src='../../images/loading.gif'/></center>").fadeIn('fast');
			}
			function loading_hide(){
				$('#loading').fadeOut('fast');
			}      
				
			function loadDataPRJ(page){ loading_show(); $.ajax ({type: "POST",url: "validacaoRepetidos/load_data_Projetos.php", data: "page="+page, contentType: "application/x-www-form-urlencoded", dataType:"html",  success: function(msg){ loading_hide(); $("#projectos_container").html(msg + "<br>"); sortTable(); ;} });}				
			     					         
			loadDataPRJ(1); 
			
			function sortTable() {
				$('#prj').tablesorter({
					'headers': { 						
                        10: {       // Change this to your column position
                            'sorter' : 'customDates' 
                        } ,	
						
						11: {       // Change this to your column position
                            'sorter' : 'customDates' 
                        } ,
						14: { sorter : false },	
						15: { sorter : false },
						16: { sorter : false },
						17: { sorter : false } 
                    } 
                });	
				$("#prj").stickyTableHeaders();
				
				$('#projectos img').click(function () {
					id = $("#chave-projectos").text();

					tipo = $("#tipo-projectos");
					entidade = $("#entidade-projectos");
					inst = $("#inst-projectos");
					monsol = $("#monsol-projectos");
					monapr = $("#monapr-projectos");
					monfmup = $("#monfmup-projectos");
					resp = $("#resp-projectos");
					codigo = $("#codigo-projectos");
					titulo = $("#titulo-projectos");
					dataini = $("#dataini-projectos");
					datafim = $("#datafim-projectos");
					link = $("#link-projectos");
					estado = $("#estado-projectos");

					tipo.val($("#projectos #td_projectos_tipoentidade_" + id).text());
					entidade.val($("#projectos #td_projectos_entidade_" + id).text());
					inst.val($("#projectos #td_projectos_acolhimento_" + id).text());
					monsol.val($("#projectos #td_projectos_montante_" + id).text().split('')[0]);
					monapr.val($("#projectos #td_projectos_montantea_" + id).text().split('')[0]);
					monfmup.val($("#projectos #td_projectos_montantefmup_" + id).text().split('')[0]);
					resp.val($("#projectos #td_projectos_invres_" + id).text().trim());
					codigo.val($("#projectos #td_projectos_codigo_" + id).text());
					titulo.val($("#projectos #td_projectos_titulo_" + id).text());
					dataini.val($("#projectos #td_projectos_dataini_" + id).text());
					datafim.val($("#projectos #td_projectos_datafim_" + id).text());
					link.val($("#projectos #td_projectos_link_" + id).text());
					
					if($("#projectos #td_projectos_estado_" + id).text().trim() == "Aprovado")
						estado.val(0);
					else 
						estado.val(1);
					
					tips = $(".validateTips-projectos");

					$("#dialog-form-projectos").dialog("open");
				});			
			}
			
			$.tablesorter.addParser({ 
				'id': 'customDates', 
				'is': function(string) { 
					 return false; 
					}, 
				'format': function(string) { 
					if (!string) {
					   return '';
					}    
					var thedate = string.split('-');                        
					var myDate = new Date(thedate[2],thedate[1],thedate[0]);
					return myDate.getTime();
				}, 
				'type': 'numeric' 
			}); 
			
			/* -------------------------- projectos_container -----------------------------*/			
			function clickNumPRJ(elem) {	
				page = $(elem).attr('p');	
				loadDataPRJ(page);
				$("html, body").animate({ scrollTop: $("#load").offset().top }, 600);
				pagina = page;
			}

			function procuraPRJ() {
				var page = parseInt($('#projectos_container .goto').val());
				var no_of_pages = parseInt($('.total').attr('a'));
				if(page != 0 && page <= no_of_pages){
					loadDataPRJ(page);
					pagina = page;
				}else{
					alert('Introduza um número entre 1 e '+no_of_pages);
					$('#projectos_container .goto').val("").focus();
					return false;
				}
				$("html, body").animate({ scrollTop: $("#load").offset().top }, 600);
			}
			/*-----------------------------------------------------------------*/
			
        </script>

        <style type="text/css">     			
			#projectos_container .pubPATpagination ul li.inactive,
            #projectos_container .pubPRJpagination ul li.inactive:hover{
                background-color:#ededed;
                color:#bababa;
                border:1px solid #bababa;
                cursor: default;
            }
			
            #projectos_container .pubPRJdata ul li{
                list-style: none;
                font-family: verdana;
                margin: 5px 0 5px 0;
                color: #000;
                font-size: 13px;
            }
			
            #projectos_container .pubPRJpagination {
                width: 800px;
                height: 25px;
            }
			
            #projectos_container .pubPRJpagination ul li{
                list-style: none;
                float: left;
                border: 1px solid #006699;
                padding: 2px 6px 2px 6px;
                margin: 0 3px 0 3px;
                font-family: arial;
                font-size: 14px;
                color: #006699;
                font-weight: bold;
                background-color: #f2f2f2;
            }
			
            #projectos_container .pubPRJpagination ul li:hover{
                color: #fff;
                background-color: #006699;
                cursor: pointer;
            }
			
			.go_button
			{
				background-color:#f2f2f2;border:1px solid #006699;color:#cc0000;padding:2px 6px 2px 6px;cursor:pointer;position:absolute;margin-top:-1px;
			}
			
			.total
			{
				float:right;font-family:arial;color:#999;
			}
        </style>	

			
			
		<fieldset class='normal'>						
			<div id="projectos_container">
			<div class="pubPRJpagination"></div>
			</div>			
		</fieldset>
		
	<?php 	
		echo "<p id='login' hidden>" . $_SESSION['login'] . "</p>
			  <p id='dep' hidden>" . $_SESSION['departamento'] . "</p>";
		
		$db = new Database();
		$listaIds = '';
		$listaIds = $db->getIdsPrjRep();
		return $listaIds;
	} ?>
	

	