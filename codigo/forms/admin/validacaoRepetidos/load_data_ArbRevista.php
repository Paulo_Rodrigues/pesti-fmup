﻿<?php
	ini_set('display_errors',1);
	ini_set('display_startup_errors',1);
	error_reporting(-1);
	
	//include "db.php";
	
	require_once '../../../classlib/Database.class.inc';
				
	$db = new Database();

	$db->connect();

	$msgArt = "<thead class='header'>
				<tr>
					<th>IDINV</th>
					<th>Tipo</th>
					<th>ISSN</th>
					<th>Revista</th>
					<th>Link</th>
					<th>Repetidos</th>
					<th>Original</th>
					<th></th>
					<th></th>
				</tr>
				</thead>
				<tbody>";
	$msgJ ='';	
	$page = $_POST['page'];
	$page -= 1;
	$per_page = 30;
	$start = $page * $per_page;
	$ids = array();
	$ids_final = '';
	$originais = array();		

	//Get 30 registos 
	$query_pag_data = "SELECT DISTINCT id_original from repetidos_arbRevista LIMIT $start, $per_page";
	$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error() );
	while ($row = mysql_fetch_array($result_pag_data)) {
		$ids[]=$row['id_original'];
	}
	
	if(!empty($ids)) {
		$ids = array_unique($ids);

		foreach ($ids as $i => $value) {
			$ids_final .= $ids[$i] . ',';
			$query_pag_data = "SELECT id_repetido from repetidos_arbRevista WHERE id_original = " . $ids[$i];
			
			$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error());
			
			$originais[$i] = array();
			$originais[$i][] = $ids[$i];
			while ($row = mysql_fetch_array($result_pag_data)) {
				if($row['id_repetido'] != null)
					$originais[$i][]=$row['id_repetido'];
			}
		}
		
		foreach ($originais as $i => $value) {		
			foreach ($originais[$i] as $j => $value1) {		
				
				//Verifica se existe algum registo repetido
				if($j == 0)	{
					if (count($originais[$i])==1) {
						$style ="style=\"overflow:hidden;\"";	
					} else {		
						$style ="style=\"background-color:#99FFFF; overflow:hidden;\"";	
					}				
				} else {
					$style ="style=\"background-color:#FFFF33; overflow:hidden;\"";	
				}		
				
				$msgJ .= "<tr>";
				
				$repetidos = $db->getRepetidosArbitragensRevistas($originais[$i][$j]);
				$idInv = $db->getIdInvByIdArbRevista($originais[$i][$j]);
				if($idInv) {
					if ($repetidos) {	
						$style ="style=\"background-color:#99FF66; overflow:hidden;\"";		
							$msgJ .= "<td id='". $originais[$i][$j]. "' $style>".$idInv ;							
						foreach ($repetidos as $x => $value2) {	
							if($idInv != $value2) {
								$msgJ .= "," . $value2 ;				
							}								
						}
						$msgJ .=  "</td>";
					} else {						
						$msgJ .=  "<td id='". $originais[$i][$j]. "' $style>".$idInv ."</td>";
					}			
					
					$query_pag_data = "SELECT * FROM arbitragensrevistas WHERE id =" .  $originais[$i][$j] . ";";	
					$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error() . " dentro do repetidos $query_pag_data");
					while ($row = mysql_fetch_array($result_pag_data)) {	
						$msgJ .= printArbRevistas($row,$style);							
					}
				}
			}					
		}
		$msgJ = "<table id='arbitragensRevistas' class='box-table-b'><caption><u><h2>Edição e Arbitragens Revistas</h2></u></caption>$msgArt $msgJ</tbody></table><p id='chave-arb-revistas' hidden></p></p><br>" . drawButtons() . "<br>";
		
		$msg = "<div id='arbRev'><ul>" . $msgJ . "</ul></div>"; // Content for Data
		echo $msg;
	} else {
		$msg = "<p>Não existem Edições nem Arbitragens Revistas a validar.</p>"; 
		echo $msg;	
	}
	$db->disconnect();

function printArbRevistas($row,$style) {
	$msg = '';
	$id=$row['ID'];		
	$tipo=$row['TIPO'];	
	$issn=htmlentities(utf8_decode($row['ISSN']));
	$tituloRevista=htmlentities(utf8_decode($row['TITULOREVISTA']));
	$link=htmlentities($row['LINK']);
	
	$msg .="<td id='td_arbrevistas_tipo_" . $id ."' $style>" . getTipoArbitragem($tipo) . "</td>";
	$msg .="<td id='td_arbrevistas_issn_" . $id ."' $style>".$issn."</td>";
	$msg .="<td id='td_arbrevistas_titulo_" . $id ."' $style>".$tituloRevista."</td>";
	$msg .="<td id='td_arbrevistas_link_" . $id ."' $style><a href='".$link."' target='_blank'>".$link."</a></td>";					
	$msg .="<td><input type='checkbox' id='rep_" . $id . "' onclick='addRepetido(" . $id . ");'>";
	$msg .="<td><input type='checkbox' id='orig_" . $id ."' onclick='addOriginal(" . $id . ");'>";
	$msg .="<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-arb-revistas').text('" . $id . "');\"></td>";
	$msg .="<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $id . "' value='apagar' name='navOption' onclick=\"$('#chave-arb-revistas').text('" . $id . "');apagarArbRevista();return false;\" ></center></td>";
	$msg .="</tr>";
	
	return $msg;
}
	
function drawButtons() {
	$db = new Database();
	$db->connect();
	
	$page = $_POST['page'];
	$cur_page = $page;
	$page -= 1;
	$per_page = 30;
	$previous_btn = true;
	$next_btn = true;
	$first_btn = true;
	$last_btn = true;
	$start = $page * $per_page;
	
	$msg = '';
	/* --------------------------------------------- */
	$query_pag_num = "SELECT COUNT(*) AS count FROM repetidos_arbRevista";
	$result_pag_num = mysql_query($query_pag_num,$db->conn);
	$row = mysql_fetch_array($result_pag_num);
	$count = $row['count'];
	$no_of_paginations = ceil($count / $per_page);

	/* ---------------Calculating the starting and endign values for the loop----------------------------------- */
	if ($cur_page >= 7) {
		$start_loop = $cur_page - 3;
		if ($no_of_paginations > $cur_page + 3)
			$end_loop = $cur_page + 3;
		else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
			$start_loop = $no_of_paginations - 6;
			$end_loop = $no_of_paginations;
		} else {
			$end_loop = $no_of_paginations;
		}
	} else {
		$start_loop = 1;
		if ($no_of_paginations > 7)
			$end_loop = 7;
		else
			$end_loop = $no_of_paginations;
	}
	/* ----------------------------------------------------------------------------------------------------------- */
	$msg .= "<div class='pubArbRevistasPagination'><ul>";
	
	for ($i = $start_loop; $i <= $end_loop; $i++) {
		if ($cur_page == $i)
			$msg .= "<li p='$i' style='color:#fff;background-color:#006699;' class='active' onclick='clickNumArbRevistas(this);'>{$i}</li>";
		else
			$msg .= "<li p='$i' class='active' onclick='clickNumArbRevistas(this);'>{$i}</li>";
	}

	$goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn_pub_ArbRevistas' class='go_button' value='Procurar' onclick='procuraArbRevistas();'/>&nbsp;";
	$total_string = "<span class='total' a='$no_of_paginations'>P&aacutegina <b>" . $cur_page . "</b> de <b>$no_of_paginations</b></span>";
	$msg = $msg . "</ul>" . $goto . $total_string . "</div>";  // Content for pagination
	
	return $msg;
}
	
function getTipoArbitragem($i){

	$db = new Database();
	$lValues =$db->getLookupValues("lista_tipoarbitragens");
	
	while ($row = mysql_fetch_assoc($lValues)) {	
		if($i==$row["ID"])
			return $row["DESCRICAO"];
	}

	$db->disconnect();				
}		
		
?>