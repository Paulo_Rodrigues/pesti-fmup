﻿<?php
	ini_set('display_errors',1);
	ini_set('display_startup_errors',1);
	error_reporting(-1);
	
	//include "db.php";
	
	require_once '../../../classlib/Database.class.inc';
				
	$db = new Database();
	$db->connect();

	
	$msgArt = "<thead class='header'>
				<tr>
					<th>IDINV</th>
					<th>Âmbito</th>
					<th>Tipo</th>
					<th>Data Início</th>
					<th>Data Fim</th>
					<th>Título</th>
					<th>Local</th>
					<th>Nº Participantes</th>
					<th width='100'>Link</th>					
					<th>Repetidos</th>
					<th>Original</th>
					<th></th>
					<th></th>
				</tr>
				</thead>
				<tbody>";
	$msgJ ='';	
	$page = $_POST['page'];
	$page -= 1;
	$per_page = 30;
	$start = $page * $per_page;
	$ids = array();
	$ids_final = '';
	$originais = array();		

	//Get 30 registos 
	$query_pag_data = "SELECT DISTINCT id_original from repetidos_cnf LIMIT $start, $per_page";
	$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error() );
	while ($row = mysql_fetch_array($result_pag_data)) {
		$ids[]=$row['id_original'];
	}
	
	if(!empty($ids)) {
		$ids = array_unique($ids);

		foreach ($ids as $i => $value) {
			$ids_final .= $ids[$i] . ',';
			$query_pag_data = "SELECT id_repetido from repetidos_cnf WHERE id_original = " . $ids[$i];
			$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error());
			
			$originais[$i] = array();
			$originais[$i][] = $ids[$i];
			while ($row = mysql_fetch_array($result_pag_data)) {
				if($row['id_repetido'] != null)
					$originais[$i][]=$row['id_repetido'];
			}
		}
		
		foreach ($originais as $i => $value) {		
			foreach ($originais[$i] as $j => $value1) {		
				
				//Verifica se existe algum registo repetido
				if($j == 0)	{
					if (count($originais[$i])==1) {
						$style ="style=\"overflow:hidden;\"";	
					} else {		
						$style ="style=\"background-color:#99FFFF; overflow:hidden;\"";	
					}				
				} else {
					$style ="style=\"background-color:#FFFF33; overflow:hidden;\"";	
				}		
				
				$msgJ .= "<tr>";
				
				$repetidos = $db->getRepetidosConferencia($originais[$i][$j]);
				$idInv = $db->getIdInvByIdCnf($originais[$i][$j]);
				if($idInv) {
					if ($repetidos) {	
						$style ="style=\"background-color:#99FF66; overflow:hidden;\"";		
							$msgJ .= "<td id='". $originais[$i][$j]. "' $style>".$idInv;							
						foreach ($repetidos as $x => $value2) {	
							if($idInv != $value2) {
								$msgJ .= "," . $value2 ;				
							}								
						}
						$msgJ .=  "</td>";
					} else {						
						$msgJ .=  "<td id='". $originais[$i][$j]. "' $style>".$idInv."</td>";
					}			
					
					$query_pag_data = "SELECT * FROM conferencias WHERE id =" .  $originais[$i][$j] . ";";	
					$result_pag_data = mysql_query($query_pag_data) or die('MySql Error' . mysql_error() . " dentro do repetidos $query_pag_data");
					while ($row = mysql_fetch_array($result_pag_data)) {	
						$msgJ .= printCnf($row,$style);							
					}
				}
			}					
		}
		$msgJ = "<table id='cnf' class='box-table-b'><caption><u><h2>Conferências Organizadas (LOC)</h2></u></caption>$msgArt $msgJ</tbody></table><p id='chave-conf' hidden></p></p><br>" . drawButtons() . "<br>";
		
		$msg = "<div id='conferencias'><ul>" . $msgJ . "</ul></div>"; // Content for Data
		echo $msg;
	} else {
		$msg = "<p>Não existem Conferências a validar.</p>"; 
		echo $msg;	
	}
	$db->disconnect();

function printCnf($row,$style) {
	$msg = '';
	$id=$row['ID'];		
	$ambito=$row['AMBITO'];	
	$tipo=$row['TIPO'];	
	$datainicio=htmlentities(utf8_decode($row['DATAINICIO']));
	$datafim=htmlentities(utf8_decode($row['DATAFIM']));
	$titulo=htmlentities(utf8_decode($row['TITULO']));
	$local=htmlentities($row['LOCAL']);
	$participantes=htmlentities($row['PARTICIPANTES']);	
	$link=htmlentities($row['LINK']);
				
	$msg .= "<td id='td_conferencias_ambito_" . $id . "' $style>" . getAmbitoConf($ambito);$msg .= "</td>";	
	$msg .= "<td id='td_conferencias_tipo_" . $id . "' $style>" . getTipoConf($tipo);$msg .= "</td>";
	$msg .= "<td id='td_conferencias_dataini_" . $id . "' $style>".$datainicio."</td>";	
	$msg .= "<td id='td_conferencias_datafim_" . $id. "' $style>".$datafim."</td>";	
	$msg .= "<td id='td_conferencias_titulo_" . $id. "' $style>".$titulo."</td>";
	$msg .= "<td id='td_conferencias_local_" . $id. "' $style>".$local."</td>";
	$msg .= "<td id='td_conferencias_npart_" . $id. "' $style>".$participantes."</td>";
	$msg .= "<td id='td_conferencias_link_" . $id. "' $style ><a href='".$link."' target='_blank' title=''".$link."''>".$link."</a></td>";					
	$msg .= "<td><input type='checkbox' id='rep_" . $id . "' onclick='addRepetido(" . $id . ");'>";
	$msg .= "<td><input type='checkbox' id='orig_" . $id ."' onclick='addOriginal(" . $id . ");'>";
	$msg .= "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-conf').text('" . $id. "');\"></td>";
	$msg .= "<td><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-conf').text('" . $id. "');apagarConferencia();return false;\" ></center></td>";
	$msg .= "</tr>";	 
	
	return $msg;
}
	
function drawButtons() {
	$db = new Database();
	$db->connect();
	
	$page = $_POST['page'];
	$cur_page = $page;
	$page -= 1;
	$per_page = 30;
	$previous_btn = true;
	$next_btn = true;
	$first_btn = true;
	$last_btn = true;
	$start = $page * $per_page;
	
	$msg = '';
	/* --------------------------------------------- */
	$query_pag_num = "SELECT COUNT(*) AS count FROM repetidos_cnf";
	$result_pag_num = mysql_query($query_pag_num,$db->conn);
	$row = mysql_fetch_array($result_pag_num);
	$count = $row['count'];
	$no_of_paginations = ceil($count / $per_page);

	/* ---------------Calculating the starting and endign values for the loop----------------------------------- */
	if ($cur_page >= 7) {
		$start_loop = $cur_page - 3;
		if ($no_of_paginations > $cur_page + 3)
			$end_loop = $cur_page + 3;
		else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
			$start_loop = $no_of_paginations - 6;
			$end_loop = $no_of_paginations;
		} else {
			$end_loop = $no_of_paginations;
		}
	} else {
		$start_loop = 1;
		if ($no_of_paginations > 7)
			$end_loop = 7;
		else
			$end_loop = $no_of_paginations;
	}
	/* ----------------------------------------------------------------------------------------------------------- */
	$msg .= "<div class='pubCNFpagination'><ul>";
	
	for ($i = $start_loop; $i <= $end_loop; $i++) {
		if ($cur_page == $i)
			$msg .= "<li p='$i' style='color:#fff;background-color:#006699;' class='active' onclick='clickNumCNF(this);'>{$i}</li>";
		else
			$msg .= "<li p='$i' class='active' onclick='clickNumCNF(this);'>{$i}</li>";
	}
	
	$goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn_pub_CNF' class='go_button' value='Procurar' onclick='procuraCNF();'/>&nbsp;";
	$total_string = "<span class='total' a='$no_of_paginations'>P&aacutegina <b>" . $cur_page . "</b> de <b>$no_of_paginations</b></span>";
	$msg = $msg . "</ul>" . $goto . $total_string . "</div>";  // Content for pagination
	
	return $msg;
}

function getTipoConf($i) { 
	$db = new Database();
	$lValues =$db->getLookupValues("lista_tipoconf");

	while ($row = mysql_fetch_assoc($lValues)) {	
		if($i==$row["ID"])
		return $row["DESCRICAO"];
	}

	$db->disconnect();				
}	

function getAmbitoConf($i) { 
	$db = new Database();
	$lValues =$db->getLookupValues("lista_ambitoconf");
	
	while ($row = mysql_fetch_assoc($lValues)) {	
		if($i==$row["ID"])
			return $row["DESCRICAO"];
	}

	$db->disconnect();				
}	
		
?>