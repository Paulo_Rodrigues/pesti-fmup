<?php

require_once '../../classlib/Missoes.class.inc';


function showAcoesMissoes($dadosNew) {
	 echo "<fieldset class='normal'><div id='missoes'>";
    
    echo "<table class='box-table-b'>
			<caption><u><h2>Missões</h2></u></caption>	
            <thead>
                <tr>
                    <th>ID Inv</th>
                    <th>Data de Inicio</th>
                    <th>Data de Fim</th>
                    <th>Motivacao</th>
                    <th>Instituição de Acolhimento</th>
                    <th>Pais</th>
                    <th>Âmbito de Tese</th>
					<th><b>Validar</b></th>
					<th><b>Eliminar</b></th>	
					<th><b>Informações</b></th>
					<th><b>Edição</b></th>
				</tr>
            </thead>
            <tbody>";	
			
	$acoes = array();
	$dadosNew2 = $dadosNew;
	
	while ( list($key, $val) = each($dadosNew) ) {	
		$acoes[$key]=array();
		
		while (list($key2, $val2) = each($dadosNew2)) {

			if($val->idreg == $val2->idreg) {							
				$acoes[$key][]=$dadosNew2[$key2];	  
				unset($dadosNew2[$key2]);				  
				unset($dadosNew[$key2]);
				continue;				
			}

		}
		unset($dadosNew2[$key]);
		reset($dadosNew2);
	}
	
		foreach ($acoes as $i => $value) {
		if($acoes[$i][0]->idreg != '') {
			$db = new Database();		
			$missao = $db->getMissaoFromDB($acoes[$i][0]->idreg);
			 echo "<tr>";
				echo "<td>" . $missao->idinv . "</td>";
				echo "<td id='td_missoes_dataini_" . $missao->id . "'>" . $missao->datainicio . "</td>";
				echo "<td id='td_missoes_datafim_" . $missao->id . "'>" . $missao->datafim . "</td>";
				echo "<td id='td_missoes_motivacao_" . $missao->id . "'>" . $missao->motivacao . "</td>";
				echo "<td id='td_missoes_instituicao_" . $missao->id . "'>" . $missao->instituicao . "</td>";
				echo "<td id='td_missoes_pais_" . $missao->id . "'>"; getPaisesMissoes($missao->pais); echo "</td>";
				echo "<td id='td_missoes_ambtese_" . $missao->id . "'>" . checkAmbTese($missao->ambtese) . "</td>";
				echo "<td></td>";
				echo "<td></td>";
				echo "<td></td>";	
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#dep').text('" . $acoes[$i][0]->departamento. "');$('#chave-missoes').text('" . $missao->id . "');\"></td>";
			echo "</tr>";
		}
		
		foreach ($acoes[$i] as $j => $value2) {
			if($acoes[$i][$j]->acao == 4) {	
				imprimeMissao($acoes[$i][$j], '');				
			} else {
				$db = new Database();		
				$missao = $db->getMissaoFromDB($acoes[$i][$j]->idreg);
				imprimeMissao($acoes[$i][$j], $missao);
			}		
		}		
	}	
		
	
	echo "</tbody>
    </table>
    <p id='chave-missoes' hidden></p>
    </div></fieldset>";
}


function imprimeMissao($dadosNew, $missao) {
	switch($dadosNew->acao) {
		case 1: {				
			//Edição
			$cena = explode("PAIS=", $dadosNew->descricao);
    
			$cena1 = explode(", DATAINICIO='", $cena[1]);
			$pais = $cena1[0];
			
			$cena2 = explode("', DATAFIM='", $cena1[1]);
			$datainicio = $cena2[0];
			
			$cena3 = explode("', INSTITUICAO='", $cena2[1]);
			$datafim = $cena3[0];
			
			$cena4 = explode("', MOTIVACAO='", $cena3[1]);
			$instituicao = $cena4[0];
			
			$cena5 = explode("', AMBTESE='", $cena4[1]);
			$motivacao = $cena5[0];
			
			$cena6 = explode("' where ", $cena5[1]);
			$ambtese = $cena6[0];
											
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$missao->idinv."</td>";			
				if($missao->datainicio != $datainicio) {
					echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>". $datainicio."</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>". $missao->datainicio."</td>";
				}
				
				if($missao->datafim != $datafim) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$datafim."</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$missao->datafim."</td>";
				}
				
				if($missao->motivacao != $motivacao) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $motivacao. "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>". $missao->motivacao. "</td>";
				}
				
				if($missao->instituicao != $instituicao) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $instituicao. "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>". $missao->instituicao. "</td>";
				}				
				
				if($missao->pais != $pais) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>"; getPaisesMissoes($pais); echo "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>"; getPaisesMissoes($missao->pais); echo "</td>";
				}	

				if($missao->ambtese != $ambtese) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>" . checkAmbTese($ambtese) . "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>" . checkAmbTese($missao->ambtese) . "</td>";
				}					
						
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			echo "</tr>";
		}
		break;
		case 2: {
			//Eliminação
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_warning.png\" name='navOption'></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;' colspan='13'>Esta habilitacao foi assinalado como dispensável. Se pretender confirmar esta ação, clique no visto. Caso contrário, clique na cruz.</td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
			echo "<tr>";
		}
		break;
		case 3: {
			//Observação
			echo "<tr>";
				echo "<td colspan='7' style='background-color:#99CCFF;'><u>Observações:</u> ";
				echo $dadosNew->descricao;
			echo "</td></tr>";
		}
		break;
		case 4: {
			$cena = explode("VALUES (",$dadosNew->descricao);								
			$cena1 = explode(",",$cena[1]);					
			$tudo = str_replace("'","",$cena1);
								
			echo "<tr style='border-top: solid yellow'>";					
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".str_replace(");","",$tudo[6])."</td>";	
				echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>". $tudo[1]."</td>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$tudo[2]."</td>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[4]. "</td>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[3]. "</td>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>"; getPaisesMissoes($tudo[0]); echo "</td>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>" . checkAmbTese($tudo[5]) . "</td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			echo "</tr>";
		}
		break;
	}		
}

function getPaisesMissoes($i)
{
    $db = new Database();
    $lValues = $db->getLookupValues("lista_paises");
    while ($row = mysql_fetch_assoc($lValues)) {
        if ($i == $row["ID"])
            echo $row["DESCRICAO"];
    }
    $db->disconnect();
}

function checkAmbTese($i)
{
    global $dadosDep;
    if ($i == 1)
        return "Sim";
    else
        return "Não";
}

?>