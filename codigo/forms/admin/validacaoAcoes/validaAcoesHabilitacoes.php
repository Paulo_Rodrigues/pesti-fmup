<?php
//PASSO 6 PMARQUES
require_once '../../classlib/Habilitacoes.class.inc';

function showAcoesHabilitacoes($dadosNew) {
	
	echo "<fieldset class='normal'>";
		echo "<div id='habilitacoes'>\n	
			<table class='box-table-b' id='habs'>
			  <caption><u><h2>Curso(s) a frequentar</h2></u></caption>
			<thead>
				<tr>
					<th>ID Inv</th>
					<th>Ano de Início</th>
					<th>Ano de Fim</th>
					<th>Curso</th>
					<th>Instituição</th>
					<th>Grau/Título</th>
					<th>Distinção</th>
					<th>Bolsa</th>
					<th>Título da Tese em Português</th>
					<th>Percentagem</th>
					<th>Orientador<p><i>Supervisor</i></p></th>
					<th>Instituição do Orientador<p><i>Supervisor Institution</i></p></th>
					<th>COrientador<p><i>COSupervisor</i></p></th>
					<th>Instituição do Coorientador<p><i>CoSupervisor Institution</i></p></th>
					
					<th><b>Validar</b></th>
					<th><b>Eliminar</b></th>	
					<th><b>Informações</b></th>
					<th><b>Edição</b></th>
				</tr>
			</thead>
			<tbody>";
			
	$acoes = array();
	$dadosNew2 = $dadosNew;
	
	while ( list($key, $val) = each($dadosNew) ) {	
		$acoes[$key]=array();
		
		while (list($key2, $val2) = each($dadosNew2)) {

			if($val->idreg == $val2->idreg) {							
				$acoes[$key][]=$dadosNew2[$key2];	  
				unset($dadosNew2[$key2]);				  
				unset($dadosNew[$key2]);
				continue;				
			}

		}
		unset($dadosNew2[$key]);
		reset($dadosNew2);
	}
	
	foreach ($acoes as $i => $value) {
		if($acoes[$i][0]->idreg != '') {
			$db = new Database();		
			$habilitacao = $db->getHabilitacaoFromDB($acoes[$i][0]->idreg);
			
			echo "<tr>";
				echo "<td>".$habilitacao->idinv . "</td>";
				echo "<td id='td_ano_ini_hab_" . $habilitacao->id . "'>";	
				getAnoInicio($habilitacao->anoinicio);
				echo "</td>";
				echo "<td id='td_ano_fim_hab_" . $habilitacao->id . "'>";
				getAnoFim($habilitacao->anofim);
				echo "</td>";			
				echo "<td id='td_curso_hab_" . $habilitacao->id . "'>".$habilitacao->curso."</td>";
				echo "<td id='td_instituicao_hab_" . $habilitacao->id . "''>".$habilitacao->instituicao."</td>";
				echo "<td id='td_grau_hab_" . $habilitacao->id . "'>";
				getGrau($habilitacao->grau);	
				echo "</td>";
				echo "<td id='td_dist_hab_" . $habilitacao->id . "'>";
				getDistincao($habilitacao->distincao);			
				echo "</td>";
				echo "<td id='td_bolsa_hab_" . $habilitacao->id . "'>";
				getBolsa($habilitacao->bolsa);	
				echo "</td>";
				echo "<td id='td_tit_hab_" . $habilitacao->id . "'>".$habilitacao->titulo."</td>";
				echo "<td id='td_perc_hab_" . $habilitacao->id . "'>".$habilitacao->percentagem."</td>";			
				echo "<td id='td_ori_hab_" . $habilitacao->id . "'>".$habilitacao->orientador."</td>";
				echo "<td id='td_ori_inst_hab_" . $habilitacao->id . "'>".$habilitacao->oinstituicao."</td>"; //TODO lookup Instituições
				echo "<td id='td_cori_hab_" . $habilitacao->id . "'>".$habilitacao->corientador."</td>";
				echo "<td id='td_cori_inst_hab_" . $habilitacao->id . "'>".$habilitacao->coinstituicao."</td>";
				echo "<td></td>";
				echo "<td></td>";
				echo "<td></td>";			
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#dep').text('" . $acoes[$i][0]->departamento. "');$('#chave-hab').text('". $habilitacao->id ."');\"></td>";
			echo "</tr>";
		}
		
		foreach ($acoes[$i] as $j => $value2) {
			if($acoes[$i][$j]->acao == 4) {	
				imprimeAcaoHabilitacao($acoes[$i][$j], '');				
			} else {
				$db = new Database();		
				$apresentacao = $db->getHabilitacaoFromDB($acoes[$i][$j]->idreg);
				imprimeAcaoHabilitacao($acoes[$i][$j], $apresentacao);
			}		
		}		
	}	
	
	echo "</tbody>
		</table>
		<p id='chave-hab' hidden></p>
	</div></fieldset>";
}


function imprimeAcaoHabilitacao($dadosNew, $habilitacao) {
	switch($dadosNew->acao) {
		case 1: {				
			//Edição		
	//echo $dadosNew->descricao;
	$cena = explode("anoinicio=",$dadosNew->descricao);
			
	$cena1 = explode(", anofim=",$cena[1]);
	$anoinicio = $cena1[0];
	
	$cena2 = explode(", curso='",$cena1[1]);
	$anofim = $cena2[0];
	
	$cena3 = explode("', instituicao='",$cena2[1]);
	$curso= $cena3[0];
	
	$cena4 = explode("', grau = ",$cena3[1]);
	$instituicao= $cena4[0];
	
	$cena5 = explode(", distincao = '",$cena4[1]);
	$grau= $cena5[0];
	
	$cena6 = explode("', bolsa= '",$cena5[1]);
	$distincao= $cena6[0];
				
	$cena7 = explode("', titulo = '",$cena6[1]);
	$bolsa= $cena7[0];
	
	$cena8 = explode("', percentagem = ",$cena7[1]);
	$titulo = $cena8[0];
	
	$cena9 = explode(", ORIENTADOR='",$cena8[1]);
	$percentagem = $cena9[0];

	$cena10 = explode("', OINSTITUICAO='",$cena9[1]);
	$orientador = $cena10[0];
	
	$cena11 = explode("', CORIENTADOR='",$cena10[1]);
	$oinstituicao = $cena11[0];
	
	$cena12 = explode("',COINSTITUICAO='",$cena11[1]);
	$corientador = $cena12[0];
	
	$cena13 = explode("' where id='",$cena12[1]);
	$coinstituicao = $cena13[0];
			
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$habilitacao->idinv."</td>";			
				if($habilitacao->anoinicio != $anoinicio) {
					echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>"; getAnoInicio($anoinicio); echo "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>"; getAnoInicio($habilitacao->anoinicio); echo "</td>";
				}
				
				if($habilitacao->anofim != $anofim) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>"; getAnoFim($anofim); echo "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>"; getAnoFim($habilitacao->anofim); echo"</td>";
				}
				
				if($habilitacao->curso != $curso) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$curso."</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$habilitacao->curso."</td>";
				}				
				
				if($habilitacao->instituicao != $instituicao) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$instituicao."</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$habilitacao->instituicao."</td>";
				}	
				
				if($habilitacao->grau != $grau) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>"; getGrau($grau); echo "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>"; getGrau($habilitacao->grau); echo"</td>";
				}	
			
				if($habilitacao->distincao != $distincao) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>"; getDistincao($distincao); echo "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>"; getDistincao($habilitacao->distincao); echo "</td>";
				}	
				
				if($habilitacao->bolsa != $bolsa) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>"; getBolsa($bolsa); echo "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>"; getBolsa($habilitacao->bolsa); echo "</td>";
				}	
			
				if($habilitacao->titulo != $titulo) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$titulo."</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$habilitacao->titulo."</td>";
				}
				
				if($habilitacao->percentagem != $percentagem) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$percentagem."</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$habilitacao->percentagem."</td>";
				}

				if($habilitacao->orientador != $orientador) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$orientador."</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$habilitacao->orientador."</td>";
				}
				if($habilitacao->oinstituicao != $oinstituicao) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$oinstituicao."</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$habilitacao->oinstituicao."</td>";
				}
				if($habilitacao->corientador != $corientador) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$corientador."</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$habilitacao->corientador."</td>";
				}
				if($habilitacao->coinstituicao != $coinstituicao) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$coinstituicao."</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$habilitacao->coinstituicao."</td>";
				}
					
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();return false;\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			echo "</tr>";
		}
		break;
		case 2: {
			//Eliminação
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_warning.png\" name='navOption'></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;' colspan='9'>Esta habilitacao foi assinalado como dispensável. Se pretender confirmar esta ação, clique no visto. Caso contrário, clique na cruz.</td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
			echo "<tr>";
		}
		break;
		case 3: {
			//Observação
			echo "<tr>";
				echo "<td colspan='10' style='background-color:#99CCFF;'><u>Observações:</u> ";
				echo $dadosNew->descricao;
			echo "</td></tr>";
		}
		break;
		case "4": {
			//Inserir
			$cena = explode("VALUES (",$dadosNew->descricao);
								
			$cena1 = explode(",",$cena[1]);		
			
			$tudo = str_replace("'","",$cena1);
			
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". str_replace(");","",$tudo[13])."</td>";		
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>"; getAnoInicio($tudo[0]); echo "</td>";			
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>"; getAnoFim($tudo[1]); echo"</td>";	
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$tudo[2]."</td>";			
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$tudo[3]."</td>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>"; getGrau($tudo[4]); echo"</td>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>"; getDistincao($tudo[5]); echo "</td>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>"; getBolsa($tudo[6]); echo "</td>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$tudo[7]."</td>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$tudo[8]."</td>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$tudo[9]."</td>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$tudo[10]."</td>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$tudo[11]."</td>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$tudo[12]."</td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			echo "</tr>";
		}
		break;
	}		
}

function getAnoInicio($i){	
	$db = new Database();
	$lValues =$db->getLookupValuesAnoInicio("lista_anoslectivos");
	while ($row = mysql_fetch_assoc($lValues)) {	
		if($i==$row["ID"])
			echo $row["DESCRICAO"];
	}	
	$db->disconnect();
}
			
function checkAnoInicio($id,$i){
	global $dadosDep;
	if($dadosDep[$i]->anoinicio==$id)
		return true;
	else 
		return false;
}

function getAnoFim($i) {
	$db = new Database();
	$lValues =$db->getLookupValues("lista_anoslectivos");
	while ($row = mysql_fetch_assoc($lValues)) {	
		if($i==$row["ID"]){
			echo $row["DESCRICAO"];
		}
	}
	$db->disconnect();	
}
			
function checkAnofim($id,$i){
	global $dadosDep;
	if($dadosDep[$i]->anofim==$id)
		return true;
	else 
		return false;
}

function getGrau($i) {
	$db = new Database();
	$lValues =$db->getLookupValues("lista_graucursos");	
	while ($row = mysql_fetch_assoc($lValues)) {	
		if($i==$row["ID"])
			echo $row["DESCRICAO"];
	}
	echo "</SELECT><br />\n";
	$db->disconnect();			
}	
			
function checkGrau($id,$i){
	global $dadosDep;
	if($dadosDep[$i]->grau==$id)
		return true;
	else 
		return false;
}

function getDistincao($i) {
	$db = new Database();
	$lValues =$db->getLookupValues("lista_distincaoValores");
	while ($row = mysql_fetch_assoc($lValues)) {
		if($i==$row["ID"])
			echo $row["DESCRICAO"];
	}
	$db->disconnect();
}

function checkDistincao($id,$i){
	global $dadosDep;
	if($dadosDep[$i]->distincao==$id)
		return true;
	else
		return false;
}

function getBolsa($i) {
	$db = new Database();
	$lValues =$db->getLookupValues("lista_TipoBolsas");
	while ($row = mysql_fetch_assoc($lValues)) {
		if($i==$row["ID"])
			echo $row["DESCRICAO"];
	}
	$db->disconnect();
}

function checkBolsa($id,$i){
	global $dadosDep;
	if($dadosDep[$i]->bolsa==$id)
		return true;
	else
		return false;
}
?>