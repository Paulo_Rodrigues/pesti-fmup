<?php

require_once '../../classlib/Publicacao.class.inc';

function showAcoesPublicacoesManualInternacional($dadosNew) {
	$outJMAN="";
	$outPRPMAN="";
	$outJAMAN="";
	$outLMAN="";
	$outCLMAN="";
	$outCPMAN="";
	$outOAMAN="";

	$tcolsJ="<thead>
				<tr>
					<th>IDINV</th>
					<th>ISSN</th>
					<th>REVISTA</th>
					<th>TÍTULO</th>
					<th>AUTORES</th>
					<th>VOLUME</th>
					<th>ISSUE</th>
					<th>PRIMEIRA PÁG.</th>
					<th>ÚLTIMA PÁG.</th>
					<th>LINK</th>
					<th>ESTADO</th>
					<th><b>Validar</b></th>
					<th><b>Eliminar</b></th>	
					<th><b>Informações</b></th>
					<th><b>Edição</b></th>
				</tr>
			</thead>";

	$tcolsCONF="<thead>
					<tr>
						<th>IDINV</th>
						<th>ISBN</th>
						<th>TÍTULO DA OBRA</th>
						<th>EDITORES</th>
						<th>TÍTULO ARTIGO</th>
						<th>AUTORES</th>
						<th>PRIMEIRA PÁG.</th>
						<th>ÚLTIMA PÁG.</th>
						<th>LINK</th>
						<th>ESTADO</th>
						<th><b>Validar</b></th>
						<th><b>Eliminar</b></th>	
						<th><b>Informações</b></th>
						<th><b>Edição</b></th>
					</tr>
				</thead>";

	$tcolsPRP="<thead>
					<tr>
						<th>IDINV</th>
						<th>ISSN</th>
						<th>TÍTULO DA OBRA</th>
						<th>EDITORES</th>
						<th>TÍTULO ARTIGO</th>
						<th>AUTORES</th>
						<th>PRIMEIRA PÁG.</th>
						<th>ÚLTIMA PÁG.</th>
						<th>LINK</th>
						<th>ESTADO</th>
						<th><b>Validar</b></th>
						<th><b>Eliminar</b></th>	
						<th><b>Informações</b></th>
						<th><b>Edição</b></th>
					</tr>
				</thead>";

	$tcolsCLIV="<thead>
					<tr>
						<th>IDINV</th>					
						<th>ISBN</th>
						<th>TÍTULO DA OBRA</th>
						<th>EDITORA</th>
						<th>EDITORES</th>			
						<th>TÍTULO ARTIGO</th>
						<th>AUTORES</th>
						<th>PRIMEIRA PÁG.</th>
						<th>ÚLTIMA PÁG.</th>
						<th>LINK</th>
						<th>ESTADO</th>
						<th><b>Validar</b></th>
						<th><b>Eliminar</b></th>	
						<th><b>Informações</b></th>
						<th><b>Edição</b></th>
					</tr>
				</thead>";

	$tcolsPAT="<thead>
					<tr>
						<th>IDINV</th>
						<th>Nº PATENTE</th>
						<th>IPC</th>
						<th>TÍTULO</th>
						<th>AUTORES</th>		
						<th>DATA de PUBLICAÇÃO</th>
						<th>LINK</th>
						<th>ESTADO</th>
						<th><b>Validar</b></th>
						<th><b>Eliminar</b></th>	
						<th><b>Informações</b></th>
						<th><b>Edição</b></th>
					</tr>
				</thead>";

	$tcolsLIV="<thead>
					<tr>
						<th>IDINV</th>
						<th>ISBN</th>
						<th>TÍTULO</th>
						<th>AUTORES</th>		
						<th>EDITOR</th>
						<th>EDITORA</th>
						<th>LINK</th>
						<th>ESTADO</th>
						<th><b>Validar</b></th>
						<th><b>Eliminar</b></th>	
						<th><b>Informações</b></th>
						<th><b>Edição</b></th>
					</tr>
				</thead>";
	
	$acoes = array();
	$dadosNew2 = $dadosNew;
	
	while ( list($key, $val) = each($dadosNew) ) {	
		$acoes[$key]=array();
		
		while (list($key2, $val2) = each($dadosNew2)) {

			if($val->idreg == $val2->idreg) {							
				$acoes[$key][]=$dadosNew2[$key2];	  
				unset($dadosNew2[$key2]);				  
				unset($dadosNew[$key2]);
				continue;				
			}

		}
		unset($dadosNew2[$key]);
		reset($dadosNew2);
	}	
	
	foreach ($acoes as $i => $value) {
		if($acoes[$i][0]->idreg != '') {
			$db = new Database();
			$publicacao = $db->getPublicacaoManualInternacionalFromDB($acoes[$i][0]->idreg);
			$tipo=$publicacao->tipofmup;

			switch($tipo){
				case 'J': { 
					$outJMAN=$outJMAN."<tr>";
						$outJMAN=$outJMAN."<td>".$publicacao->idinv."</td>";
						$outJMAN=$outJMAN."<td id='td_outJMAN_issn_" . $publicacao->id . "'>".$publicacao->issn."</td>";			
						$outJMAN=$outJMAN."<td id='td_outJMAN_nomepub_" . $publicacao->id . "'>".$publicacao->nomepublicacao."</td>";
						$outJMAN=$outJMAN."<td id='td_outJMAN_titulo_" . $publicacao->id . "'>".$publicacao->titulo."</td>";
						$outJMAN=$outJMAN."<td id='td_outJMAN_autores_" . $publicacao->id . "'>".$publicacao->autores."</td>";
						$outJMAN=$outJMAN."<td id='td_outJMAN_volume_" . $publicacao->id . "'>".$publicacao->volume."</td>";
						$outJMAN=$outJMAN."<td id='td_outJMAN_issue_" . $publicacao->id . "'>".$publicacao->issue."</td>";
						$outJMAN=$outJMAN."<td id='td_outJMAN_prim_" . $publicacao->id . "'>".$publicacao->primpagina."</td>";
						$outJMAN=$outJMAN."<td id='td_outJMAN_ult_" . $publicacao->id . "'>".$publicacao->ultpagina."</td>";
						$outJMAN=$outJMAN."<td id='td_outJMAN_link_" . $publicacao->id . "'><a href='".$publicacao->link."' target='_blank'>".$publicacao->link."</a></td>";
						$outJMAN=$outJMAN."<td id='td_outJMAN_estado_" . $publicacao->id . "'>".getEstadoPublicacaoMI($publicacao->estado)."</td>";						
						$outJMAN=$outJMAN. "<td></td>";
						$outJMAN=$outJMAN. "<td></td>";
						$outJMAN=$outJMAN. "<td></td>";			
						$outJMAN=$outJMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#tabela').text('" . $acoes[$i][0]->tabela. "');$('#dep').text('" . $acoes[$i][0]->departamento. "');$('#chave-pubIntMan-Art').text('" . $publicacao->id . "');$('#tabela').text('12');\"></td>";
					$outJMAN=$outJMAN."</tr>";						
				}
				break;	
				case 'PRP': {
					$outPRPMAN=$outPRPMAN."<tr>";
						$outPRPMAN=$outPRPMAN."<td>".$publicacao->idinv."</td>";
						$outPRPMAN=$outPRPMAN."<td id='td_outPRPMAN_issn_" . $publicacao->id . "'>".$publicacao->issn."</td>";
						$outPRPMAN=$outPRPMAN."<td id='td_outPRPMAN_numepub_" . $publicacao->id . "'>".$publicacao->nomepublicacao."</td>";
						$outPRPMAN=$outPRPMAN."<td id='td_outPRPMAN_titulo_" . $publicacao->id . "'>".$publicacao->titulo."</td>";
						$outPRPMAN=$outPRPMAN."<td id='td_outPRPMAN_autores_" . $publicacao->id . "'>".$publicacao->autores."</td>";
						$outPRPMAN=$outPRPMAN."<td id='td_outPRPMAN_volume_" . $publicacao->id . "'>".$publicacao->volume."</td>";
						$outPRPMAN=$outPRPMAN."<td id='td_outPRPMAN_issue_" . $publicacao->id . "'>".$publicacao->issue."</td>";
						$outPRPMAN=$outPRPMAN."<td id='td_outPRPMAN_prim_" . $publicacao->id . "'>".$publicacao->primpagina."</td>";
						$outPRPMAN=$outPRPMAN."<td id='td_outPRPMAN_ult_" . $publicacao->id . "'>".$publicacao->ultpagina."</td>";
						$outPRPMAN=$outPRPMAN."<td id='td_outPRPMAN_link_" . $publicacao->id . "'><a href='".$publicacao->link."' target='_blank'>".$publicacao->link."</a></td>";
						$outPRPMAN=$outPRPMAN."<td id='td_outPRPMAN_estado_" . $publicacao->id . "'>".getEstadoPublicacaoMI($publicacao->estado)."</td>";				
						$outPRPMAN=$outPRPMAN. "<td></td>";
						$outPRPMAN=$outPRPMAN. "<td></td>";
						$outPRPMAN=$outPRPMAN. "<td></td>";			
						$outPRPMAN=$outPRPMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#tabela').text('" . $acoes[$i][0]->tabela. "');$('#dep').text('" . $acoes[$i][0]->departamento. "');$('#chave-pubIntMan-PRP').text('" . $publicacao->id . "');$('#tabela').text('12');\"></td>";
					$outPRPMAN=$outPRPMAN."</tr>";	
				}
				break;	
				case 'JA': { 				
					$outJAMAN=$outJAMAN."<tr>";
						$outJAMAN=$outJAMAN."<td>".$publicacao->idinv."</td>";
						$outJAMAN=$outJAMAN."<td id='td_outJAMAN_issn_" . $publicacao->id . "'>".$publicacao->issn."</td>";	
						$outJAMAN=$outJAMAN."<td id='td_outJAMAN_nomepub_" . $publicacao->id . "'>".$publicacao->nomepublicacao."</td>";
						$outJAMAN=$outJAMAN."<td id='td_outJAMAN_titulo_" . $publicacao->id . "'>".$publicacao->titulo."</td>";						
						$outJAMAN=$outJAMAN."<td id='td_outJAMAN_autores_" . $publicacao->id . "'>".$publicacao->autores."</td>";
						$outJAMAN=$outJAMAN."<td id='td_outJAMAN_volume_" . $publicacao->id . "'>".$publicacao->volume."</td>";
						$outJAMAN=$outJAMAN."<td id='td_outJAMAN_issue_" . $publicacao->id . "'>".$publicacao->issue."</td>";	
						$outJAMAN=$outJAMAN."<td id='td_outJAMAN_prim_" . $publicacao->id . "'>".$publicacao->primpagina."</td>";
						$outJAMAN=$outJAMAN."<td id='td_outJAMAN_ult_" . $publicacao->id . "'>".$publicacao->ultpagina."</td>";
						$outJAMAN=$outJAMAN."<td id='td_outJAMAN_link_" . $publicacao->id . "'><a href='".$publicacao->link."' target='_blank'>".$publicacao->link."</a></td>";
						$outJAMAN=$outJAMAN."<td id='td_outJAMAN_estado_" . $publicacao->id . "'>".getEstadoPublicacaoMI($publicacao->estado)."</td>";		
						$outJAMAN=$outJAMAN. "<td></td>";
						$outJAMAN=$outJAMAN. "<td></td>";
						$outJAMAN=$outJAMAN. "<td></td>";			
						$outJAMAN=$outJAMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#tabela').text('" . $acoes[$i][0]->tabela. "');$('#dep').text('" . $acoes[$i][0]->departamento. "');$('#chave-pubIntMan-Sum').text('" . $publicacao->id . "');$('#tabela').text('12');\"></td>";
				   $outJAMAN=$outJAMAN."</tr>";	
				}
				break;	
				case 'CP': { 
					$outCPMAN=$outCPMAN."<tr>";
						$outCPMAN=$outCPMAN."<td>".$publicacao->idinv."</td>";
						$outCPMAN=$outCPMAN."<td id='td_outCPMAN_isbn_" . $publicacao->id . "'>".$publicacao->isbn."</td>";
						$outCPMAN=$outCPMAN."<td id='td_outCPMAN_nomepub_" . $publicacao->id . "'>".$publicacao->nomepublicacao."</td>";
						$outCPMAN=$outCPMAN."<td id='td_outCPMAN_editor_" . $publicacao->id . "'>".$publicacao->editor."</td>";
						$outCPMAN=$outCPMAN."<td id='td_outCPMAN_titulo_" . $publicacao->id . "'>".$publicacao->titulo."</td>";
						$outCPMAN=$outCPMAN."<td id='td_outCPMAN_autores_" . $publicacao->id . "'>".$publicacao->autores."</td>";
						$outCPMAN=$outCPMAN."<td id='td_outCPMAN_prim_" . $publicacao->id . "'>".$publicacao->primpagina."</td>";
						$outCPMAN=$outCPMAN."<td id='td_outCPMAN_ult_" . $publicacao->id . "'>".$publicacao->ultpagina."</td>";
						$outCPMAN=$outCPMAN."<td id='td_outCPMAN_link_" . $publicacao->id . "'>".$publicacao->link."</textarea></td>";
						$outCPMAN=$outCPMAN."<td id='td_outCPMAN_estado_" . $publicacao->id . "'>".getEstadoPublicacaoMI($publicacao->estado)."</td>";
						$outCPMAN=$outCPMAN. "<td></td>";
						$outCPMAN=$outCPMAN. "<td></td>";
						$outCPMAN=$outCPMAN. "<td></td>";			
						$outCPMAN=$outCPMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#tabela').text('" . $acoes[$i][0]->tabela. "');$('#dep').text('" . $acoes[$i][0]->departamento. "');$('#chave-pubIntMan-Con').text('" . $publicacao->id . "');$('#tabela').text('12');\"></td>";
					$outCPMAN=$outCPMAN."</tr>";
				}
				break;
				case 'OA': { 			
					$outOAMAN=$outOAMAN."<tr>";
						$outOAMAN=$outOAMAN."<td >".$publicacao->idinv."</td>";
						$outOAMAN=$outOAMAN."<td id='td_outOAMAN_isbn_" . $publicacao->id . "'>".$publicacao->isbn."</td>";
						$outOAMAN=$outOAMAN."<td id='td_outOAMAN_nomepub_" . $publicacao->id . "'>".$publicacao->nomepublicacao."</td>";
						$outOAMAN=$outOAMAN."<td id='td_outOAMAN_editor_" . $publicacao->id . "'>".$publicacao->editor."</td>";
						$outOAMAN=$outOAMAN."<td id='td_outOAMAN_titulo_" . $publicacao->id . "'>".$publicacao->titulo."</td>";
						$outOAMAN=$outOAMAN."<td id='td_outOAMAN_autores_" . $publicacao->id . "'>".$publicacao->autores."</td>";
						$outOAMAN=$outOAMAN."<td id='td_outOAMAN_prim_" . $publicacao->id . "'>".$publicacao->primpagina."</td>";
						$outOAMAN=$outOAMAN."<td id='td_outOAMAN_ult_" . $publicacao->id . "'>".$publicacao->ultpagina."</td>";
						$outOAMAN=$outOAMAN."<td id='td_outOAMAN_link_" . $publicacao->id . "'>".$publicacao->link."</td>";
						$outOAMAN=$outOAMAN."<td id='td_outOAMAN_estado_" . $publicacao->id . "'>".getEstadoPublicacaoMI($publicacao->estado)."</td>";
						$outOAMAN=$outOAMAN. "<td></td>";
						$outOAMAN=$outOAMAN. "<td></td>";
						$outOAMAN=$outOAMAN. "<td></td>";			
						$outOAMAN=$outOAMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#tabela').text('" . $acoes[$i][0]->tabela. "');$('#dep').text('" . $acoes[$i][0]->departamento. "');$('#chave-pubIntMan-Oth').text('" . $publicacao->id . "');$('#tabela').text('12');\"></td>";
					$outOAMAN=$outOAMAN."</tr>";	
				}			
				break;	
				case 'L': { 			
					$outLMAN=$outLMAN."<tr>";
						$outLMAN=$outLMAN."<td>".$publicacao->idinv."</td>";
						$outLMAN=$outLMAN."<td id='td_outLMAN_isbn_" . $publicacao->id . "'>".$publicacao->isbn."</td>";
						$outLMAN=$outLMAN."<td id='td_outLMAN_titulo_" . $publicacao->id . "'>".$publicacao->titulo."</td>";
						$outLMAN=$outLMAN."<td id='td_outLMAN_autores_" . $publicacao->id . "'>".$publicacao->autores."</td>";
						$outLMAN=$outLMAN."<td id='td_outLMAN_editor_" . $publicacao->id . "'>".$publicacao->editor."</td>";
						$outLMAN=$outLMAN."<td id='td_outLMAN_editora_" . $publicacao->id . "'>".$publicacao->editora."</td>";
						$outLMAN=$outLMAN."<td id='td_outLMAN_link_" . $publicacao->id . "'>".$publicacao->link."</td>";
						$outLMAN=$outLMAN."<td id='td_outLMAN_estado_" . $publicacao->id . "'>".getEstadoPublicacaoMI($publicacao->estado)."</td>";
						$outLMAN=$outLMAN. "<td></td>";
						$outLMAN=$outLMAN. "<td></td>";
						$outLMAN=$outLMAN. "<td></td>";			
						$outLMAN=$outLMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#tabela').text('" . $acoes[$i][0]->tabela. "');$('#dep').text('" . $acoes[$i][0]->departamento. "');$('#chave-pubIntMan-Liv').text('" . $publicacao->id . "');$('#tabela').text('12');\"></td>";
					$outLMAN=$outLMAN."</tr>";	
				}
				break;
				
				case 'CL': { 				
					$outCLMAN=$outCLMAN."<tr>";
						$outCLMAN=$outCLMAN."<td>".$publicacao->idinv."</td>";
						$outCLMAN=$outCLMAN."<td id='td_outCLMAN_isbn_" . $publicacao->id . "'>".$publicacao->isbn."</td>";
						$outCLMAN=$outCLMAN."<td id='td_outCLMAN_nomepub_" . $publicacao->id . "'>".$publicacao->nomepublicacao."</td>";
						$outCLMAN=$outCLMAN."<td id='td_outCLMAN_editora_" . $publicacao->id . "'>".$publicacao->editora."</td>";
						$outCLMAN=$outCLMAN."<td id='td_outCLMAN_editor_" . $publicacao->id . "'>".$publicacao->editor."</td>";						
						$outCLMAN=$outCLMAN."<td id='td_outCLMAN_titulo_" . $publicacao->id . "'>".$publicacao->titulo."</td>";
						$outCLMAN=$outCLMAN."<td id='td_outCLMAN_autores_" . $publicacao->id . "'>".$publicacao->autores."</td>";
						$outCLMAN=$outCLMAN."<td id='td_outCLMAN_prim_" . $publicacao->id . "'>".$publicacao->primpagina."</td>";
						$outCLMAN=$outCLMAN."<td id='td_outCLMAN_ult_" . $publicacao->id . "'>".$publicacao->ultpagina."</td>";
						$outCLMAN=$outCLMAN."<td id='td_outCLMAN_link_" . $publicacao->id . "'>".$publicacao->link."</td>";
						$outCLMAN=$outCLMAN."<td id='td_outCLMAN_estado_" . $publicacao->id . "'>".getEstadoPublicacaoMI($publicacao->estado)."</td>";	
						$outCLMAN=$outCLMAN. "<td></td>";
						$outCLMAN=$outCLMAN. "<td></td>";
						$outCLMAN=$outCLMAN. "<td></td>";							   	
						$outCLMAN=$outCLMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#tabela').text('" . $acoes[$i][0]->tabela. "');$('#dep').text('" . $acoes[$i][0]->departamento. "');$('#chave-pubIntMan-CLiv').text('" . $publicacao->id . "');$('#tabela').text('12');\"></td>";
					$outCLMAN=$outCLMAN."</tr>";
				}
				break;	
			}
		}
		
		foreach ($acoes[$i] as $j => $value2) {
			if($acoes[$i][$j]->acao == 4) {	
				$tipo = getTipoFmup($acoes[$i][$j]->descricao);
				switch($tipo){
					case 'J': { $outJMAN=$outJMAN.imprimePubManIntJ($acoes[$i][$j], '');	} break;
					case 'PRP': { $outPRPMAN=$outPRPMAN.imprimePubManIntJ($acoes[$i][$j], ''); } break;
					case 'JA': { $outJAMAN=$outJAMAN.imprimePubManIntJ($acoes[$i][$j], ''); } break;	
					case 'CP': { $outCPMAN=$outCPMAN.imprimePubManIntCP($acoes[$i][$j], ''); } break;
					case 'OA': { $outOAMAN=$outOAMAN.imprimePubManIntCP($acoes[$i][$j], ''); } break;	
					case 'L': { $outLMAN=$outLMAN.imprimePubManIntL($acoes[$i][$j], ''); } break;
					case 'CL': { $outCLMAN=$outCLMAN.imprimePubManIntCL($acoes[$i][$j], ''); } break;
				}	
			} else {
				$db = new Database();		
				$publicacao = $db->getPublicacaoManualInternacionalFromDB($acoes[$i][$j]->idreg);	
				$tipo = $publicacao->tipofmup;
				switch($tipo){
					case 'J': { $outJMAN=$outJMAN.imprimePubManIntJ($acoes[$i][$j], $publicacao);	} break;
					case 'PRP': { $outPRPMAN=$outPRPMAN.imprimePubManIntJ($acoes[$i][$j], $publicacao); } break;
					case 'JA': { $outJAMAN=$outJAMAN.imprimePubManIntJ($acoes[$i][$j], $publicacao); } break;	
					case 'CP': { $outCPMAN=$outCPMAN.imprimePubManIntCP($acoes[$i][$j], $publicacao); } break;
					case 'OA': { $outOAMAN=$outOAMAN.imprimePubManIntCP($acoes[$i][$j], $publicacao); } break;	
					case 'L': { $outLMAN=$outLMAN.imprimePubManIntL($acoes[$i][$j], $publicacao); } break;
					case 'CL': { $outCLMAN=$outCLMAN.imprimePubManIntCL($acoes[$i][$j], $publicacao); } break;
				}	
			}		
		}		
	}	
	
	
	echo "<fieldset class='normal'><div id='pubManualInternacional'>";
		echo "<p id='tabela' hidden></p>";
		echo"<div id='pubManualInt-Art'><table id='pubManInt-Art' class='box-table-b'><caption><u><h2>Publicações Manuais Internacionais</h2></u></caption><caption>ARTIGOS</caption>";echo $tcolsJ;echo "<tbody>";echo $outJMAN;echo "</table><p id='chave-pubIntMan-Art' hidden></p></div><br>";
		echo"<div id='pubManualInt-Rev'><table id='pubManInt-Rev' class='box-table-b'><caption >PEER REVIEW PROCEEDINGS</caption>";echo $tcolsJ;echo "<tbody>";echo $outPRPMAN;echo "</tbody></table><p id='chave-pubIntMan-PRP' hidden></p></div><br>";
		echo"<div id='pubManualInt-Sum'><table id='pubManInt-Sum' class='box-table-b'><caption >SUMÁRIOS<p><i>JOURNAL ABSTRACTS</i></p></caption>";echo $tcolsJ;echo "<tbody>";echo $outJAMAN;echo "</tbody></table><p id='chave-pubIntMan-Sum' hidden></p></div><br>";		
		echo"<div id='pubManualInt-Liv'><table id='pubManInt-Liv' class='box-table-b'><caption >LIVROS<p><i>BOOKS</i></p></caption>";echo $tcolsLIV;echo "<tbody>";echo $outLMAN;echo "</tbody></table><p id='chave-pubIntMan-Liv' hidden></p></div><br>";
		echo"<div id='pubManualInt-CLiv'><table id='pubManInt-CLiv' class='box-table-b'><caption >CAPITULOS DE LIVROS<p><i>BOOK CHAPTERS</i></p></caption>";echo $tcolsCLIV;echo "<tbody>";echo $outCLMAN;echo "</tbody></table><p id='chave-pubIntMan-CLiv' hidden></p></div><br>";
		echo"<div id='pubManualInt-Con'><table id='pubManInt-Con' class='box-table-b'><caption >CONFERENCE PROCEEDINGS</caption>";echo $tcolsCONF;echo "<tbody>";echo $outCPMAN;echo "</tbody></table><p id='chave-pubIntMan-Con' hidden></p></div><br>";
		echo"<div id='pubManualInt-Oth'><table id='pubManInt-Oth' class='box-table-b'><caption >OUTROS SUMÁRIOS<br>OTHER ABSTRACTS</caption>";echo $tcolsCONF;echo "<tbody>";echo $outOAMAN;echo "</tbody></table><p id='chave-pubIntMan-Oth' hidden></p></div><br>";
	echo "</div></fieldset>";
}

function getTipoFmup($descricao) {
	$cena = explode("VALUES (",$descricao);								
	$cena1 = explode(",",$cena[1]);					
	$tudo = str_replace("'","",$cena1);
	return $tudo[1];
}
	
function imprimePubManIntJ($dadosNew, $publicacao) {
	$return = '';
	switch($dadosNew->acao) {
		case 1: {				
			//Edição			
			$cena = explode("ISSN='",$dadosNew->descricao);
				
			$cena1 = explode("', AUTORES='",$cena[1]);
			$issn = $cena1[0];
			
			$cena2 = explode("', NOMEPUBLICACAO='",$cena1[1]);
			$autores = $cena2[0];
			
			$cena3 = explode("', VOLUME='",$cena2[1]);
			$nomepublicacao= $cena3[0];
			
			$cena4 = explode("', ISSUE='",$cena3[1]);
			$volume= $cena4[0];	
			
			$cena5 = explode("', PRIMPAGINA='",$cena4[1]);
			$issue= $cena5[0];	
			
			$cena6 = explode("', ULTPAGINA='",$cena5[1]);
			$primpagina= $cena6[0];	
			
			$cena7 = explode("', TITULO='",$cena6[1]);
			$ultpagina= $cena7[0];	
			
			$cena8 = explode("', LINK='",$cena7[1]);
			$titulo= $cena8[0];	
			
			$cena9 = explode("', ESTADO=",$cena8[1]);
			$link= $cena9[0];	
			
			$cena10 = explode(" where",$cena9[1]);
			$estado= $cena10[0];	
										
			$return=$return. "<tr style='border-top: solid yellow'>";	
				$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$publicacao->idinv."</td>";		
				if($publicacao->issn != $issn) {
					$return=$return. "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$issn. "</td>";
				} else {
					$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$publicacao->issn."</td>";
				}
				
				if($publicacao->nomepublicacao != $nomepublicacao) {
					$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $nomepublicacao. "</td>";
				} else {
					$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$publicacao->nomepublicacao. "</td>";
				}		

				if(trim($publicacao->titulo) != trim($titulo)) {
					$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $titulo. "</td>";
				} else {
					$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$publicacao->titulo. "</td>";
				}	
				
				if($publicacao->autores != $autores) {
					$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $autores. "</td>";
				} else {
					$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$publicacao->autores. "</td>";
				}	

				if($publicacao->volume != $volume) {
					$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $volume. "</td>";
				} else {
					$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$publicacao->volume. "</td>";
				}	
				
				if($publicacao->issue != $issue) {
					$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $issue. "</td>";
				} else {
					$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$publicacao->issue. "</td>";
				}	
				
				if($publicacao->primpagina != $primpagina) {
					$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $primpagina. "</td>";
				} else {
					$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$publicacao->primpagina. "</td>";
				}	
				
				if($publicacao->ultpagina != $ultpagina) {
					$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $ultpagina. "</td>";
				} else {
					$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$publicacao->ultpagina. "</td>";
				}	

				if($publicacao->link != $link) {
					$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'><a href='". $link. "' target='_blank'>". $link. "</a></td>";
				} else {
					$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><a href='".$publicacao->link. "' target='_blank'>".$publicacao->link. "</a></td>";
				}
					
				if($publicacao->estado != $estado) {
					$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".getEstadoPublicacaoMI($estado). "</td>";
				} else {
					$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".getEstadoPublicacaoMI($publicacao->estado). "</td>";
				}
										
				$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			$return=$return. "</tr>";
		}
		break;
		case 2: {
			//Eliminação
			$return=$return. "<tr style='border-top: solid yellow'>";
				$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_warning.png\" name='navOption'></center></td>";
				$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;' colspan='13'>Esta habilitacao foi assinalado como dispensável. Se pretender confirmar esta ação, clique no visto. Caso contrário, clique na cruz.</td>";
				$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
			$return=$return. "<tr>";
		}
		break;
		case 3: {
			//Observação
			$return=$return. "<tr>";
				$return=$return. "<td colspan='11' style='background-color:#99CCFF;'><u>Observações:</u> ";
				$return=$return. $dadosNew->descricao;
			$return=$return. "</td></tr>";
		}
		break;
		case 4: {
			$cena = explode("VALUES (",$dadosNew->descricao);								
			$cena1 = explode(",",$cena[1]);					
			$tudo = str_replace("'","",$cena1);
								
			$return=$return. "<tr style='border-top: solid yellow'>";
				$return=$return. "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$tudo[10]. "</td>";			
				$return=$return. "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$tudo[0]. "</td>";
				$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[1]. "</td>";
				$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[2]. "</td>";
				$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[3]. "</td>";
				$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[4]. "</td>";
				$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[5]. "</td>";
				$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[6]. "</td>";
				$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[7]. "</td>";
				$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'><a href='". $tudo[8]. "' target='_blank'>". $tudo[8]. "</a></td>";
				$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".getEstadoPublicacaoMI($tudo[9]). "</td>";
				$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			$return=$return. "</tr>";
		}
		break;
	}
	
	return $return;
}


function imprimePubManIntCP($dadosNew, $publicacao) {
	$return = '';
	switch($dadosNew->acao) {
		case 1: {				
			//Edição			
			$cena = explode("ISBN='",$dadosNew->descricao);
						
			$cena1 = explode("', NOMEPUBLICACAO='",$cena[1]);
			$isbn = trim($cena1[0]);
			
			$cena2 = explode("', EDITOR='",$cena1[1]);
			$nomepublicacao = trim($cena2[0]);
			
			$cena3 = explode("', TITULO='",$cena2[1]);
			$editor= trim($cena3[0]);
			
			$cena4 = explode("', AUTORES='",$cena3[1]);
			$titulo= trim($cena4[0]);	
			
			$cena5 = explode("', PRIMPAGINA='",$cena4[1]);
			$autores= trim($cena5[0]);	
			
			$cena6 = explode("', ULTPAGINA='",$cena5[1]);
			$primpagina= trim($cena6[0]);	
			
			$cena7 = explode("', LINK='",$cena6[1]);
			$ultpagina= trim($cena7[0]);	
			
			$cena8 = explode("', ESTADO=",$cena7[1]);
			$link= trim($cena8[0]);	
			
			$cena9 = explode(" where",$cena8[1]);
			$estado= trim($cena9[0]);	
										
			$return=$return. "<tr style='border-top: solid yellow'>";	
				$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$publicacao->idinv."</td>";		
				if(trim($publicacao->isbn) != trim($isbn)) {				
					$return=$return. "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$isbn."</td>";
				} else {
					$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$publicacao->isbn."</td>";
				}
				
				if(trim($publicacao->nomepublicacao) != trim($nomepublicacao)) {
					$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $nomepublicacao. "</td>";
				} else {
					$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$publicacao->nomepublicacao. "</td>";
				}		

				if(trim($publicacao->editor) != trim($editor)) {
					$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $editor. "</td>";
				} else {
					$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$publicacao->editor. "</td>";
				}	
				
				if(trim($publicacao->titulo) != trim($titulo)) {
					$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $titulo. "</td>";
				} else {
					$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$publicacao->titulo. "</td>";
				}	

				if(trim($publicacao->autores) != trim($autores)) {
					$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $autores. "</td>";
				} else {
					$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$publicacao->autores. "</td>";
				}	
								
				if(trim($publicacao->primpagina) != trim($primpagina)) {
					$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $primpagina. "</td>";
				} else {
					$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$publicacao->primpagina. "</td>";
				}	
				
				if(trim($publicacao->ultpagina) != trim($ultpagina)) {
					$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $ultpagina. "</td>";
				} else {
					$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$publicacao->ultpagina. "</td>";
				}	

				if(trim($publicacao->link) != trim($link)) {
					$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'><a href='". $link. "' target='_blank'>". $link. "</a></td>";
				} else {
					$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><a href='".$publicacao->link. "' target='_blank'>".$publicacao->link. "</a></td>";
				}
					
				if(trim($publicacao->estado) != trim($estado)) {
					$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".getEstadoPublicacaoMI($estado). "</td>";
				} else {
					$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".getEstadoPublicacaoMI($publicacao->estado). "</td>";
				}
										
				$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			$return=$return. "</tr>";
		}
		break;
		case 2: {
			//Eliminação
			$return=$return. "<tr style='border-top: solid yellow'>";
				$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_warning.png\" name='navOption'></center></td>";
				$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;' colspan='13'>Esta habilitacao foi assinalado como dispensável. Se pretender confirmar esta ação, clique no visto. Caso contrário, clique na cruz.</td>";
				$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
			$return=$return. "<tr>";
		}
		break;
		case 3: {
			//Observação
			$return=$return. "<tr>";
				$return=$return. "<td colspan='10' style='background-color:#99CCFF;'><u>Observações:</u> ";
				$return=$return. $dadosNew->descricao;
			$return=$return. "</td></tr>";
		}
		break;
		case 4: {
			$cena = explode("VALUES (",$dadosNew->descricao);								
			$cena1 = explode(",",$cena[1]);					
			$tudo = str_replace("'","",$cena1);
								
			$return=$return. "<tr style='border-top: solid yellow'>";
				$return=$return. "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$tudo[0]. "</td>";			
				$return=$return. "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$tudo[3]."</td>";
				$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[4]. "</td>";
				$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[10]. "</td>";
				$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'></td>";
				$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[5]. "</td>";
				$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[7]. "</td>";
				$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[8]. "</td>";
				$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'><a href='' target='_blank'></a></td>";
				$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".getEstadoPublicacaoMI($tudo[9]). "</td>";
				$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			$return=$return. "</tr>";
		}
		break;
	}
	
	return $return;
}

function imprimePubManIntL($dadosNew, $publicacao) {
	$return = '';
	switch($dadosNew->acao) {
		case 1: {				
			//Edição		
			$cena = explode("ISBN='",$dadosNew->descricao);
				
			$cena1 = explode("', AUTORES='",$cena[1]);
			$isbn = $cena1[0];
			
			$cena2 = explode("', EDITOR='",$cena1[1]);
			$autores = $cena2[0];
			
			$cena3 = explode("', EDITORA='",$cena2[1]);
			$editor= $cena3[0];
			
			$cena4 = explode("', TITULO='",$cena3[1]);
			$editora= $cena4[0];	
			
			$cena5 = explode("', LINK='",$cena4[1]);
			$titulo= $cena5[0];	
			
			$cena6 = explode("', ESTADO=",$cena5[1]);
			$link= $cena6[0];	
			
			$cena7 = explode(" where",$cena6[1]);
			$estado= $cena7[0];	
								
			$return=$return. "<tr style='border-top: solid yellow'>";	
				$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$publicacao->idinv."</td>";		
				if(trim($publicacao->isbn) != trim($isbn)) {				
					$return=$return. "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$isbn."</td>";
				} else {
					$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$publicacao->isbn."</td>";
				}	
				
				if(trim($publicacao->titulo) != trim($titulo)) {
					$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $titulo. "</td>";
				} else {
					$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$publicacao->titulo. "</td>";
				}	

				if(trim($publicacao->autores) != trim($autores)) {
					$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $autores. "</td>";
				} else {
					$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$publicacao->autores. "</td>";
				}	
								
				if(trim($publicacao->editor) != trim($editor)) {
					$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $editor. "</td>";
				} else {
					$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$publicacao->editor. "</td>";
				}	
				
				if(trim($publicacao->editora) != trim($editora)) {
					$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $editora. "</td>";
				} else {
					$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$publicacao->editora. "</td>";
				}	

				if(trim($publicacao->link) != trim($link)) {
					$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'><a href='". $link. "' target='_blank'>". $link. "</a></td>";
				} else {
					$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><a href='".$publicacao->link. "' target='_blank'>".$publicacao->link. "</a></td>";
				}
					
				if(trim($publicacao->estado) != trim($estado)) {
					$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".getEstadoPublicacaoMI($estado). "</td>";
				} else {
					$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".getEstadoPublicacaoMI($publicacao->estado). "</td>";
				}
										
				$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			$return=$return. "</tr>";
		}
		break;
		case 2: {
			//Eliminação
			$return=$return. "<tr style='border-top: solid yellow'>";
				$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_warning.png\" name='navOption'></center></td>";
				$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;' colspan='13'>Esta habilitacao foi assinalado como dispensável. Se pretender confirmar esta ação, clique no visto. Caso contrário, clique na cruz.</td>";
				$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
			$return=$return. "<tr>";
		}
		break;
		case 3: {
			//Observação
			$return=$return. "<tr>";
				$return=$return. "<td colspan='8' style='background-color:#99CCFF;'><u>Observações:</u> ";
				$return=$return. $dadosNew->descricao;
			$return=$return. "</td></tr>";
		}
		break;
		case 4: {
			$cena = explode("VALUES (",$dadosNew->descricao);								
			$cena1 = explode(",",$cena[1]);					
			$tudo = str_replace("'","",$cena1);
								
			$return=$return. "<tr style='border-top: solid yellow'>";
				$return=$return. "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$tudo[0]. "</td>";	
				$return=$return. "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$tudo[4]."</td>";
				$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[5]. "</td>";
				$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[6]. "</td>";
				$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[16]. "</td>";
				$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'></td>";
				$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'></td>";
				$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".getEstadoPublicacaoMI($tudo[17]). "</td>";
				$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			$return=$return. "</tr>";
		}
		break;
	}
	
	return $return;
}

function imprimePubManIntCL($dadosNew, $publicacao) {
	$return = '';
	switch($dadosNew->acao) {
		case 1: {				
			//Edição		
			$cena = explode("ISBN='",$dadosNew->descricao);
				
			$cena1 = explode("', NOMEPUBLICACAO='",$cena[1]);
			$isbn = $cena1[0];
			
			$cena2 = explode("', EDITORA='",$cena1[1]);
			$nomepublicacao = $cena2[0];
			
			$cena3 = explode("', EDITOR='",$cena2[1]);
			$editora= $cena3[0];
			
			$cena4 = explode("', TITULO='",$cena3[1]);
			$editor= $cena4[0];	
			
			$cena5 = explode("', AUTORES='",$cena4[1]);
			$titulo= $cena5[0];	
			
			$cena6 = explode("', PRIMPAGINA='",$cena5[1]);
			$autores= $cena6[0];	
			
			$cena7 = explode("', ULTPAGINA='",$cena6[1]);
			$primpagina= $cena7[0];	
			
			$cena8 = explode("', LINK='",$cena7[1]);
			$ultpagina= $cena8[0];	
			
			$cena9 = explode("', ESTADO=",$cena8[1]);
			$link= $cena9[0];	
			
			$cena10 = explode(" where",$cena9[1]);
			$estado= $cena10[0];
			
			$return=$return. "<tr style='border-top: solid yellow'>";	
				$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$publicacao->idinv."</td>";		
				if(trim($publicacao->isbn) != $isbn) {				
					$return=$return. "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$isbn."</td>";
				} else {
					$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$publicacao->isbn."</td>";
				}	
				
				if(trim($publicacao->nomepublicacao) != $nomepublicacao) {
					$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $nomepublicacao. "</td>";
				} else {
					$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$publicacao->nomepublicacao. "</td>";
				}	
				
				if(trim($publicacao->editora) != $editora) {
					$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $editora. "</td>";
				} else {
					$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$publicacao->editora. "</td>";
				}	
				
				if(trim($publicacao->editor) != $editor) {
					$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $editor. "</td>";
				} else {
					$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$publicacao->editor. "</td>";
				}	

				if(trim($publicacao->titulo) != $titulo) {
					$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $titulo. "</td>";
				} else {
					$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$publicacao->titulo. "</td>";
				}	
				
				if(trim($publicacao->autores) != $autores) {
					$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $autores. "</td>";
				} else {
					$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$publicacao->autores. "</td>";
				}	
				
				if(trim($publicacao->primpagina) != $primpagina) {
					$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $primpagina. "</td>";
				} else {
					$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$publicacao->primpagina. "</td>";
				}
				
				if(trim($publicacao->ultpagina) != $ultpagina) {
					$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $ultpagina. "</td>";
				} else {
					$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$publicacao->ultpagina. "</td>";
				}

				if($publicacao->link != $link) {
					$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'><a href='". $link. "' target='_blank'>". $link. "</a></td>";
				} else {
					$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><a href='".$publicacao->link. "' target='_blank'>".$publicacao->link. "</a></td>";
				}
					
				if($publicacao->estado != $estado) {
					$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".getEstadoPublicacaoMI($estado). "</td>";
				} else {
					$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".getEstadoPublicacaoMI($publicacao->estado). "</td>";
				}
										
				$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			$return=$return. "</tr>";
		}
		break;
		case 2: {
			//Eliminação
			$return=$return. "<tr style='border-top: solid yellow'>";
				$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_warning.png\" name='navOption'></center></td>";
				$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;' colspan='13'>Esta habilitacao foi assinalado como dispensável. Se pretender confirmar esta ação, clique no visto. Caso contrário, clique na cruz.</td>";
				$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
			$return=$return. "<tr>";
		}
		break;
		case 3: {
			//Observação
			$return=$return. "<tr>";
				$return=$return. "<td colspan='11' style='background-color:#99CCFF;'><u>Observações:</u> ";
				$return=$return. $dadosNew->descricao;
			$return=$return. "</td></tr>";
		}
		break;
		case 4: {
			$cena = explode("VALUES (",$dadosNew->descricao);								
			$cena1 = explode(",",$cena[1]);					
			$tudo = str_replace("'","",$cena1);
								
			$return=$return. "<tr style='border-top: solid yellow'>";
				$return=$return. "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$tudo[0]. "</td>";		
				$return=$return. "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$tudo[3]."</td>";
				$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[2]. "</td>";				
				$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[6]. "</td>";
				$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[7]. "</td>";				
				$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[4]. "</td>";
				$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[5]. "</td>";
				$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[8]. "</td>";
				$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[9]. "</td>";
				$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'><a href='". $tudo[10]. "' target='_blank'>". $tudo[10]. "</a></td>";
				$return=$return. "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".getEstadoPublicacaoMI($tudo[11]). "</td>";
				$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				$return=$return. "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			$return=$return. "</tr>";
		}
		break;
	}
	
	return $return;
}

function getEstadoPublicacaoMI($i) {
	$db = new Database();
	$lValues =$db->getLookupValues("lista_estadoPublicacoes");
	$texto="";
	while ($row = mysql_fetch_assoc($lValues)) {
		if($i==$row["ID"])
			$texto=$row["DESCRICAO"];
	}
	$db->disconnect();
	return $texto;
}
?>
