<?php

require_once '../../classlib/OutrasActividades.class.inc';

function showAcoesOutrasActividades($dadosNew) {	
	echo "<fieldset class='normal'>
			<div id='outact'>";
				
	echo "<table class='box-table-b'>
			<caption><u><h2>Outras Atividades</h2></u></caption>	
			  <thead>
				<tr>
					<th>IDINV</th>
					<th>Monografias de apoio às aulas ou a trabalho clínico</th>
					<th>Livros Didáticos</th>
					<th>Livros de Divulgação</th>
					<th>Vídeos, jogos, aplicações</th>
					<th>Páginas Internet</th>
					<th><b>Validar</b></th>
					<th><b>Eliminar</b></th>	
					<th><b>Informações</b></th>
					<th><b>Edição</b></th>
				</tr>
			  </thead>
			  <tbody>";
	
	$acoes = array();
	$dadosNew2 = $dadosNew;
	
	while ( list($key, $val) = each($dadosNew) ) {	
		$acoes[$key]=array();
		
		while (list($key2, $val2) = each($dadosNew2)) {

			if($val->idreg == $val2->idreg) {							
				$acoes[$key][]=$dadosNew2[$key2];	  
				unset($dadosNew2[$key2]);				  
				unset($dadosNew[$key2]);
				continue;				
			}

		}
		unset($dadosNew2[$key]);
		reset($dadosNew2);
	}
	
	foreach ($acoes as $i => $value) {
		if($acoes[$i][0]->idreg != '') {
			$db = new Database();		
			$outAct = $db->getOutActFromDB($acoes[$i][0]->idreg);
			echo "<tr>";
				echo "<td>".$outAct->idinv."</td>";
				echo "<td id='td_outact_texapoio_" . $outAct->id . "'>".$outAct->texapoio."</td>";
				echo "<td id='td_outact_livdidaticos_" . $outAct->id . "'>".$outAct->livdidaticos."</td>";
				echo "<td id='td_outact_livdivulgacao_" . $outAct->id . "'>".$outAct->livdivulgacao."</td>";
				echo "<td id='td_outact_app_" . $outAct->id . "'>".$outAct->app."</td>";
				echo "<td id='td_outact_webp_" . $outAct->id . "'>".$outAct->webp."</td>";
				echo "<td></td>";
				echo "<td></td>";
				echo "<td></td>";	
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#dep').text('" . $acoes[$i][0]->departamento. "');$('#chave-outact').text('" . $outAct->id . "');\"></td>";
			echo "</tr>";
		}
		
		foreach ($acoes[$i] as $j => $value2) {
			if($acoes[$i][$j]->acao == 4) {	
				imprimeOutAct($acoes[$i][$j], '');				
			} else {
				$db = new Database();		
				$outAct = $db->getOutActFromDB($acoes[$i][$j]->idreg);
				imprimeOutAct($acoes[$i][$j], $outAct);
			}		
		}		
	}				
	
	echo "</tbody>
    </table>
    <p id='chave-outact' hidden></p>
</div></fieldset>";
}

function imprimeOutAct($dadosNew, $outAct) {
	switch($dadosNew->acao) {
		case 1: {				
			//Edição
			$cena = explode("TEXAPOIO=",$dadosNew->descricao);
						
			$cena1 = explode(", LIVDIDATICOS=",$cena[1]);
			$texapoio = $cena1[0];
			
			$cena2 = explode(", LIVDIVULGACAO=",$cena1[1]);
			$livdidaticos = $cena2[0];
			
			$cena3 = explode(", APP=",$cena2[1]);
			$livdivulgacao= $cena3[0];
			
			$cena4 = explode(", WEBP=",$cena3[1]);
			$app= $cena4[0];
			
			$cena5 = explode(" where",$cena4[1]);
			$webp = $cena5[0];
			
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$outAct->idinv."</td>";			
				if($outAct->texapoio != $texapoio) {
					echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$texapoio."</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$outAct->texapoio."</td>";
				}
				
				if($outAct->livdidaticos != $livdidaticos) {
					echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$livdidaticos."</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$outAct->livdidaticos."</td>";
				}
				
				if($outAct->livdivulgacao != $livdivulgacao) {
					echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$livdivulgacao."</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$outAct->livdivulgacao."</td>";
				}
				
				if($outAct->app != $app) {
					echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$app."</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$outAct->app."</td>";
				}
				
				if($outAct->webp != $webp) {
					echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$webp."</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$outAct->webp."</td>";
				}			
				
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			echo "</tr>";
		}
		break;
		case 2: {
			//Eliminação
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_warning.png\" name='navOption'></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;' colspan='13'>Esta habilitacao foi assinalado como dispensável. Se pretender confirmar esta ação, clique no visto. Caso contrário, clique na cruz.</td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
			echo "<tr>";
		}
		break;
		case 3: {
			//Observação
			echo "<tr>";
				echo "<td colspan='6' style='background-color:#99CCFF;'><u>Observações:</u> ";
				echo $dadosNew->descricao;
			echo "</td></tr>";
		}
		break;
		case 4: {
			$cena = explode("VALUES (",$dadosNew->descricao);								
			$cena1 = explode(",",$cena[1]);					
			$tudo = str_replace("'","",$cena1);
								
			echo "<tr style='border-top: solid yellow'>";					
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".str_replace(");","",$tudo[5])."</td>";	
				echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$tudo[0]."</td>";
				echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$tudo[1]."</td>";
				echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$tudo[2]."</td>";
				echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$tudo[3]."</td>";
				echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$tudo[4]."</td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			echo "</tr>";
		}
		break;
	}		
}

?>