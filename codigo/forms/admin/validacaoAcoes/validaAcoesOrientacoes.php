<?php

require_once '../../classlib/Orientacao.class.inc';

function showAcoesOrientacoes($dadosNew) {
	echo "<fieldset class='normal'>
		  <div id='orientacoes'>\n";

	echo "<table class='box-table-b'>
			<caption><u><h2>Orientações</h2></u></caption>	
			<thead>
				<tr>
					<th>IDINV</th>
					<th>Tipo de Orientação</th>
					<th>Tipo de Orientador</th>
					<th>Estado</th>
					<th>Título da Tese em Português</th>
					<th><b>Validar</b></th>
					<th><b>Eliminar</b></th>	
					<th><b>Informações</b></th>
					<th><b>Edição</b></th>
				</tr>
			</thead>
			<tbody>";
	
	$acoes = array();
	$dadosNew2 = $dadosNew;
	
	while ( list($key, $val) = each($dadosNew) ) {	
		$acoes[$key]=array();
		
		while (list($key2, $val2) = each($dadosNew2)) {

			if($val->idreg == $val2->idreg) {							
				$acoes[$key][]=$dadosNew2[$key2];	  
				unset($dadosNew2[$key2]);				  
				unset($dadosNew[$key2]);
				continue;				
			}

		}
		unset($dadosNew2[$key]);
		reset($dadosNew2);
	}
	
	foreach ($acoes as $i => $value) {
		if($acoes[$i][0]->idreg != '') {
			$db = new Database();		
			$orientacao = $db->getOrientacaoFromDB($acoes[$i][0]->idreg);
			echo "<tr>";
				echo "<td>".$orientacao->idinv."</td>";
				echo "<td id='td_orientacoes_tipo_" . $orientacao->id . "'>";getTipoOrientacao($orientacao->tipo_orientacao);	echo "</td>";
				echo "<td id='td_orientacoes_tipoori_" . $orientacao->id . "'>";getTipoOrientador($orientacao->tipo_orientador);echo "</td>";
				echo "<td id='td_orientacoes_estado_" . $orientacao->id . "'>";	getOrientacaoEstado($orientacao->tipo_orientacao);echo "</td>";
				echo "<td id='td_orientacoes_titulo_" . $orientacao->id . "'>".$orientacao->titulo."</td>";
				echo "<td></td>";
				echo "<td></td>";
				echo "<td></td>";	
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#dep').text('" . $acoes[$i][0]->departamento. "');$('#chave-orientacoes').text('" . $orientacao->id . "');\"></td>";
			echo "</tr>";	   
		}
		
		foreach ($acoes[$i] as $j => $value2) {
			if($acoes[$i][$j]->acao == 4) {	
				imprimeOrientacao($acoes[$i][$j], '');				
			} else {
				$db = new Database();		
				$orientacao = $db->getOrientacaoFromDB($acoes[$i][$j]->idreg);
				imprimeOrientacao($acoes[$i][$j], $orientacao);
			}		
		}		
	}	
	
	 echo "</tbody>
    </table>
    <p id='chave-orientacoes' hidden></p>
</div></fieldset>";   
}

function imprimeOrientacao($dadosNew, $orientacao) {
	switch($dadosNew->acao) {
		case 1: {				
			//Edição
			$cena = explode("TIPO_ORIENTACAO=", $dadosNew->descricao);
			
			$cena1 = explode(", TIPO_ORIENTADOR=", $cena[1]);
			$tipo_orientacao = $cena1[0];
			
			$cena2 = explode(", ESTADO=", $cena1[1]);
			$tipo_orientador = $cena2[0];
			
			$cena3 = explode(", TITULO='", $cena2[1]);
			$estado = $cena3[0];
			
			$cena4 = explode("' where", $cena3[1]);
			$titulo = $cena4[0];    
											
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$orientacao->idinv."</td>";			
				if($orientacao->tipo_orientacao != $tipo_orientacao) {
					echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>";getTipoOrientacao($tipo_orientacao);	echo"</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>";getTipoOrientacao($orientacao->tipo_orientacao); echo"</td>";
				}
				
				if($orientacao->tipo_orientador != $tipo_orientador) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>";getTipoOrientador($tipo_orientador); echo"</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>";getTipoOrientador($orientacao->tipo_orientador); echo"</td>";
				}
				
				if($orientacao->estado != $estado) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>"; getOrientacaoEstado($estado);echo "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>"; getOrientacaoEstado($orientacao->estado);echo "</td>";
				}
				
				if($orientacao->titulo != $titulo) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $titulo. "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>". $orientacao->titulo. "</td>";
				}	
				
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			echo "</tr>";
		}
		break;
		case 2: {
			//Eliminação
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_warning.png\" name='navOption'></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;' colspan='13'>Esta habilitacao foi assinalado como dispensável. Se pretender confirmar esta ação, clique no visto. Caso contrário, clique na cruz.</td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
			echo "<tr>";
		}
		break;
		case 3: {
			//Observação
			echo "<tr>";
				echo "<td colspan='5' style='background-color:#99CCFF;'><u>Observações:</u> ";
				echo $dadosNew->descricao;
			echo "</td></tr>";
		}
		break;
		case 4: {
			echo $dadosNew->descricao . "<br>";
			$cena = explode("VALUES (",$dadosNew->descricao);								
			$cena1 = explode(",",$cena[1]);					
			$tudo = str_replace("'","",$cena1);
								
			echo "<tr style='border-top: solid yellow'>";					
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".str_replace(");","",$tudo[4])."</td>";	
				echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>";getTipoOrientacao($tudo[0]);	echo"</td>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>";getTipoOrientador($tudo[1]); echo"</td>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>"; getOrientacaoEstado($tudo[2]);echo "</td>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[3]. "</td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			echo "</tr>";
		}
		break;
	}		
}

function getTipoOrientacao($i) {
	global $dadosDep;
	$db = new Database();
	$lValues =$db->getLookupValues("lista_tipoOrientacao");
	while ($row = mysql_fetch_assoc($lValues)) {	
		if($i==$row["ID"])
			echo $row["DESCRICAO"];
	}
	$db->disconnect();				
}	

function getTipoOrientador($i) {	
	$db = new Database();
	$lValues =$db->getLookupValues("lista_tipoOrientador");	
	while ($row = mysql_fetch_assoc($lValues)) {	
		if($i==$row["ID"])
			echo $row["DESCRICAO"];
	}
	$db->disconnect();				
}	

function getOrientacaoEstado($i) {	
	$db = new Database();
	$lValues =$db->getLookupValues("lista_estadosOrientacao");
	while ($row = mysql_fetch_assoc($lValues)) {
		if($i==$row["ID"])
			echo $row["DESCRICAO"];
	}
	$db->disconnect();	
}
?>