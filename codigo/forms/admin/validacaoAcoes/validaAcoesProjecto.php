<?php

	function showAcoesProjecto($dadosNew){
		echo "<fieldset class='normal' style='margin-left:130px'>
		   <div id='projectos'>
			<table id='prj' class='box-table-b' >
				<caption><u><h2>Projetos / Ensaios Clínicos 2013</h2></u></caption>
				<thead>
					<tr>
						<th>IDINV</th>
						<th>Tipo/Entidade</th>
						<th>Entidade Financiadora</th>
						<th>Instituição de Acolhimento</th>
						<th>Montante total solicitado</th>
						<th>Montante total aprovado</th>
						<th>Montante  atribuído à FMUP</th>
						<th>É Investigador Responsável?</th>
						<th>Código/Referência</th>
						<th>Título</th>
						<th>Data Início</th>
						<th>Data Fim</th>
						<th>Link</th>
						<th>Estado</th>	
						<th><b>Validar</b></th>
						<th><b>Eliminar</b></th>	
						<th><b>Informações</b></th>
						<th><b>Edição</b></th>
					</tr>
				</thead>
				<tbody>";
	
	$acoes = array();
	$dadosNew2 = $dadosNew;
	
	while ( list($key, $val) = each($dadosNew) ) {	
		$acoes[$key]=array();
		
		while (list($key2, $val2) = each($dadosNew2)) {

			if($val->idreg == $val2->idreg) {							
				$acoes[$key][]=$dadosNew2[$key2];	  
				unset($dadosNew2[$key2]);				  
				unset($dadosNew[$key2]);
				continue;				
			}

		}
		unset($dadosNew2[$key]);
		reset($dadosNew2);
	}	
	
	foreach ($acoes as $i => $value) {
		if($acoes[$i][0]->idreg != '') {
			$db = new Database();		
			$projecto = $db->getProjectoFromDB($acoes[$i][0]->idreg);
			echo "<tr>";
				echo "<td id='td_projectos_idinv_". $projecto->id ."'>".$projecto->idinv."</td>";
				echo "<td id='td_projectos_tipoentidade_". $projecto->id ."'>"; getTipoEntidade($projecto->entidade); echo "</td>";
				echo "<td id='td_projectos_entidade_". $projecto->id ."'>".$projecto->entidadefinanciadora."</td>";
				echo "<td id='td_projectos_acolhimento_". $projecto->id ."'>".$projecto->acolhimento."</td>";
				echo "<td id='td_projectos_montante_". $projecto->id ."'>".$projecto->montante." €</td>";
				echo "<td id='td_projectos_montantea_". $projecto->id ."'>".$projecto->montantea." €</td>";
				echo "<td id='td_projectos_montantefmup_". $projecto->id ."'>".$projecto->montantefmup." €</td>";
				echo "<td id='td_projectos_invres_". $projecto->id ."'>".checkInv($projecto->invresponsavel)."</td>";
				echo "<td id='td_projectos_codigo_". $projecto->id ."'>".$projecto->codigo."</td>";
				echo "<td id='td_projectos_titulo_". $projecto->id ."'>".$projecto->titulo."</td>";
				echo "<td id='td_projectos_dataini_". $projecto->id ."'>".$projecto->datainicio."</td>";
				echo "<td id='td_projectos_datafim_". $projecto->id ."'>".$projecto->datafim."</td>";
				echo "<td id='td_projectos_link_". $projecto->id ."'>".$projecto->link."</td>";
				echo "<td id='td_projectos_estado_". $projecto->id ."'>".getEstadoProjeto($projecto->estado)."</td>";
				echo "<td></td>";
				echo "<td></td>";
				echo "<td></td>";
				echo "<td style='overflow:hidden;'><img src=\"../../images/icon_edit.png\" onclick=\"$('#dep').text('" . $acoes[$i][0]->departamento. "');$('#chave-projectos').text('" .  $projecto->id. "');\"></td>";    
			echo "</tr>";
		}
		
		foreach ($acoes[$i] as $j => $value2) {
			if($acoes[$i][$j]->acao == 4) {	
				imprimeAcaoProjecto($acoes[$i][$j], '');				
			} else {
				$db = new Database();		
				$projecto = $db->getProjectoFromDB($acoes[$i][$j]->idreg);
				imprimeAcaoProjecto($acoes[$i][$j], $projecto);
			}		
		}		
	}	
	
			echo "</tbody>
			</table>
			<p id='chave-acao' hidden></p>
			<p id='chave-projectos' hidden></p>
			<div>
		</fieldset>";
	}

	function getTipoEntidade($i) {
		$db = new Database();
		$lValues =$db->getLookupValues("lista_tipoentidadefinanciadora");
		while ($row = mysql_fetch_assoc($lValues)) {	
			if($i==$row["ID"])
				echo $row["DESCRICAO"];
		}
		$db->disconnect();
	}	
	
	function checkInv($i){
		global $dadosNewDep;
		if($i==1)
			return "Sim";
		else 
			return "Não";
	}
	
	function getEstadoProjeto($i) {
		global $dadosNewDep;
		$db = new Database();
		$lValues =$db->getLookupValues("lista_estadoProjetos");
		$texto="";
		while ($row = mysql_fetch_assoc($lValues)) {
			if($i==$row["ID"])
				$texto=$row["DESCRICAO"];
		}
		$db->disconnect();
		return $texto;
	}	
	
	function imprimeAcaoProjecto($dadosNew, $projecto) {
		switch($dadosNew->acao) {
			case 1: {				
				//Edição		
				$cena = explode("ENTIDADE=",$dadosNew->descricao);
				
				$cena1 = explode(", MONTANTE=",$cena[1]);
				$entidade = $cena1[0];
				
				$cena2 = explode(", MONTANTEA=",$cena1[1]);
				$montante = $cena2[0];
				
				$cena3 = explode(", MONTANTEFMUP=",$cena2[1]);
				$montantea= $cena3[0];				
				
				$cena4 = explode(", INVRESPONSAVEL=",$cena3[1]);
				$montantefmup= $cena4[0];
				
				$cena5 = explode(", CODIGO='",$cena4[1]);
				$invres= $cena5[0];
				
				$cena6 = explode("', TITULO='",$cena5[1]);
				$codigo= $cena6[0];
						
				$cena7 = explode("', DATAINICIO='",$cena6[1]);
				$titulo= $cena7[0];
				
				$cena8 = explode("', DATAFIM='",$cena7[1]);
				$datainicio = $cena8[0];
				
				$cena9 = explode("', ENTIDADEFINANCIADORA='",$cena8[1]);
				$datafim = $cena9[0];
				
				$cena10 = explode("', ACOLHIMENTO='",$cena9[1]);
				$entidadefinanciadora = $cena10[0];
				
				$cena11 = explode("', LINK='",$cena10[1]);
				$acolhimento = $cena11[0];
				
				$cena12 = explode("', ESTADO=",$cena11[1]);
				$link = $cena12[0];
				
				$cena13 = explode(" where",$cena12[1]);
				$estado = $cena13[0];
				
				echo "<tr style='border-top: solid yellow'>";
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$projecto->idinv."</td>";
					if($projecto->entidade != $entidade) {
						echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>"; echo getTipoEntidade($entidade); echo "</td>";
					} else {
						echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>"; echo getTipoEntidade($projecto->entidade); echo "</td>";
					}
					
					if($projecto->entidadefinanciadora != $entidadefinanciadora) {
						echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$entidadefinanciadora."</td>";
					} else {
						echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$projecto->entidadefinanciadora."</td>";
					}
					
					if(trim($projecto->acolhimento) != $acolhimento) {
						echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$acolhimento."</td>";
					} else {
						echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$projecto->acolhimento."</td>";
					}
					
					if(trim($projecto->montante) != trim($montante)) {
						echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$montante." €</td>";
					} else {
						echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$projecto->montante." €</td>";
					}
					
					if(trim($projecto->montantea) != trim($montantea)) {
						echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$montantea." €</td>";
					} else {
						echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$projecto->montantea." €</td>";
					}
					
					if(trim($projecto->montantefmup) != trim($montantefmup)) {
						echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$montantefmup." €</td>";
					} else {
						echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$projecto->montantefmup." €</td>";
					}
					
					if($projecto->invresponsavel != $invres) {
						echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".checkInv($invres)."</td>";
					} else {
						echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".checkInv($projecto->invresponsavel)."</td>";
					}
					
					if($projecto->codigo != $codigo) {
						echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$codigo."</td>";
					} else {
						echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$projecto->codigo."</td>";
					}
					
					if($projecto->titulo != $titulo) {
						echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$titulo."</td>";
					} else {
						echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$projecto->titulo."</td>";
					}
					
					if($projecto->datainicio != $datainicio) {
						echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$datainicio."</td>";
					} else {
						echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$projecto->datainicio."</td>";
					}

					if($projecto->datafim != $datafim) {
						echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$datafim."</td>";
					} else {
						echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$projecto->datafim."</td>";
					}
					
					if($projecto->link != $link) {
						echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$link."</td>";
					} else {
						echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$projecto->link."</td>";
					}
					
					if($projecto->estado != $estado) {
						echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$estado."</td>";
					} else {
						echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$projecto->estado."</td>";
					}
					
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
				echo "</tr>";
			}
			break;
			case 2: {
				//Eliminação
				echo "<tr style='border-top: solid yellow'>";
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_warning.png\" name='navOption'></center></td>";
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;' colspan='13'>Este projeto foi assinalado como dispensável. Se pretender confirmar esta ação, clique no visto. Caso contrário, clique na cruz.</td>";
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				echo "<tr>";
			}
			break;
			case 3: {
				//Observação
				echo "<tr>";
					echo "<td colspan='14' style='background-color:#99CCFF;'><u>Observações:</u> ";
					echo $dadosNew->descricao;
				echo "</td></tr>";
			}
			break;
			case 4: {
				$cena = explode("VALUES (",$dadosNew->descricao);								
				$cena1 = explode(",",$cena[1]);					
				$tudo = str_replace("'","",$cena1);
									
				echo "<tr style='border-top: solid yellow'>";					
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".str_replace(");","",$tudo[13])."</td>";
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>"; echo getTipoEntidade($tudo[0]); echo "</td>";					
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$tudo[9]."</td>";					
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$tudo[10]."</td>";					
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$tudo[1]." €</td>";					
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$tudo[2]." €</td>";					
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$tudo[3]." €</td>";					
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".checkInv($tudo[4])."</td>";					
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$tudo[5]."</td>";				
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$tudo[6]."</td>";				
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$tudo[7]."</td>";				
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$tudo[8]."</td>";				
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$tudo[11]."</td>";				
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$tudo[12]."</td>";					
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
				echo "</tr>";
			}
			break;
		}		
	}
	
?>