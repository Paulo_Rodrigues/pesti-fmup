<?php

require_once '../../classlib/Investigador.class.inc';

function showAcoesInvestigadores($dadosNew) {
	echo "<fieldset class='normal'>
		    <div id='investigadores' >   
			  <table id='users' class='box-table-b' >
			  <caption><u><h2>Investigadores Registados</h2></u></caption>
				<thead>
				  <tr>
					<th>ID Inv</th>
					<th>Login</th>
					<th>Nome</th>
					<th>Nº Mecanográfico</th>
					<th>Data Nascimento</th>
					<th>Estado</th>
					<th>Data de Lacre</th>
					<th><b>Validar</b></th>
					<th><b>Eliminar</b></th>	
					<th><b>Informações</b></th>
					<th><b>Edição</b></th>
				  </tr>
			  </thead>
			  <tbody>";	
	  			  
	$acoes = array();
	$dadosNew2 = $dadosNew;
	
	while ( list($key, $val) = each($dadosNew) ) {	
		$acoes[$key]=array();
		
		while (list($key2, $val2) = each($dadosNew2)) {

			if($val->idreg == $val2->idreg) {							
				$acoes[$key][]=$dadosNew2[$key2];	  
				unset($dadosNew2[$key2]);				  
				unset($dadosNew[$key2]);
				continue;				
			}

		}
		unset($dadosNew2[$key]);
		reset($dadosNew2);
	}
	
	foreach ($acoes as $i => $value) {
		if($acoes[$i][0]->idreg != '') {
			$db = new Database();		
			$investigador = $db->getInvestigadorFromDB($acoes[$i][0]->idreg);
			
			echo "<tr id='user" .$investigador->id. "'>";
				echo "<td>".$investigador->id."</td>";
				echo "<td>".$investigador->login."</td>";
				echo "<td id='td" . $investigador->id . "_nome'>".$investigador->nome."</td>";
				echo "<td id='td" . $investigador->id . "_nummec'>".$investigador->nummec."</td>";
				echo "<td id='td" . $investigador->id . "_datanasc'>".$investigador->datanasc."</td>";
				if($investigador->estado=="0")
					echo "<td style='color:red !important;'>Não lacrado</td>";
				else
				echo "<td>Lacrado</td>";
				echo "<td>".$investigador->datasubmissao."</td>";	
				echo "<td></td>";
				echo "<td></td>";
				echo "<td></td>";			
				echo "<td style='overflow:hidden;'><img src=\"../../images/icon_edit.png\" onclick=\"$('#dep').text('" . $acoes[$i][0]->departamento. "');$('#chave-inv').text('" . $investigador->id. "');\"></td>";
			echo "</tr>";  
		}
		
		foreach ($acoes[$i] as $j => $value2) {
			if($acoes[$i][$j]->acao == 4) {	
				imprimeAcaoInvestigador($acoes[$i][$j], '');				
			} else {
				$db = new Database();		
				$investigador = $db->getInvestigadorFromDB($acoes[$i][$j]->idreg);
				imprimeAcaoInvestigador($acoes[$i][$j], $investigador);
			}		
		}		
	}
				
	echo "</tbody></table>
		<p id='chave-inv' hidden></p>
	</div>
	</fieldset>";			
}

function imprimeAcaoInvestigador($dadosNew, $investigador) {
	switch($dadosNew->acao) {
		case "1": {				
			//Edição		
			$cena = explode("NOME='",$dadosNew->descricao);
				
			$cena1 = explode("',NUMMEC='",$cena[1]);
			$nome = $cena1[0];
			
			$cena2 = explode("',DATANASC='",$cena1[1]);
			$nummec = $cena2[0];
			
			$cena3 = explode("' where",$cena2[1]);
			$datanasc = $cena3[0];
			
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$investigador->id."</td>";	
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$investigador->login."</td>";				
				if($investigador->nome != $nome) {
					echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>"; echo $nome; echo "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>"; echo $investigador->nome; echo "</td>";
				}
				
				if($investigador->nummec != $nummec) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$nummec."</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$investigador->nummec."</td>";
				}
				
				if($investigador->datanasc != $datanasc) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$datanasc."</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$investigador->datanasc."</td>";
				}				
				
				if($investigador->estado=="0")
					echo "<td style='color:red !important;'>Não lacrado</td>";
				else
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>Lacrado</td>";
				
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$investigador->datasubmissao."</td>";		
					
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			echo "</tr>";
		}
		break;
		case "2": {
			//Eliminação
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_warning.png\" name='navOption'></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;' colspan='6'>Este investigador foi assinalado como dispensável. Se pretender confirmar esta ação, clique no visto. Caso contrário, clique na cruz.</td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'></td>";
			echo "<tr>";
		}
		break;
		case "3": {
			//Observação
			echo "<tr>";
				echo "<td colspan='7' style='background-color:#99CCFF;'><u>Observações:</u> ";
				echo $dadosNew->descricao;
			echo "</td></tr>";
		}
		break;
		case "4": {
			//Inserir
			$cena = explode("VALUES (",$dadosNew->descricao);
			
			$cena1 = explode(",",$cena[1]);		
			
			$tudo = str_replace("'","",$cena1);
			
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'></td>";	
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'></td>";		
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$tudo[0]."</td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$tudo[1]."</td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$tudo[2]."</td>";
					
				if(str_replace(")","",$tudo[4])=="0")
					echo "<td style='color:red !important;'>Não lacrado</td>";
				else if(str_replace(")","",$tudo[4])=="1")
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>Lacrado</td>";
				else if(str_replace(")","",$tudo[4])=="-1")
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>Novo Investigador</td>";
					
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'></td>";		
						
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'></td>";
			echo "</tr>";
		}
		break;		
	}		
}

?>