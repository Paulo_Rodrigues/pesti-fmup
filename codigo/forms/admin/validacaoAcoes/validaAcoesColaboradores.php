<?php

require_once '../../classlib/Colaboradores.class.inc';
require_once '../../classlib/ColaboradoresInternos.class.inc';

function showAcoesColaboradores($colInt, $colExt) {
	echo "<fieldset class='normal'><div id='colaboradoresInternos'>			
			<table id='colIntAct' class='box-table-b'>
			<caption><u><h2>Colaboradores Atuais na FMUP</h2></u></caption>
				<thead>
					<tr>
						<th>ID Inv</th>
						<th>Nome Científico</th>
						<th>Departmento</th>
						<th><b>Validar</b></th>
						<th><b>Eliminar</b></th>	
						<th><b>Informações</b></th>
						<th><b>Edição</b></th>
					</tr>
				</thead>
				<tbody>";
				
	$acoes = array();
	$colInt2 = $colInt;
	
	while ( list($key, $val) = each($colInt) ) {	
		$acoes[$key]=array();
		
		while (list($key2, $val2) = each($colInt2)) {

			if($val->idreg == $val2->idreg) {							
				$acoes[$key][]=$colInt2[$key2];	  
				unset($colInt2[$key2]);				  
				unset($colInt[$key2]);
				continue;				
			}

		}
		unset($colInt2[$key]);
		reset($colInt2);
	}
	
	foreach ($acoes as $i => $value) {
		if($acoes[$i][0]->idreg != '') {
			$db = new Database();		
			$colaboradorInterno = $db->getColIntFromDB($acoes[$i][0]->idreg);
			echo "<tr>";
				echo "<td>";echo $colaboradorInterno->idinv;echo "</td>";				
				echo "<td id='td_colIntAct_nomecient_" . $colaboradorInterno->id . "'>";echo $colaboradorInterno->nomecient;echo "</td>";
				echo "<td id='td_colIntAct_dep_" . $colaboradorInterno->id . "'>";getDepartamentos($colaboradorInterno->departamento);echo "</td>";
				echo "<td></td>";
				echo "<td></td>";
				echo "<td></td>";	
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#dep').text('" . $acoes[$i][0]->departamento. "');$('#chave-colIntAct').text('" . $colaboradorInterno->id . "');\"></td>";
			echo "</tr>";
		}
		
		foreach ($acoes[$i] as $j => $value2) {
			if($acoes[$i][$j]->acao == 4) {	
				imprimeColInt($acoes[$i][$j], '');				
			} else {
				$db = new Database();		
				$colaboradorInterno = $db->getColIntFromDB($acoes[$i][$j]->idreg);
				imprimeColInt($acoes[$i][$j], $colaboradorInterno);
			}		
		}		
	}	
	
	echo "</tbody>
	</table>
	<p id='chave-colIntAct' hidden></p>
	</div><br>";
	
	echo "<div id='colaboradoresExternos'>";
	echo "<table id='colExtAct' class='box-table-b'>		
			<caption><u><h2>Colaboradores Externos Atuais</h2></u></caption>
			<thead>		
				<tr>
					<th>ID Inv</th>
					<th>Nome Científico com Iniciais</th>
					<th>Instituição</th>
					<th>País</th>
					<th><b>Validar</b></th>
					<th><b>Eliminar</b></th>	
					<th><b>Informações</b></th>
					<th><b>Edição</b></th>
				</tr>
			</thead>
			<tbody>";
			
	$acoes = array();
	$colExt2 = $colExt;
	
	while ( list($key, $val) = each($colExt) ) {	
		$acoes[$key]=array();
		
		while (list($key2, $val2) = each($colExt2)) {

			if($val->idreg == $val2->idreg) {							
				$acoes[$key][]=$colExt2[$key2];	  
				unset($colExt2[$key2]);				  
				unset($colExt[$key2]);
				continue;				
			}

		}
		unset($colExt2[$key]);
		reset($colExt2);
	}
	
	foreach ($acoes as $i => $value) {
		if($acoes[$i][0]->idreg != '') {
			$db = new Database();		
			$colaboradorExterno = $db->getColExtFromDB($acoes[$i][0]->idreg);
			echo "<tr>";
				echo "<td>";echo $colaboradorExterno->idinv;echo "</td>";
				echo "<td id='td_colExtAct_nomecient_" . $colaboradorExterno->id . "'>";echo $colaboradorExterno->nomecient;echo "</td>";							
				echo "<td id='td_colExtAct_instituicao_" . $colaboradorExterno->id . "'>";echo $colaboradorExterno->instituicao;echo "</td>";				
				echo "<td id='td_colExtAct_pais_" . $colaboradorExterno->id . "'>";getPaisesCol($colaboradorExterno->pais);echo "</td>";
				echo "<td></td>";
				echo "<td></td>";
				echo "<td></td>";	
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#dep').text('" . $acoes[$i][0]->departamento. "');$('#chave-colExtAct').text('" . $colaboradorExterno->id . "');\"></td>";
			echo "</tr>";
		}
		
		foreach ($acoes[$i] as $j => $value2) {
			if($acoes[$i][$j]->acao == 4) {	
				imprimeColExt($acoes[$i][$j], '');				
			} else {
				$db = new Database();		
				$colaboradorExterno = $db->getColExtFromDB($acoes[$i][$j]->idreg);
				imprimeColExt($acoes[$i][$j], $colaboradorExterno);
			}		
		}		
	}	
	
	echo "</tbody>
	</table> <p id='chave-colExtAct' hidden></p>
   </div></fieldset>";	
}

function imprimeColInt($dadosNew, $colInt) {
	switch($dadosNew->acao) {
		case 1: {				
			//Edição							
			$cena = explode("NOMECIENT='",$dadosNew->descricao);
						
			$cena1 = explode("', DEPARTAMENTO=",$cena[1]);
			$nomecient = $cena1[0];
				
			$cena2 = explode(" where",$cena1[1]);
			$dep = $cena2[0];
								
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$colInt->idinv."</td>";			
				if($colInt->nomecient != $nomecient) {
					echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$nomecient. "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$colInt->nomecient."</td>";
				}
				
				if($colInt->departamento != $dep) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>";getDepartamentos( $dep); echo  "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>";getDepartamentos($colInt->departamento); echo "</td>";
				}			
									
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			echo "</tr>";
		}
		break;
		case 2: {
			//Eliminação
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_warning.png\" name='navOption'></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;' colspan='13'>Esta habilitacao foi assinalado como dispensável. Se pretender confirmar esta ação, clique no visto. Caso contrário, clique na cruz.</td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
			echo "<tr>";
		}
		break;
		case 3: {
			//Observação
			echo "<tr>";
				echo "<td colspan='3' style='background-color:#99CCFF;' ><u>Observações:</u> ";
				echo $dadosNew->descricao;
			echo "</td></tr>";
		}
		break;
		case 4: {
			$cena = explode("VALUES (",$dadosNew->descricao);								
			$cena1 = explode(",",$cena[1]);					
			$tudo = str_replace("'","",$cena1);
	
			$tudo[1] = 	explode(",",$tudo[1]);	
			
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".str_replace(");","",$tudo[1][1])."</td>";	
				echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$tudo[0]. "</td>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>";getDepartamentos( $tudo[1][0]); echo  "</td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			echo "</tr>";
		}
		break;
	}		
}
	
function imprimeColExt($dadosNew, $colExt) {
	switch($dadosNew->acao) {
		case 1: {				
			//Edição							
			$cena = explode("NOMECIENT='",$dadosNew->descricao);
						
			$cena1 = explode("', INSTITUICAO='",$cena[1]);
			$nomecient = $cena1[0];
			
			$cena2 = explode("', PAIS='",$cena1[1]);
			$ins = $cena2[0];
				
			$cena3 = explode("' where",$cena2[1]);
			$pais = $cena3[0];
								
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$colExt->idinv."</td>";			
				if($colExt->nomecient != $nomecient) {
					echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$nomecient. "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$colExt->nomecient."</td>";
				}
				
				if($colExt->instituicao != $ins) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$ins."</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$colExt->instituicao."</td>";
				}

				if($colExt->pais != $pais) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>";getPaisesCol(pais); echo"</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>";getPaisesCol($colExt->pais); echo"</td>";
				}				
									
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			echo "</tr>";
		}
		break;
		case 2: {
			//Eliminação
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_warning.png\" name='navOption'></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;' colspan='13'>Esta habilitacao foi assinalado como dispensável. Se pretender confirmar esta ação, clique no visto. Caso contrário, clique na cruz.</td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
			echo "<tr>";
		}
		break;
		case 3: {
			//Observação
			echo "<tr>";
				echo "<td colspan='3' style='background-color:#99CCFF;'><u>Observações:</u> ";
				echo $dadosNew->descricao;
			echo "</td></tr>";
		}
		break;
		case 4: {
			$cena = explode("VALUES ('",$dadosNew->descricao);								
			$cena1 = explode("','",$cena[1]);					
			$tudo = str_replace("'","",$cena1);			
			$tudo[2] = 	explode(",",$tudo[2]);	
			
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".str_replace(");","",$tudo[2][1])."</td>";	
				echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$tudo[0]. "</td>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$tudo[1]."</td>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>";getPaisesCol($tudo[2][0]); echo"</td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			echo "</tr>";
		}
		break;
	}		
}

function getPaisesCol($i){
	$db = new Database();
	$lValues =$db->getLookupValues("lista_paises");
	while ($row = mysql_fetch_assoc($lValues)) {	
		if($i==$row["ID"])
			echo $row["DESCRICAO"];
	}
	$db->disconnect();			
}	
	
function checkPaisCol($id,$i){
	global $dadosDep;
	if($dadosDep->colaboradores[$i]->pais==$id)
		return true;
	else 
		return false;
}
function getDepartamentos($i){
	$db = new Database();
	$lValues =$db->getLookupValues("lista_servicosfmup");
	while ($row = mysql_fetch_assoc($lValues)) {
		if($row["ID"]==$i)
			echo $row["DESCRICAO"];
	}
	$db->disconnect();
}

function checkDepartamento($id,$i){
	global $dadosDep;
	if($dadosDep->colaboradoresinternos[$i]->departamento==$id)
		return true;
	else
		return false;
}

?>