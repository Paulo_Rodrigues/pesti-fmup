<?php

require_once '../../classlib/SC.class.inc';

function showAcoesSociedadesCientificas($dadosNew) {
	echo "<fieldset class='normal'><div id='sc'>";
	echo "<table class='box-table-b'>		
			<caption><u><h2>Organizações e Sociedades Científicas</h2></u></caption>	
			<thead>
				<tr>
					<th>IDINV</th>
					<th>Nome</th>
					<th>Tipo</th>
					<th>Data Início</th>
					<th>Data Fim</th>
					<th>Cargo</th>
					<th>Link</th>
					<th><b>Validar</b></th>
					<th><b>Eliminar</b></th>	
					<th><b>Informações</b></th>
					<th><b>Edição</b></th>
				</tr>
			</thead>
			<tbody>";
	
	$acoes = array();
	$dadosNew2 = $dadosNew;
	
	while ( list($key, $val) = each($dadosNew) ) {	
		$acoes[$key]=array();
		
		while (list($key2, $val2) = each($dadosNew2)) {

			if($val->idreg == $val2->idreg) {							
				$acoes[$key][]=$dadosNew2[$key2];	  
				unset($dadosNew2[$key2]);				  
				unset($dadosNew[$key2]);
				continue;				
			}

		}
		unset($dadosNew2[$key]);
		reset($dadosNew2);
	}	
	
	foreach ($acoes as $i => $value) {
		if($acoes[$i][0]->idreg != '') {
			$db = new Database();		
			$sc = $db->getSociedadeCientificaFromDB($acoes[$i][0]->idreg);
			echo "<tr>";
				echo "<td>".$sc->idinv."</td>";
				echo "<td id='td_sc_nome_" .$sc->id . "'>".$sc->nome."</td>";
				echo "<td id='td_sc_tipo_" .$sc->id . "'>";getTipoSC($sc->tipo);echo "</td>";
				echo "<td id='td_sc_datainicio_" .$sc->id . "'>".$sc->datainicio."</td>";	
				echo "<td id='td_sc_datafim_" .$sc->id . "'>".$sc->datafim."</td>";	
				echo "<td id='td_sc_cargo_" .$sc->id . "'>".$sc->cargo."</td>";
				echo "<td id='td_sc_link_" .$sc->id . "'><a href='".$sc->link."' target='_blank'>".$sc->link."</a></td>";
				echo "<td></td>";
				echo "<td></td>";
				echo "<td></td>";	
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-sc').text('" .$sc->id . "');$('#dep').text('" . $acoes[$i][0]->departamento. "');\"></td>";
			echo "</tr>";	
		}
		
		foreach ($acoes[$i] as $j => $value2) {
			if($acoes[$i][$j]->acao == 4) {	
				imprimeSC($acoes[$i][$j], '');				
			} else {
				$db = new Database();		
				$sc = $db->getSociedadeCientificaFromDB($acoes[$i][$j]->idreg);
				imprimeSC($acoes[$i][$j], $sc);
			}		
		}		
	}	
	
		echo "</tbody></table>
		<p id='chave-sc' hidden></p>";
		echo "</div></fieldset>";		
}


function imprimeSC($dadosNew, $sc) {
	switch($dadosNew->acao) {
		case 1: {				
			//Edição
			$cena = explode("TIPO='",$dadosNew->descricao);
					
			$cena1 = explode("', DATAINICIO='",$cena[1]);
			$tipo = $cena1[0];
			
			$cena2 = explode("', DATAFIM='",$cena1[1]);
			$datainicio= $cena2[0];
			
			$cena3 = explode("', CARGO='",$cena2[1]);
			$datafim = $cena3[0];	
			
			$cena4 = explode("', LINK='",$cena3[1]);
			$cargo = $cena4[0];	
			
			$cena5 = explode("', NOME='",$cena4[1]);
			$link = $cena5[0];	
									
			$cena6 = explode("' where",$cena5[1]);
			$nome = $cena6[0];
											
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$sc->idinv."</td>";			
				if($sc->nome != $nome) {
					echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>". $nome."</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>". $sc->nome."</td>";
				}
				
				if($sc->tipo != $tipo) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>";getTipoSC($tipo);echo "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>";getTipoSC($sc->tipo);echo "</td>";
				}
				
				if($sc->datainicio != $datainicio) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $datainicio. "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>". $sc->datainicio. "</td>";
				}
				
				if($sc->datafim != $datafim) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $datafim. "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>". $sc->datafim. "</td>";
				}
				
				if($sc->cargo != $cargo) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $cargo. "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>". $sc->cargo. "</td>";
				}
				
				if($sc->link != $link) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $link. "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><a href='".$sc->link."' target='_blank'>". $sc->link. "</a></td>";
				}				
						
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			echo "</tr>";
		}
		break;
		case 2: {
			//Eliminação
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_warning.png\" name='navOption'></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;' colspan='13'>Esta habilitacao foi assinalado como dispensável. Se pretender confirmar esta ação, clique no visto. Caso contrário, clique na cruz.</td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
			echo "<tr>";
		}
		break;
		case 3: {
			//Observação
			echo "<tr>";
				echo "<td colspan='7' style='background-color:#99CCFF;'><u>Observações:</u> ";
				echo $dadosNew->descricao;
			echo "</td></tr>";
		}
		break;
		case 4: {
			$cena = explode("VALUES (",$dadosNew->descricao);								
			$cena1 = explode(",",$cena[1]);					
			$tudo = str_replace("'","",$cena1);
								
			echo "<tr style='border-top: solid yellow'>";					
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".str_replace(");","",$tudo[6])."</td>";	
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[5]. "</td>";			
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>";getTipoSC($tudo[0]);echo "</td>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[1]. "</td>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[2]. "</td>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[3]. "</td>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[4]. "</td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			echo "</tr>";
		}
		break;
	}		
}

function getTipoSC($i) {	
	$db = new Database();
	$lValues =$db->getLookupValues("lista_tiposociedadecientifica");
	
	while ($row = mysql_fetch_assoc($lValues)) {	
		if($i==$row["ID"])
			echo $row["DESCRICAO"];
	}

	$db->disconnect();				
}	

?>