<?php

require_once '../../classlib/Juri.class.inc';

function showAcoesJuris($dadosNew) {
echo "<fieldset class='normal'>
		  <div id='juris'>";		
				
	echo "<table class='box-table-b'>
			<caption><u><h2>Júris</h2></u></caption>	
			<thead>
				<tr>
					<th></th>
					<th colspan='4'>Agregacao</th>
					<th colspan='4'>Doutoramento</i></th>
					<th colspan='4'>Mestrado Científico</i></th>
					<th colspan='4'>Mestrado Integrado</i></th>
				</tr>
				<tr>
					<td></td>
					<td colspan='2'>FMUP</td>
					<td colspan='2'>Exterior</td>
					<td colspan='2'>FMUP</td>
					<td colspan='2'>Exterior</td>
					<td colspan='2'>FMUP</td>
					<td colspan='2'>Exterior</td>
					<td colspan='2'>FMUP</td>
					<td colspan='2'>Exterior</td>
				</tr>
				<tr>
					<td>IDINV</td>
					<td>Arguente</td>
					<td>Outro</td>
					<td>Arguente</td>
					<td>Outro</td>
					<td>Arguente</td>
					<td>Outro</td>
					<td>Arguente</td>
					<td>Outro</td>
					<td>Arguente</td>
					<td>Outro</td>
					<td>Arguente</td>
					<td>Outro</td>
					<td>Arguente</td>
					<td>Outro</td>
					<td>Arguente</td>
					<td>Outro</td>
					<th><b>Validar</b></th>
					<th><b>Eliminar</b></th>	
					<th><b>Informações</b></th>
					<th><b>Edição</b></th>
				</tr>
			</thead>
			<tbody>";
	
	
	$acoes = array();
	$dadosNew2 = $dadosNew;
	
	while ( list($key, $val) = each($dadosNew) ) {	
		$acoes[$key]=array();
		
		while (list($key2, $val2) = each($dadosNew2)) {

			if($val->idreg == $val2->idreg) {							
				$acoes[$key][]=$dadosNew2[$key2];	  
				unset($dadosNew2[$key2]);				  
				unset($dadosNew[$key2]);
				continue;				
			}

		}
		unset($dadosNew2[$key]);
		reset($dadosNew2);
	}
	
	foreach ($acoes as $i => $value) {
		if($acoes[$i][0]->idreg != '') {
			$db = new Database();		
			$juri = $db->getJuriFromDB($acoes[$i][0]->idreg);
			echo "<tr>";
				echo "<td>".$juri->idinv."</td>";
				echo "<td id='td_juris_agrargfmup_" . $juri->id . "'>".$juri->agregacaoargfmup."</td>";
				echo "<td id='td_juris_agrvogalfmup_" . $juri->id . "'>".$juri->agregacaovogalfmup."</td>";
				echo "<td id='td_juris_agrargext_" . $juri->id . "'>".$juri->agregacaoargext."</td>";
				echo "<td id='td_juris_agrvogalext_" . $juri->id . "'>".$juri->agregacaovogalext."</td>";
				echo "<td id='td_juris_doutargfmup_" . $juri->id . "'>".$juri->doutoramentoargfmup."</td>";
				echo "<td id='td_juris_doutvogalfmup_" . $juri->id . "'>".$juri->doutoramentovogalfmup."</td>";
				echo "<td id='td_juris_doutargext_" . $juri->id . "'>".$juri->doutoramentoargext."</td>";
				echo "<td id='td_juris_doutvogalext_" . $juri->id . "'>".$juri->doutoramentovogalext."</td>";
				echo "<td id='td_juris_mesargfmup_" . $juri->id . "'>".$juri->mestradoargfmup."</td>";
				echo "<td id='td_juris_mesvogalfmup_" . $juri->id . "'>".$juri->mestradovogalfmup."</td>";
				echo "<td id='td_juris_mesargext_" . $juri->id . "'>".$juri->mestradoargext."</td>";
				echo "<td id='td_juris_mesvogalext_" . $juri->id . "'>".$juri->mestradovogalext."</td>";
				echo "<td id='td_juris_mesintfmup_" . $juri->id . "'>".$juri->mestradointargfmup."</td>";
				echo "<td id='td_juris_mesintvogalfmup_" . $juri->id . "'>".$juri->mestradointvogalfmup."</td>";
				echo "<td id='td_juris_mesintargext_" . $juri->id . "'>".$juri->mestradointargext."</td>";
				echo "<td id='td_juris_mesintvogalext_" . $juri->id . "'>".$juri->mestradointvogalext."</td>";
				echo "<td></td>";
				echo "<td></td>";
				echo "<td></td>";	
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#dep').text('" . $acoes[$i][0]->departamento. "');$('#chave-juris').text('" . $juri->id . "');\"></td>";
			
			echo "</tr>";
		}
		
		foreach ($acoes[$i] as $j => $value2) {
			if($acoes[$i][$j]->acao == 4) {	
				imprimeJuri($acoes[$i][$j], '');				
			} else {
				$db = new Database();		
				$juri = $db->getJuriFromDB($acoes[$i][$j]->idreg);
				imprimeJuri($acoes[$i][$j], $juri);
			}		
		}		
	}	
	
	echo "</tbody>
	</table>
	<p id='chave-juris' hidden ></p>
</div></fieldset>";
}

function imprimeJuri($dadosNew, $juri) {
	switch($dadosNew->acao) {
		case 1: {				
			//Edição
			$cena = explode("AGREGACAOARGFMUP=",$dadosNew->descricao);
					
			$cena1 = explode(", AGREGACAOVOGALFMUP=",$cena[1]);
			$agregacaoargfmup = $cena1[0];
			
			$cena2 = explode(", AGREGACAOARGEXT=",$cena1[1]);
			$agregacaovogalfmup = $cena2[0];
			
			$cena3 = explode(", AGREGACAOVOGALEXT=",$cena2[1]);
			$agregacaoargext= $cena3[0];
			
			$cena4 = explode(", DOUTORAMENTOARGFMUP=",$cena3[1]);
			$agregacaovogalext= $cena4[0];
			
			$cena5 = explode(", DOUTORAMENTOVOGALFMUP=",$cena4[1]);
			$doutoramentoargfmup= $cena5[0];
			
			$cena6 = explode(", DOUTORAMENTOARGEXT=",$cena5[1]);
			$doutoramentovogalfmup= $cena6[0];
					
			$cena7 = explode(", DOUTORAMENTOVOGALEXT=",$cena6[1]);
			$doutoramentoargext= $cena7[0];
			
			$cena8 = explode(", MESTRADOARGFMUP=",$cena7[1]);
			$doutoramentovogalext = $cena8[0];
			
			$cena9 = explode(", MESTRADOVOGALFMUP=",$cena8[1]);
			$mestradoargfmup = $cena9[0];
			
			$cena10 = explode(", MESTRADOARGEXT=",$cena9[1]);
			$mestradovogalfmup = $cena10[0];
			
			$cena11 = explode(", MESTRADOVOGALEXT=",$cena10[1]);
			$mestradoargext = $cena11[0];
			
			$cena12 = explode(", MESTRADOINTARGFMUP=",$cena11[1]);
			$mestradovogalext = $cena12[0];
			
			$cena13 = explode(", MESTRADOINTVOGALFMUP=",$cena12[1]);
			$mestradointargfmup = $cena13[0];
			
			$cena14 = explode(", MESTRADOINTARGEXT=",$cena13[1]);
			$mestradointvogalfmup = $cena14[0];
			
			$cena15 = explode(", MESTRADOINTVOGALEXT=",$cena14[1]);
			$mestradointargext = $cena15[0];
			
			$cena16 = explode(" where",$cena15[1]);
			$mestradointvogalext = $cena16[0];
			
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$juri->idinv."</td>";			
				if($juri->agregacaoargfmup != $agregacaoargfmup) {
					echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$agregacaoargfmup."</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$juri->agregacaoargfmup."</td>";
				}
				
				if($juri->agregacaovogalfmup != $agregacaovogalfmup) {
					echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$agregacaovogalfmup."</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$juri->agregacaovogalfmup."</td>";
				}
				
				if($juri->agregacaoargext != $agregacaoargext) {
					echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$agregacaoargext."</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$juri->agregacaoargext."</td>";
				}
				
				if($juri->agregacaovogalext != $agregacaovogalext) {
					echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$agregacaovogalext."</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$juri->agregacaovogalext."</td>";
				}
				
				if($juri->doutoramentoargfmup != $doutoramentoargfmup) {
					echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$doutoramentoargfmup."</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$juri->doutoramentoargfmup."</td>";
				}
				
				if($juri->doutoramentovogalfmup != $doutoramentovogalfmup) {
					echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$doutoramentovogalfmup."</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$juri->doutoramentovogalfmup."</td>";
				}
				
				if($juri->doutoramentoargext != $doutoramentoargext) {
					echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$doutoramentoargext."</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$juri->doutoramentoargext."</td>";
				}
				
				if($juri->doutoramentovogalext != $doutoramentovogalext) {
					echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$doutoramentovogalext."</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$juri->doutoramentovogalext."</td>";
				}
				
				if($juri->mestradoargfmup != $mestradoargfmup) {
					echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$mestradoargfmup."</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$juri->mestradoargfmup."</td>";
				}
				
				if($juri->mestradovogalfmup != $mestradovogalfmup) {
					echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$mestradovogalfmup."</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$juri->mestradovogalfmup."</td>";
				}
				
				if($juri->mestradoargext != $mestradoargext) {
					echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$mestradoargext."</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$juri->mestradoargext."</td>";
				}
				
				if($juri->mestradovogalext != $mestradovogalext) {
					echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$mestradovogalext."</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$juri->mestradovogalext."</td>";
				}
				
				if($juri->mestradointargfmup != $mestradointargfmup) {
					echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$mestradointargfmup."</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$juri->mestradointargfmup."</td>";
				}
				
				if($juri->mestradointvogalfmup != $mestradointvogalfmup) {
					echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$mestradointvogalfmup."</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$juri->mestradointvogalfmup."</td>";
				}
				
				if($juri->mestradointargext != $mestradointargext) {
					echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$mestradointargext."</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$juri->mestradointargext."</td>";
				}
				
				if($juri->mestradointvogalext != $mestradointvogalext) {
					echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$mestradointvogalext."</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$juri->mestradointvogalext."</td>";
				}
				
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			echo "</tr>";
		}
		break;
		case 2: {
			//Eliminação
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_warning.png\" name='navOption'></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;' colspan='13'>Esta habilitacao foi assinalado como dispensável. Se pretender confirmar esta ação, clique no visto. Caso contrário, clique na cruz.</td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
			echo "<tr>";
		}
		break;
		case 3: {
			//Observação
			echo "<tr>";
				echo "<td colspan='17' style='background-color:#99CCFF;' ><u>Observações:</u> ";
				echo $dadosNew->descricao;
			echo "</td></tr>";
		}
		break;
		case 4: {
			$cena = explode("VALUES (",$dadosNew->descricao);								
			$cena1 = explode(",",$cena[1]);					
			$tudo = str_replace("'","",$cena1);
								
			echo "<tr style='border-top: solid yellow'>";					
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".str_replace(");","",$tudo[16])."</td>";	
				echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$tudo[0]."</td>";
				echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$tudo[1]."</td>";
				echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$tudo[2]."</td>";
				echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$tudo[3]."</td>";
				echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$tudo[4]."</td>";
				echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$tudo[5]."</td>";
				echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$tudo[6]."</td>";
				echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$tudo[7]."</td>";
				echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$tudo[8]."</td>";
				echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$tudo[9]."</td>";
				echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$tudo[10]."</td>";
				echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$tudo[11]."</td>";
				echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$tudo[12]."</td>";
				echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$tudo[13]."</td>";
				echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$tudo[14]."</td>";
				echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$tudo[15]."</td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			echo "</tr>";
		}
		break;
	}		
}


?>