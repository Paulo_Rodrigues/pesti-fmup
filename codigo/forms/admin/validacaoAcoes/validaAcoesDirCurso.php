<?php

require_once '../../classlib/DirCurso.class.inc';

function showAcoesDirCurso($dadosNew) {
	echo "<fieldset class='normal'><div id='direcaoCurso'>\n";
	echo "<table id='dircurso' class='box-table-b'>
			<caption><u><h2>Direções de Curso</h2></u></caption>
			<thead>
				<tr>
					<th>ID Inv</th>
					<th>Data de Inicio</th>
					<th>Data de Fim</th>
					<th>Curso</th>
					<th>Grau</th>
					<th><b>Validar</b></th>
					<th><b>Eliminar</b></th>	
					<th><b>Informações</b></th>
					<th><b>Edição</b></th>
				</tr>
			</thead>
			<tbody>";
			
	$acoes = array();
	$dadosNew2 = $dadosNew;
	
	while ( list($key, $val) = each($dadosNew) ) {	
		$acoes[$key]=array();
		
		while (list($key2, $val2) = each($dadosNew2)) {

			if($val->idreg == $val2->idreg) {							
				$acoes[$key][]=$dadosNew2[$key2];	  
				unset($dadosNew2[$key2]);				  
				unset($dadosNew[$key2]);
				continue;				
			}

		}
		unset($dadosNew2[$key]);
		reset($dadosNew2);
	}
			
	foreach ($acoes as $i => $value) {
		if($acoes[$i][0]->idreg != '') {
			$db = new Database();		
			$dirCurso = $db->getDirCursoFromDB($acoes[$i][0]->idreg);
			echo "<tr>";
				echo "<td>".$dirCurso->idinv."</td>";
				echo "<td id='td_dircurso_dataini_" . $dirCurso->id . "'>".$dirCurso->datainicio."</td>";
				echo "<td id='td_dircurso_datafim_" . $dirCurso->id . "'>".$dirCurso->datafim."</td>";
				echo "<td id='td_dircurso_curso_" . $dirCurso->id . "'>".$dirCurso->nome."</td>";
				echo "<td id='td_dircurso_grau_" . $dirCurso->id . "'>";
				getGrauDirCursos($dirCurso->grau);	
				echo "</td>";			
				echo "<td></td>";
				echo "<td></td>";
				echo "<td></td>";	
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#dep').text('" . $acoes[$i][0]->departamento. "');$('#chave-dircurso').text('" . $dirCurso->id . "');\"></td>";
			echo "</tr>";
		}
		
		foreach ($acoes[$i] as $j => $value2) {
			if($acoes[$i][$j]->acao == 4) {	
				imprimeDirCurso($acoes[$i][$j], '');				
			} else {
				$db = new Database();		
				$dirCurso = $db->getDirCursoFromDB($acoes[$i][$j]->idreg);
				imprimeDirCurso($acoes[$i][$j], $dirCurso);
			}		
		}		
	}	
	
	echo "</tbody></table>";
    echo "<p id='chave-dircurso' hidden></p>";
    echo "</div></fieldset>";	
}


function imprimeDirCurso($dadosNew, $dirCurso) {
	switch($dadosNew->acao) {
		case 1: {				
			//Edição		
			$cena = explode("DATAINICIO='",$dadosNew->descricao);
						
			$cena1 = explode("', DATAFIM='",$cena[1]);
			$datainicio = $cena1[0];
			
			$cena2 = explode("', NOME='",$cena1[1]);
			$datafim = $cena2[0];
			
			$cena3 = explode("', GRAU='",$cena2[1]);
			$nome = $cena3[0];
			
			$cena4 = explode("' where",$cena3[1]);
			$grau = $cena4[0];
								
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$dirCurso->idinv."</td>";			
				if($dirCurso->datainicio != $datainicio) {
					echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$datainicio. "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$dirCurso->datainicio. "</td>";
				}
				
				if($dirCurso->datafim != $datafim) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $datafim. "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>". $dirCurso->datafim. "</td>";
				}
				
				if($dirCurso->nome != $nome) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $nome. "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>". $dirCurso->nome. "</td>";
				}
				
				if($dirCurso->grau != $grau) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>"; getGrauDirCursos($grau); echo "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>"; getGrauDirCursos($dirCurso->grau); echo "</td>";
				}
									
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			echo "</tr>";
		}
		break;
		case 2: {
			//Eliminação
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_warning.png\" name='navOption'></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;' colspan='13'>Esta habilitacao foi assinalado como dispensável. Se pretender confirmar esta ação, clique no visto. Caso contrário, clique na cruz.</td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
			echo "<tr>";
		}
		break;
		case 3: {
			//Observação
			echo "<tr>";
				echo "<td colspan='5' style='background-color:#99CCFF;'><u>Observações:</u> ";
				echo $dadosNew->descricao;
			echo "</td></tr>";
		}
		break;
		case 4: {
			$cena = explode("VALUES (",$dadosNew->descricao);								
			$cena1 = explode(",",$cena[1]);					
			$tudo = str_replace("'","",$cena1);
						
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".str_replace(");","",$tudo[4])."</td>";	
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$tudo[0]. "</td>";				
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[1]. "</td>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[2]. "</td>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>"; getGrauDirCursos($tudo[3]); echo "</td>";			
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			echo "</tr>";
		}
		break;
	}		
}

function getGrauDirCursos($i) {
	$db = new Database();
	$lValues =$db->getLookupValues("lista_graucursos");
	while ($row = mysql_fetch_assoc($lValues)) {	
		if($i==$row["ID"])
			echo $row["DESCRICAO"];
	}
	$db->disconnect();			
}	
				
function checkGrauDirCursos($id,$i){
	global $dadosDep;
	if($dirCurso->grau==$id)
		return true;
	else 
		return false;
}
?>