<?php

require_once '../../classlib/Rede.class.inc';

function showAcoesRedes($dadosNew) {
	echo "<fieldset class='normal'><div id='redes'>";
	echo "<table class='box-table-b'>
			<caption><u><h2>Redes Científicas</h2></u></caption>	
			<thead>
				<tr>
					<th>ID Inv</th>
					<th>Nome Rede</th>
					<th>Tipo</th>
					<th>Data Início</th>
					<th>Data Fim</th>
					<th>Link</th>
					<th><b>Validar</b></th>
					<th><b>Eliminar</b></th>	
					<th><b>Informações</b></th>
					<th><b>Edição</b></th>
				</tr>
			</thead>
			<tbody>";
	
	$acoes = array();
	$dadosNew2 = $dadosNew;
	
	while ( list($key, $val) = each($dadosNew) ) {	
		$acoes[$key]=array();
		
		while (list($key2, $val2) = each($dadosNew2)) {

			if($val->idreg == $val2->idreg) {							
				$acoes[$key][]=$dadosNew2[$key2];	  
				unset($dadosNew2[$key2]);				  
				unset($dadosNew[$key2]);
				continue;				
			}

		}
		unset($dadosNew2[$key]);
		reset($dadosNew2);
	}	
	
	foreach ($acoes as $i => $value) {
		if($acoes[$i][0]->idreg != '') {
			$db = new Database();		
			$rede = $db->getRedeFromDB($acoes[$i][0]->idreg);
			echo "<tr>";
				echo "<td>".$rede->idinv."</td>";
				echo "<td id='td_redes_nome_" .$rede->id. "'>".$rede->nome."</td>";
				echo "<td id='td_redes_tipo_" .$rede->id. "'>";getTipoRedes($rede->tipo);	echo "</td>";
				echo "<td id='td_redes_datainicio_" .$rede->id. "'>".$rede->datainicio."</td>";	
				echo "<td id='td_redes_datafim_" .$rede->id. "'>".$rede->datafim."</td>";	
				echo "<td id='td_redes_link_" .$rede->id. "'><a href='".$rede->link."' target='_blank' >".$rede->link."</a></td>";
				echo "<td></td>";
				echo "<td></td>";
				echo "<td></td>";	
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-redes').text('" . $rede->id . "');$('#dep').text('" . $acoes[$i][0]->departamento. "');\"></td>";
			echo "</tr>";	
		}
		
		foreach ($acoes[$i] as $j => $value2) {
			if($acoes[$i][$j]->acao == 4) {	
				imprimeRede($acoes[$i][$j], '');				
			} else {
				$db = new Database();		
				$rede = $db->getRedeFromDB($acoes[$i][$j]->idreg);
				imprimeRede($acoes[$i][$j], $rede);
			}		
		}		
	}	
	
	echo "</tbody>
    </table>
    <p id='chave-redes' hidden></p>
</div></fieldset>";
}

function imprimeRede($dadosNew, $rede) {
	switch($dadosNew->acao) {
		case 1: {				
			//Edição
			$cena = explode("TIPO='",$dadosNew->descricao);
				
			$cena1 = explode("', DATAINICIO='",$cena[1]);
			$tipo = $cena1[0];
			
			$cena2 = explode("', DATAFIM='",$cena1[1]);
			$datainicio= $cena2[0];
			
			$cena3 = explode("', LINK='",$cena2[1]);
			$datafim = $cena3[0];	
			
			$cena4 = explode("', NOME='",$cena3[1]);
			$link = $cena4[0];	
			
			$cena5 = explode("' where ",$cena4[1]);
			$nome = $cena5[0];	
											
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$rede->idinv."</td>";			
				if($rede->nome != $nome) {
					echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>". $nome."</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>". $rede->nome."</td>";
				}
				
				if($rede->tipo != $tipo) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>";getTipoRedes($tipo);	echo "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>";getTipoRedes($rede->tipo); echo "</td>";
				}
				
				if($rede->datainicio != $datainicio) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $datainicio. "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>". $rede->datainicio. "</td>";
				}
				
				if($rede->datafim != $datafim) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $datafim. "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>". $rede->datafim. "</td>";
				}				
				
				if($rede->link != $link) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $link. "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><a href='".$rede->link."' target='_blank'>". $rede->link. "</a></td>";
				}				
						
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			echo "</tr>";
		}
		break;
		case 2: {
			//Eliminação
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_warning.png\" name='navOption'></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;' colspan='13'>Esta habilitacao foi assinalado como dispensável. Se pretender confirmar esta ação, clique no visto. Caso contrário, clique na cruz.</td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
			echo "<tr>";
		}
		break;
		case 3: {
			//Observação
			echo "<tr>";
				echo "<td colspan='6' style='background-color:#99CCFF;'><u>Observações:</u> ";
				echo $dadosNew->descricao;
			echo "</td></tr>";
		}
		break;
		case 4: {
			$cena = explode("VALUES (",$dadosNew->descricao);								
			$cena1 = explode(",",$cena[1]);					
			$tudo = str_replace("'","",$cena1);
								
			echo "<tr style='border-top: solid yellow'>";					
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".str_replace(");","",$tudo[5])."</td>";	
				echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>". $tudo[4]."</td>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>";getTipoRedes($tudo[0]);	echo "</td>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[1]. "</td>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[2]. "</td>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[3]. "</td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			echo "</tr>";
		}
		break;
	}		
}


function getTipoRedes($i) {	
	$db = new Database();
	$lValues =$db->getLookupValues("lista_tiporedes");

	while ($row = mysql_fetch_assoc($lValues)) {
		if($i==$row["ID"])
			echo $row["DESCRICAO"];
	}
	$db->disconnect();				
}	

?>