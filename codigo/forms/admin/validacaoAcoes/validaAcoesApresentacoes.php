<?php

require_once '../../classlib/Apresentacao.class.inc';

function showAcoesApresentacoes($dadosNew) {
	echo "<fieldset class='normal'><div id='apresentacoes'>";
		echo "<table class='box-table-b'>				
				<caption><u><h2>Apresentações</h2></u></caption>
				<thead>
					<tr>
						<th></th>
						<th colspan='3'>Internacional</th>
						<th colspan='3'>Nacional</th>
					</tr>
					<tr>
						<td>IDINV</td>
						<td>Apresentações por convite</td>
						<td>Apresentações como participante</td>
						<td>Seminários Apresentados</td>
						<td>Apresentações por convite</td>
						<td>Apresentações como participante</td>
						<td>Seminários Apresentados</td>							
						<th><b>Validar</b></th>
						<th><b>Eliminar</b></th>	
						<th><b>Informações</b></th>
						<th><b>Edição</b></th>
					</tr>
				</thead>
				<tbody>";
	
	$acoes = array();
	$dadosNew2 = $dadosNew;
	
	while ( list($key, $val) = each($dadosNew) ) {	
		$acoes[$key]=array();
		
		while (list($key2, $val2) = each($dadosNew2)) {

			if($val->idreg == $val2->idreg) {							
				$acoes[$key][]=$dadosNew2[$key2];	  
				unset($dadosNew2[$key2]);				  
				unset($dadosNew[$key2]);
				continue;				
			}

		}
		unset($dadosNew2[$key]);
		reset($dadosNew2);
	}
	
	foreach ($acoes as $i => $value) {
		if($acoes[$i][0]->idreg != '') {
			$db = new Database();		
			$apresentacao = $db->getApresentacaoFromDB($acoes[$i][0]->idreg);
			echo "<tr>";
				echo "<td>".$apresentacao->idinv."</td>";
				echo "<td id='td_apresentacoes_iconfconv_" . $apresentacao->id . "'>".$apresentacao->iconfconv."</td>";
				echo "<td id='td_apresentacoes_iconfpart_" . $apresentacao->id . "'>".$apresentacao->iconfpart."</td>";
				echo "<td id='td_apresentacoes_isem_" . $apresentacao->id . "'>".$apresentacao->isem."</td>";
				echo "<td id='td_apresentacoes_nconfconv_" . $apresentacao->id . "'>".$apresentacao->nconfconv."</td>";
				echo "<td id='td_apresentacoes_nconfpart_" . $apresentacao->id . "'>".$apresentacao->nconfpart."</td>";
				echo "<td id='td_apresentacoes_nsem_" . $apresentacao->id . "'>".$apresentacao->nsem."</td>";				
				echo "<td></td>";
				echo "<td></td>";
				echo "<td></td>";	
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#dep').text('" . $acoes[$i][0]->departamento. "');$('#chave-apresentacoes').text('" . $apresentacao->id . "');\"></td>";
			echo "</tr>";
		}
		
		foreach ($acoes[$i] as $j => $value2) {
			if($acoes[$i][$j]->acao == 4) {	
				imprimeApresentacao($acoes[$i][$j], '');				
			} else {
				$db = new Database();		
				$apresentacao = $db->getApresentacaoFromDB($acoes[$i][$j]->idreg);
				imprimeApresentacao($acoes[$i][$j], $apresentacao);
			}		
		}		
	}	
	
	echo "<tbody>
	</table>
	<p id='chave-apresentacoes' hidden></p>
</div></fieldset>";
}

function imprimeApresentacao($dadosNew, $apresentacao) {
	switch($dadosNew->acao) {
		case 1: {				
			//Edição				
			$cena = explode("ICONFCONV=",$dadosNew->descricao);
				
			$cena1 = explode(", ICONFPART=",$cena[1]);
			$iconfconv = $cena1[0];
			
			$cena2 = explode(", ISEM=",$cena1[1]);
			$iconfpart= $cena2[0];
			
			$cena3 = explode(", NCONFCONV=",$cena2[1]);
			$isem = $cena3[0];	
			
			$cena4 = explode(", NCONFPART=",$cena3[1]);
			$nconfconv=$cena4[0];	
			
			$cena5 = explode(", NSEM=",$cena4[1]);
			$nconfpart= $cena5[0];
						
			$cena6 = explode(" where",$cena5[1]);
			$nsem = $cena6[0];
											
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$apresentacao->idinv."</td>";			
				if($apresentacao->iconfconv != $iconfconv) {
					echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$iconfconv."</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$apresentacao->iconfconv."</td>";
				}
				
				if($apresentacao->iconfpart != $iconfpart) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$iconfpart."</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$apresentacao->iconfpart."</td>";
				}
				
				if($apresentacao->isem != $isem) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $isem. "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>". $apresentacao->isem. "</td>";
				}
				
				if($apresentacao->nconfconv != $nconfconv) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $nconfconv. "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>". $apresentacao->nconfconv. "</td>";
				}
				
				if($apresentacao->nconfpart != $nconfpart) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $nconfpart. "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>". $apresentacao->nconfpart. "</td>";
				}
				
				if($apresentacao->nsem != $nsem) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $nsem. "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>". $apresentacao->nsem. "</td>";
				}				
						
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			echo "</tr>";
		}
		break;
		case 2: {
			//Eliminação
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_warning.png\" name='navOption'></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;' colspan='6'>Esta habilitacao foi assinalado como dispensável. Se pretender confirmar esta ação, clique no visto. Caso contrário, clique na cruz.</td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
			echo "<tr>";
		}
		break;
		case 3: {
			//Observação
			echo "<tr>";
				echo "<td colspan='7' style='background-color:#99CCFF;'><u>Observações:</u> ";
				echo $dadosNew->descricao;
			echo "</td></tr>";
		}
		break;
		case 4: {
			$cena = explode("VALUES (",$dadosNew->descricao);								
			$cena1 = explode(",",$cena[1]);					
			$tudo = str_replace("'","",$cena1);
								
			echo "<tr style='border-top: solid yellow'>";					
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".str_replace(");","",$tudo[6])."</td>";	
				echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$tudo[0]."</td>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$tudo[1]."</td>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[2]. "</td>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[3]. "</td>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[4]. "</td>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[5]. "</td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			echo "</tr>";
		}
		break;
	}		
}
?>