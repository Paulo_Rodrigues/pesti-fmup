<?php

function showAcoesArbitragensRevistas($dadosNew) {

	echo "<fieldset class='normal'><div id='arbitragensRevistas'>			
				<table class='box-table-b'>
					<caption><u><h2>Arbitragens Revistas</h2></u></caption>		
						<thead>
							<tr>
								<th>IDINV</th>
								<th>Tipo</th>
								<th>ISSN</th>
								<th>Revista</th>
								<th>Link</th>						
								<th><b>Validar</b></th>
								<th><b>Eliminar</b></th>	
								<th><b>Informações</b></th>
								<th><b>Edição</b></th>
							</tr>
						</thead>
						<tbody>";
	
	
	$acoes = array();
	$dadosNew2 = $dadosNew;
	
	while ( list($key, $val) = each($dadosNew) ) {	
		$acoes[$key]=array();
		
		while (list($key2, $val2) = each($dadosNew2)) {

			if($val->idreg == $val2->idreg) {							
				$acoes[$key][]=$dadosNew2[$key2];	  
				unset($dadosNew2[$key2]);				  
				unset($dadosNew[$key2]);
				continue;				
			}

		}
		unset($dadosNew2[$key]);
		reset($dadosNew2);
	}
		
	foreach ($acoes as $i => $value) {
		if($acoes[$i][0]->idreg != '') {
			$db = new Database();		
			$arbRevista = $db->getArbRevistaFromDB($acoes[$i][0]->idreg);
			echo "<tr>";
				echo "<td>".$arbRevista->idinv."</td>";
				echo "<td id='td_arbrevistas_tipo_" . $arbRevista->id ."'>";
				getTipoArbitragem($arbRevista->tipo);
				echo "</td>";
				echo "<td id='td_arbrevistas_issn_" . $arbRevista->id ."'>".$arbRevista->issn."</td>";
				echo "<td id='td_arbrevistas_titulo_" . $arbRevista->id ."'>".$arbRevista->tituloRevista."</td>";
				echo "<td id='td_arbrevistas_link_" . $arbRevista->id ."'><a href='".$arbRevista->link."' target='_blank'>".$arbRevista->link."</a></td>";		
				echo "<td></td>";
				echo "<td></td>";
				echo "<td></td>";	
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#dep').text('" . $acoes[$i][0]->departamento. "');$('#chave-arb-revistas').text('" . $arbRevista->id . "');\"></td>";
			echo "</tr>";	
		}
		
		foreach ($acoes[$i] as $j => $value2) {
			if($acoes[$i][$j]->acao == 4) {	
				imprimeArbRevista($acoes[$i][$j], '');				
			} else {	
				$db = new Database();		
				$arbRevista = $db->getArbRevistaFromDB($acoes[$i][$j]->idreg);
				imprimeArbRevista($acoes[$i][$j], $arbRevista);
			}		
		}		
	}	
	
	echo "</tbody>
		</table>
		<p id='chave-arb-revistas'></p>";

		echo "</div></fieldset>";
}

function imprimeArbRevista($dadosNew, $arbRevista) {
	switch($dadosNew->acao) {
		case 1: {				
			//Edição				
			$cena = explode("TIPO=",$dadosNew->descricao);
					
			$cena1 = explode(", ISSN='",$cena[1]);
			$tipo = $cena1[0];
			
			$cena2 = explode("', TITULOREVISTA='",$cena1[1]);
			$issn= $cena2[0];
			
			$cena3 = explode("', LINK='",$cena2[1]);
			$tituloRevista = $cena3[0];	
									
			$cena4 = explode("' where",$cena3[1]);
			$link = $cena4[0];
											
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$arbRevista->idinv."</td>";			
				if($arbRevista->tipo != $tipo) {
					echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>"; echo getTipoArbitragem($tipo); echo "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>"; echo getTipoArbitragem($arbRevista->tipo); echo "</td>";
				}
				
				if($arbRevista->issn != $issn) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$issn."</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$arbRevista->issn."</td>";
				}
				
				if($arbRevista->tituloRevista != $tituloRevista) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tituloRevista. "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>". $arbRevista->tituloRevista. "</td>";
				}
				
				if($arbRevista->link != $link) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $link. "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><a href='".$arbRevista->link."' target='_blank'>". $arbRevista->link. "</a></td>";
				}				
						
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			echo "</tr>";
		}
		break;
		case 2: {
			//Eliminação
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_warning.png\" name='navOption'></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;' colspan='13'>Esta habilitacao foi assinalado como dispensável. Se pretender confirmar esta ação, clique no visto. Caso contrário, clique na cruz.</td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
			echo "<tr>";
		}
		break;
		case 3: {
			//Observação
			echo "<tr>";
				echo "<td colspan='8'><u>Observações:</u> ";
				echo $dadosNew->descricao;
			echo "</td></tr>";
		}
		break;
		case 4: {
			$cena = explode("VALUES (",$dadosNew->descricao);								
			$cena1 = explode(",",$cena[1]);					
			$tudo = str_replace("'","",$cena1);
								
			echo "<tr style='border-top: solid yellow'>";					
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".str_replace(");","",$tudo[4])."</td>";	
				echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>"; echo getTipoArbitragem($tudo[0]); echo "</td>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$tudo[1]."</td>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[2]. "</td>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[3]. "</td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			echo "</tr>";
		}
		break;
	}		
}

function getTipoArbitragem($i)
{		
	$db = new Database();
	$lValues =$db->getLookupValues("lista_tipoarbitragens");		
	while ($row = mysql_fetch_assoc($lValues)) {	
		if($i==$row["ID"])
			echo $row["DESCRICAO"];
	}
	$db->disconnect();				
}		
?>