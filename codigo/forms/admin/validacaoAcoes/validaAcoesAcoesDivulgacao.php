<?php

function showAcoesAcoesDivulgacao($dadosNew) {
	echo "<fieldset class='normal'>";
			
	echo "<div id='acaodivulgacao'>
		  <table class='box-table-b'>
			<caption><u><h2>Ações de Divulgação</h2></u></caption>	
		  <thead>
			<tr>
				<th>IDINV</th>
				<th>Título</th>
				<th>Data</th>
				<th>Local</th>
				<th>Nº de Participantes</th>
				<th><b>Validar</b></th>
				<th><b>Eliminar</b></th>	
				<th><b>Informações</b></th>
				<th><b>Edição</b></th>
			</tr>
		</thead>
		<tbody>";
	
	$id = 0;
	
	$acoes = array();
	$dadosNew2 = $dadosNew;
	
	while ( list($key, $val) = each($dadosNew) ) {	
		$acoes[$key]=array();
		
		while (list($key2, $val2) = each($dadosNew2)) {

			if($val->idreg == $val2->idreg) {							
				$acoes[$key][]=$dadosNew2[$key2];	  
				unset($dadosNew2[$key2]);				  
				unset($dadosNew[$key2]);
				continue;				
			}

		}
		unset($dadosNew2[$key]);
		reset($dadosNew2);
	}
	
	foreach ($acoes as $i => $value) {
		if($acoes[$i][0]->idreg != '') {
			$db = new Database();		
			$acaoDiv = $db->getAcaoDivFromDB($acoes[$i][0]->idreg);
			
			echo "<tr>";
				echo "<td>". $acaoDiv->idinv. "</td>";
				echo "<td id='td_acaodivulgacao_titulo_" .$acaoDiv->id. "'>". $acaoDiv->titulo ."</td>";
				echo "<td id='td_acaodivulgacao_data_" .$acaoDiv->id. "'>". $acaoDiv->data ."</td>";
				echo "<td id='td_acaodivulgacao_local_" .$acaoDiv->id. "'>". $acaoDiv->local ."</td>";
				echo "<td id='td_acaodivulgacao_npart_" .$acaoDiv->id. "'>". $acaoDiv->npart ."</td>";
				echo "<td></td>";
				echo "<td></td>";
				echo "<td></td>";	
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#dep').text('" . $acoes[$i][0]->departamento. "');$('#chave-acaodivulgacao').text('" . $acaoDiv->id . "');\"></td>";
			echo "</tr>";
			
			echo "</tr>";
			$id = $acaoDiv->id;
		}
		
		foreach ($acoes[$i] as $j => $value2) {
			if($acoes[$i][$j]->acao == 4) {	
				imprimeAcaoDiv($acoes[$i][$j], '');				
			} else {
				$db = new Database();		
				$acaoDiv = $db->getAcaoDivFromDB($acoes[$i][$j]->idreg);
				imprimeAcaoDiv($acoes[$i][$j], $acaoDiv);
			}		
		}		
	}
	
    echo "</tbody>
    </table>
    <p id='chave-acaodivulgacao' hidden></p>
</div></fieldset>";	
}

function imprimeAcaoDiv($dadosNew, $acaoDiv) {
	switch($dadosNew->acao) {
		case 1: {				
			//Edição
			$cena = explode("TITULO='",$dadosNew->descricao);
						
			$cena1 = explode("', DATA='",$cena[1]);
			$titulo = $cena1[0];
			
			$cena2 = explode("', LOCAL='",$cena1[1]);
			$data = $cena2[0];
			
			$cena3 = explode("', NPART=",$cena2[1]);
			$local= $cena3[0];
			
			$cena4 = explode(" where",$cena3[1]);
			$npart= $cena4[0];	
			
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$acaoDiv->idinv."</td>";			
				if($acaoDiv->titulo != $titulo) {
					echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$titulo."</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$acaoDiv->titulo."</td>";
				}
				
				if($acaoDiv->data != $data) {
					echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$data."</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$acaoDiv->data."</td>";
				}
				
				if($acaoDiv->local != $local) {
					echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$local."</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$acaoDiv->local."</td>";
				}
				
				if($acaoDiv->npart != $npart) {
					echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$npart."</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$acaoDiv->npart."</td>";
				}	
				
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			echo "</tr>";
		}
		break;
		case 2: {
			//Eliminação
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_warning.png\" name='navOption'></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;' colspan='13'>Esta habilitacao foi assinalado como dispensável. Se pretender confirmar esta ação, clique no visto. Caso contrário, clique na cruz.</td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
			echo "<tr>";
		}
		break;
		case 3: {
			//Observação
			echo "<tr>";
				echo "<td colspan='5' style='background-color:#99CCFF;'><u>Observações:</u> ";
				echo $dadosNew->descricao;
			echo "</td></tr>";
		}
		break;
		case 4: {
			$cena = explode("VALUES (",$dadosNew->descricao);								
			$cena1 = explode(",",$cena[1]);					
			$tudo = str_replace("'","",$cena1);
								
			echo "<tr style='border-top: solid yellow'>";					
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".str_replace(");","",$tudo[4])."</td>";	
				echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$tudo[0]."</td>";
				echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$tudo[1]."</td>";
				echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$tudo[2]."</td>";
				echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$tudo[3]."</td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			echo "</tr>";
		}
		break;
	}		
}


?>