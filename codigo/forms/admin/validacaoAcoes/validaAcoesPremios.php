<?php

function showAcoesPremios($dadosNew) {
	echo "<fieldset class='normal'><div id='premios'>";
			
	echo "<table class='box-table-b'>
			<caption><u><h2>Prémios</h2></u></caption>	
			<thead>
				<tr>
					<th>IDINV</th>		
					<th>Tipo</th>
					<th>Nome</th>
					<th>Contexto</th>
					<th>Montante</th>
					<th>Link</th>	
					<th><b>Validar</b></th>
					<th><b>Eliminar</b></th>	
					<th><b>Informações</b></th>
					<th><b>Edição</b></th>
				</tr>
			</thead>
			<tbody>";
			
	$acoes = array();
	$dadosNew2 = $dadosNew;
	
	while ( list($key, $val) = each($dadosNew) ) {	
		$acoes[$key]=array();
		
		while (list($key2, $val2) = each($dadosNew2)) {

			if($val->idreg == $val2->idreg) {							
				$acoes[$key][]=$dadosNew2[$key2];	  
				unset($dadosNew2[$key2]);				  
				unset($dadosNew[$key2]);
				continue;				
			}

		}
		unset($dadosNew2[$key]);
		reset($dadosNew2);
	}
	
	foreach ($acoes as $i => $value) {
		if($acoes[$i][0]->idreg != '') {
			$db = new Database();		
			$premio = $db->getPremioFromDB($acoes[$i][0]->idreg);
			echo "<tr>";
				echo "<td>".$premio->idinv."</td>";
				echo "<td id='td_premios_tipo_" . $premio->id . "'>";getTipoPremios($premio->tipo);	echo "</td>";	
				echo "<td id='td_premios_nome_" . $premio->id . "'>".$premio->nome."</td>";
				echo "<td id='td_premios_contexto_" . $premio->id . "'>".$premio->contexto."</td>";
				echo "<td id='td_premios_montante_" . $premio->id . "'>".$premio->montante." €</td>";
				echo "<td id='td_premios_link_" . $premio->id . "'><a href='".$premio->link."' target='_blank'>".$premio->link."</a></td>";	
				echo "<td></td>";
				echo "<td></td>";
				echo "<td></td>";	
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#dep').text('" . $acoes[$i][0]->departamento. "');$('#chave-premios').text('" . $premio->id . "');\"></td>";
			echo "</tr>";	
		}
		
		foreach ($acoes[$i] as $j => $value2) {
			if($acoes[$i][$j]->acao == 4) {	
				imprimePremio($acoes[$i][$j], '');				
			} else {
				$db = new Database();		
				$premio = $db->getPremioFromDB($acoes[$i][$j]->idreg);
				imprimePremio($acoes[$i][$j], $premio);
			}		
		}		
	}	
			
	 echo "</tbody>
    </table>
    <p id='chave-premios' hidden></p>
  </div></fieldset>";
}


function imprimePremio($dadosNew, $premio) {
	switch($dadosNew->acao) {
		case 1: {				
			//Edição							
			$cena = explode("TIPO='",$dadosNew->descricao);
				
			$cena1 = explode("', NOME='",$cena[1]);
			$tipo = $cena1[0];
			
			$cena2 = explode("', CONTEXTO='",$cena1[1]);
			$nome= $cena2[0];
			
			$cena3 = explode("', MONTANTE='",$cena2[1]);
			$contexto = $cena3[0];	
			
			$cena4 = explode("', LINK='",$cena3[1]);
			$montante = $cena4[0];	
									
			$cena5 = explode("' where",$cena4[1]);
			$link = $cena5[0];
											
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$premio->idinv."</td>";			
				if($premio->tipo != $tipo) {
					echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>";getTipoPremios($tipo); echo "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>";getTipoPremios($premio->tipo); echo "</td>";
				}
				
				if($premio->nome != $nome) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$nome."</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$premio->nome."</td>";
				}
				
				if($premio->contexto != $contexto) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $contexto. "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>". $premio->contexto. "</td>";
				}
				
				if(trim($premio->montante) != trim($montante)) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $montante. " €</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>". $premio->montante. " €</td>";
				}
				
				if($premio->link != $link) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $link. "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><a href='".$premio->link."' target='_blank'>". $premio->link. "</a></td>";
				}				
						
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			echo "</tr>";
		}
		break;
		case 2: {
			//Eliminação
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_warning.png\" name='navOption'></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;' colspan='13'>Esta habilitacao foi assinalado como dispensável. Se pretender confirmar esta ação, clique no visto. Caso contrário, clique na cruz.</td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
			echo "<tr>";
		}
		break;
		case 3: {
			//Observação
			echo "<tr>";
				echo "<td colspan='6' style='background-color:#99CCFF;'><u>Observações:</u> ";
				echo $dadosNew->descricao;
			echo "</td></tr>";
		}
		break;
		case 4: {
			$cena = explode("VALUES (",$dadosNew->descricao);								
			$cena1 = explode(",",$cena[1]);					
			$tudo = str_replace("'","",$cena1);
								
			echo "<tr style='border-top: solid yellow'>";					
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".str_replace(");","",$tudo[5])."</td>";	
				echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>";getTipoPremios($tudo[0]); echo "</td>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$tudo[1]."</td>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[2]. "</td>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[3]. " €</td>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[4]. "</td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			echo "</tr>";
		}
		break;
	}		
}

function getTipoPremios($i) {	
	$db = new Database();
	$lValues =$db->getLookupValues("lista_tipopremio");	
	while ($row = mysql_fetch_assoc($lValues)) {	
		if($i==$row["ID"])
			echo $row["DESCRICAO"];
	}
	echo "</SELECT><br />\n";
	$db->disconnect();				
}	
?>