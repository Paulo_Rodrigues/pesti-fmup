<?php

require_once '../../classlib/ExperienciaProfissional.class.inc';
require_once '../../classlib/OcupacaoProfissional.class.inc';

function showAcoesExperienciaProfissional($caracDoc, $caracPosDoc) {

	echo "<fieldset class='normal'>
			<div id='caracterizacaoDocentes'>";
		echo "<table id='caracDoc' class='box-table-b'>
			  <caption><u><h2>Caracterização Docentes</h2></u></caption>
				<thead>
					<tr>
						<th>IDINV</th>
						<th>Tipo</th>
						<th>Percentagem</th>
						<th><b>Validar</b></th>
						<th><b>Eliminar</b></th>	
						<th><b>Informações</b></th>
						<th><b>Edição</b></th>
					</tr>
				</thead>
				<tbody>";
				
	$acoes = array();
	$caracDoc2 = $caracDoc;
	
	while ( list($key, $val) = each($caracDoc) ) {	
		$acoes[$key]=array();
		
		while (list($key2, $val2) = each($caracDoc2)) {

			if($val->idreg == $val2->idreg) {							
				$acoes[$key][]=$caracDoc2[$key2];	  
				unset($caracDoc2[$key2]);				  
				unset($caracDoc[$key2]);
				continue;				
			}

		}
		unset($caracDoc2[$key]);
		reset($caracDoc2);
	}
	
	foreach ($acoes as $i => $value) {
		if($acoes[$i][0]->idreg != '') {
			$db = new Database();		
			$expProf = $db->getOcuProfissionalFromDB($acoes[$i][0]->idreg);
			if($expProf->docentecat!=0 || $expProf->docenteper!=0) {	
				echo "<tr>";
					echo "<td>".$expProf->idinv."</td>";
					echo "<td id='td_exp_pro_doc_cat_" . $expProf->id . "'>".getCat("docente",$expProf->docentecat)."</td>";
					echo "<td id='td_exp_pro_doc_per_" . $expProf->id . "'>".$expProf->docenteper."</td>";
					echo "<td></td>";
					echo "<td></td>";
					echo "<td></td>";				
					echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#dep').text('" . $acoes[$i][0]->departamento. "');$('#chave-exp-pro-doc').text('" . $expProf->id. "');\"></td>";	
				echo "</tr>";
			}
		}
		
		foreach ($acoes[$i] as $j => $value2) {
			if($acoes[$i][$j]->acao == 4) {	
				imprimeExpProDoc($acoes[$i][$j], '');
			} else {
				$db = new Database();		
				$expProf = $db->getOcuProfissionalFromDB($acoes[$i][$j]->idreg);
					
				if($expProf->docentecat!=0 || $expProf->docenteper!=0) {
					imprimeExpProDoc($acoes[$i][$j], $expProf);
				}
			}		
		}		
	}	
	
	 echo "</tbody>
    </table>
	<p id='chave-exp-pro-doc' hidden ></p>
</div><br>";

	echo "<div id='caracterizacaoPos'>";
	echo "<table id='caracPos' class='box-table-b'>			
		  <caption><u><h2>Caracterização Pós-Doc</h2></u></caption>
			<thead>
				<tr>
					<th>IDINV</th>
					<th>Tipo Bolsa</th>
					<th>Percentagem</th>
					<th><b>Validar</b></th>
					<th><b>Eliminar</b></th>	
					<th><b>Informações</b></th>
					<th><b>Edição</b></th>
				</tr>
			</thead>
			<tbody>";
	
	unset($acoes);
	$acoes = array();
	$caracPosDoc2 = $caracPosDoc;
	
	while ( list($key, $val) = each($caracPosDoc) ) {	
		$acoes[$key]=array();
		
		while (list($key2, $val2) = each($caracPosDoc2)) {

			if($val->idreg == $val2->idreg) {							
				$acoes[$key][]=$caracPosDoc2[$key2];	  
				unset($caracPosDoc2[$key2]);				  
				unset($caracPosDoc[$key2]);
				continue;				
			}

		}
		unset($caracPosDoc2[$key]);
		reset($caracPosDoc2);
	}
	
	foreach ($acoes as $i => $value) {
		if($acoes[$i][0]->idreg != '') {
			$db = new Database();		
			$expProf = $db->getOcuProfissionalFromDB($acoes[$i][0]->idreg);
			if($expProf->investigadorcat==1) {
				echo "<tr>";
					echo "<td>".$expProf->idinv."</td>";
					echo "<td id='td_exp_pro_pdoc_bol_" . $expProf->id . "'>".getBolsaInvestigador($expProf->investigadorbolsa)."</td>";
					echo "<td id='td_exp_pro_pdoc_per_" . $expProf->id . "'>".$expProf->investigadorper."</td>";
					echo "<td></td>";
					echo "<td></td>";
					echo "<td></td>";				
					echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#dep').text('" . $acoes[$i][0]->departamento. "');$('#chave-exp-pro-pdoc').text('" . $expProf->id. "');\"></td>";
				echo "</tr>";
			}
		}
		
		foreach ($acoes[$i] as $j => $value2) {		
			if($acoes[$i][$j]->acao == 4) {		
				imprimeExpProPosDoc($acoes[$i][$j], '');					
			} else {
				$db = new Database();		
				$expProf = $db->getOcuProfissionalFromDB($acoes[$i][$j]->idreg);
				if($expProf->investigadorcat==1) {
					imprimeExpProPosDoc($acoes[$i][$j], $expProf);
				}
			}		
		}		
	}
	
	echo "</tbody>
	</table>";
	echo "<p id='chave-exp-pro-pdoc' hidden></p>";
	echo "</div></fieldset>";
}

function imprimeExpProDoc($dadosNew, $expProf) {
	switch($dadosNew->acao) {
		case 1: {				
			//Edição		
			$acao = array();
			$cena = explode("DOCENTECAT=",$dadosNew->descricao);
					
			$cena1 = explode(", DOCENTEPER=",$cena[1]);
			$docentecat = $cena1[0];
			
			$cena2 = explode(" where",$cena1[1]);
			$docenteper = $cena2[0];
								
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$expProf->idinv."</td>";			
				if($expProf->docentecat != $docentecat) {
					echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".getCat("docente",$docentecat). "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".getCat("docente",$expProf->docentecat). "</td>";
				}
				
				if($expProf->docenteper != $docenteper) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $docenteper. "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>". $expProf->docenteper. "</td>";
				}
									
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			echo "</tr>";
		}
		break;
		case 2: {
			//Eliminação
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_warning.png\" name='navOption'></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;' colspan='13'>Esta habilitacao foi assinalado como dispensável. Se pretender confirmar esta ação, clique no visto. Caso contrário, clique na cruz.</td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
			echo "<tr>";
		}
		break;
		case 3: {
			//Observação
			echo "<tr>";
				echo "<td colspan='3' style='background-color:#99CCFF;'><u>Observações:</u> ";
				echo $dadosNew->descricao;
			echo "</td></tr>";
		}
		break;
		case 4: {
			$cena = explode("VALUES (",$dadosNew->descricao);								
			$cena1 = explode(",",$cena[1]);					
			$tudo = str_replace("'","",$cena1);
						
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".str_replace(");","",$tudo[2])."</td>";	
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".getCat("docente",$tudo[0]). "</td>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[1]. "</td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			echo "</tr>";
		}
		break;	
	}		
}

function imprimeExpProPosDoc($dadosNew, $expProf) {
	switch($dadosNew->acao) {
		case 1: {				
			//Edição		
			$cena = explode("INVESTIGADORBOLSA=",$dadosNew->descricao);
					
			$cena1 = explode(", INVESTIGADORPER=",$cena[1]);
			$investigadorbolsa = $cena1[0];
			
			$cena2 = explode(" where",$cena1[1]);
			$investigadorper = $cena2[0];
								
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$expProf->idinv."</td>";			
				if($expProf->investigadorbolsa != $investigadorbolsa) {
					echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".getBolsaInvestigador($investigadorbolsa). "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".getBolsaInvestigador($expProf->investigadorbolsa). "</td>";
				}
				
				if($expProf->investigadorper != $investigadorper) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $investigadorper. "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>". $expProf->investigadorper. "</td>";
				}
									
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			echo "</tr>";
		}
		break;
		case 2: {
			//Eliminação
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_warning.png\" name='navOption'></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;' colspan='13'>Esta habilitacao foi assinalado como dispensável. Se pretender confirmar esta ação, clique no visto. Caso contrário, clique na cruz.</td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
			echo "<tr>";
		}
		break;
		case 3: {
			//Observação
			echo "<tr>";
				echo "<td colspan='3' style='background-color:#99CCFF;'><u>Observações:</u> ";
				echo $dadosNew->descricao;
			echo "</td></tr>";
		}
		break;		
		case 4: {
			$cena = explode("VALUES (",$dadosNew->descricao);								
			$cena1 = explode(",",$cena[1]);					
			$tudo = str_replace("'","",$cena1);
						
			echo "<tr style='border-top: solid yellow'>";			
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".str_replace(");","",$tudo[2])."</td>";	
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".getBolsaInvestigador($tudo[0]). "</td>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[1]. "</td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			echo "</tr>";
		}
		break;	
	}		
}

	function getCat($tipo,$valorcat) {
		$resultado="";
		$db = new Database();
		$lValues =$db->getLookupValues("lista_tipo".$tipo);
	
	
		while ($row = mysql_fetch_assoc($lValues)) {
			if(checkSelectedOP($row["ID"],$valorcat))
				$resultado=$row["DESCRICAO"];
		}
		$db->disconnect();
		return $resultado;
	}
	
	function getCatInvestigador($tipo,$valorcat) {
		$resultado="";
		$db = new Database();
		$lValues =$db->getLookupValues("lista_tipo".$tipo);
		while ($row = mysql_fetch_assoc($lValues)) {
			if(checkSelectedOP($row["ID"],$valorcat))
				$resultado=$row["DESCRICAO"];
		}
	
		$db->disconnect();
		return $resultado;
	}
	
	function getBolsaInvestigador($bolsa) {
		$resultado="";
		$db = new Database();
		$lValues =$db->getLookupValues("lista_TipoBolsas");
	
		while ($row = mysql_fetch_assoc($lValues)) {
			if(checkSelectedOP($row["ID"],$bolsa))
				$resultado=$row["DESCRICAO"];
		}

		$db->disconnect();
		return $resultado;
	}
	
	function checkSelectedOP($i,$valor){	
		if($i==$valor){
			return true;
		}else{
			return false;
		}	
	}

?>