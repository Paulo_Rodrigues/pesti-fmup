<?php

require_once '../../classlib/Acao.class.inc';
require_once '../../classlib/Database.class.inc';

require_once 'validaAcoesInvestigadores.php';
require_once 'validaAcoesHabilitacoes.php';
require_once 'validaAcoesAgregacoes.php';
require_once 'validaAcoesExperienciaProfissional.php';
require_once 'validaAcoesDirCurso.php';
require_once 'validaAcoesColaboradores.php';
require_once 'validaAcoesUnidadesInvestigacao.php';
require_once 'validaAcoesPublicacoesManualPatentes.php';
require_once 'validaAcoesPublicacoesManualInternacional.php';
require_once 'validaAcoesPublicacoesManualNacional.php';
require_once 'validaAcoesProjecto.php';
require_once 'validaAcoesConferencias.php';
require_once 'validaAcoesApresentacoes.php';
require_once 'validaAcoesArbitragensRevistas.php';
require_once 'validaAcoesPremios.php';
require_once 'validaAcoesSociedadesCientificas.php';
require_once 'validaAcoesRedes.php';
require_once 'validaAcoesMissoes.php';
require_once 'validaAcoesOrientacoes.php';
require_once 'validaAcoesJuris.php';
require_once 'validaAcoesOutrasActividades.php';
require_once 'validaAcoesAcoesDivulgacao.php';
require_once 'validaAcoesProdutos.php';

include("../camposEdicao.php");

if(isset($_SESSION['departamento']))
{
	if($_SESSION['departamento'] == 50)
	{
		//Informações de todos os departamentos	
		
		$dados = array();
		$dados['investigadores'] = getALLDEPAccoes(1);
		$dados['habilitacoes'] = getALLDEPAccoes(2);
		$dados['agregacoes'] = getALLDEPAccoes(3); 		
		$dados['caracDocentes']= getALLDEPAccoes(4);		
		$dados['caracPosDoc'] = getALLDEPAccoes(27);		
		$dados['dirCursos']= getALLDEPAccoes(5);			
		$dados['colInt']= getALLDEPAccoes(6);		
		$dados['colExt']= getALLDEPAccoes(7);	
		$dados['uniInv']= getALLDEPAccoes(8);	
		$dados['pubPatentes']= getALLDEPAccoes(11);	
		$dados['pubManInternacional']= getALLDEPAccoes(12);	
		$dados['pubManNacional']= getALLDEPAccoes(13);			
		$dados['projectos'] = getALLDEPAccoes(14);		
		$dados['conferencias'] = getALLDEPAccoes(15);
		$dados['apresentacoes'] = getALLDEPAccoes(16);
		$dados['arbRevistas'] = getALLDEPAccoes(17);
		$dados['premios'] = getALLDEPAccoes(18);
		$dados['sociedadesCientificas'] = getALLDEPAccoes(19);
		$dados['redes'] = getALLDEPAccoes(20);
		$dados['missoes'] = getALLDEPAccoes(21);
		$dados['orientacoes'] = getALLDEPAccoes(22);
		$dados['juris'] = getALLDEPAccoes(23);
		$dados['outrasActividades'] = getALLDEPAccoes(24);
		$dados['acoesDivulgacao'] = getALLDEPAccoes(25);
		$dados['produtos'] = getALLDEPAccoes(26);
		if(count($dados) == 0)
			echo "<fieldset class='normal'>\n<p>Não existem ações a validar.<p></fieldset>";
		else
		{
			echo "<p id='login' hidden>" . $_SESSION['login'] . "</p>
			<p id='dep' hidden>" . $_SESSION['departamento'] . "</p>";
			
			showAcoesInvestigadores($dados['investigadores']);				
			showAcoesHabilitacoes($dados['habilitacoes']);
			showAcoesAgregacoes($dados['agregacoes']);
			showAcoesExperienciaProfissional($dados['caracDocentes'], $dados['caracPosDoc']);
			showAcoesDirCurso($dados['dirCursos']);
			showAcoesColaboradores($dados['colInt'],$dados['colExt']);	
			showAcoesUnidadesInvestigacao($dados['uniInv']);		
			showAcoesPublicacoesManualPatentes($dados['pubPatentes']);
			showAcoesPublicacoesManualInternacional($dados['pubManInternacional']);			
			showAcoesPublicacoesManualNacional($dados['pubManNacional']);
			showAcoesProjecto($dados['projectos']);	
			showAcoesConferencias($dados['conferencias']);
			showAcoesApresentacoes($dados['apresentacoes']);
			showAcoesArbitragensRevistas($dados['arbRevistas']);
			showAcoesPremios($dados['premios']);
			showAcoesSociedadesCientificas($dados['sociedadesCientificas']);
			showAcoesRedes($dados['redes']);
			showAcoesMissoes($dados['missoes']);
			showAcoesOrientacoes($dados['orientacoes']);
			showAcoesJuris($dados['juris']);
			showAcoesOutrasActividades($dados['outrasActividades']);
			showAcoesAcoesDivulgacao($dados['acoesDivulgacao']);
			showAcoesProdutos($dados['produtos']);
		}		
	}
	else
	{
		//Informação de um departamento específico
		
		$dados = array();
		$dados['investigadores'] = getDEPAccoes($_SESSION['departamento'],1);
		$dados['habilitacoes'] = getDEPAccoes($_SESSION['departamento'],2);
		$dados['agregacoes'] = getDEPAccoes($_SESSION['departamento'],3);		
		$dados['caracDocentes']= getDEPAccoes($_SESSION['departamento'],4);		
		$dados['caracPosDoc'] = getDEPAccoes($_SESSION['departamento'],27);		
		$dados['dirCursos']= getDEPAccoes($_SESSION['departamento'],5);				
		$dados['colInt']= getDEPAccoes($_SESSION['departamento'],6);		
		$dados['colExt']= getDEPAccoes($_SESSION['departamento'],7);	
		$dados['uniInv']= getDEPAccoes($_SESSION['departamento'],8);
		$dados['pubPatentes']=  getDEPAccoes($_SESSION['departamento'],11);	
		$dados['pubManInternacional']= getDEPAccoes($_SESSION['departamento'],12);	
		$dados['pubManNacional']= getDEPAccoes($_SESSION['departamento'],13);					
		$dados['projectos'] = getDEPAccoes($_SESSION['departamento'],14);
		$dados['conferencias'] = getDEPAccoes($_SESSION['departamento'],15);
		$dados['apresentacoes'] = getDEPAccoes($_SESSION['departamento'],16);
		$dados['arbRevistas'] = getDEPAccoes($_SESSION['departamento'],17);
		$dados['premios']= getDEPAccoes($_SESSION['departamento'],18);
		$dados['sociedadesCientificas'] = getDEPAccoes($_SESSION['departamento'],19);		
		$dados['redes'] = getDEPAccoes($_SESSION['departamento'],20);
		$dados['missoes'] = getDEPAccoes($_SESSION['departamento'],21);
		$dados['orientacoes'] = getDEPAccoes($_SESSION['departamento'],22);
		$dados['juris'] = getDEPAccoes($_SESSION['departamento'],23);
		$dados['outrasActividades'] = getDEPAccoes($_SESSION['departamento'],24);
		$dados['acoesDivulgacao'] = getDEPAccoes($_SESSION['departamento'],25);
		$dados['produtos'] = getDEPAccoes($_SESSION['departamento'],26);
		if(count($dados) == 0)
			echo "<fieldset class='normal'>\n<p>Não existem ações registadas relacionadas com o departamento seleccionado.<p></fieldset>";
		else
		{	
			echo "<p id='login' hidden>" . $_SESSION['login'] . "</p>
			<p id='dep' hidden>" . $_SESSION['departamento'] . "</p>";
			showAcoesInvestigadores($dados['investigadores']);
			showAcoesHabilitacoes($dados['habilitacoes']);
			showAcoesAgregacoes($dados['agregacoes']);
			showAcoesExperienciaProfissional($dados['caracDocentes'], $dados['caracPosDoc']);
			showAcoesDirCurso($dados['dirCursos']);			
			showAcoesColaboradores($dados['colInt'],$dados['colExt']);		
			showAcoesUnidadesInvestigacao($dados['uniInv']);
			showAcoesPublicacoesManualPatentes($dados['pubPatentes']);
			showAcoesPublicacoesManualInternacional($dados['pubManInternacional']);
			showAcoesPublicacoesManualNacional($dados['pubManNacional']);
			showAcoesProjecto($dados['projectos']);
			showAcoesConferencias($dados['conferencias']);
			showAcoesApresentacoes($dados['apresentacoes']);
			showAcoesArbitragensRevistas($dados['arbRevistas']);
			showAcoesPremios($dados['premios']);
			showAcoesSociedadesCientificas($dados['sociedadesCientificas']);
			showAcoesRedes($dados['redes']);
			showAcoesMissoes($dados['missoes']);
			showAcoesOrientacoes($dados['orientacoes']);
			showAcoesJuris($dados['juris']);
			showAcoesOutrasActividades($dados['outrasActividades']);
			showAcoesAcoesDivulgacao($dados['acoesDivulgacao']);
			showAcoesProdutos($dados['produtos']);
		}	
	}	
} 
	
	function getDEPAccoes($iddep,$tabela) {
		$accoes = array();		
		$db = new Database();
		$lValues = $db->getDEPAccoesFromDB($iddep,$tabela);
		$i = 0;
		while ($row = mysql_fetch_assoc($lValues)) {
			$accoes[$i++] = new Acao(
					$row['id'], $row['idreg'], $row['tabela'], $row['acao'], $row['autor'], $row['data'], $row['campos'], $row['estado'], $row['descricao'], $row['departamento']
			);
		}
		$db->disconnect();				
		return $accoes;
	}
	
	function getALLDEPAccoes($tabela) {
		$accoes = array();
		$db = new Database();
		$lValues = $db->getALLDEPAccoesFromDB($tabela);
		$i = 0;
		while ($row = mysql_fetch_assoc($lValues)) {
			$accoes[$i] = new Acao(
					$row['id'], $row['idreg'], $row['tabela'], $row['acao'], $row['autor'], $row['data'], $row['campos'], $row['estado'], $row['descricao'], $row['departamento']
			);			
			$i++;
		}
		$db->disconnect();							
		return $accoes;
	}	
	
?>