<?php

require_once '../../classlib/UnidadesInvestigacao.class.inc';

function showAcoesUnidadesInvestigacao($dadosNew) {
		
	echo "<fieldset class='normal'><div id='unidadesInvestigacao'>";		
	echo "<table id='uniInv' class='box-table-b'>
			<caption><u><h2>Unidades de Investigação</h2></u></caption>
			<thead>
				<tr>
					<th>Id Inv</th>
					<th>Unidade de Investigação</th>
					<th>Avaliação 2007</th>
					<th>Percentagem</th>
					<th>Responsável pela Unidade</th>
					<th><b>Validar</b></th>
					<th><b>Eliminar</b></th>	
					<th><b>Informações</b></th>
					<th><b>Edição</b></th>
				</tr>
			</thead>
		<tbody>";
		
	$acoes = array();
	$dadosNew2 = $dadosNew;
	
	while ( list($key, $val) = each($dadosNew) ) {	
		$acoes[$key]=array();
		
		while (list($key2, $val2) = each($dadosNew2)) {

			if($val->idreg == $val2->idreg) {							
				$acoes[$key][]=$dadosNew2[$key2];	  
				unset($dadosNew2[$key2]);				  
				unset($dadosNew[$key2]);
				continue;				
			}

		}
		unset($dadosNew2[$key]);
		reset($dadosNew2);
	}	
	
	foreach ($acoes as $i => $value) {
		if($acoes[$i][0]->idreg != '') {	
			$db = new Database();		
			$unidadeInvestigacao = $db->getUniInvFromDB($acoes[$i][0]->idreg);
							
			echo "<tr>";
				echo "<td>";echo $unidadeInvestigacao->idinv;echo "</td>";				
				echo "<td id='td_uni_inv_unidade_" . $unidadeInvestigacao->id . "'>"; echo $unidadeInvestigacao->unidade; echo "</td>";				
				echo "<td id='td_uni_inv_ava2007_" . $unidadeInvestigacao->id . "'>"; echo $unidadeInvestigacao->ava2007; echo "</td>";				
				echo "<td id='td_uni_inv_perc_" . $unidadeInvestigacao->id . "'>"; echo $unidadeInvestigacao->percentagem; echo "</td>";				
				echo "<td id='td_uni_inv_resp_" . $unidadeInvestigacao->id . "'>";
				if ($unidadeInvestigacao->responsavel==1) {
					echo "Sim";
				}else {
					echo "Não";
				}
				echo "</td>";
				echo "<td></td>";
				echo "<td></td>";
				echo "<td></td>";			
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-uniInv').text('" . $unidadeInvestigacao->id . "');$('#dep').text('" . $acoes[$i][0]->departamento. "');\"></td>";
			echo "</tr>";
		}
		
		foreach ($acoes[$i] as $j => $value2) {
			if($acoes[$i][$j]->acao == 4) {	
				imprimeUniInv($acoes[$i][$j], '');				
			} else {
				$db = new Database();		
				$unidadeInvestigacao = $db->getUniInvFromDB($acoes[$i][$j]->idreg);
				imprimeUniInv($acoes[$i][$j], $unidadeInvestigacao);
			}		
		}		
	}	
		
	
	echo "</tbody></table>
    <p id='chave-uniInv' hidden></p>";
	echo "</div></fieldset>";
}

function imprimeUniInv($dadosNew, $uniInv) {
	switch($dadosNew->acao) {
		case 1: {				
			//Edição							
			$cena = explode("UNIDADE='",$dadosNew->descricao);
					
			$cena1 = explode("', AVA2007='",$cena[1]);
			$unidade = $cena1[0];

			$cena2 = explode("', PERCENTAGEM=",$cena1[1]);
			$ava2007 = $cena2[0];
			
			$cena3 = explode(", RESPONSAVEL=",$cena2[1]);
			$percentagem = $cena3[0];

			$cena4 = explode(" where",$cena3[1]);
			$responsavel = $cena4[0];
								
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$uniInv->idinv."</td>";			
				if($uniInv->unidade != $unidade) {
					echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$unidade. "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$uniInv->unidade."</td>";
				}
				
				if($uniInv->ava2007 != $ava2007) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $ava2007. "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$uniInv->ava2007. "</td>";
				}		

				if($uniInv->percentagem != $percentagem) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $percentagem. "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$uniInv->percentagem. "</td>";
				}	
				
				if($uniInv->responsavel != $responsavel) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>";
					if ($responsavel==1) {
						echo "Sim</td>";
					} else {
						echo "Não</td>";
					}
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>";
					if ($uniInv->responsavel==1) {
						echo "Sim</td>";
					} else {
						echo "Não</td>";
					}
				}
									
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			echo "</tr>";
		}
		break;
		case 2: {
			//Eliminação
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_warning.png\" name='navOption'></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;' colspan='13'>Esta habilitacao foi assinalado como dispensável. Se pretender confirmar esta ação, clique no visto. Caso contrário, clique na cruz.</td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
			echo "<tr>";
		}
		break;
		case 3: {
			//Observação
			echo "<tr>";
				echo "<td colspan='5' style='background-color:#99CCFF;'><u>Observações:</u> ";
				echo $dadosNew->descricao;
			echo "</td></tr>";
		}
		break;
		case 4: {
			$cena = explode("VALUES (",$dadosNew->descricao);								
			$cena1 = explode(",",$cena[1]);					
			$tudo = str_replace("'","",$cena1);
								
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".str_replace(");","",$tudo[5])."</td>";	
				echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$tudo[1]. "</td>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[2]. "</td>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[3]. "</td>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>";
				if ($tudo[4]==1) {
					echo "Sim</td>";
				} else {
					echo "Não</td>";
				}	
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			echo "</tr>";
		}
		break;
	}		
}

?>