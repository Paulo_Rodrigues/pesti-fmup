<?php

require_once '../../classlib/Conferencia.class.inc';

function showAcoesConferencias($dadosNew) {
	echo "<fieldset class='normal'><div id='conferencias'> 
			<table class='box-table-b'>
			<caption><u><h2>Conferências</h2></u></caption>
			<thead>
				<tr>
					<th>IDINV</th>
					<th>Âmbito</th>
					<th>Data Início</th>
					<th>Data Fim</th>
					<th>Título</th>
					<th>Local (cidade,país)</th>
					<th>Nº Participantes</th>
					<th>Link</th>
					<th><b>Validar</b></th>
					<th><b>Eliminar</b></th>	
					<th><b>Informações</b></th>
					<th><b>Edição</b></th>
				</tr>
			</thead>
			<tbody>";
			
	$acoes = array();
	$dadosNew2 = $dadosNew;
	
	while ( list($key, $val) = each($dadosNew) ) {	
		$acoes[$key]=array();
		
		while (list($key2, $val2) = each($dadosNew2)) {

			if($val->idreg == $val2->idreg) {							
				$acoes[$key][]=$dadosNew2[$key2];	  
				unset($dadosNew2[$key2]);				  
				unset($dadosNew[$key2]);
				continue;				
			}

		}
		unset($dadosNew2[$key]);
		reset($dadosNew2);
	}
	
	foreach ($acoes as $i => $value) {
		if($acoes[$i][0]->idreg != '') {
			$db = new Database();		
			$conferencia = $db->getConferenciaFromDB($acoes[$i][0]->idreg);
			echo "<tr>";
				echo "<td>".$conferencia->idinv."</td>";
				echo "<td id='td_conferencias_ambito_" . $conferencia->id . "'>";
				getAmbitoConf($conferencia->ambito);
				echo "</td>";					
				echo "<td id='td_conferencias_dataini_" . $conferencia->id . "'>".$conferencia->datainicio."</td>";	
				echo "<td id='td_conferencias_datafim_" . $conferencia->id . "'>".$conferencia->datafim."</td>";	
				echo "<td id='td_conferencias_titulo_" . $conferencia->id . "'>".$conferencia->titulo."</td>";
				echo "<td id='td_conferencias_local_" . $conferencia->id . "'>".$conferencia->local."</td>";
				echo "<td id='td_conferencias_npart_" . $conferencia->id . "'>".$conferencia->participantes."</td>";
				echo "<td id='td_conferencias_link_" . $conferencia->id . "'><a href='".$conferencia->link."' target='_blank'>".$conferencia->link."</a></td>";					
				echo "<td></td>";
				echo "<td></td>";
				echo "<td></td>";	
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#dep').text('" . $acoes[$i][0]->departamento. "');$('#chave-conf').text('" . $conferencia->id. "');\"></td>";
			echo "</tr>";	
		}
		
		foreach ($acoes[$i] as $j => $value2) {
			if($acoes[$i][$j]->acao == 4) {	
				imprimeConferencia($acoes[$i][$j], '');				
			} else {
				$db = new Database();		
				$conferencia = $db->getConferenciaFromDB($acoes[$i][$j]->idreg);
				imprimeConferencia($acoes[$i][$j], $conferencia);
			}		
		}		
	}		
	
	echo "</tbody>
    </table>
    <p id='chave-conf' hidden></p>
    </div></fieldset>";		
		
}

function imprimeConferencia($dadosNew, $conferencia) {
	switch($dadosNew->acao) {
		case 1: {				
			//Edição	
			$cena = explode("AMBITO=",$dadosNew->descricao);
			
			$cena1 = explode(", DATAINICIO='",$cena[1]);
			$ambito = $cena1[0];
			
			$cena3 = explode("', DATAFIM='",$cena1[1]);
			$datainicio= $cena3[0];
			
			$cena4 = explode("', TITULO='",$cena3[1]);
			$datafim= $cena4[0];
			
			$cena5 = explode("', LOCAL='",$cena4[1]);
			$titulo= $cena5[0];
			
			$cena6 = explode("', PARTICIPANTES=",$cena5[1]);
			$local= $cena6[0];
					
			$cena7 = explode(", LINK='",$cena6[1]);
			$participantes= $cena7[0];
				
			$cena8 = explode("' where",$cena7[1]);
			$link = $cena8[0];
											
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$conferencia->idinv."</td>";			
				if($conferencia->ambito != $ambito) {
					echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>"; echo getAmbitoConf($ambito); echo "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>"; echo getAmbitoConf($conferencia->ambito); echo "</td>";
				}
				
				if($conferencia->datainicio != $datainicio) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $datainicio. "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>". $conferencia->datainicio. "</td>";
				}
				
				if($conferencia->datafim != $datafim) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $datafim. "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>". $conferencia->datafim. "</td>";
				}
				
				if($conferencia->titulo != $titulo) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $titulo. "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>". $conferencia->titulo. "</td>";
				}
				
				if($conferencia->local != $local) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $local. "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>". $conferencia->local. "</td>";
				}
				
				if($conferencia->participantes != $participantes) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $participantes. "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>". $conferencia->participantes. "</td>";
				}
				
				if($conferencia->link != $link) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'><a href='". $link. "' target='_blank'>". $link. "</a></td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><a href='". $conferencia->link. "' target='_blank'>". $conferencia->link. "</a></td>";
				}
									
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			echo "</tr>";
		}
		break;
		case 2: {
			//Eliminação
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_warning.png\" name='navOption'></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;' colspan='13'>Esta habilitacao foi assinalado como dispensável. Se pretender confirmar esta ação, clique no visto. Caso contrário, clique na cruz.</td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
			echo "<tr>";
		}
		break;
		case 3: {
			//Observação
			echo "<tr>";
				echo "<td colspan='8'><u>Observações:</u> ";
				echo $dadosNew->descricao;
			echo "</td></tr>";
		}
		break;
		case 4: {
			$cena = explode("VALUES (",$dadosNew->descricao);								
			$cena1 = explode(",",$cena[1]);					
			$tudo = str_replace("'","",$cena1);
								
			echo "<tr style='border-top: solid yellow'>";					
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".str_replace(");","",$tudo[7])."</td>";		
				echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>"; echo getAmbitoConf($tudo[0]); echo "</td>";	
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[1]. "</td>";				
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[2]. "</td>";				
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[3]. "</td>";				
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[4]. "</td>";				
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[5]. "</td>";				
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'><a href='". $tudo[6]. "' target='_blank'>". $tudo[6]. "</a></td>";				
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			echo "</tr>";
		}
		break;
	}		
}

function getTipoConf($i) {	
	$db = new Database();
	$lValues =$db->getLookupValues("lista_tipoconf");
	while ($row = mysql_fetch_assoc($lValues)) {	
		if($i==$row["ID"])
			echo $row["DESCRICAO"];
	}
	$db->disconnect();				
}	

function getAmbitoConf($i) {
	$db = new Database();
	$lValues =$db->getLookupValues("lista_ambitoconf");	
	while ($row = mysql_fetch_assoc($lValues)) {	
		if($i==$row["ID"])
			echo $row["DESCRICAO"];
	}
	$db->disconnect();				
}	
?>