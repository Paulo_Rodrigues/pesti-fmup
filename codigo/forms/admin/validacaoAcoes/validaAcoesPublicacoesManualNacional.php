<?php

function showAcoesPublicacoesManualNacional($dadosNew) {
	
	$outJNMAN="";
	$outJANMAN="";
	$outPRPNMAN="";
	$outCPNMAN="";
	$outOANMAN="";
	$outLNMAN="";
	$outCLNMAN="";

	$tcols="<thead>
				<tr>
					<th>IDINV</th>
					<th>TIPO</th>
					<th>TÍTULO DA OBRA</th>
					<th>ISSN</th>
					<th>ISSN_LINKING</th>
					<th>ISSN_ELECTRONIC</th>
					<th>ISBN</th>
					<th>TITULO</th>
					<th>AUTORES</th>
					<th>VOLUME</th>
					<th>ISSUE</th>
					<th>AR</th>
					<th>COLECAO</th>
					<th>PRIMPAGINA</th>
					<th>ULTPAGINA</th>
					<th>IFACTOR</th>
					<th>ANO</th>
					<th>CITACOES</th>
					<th>NPATENTE</th>
					<th>DATAPATENTE</th>
					<th>IPC</th>
					<th>AREA</th>
					<th>LINK</th>
					<th>ESTADO</th>
					<th><b>Validar</b></th>
					<th><b>Eliminar</b></th>	
					<th><b>Informações</b></th>
					<th><b>Edição</b></th>
				</tr>
			</thead>";

	$tcolsJ="<thead>
				<tr>
					<th>IDINV</th>
					<th>ISSN</th>
					<th>REVISTA</th>
					<th>TÍTULO</th>
					<th>AUTORES</th>
					<th>VOLUME</th>
					<th>ISSUE</th>
					<th>PRIMEIRA PÁG.</th>
					<th>ÚLTIMA PÁG.</th>
					<th>LINK</th>
					<th>ESTADO</th>
					<th><b>Validar</b></th>
					<th><b>Eliminar</b></th>	
					<th><b>Informações</b></th>
					<th><b>Edição</b></th>
				</tr>
			</thead>";

	$tcolsCONF="<thead>
					<tr>
						<th>IDINV</th>
						<th>ISBN</th>
						<th>TÍTULO DA OBRA</th>
						<th>EDITORES</th>
						<th>TÍTULO ARTIGO</th>
						<th>AUTORES</th>
						<th>PRIMEIRA PÁG.</th>
						<th>ÚLTIMA PÁG.</th>
						<th>LINK</th>
						<th>ESTADO</th>
						<th><b>Validar</b></th>
						<th><b>Eliminar</b></th>	
						<th><b>Informações</b></th>
						<th><b>Edição</b></th>
					</tr>
				</thead>";

	$tcolsPRP="<thead>
					<tr>
						<th>IDINV</th>
						<th>ISSN</th>
						<th>TÍTULO DA OBRA</th>
						<th>EDITORES</th>
						<th>TÍTULO ARTIGO</th>
						<th>AUTORES</th>
						<th>PRIMEIRA PÁ</th>
						<th>LINK</th>
						<th>ESTADO</th>
						<th><b>Validar</b></th>
						<th><b>Eliminar</b></th>	
						<th><b>Informações</b></th>
						<th><b>Edição</b></th>
					</tr>
				</thead>";

	$tcolsCLIV="<thead>
					<tr>
						<th>IDINV</th>             
						<th>ISBN</th>
						<th>TÍTULO DA OBRA</th>
						<th>EDITORA</th>
						<th>EDITORES</th>
						<th>TÍTULO ARTIGO</th>
						<th>AUTORES</th>
						<th>PRIMEIRA PÁG.</th>
						<th>ÚLTIMA PÁG.</th>
						<th>LINK</th>
						<th>ESTADO</th>
						<th><b>Validar</b></th>
						<th><b>Eliminar</b></th>	
						<th><b>Informações</b></th>
						<th><b>Edição</b></th>
					</tr>
				</thead>";

	$tcolsPAT="<thead>
					<tr>
						<th>IDINV</th>
						<th>Nº PATENTE</th>
						<th>IPC</th>
						<th>TÍTULO</th>
						<th>AUTORES</th>
						<th>DATA de PUBLICAÇÃO</th>
						<th>LINK</th>
						<th>ESTADO</th>
						<th><b>Validar</b></th>
						<th><b>Eliminar</b></th>	
						<th><b>Informações</b></th>
						<th><b>Edição</b></th>
					</tr>
			</thead>";

	$tcolsLIV="<thead>
					<tr>
						<th>IDINV</th>
						<th>ISBN</th>
						<th>TÍTULO</th>
						<th>AUTORES</th>
						<th>EDITOR</th>
						<th>EDITORA</th>
						<th>LINK</th>
						<th>ESTADO</th>
						<th><b>Validar</b></th>
						<th><b>Eliminar</b></th>	
						<th><b>Informações</b></th>
						<th><b>Edição</b></th>
					</tr>
				</thead>";
				
	$acoes = array();
	$dadosNew2 = $dadosNew;
	
	while ( list($key, $val) = each($dadosNew) ) {	
		$acoes[$key]=array();
		
		while (list($key2, $val2) = each($dadosNew2)) {

			if($val->idreg == $val2->idreg) {							
				$acoes[$key][]=$dadosNew2[$key2];	  
				unset($dadosNew2[$key2]);				  
				unset($dadosNew[$key2]);
				continue;				
			}

		}
		unset($dadosNew2[$key]);
		reset($dadosNew2);
	}	
	
	foreach ($acoes as $i => $value) {
		if($acoes[$i][0]->idreg != '') {
			$db = new Database();
			$publicacao = $db->getPublicacaoManualNacionalFromDB($acoes[$i][0]->idreg);
			$tipo=$publicacao->tipofmup;
			switch($tipo){
				case 'CPN': {				
					$outCPNMAN=$outCPNMAN."<tr>";
						$outCPNMAN=$outCPNMAN."<td>".$publicacao->idinv."</td>";
						$outCPNMAN=$outCPNMAN."<td id='td_outCPNMAN_isbn_" . $publicacao->id . "'>".$publicacao->isbn."</td>";
						$outCPNMAN=$outCPNMAN."<td id='td_outCPNMAN_nomepub_" . $publicacao->id . "'>".$publicacao->nomepublicacao."</td>";
						$outCPNMAN=$outCPNMAN."<td id='td_outCPNMAN_editor_" . $publicacao->id . "'>".$publicacao->editor."</td>";
						$outCPNMAN=$outCPNMAN."<td id='td_outCPNMAN_titulo_" . $publicacao->id . "'>".$publicacao->titulo."</td>";
						$outCPNMAN=$outCPNMAN."<td id='td_outCPNMAN_autores_" . $publicacao->id . "'>".$publicacao->autores."</td>";
						$outCPNMAN=$outCPNMAN."<td id='td_outCPNMAN_prim_" . $publicacao->id . "'>".$publicacao->primpagina."</td>";
						$outCPNMAN=$outCPNMAN."<td id='td_outCPNMAN_ult_" . $publicacao->id . "'>".$publicacao->ultpagina."</td>";
						$outCPNMAN=$outCPNMAN."<td><a href ='".$publicacao->link."' target='_blank'>".$publicacao->link."</a></td>";
						$outCPNMAN=$outCPNMAN."<td id='td_outCPNMAN_estado_" . $publicacao->id . "'>".getEstadoPublicacao($publicacao->estado)."</td>";
						$outCPNMAN=$outCPNMAN. "<td></td>";
						$outCPNMAN=$outCPNMAN. "<td></td>";
						$outCPNMAN=$outCPNMAN. "<td></td>";							   	
						$outCPNMAN=$outCPNMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#tabela').text('" . $acoes[$i][0]->tabela. "');$('#dep').text('" . $acoes[$i][0]->departamento. "');$('#chave-pubNacMan-Con').text('" . $publicacao->id . "');$('#tabela').text('13');\"></td>";
					$outCPNMAN=$outCPNMAN."</tr>";		
				}
				break;
				case 'JN': {			
					$outJNMAN=$outJNMAN."<tr>";
						$outJNMAN=$outJNMAN."<td>".$publicacao->idinv."</td>";
						$outJNMAN=$outJNMAN."<td id='td_outJNMAN_issn_" . $publicacao->id . "'>".$publicacao->issn."</td>";
						$outJNMAN=$outJNMAN."<td id='td_outJNMAN_nomepub_" . $publicacao->id . "'>".$publicacao->nomepublicacao."</td>";
						$outJNMAN=$outJNMAN."<td id='td_outJNMAN_titulo_" . $publicacao->id . "'>".$publicacao->titulo."</td>";
						$outJNMAN=$outJNMAN."<td id='td_outJNMAN_autores_" . $publicacao->id . "'>".$publicacao->autores."</td>";
						$outJNMAN=$outJNMAN."<td id='td_outJNMAN_volume_" . $publicacao->id . "'>".$publicacao->volume."</td>";
						$outJNMAN=$outJNMAN."<td id='td_outJNMAN_issue_" . $publicacao->id . "'>".$publicacao->issue."</td>";
						$outJNMAN=$outJNMAN."<td id='td_outJNMAN_prim_" . $publicacao->id . "'>".$publicacao->primpagina."</td>";
						$outJNMAN=$outJNMAN."<td id='td_outJNMAN_ult_" . $publicacao->id . "'>".$publicacao->ultpagina."</td>";
						$outJNMAN=$outJNMAN."<td id='td_outJNMAN_link_" . $publicacao->id . "'><a href='".$publicacao->link."' target='_blank'>".$publicacao->link."</td>";
						$outJNMAN=$outJNMAN."<td id='td_outJNMAN_estado_" . $publicacao->id . "'>".getEstadoPublicacao($publicacao->estado)."</td>";
						$outJNMAN=$outJNMAN. "<td></td>";
						$outJNMAN=$outJNMAN. "<td></td>";
						$outJNMAN=$outJNMAN. "<td></td>";							   	
						$outJNMAN=$outJNMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#tabela').text('" . $acoes[$i][0]->tabela. "');$('#dep').text('" . $acoes[$i][0]->departamento. "');$('#chave-pubNacMan-Art').text('" . $publicacao->id . "');$('#tabela').text('13');\"></td>";
					$outJNMAN=$outJNMAN."</tr>";	
				}
				break;			
				case 'PRPN': {				
					$outPRPNMAN=$outPRPNMAN."<tr>";
						$outPRPNMAN=$outPRPNMAN."<td>".$publicacao->idinv."</td>";
						$outPRPNMAN=$outPRPNMAN."<td id='td_outPRPNMAN_issn_" . $publicacao->id . "'>".$publicacao->issn."</td>";
						$outPRPNMAN=$outPRPNMAN."<td id='td_outPRPNMAN_nomepub_" . $publicacao->id . "'>".$publicacao->nomepublicacao."</td>";
						$outPRPNMAN=$outPRPNMAN."<td id='td_outPRPNMAN_titulo_" . $publicacao->id . "'>".$publicacao->titulo."</td>";
						$outPRPNMAN=$outPRPNMAN."<td id='td_outPRPNMAN_autores_" . $publicacao->id . "'>".$publicacao->autores."</td>";
						$outPRPNMAN=$outPRPNMAN."<td id='td_outPRPNMAN_volume_" . $publicacao->id . "'>".$publicacao->volume."</td>";
						$outPRPNMAN=$outPRPNMAN."<td id='td_outPRPNMAN_issue_" . $publicacao->id . "'>".$publicacao->issue."</td>";
						$outPRPNMAN=$outPRPNMAN."<td id='td_outPRPNMAN_prim_" . $publicacao->id . "'>".$publicacao->primpagina."</td>";
						$outPRPNMAN=$outPRPNMAN."<td id='td_outPRPNMAN_ult_" . $publicacao->id . "'>".$publicacao->ultpagina."</td>";
						$outPRPNMAN=$outPRPNMAN."<td id='td_outPRPNMAN_link_" . $publicacao->id . "'><a href='".$publicacao->link."' target='_blank'>".$publicacao->link."</a></td>";
						$outPRPNMAN=$outPRPNMAN."<td id='td_outPRPNMAN_estado_" . $publicacao->id . "'>".getEstadoPublicacao($publicacao->estado)."</td>";
						$outPRPNMAN=$outPRPNMAN. "<td></td>";
						$outPRPNMAN=$outPRPNMAN. "<td></td>";
						$outPRPNMAN=$outPRPNMAN. "<td></td>";							   	
						$outPRPNMAN=$outPRPNMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#tabela').text('" . $acoes[$i][0]->tabela. "');$('#dep').text('" . $acoes[$i][0]->departamento. "');$('#chave-pubNacMan-Rev').text('" . $publicacao->id . "');$('#tabela').text('13');\"></td>";
					$outPRPNMAN=$outPRPNMAN."</tr>";	
				}
				break;
				case 'JAN': {			
					$outJANMAN=$outJANMAN."<tr>";
						$outJANMAN=$outJANMAN."<td>".$publicacao->idinv."</td>";
						$outJANMAN=$outJANMAN."<td id='td_outJANMAN_issn_" . $publicacao->id . "'>".$publicacao->issn."</td>";
						$outJANMAN=$outJANMAN."<td id='td_outJANMAN_nomepub_" . $publicacao->id . "'>".$publicacao->nomepublicacao."</td>";
						$outJANMAN=$outJANMAN."<td id='td_outJANMAN_titulo_" . $publicacao->id . "'>".$publicacao->titulo."</td>";
						$outJANMAN=$outJANMAN."<td id='td_outJANMAN_autores_" . $publicacao->id . "'>".$publicacao->autores."</td>";
						$outJANMAN=$outJANMAN."<td id='td_outJANMAN_volume_" . $publicacao->id . "'>".$publicacao->volume."</td>";
						$outJANMAN=$outJANMAN."<td id='td_outJANMAN_issue_" . $publicacao->id . "'>".$publicacao->issue."</td>";
						$outJANMAN=$outJANMAN."<td id='td_outJANMAN_prim_" . $publicacao->id . "'>".$publicacao->primpagina."</td>";
						$outJANMAN=$outJANMAN."<td id='td_outJANMAN_ult_" . $publicacao->id . "'>".$publicacao->ultpagina."</td>";
						$outJANMAN=$outJANMAN."<td id='td_outJANMAN_link_" . $publicacao->id . "'><a href='".$publicacao->link."' target='_blank'>".$publicacao->link."</a></td>";
						$outJANMAN=$outJANMAN."<td id='td_outJANMAN_estado_" . $publicacao->id . "'>".getEstadoPublicacao($publicacao->estado)."</td>";
						$outJANMAN=$outJANMAN. "<td></td>";
						$outJANMAN=$outJANMAN. "<td></td>";
						$outJANMAN=$outJANMAN. "<td></td>";							   	
						$outJANMAN=$outJANMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#tabela').text('" . $acoes[$i][0]->tabela. "');$('#dep').text('" . $acoes[$i][0]->departamento. "');$('#chave-pubNacMan-Sum').text('" . $publicacao->id . "');$('#tabela').text('13');\"></td>";
					$outJANMAN=$outJANMAN."</tr>";
				}
				break;			
				case 'OAN': {				
					$outOANMAN=$outOANMAN."<tr>";
						$outOANMAN=$outOANMAN."<td>".$publicacao->idinv."</td>";
						$outOANMAN=$outOANMAN."<td id='td_outOANMAN_isbn_" . $publicacao->id . "'>".$publicacao->isbn."</td>";
						$outOANMAN=$outOANMAN."<td id='td_outOANMAN_nomepub_" . $publicacao->id . "'>".$publicacao->nomepublicacao."</td>";
						$outOANMAN=$outOANMAN."<td id='td_outOANMAN_editor_" . $publicacao->id . "'>".$publicacao->editor."</td>";
						$outOANMAN=$outOANMAN."<td id='td_outOANMAN_titulo_" . $publicacao->id . "'>".$publicacao->titulo."</td>";
						$outOANMAN=$outOANMAN."<td id='td_outOANMAN_autores_" . $publicacao->id . "'>".$publicacao->autores."</td>";
						$outOANMAN=$outOANMAN."<td id='td_outOANMAN_prim_" . $publicacao->id . "'>".$publicacao->primpagina."</td>";
						$outOANMAN=$outOANMAN."<td id='td_outOANMAN_ult_" . $publicacao->id . "'>".$publicacao->ultpagina."</td>";
						$outOANMAN=$outOANMAN."<td id='td_outOANMAN_link_" . $publicacao->id . "'><a href='".$publicacao->link."' target='_blank'>".$publicacao->link."</a></td>";
						$outOANMAN=$outOANMAN."<td id='td_outOANMAN_estado_" . $publicacao->id . "'>".getEstadoPublicacao($publicacao->estado)."</td>";
						$outOANMAN=$outOANMAN. "<td></td>";
						$outOANMAN=$outOANMAN. "<td></td>";
						$outOANMAN=$outOANMAN. "<td></td>";							   	
						$outOANMAN=$outOANMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#tabela').text('" . $acoes[$i][0]->tabela. "');$('#dep').text('" . $acoes[$i][0]->departamento. "');$('#chave-pubNacMan-Oth').text('" . $publicacao->id . "');$('#tabela').text('13');\"></td>";
					$outOANMAN=$outOANMAN."</tr>";	
				}
				break;
				case 'LN': {				
					$outLNMAN=$outLNMAN."<tr>";
						$outLNMAN=$outLNMAN."<td>".$publicacao->idinv."</td>";
						$outLNMAN=$outLNMAN."<td id='td_outLNMAN_isbn_" . $publicacao->id . "'>".$publicacao->isbn."</td>";
						$outLNMAN=$outLNMAN."<td id='td_outLNMAN_titulo_" . $publicacao->id . "'>".$publicacao->titulo."</td>";
						$outLNMAN=$outLNMAN."<td id='td_outLNMAN_autores_" . $publicacao->id . "'>".$publicacao->autores."</td>";
						$outLNMAN=$outLNMAN."<td id='td_outLNMAN_editor_" . $publicacao->id . "'>".$publicacao->editor."</td>";
						$outLNMAN=$outLNMAN."<td id='td_outLNMAN_editora_" . $publicacao->id . "'>".$publicacao->editora."</td>";
						$outLNMAN=$outLNMAN."<td id='td_outLNMAN_link_" . $publicacao->id . "'><a href='".$publicacao->link."' target='_blank'>".$publicacao->link."</a></td>";
						$outLNMAN=$outLNMAN."<td id='td_outLNMAN_estado_" . $publicacao->id . "'>".getEstadoPublicacao($publicacao->estado)."</td>";
						$outLNMAN=$outLNMAN. "<td></td>";
						$outLNMAN=$outLNMAN. "<td></td>";
						$outLNMAN=$outLNMAN. "<td></td>";							   	
						$outLNMAN=$outLNMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#tabela').text('" . $acoes[$i][0]->tabela. "');$('#dep').text('" . $acoes[$i][0]->departamento. "');$('#chave-pubNacMan-Liv').text('" . $publicacao->id . "');$('#tabela').text('13');\"></td>";
					$outLNMAN=$outLNMAN."</tr>";	
				}
				break;
				case 'CLN': {			
					$outCLNMAN=$outCLNMAN."<tr>";
						$outCLNMAN=$outCLNMAN."<td>".$publicacao->idinv."</td>";
						$outCLNMAN=$outCLNMAN."<td id='td_outCLNMAN_isbn_" . $publicacao->id . "'>".$publicacao->isbn."</td>";
						$outCLNMAN=$outCLNMAN."<td id='td_outCLNMAN_nomepub_" . $publicacao->id . "'>".$publicacao->nomepublicacao."</td>";
						$outCLNMAN=$outCLNMAN."<td id='td_outCLNMAN_editora_" . $publicacao->id . "'>".$publicacao->editora."</td>";
						$outCLNMAN=$outCLNMAN."<td id='td_outCLNMAN_editor_" . $publicacao->id . "'>".$publicacao->editor."</td>";
						$outCLNMAN=$outCLNMAN."<td id='td_outCLNMAN_titulo_" . $publicacao->id . "'>".$publicacao->titulo."</td>";
						$outCLNMAN=$outCLNMAN."<td id='td_outCLNMAN_autores_" . $publicacao->id . "'>".$publicacao->autores."</td>";
						$outCLNMAN=$outCLNMAN."<td id='td_outCLNMAN_prim_" . $publicacao->id . "'>".$publicacao->primpagina."</td>";
						$outCLNMAN=$outCLNMAN."<td id='td_outCLNMAN_ult_" . $publicacao->id . "'>".$publicacao->ultpagina."</td>";
						$outCLNMAN=$outCLNMAN."<td id='td_outCLNMAN_link_" . $publicacao->id . "'><a href='".$publicacao->link."' target='_blank'>".$publicacao->link."</a></td>";
						$outCLNMAN=$outCLNMAN."<td id='td_outCLNMAN_estado_" . $publicacao->id . "'>".getEstadoPublicacao($publicacao->estado)."</td>";
						$outCLNMAN=$outCLNMAN. "<td></td>";
						$outCLNMAN=$outCLNMAN. "<td></td>";
						$outCLNMAN=$outCLNMAN. "<td></td>";							   	
						$outCLNMAN=$outCLNMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#tabela').text('" . $acoes[$i][0]->tabela. "');$('#dep').text('" . $acoes[$i][0]->departamento. "');$('#chave-pubNacMan-CLiv').text('" . $publicacao->id . "');$('#tabela').text('13');\"></td>";						
					$outCLNMAN=$outCLNMAN."</tr>";	
				}
				break;
			}
		}
		
		foreach ($acoes[$i] as $j => $value2) {
			if($acoes[$i][$j]->acao == 4) {	
				$tipo = getTipoFmup($acoes[$i][$j]->descricao);
				switch($tipo){
					case 'JN': { $outJNMAN=$outJNMAN.imprimePubManIntJ($acoes[$i][$j], ''); } break;
					case 'PRPN': { $outPRPNMAN=$outPRPNMAN.imprimePubManIntJ($acoes[$i][$j], ''); } break;
					case 'JAN': { $outJANMAN=$outJANMAN.imprimePubManIntJ($acoes[$i][$j], ''); } break;	
					case 'CPN': { $outCPNMAN=$outCPNMAN.imprimePubManIntCP($acoes[$i][$j], ''); } break;
					case 'OAN': { $outOANMAN=$outOANMAN.imprimePubManIntCP($acoes[$i][$j], ''); } break;	
					case 'LN': { $outLNMAN=$outLNMAN.imprimePubManIntL($acoes[$i][$j], ''); } break;
					case 'CLN': { $outCLNMAN=$outCLNMAN.imprimePubManIntCL($acoes[$i][$j], ''); } break;
				}	
			} else {
				$db = new Database();		
				$publicacao = $db->getPublicacaoManualNacionalFromDB($acoes[$i][$j]->idreg);	
				$tipo = $publicacao->tipofmup;
				switch($tipo){
					case 'JN': { $outJNMAN=$outJNMAN.imprimePubManIntJ($acoes[$i][$j], $publicacao);	} break;
					case 'PRPN': { $outPRPNMAN=$outPRPNMAN.imprimePubManIntJ($acoes[$i][$j], $publicacao); } break;
					case 'JAN': { $outJANMAN=$outJANMAN.imprimePubManIntJ($acoes[$i][$j], $publicacao); } break;	
					case 'CPN': { $outCPNMAN=$outCPNMAN.imprimePubManIntCP($acoes[$i][$j], $publicacao); } break;
					case 'OAN': { $outOANMAN=$outOANMAN.imprimePubManIntCP($acoes[$i][$j], $publicacao); } break;	
					case 'LN': { $outLNMAN=$outLNMAN.imprimePubManIntL($acoes[$i][$j], $publicacao); } break;
					case 'CLN': { $outCLNMAN=$outCLNMAN.imprimePubManIntCL($acoes[$i][$j], $publicacao); } break;
				}	
			}		
		}
	}	
	
	echo "<fieldset class='normal'><div id='pubManualNacional'>";
		echo"<div id='pubManualNac-Art'><table id='pubManNac-Art' class='box-table-b'><caption><u><h2>Publicações Manuais Não Inglês</h2></u></caption><caption >ARTIGOS</caption>";echo $tcolsJ;echo "<tbody>";echo $outJNMAN; echo "</body></table><p id='chave-pubNacMan-Art' hidden></p></div><br>";
		echo"<div id='pubManualNac-Rev'><table id='pubManNac-Rev' class='box-table-b'><caption >PEER REVIEW PROCEEDINGS</caption>";echo $tcolsJ;echo "<tbody>";echo $outPRPNMAN;echo "</body></table><p id='chave-pubNacMan-Rev' hidden></div><br>";
		echo"<div id='pubManualNac-Sum'><table id='pubManNac-Sum' class='box-table-b'><caption >SUMÁRIOS<p><i>JOURNAL ABSTRACTS</i></p></caption>";echo $tcolsJ;echo "<tbody>";echo $outJANMAN;echo "</body></table><p id='chave-pubNacMan-Sum' hidden></div><br>";
		echo"<div id='pubManualNac-Liv'><table id='pubManNac-Liv' class='box-table-b'><caption >LIVROS<p><i>BOOKS</i></p></caption>";echo $tcolsLIV;echo "<tbody>";echo $outLNMAN;echo "</body></table><p id='chave-pubNacMan-Liv' hidden></div><br>";
		echo"<div id='pubManualNac-CLiv'><table id='pubManNac-CLiv' class='box-table-b'><caption >CAPITULOS DE LIVROS<p><i>BOOK CHAPTERS</i></p></caption>";echo $tcolsCLIV;echo "<tbody>";echo $outCLNMAN;echo "</body></table><p id='chave-pubNacMan-CLiv' hidden></div><br>";
		echo"<div id='pubManualNac-Con'><table id='pubManNac-Con' class='box-table-b'><caption >CONFERENCE PROCEEDINGS</caption>";echo $tcolsCONF;echo "<tbody>";echo $outCPNMAN;echo "</body></table><p id='chave-pubNacMan-Con' hidden></div><br>";
		echo"<div id='pubManualNac-Oth'><table id='pubManNac-Oth' class='box-table-b'><caption >OUTROS SUMÁRIOS<br>OTHER ABSTRACTS</caption>";echo $tcolsCONF;echo "<tbody>";echo $outOANMAN;echo "</body></table><p id='chave-pubNacMan-Oth' hidden></div><br>";
	echo "</div></fieldset>";
}

function getEstadoPublicacao($i) {
	$db = new Database();
	$lValues =$db->getLookupValues("lista_estadoPublicacoes");
	$texto="";
	while ($row = mysql_fetch_assoc($lValues)) {
		if($i==$row["ID"])
			$texto=$row["DESCRICAO"];
	}
	
	$db->disconnect();
	return $texto;
}
?>