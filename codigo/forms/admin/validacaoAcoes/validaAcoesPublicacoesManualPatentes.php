<?php

require_once '../../classlib/Publicacao.class.inc';

function showAcoesPublicacoesManualPatentes($dadosNew) {
	
	echo "<fieldset class='normal'><div id='pubManualPatentes'>";		
		echo "<table id='pubManPat' class='box-table-b'>
			<caption><u><h2>Patentes Manuais</h2></u></caption>";
			echo "<thead>
					<tr>
						<th>ID INV</th>
						<th>Nº PATENTE</th>
						<th>IPCs</th>
						<th>TÍTULO</th>
						<th>AUTORES</th>		
						<th>DATA de PUBLICAÇÃO</th>
						<th>Link</th>
						<th>Estado</th>
						<th><b>Validar</b></th>
						<th><b>Eliminar</b></th>	
						<th><b>Informações</b></th>
						<th><b>Edição</b></th>
					</tr>
				</thead>
				<tbody>";
				
	$acoes = array();
	$dadosNew2 = $dadosNew;
	
	while ( list($key, $val) = each($dadosNew) ) {	
		$acoes[$key]=array();
		
		while (list($key2, $val2) = each($dadosNew2)) {

			if($val->idreg == $val2->idreg) {							
				$acoes[$key][]=$dadosNew2[$key2];	  
				unset($dadosNew2[$key2]);				  
				unset($dadosNew[$key2]);
				continue;				
			}

		}
		unset($dadosNew2[$key]);
		reset($dadosNew2);
	}	
	
	foreach ($acoes as $i => $value) {
		if($acoes[$i][0]->idreg != '') {
			$db = new Database();		
			$publicacao = $db->getPublicacaoManualPatenteFromDB($acoes[$i][0]->idreg);
			
			echo "<tr>";
				echo "<td>".$publicacao->idinv."</td>";
				echo "<td id='td_pubManualPatentes_npatente_" . $publicacao->id . "'>".$publicacao->npatente."</td>";
				echo "<td id='td_pubManualPatentes_ipc_" . $publicacao->id . "'>".$publicacao->ipc."</td>";
				echo "<td id='td_pubManualPatentes_titulo_" . $publicacao->id . "'>".$publicacao->titulo."</td>";
				echo "<td id='td_pubManualPatentes_autores_" . $publicacao->id . "'>".$publicacao->autores."</td>";										
				echo "<td id='td_pubManualPatentes_datapatente_" . $publicacao->id . "'>".$publicacao->datapatente."</td>";
				echo "<td id='td_pubManualPatentes_link_" . $publicacao->id . "'><a href='".$publicacao->link."' target='_blank'>".$publicacao->link."</a></td>";
				echo "<td id='td_pubManualPatentes_estado_" . $publicacao->id . "'>".getEstadoPublicacaoPatente($publicacao->estado)."</td>";	
				echo "<td></td>";
				echo "<td></td>";
				echo "<td></td>";			
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#dep').text('" . $acoes[$i][0]->departamento. "');$('#chave-pubManPatentes').text('" . $publicacao->id . "');\"></td>";
			echo "</tr>";	
		}
		
		foreach ($acoes[$i] as $j => $value2) {
			if($acoes[$i][$j]->acao == 4) {	
				imprimePubManPatente($acoes[$i][$j], '');				
			} else {
				$db = new Database();		
				$publicacao = $db->getPublicacaoManualPatenteFromDB($acoes[$i][$j]->idreg);
				imprimePubManPatente($acoes[$i][$j], $publicacao);
			}		
		}		
	}	
	
	echo "</tbody>";
		echo "</table>";
		echo "<p id='chave-pubManPatentes' hidden></p>";
    echo "</div></fieldset>";
}

function imprimePubManPatente($dadosNew, $publicacao) {
	switch($dadosNew->acao) {
		case 1: {				
			//Edição						
			$cena = explode("TITULO='",$dadosNew->descricao);
						
			$cena1 = explode("', AUTORES='",$cena[1]);
			$titulo = $cena1[0];
			
			$cena2 = explode("',IPC='",$cena1[1]);
			$autores = $cena2[0];
			
			$cena3 = explode("', NPATENTE='",$cena2[1]);
			$ipc= $cena3[0];
			
			$cena4 = explode("', DATAPATENTE='",$cena3[1]);
			$npatente= $cena4[0];	
			
			$cena5 = explode("', LINK='",$cena4[1]);
			$datapatente= $cena5[0];	
			
			$cena6 = explode("', ESTADO=",$cena5[1]);
			$link= $cena6[0];	
			
			$cena7 = explode(" where",$cena6[1]);
			$estado= $cena7[0];	
								
			echo "<tr style='border-top: solid yellow'>";	
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$publicacao->idinv."</td>";
				if($publicacao->npatente != $npatente) {
					echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px;'>".$npatente. "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$publicacao->npatente."</td>";
				}
				
				if($publicacao->ipc != $ipc) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $ipc. "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$publicacao->ipc. "</td>";
				}		

				if($publicacao->titulo != $titulo) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $titulo. "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$publicacao->titulo. "</td>";
				}	
				
				if($publicacao->autores != $autores) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $autores. "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$publicacao->autores. "</td>";
				}	

				if($publicacao->datapatente != $datapatente) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $datapatente. "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$publicacao->datapatente. "</td>";
				}	

				if($publicacao->link != $link) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'><a href='". $link. "' target='_blank'>". $link. "</a></td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><a href='".$publicacao->link. "' target='_blank'>".$publicacao->link. "</a></td>";
				}
					
				if($publicacao->estado != $estado) {
					echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".getEstadoPublicacaoPatente($estado). "</td>";
				} else {
					echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".getEstadoPublicacaoPatente($publicacao->estado). "</td>";
				}
										
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			echo "</tr>";
		}
		break;
		case 2: {
			//Eliminação
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_warning.png\" name='navOption'></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;' colspan='14'>Esta habilitacao foi assinalado como dispensável. Se pretender confirmar esta ação, clique no visto. Caso contrário, clique na cruz.</td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
			echo "<tr>";
		}
		break;
		case 3: {
			//Observação
			echo "<tr>";
				echo "<td colspan='8' style='background-color:#99CCFF;'><u>Observações:</u> ";
				echo $dadosNew->descricao;
			echo "</td></tr>";
		}
		break;
		case 4: {
			$cena = explode("VALUES (",$dadosNew->descricao);								
			$cena1 = explode(",",$cena[1]);					
			$tudo = str_replace("'","",$cena1);
											
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$tudo[0]. "</td>";			
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".$tudo[2]. "</td>";			
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[3]. "</td>";			
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[5]. "</td>";			
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[6]. "</td>";		
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>". $tudo[4]. "</td>";			
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'><a href='". $tudo[7]. "' target='_blank'>". $tudo[7]. "</a></td>";			
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>".getEstadoPublicacaoPatente($tudo[8]). "</td>";				
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			echo "</tr>";
		}
		break;
	}		
}

function getEstadoPublicacaoPatente($i) {
	$db = new Database();
	$lValues =$db->getLookupValues("lista_estadoPublicacoes");
	$texto="";
	while ($row = mysql_fetch_assoc($lValues)) {
		if($i==$row["ID"])
			$texto=$texto.$row["DESCRICAO"];
	}

	$db->disconnect();
	return $texto;
}

?>