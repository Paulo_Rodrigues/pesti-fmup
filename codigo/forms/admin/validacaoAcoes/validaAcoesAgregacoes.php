<?php

require_once '../../classlib/Agregacao.class.inc';

function showAcoesAgregacoes($dadosNew) {
	echo"<fieldset class='normal'>
			<div id='agregacoes'>";
		
	echo "<table id='agreg' class='box-table-b'>		
			  <caption><u><h2>Agregações</h2></u></caption>
			<tr>
				<th>ID Investigador</th>
				<th>Unanimidade</th>
				<th><b>Validar</b></th>
				<th><b>Eliminar</b></th>	
				<th><b>Informações</b></th>
				<th><b>Edição</b></th>
			</tr>";
			
	$acoes = array();
	$dadosNew2 = $dadosNew;
	
	while ( list($key, $val) = each($dadosNew) ) {	
		$acoes[$key]=array();
		
		while (list($key2, $val2) = each($dadosNew2)) {

			if($val->idreg == $val2->idreg) {							
				$acoes[$key][]=$dadosNew2[$key2];	  
				unset($dadosNew2[$key2]);				  
				unset($dadosNew[$key2]);
				continue;				
			}

		}
		unset($dadosNew2[$key]);
		reset($dadosNew2);
	}
	
	foreach ($acoes as $i => $value) {
		if($acoes[$i][0]->idreg != '') {
			$db = new Database();		
			$agregacao = $db->getAgregacaoObjFromDB($acoes[$i][0]->idreg);
			
			echo "<tr>
				<td>".$agregacao->idinv."</td>";
				if($agregacao->unanimidade == 1)
					echo "<td id='td_unanimidade_agr_" .$agregacao->id. "'>Sim</td>";
				else
					echo "<td id='td_unanimidade_agr_" .$agregacao->id. "'>Não</td>";
					
				echo "<td></td>";
				echo "<td></td>";
				echo "<td></td>";	
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#dep').text('" . $acoes[$i][0]->departamento. "');$('#chave-agr').text('" . $agregacao->id . "');\"></td>";
			echo "</tr>";
		}
		
		foreach ($acoes[$i] as $j => $value2) {
			if($acoes[$i][$j]->acao == 4) {	
				imprimeAcaoAgregacao($acoes[$i][$j], '');				
			} else {
				$db = new Database();		
				$agregacao = $db->getAgregacaoObjFromDB($acoes[$i][$j]->idreg);
				imprimeAcaoAgregacao($acoes[$i][$j], $agregacao);
			}		
		}		
	}
		
	echo "</table>
	<p id='chave-agr' hidden></p>
	</div></fieldset>";
}

function imprimeAcaoAgregacao($dadosNew, $agregacao) {
	switch($dadosNew->acao) {
		case 1: {				
			//Edição		
			$cena = explode("UNANIMIDADE=",$dadosNew->descricao);			

			$cena1 = explode(" where",$cena[1]);
			$unanimidade = $cena1[0];
			
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'>".$agregacao->idinv."</td>";			
				if($agregacao->unanimidade != $unanimidade) {
					if($unanimidade == 1)
						echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px; id='td_unanimidade_agr_" .$agregacao->id. "'>Sim</td>";
					else
						echo "<td style='background-color:#99CCFF;  border-bottom: solid yellow 2px; id='td_unanimidade_agr_" .$agregacao->id. "'>Não</td>";
				} else {
					if($agregacao->unanimidade == 1)
						echo "<td style='background-color:#FFFFFF;  border-bottom: solid yellow 2px; id='td_unanimidade_agr_" .$agregacao->id. "'>Sim</td>";
					else
						echo "<td style='background-color:#FFFFFF;  border-bottom: solid yellow 2px; id='td_unanimidade_agr_" .$agregacao->id. "'>Não</td>";
				}
									
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			echo "</tr>";
		}
		break;
		case 2: {
			//Eliminação
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_warning.png\" name='navOption'></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;' colspan='13'>Esta habilitacao foi assinalado como dispensável. Se pretender confirmar esta ação, clique no visto. Caso contrário, clique na cruz.</td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
			echo "<tr>";
		}
		break;
		case 3: {
			//Observação
			echo "<tr>";
				echo "<td colspan='2' style='background-color:#99CCFF;'><u>Observações:</u> ";
				echo $dadosNew->descricao;
			echo "</td></tr>";
		}
		break;
		case 4: {
			$cena = explode("VALUES (",$dadosNew->descricao);								
			$cena1 = explode(",",$cena[1]);					
			$tudo = str_replace("'","",$cena1);
						
			echo "<tr style='border-top: solid yellow'>";
				echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>" . str_replace(");","",$tudo[1]) . "</td>";			
					if($tudo[0] == 1) {
						echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>Sim</td>";
					}else{
						echo "<td style='background-color:#99CCFF; border-bottom: solid yellow 2px;'>Não</td>";
					}				
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_valid.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');validaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');eliminaAcao();\" ></center></td>";
				echo "<td style='background-color:#FFFFFF; border-bottom: solid yellow 2px;'><center><input type='image' src=\"../../images/icon_help.png\" name='navOption' onclick=\"$('#chave-acao').text('" . $dadosNew->id . "');mostraInfoAcao('" .$dadosNew->autor. "', '" .$dadosNew->data. "');\" ></center></td>";
			echo "</tr>";
		}
		break;	
		
	}		
}

?>