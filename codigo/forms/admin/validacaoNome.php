<?php
$a = session_id();
if(empty($a)){
	echo "<br>Acesso Impedido<br>";
	echo "Usar Link: <a href='https://lgdf.med.up.pt/'> LGDF </a>";
	break("erro de sessão");
}

echo "<input type='hidden' id='investigador' name='investigador' />";
echo "<input type='hidden' id='data_antiga' name='data_antiga' />";

echo "<fieldset class='normal'>\n";

echo "
    <table class='box-table-c'>
    <!-- Results table headers -->
    <caption>Já lacrados</caption>
    <tr>
		<th>PubID</th>
		<th>TIPO</th>
		<th>INDEXED ISI</th>
		<th>INDEXED PUBMED</th>
      <th>IdINV</th>
      <th>Autores</th>
      <th>Nome Cientifico abrev</th>
      <th>Nome Cientifico abrev extenso</th>
    </tr>";

$nvalidado=0;
$nnaovalidados=0;
$npublicacoes=0;

	$db1 = new Database();
	$db2 = new Database();
		$lValues =$db1->adminGetPublicacoes();

		while ($row = mysql_fetch_assoc($lValues)) {
			$validado=0;
			$npublicacoes++;
			echo "<tr>";
			echo "<td>";
			echo $row['ID'];
			echo "</td>";
			echo "<td>";
			echo $row['TIPOFMUP'];
			echo "</td>";
			echo "<td>";
			echo $row['INDEXED_ISI'];
			echo "</td>";
			echo "<td>";
			echo $row['INDEXED_PUBMED'];
			echo "</td>";
				
			echo "<td>";
			echo $row['IDINV'];
			echo "</td>";
			echo "<td>";
			echo $row['AUTORES'];			
			echo "</td>";
			
			$nomeValues = $db2->adminGetNomeCientificos($row['IDINV']);
			
			while ($rowNomes = mysql_fetch_assoc($nomeValues)) {
				echo "<td>";
				echo $rowNomes['NOMECIENTINI'];
				echo "</td>";
				if(checkAutor($row['AUTORES'],$rowNomes['NOMECIENTINI'])){
					echo "<td style='background-color:#00FF00;'>";
					echo "CHECK";
					$validado=1;
					echo "</td>";
				}else{
					echo "<td style='background-color:#FF0000;'>";
					echo "NOT CHECK";
					echo "</td>";
				}
				echo "<td>";
				echo $rowNomes['NOMECIENTCOM'];
				echo "</td>";
				if(checkAutor($row['AUTORES'],$rowNomes['NOMECIENTCOM'])){
					echo "<td style='background-color:#00FF00;'>";
					echo "CHECK";
					$validado=1;
					echo "</td>";
				}else{
					echo "<td style='background-color:#FF0000;'>";
					echo "NOT CHECK";
					echo "</td>";
				}
			}
			if($validado==0)
				$nnaovalidado++;
			else
				$nvalidado++;
						
			echo "</tr>\n";

    }
    $db2->disconnect();
	$db1->disconnect();
			  

    echo "</table>";
    
    
    echo "
    <table class='box-table-c'>
    <!-- Results table headers -->
    <caption>Estatisticas</caption>
    <tr>
		<th>VALIDADOS</th>
		<th>NAO VALIDADOS</th>
    </tr><tr>";
    echo "<td style='background-color:#00FF00;'>";
    echo $nvalidado."/".$npublicacoes;
    echo "</td>";
    echo "<td style='background-color:#FF0000;'>";
    echo $nnaovalidado."/".$npublicacoes;
    echo "</td></tr>";

	echo "</fieldset>";

	

	function checkAutor($autores,$nome){
		$caracteres=array(' '," ",".");
		
		$autoreslimpo = str_replace($caracteres,"",$autores);
		
		$autoreslimpo=preg_replace( '/\s+/', '', $autoreslimpo );
		
		$nomelimpo = str_replace($caracteres,"",$nome);
		$nomelimpo=preg_replace( '/\s+/', '', $nomelimpo );
		
		$autoreslimpo =$autoreslimpo.";";
		$nomelimpo=$nomelimpo.";";
		
		

		$validade=false;

			if(strlen($nomelimpo)>1 && strchr($autoreslimpo,$nomelimpo))
				$validade=true;	
	
		//echo $autoreslimpo."-".$nomelimpo."-".$validade."<br>";

		return $validade;
	
	
	}
	
?>