<?php
$a = session_id();
if(empty($a)){
	echo "<br>Acesso Impedido<br>";
	echo "Usar Link: <a href='https://lgdf.med.up.pt/'> LGDF </a>";
	break("erro de sessão");
}


echo "
    <table class='box-table-c'>
    <!-- Results table headers -->
    <caption>Tabelas</caption>
    <tr>
      <th>Tabela Principal</th>
      <th>Tabelas Lookup Relacionadas</th>
    </tr>";

echo "<tr>";
echo "<td><a href='./export/exportacao/exportDataInvestigador.php'>Dados Investigadores (tabela investigadores - Todas as tabelas ligam com esta atraves do campo: IDINV)</a></td>
	  		<td><a href='./export/exportacao/exportDataPaises.php'>Lookup Paises</a><br><a href='./export/exportacao/exportDataConcelhos.php'> Lookup Concelhos</a><br><a href='./export/exportacao/exportDataFreguesias.php'>Lookup Freguesias</a></td>";
echo "</tr>";
echo "<tr>";
echo "<td><a href='./export/exportacao/exportDataCursos.php'>Dados Cursos (tabela habilitacoesliterarias)</a></td>
			<td><a href='./export/exportacao/exportDataGrauAcademico.php'>Lookup Graus Academicos</a><br><a href='./export/exportacao/exportDataBolsas.php'>Lookup Bolsas</a>
		<br><a href='./export/exportacao/exportDataAnosLetivos.php'>Lookup Anos Letivos</a></td>";
echo "</tr>";
echo "<tr>";
echo "<td><a href='./export/exportacao/exportDataAgregacoes.php'>Dados Agregacoes (tabela agregacoes)</a></td>
			<td></td>";		
echo "</tr>";
echo "<tr>";
echo "<td><a href='./export/exportacao/exportDataOcuProfissional.php'>Caracterização + Dedicação (tabela ocupacaoprofissional)</a></td>
			<td><a href='./export/exportacao/exportDataBolsas.php'>Lookup Bolsas</a></td>";
echo "</tr>";
echo "<tr><td>
 <a href='./export/exportacao/exportDataExpProfissional.php'>Dados Experiencia Profissional (tabela expprofissional)</a>
 </td><td></td></tr>
 <tr>";
//echo "<tr>";
//echo "<td><a href='./export/exportacao/exportDataOrgaosUPFMUP.php'>Orgãos UP/FMUP  (tabela lista_orgaosup)</a></td>
//			<td></td>";
//echo "</tr>";
echo "<tr>";
echo "<td><a href='./export/exportacao/exportDataPublicacoes.php'>Publicacoes (tabela publicacoes)</a></td>
			<td></td>";
echo "</tr>";
echo "<tr>";
echo "<td><a href='./export/exportacao/exportDataCargos.php'>Cargos UP/FMUP  (tabela cargos)</a></td>
			<td>Tipo de instituição 1 - FMUP ; 2 - UP<br>
		<a href='./export/exportacao/exportDataOrgaosHospital.php'>Lookup Lista Orgaos Hospital</a></td>";
echo "</tr>";
echo "<tr>";
echo "<td><a href='./export/exportacao/exportDataDirCurso.php'>Direcoes de Curso (tabela dircursos)</a></td>
			<td><a href='./export/exportacao/exportDataGrauAcademico.php'>Lookup Graus Academicos</a></td>";
echo "</tr>";
echo "<tr>";
echo "<td><a href='./export/exportacao/exportDataDadosCientificos.php'>Dados Cientificos (tabela dadoscientificos)</a></td>
			<td></td>";
echo "</tr>";
echo "<tr>";
echo "<td><a href='./export/exportacao/exportDataColaboradoresFMUP.php'>Dados Colaboradores FMUP (tabela colaboradoresinternos)</a></td>
			<td><a href='./export/exportacao/exportDataServicosFMUP.php'>Lookup Servicos FMUP </a></td>";
echo "</tr>";
echo "<tr>";
echo "<td><a href='./export/exportacao/exportDataColaboradores.php'>Dados Colaboradores (tabela colaboradores)</a></td>
			<td><a href='./export/exportacao/exportDataPaises.php'>Lookup Paises</a></td>";
echo "</tr>";
echo "<tr>";
echo "<td><a href='./export/exportacao/exportDataUnidadesInvestigacao.php'>Dados Unidades de Investigacao (tabela unidadesinvestigacao)</a>
			</td><td><a href='./export/exportacao/exportDataTipoUniInv.php'>Lookup Tipo Unidade de Investigacao </a></td>";
echo "</tr>";
echo "<tr>";
echo "<td><a href='./export/exportacao/exportDataProjetos.php'>Dados Projetos (tabela projectosinv)</a></td>
			<td><a href='./export/exportacao/exportDataEntidades.php'>Lookup Entidades </a></td>";
echo "</tr>";

echo "<tr><td>
 <a href='./export/exportacao/exportDataArbitragensProjetos.php'>Dados Arbitragens de Projetos (tabela arbitragensprojetos)</a>
 </td><td></td></tr>";

echo "<tr><td>
 <a href='./export/exportacao/exportDataArbitragensRevistas.php'>Dados Edição/Arbitragens de Revistas (tabela arbitragensrevistas)</a>
 </td><td><a href='./export/exportacao/exportDataTipoArbitragens.php'>Lookup Tipos Arbitragens Revistas </a></td></tr>";

echo "<tr><td>
 <a href='./export/exportacao/exportDataConferencias.php'>Dados Conferencias (tabela conferencias)</a>
 </td><td><a href='./export/exportacao/exportDataAmbitoConf.php'>Lookup Ambitos Conferencias</a>
		<a href='./export/exportacao/exportDataTipoConf.php'>Lookup Tipo Conferencias </a></td></tr>";

echo "<tr><td>
 <a href='./export/exportacao/exportDataApresConferencias.php'>Dados Apresentacoes Conferencias (tabela apresentacoes)</a>
 </td><td></td></tr>";

echo "<tr><td>
 <a href='./export/exportacao/exportDataPremios.php'>Dados Premios (tabela premios)</a>
 </td><td><a href='./export/exportacao/exportDataTipoPremio.php'>Lookup Tipo Premio </a></td></tr>";

//echo "<tr>";
//echo "<td><a href='./export/exportacao/exportDataSocCientifica.php'>Dados Sociedades Cientificas (tabela sociedadescientificas)</a></td>
//		<td></td>";
//echo "</tr>";
echo "<tr>";
echo "<td><a href='./export/exportacao/exportDataOrientacoes.php'>Dados Orientacoes  (tabela orientacoes)</a></td>
		<td><a href='./export/exportacao/exportDataTipoOrientacao.php'>Lookup Tipo Orientacao</a><br><a href='./export/exportacao/exportDataTipoOrientador.php'>Lookup Tipo Orientador</a><br><a href='./export/exportacao/exportDataEstadoOrientacao.php'>Lookup Estado Orientacao</a></td>";
//echo "<tr>";
//echo "<td><a href='./export/exportacao/exportDataAcreditacoes.php'>Dados Acreditacoes  (tabela acreditacoes)</a></td>
//		<td></td>";
//echo "</tr>";
echo "<tr><td><a href='./export/exportacao/exportDataSocCientifica.php'>Dados Sociedades Cientificas (tabela sociedadescientificas)</a></td>
		<td><a href='./export/exportacao/exportDataTipoSC.php'>Lookup Tipo Soc Cient</a></td>";
echo "<tr>";

echo "<tr><td><a href='./export/exportacao/exportDataRedes.php'>Dados Redes  (tabela Redes)</a></td>
		<td><a href='./export/exportacao/exportDataTipoRedes.php'>Lookup Tipo Redes</a></td>";
echo "<tr>";

echo "<tr><td>
 <a href='./export/exportacao/exportDataMissoes.php'>Dados Missões </a>
 </td><td></td></tr>";

echo "<tr><td>
 <a href='./export/exportacao/exportDataAulas.php'>Dados Aulas (tabela aulas)</a>
 </td><td></td></tr>";
echo "<tr><td>
 <a href='./export/exportacao/exportDataJuris.php'>Dados Juris (tabela juris)</a>
 </td><td></td></tr>";

echo "<tr><td>
 <a href='./export/exportacao/exportDataOutrasActividades.php'>DadosOutrasActividades (tabela OutrasActividades)</a>
 </td><td></td></tr>";

echo "<tr><td>
<a href='./export/exportacao/exportDataAcoesDivulgacao.php'>DadosAcoesDivulgacao (tabela AcoesDivulgacao)</a>
 </td><td></td></tr>";

echo "<tr><td>
 <a href='./export/exportacao/exportDataProdutos.php'>Dados Produtos (tabela produtos)</a>
 </td><td></td></tr>";

echo "<tr><td>
 <a href='./export/exportacao/exportDataAcoesFormacao.php'>Dados Acoes de Formacao (tabela acoesformacao)</a>
 </td><td><a href='./export/exportacao/exportDataTipoFormandos.php'>Lookup Tipo Formandos </a></td></tr>
 <tr><td>
 <a href='./export/exportacao/exportDataAcreditacoes.php'>Dados Acreditacoes (tabela acreditacoes)</a>
 </td><td></td></tr>";
 echo "<tr><td>
 <a href='./export/exportacao/exportDataObsFinais.php'>Dados Observacoes Finais (tabela observacoesfinais)</a>
 </td><td></td></tr>";
echo "</table>";


// echo "<table border=1>
// <tr><td>
// <a href='./exportacao/exportDataInvestigador.php'>Dados Investigadores (tabela investigadores - Todas as tabelas ligam com esta atraves do campo: IDINV)</a>
// </td><td><a href='./exportacao/exportDataPaises.php'>Lookup Paises --- </a><a href='./exportacao/exportDataConcelhos.php'> Lookup Concelhos --- </a> <a href='./exportacao/exportDataFreguesias.php'>Lookup Freguesias</a></td></tr>
// <tr><td>

// <a href='./exportacao/exportDataAnosLetivos.php'>Lookup Anos Letivos <a href='./exportacao/exportDataBolsas.php'>Lookup Bolsas <a href='./exportacao/exportDataDistincao.php'>Lookup Distincao</a>
// </td></tr>
// <tr><td>
// <a href='./exportacao/exportDataMissoes.php'>Dados MISSOES </a>
// </td><td></td></tr>
// <tr><td>
// <a href='./exportacao/exportDataExpProfissional.php'>Dados Experiencia Profissional (tabela expprofissional)</a>
// </td><td></td></tr>
// <tr><td>

// </td><td><a href='./exportacao/exportDataTipoDocente.php'>Lookup Tipo Docente --- </a><a href='./exportacao/exportDataTipoInvestigador.php'>Lookup Tipo Investigador </a></td></tr>
// <a href='./exportacao/exportDataAtividadeCientifica.php'>Dados Atividade Cientifica 2010 (tabela actividadecientifica)</a>
// </td><td></td></tr>
// <tr><td>
// <a href='./exportacao/exportDataOrgaosUPFMUP.php'>Dados Cargos UP/FMUP  (tabela lista_orgaosup)</a>
// </td><td>No valor da instituicao: 1->FMUP e 2->UP</td></tr>
// <tr><td>
// <a href='./exportacao/exportDataMontanteProj.php'>Dados Montante Projectos (tabela montanteprojectos)</a>
// </td><td><a href='./exportacao/exportDataEntidades.php'>Lookup Entidades </a></td></tr>
// <tr><td>
// <a href='./exportacao/exportDataDirCurso.php'>Dados Direcoes de Curso (tabela dircursos)</a>
// </td><td><a href='./exportacao/exportDataGrauAcademico.php'>Lookup Graus Academicos </a></td></tr>
// <tr><td>
// <a href='./exportacao/exportDataPublicacoes.php'>Dados Publicacoes (tabela publicacoes)</a>
// </td><td><a href='./exportacao/exportDataTipoPub.php'>Lookup Tipo Publicacoes  </a></td></tr>
// <tr></tr>

// <a href='./exportacao/exportDataProjetos.php'>Dados Projetos (tabela projectosinv)</a>
// </td><td><a href='./exportacao/exportDataEntidades.php'>Lookup Entidades </a></td></tr>
// <tr><td>
// <a href='./exportacao/exportDataArbitragensProjetos.php'>Dados Arbitragens de Projetos (tabela arbitragensprojetos)</a>
// </td><td></td></tr>
// <tr><td>
// <a href='./exportacao/exportDataConferencias.php'>Dados Conferencias (tabela conferencias)</a>
// </td><td><a href='./exportacao/exportDataAmbitoConf.php'>Lookup Ambitos Conferencias --- </a><a href='./exportacao/exportDataTipoConf.php'>Lookup Tipo Conferencias </a></td></tr>
// <tr><td>
// <a href='./exportacao/exportDataArbitragensRevistas.php'>Dados Arbitragens de Revistas (tabela arbitragensrevistas)</a>
// </td><td><a href='./exportacao/exportDataTipoArbitragens.php'>Lookup Tipos Arbitragens Revistas </a></td></tr>
// <tr><td>
// <a href='./exportacao/exportDataPremios.php'>Dados Premios (tabela premios)</a>
// </td><td><a href='./exportacao/exportDataTipoPremio.php'>Lookup Tipo Premio </a></td></tr>
// <tr><td>
// <a href='./exportacao/exportDataSocCientifica.php'>Dados Sociedades Cientificas (tabela sociedadescientificas)</a>
// </td><td><a href='./exportacao/exportDataTipoSC.php'>Lookup Tipo Sociedade Cientifica </a></td></tr>
// <tr><td>
// <a href='./exportacao/exportDataOrientacoes.php'>Dados Orientacoes  (tabela orientacoes)</a>
// </td><td><a href='./exportacao/exportDataTipoOrientacao.php'>Lookup Tipo Orientacao </a></td></tr>
// <tr><td>
// <a href='./exportacao/exportDataJuris.php'>Dados Juris (tabela juris)</a>
// </td><td></td></tr>
// <tr><td>
// <a href='./exportacao/exportDataAulas.php'>Dados Aulas (tabela aulas)</a>
// </td><td></td></tr>
// <tr><td>
// <a href='./exportacao/exportDataAcoesDivulgacao.php'>DadosAcoesDivulgacao (tabela AcoesDivulgacao)</a>
// </td><td></td></tr>
// <tr><td>
// <a href='./exportacao/exportDataOutrasActividades.php'>DadosOutrasActividades (tabela OutrasActividades)</a>
// </td><td></td></tr>
// <tr><td>
// <a href='./exportacao/exportDataProdutos.php'>Dados Produtos (tabela produtos)</a>
// </td><td></td></tr>
// <tr><td>
// <a href='./exportacao/exportDataAcoesFormacao.php'>Dados Acoes de Formacao (tabela acoesformacao)</a>
// </td><td><a href='./exportacao/exportDataTipoFormandos.php'>Lookup Tipo Formandos </a></td></tr>
// <tr><td>
// <a href='./exportacao/exportDataAcreditacoes.php'>Dados Acreditacoes (tabela acreditacoes)</a>
// </td><td></td></tr>
// <tr><td>
// <a href='./exportacao/exportDataObsFinais.php'>Dados Observacoes Finais (tabela observacoesfinais)</a>
// </td><td></td></tr>
// </table>";
	
?>