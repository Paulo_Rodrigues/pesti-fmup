<?php
echo "<div id='content14' style='display: inline;'>";

echo "<p><fieldset class='normal'>\n";
echo "<legend>Colaboradores Atuais na FMUP/<i>Current FMUP Collaborators</i></legend>\n";

echo "<p class='ppthelp'>Colaborador é todo/a aquele/a que faz investigação de forma ativa em colaboração consigo, e.g., tendo artigos científicos comuns em preparação. No que segue preencha o(s) nome(s) completo do(s) seu(s) colaborador(es) – se estiver na base de dados até aparece automaticamente; se necessário, preencha também o(s) nome(s) científico(s) do(s) seu(s) colaborador(es), exatamente no formato indicado, um por linha, não utilizando acentos ou cedilhas; se não apareceu automaticamente, indique o respetivo Departamento/Serviço da FMUP.</p>";

echo "<p class='penhelp'><i>A collaborator is an individual that does active research in cooperation with you namely by having common scientific papers in preparation. In what follows please fill the full name(s) of your scientific collaborator(s) – it might appear automatically too; if needed, please also fill the scientific name(s) of your scientific collaborator(s), exactly in the format shown, one per line, without any accent or other marks on the letters; also his/her/their Departament/Service of FMUP.</i></p>";
	
	
echo "
<table id='colfmup' class='box-table-b'>
<tbody>
<!-- Results table headers -->
<tr>
<th></th>
<th>Nome Completo<p><i>Full Name</i></p></th>
<th>Nome Científico<p><i>Scientific Name with Initials (e.g. Guimaraes, LP)</i></p></th>
<th>Departmento<p><i>Department</i></p></th>
</tr>";
 
foreach ($questionario->colaboradoresinternos as $i => $value){
	 
		
	echo "<tr>";
	echo "<td>";
	echo "<input type='image' src='../images/icon_delete.png' name='navOption' value='Apagar Colaborador' onclick='if(formSubmited==0){return false;};document.questionario.apagaRegColaboradorInterno.value=".$questionario->colaboradoresinternos[$i]->id.";document.questionario.operacao.value=53;' >";
	echo "</td>";
	echo "<td>";
	echo "<input class='inp-textAuto' type='text' id='cinome".$i."' name='cinome".$i."' value='".$questionario->colaboradoresinternos[$i]->nome."' onkeyup='mostraColNome(this.value,".$i.");'><div class='divlookup' id='livesearchcolnome_".$i."'></div>\n";
	echo "</td>";
	echo "<td>";
	echo "<input class='inp-textAuto' type='text' id='cinomecient".$i."' name='cinomecient".$i."' value='".$questionario->colaboradoresinternos[$i]->nomecient."'>";
	echo "</td>";

	echo "<td>";
	getDepartamentos($i);
	echo "</td>";
		

	echo "</tr>";


}
echo "<tr><td><input input type='image' src='../images/icon_new.png' name='navOption' value='Novo Colaborador Interno' onclick='if(formSubmited==0){return false;};document.questionario.operacao.value=52;'></td></tr>";

echo "</tbody></table>";
echo "<br /><p>";

echo "<input type='hidden' name='apagaRegColaboradorInterno' />";
echo "</fieldset>";


echo "<p><fieldset class='normal'>\n";
			echo "<legend>Colaboradores Externos Atuais/<i>Current External Collaborators</i></legend>\n";

			echo "<p class='ppthelp'>Colaborador é todo/a aquele/a que faz investigação de forma ativa em colaboração consigo, e.g., tendo artigos científicos comuns em preparação. No que segue preencha o(s) nome(s) científico(s) do(s) seu(s) colaborador(es) exatamente no formato indicado, um por linha. Não utilize acentos ou cedilhas! Indique também a Instituição e o País onde cada um trabalha: basta escrever algumas letras do nome para que a Instituição apareça numa lista – selecionar. O respetivo país também aparece. Caso a Instituição seja nova, preencher tudo manualmente.</p>";			

			echo "<p class='penhelp'><i>A collaborator is an individual that does active research in cooperation with you namely by having common scientific papers in preparation. In what follows please fill the scientific name(s) of your external scientific collaborator(s) exactly in the format shown, one per line. Please do not use any accent or other marks on the letters. Please also indicate the Institution and the Country where each collaborator works: it is enough to key a few characters from its name for the Institution to show up in a list – please select it. The respective country also shows up. For a new Institution please fill it all manually.</i></p>";
			
			    echo "
    <table id='colext' class='box-table-b'>
  <tbody>
    <!-- Results table headers -->
    <tr>
    <th></th>
      <th>Nome Científico com Iniciais<p><i>Scientific Name with Initials (e.g. Guimaraes, LP)</i></p></th>
      <th>Instituição<p><i>Institution</i></p></th>
      <th>País<p><i>Country</i></p></th>
    </tr>";
			    
    foreach ($questionario->colaboradores as $i => $value){
    	
			
			echo "<tr>";
			echo "<td>";
				echo "<input type='image' src='../images/icon_delete.png' name='navOption' value='Apagar Colaborador' onclick='if(formSubmited==0){return false;};document.questionario.apagaRegColaborador.value=".$questionario->colaboradores[$i]->id.";document.questionario.operacao.value=16;' >";
			echo "</td>";
			echo "<td>";
			echo "<input class='inp-textAuto' type='text' name='cenomecient".$i."' value='".$questionario->colaboradores[$i]->nomecient."'>";
    		echo "</td>";
    					
			echo "<td>";
			echo "<input class='inp-textAuto' type='text' id='ceinstituicao".$i."' name='ceinstituicao".$i."' value='".$questionario->colaboradores[$i]->instituicao."' onkeyup='mostraColexInstituicao(this.value,".$i.");'><div class='divlookup' id='livesearchcolexinstituicao_".$i."'></div>\n";
			echo "</td>";
			
			echo "<td>";
			//echo "<input class='inp-textAuto' type='text' name='pais".$i."' value='".$questionario->colaboradores[$i]->pais."'></p>\n";
			getPaisesCol($i);
			echo "</td>";
			

	    	echo "</tr>";
	    	
	    	
    }
         echo "<tr><td><input input type='image' src='../images/icon_new.png' name='navOption' value='Novo Colaborador' onclick='if(formSubmited==0){return false;};document.questionario.operacao.value=8;'></td></tr>";
	
    echo "</tbody></table>";
    echo "<br /><p>";

	echo "<input type='hidden' name='apagaRegColaborador' />";
	echo "</fieldset>";
	echo "</div>";
	
	function getPaisesCol($i){
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_paises");
	
		echo "<SELECT id='cepais".$i."' name='cepais".$i."' >\n";
		echo "<option></option>\n";
		
		while ($row = mysql_fetch_assoc($lValues)) {	
			echo "<option value='".$row["ID"]."'".checkPaisCol($row["ID"],$i).">".$row["DESCRICAO"]."</option>\n";
		}
		echo "</SELECT><br />\n";
		$db->disconnect();
				
	}	
				
	function checkPaisCol($id,$i){
		global $questionario;
		if($questionario->colaboradores[$i]->pais==$id)
			return " SELECTED";
		else 
			return "";
	}
	function getDepartamentos($i){
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_servicosfmup");
	
		echo "<SELECT id='cidepartamento".$i."' name='cidepartamento".$i."'>\n";
		echo "<option></option>\n";
	
		while ($row = mysql_fetch_assoc($lValues)) {
			echo "<option value='".$row["ID"]."'".checkDepartamento($row["ID"],$i).">".$row["DESCRICAO"]."</option>\n";
		}
		echo "</SELECT>\n";
		$db->disconnect();
	}
	
	function checkDepartamento($id,$i){
		global $questionario;
		if($questionario->colaboradoresinternos[$i]->departamento==$id)
			return " SELECTED";
		else
			return "";
	}
	
	
	?>