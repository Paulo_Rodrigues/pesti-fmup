<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt" lang="pt">

<head>


<link rel="stylesheet" type="text/css" href="../../styles/style_screen2.css" media="screen" />
<link rel="stylesheet" type="text/css" href="../../styles/style_print2.css" media="print" />


<meta http-equiv="Content-Type" content="text/html; charset=utf-8">


<title>Levantamento Geral de Dados da FMUP</title>

</head>
<?php
session_start();

require_once '../classlib/QuestionarioDepartamento.class.inc';
echo "<body>";


//if(isset($_SESSION['questionario'])){
//	$questionario=unserialize($_SESSION['questionario']);

//echo "SERVIÇO".$_SESSION['departamento'];

if(isset($_SESSION['departamento']))
	$dadosDep=new QuestionarioDepartamento($_SESSION['departamento']);
else
	header("Location: ../../index.php?erro=404");

//echo "<a href='#' onclick='print();'><img src='../images/printer.png' alt=''>IMPRIMIR</a>";
	
	echo "<fieldset class='normal'>";
	echo "<legend>Dados de Submissão</legend>";
	echo "Nº de investigadores registados: ".sizeof($dadosDep->investigadores);
	
	echo "</fieldset>";
			
 			include('../reports/diretordep/investigadoresDEP.php');
		
 			include('../reports/diretordep/habilitacoes.php');
		
 			include('../reports/diretordep/agregacoes.php');
 			
 			include('../reports/diretordep/experienciaProfissional.php');
 			
 			include('../reports/diretordep/direcoesCurso.php');
 			
 			include('../reports/diretordep/colaboradores.php');
 				
 			include('../reports/diretordep/unidadesInvestigacao.php');
 			
			include('../reports/diretordep/publicacoesISI.php');
 			
			include('../reports/diretordep/publicacoesPubmed.php');
 			
			include('../reports/diretordep/publicacoesManualPatentes.php');
 			
			include('../reports/diretordep/publicacoesManualInternacional.php');
 			
			include('../reports/diretordep/publicacoesManualNacional.php');
			
			include('../reports/diretordep/projectos.php');
			
			include('../reports/diretordep/conferencias.php');
			
			include('../reports/diretordep/apresentacoes.php');
			
			include('../reports/diretordep/arbitragensRevistas.php');
			
			include('../reports/diretordep/premios.php');
			
			include('../reports/diretordep/sc.php');
				
			include('../reports/diretordep/redes.php');
			
			include('../reports/diretordep/missoes.php');
			
			include('../reports/diretordep/orientacoes.php');
			
			include('../reports/diretordep/juris.php');
			
			include('../reports/diretordep/outrasactividades.php');
			
			include('../reports/diretordep/acoesdivulgacao.php');
			
			include('../reports/diretordep/produtos.php');
			
			
// 			
		
// 			include('../reports/diretordep/cargosUP.php');
		
// 			include('../reports/diretordep/cargosFMUP.php');
		
// 			include('../reports/diretordep/cargosHospitais.php');
		
// 			include('../reports/diretordep/identificacaoFormal.php');
		
// 			include('../reports/diretordep/colaboradores.php');
		
// 			include('../reports/diretordep/aulas.php');

// 			include('../reports/diretordep/acoesformacao.php');
			
// 			include('../reports/diretordep/observacoesfinais.php');



?>

</body></html>