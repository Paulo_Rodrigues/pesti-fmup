<?php

echo "<fieldset class='normal'>\n";
			echo "<legend>Ações de Formação Frequentadas <a href='../helpfiles/Ajuda_Acoes_Freq.JPG' style='color: #FFFF00' target='_blank'>Ajuda</a> /<i>Training taken <a href='../helpfiles/Ajuda_Acoes_Freq.JPG' style='color: #FFFF00' target='_blank'>Help</a></i></legend>\n";


echo "<p class='ppthelp'>Para cada ação de formação frequentada indicar o título, o nº total de horas. Ainda, o período em que decorreu, o local e o seu objetivo.</p>";			

echo "<p class='penhelp'><i>For each training session attended please give the title, the total number of hours. Finally, also indicate the dates, the place and its objective.</i></p>";
	

			
echo "
<table id='acff' class='box-table-b'>
<thead>
	<tr>
      <th data-sorter='text'></th>
	  <th><u>Tipo<p><i>Type</i></p></u></th>
      <th><u>Título<p><i>Title</i></p></u></th>
      <th><u>Nº Horas<p><i>Nr. Hours</i></p></u></th>
      <th><u>Data Início<p><i>Begin Date</i></p></u></th>
      <th><u>Data Fim<p><i>End Date</i></p></u></th>
      <th><u>Cidade, País<p><i>City, Country</i></p></u></th>
      <th><u>Objetivo<p><i>Objective</i></p></u></th>
    </tr>
	</thead>       
<tbody>    
";    
    foreach ($questionario->acoesformacaofrequentada as $i => $value){
    	
			
			echo "<tr>";
			
		   	echo "<td>";
			echo "<input type='image' src='../images/icon_delete_s.png' name='navOption' value='Apagar Acao' onclick='if(formSubmited==0){return false;};document.questionario.apagaRegAcaoFormacaoFrequentada.value=".$questionario->acoesformacaofrequentada[$i]->id.";document.questionario.operacao.value=87;' >";
			echo "</td>";

			echo "<td>";
			//echo "<input class='inp-textAuto' type='text' name='_formandos_".$i."' value='".$questionario->acoesformacao[$i]->formandos."'>".getTipoFormando($i);
			getTipoAcao($i);
			echo "</td>";
			
			echo "<td>";
			echo "<input class='inp-textAuto' type='text' name='titulo_".$i."' value='".$questionario->acoesformacaofrequentada[$i]->titulo."'>";			
			echo "</td>";
    					
			echo "<td>";
			echo "<input class='inp-textAuto' type='text' name='horas_".$i."' maxlength='4' size='4' onkeypress='validate(event)' value='".$questionario->acoesformacaofrequentada[$i]->horas."'>";			
			echo "</td>";
			
			echo "<td>";
			echo "<input class='inp-textAuto' type='text' id='diaff_".$i."' maxlength='11' size='11' name='affdataini_".$i."' onfocus='calendario(\"diaff_".$i."\");' onkeypress='validateCal(event);' value='".$questionario->acoesformacaofrequentada[$i]->dataini."'>";			
			echo "</td>";			

			echo "<td>";
			echo "<input class='inp-textAuto' type='text' id='dfaff_".$i."' maxlength='11' size='11' name='affdatafim_".$i."' onfocus='calendario(\"dfaff_".$i."\");' onkeypress='validateCal(event);' value='".$questionario->acoesformacaofrequentada[$i]->datafim."'>";			
			echo "</td>";	
			
			echo "<td>";
			echo "<input class='inp-textAuto' type='text' name='local_".$i."' value='".$questionario->acoesformacaofrequentada[$i]->local."'>";			
			echo "</td>";

			echo "<td>";
			echo "<input class='inp-textAuto' type='text' name='objectivo_".$i."' value='".$questionario->acoesformacaofrequentada[$i]->objectivo."'>";			
			echo "</td>";
			
			echo "</tr>";

    }
   	echo "</tbody><tfoot><tr><td><input input type='image' src='../images/icon_new_s.png' name='navOption' value='Nova Acão Frequentada' onclick='if(formSubmited==0){return false;};document.questionario.operacao.value=88;'></td></tr></tfoot>";
	
    echo "</table>";

	echo "<input type='hidden' name='apagaRegAcaoFormacaoFrequentada' />";
	echo "</fieldset>";
	
	
	
	function getTipoAcao($i) {
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_tipoacaoformacaofrequentada");
	
		
		echo "<select name='tipo_".$i."' id='tipo_".$i."'>\n";
		echo "<option value=''></option>";
		while ($row = mysql_fetch_assoc($lValues)) {
			echo "<option value='".$row["ID"]."'".checkTipoAcao($row["ID"],$i).">".$row["DESCRICAO"]."</option>";
		}
		echo "</select>";
		$db->disconnect();
	
	}
	
	function checkTipoAcao($id,$i){
		global $questionario;
		if($questionario->acoesformacaofrequentada[$i]->tipo==$id)
			return " selected='selected'";
		else
			return "";
	}
	
	
	
?>