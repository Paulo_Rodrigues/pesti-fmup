<?php
// PASSO 2 PMARQUES
require_once '../../classlib/Database.class.inc';

echo "<link rel=\"stylesheet\" href=\"../../js/jQuery/themes/base/jquery.ui.all.css\">";

echo "<script>

 $(function() {
  
	$( \"#datanasc-inv\" ).datepicker({
      changeMonth: true,
      changeYear: true,
	  yearRange: '1950:2020',
	  dateFormat: 'dd-mm-yy'
    },
	$.datepicker.regional['pt']);
	
	$( \"#dataini-projectos\" ).datepicker({
      changeMonth: true,
      changeYear: true,
	  yearRange: '1950:2020',
	  dateFormat: 'dd-mm-yy'
    },
	$.datepicker.regional['pt']);
	
	$( \"#datafim-projectos\" ).datepicker({
      changeMonth: true,
      changeYear: true,
	  yearRange: '1950:2020',
	  dateFormat: 'dd-mm-yy'
    },
	$.datepicker.regional['pt']);
	
	$( \"#data-pubManualPatentes\" ).datepicker({
      changeMonth: true,
      changeYear: true,
	  yearRange: '1950:2020',
	  dateFormat: 'dd-mm-yy'
    },
	$.datepicker.regional['pt']);	
	
	$( \"#dataini-dircurso\" ).datepicker({
      changeMonth: true,
      changeYear: true,
	  yearRange: '1950:2020',
	  dateFormat: 'dd-mm-yy'
    },
	$.datepicker.regional['pt']);
	
	$( \"#datafim-dircurso\" ).datepicker({
      changeMonth: true,
      changeYear: true,
	  yearRange: '1950:2020',
	  dateFormat: 'dd-mm-yy'
    },
	$.datepicker.regional['pt']);	
		
	$( \"#dataini-conferencias\" ).datepicker({
      changeMonth: true,
      changeYear: true,
	  yearRange: '1950:2020',
	  dateFormat: 'dd-mm-yy'
    },
	$.datepicker.regional['pt']);	
	
	$( \"#datafim-conferencias\" ).datepicker({
      changeMonth: true,
      changeYear: true,
	  yearRange: '1950:2020',
	  dateFormat: 'dd-mm-yy'
    },
	$.datepicker.regional['pt']);
	
	$( \"#datainicio-sc\" ).datepicker({
      changeMonth: true,
      changeYear: true,
	  yearRange: '1950:2020',
	  dateFormat: 'dd-mm-yy'
    },
	$.datepicker.regional['pt']);
	
	$( \"#datafim-sc\" ).datepicker({
      changeMonth: true,
      changeYear: true,
	  yearRange: '1950:2020',
	  dateFormat: 'dd-mm-yy'
    },
	$.datepicker.regional['pt']);	
	
	$( \"#datainicio-missoes\" ).datepicker({
      changeMonth: true,
      changeYear: true,
	  yearRange: '1950:2020',
	  dateFormat: 'dd-mm-yy'
    },
	$.datepicker.regional['pt']);	
	
	$( \"#datafim-missoes\" ).datepicker({
      changeMonth: true,
      changeYear: true,
	  yearRange: '1950:2020',
	  dateFormat: 'dd-mm-yy'
    },
	$.datepicker.regional['pt']);
	
	$( \"#data-acaodivulgacao\" ).datepicker({
      changeMonth: true,
      changeYear: true,
	  yearRange: '1950:2020',
	  dateFormat: 'dd-mm-yy'
    },
	$.datepicker.regional['pt']);	
	
	$( \"#datanasc-ins-inv\" ).datepicker({
      changeMonth: true,
      changeYear: true,
	  yearRange: '1950:2020',
	  dateFormat: 'dd-mm-yy'
    },
	$.datepicker.regional['pt']);
	
	$( \"#dataini-ins-dircurso\" ).datepicker({
      changeMonth: true,
      changeYear: true,
	  yearRange: '1950:2020',
	  dateFormat: 'dd-mm-yy'
    },
	$.datepicker.regional['pt']);
	
	$( \"#datafim-ins-dircurso\" ).datepicker({
      changeMonth: true,
      changeYear: true,
	  yearRange: '1950:2020',
	  dateFormat: 'dd-mm-yy'
    },
	$.datepicker.regional['pt']);	
		
	$( \"#datafim-ins-projectos\" ).datepicker({
      changeMonth: true,
      changeYear: true,
	  yearRange: '1950:2020',
	  dateFormat: 'dd-mm-yy'
    },
	$.datepicker.regional['pt']);	
	
	$( \"#dataini-ins-projectos\" ).datepicker({
      changeMonth: true,
      changeYear: true,
	  yearRange: '1950:2020',
	  dateFormat: 'dd-mm-yy'
    },
	$.datepicker.regional['pt']);
	
	$( \"#dataini-ins-conferencias\" ).datepicker({
      changeMonth: true,
      changeYear: true,
	  yearRange: '1950:2020',
	  dateFormat: 'dd-mm-yy'
    },
	$.datepicker.regional['pt']);	
	
	$( \"#datafim-ins-conferencias\" ).datepicker({
      changeMonth: true,
      changeYear: true,
	  yearRange: '1950:2020',
	  dateFormat: 'dd-mm-yy'
    },
	$.datepicker.regional['pt']);		
	
	$( \"#datafim-ins-sc\" ).datepicker({
      changeMonth: true,
      changeYear: true,
	  yearRange: '1950:2020',
	  dateFormat: 'dd-mm-yy'
    },
	$.datepicker.regional['pt']);	
	
	$( \"#datainicio-ins-sc\" ).datepicker({
      changeMonth: true,
      changeYear: true,
	  yearRange: '1950:2020',
	  dateFormat: 'dd-mm-yy'
    },
	$.datepicker.regional['pt']);	
	
	$( \"#datainicio-ins-redes\" ).datepicker({
      changeMonth: true,
      changeYear: true,
	  yearRange: '1950:2020',
	  dateFormat: 'dd-mm-yy'
    },
	$.datepicker.regional['pt']);	
	
	$( \"#datafim-ins-redes\" ).datepicker({
      changeMonth: true,
      changeYear: true,
	  yearRange: '1950:2020',
	  dateFormat: 'dd-mm-yy'
    },
	$.datepicker.regional['pt']);	
	
	$( \"#datainicio-ins-missoes\" ).datepicker({
      changeMonth: true,
      changeYear: true,
	  yearRange: '1950:2020',
	  dateFormat: 'dd-mm-yy'
    },
	$.datepicker.regional['pt']);	
	
	$( \"#datafim-ins-missoes\" ).datepicker({
      changeMonth: true,
      changeYear: true,
	  yearRange: '1950:2020',
	  dateFormat: 'dd-mm-yy'
    },
	$.datepicker.regional['pt']);
	
	$( \"#data-ins-acaodivulgacao\" ).datepicker({
      changeMonth: true,
      changeYear: true,
	  yearRange: '1950:2020',
	  dateFormat: 'dd-mm-yy'
    },
	$.datepicker.regional['pt']);	
  });
</script>";
		
echo "<div id=\"dialog-confirm-inv\" title=\"Apagar Investigador\" hidden>
	<p><span class=\"ui-icon ui-icon-alert\" style=\"float:left; margin:0 7px 0px 0;\"></span>Está prestes a eliminar um investigador. Tem a certeza que pretende eliminar? Esta ação é destrutiva e definitiva.</p>
</div>";


//Inserir Investigador
	echo "<div id=\"dialog-insert-investigador\" title=\"Inserir Novo Investigador\" hidden>
			  <p class=\"validateTips\"></p>
			  <form>
				  <fieldset id = \"edit\">
				    <label for=\"nome-ins-inv\">Nome</label>
				    <input type=\"text\" name=\"nome-ins-inv\" id=\"nome-ins-inv\" class=\"text ui-widget-content ui-corner-all\" />
				    <label for=\"nummec-ins-inv\">Número Mecanográfico</label>
				    <input type=\"text\" name=\"nummec-ins-inv\" id=\"nummec-ins-inv\" value=\"\" class=\"text ui-widget-content ui-corner-all\" />
				    <label for=\"datanasc-ins-inv\">Data de Nascimento</label>
				    <input type=\"text\" name=\"datanasc-ins-inv\" id=\"datanasc-ins-inv\" value=\"\" class=\"text ui-widget-content ui-corner-all\" />
				   </fieldset>
			  </form>
		</div>";


		
	//Inserir Agregacao
	echo "<div id=\"dialog-insert-agregacao\" title=\"Inserir Agregação\" hidden>
			  <form>
				  <fieldset id = \"edit\">
				  <label for=\"unanimidade-ins-agr\">Unanimidade</label>
				    <SELECT id='unanimidade-ins-agr' name='unanimidade-ins-agr' >\n
						<option>Sim</option>\n	
						<option>Não</option>\n		
					</SELECT><br />\n	
			</fieldset>
			  </form>
		</div>";
				
	//Inserir Caracterizacao Docentes
	echo "<div id=\"dialog-insert-caracDoc\" title=\"Inserir Caracterização Docentes\" hidden>
			<form>
			   <fieldset id = \"edit\">
			   <label for=\"tipo-ins-doc\">Tipo</label>
				    <select name='tipo-ins-doc' id='tipo-ins-doc'>";
					getCatEdit();
			  echo "<br><br><label for=\"perc-ins-doc\">Percentagem</label>
		  	  		<select id=\"perc-ins-doc\" name=\"perc-ins-doc\">";
		  	  		getPercentagemEdit();
		  echo"</fieldset>
		  </form>
		</div>";
	
	//Inserir Caracterizacao Pos-Doc
	echo "<div id=\"dialog-insert-caracPosDoc\" title=\"Inserir Caracterização Pós-Doc\" hidden>
			<form>
			    <fieldset id = \"edit\">
					<label for=\"bolsa-ins-pdoc\">Tipo Bolsa</label>
				    <select name='bolsa-ins-pdoc' id='bolsa-ins-pdoc'>\n";			  
					getBolsaEdit();	  
		  	  echo "<br><label for=\"per-ins-pdoc\">Percentagem</label>
		  	  		<select id=\"per-ins-pdoc\" name=\"per-ins-pdoc\">";
		  	  		getPercentagemEdit();
			echo"</fieldset>
			</form>
		</div>";
		
	//Inserir Direção Curso		
	echo "<div id=\"dialog-insert-dirCurso\" title=\"Inserir Direção Curso\" hidden>	
				<p class=\"validateTips-dirCurso\"></p>
				<form>
			    <fieldset id = \"edit\">				    
					<label for=\"dataini-ins-dircurso\">Data de Início</label>
				    <input type=\"text\" name=\"dataini-ins-dircurso\" id=\"dataini-ins-dircurso\" class=\"text ui-widget-content ui-corner-all\" />				 	  
		  	  		<label for=\"datafim-ins-dircurso\">Data de Fim</label>
					<input type=\"text\" name=\"datafim-ins-dircurso\" id=\"datafim-ins-dircurso\" class=\"text ui-widget-content ui-corner-all\" />
		  	  		<label for=\"datafim-ins-dircurso\">Curso</label>
		  	  		<input type=\"text\" name=\"curso-ins-dircurso\" id=\"curso-ins-dircurso\" class=\"text ui-widget-content ui-corner-all\" />
		  	  		<label for=\"grau-ins-dircurso\">Grau</label>";
		  	  		echo "<SELECT id='grau-ins-dircurso' name='grau-ins-dircurso' >\n";
		  	  		getGrauEdit();
		  echo "</fieldset>
		  </form>
		</div>";
		
	//Inserir Colaboradores Internos FMUP
	echo "<div id=\"dialog-insert-colIntAct\" title=\"Inserir Colaboradores FMUP Atuais\" hidden>
			<p class=\"validateTips-colIntAct\"></p>
				<form>
			    <fieldset id = \"edit\">
					<label for=\"nome-ins-colIntAct\">Nome</label>
				    <input type=\"text\" name=\"nome-ins-colIntAct\" id=\"nome-ins-colIntAct\" class=\"text ui-widget-content ui-corner-all\" />	
					<label for=\"nomecient-ins-colIntAct\">Nome Científico</label>
				    <input type=\"text\" name=\"nomecient-ins-colIntAct\" id=\"nomecient-ins-colIntAct\" class=\"text ui-widget-content ui-corner-all\" />				 	  
		  	  		<label for=\"dep-ins-colIntAct\">Departamento</label>
		  	  		<select name='dep-ins-colIntAct' id='dep-ins-colIntAct'>";
					getDepartamentosEdit();	  	  		
		  	echo"</fieldset>
		  		</form>
		 </div>";
	
	//Inserir Colaboradores Externos FMUP
	echo "<div id=\"dialog-insert-colExtAct\" title=\"Editar Dados Colaboradores Externos FMUP\" hidden>
			<p class=\"validateTips-colExtAct\"></p>
				<form>
			    <fieldset id = \"edit\">
					<label for=\"nomecient-ins-colExtAct\">Nome Científico</label>
				    <input type=\"text\" name=\"nomecient-ins-colExtAct\" id=\"nomecient-ins-colExtAct\" class=\"text ui-widget-content ui-corner-all\" />				 	  
		  	  		<label for=\"inst-ins-colExtAct\">Instituição</label>
					<SELECT name='inst-ins-colExtAct' id='inst-ins-colExtAct'>\n";
					getInstEdit();
		echo "<br><label for=\"pais-ins-colExtAct\">Pais</label>
				   	<select name='pais-ins-colExtAct' id='pais-ins-colExtAct'>";
					getPaisesEdit();	  	  		
		  	echo"</fieldset>
		  		</form>
		 </div>";
	
	//Inserir Unidades Investigacao
	echo "<div id=\"dialog-insert-uniInv\" title=\"Editar Dados Unidades Investigação\" hidden>
			<p class=\"validateTips-uniInv\"></p>
				<form>
			    <fieldset id = \"edit\">
					<label for=\"uni-ins-uniInv\">Unidade</label>
					<SELECT name='uni-ins-uniInv' id='uni-ins-uniInv' onchange='$(\"#ava-ins-uniInv\").val(value);'>\n";
				    getUniInvEdit();
		  echo "<br><br><label for=\"ava-ins-uniInv\">Avaliação 2007</label>		
					<input type=\"text\" name=\"ava-ins-uniInv\" id=\"ava-ins-uniInv\" class=\"text ui-widget-content ui-corner-all\" />	
					<label for=\"perc-ins-uniInv\">Percentagem</label>				    
				    <select id=\"perc-ins-uniInv\" name=\"perc-ins-uniInv\">";
		  	  		getPercentagemEdit();
			echo "	<br /><br /><label for=\"resp-ins-uniInv\">Responsável</label>
				    <SELECT id='resp-ins-uniInv' name='resp-ins-uniInv' >\n
						<option value='1'>Sim</option>\n	
						<option value='0'>Não</option>\n		
					</SELECT>\n	
					</fieldset>
		  		</form>
		 </div>";
		 
	//Inserir Projectos
    echo "<div id=\"dialog-insert-projectos\" title=\"Inserir Projectos\" hidden>
                <p class=\"validateTips-projectos\"></p>
                    <form>
                    <fieldset id = \"edit\">
						<label for=\"tipo-ins-projectos\">Tipo/Entidade</label>                        
                       	<SELECT id='tipo-ins-projectos' name='tipo-ins-projectos'>\n";
                       	getEntidadeFinProjEdit();

                  echo "<br><br><label for=\"entidade-ins-projectos\">Entidade Financiadora</label>
                        <input type=\"text\" name=\"entidade-ins-projectos\" id=\"entidade-ins-projectos\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"inst-ins-projectos\">Instituição de Acolhimento</label>
                        <input type=\"text\" name=\"inst-ins-projectos\" id=\"inst-ins-projectos\" class=\"text ui-widget-content ui-corner-all\" />
                       
                        <label for=\"monsol-ins-projectos\">Montante Total Solicitado (Em euros)</label>
                        <input type=\"text\" name=\"monsol-ins-projectos\" id=\"monsol-ins-projectos\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"monapr-ins-projectos\">Montante Total Aprovado (Em euros)</label>
                        <input type=\"text\" name=\"monapr-ins-projectos\" id=\"monapr-ins-projectos\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"monfmup-ins-projectos\">Montante atribuído à FMUP (Em euros)</label>
                        <input type=\"text\" name=\"monfmup-ins-projectos\" id=\"monfmup-ins-projectos\" class=\"text ui-widget-content ui-corner-all\" />

	                    <label for=\"resp-ins-projectos\">É Investigador Responsável?</label>
					    <SELECT id='resp-ins-projectos' name='resp-ins-projectos' >\n
							<option value='1'>Sim</option>\n	
							<option value='0'>Não</option>\n		
						</SELECT>\n	

                        <br><br><label for=\"codigo-ins-projectos\">Código/Referência</label>
                        <input type=\"text\" name=\"codigo-ins-projectos\" id=\"codigo-ins-projectos\" class=\"text ui-widget-content ui-corner-all\" />

                         <label for=\"titulo-ins-projectos\">Título</label>
                        <input type=\"text\" name=\"titulo-ins-projectos\" id=\"titulo-ins-projectos\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"dataini-ins-projectos\">Data de Início</label>
                        <input type=\"text\" name=\"dataini-ins-projectos\" id=\"dataini-ins-projectos\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"datafim-ins-projectos\">Data de Fim</label>
                        <input type=\"text\" name=\"datafim-ins-projectos\" id=\"datafim-ins-projectos\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"link-ins-projectos\">Link</label>
                        <textarea name='link-ins-projectos' id='link-ins-projectos' style='height: 75px; width: 500px'></textarea></td>

                        <label for=\"estado-ins-projectos\">Estado</label>
                        <SELECT id='estado-ins-projectos' name='estado-ins-projectos'>\n";
                        getEstadoProjEdit();
            echo "</fieldset>
           	     </form>
         </div>";  

	//Inserir Conferencias
    echo "<div id=\"dialog-insert-conferencia\" title=\"Inserir Conferências\" hidden>
                <p class=\"validateTips-conferencias\"></p>
                    <form>
                    <fieldset id = \"edit\">
						<label for=\"ambito-ins-conferencias\">Âmbito</label>                        
                        <SELECT id='ambito-ins-conferencias' name='ambito-ins-conferencias'>\n";
                        getAmbitoConfEdit();

           echo"<br><br><label for=\"dataini-ins-conferencias\">Data de Início</label>
                        <input type=\"text\" name=\"dataini-ins-conferencias\" id=\"dataini-ins-conferencias\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"datafim-ins-conferencias\">Data de Fim</label>
                        <input type=\"text\" name=\"datafim-ins-conferencias\" id=\"datafim-ins-conferencias\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"titulo-ins-conferencias\">Título</label>
                        <input type=\"text\" name=\"titulo-ins-conferencias\" id=\"titulo-ins-conferencias\" class=\"text ui-widget-content ui-corner-all\" />

                      	<label for=\"local-ins-conferencias\">Local (cidade,pais)</label>
                        <input type=\"text\" name=\"local-ins-conferencias\" id=\"local-ins-conferencias\" class=\"text ui-widget-content ui-corner-all\" />                      	

                        <label for=\"npart-ins-conferencias\">Nº Participantes</label>
                        <input type=\"text\" name=\"npart-ins-conferencias\" id=\"npart-ins-conferencias\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"link-ins-conferencias\">Link</label>
                        <textarea name='link-ins-conferencias' id='link-ins-conferencias' style='height: 75px; width: 500px'></textarea></td>
				</fieldset>
           	    </form>
         </div>";  
		 
	//Inserir Apresentacoes
    echo "<div id=\"dialog-insert-apresentacao\" title=\"Inserir Apresentação\" hidden>
                <p class=\"validateTips-apresentacoes\"></p>
                    <form>
                    <fieldset id = \"edit\">
						<p><b>Internacionais</b></p><br>
                        <label for=\"iconfconv-ins-apresentacoes\">Apresentações por convite</label>
                        <input type=\"text\" name=\"iconfconv-ins-apresentacoes\" id=\"iconfconv-ins-apresentacoes\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"iconfpart-ins-apresentacoes\">Apresentações como participante</label>
                        <input type=\"text\" name=\"iconfpart-ins-apresentacoes\" id=\"iconfpart-ins-apresentacoes\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"isem-ins-apresentacoes\">Seminários Apresentados</label>
                        <input type=\"text\" name=\"isem-ins-apresentacoes\" id=\"isem-ins-apresentacoes\" class=\"text ui-widget-content ui-corner-all\" />

                        <br><div class='ui-dialog-buttonpane ui-widget-content ui-helper-clearfix'/><br>

                        <p><b>Nacionais</b></p><br>
                      	<label for=\"nconfconv-ins-apresentacoes\">Apresentações por convite</label>
                        <input type=\"text\" name=\"nconfconv-ins-apresentacoes\" id=\"nconfconv-ins-apresentacoes\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"nconfpart-ins-apresentacoes\">Apresentações como participante</label>
                        <input type=\"text\" name=\"nconfpart-ins-apresentacoes\" id=\"nconfpart-ins-apresentacoes\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"nsem-ins-apresentacoes\">Seminários Apresentados</label>
                        <input type=\"text\" name=\"nsem-ins-apresentacoes\" id=\"nsem-ins-apresentacoes\" class=\"text ui-widget-content ui-corner-all\" />
				</fieldset>
           	    </form>
         </div>"; 

	//Inserir Arbitragens Revistas
    echo "<div id=\"dialog-insert-arbRevista\" title=\"Inserir Edição/Arbitragem de Revista\" hidden>
                <p class=\"validateTips-arbrevistas\"></p>
                    <form>
                    <fieldset id = \"edit\">
						<label for=\"tipo-ins-arbrevistas\">Tipo</label>                        
                        <SELECT id='tipo-ins-arbrevistas' name='tipo-ins-arbrevistas'>\n";
                        getTipoArbEdit();

           echo"<br><br><label for=\"issn-ins-arbrevistas\">ISSN</label>
                        <input type=\"text\" name=\"issn-ins-arbrevistas\" id=\"issn-ins-arbrevistas\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"titulo-ins-arbrevistas\">Revista</label>
                        <input type=\"text\" name=\"titulo-ins-arbrevistas\" id=\"titulo-ins-arbrevistas\" class=\"text ui-widget-content ui-corner-all\" />                       

                        <label for=\"link-ins-arbrevistas\">Link</label>
                        <textarea name='link-ins-arbrevistas' id='link-ins-arbrevistas' style='height: 75px; width: 500px'></textarea></td>
				</fieldset>
           	    </form>
         </div>";
	
	//Inserir Premio	
	 echo "<div id=\"dialog-insert-premio\" title=\"Inserir Prémio\" hidden>
                <p class=\"validateTips-premios\"></p>
                    <form>
                    <fieldset id = \"edit\">
						<label for=\"tipo-ins-premios\">Tipo</label>                        
                        <SELECT id='tipo-ins-premios' name='tipo-ins-premios'>\n";
                        getTipoPremioEdit();

           echo"<br><br><label for=\"nome-ins-premios\">Nome</label>
                        <input type=\"text\" name=\"nome-ins-premios\" id=\"nome-ins-premios\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"contexto-ins-premios\">Contexto</label>
                        <input type=\"text\" name=\"contexto-ins-premios\" id=\"contexto-ins-premios\" class=\"text ui-widget-content ui-corner-all\" />     

                        <label for=\"montante-ins-premios\">Montante (em euros)</label>
                        <input type=\"text\" name=\"montante-ins-premios\" id=\"montante-ins-premios\" class=\"text ui-widget-content ui-corner-all\" />                        

                        <label for=\"link-ins-premios\">Link</label>
                        <textarea name='link-ins-premios' id='link-ins-premios' style='height: 75px; width: 500px'></textarea></td>
				</fieldset>
           	    </form>
         </div>";
	
	//Inserir Sociedades Cientificas
    echo "<div id=\"dialog-insert-socCientifica\" title=\"Inserir Organização/Sociedades Científica\" hidden>
                <p class=\"validateTips-sc\"></p>
                    <form>
                    <fieldset id = \"edit\"> 
						<label for=\"nome-ins-sc\">Nome</label>
                        <input type=\"text\" name=\"nome-ins-sc\" id=\"nome-ins-sc\" class=\"text ui-widget-content ui-corner-all\" />  

                        <label for=\"tipo-ins-sc\">Tipo</label>                        
                        <SELECT id='tipo-ins-sc' name='tipo-ins-sc'>\n";
                        getTipoSCEdit();

           echo"<br><br><label for=\"datainicio-ins-sc\">Data Início</label>
                        <input type=\"text\" name=\"datainicio-ins-sc\" id=\"datainicio-ins-sc\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"datafim-ins-sc\">Data Fim</label>
                        <input type=\"text\" name=\"datafim-ins-sc\" id=\"datafim-ins-sc\" class=\"text ui-widget-content ui-corner-all\" />     

                        <label for=\"cargo-ins-sc\">Cargo</label>
                        <input type=\"text\" name=\"cargo-ins-sc\" id=\"cargo-ins-sc\" class=\"text ui-widget-content ui-corner-all\" />                        

                        <label for=\"link-ins-sc\">Link</label>
                        <textarea name='link-ins-sc' id='link-ins-sc' style='height: 75px; width: 500px'></textarea></td>
				</fieldset>
           	    </form>
         </div>";
		 
	 //Inserir Redes
    echo "<div id=\"dialog-insert-rede\" title=\"Inserir Rede\" hidden>
                <p class=\"validateTips-redes\"></p>
                    <form>
                    <fieldset id = \"edit\">   
						<label for=\"nome-ins-redes\">Nome</label>
                        <input type=\"text\" name=\"nome-ins-redes\" id=\"nome-ins-redes\" class=\"text ui-widget-content ui-corner-all\" />  

                        <label for=\"tipo-ins-redes\">Tipo</label>                        
                        <SELECT id='tipo-ins-redes' name='tipo-ins-redes'>\n";
                        getTipoRedesEdit();

           echo"<br><br><label for=\"datainicio-ins-redes\">Data Início</label>
                        <input type=\"text\" name=\"datainicio-ins-redes\" id=\"datainicio-ins-redes\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"datafim-ins-redes\">Data Fim</label>
                        <input type=\"text\" name=\"datafim-ins-redes\" id=\"datafim-ins-redes\" class=\"text ui-widget-content ui-corner-all\" />                       

                        <label for=\"link-ins-redes\">Link</label>
                        <textarea name='link-ins-redes' id='link-ins-redes' style='height: 75px; width: 500px'></textarea></td>
				</fieldset>
           	    </form>
         </div>";
		 
	  //Inserir Missões
    echo "<div id=\"dialog-insert-missao\" title=\"Inserir Missão\" hidden>
                <p class=\"validateTips-missoes\"></p>
                    <form>
                    <fieldset id = \"edit\">  
						<label for=\"motivacao-ins-missoes\">Motivação</label>
                        <input type=\"text\" name=\"motivacao-ins-missoes\" id=\"motivacao-ins-missoes\" class=\"text ui-widget-content ui-corner-all\" />
						
                    	<label for=\"datainicio-ins-missoes\">Data Início</label>
                        <input type=\"text\" name=\"datainicio-ins-missoes\" id=\"datainicio-ins-missoes\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"datafim-ins-missoes\">Data Fim</label>
                        <input type=\"text\" name=\"datafim-ins-missoes\" id=\"datafim-ins-missoes\" class=\"text ui-widget-content ui-corner-all\" />
                          
                        <label for=\"instituicao-ins-missoes\">Instituto de Acolhimento</label>
						<SELECT name='instituicao-ins-missoes' id='instituicao-ins-missoes'>\n";
                        getInstEdit();
						
				echo"<br><label for=\"pais-ins-missoes\">País</label>                        
                        <SELECT id='pais-ins-missoes' name='pais-ins-missoes'>\n";
                        getPaisesEdit();

           echo"<br><br><label for=\"ambtese-ins-missoes\">Âmbito de Tese</label>
					    <SELECT id='ambtese-ins-missoes' name='ambtese-ins-missoes' >\n
							<option>Sim</option>\n	
							<option>Não</option>\n		
						</SELECT>\n	
				</fieldset>
           	    </form>
         </div>";   
		 
	   //Inserir Orientações
    echo "<div id=\"dialog-insert-orientacao\" title=\"Inserir Orientação\" hidden>
                <p class=\"validateTips-orientacoes\"></p>
                    <form>
                    <fieldset id = \"edit\">  
						<label for=\"tipo-ins-orientacoes\">Tipo de Orientação</label>
                    	<select id='tipo-ins-orientacoes' name='tipo-ins-orientacoes'>";
                    	getTipoOrientEdit();

       	  echo "<br><br><label for=\"tipoori-ins-orientacoes\">Tipo de Orientador</label>
                        <select id='tipoori-ins-orientacoes' name='tipoori-ins-orientacoes'>";
                        getTipoOriOrientEdit();

          echo "<br><br><label for=\"estado-ins-orientacoes\">Estado</label>
                        <select id='estado-ins-orientacoes' name='estado-ins-orientacoes'> ";
                        getEstadosOrientEdit();

    	  echo "<br><br><label for=\"titulo-ins-orientacoes\">Título da Tese em Português</label>
                        <input type=\"text\" name=\"titulo-ins-orientacoes\" id=\"titulo-ins-orientacoes\" class=\"text ui-widget-content ui-corner-all\" />                          
				</fieldset>
           	    </form>
         </div>"; 
		 
   //Editar Juris
    echo "<div id=\"dialog-insert-juri\" title=\"Editar Júris\" hidden>
                <p class=\"validateTips-juris\"></p>
                    <form>
                    <fieldset id = \"edit\"> 
						<p><u><b>Agregação</b></u></p><br>
                    	<i><u>FMUP</u></i><br><br>
                    	<label for=\"agrargfmup-ins-juris\">Arguente</label>
                        <input type=\"text\" name=\"agrargfmup-ins-juris\" id=\"agrargfmup-ins-juris\" class=\"text ui-widget-content ui-corner-all\" /> 

                        <label for=\"agrvogalfmup-ins-juris\">Outro</label>
                        <input type=\"text\" name=\"agrvogalfmup-ins-juris\" id=\"agrvogalfmup-ins-juris\" class=\"text ui-widget-content ui-corner-all\" />  

                        <i><u>Exterior</u></i><br><br>
                    	<label for=\"agrargext-ins-juris\">Arguente</label>
                        <input type=\"text\" name=\"agrargext-ins-juris\" id=\"agrargext-ins-juris\" class=\"text ui-widget-content ui-corner-all\" /> 

                        <label for=\"agrvogalext-ins-juris\">Outro</label>
                        <input type=\"text\" name=\"agrvogalext-ins-juris\" id=\"agrvogalext-ins-juris\" class=\"text ui-widget-content ui-corner-all\" />  

                        <br><div class='ui-dialog-buttonpane ui-widget-content ui-helper-clearfix'/><br>   

                        <p><u><b>Doutoramento</b></u></p><br>
                    	<i><u>FMUP</u></i><br><br>
                    	<label for=\"doutargfmup-ins-juris\">Arguente</label>
                        <input type=\"text\" name=\"doutargfmup-ins-juris\" id=\"doutargfmup-ins-juris\" class=\"text ui-widget-content ui-corner-all\" /> 

                        <label for=\"doutvogalfmup-ins-juris\">Outro</label>
                        <input type=\"text\" name=\"doutvogalfmup-ins-juris\" id=\"doutvogalfmup-ins-juris\" class=\"text ui-widget-content ui-corner-all\" />  

                        <i><u>Exterior</u></i><br><br>
                    	<label for=\"doutargext-ins-juris\">Arguente</label>
                        <input type=\"text\" name=\"doutargext-ins-juris\" id=\"doutargext-ins-juris\" class=\"text ui-widget-content ui-corner-all\" /> 

                        <label for=\"doutvogalext-ins-juris\">Outro</label>
                        <input type=\"text\" name=\"doutvogalext-ins-juris\" id=\"doutvogalext-ins-juris\" class=\"text ui-widget-content ui-corner-all\" />  

                        <br><div class='ui-dialog-buttonpane ui-widget-content ui-helper-clearfix'/><br>    

                        <p><u><b>Mestrado Científico</b></u></p><br>
                    	<i><u>FMUP</u></i><br><br>
                    	<label for=\"mesargfmup-ins-juris\">Arguente</label>
                        <input type=\"text\" name=\"mesargfmup-ins-juris\" id=\"mesargfmup-ins-juris\" class=\"text ui-widget-content ui-corner-all\" /> 

                        <label for=\"mesvogalfmup-ins-juris\">Outro</label>
                        <input type=\"text\" name=\"mesvogalfmup-ins-juris\" id=\"mesvogalfmup-ins-juris\" class=\"text ui-widget-content ui-corner-all\" />  

                        <i><u>Exterior</u></i><br><br>
                    	<label for=\"mesargext-ins-juris\">Arguente</label>
                        <input type=\"text\" name=\"mesargext-ins-juris\" id=\"mesargext-ins-juris\" class=\"text ui-widget-content ui-corner-all\" /> 

                        <label for=\"mesvogalext-ins-juris\">Outro</label>
                        <input type=\"text\" name=\"mesvogalext-ins-juris\" id=\"mesvogalext-ins-juris\" class=\"text ui-widget-content ui-corner-all\" />  

                        <br><div class='ui-dialog-buttonpane ui-widget-content ui-helper-clearfix'/><br>      

                        <p><u><b>Mestrado Integrado</b></u></p><br>
                    	<i><u>FMUP</u></i><br><br>
                    	<label for=\"mesintfmup-ins-juris\">Arguente</label>
                        <input type=\"text\" name=\"mesintfmup-ins-juris\" id=\"mesintfmup-ins-juris\" class=\"text ui-widget-content ui-corner-all\" /> 

                        <label for=\"mesintvogalfmup-ins-juris\">Outro</label>
                        <input type=\"text\" name=\"mesintvogalfmup-ins-juris\" id=\"mesintvogalfmup-ins-juris\" class=\"text ui-widget-content ui-corner-all\" />  

                        <i><u>Exterior</u></i><br><br>
                    	<label for=\"mesintargext-ins-juris\">Arguente</label>
                        <input type=\"text\" name=\"mesintargext-ins-juris\" id=\"mesintargext-ins-juris\" class=\"text ui-widget-content ui-corner-all\" /> 

                        <label for=\"mesintvogalext-ins-juris\">Outro</label>
                        <input type=\"text\" name=\"mesintvogalext-ins-juris\" id=\"mesintvogalext-ins-juris\" class=\"text ui-widget-content ui-corner-all\" />  
				</fieldset>
           	    </form>
         </div>"; 
		 
	//Inserir Outras Actividades
    echo "<div id=\"dialog-insert-outAct\" title=\"Inserir Outras Atividades\" hidden>
                <p class=\"validateTips-outact\"></p>
                    <form>
                    <fieldset id = \"edit\"> 
						<label for=\"texapoio-ins-outact\">Monografias de apoio às aulas ou a trabalho clínico</label>
                        <input type=\"text\" name=\"texapoio-ins-outact\" id=\"texapoio-ins-outact\" class=\"text ui-widget-content ui-corner-all\" /> 

                        <label for=\"livdidaticos-ins-outact\">Livros Didáticos</label>
                        <input type=\"text\" name=\"livdidaticos-ins-outact\" id=\"livdidaticos-ins-outact\" class=\"text ui-widget-content ui-corner-all\" />  

                    	<label for=\"livdivulgacao-ins-outact\">Livros de Divulgação</label>
                        <input type=\"text\" name=\"livdivulgacao-ins-outact\" id=\"livdivulgacao-ins-outact\" class=\"text ui-widget-content ui-corner-all\" /> 

                        <label for=\"app-ins-outact\">Vídeos, jogos, aplicações</label>
                        <input type=\"text\" name=\"app-ins-outact\" id=\"app-ins-outact\" class=\"text ui-widget-content ui-corner-all\" />    

                    	<label for=\"webp-ins-outact\">Páginas Internet</label>
                        <input type=\"text\" name=\"webp-ins-outact\" id=\"webp-ins-outact\" class=\"text ui-widget-content ui-corner-all\" />                        
				</fieldset>
           	    </form>
         </div>"; 

	//Inserir Ações Divulgação
    echo "<div id=\"dialog-insert-acaoDiv\" title=\"Inserir Ação de Divulgação\" hidden>
                <p class=\"validateTips-acaodivulgacao\"></p>
                    <form>
                    <fieldset id = \"edit\">  
						<label for=\"titulo-ins-acaodivulgacao\">Título</label>
                        <input type=\"text\" name=\"titulo-ins-acaodivulgacao\" id=\"titulo-ins-acaodivulgacao\" class=\"text ui-widget-content ui-corner-all\" /> 

                        <label for=\"data-ins-acaodivulgacao\">Data</label>
                        <input type=\"text\" name=\"data-ins-acaodivulgacao\" id=\"data-ins-acaodivulgacao\" class=\"text ui-widget-content ui-corner-all\" />  

                    	<label for=\"local-ins-acaodivulgacao\">Local</label>
                        <input type=\"text\" name=\"local-ins-acaodivulgacao\" id=\"local-ins-acaodivulgacao\" class=\"text ui-widget-content ui-corner-all\" /> 

                        <label for=\"npart-ins-acaodivulgacao\">Número de Participantes</label>
                        <input type=\"text\" name=\"npart-ins-acaodivulgacao\" id=\"npart-ins-acaodivulgacao\" class=\"text ui-widget-content ui-corner-all\" />                           
				</fieldset>
           	    </form>
         </div>"; 
	
	 //Inserir Produtos
    echo "<div id=\"dialog-insert-produto\" title=\"Inserir Produto/Serviço\" hidden>
                <p class=\"validateTips-produtos\"></p>
                    <form>
                    <fieldset id = \"edit\">  
						<label for=\"tipo-ins-produtos\">Tipo</label>
                        <input type=\"text\" name=\"tipo-ins-produtos\" id=\"tipo-ins-produtos\" class=\"text ui-widget-content ui-corner-all\" /> 

                        <label for=\"preco-ins-produtos\">Preço (€)</label>
                        <input type=\"text\" name=\"preco-ins-produtos\" id=\"preco-ins-produtos\" class=\"text ui-widget-content ui-corner-all\" />                     	                        
				</fieldset>
           	    </form>
         </div>"; 
		 
	//Inserir Publicações - Artigos, PEER REVIEW PROCEEDINGS, sumarios
	echo "<div id=\"dialog-insert-pubArt\" title=\"Inserir Publicação\" hidden>
			<p class=\"validateTips-insert-pubArt\"></p>
				<form>
			    <fieldset id = \"edit\">
					<label for=\"issn-ins-pubArt\">ISSN</label>
				    <input type=\"text\" name=\"issn-ins-pubArt\" id=\"issn-ins-pubArt\" class=\"text ui-widget-content ui-corner-all\" />

		  	  		<label for=\"revista-ins-pubArt\">Revista</label>
				    <input type=\"text\" name=\"revista-ins-pubArt\" id=\"revista-ins-pubArt\" class=\"text ui-widget-content ui-corner-all\" />	

				    <label for=\"titulo-ins-pubArt\">Título</label>				    
				    <input type=\"text\" name=\"titulo-ins-pubArt\" id=\"titulo-ins-pubArt\" class=\"text ui-widget-content ui-corner-all\" />

				    <label for=\"autores-ins-pubArt\">Autores</label>				    
				    <input type=\"text\" name=\"autores-ins-pubArt\" id=\"autores-ins-pubArt\" class=\"text ui-widget-content ui-corner-all\" />	

				    <label for=\"volume-ins-pubArt\">Volume</label>				    
				    <input type=\"text\" name=\"volume-ins-pubArt\" id=\"volume-ins-pubArt\" class=\"text ui-widget-content ui-corner-all\" />

				    <label for=\"issue-ins-pubArt\">Issue</label>				    
				    <input type=\"text\" name=\"issue-ins-pubArt\" id=\"issue-ins-pubArt\" class=\"text ui-widget-content ui-corner-all\" />	

				    <label for=\"prim-ins-pubArt\">Primeira Página</label>				    
				    <input type=\"text\" name=\"prim-ins-pubArt\" id=\"prim-ins-pubArt\" class=\"text ui-widget-content ui-corner-all\" />

				    <label for=\"ult-ins-pubArt\">Última Página</label>				    
				    <input type=\"text\" name=\"ult-ins-pubArt\" id=\"ult-ins-pubArt\" class=\"text ui-widget-content ui-corner-all\" />

			     	<label for=\"citacoes-ins-pubArt\">Citações</label>				    
				    <input type=\"text\" name=\"citacoes-ins-pubArt\" id=\"citacoes-ins-pubArt\" class=\"text ui-widget-content ui-corner-all\" />

	                <label for=\"estado-ins-pubArt\">Estado</label>
				    <SELECT id='estado-ins-pubArt' name='estado-ins-pubArt'>\n";
					getEstadoPubEdit();	
		echo "</fieldset>
	  		</form>
	 </div>";
	 
	 //Inserir Publicações - Conference Proceedings  
    echo "<div id=\"dialog-insert-pubConf\" title=\"Inserir Publicação\" hidden>
                <p class=\"validateTips-insert-pubConf\"></p>
                    <form>
                    <fieldset id = \"edit\"> 
						<label for=\"isbn-ins-pubConf\">ISBN</label>
                        <input type=\"text\" name=\"isbn-ins-pubConf\" id=\"isbn-ins-pubConf\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"nomepub-ins-pubConf\">Nome da Publicação</label>
                        <input type=\"text\" name=\"nomepub-ins-pubConf\" id=\"nomepub-ins-pubConf\" class=\"text ui-widget-content ui-corner-all\" />
						
                        <label for=\"editores-ins-pubConf\">Editores</label>
                        <input type=\"text\" name=\"editores-ins-pubConf\" id=\"editores-ins-pubConf\" class=\"text ui-widget-content ui-corner-all\" />
						
						<label for=\"colecao-ins-pubConf\">Colecção</label>
                        <input type=\"text\" name=\"colecao-ins-pubConf\" id=\"colecao-ins-pubConf\" class=\"text ui-widget-content ui-corner-all\" />

						<label for=\"volume-ins-pubConf\">Volume</label>
                        <input type=\"text\" name=\"volume-ins-pubConf\" id=\"volume-ins-pubConf\" class=\"text ui-widget-content ui-corner-all\" />
						
                        <label for=\"titulo-ins-pubConf\">Título do Artigo</label>
                        <input type=\"text\" name=\"titulo-ins-pubConf\" id=\"titulo-ins-pubConf\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"autores-ins-pubConf\">Autores</label>
                        <input type=\"text\" name=\"autores-ins-pubConf\" id=\"autores-ins-pubConf\" class=\"text ui-widget-content ui-corner-all\" />
 
						<label for=\"prim-ins-pubConf\">Primeira Página</label>				    
						<input type=\"text\" name=\"prim-ins-pubConf\" id=\"prim-ins-pubConf\" class=\"text ui-widget-content ui-corner-all\" />

						<label for=\"ult-ins-pubConf\">Última Página</label>				    
						<input type=\"text\" name=\"ult-ins-pubConf\" id=\"ult-ins-pubConf\" class=\"text ui-widget-content ui-corner-all\" />
                        
                        <label for=\"estado-ins-pubConf\">Estado</label>
                        <SELECT id='estado-ins-pubConf' name='estado-ins-pubConf'>\n";
                        getEstadoPubEdit();
            echo "</fieldset>
           	     </form>
         </div>";
	
	//Inserir publicacoes livros, etc	
	echo "<div id=\"dialog-insert-pubLiv\" title=\"Inserir Publicação\" hidden>
			<p class=\"validateTips-pubManualNac-Liv\"></p>
				<form>
			    <fieldset id = \"edit\">
					<label for=\"nomepub-ins-pubLiv\">Nome da Publicação</label>
				    <input type=\"text\" name=\"nomepub-ins-pubLiv\" id=\"nomepub-ins-pubLiv\" class=\"text ui-widget-content ui-corner-all\" /

				    <label for=\"issn-ins-pubLiv\">ISSN</label>				    
				    <input type=\"text\" name=\"issn-ins-pubLiv\" id=\"issn-ins-pubLiv\" class=\"text ui-widget-content ui-corner-all\" />

					<label for=\"isbn-ins-pubLiv\">ISBN</label>				    
				    <input type=\"text\" name=\"isbn-ins-pubLiv\" id=\"isbn-ins-pubLiv\" class=\"text ui-widget-content ui-corner-all\" />
					
					<label for=\"titulo-ins-pubLiv\">Título</label>				    
				    <input type=\"text\" name=\"titulo-ins-pubLiv\" id=\"titulo-ins-pubLiv\" class=\"text ui-widget-content ui-corner-all\" />
					
					<label for=\"autores-ins-pubLiv\">Autores</label>				    
				    <input type=\"text\" name=\"autores-ins-pubLiv\" id=\"autores-ins-pubLiv\" class=\"text ui-widget-content ui-corner-all\" />
					
					<label for=\"volume-ins-pubLiv\">Volume</label>				    
				    <input type=\"text\" name=\"volume-ins-pubLiv\" id=\"volume-ins-pubLiv\" class=\"text ui-widget-content ui-corner-all\" />
					
					<label for=\"issue-ins-pubLiv\">Issue</label>				    
				    <input type=\"text\" name=\"issue-ins-pubLiv\" id=\"issue-ins-pubLiv\" class=\"text ui-widget-content ui-corner-all\" />
					
					<label for=\"ar-ins-pubLiv\">AR</label>				    
				    <input type=\"text\" name=\"ar-ins-pubLiv\" id=\"ar-ins-pubLiv\" class=\"text ui-widget-content ui-corner-all\" />
					
					<label for=\"colecao-ins-pubLiv\">Coleção</label>				    
				    <input type=\"text\" name=\"colecao-ins-pubLiv\" id=\"colecao-ins-pubLiv\" class=\"text ui-widget-content ui-corner-all\" />
					
					<label for=\"prim-ins-pubLiv\">Primeira Página</label>				    
					<input type=\"text\" name=\"prim-ins-pubLiv\" id=\"prim-ins-pubLiv\" class=\"text ui-widget-content ui-corner-all\" />

					<label for=\"ult-ins-pubLiv\">Última Página</label>				    
					<input type=\"text\" name=\"ult-ins-pubLiv\" id=\"ult-ins-pubLiv\" class=\"text ui-widget-content ui-corner-all\" />
					
					<label for=\"citacoes-ins-pubLiv\">Citações</label>				    
					<input type=\"text\" name=\"citacoes-ins-pubLiv\" id=\"citacoes-ins-pubLiv\" class=\"text ui-widget-content ui-corner-all\" />

					<label for=\"conftitle-ins-pubLiv\">Conftitle</label>				    
					<input type=\"text\" name=\"conftitle-ins-pubLiv\" id=\"conftitle-ins-pubLiv\" class=\"text ui-widget-content ui-corner-all\" />
								
					<label for=\"language-ins-pubLiv\">Language</label>				    
					<input type=\"text\" name=\"language-ins-pubLiv\" id=\"language-ins-pubLiv\" class=\"text ui-widget-content ui-corner-all\" />
					
					<label for=\"editor-ins-pubLiv\">Editor</label>				    
					<input type=\"text\" name=\"editor-ins-pubLiv\" id=\"editor-ins-pubLiv\" class=\"text ui-widget-content ui-corner-all\" />

	                <label for=\"estado-ins-pubLiv\">Estado</label>
				    <SELECT id='estado-ins-pubLiv' name='estado-ins-pubLiv'>\n";
					getEstadoPubEdit();	
		echo "</fieldset>
	  		</form>
	 </div>";
	
	 //Inserir Publicações Manuais Patentes
	echo "<div id=\"dialog-insert-pubManualPatentes\" title=\"Inserir Publicações Manuais - Patentes\" hidden>
			<p class=\"validateTips-ins-pubManualPatentes\"></p>
				<form>
			    <fieldset id = \"edit\">				
				    <label for=\"npatente-ins-pubManualPatentes\">Nº Patente</label>
				    <input type=\"text\" name=\"npatente-ins-pubManualPatentes\" id=\"npatente-ins-pubManualPatentes\" class=\"text ui-widget-content ui-corner-all\" />

		  	  		<label for=\"ipc-ins-pubManualPatentes\">IPC's</label>
				    <input type=\"text\" name=\"ipc-ins-pubManualPatentes\" id=\"ipc-ins-pubManualPatentes\" class=\"text ui-widget-content ui-corner-all\" />	

				    <label for=\"titulo-ins-pubManualPatentes\">Título</label>				    
				    <input type=\"text\" name=\"titulo-ins-pubManualPatentes\" id=\"titulo-ins-pubManualPatentes\" class=\"text ui-widget-content ui-corner-all\" />

				    <label for=\"autores-ins-pubManualPatentes\">Autores</label>				    
				    <input type=\"text\" name=\"autores-ins-pubManualPatentes\" id=\"autores-ins-pubManualPatentes\" class=\"text ui-widget-content ui-corner-all\" />	

				    <label for=\"data-ins-pubManualPatentes\">Data de Publicação</label>				    
				    <input type=\"text\" name=\"data-ins-pubManualPatentes\" id=\"data-ins-pubManualPatentes\" class=\"text ui-widget-content ui-corner-all\" />

			     	<label for=\"link-ins-pubManualPatentes\">Link</label>				    
				    <textarea name='link-ins-pubManualPatentes' id='link-ins-pubManualPatentes' style='height: 75px; width: 500px'></textarea></td>		

	                <label for=\"estado-ins-pubManualPatentes\">Estado</label>
				    <SELECT id='estado-ins-pubManualPatentes' name='estado-ins-pubManualPatentes'>\n";
					getEstadoPubEdit();	
		echo "</fieldset>
	  		</form>
	 </div>";
	 
	 //Inserir Publicações Manuais -  Livros
	echo "<div id=\"dialog-insert-pubManualLiv\" title=\"Inserir Publicações - Livros\" hidden>
			<p class=\"validateTips-ins-pubManualLiv\"></p>
				<form>
			    <fieldset id = \"edit\">
					<label for=\"isbn-ins-pubManualLiv\">ISBN</label>
				    <input type=\"text\" name=\"isbn-ins-pubManualLiv\" id=\"isbn-ins-pubManualLiv\" class=\"text ui-widget-content ui-corner-all\" /

				    <label for=\"titulo-ins-pubManualLiv\">Título</label>				    
				    <input type=\"text\" name=\"titulo-ins-pubManualLiv\" id=\"titulo-ins-pubManualLiv\" class=\"text ui-widget-content ui-corner-all\" />

				    <label for=\"autores-ins-pubManualLiv\">Autores</label>				    
				    <input type=\"text\" name=\"autores-ins-pubManualLiv\" id=\"autores-ins-pubManualLiv\" class=\"text ui-widget-content ui-corner-all\" />	

				    <label for=\"editor-ins-pubManualLiv\">Editor</label>				    
				    <input type=\"text\" name=\"editor-ins-pubManualLiv\" id=\"editor-ins-pubManualLiv\" class=\"text ui-widget-content ui-corner-all\" />

				    <label for=\"editora-ins-pubManualLiv\">Editora</label>				    
				    <input type=\"text\" name=\"editora-ins-pubManualLiv\" id=\"editora-ins-pubManualLiv\" class=\"text ui-widget-content ui-corner-all\" />	

				    <label for=\"link-ins-pubManualLiv\">Link</label>				    
				    <textarea name='link-ins-pubManualLiv' id='link-ins-pubManualLiv' style='height: 75px; width: 500px'></textarea></td>		

	                <label for=\"estado-ins-pubManualLiv\">Estado</label>
				    <SELECT id='estado-ins-pubManualLiv' name='estado-ins-pubManualLiv'>\n";
					getEstadoPubEdit();	
					
		echo "</fieldset>
	  		</form>
	 </div>";

	 //Inserir Publicações Manuais Internacional - Capitulos de Livros
	echo "<div id=\"dialog-insert-pubManualCLiv\" title=\"Editar Dados Publicações Manuais - Capitulos de Livros\" hidden>
			<p class=\"validateTips-pubManualCLiv\"></p>
				<form>
			    <fieldset id = \"edit\">
					<label for=\"isbn-ins-pubManualCLiv\">ISBN</label>
				    <input type=\"text\" name=\"isbn-ins-pubManualCLiv\" id=\"isbn-ins-pubManualCLiv\" class=\"text ui-widget-content ui-corner-all\" /

				    <label for=\"nomepub-ins-pubManualCLiv\">Título da Obra</label>				    
				    <input type=\"text\" name=\"nomepub-ins-pubManualCLiv\" id=\"nomepub-ins-pubManualCLiv\" class=\"text ui-widget-content ui-corner-all\" />

				    <label for=\"editora-ins-pubManualCLiv\">Editora</label>				    
				    <input type=\"text\" name=\"editora-ins-pubManualCLiv\" id=\"editora-ins-pubManualCLiv\" class=\"text ui-widget-content ui-corner-all\" />	

				    <label for=\"editor-ins-pubManualCLiv\">Editores</label>				    
				    <input type=\"text\" name=\"editor-ins-pubManualCLiv\" id=\"editor-ins-pubManualCLiv\" class=\"text ui-widget-content ui-corner-all\" />

				    <label for=\"titulo-ins-pubManualCLiv\">Título do Artigo</label>				    
				    <input type=\"text\" name=\"titulo-ins-pubManualCLiv\" id=\"titulo-ins-pubManualCLiv\" class=\"text ui-widget-content ui-corner-all\" />

				    <label for=\"autores-ins-pubManualCLiv\">Autores</label>				    
				    <input type=\"text\" name=\"autores-ins-pubManualCLiv\" id=\"autores-ins-pubManualCLiv\" class=\"text ui-widget-content ui-corner-all\" />		 				   

				    <label for=\"prim-ins-pubManualCLiv\">Primeira Página</label>				    
				    <input type=\"text\" name=\"prim-ins-pubManualCLiv\" id=\"prim-ins-pubManualCLiv\" class=\"text ui-widget-content ui-corner-all\" />

				    <label for=\"ult-ins-pubManualCLiv\">Última Página</label>				    
				    <input type=\"text\" name=\"ult-ins-pubManualCLiv\" id=\"ult-ins-pubManualCLiv\" class=\"text ui-widget-content ui-corner-all\" />

				    <label for=\"link-ins-pubManualCLiv\">Link</label>				    
				    <textarea name='link-ins-pubManualCLiv' id='link-ins-pubManualCLiv' style='height: 75px; width: 500px'></textarea></td>		

	                <label for=\"estado-ins-pubManualCLiv\">Estado</label>
				    <SELECT id='estado-ins-pubManualCLiv' name='estado-ins-pubManualCLiv'>\n";
					getEstadoPubEdit();	
		echo "</fieldset>
	  		</form>
	 </div>";
	
echo "<div id=\"dialog-confirm-hab\" title=\"Apagar Habilitação\" hidden>
	<p><span class=\"ui-icon ui-icon-alert\" style=\"float:left; margin:0 7px 0px 0;\"></span>Está prestes a eliminar uma habilitação. Tem a certeza que pretende eliminar? Esta ação é destrutiva e definitiva.</p>
</div>";

echo "<div id=\"dialog-confirm-agr\" title=\"Apagar Agregação\" hidden>
	<p><span class=\"ui-icon ui-icon-alert\" style=\"float:left; margin:0 7px 0px 0;\"></span>Está prestes a eliminar uma agregação. Tem a certeza que pretende eliminar? Esta ação é destrutiva e definitiva.</p>
</div>";

echo "<div id=\"dialog-confirm-exp-pro-doc\" title=\"Apagar Caracterização Docente\" hidden>
	<p><span class=\"ui-icon ui-icon-alert\" style=\"float:left; margin:0 7px 0px 0;\"></span>Está prestes a eliminar uma caracterização de um docente. Tem a certeza que pretende eliminar? Esta ação é destrutiva e definitiva.</p>
</div>";

echo "<div id=\"dialog-confirm-exp-pro-pdoc\" title=\"Apagar Caracterização Pós-Doc\" hidden>
	<p><span class=\"ui-icon ui-icon-alert\" style=\"float:left; margin:0 7px 0px 0;\"></span>Está prestes a eliminar uma caracterização pós-doc. Tem a certeza que pretende eliminar? Esta ação é destrutiva e definitiva.</p>
</div>";

echo "<div id=\"dialog-confirm-dircurso\" title=\"Apagar Direcção de Curso\" hidden>
	<p><span class=\"ui-icon ui-icon-alert\" style=\"float:left; margin:0 7px 0px 0;\"></span>Está prestes a eliminar uma Direcção de Curso. Tem a certeza que pretende eliminar? Esta ação é destrutiva e definitiva.</p>
</div>";

echo "<div id=\"dialog-confirm-colIntAct\" title=\"Apagar Colaboração Actual FMUP\" hidden>
	<p><span class=\"ui-icon ui-icon-alert\" style=\"float:left; margin:0 7px 0px 0;\"></span>Está prestes a eliminar uma Colaboração Atual FMUP. Tem a certeza que pretende eliminar? Esta ação é destrutiva e definitiva.</p>
</div>";

echo "<div id=\"dialog-confirm-colExtAct\" title=\"Apagar Colaboração Externa Atual\" hidden>
	<p><span class=\"ui-icon ui-icon-alert\" style=\"float:left; margin:0 7px 0px 0;\"></span>Está prestes a eliminar uma Colaboração Externa Atual. Tem a certeza que pretende eliminar? Esta ação é destrutiva e definitiva.</p>
</div>";

echo "<div id=\"dialog-confirm\" title=\"Apagar Projecto\" hidden>
	<p><span class=\"ui-icon ui-icon-alert\" style=\"float:left; margin:0 7px 0px 0;\"></span>Está prestes a eliminar um projecto. Tem a certeza que pretende eliminar? Esta ação é destrutiva e definitiva.</p>
</div>";

echo "<div id=\"dialog-confirm-pub\" title=\"Apagar Publicação\" hidden>
	<p><span class=\"ui-icon ui-icon-alert\" style=\"float:left; margin:0 7px 0px 0;\"></span>Está prestes a eliminar uma publicação. Tem a certeza que pretende eliminar? Esta ação é destrutiva e definitiva.</p>
</div>";

echo "<div id=\"dialog-confirm-cnf\" title=\"Apagar Conferência\" hidden>
	<p><span class=\"ui-icon ui-icon-alert\" style=\"float:left; margin:0 7px 0px 0;\"></span>Está prestes a eliminar uma conferência. Tem a certeza que pretende eliminar? Esta ação é destrutiva e definitiva.</p>
</div>";

echo "<div id=\"dialog-confirm-apr\" title=\"Apagar Apresentação\" hidden>
	<p><span class=\"ui-icon ui-icon-alert\" style=\"float:left; margin:0 7px 0px 0;\"></span>Está prestes a eliminar uma apresentação. Tem a certeza que pretende eliminar? Esta ação é destrutiva e definitiva.</p>
</div>";

echo "<div id=\"dialog-confirm-acaoDiv\" title=\"Apagar Ação Divulgação\" hidden>
	<p><span class=\"ui-icon ui-icon-alert\" style=\"float:left; margin:0 7px 0px 0;\"></span>Está prestes a eliminar uma ação de divulgação. Tem a certeza que pretende eliminar? Esta ação é destrutiva e definitiva.</p>
</div>";

echo "<div id=\"dialog-confirm-produto\" title=\"Apagar Produto\" hidden>
	<p><span class=\"ui-icon ui-icon-alert\" style=\"float:left; margin:0 7px 0px 0;\"></span>Está prestes a eliminar um produto. Tem a certeza que pretende eliminar? Esta ação é destrutiva e definitiva.</p>
</div>";

echo "<div id=\"dialog-confirm-premio\" title=\"Apagar Prémio\" hidden>
	<p><span class=\"ui-icon ui-icon-alert\" style=\"float:left; margin:0 7px 0px 0;\"></span>Está prestes a eliminar um prémio. Tem a certeza que pretende eliminar? Esta ação é destrutiva e definitiva.</p>
</div>";

echo "<div id=\"dialog-confirm-arbRevista\" title=\"Apagar Arbitragem Revista\" hidden>
	<p><span class=\"ui-icon ui-icon-alert\" style=\"float:left; margin:0 7px 0px 0;\"></span>Está prestes a eliminar uma arbitragem revista. Tem a certeza que pretende eliminar? Esta ação é destrutiva e definitiva.</p>
</div>";

echo "<div id=\"dialog-confirm-uniInv\" title=\"Apagar Unidades de Investigação\" hidden>
	<p><span class=\"ui-icon ui-icon-alert\" style=\"float:left; margin:0 7px 0px 0;\"></span>Está prestes a eliminar uma unidade de investigação. Tem a certeza que pretende eliminar? Esta ação é destrutiva e definitiva.</p>
</div>";

echo "<div id=\"dialog-confirm-sc\" title=\"Apagar Sociedades Científicas\" hidden>
	<p><span class=\"ui-icon ui-icon-alert\" style=\"float:left; margin:0 7px 0px 0;\"></span>Está prestes a eliminar uma sociedade científica. Tem a certeza que pretende eliminar? Esta ação é destrutiva e definitiva.</p>
</div>";

echo "<div id=\"dialog-confirm-rede\" title=\"Apagar Rede Científica\" hidden>
	<p><span class=\"ui-icon ui-icon-alert\" style=\"float:left; margin:0 7px 0px 0;\"></span>Está prestes a eliminar uma rede científica. Tem a certeza que pretende eliminar? Esta ação é destrutiva e definitiva.</p>
</div>";

echo "<div id=\"dialog-confirm-missao\" title=\"Apagar Missão\" hidden>
	<p><span class=\"ui-icon ui-icon-alert\" style=\"float:left; margin:0 7px 0px 0;\"></span>Está prestes a eliminar uma missão. Tem a certeza que pretende eliminar? Esta ação é destrutiva e definitiva.</p>
</div>";

echo "<div id=\"dialog-confirm-orientacao\" title=\"Apagar Missão\" hidden>
	<p><span class=\"ui-icon ui-icon-alert\" style=\"float:left; margin:0 7px 0px 0;\"></span>Está prestes a eliminar uma orientação. Tem a certeza que pretende eliminar? Esta ação é destrutiva e definitiva.</p>
</div>";

echo "<div id=\"dialog-confirm-juri\" title=\"Apagar Júri\" hidden>
	<p><span class=\"ui-icon ui-icon-alert\" style=\"float:left; margin:0 7px 0px 0;\"></span>Está prestes a eliminar um júri. Tem a certeza que pretende eliminar? Esta ação é destrutiva e definitiva.</p>
</div>";

echo "<div id=\"dialog-confirm-outact\" title=\"Apagar Outras Actividades\" hidden>
	<p><span class=\"ui-icon ui-icon-alert\" style=\"float:left; margin:0 7px 0px 0;\"></span>Está prestes a eliminar um registo de outras actividades. Tem a certeza que pretende eliminar? Esta ação é destrutiva e definitiva.</p>
</div>";

echo "<div id=\"dialog-confirm-acao\" title=\"Confirmar Ação\" hidden>
	<p><span class=\"ui-icon ui-icon-alert\" style=\"float:left; margin:0 7px 0px 0;\"></span>Está prestes a confirmar uma ação. Tem a certeza que pretende confirmar?</p>
</div>";

echo "<div id=\"dialog-remove-acao\" title=\"Eliminar Ação\" hidden>
	<p><span class=\"ui-icon ui-icon-alert\" style=\"float:left; margin:0 7px 0px 0;\"></span>Está prestes a eliminar uma ação. Tem a certeza que pretende eliminar?</p>
</div>";

echo "<div id=\"dialog-apaga-acao\" title=\"Apagar Ação\" hidden>
	<p><span class=\"ui-icon ui-icon-alert\" style=\"float:left; margin:0 7px 0px 0;\"></span>Está prestes a apagar uma ação. Tem a certeza que pretende apagar?</p>
</div>";

echo "<div id=\"dialog-confirm-repetidos\" title=\"LGDF - Validação de Duplicados/Repetidos\" hidden>
	<span id='dialog-confirm-repetidos-img' class=\"ui-icon ui-icon-alert\" style=\"float:left; margin:0 7px 0px 0;\"></span><p id='dialog-confirm-repetidos-txt'></p>
</div>";

echo "<div id=\"dialog-showinfo-acao\" title=\"LGDF - Informação Ação\" hidden>
	<p id='dialog-showinfo-acao-autor'></p>
	<br><p id='dialog-showinfo-acao-data'></p>
</div>";

	//Editar Investigador
	echo "<div id=\"dialog-form-inv\" title=\"Editar Dados Investigador\" hidden>
			  <p class=\"validateTips\"></p>
			  <form>
				  <fieldset id = \"edit\">
				    <label for=\"nome-inv\">Nome</label>
				    <input type=\"text\" name=\"nome-inv\" id=\"nome-inv\" class=\"text ui-widget-content ui-corner-all\" />
				    <label for=\"nummec-inv\">Número Mecanográfico</label>
				    <input type=\"text\" name=\"nummec-inv\" id=\"nummec-inv\" value=\"\" class=\"text ui-widget-content ui-corner-all\" />
				    <label for=\"datanasc-inv\">Data Nascimento</label>
				    <input type=\"text\" name=\"datanasc-inv\" id=\"datanasc-inv\" value=\"\" class=\"text ui-widget-content ui-corner-all\" />
				   </fieldset>
			  </form>
		</div>";

	//Editar Habilitacoes
	/* PEDRO bloco para inserir dados */
	
	//Inserir Habilitacoes
	echo "<div id=\"dialog-insert-habilitacao\" title=\"Inserir Habilitação\" hidden>
		  <p class=\"validateTips\"></p>
		  <form>
			  <fieldset id = \"edit\">
				<label for=\"ano-ini-ins-hab\">Ano de início:</label>
				<SELECT name='ano-ini-ins-hab' id='ano-ini-ins-hab'>\n";
	getAnoEdit();
	echo "<br><label for=\"ano-fim-hab\">Ano de fim:</label>";
	echo "<SELECT name='ano-fim-ins-hab' id='ano-fim-ins-hab'>\n";
	getAnoEdit();
	echo "<br><label for=\"curso-ins-hab\">Curso:</label>
				<input type=\"text\" name=\"curso-ins-hab\" id=\"curso-ins-hab\" class=\"text ui-widget-content ui-corner-all\" />
				<label for=\"inst-ins-hab\">Instituição:</label>
				<input type=\"text\" name=\"inst-ins-hab\" id=\"inst-ins-hab\" class=\"text ui-widget-content ui-corner-all\" />
				<label for=\"grau-ins-hab\">Grau:</label>";
	echo "<SELECT id='grau-ins-hab' name='grau-ins-hab' >\n";
	getGrauEdit();
	echo "<br><label for=\"dist-ins-hab\">Distinção:</label>";
	echo "<SELECT name='dist-ins-hab' id='dist-ins-hab'>\n";
	getDistincaoEdit();
	echo "<br><label for=\"bolsa-ins-hab\">Bolsa:</label>";
	echo "<SELECT name=bolsa-ins-hab' id='bolsa-ins-hab'>\n";
	getBolsaEdit();
	echo "<br><label for=\"tit-ins-hab\">Título da Tese:</label>
				<textarea name='tit-ins-hab' id='tit-ins-hab' style='height: 75px; width: 500px'></textarea></td>";
	echo "<br><label for=\"perc-ins-hab\">Percentagem</label>";
	echo "<select name='perc-ins-hab' id='perc-ins-hab' >";
	echo getPercentagemEdit();
	//TODO lookup Instituicao
	echo "<br><label for=\"ori-ins-hab\">Orientador:</label>
				<input type=\"text\" name=\"ori-ins-hab\" id=\"ori-ins-hab\" class=\"text ui-widget-content ui-corner-all\" />
				<label for=\"ori_inst-ins-hab\">Instituição Orientador:</label>
				<input type=\"text\" name=\"ori_inst-ins-hab\" id=\"ori_inst-ins-hab\" class=\"text ui-widget-content ui-corner-all\" />";
	//TODO lookup Instituicao
	echo "<br><label for=\"cori-ins-hab\">COOrientador:</label>
				<input type=\"text\" name=\"cori-ins-hab\" id=\"cori-ins-hab\" class=\"text ui-widget-content ui-corner-all\" />
				<label for=\"cori_inst-ins-hab\">Instituição COOrientador:</label>
				<input type=\"text\" name=\"cori_inst-ins-hab\" id=\"cori_inst-ins-hab\" class=\"text ui-widget-content ui-corner-all\" />";
	echo " </fieldset>
		  </form>
	</div>";
	
	/* PEDRO bloco para editar dados */
	
		echo "<div id=\"dialog-form-hab\" title=\"Editar Dados Habilitações\" hidden>
			  <p class=\"validateTips\"></p>
			  <form>
				  <fieldset id = \"edit\">				    
				    <label for=\"ano-ini-hab\">Ano de início:</label>
				    <SELECT name='ano-ini-hab' id='ano-ini-hab'>\n";
				    getAnoEdit();	
				    echo "<br><label for=\"ano-fim-hab\">Ano de fim:</label>";
				    echo "<SELECT name='ano-fim-hab' id='ano-fim-hab'>\n";
				    getAnoEdit();	
				    echo "<br><label for=\"curso-hab\">Curso:</label>				    
				    <input type=\"text\" name=\"curso-hab\" id=\"curso-hab\" class=\"text ui-widget-content ui-corner-all\" />
				    <label for=\"inst-hab\">Instituição:</label>				    
				    <input type=\"text\" name=\"inst-hab\" id=\"inst-hab\" class=\"text ui-widget-content ui-corner-all\" />
				    <label for=\"grau-hab\">Grau:</label>";
				    echo "<SELECT id='grau-hab' name='grau-hab' >\n";
				    getGrauEdit();	
				    echo "<br><label for=\"dist-hab\">Distinção:</label>";	
					echo "<SELECT name='dist-hab' id='dist-hab'>\n";
				    getDistincaoEdit();	
				    echo "<br><label for=\"bolsa-hab\">Bolsa:</label>";	
					echo "<SELECT name=bolsa-hab' id='bolsa-hab'>\n";
				    getBolsaEdit();	 
				    echo "<br><label for=\"tit-hab\">Título da Tese:</label>
				    <textarea name='tit-hab' id='tit-hab' style='height: 75px; width: 500px'></textarea></td>";		   
				    echo "<br><label for=\"perc-hab\">Percentagem</label>";	
				    echo "<select name='perc-hab' id='perc-hab' >";
				    echo getPercentagemEdit();
				    //TODO lookup Instituicao
				    echo "<br><label for=\"ori-hab\">Orientador:</label> 
					<input type=\"text\" name=\"ori-hab\" id=\"ori-hab\" class=\"text ui-widget-content ui-corner-all\" />
					<label for=\"ori_inst-hab\">Instituição Orientador:</label>
					<input type=\"text\" name=\"ori_inst-hab\" id=\"ori_inst-hab\" class=\"text ui-widget-content ui-corner-all\" />";
				    //TODO lookup Instituicao
				    echo "<br><label for=\"cori-hab\">COOrientador:</label> 
					<input type=\"text\" name=\"cori-hab\" id=\"cori-hab\" class=\"text ui-widget-content ui-corner-all\" />
					<label for=\"cori_inst-hab\">Instituição COOrientador:</label>
					<input type=\"text\" name=\"cori_inst-hab\" id=\"cori_inst-hab\" class=\"text ui-widget-content ui-corner-all\" />";
					echo " </fieldset>
			  </form>
		</div>";
					
		/*PEDRO  Fim de bloco */ 
		
	//Editar Agregacao
	echo "<div id=\"dialog-form-agr\" title=\"Editar Dados Agregação\" hidden>
			  <form>
				  <fieldset id = \"edit\">
				    <label for=\"unanimidade-agr\">Unanimidade</label>
				    <SELECT id='unanimidade-agr' name='unanimidade-agr' >\n
						<option>Sim</option>\n	
						<option>Não</option>\n		
					</SELECT><br />\n		    
				   </fieldset>
			  </form>
		</div>";

	//Editar Caracterização Docentes
	echo "<div id=\"dialog-form-exp-pro-doc\" title=\"Editar Dados Caracterização Docentes\" hidden>
			<form>
			   <fieldset id = \"edit\">
				    <label for=\"tipo-doc\">Tipo</label>
				    <select name='tipo-doc' id='tipo-doc'>";
					getCatEdit();
			  echo "<br><br><label for=\"perc-doc\">Percentagem</label>
		  	  		<select id=\"perc-doc\" name=\"perc-doc\">";
		  	  		getPercentagemEdit();
		  echo"</fieldset>
		  </form>
		</div>";

	//Editar Caracterização Pós-Doc
	echo "<div id=\"dialog-form-exp-pro-pdoc\" title=\"Editar Dados Caracterização Pós-Doc\" hidden>
			<form>
			    <fieldset id = \"edit\">
				    <label for=\"bolsa-pdoc\">Tipo Bolsa</label>
				    <select name='bolsa-pdoc' id='bolsa-pdoc'>\n";			  
					getBolsaEdit();	  
		  	  echo "<br><br><label for=\"per-pdoc\">Percentagem</label>
		  	  		<select id=\"per-pdoc\" name=\"per-pdoc\">";
		  	  		getPercentagemEdit();
			echo"</fieldset>
			</form>
		</div>";

	//Editar Direção Curso		
	echo "<div id=\"dialog-form-dircurso\" title=\"Editar Dados Direção Curso\" hidden>	
				<p class=\"validateTips-dircurso\"></p>
				<form>
			    <fieldset id = \"edit\">
				    <label for=\"dataini-dircurso\">Data de Início</label>
				    <input type=\"text\" name=\"dataini-dircurso\" id=\"dataini-dircurso\" class=\"text ui-widget-content ui-corner-all\" />				 	  
		  	  		<label for=\"datafim-dircurso\">Data de Fim</label>
					<input type=\"text\" name=\"datafim-dircurso\" id=\"datafim-dircurso\" class=\"text ui-widget-content ui-corner-all\" />
		  	  		<label for=\"datafim-dircurso\">Curso</label>
		  	  		<input type=\"text\" name=\"curso-dircurso\" id=\"curso-dircurso\" class=\"text ui-widget-content ui-corner-all\" />
		  	  		<label for=\"grau-dircurso\">Grau</label>";
		  	  		echo "<SELECT id='grau-dircurso' name='grau-dircurso' >\n";
		  	  		getGrauEdit();
		  echo "</fieldset>
		  </form>
		</div>";

	//Editar Colaboradores Internos FMUP
	echo "<div id=\"dialog-form-colIntAct\" title=\"Editar Dados Colaboradores FMUP Atuais\" hidden>
			<p class=\"validateTips-colIntAct\"></p>
				<form>
			    <fieldset id = \"edit\">
					<label for=\"nome-colIntAct\">Nome</label>
				    <input type=\"text\" name=\"nome-colIntAct\" id=\"nome-colIntAct\" class=\"text ui-widget-content ui-corner-all\" />	
				    <label for=\"nomecient-colIntAct\">Nome Científico</label>
				    <input type=\"text\" name=\"nomecient-colIntAct\" id=\"nomecient-colIntAct\" class=\"text ui-widget-content ui-corner-all\" />				 	  
		  	  		<label for=\"dep-colIntAct\">Departamento</label>
		  	  		<select name='dep-colIntAct' id='dep-colIntAct'>";
					getDepartamentosEdit();	  	  		
		  	echo"</fieldset>
		  		</form>
		 </div>";

	//Editar Colaboradores Externos FMUP
	echo "<div id=\"dialog-form-colExtAct\" title=\"Editar Dados Colaboradores FMUP Atuais\" hidden>
			<p class=\"validateTips-colIntAct\"></p>
				<form>
			    <fieldset id = \"edit\">
				    <label for=\"nomecient-colExtAct\">Nome Científico</label>
				    <input type=\"text\" name=\"nomecient-colExtAct\" id=\"nomecient-colExtAct\" class=\"text ui-widget-content ui-corner-all\" />				 	  
		  	  		<label for=\"inst-colExtAct\">Instituição</label>
					<SELECT name='inst-colExtAct' id='inst-colExtAct'>\n";
					getInstEdit();
		echo "<br><label for=\"pais-colExtAct\">Pais</label>
				   	<select name='pais-colExtAct' id='pais-colExtAct'>";
					getPaisesEdit();	  	  		
		  	echo"</fieldset>
		  		</form>
		 </div>";

	//Editar Unidades Investigacao
	echo "<div id=\"dialog-form-uniInv\" title=\"Editar Dados Unidades Investigação\" hidden>
			<p class=\"validateTips-uniInv\"></p>
				<form>
			    <fieldset id = \"edit\">
				    <label for=\"uni-uniInv\">Unidade</label>
					<SELECT name='uni-uniInv' id='uni-uniInv' onchange='$(\"#ava-uniInv\").val(value);'>\n";
				    getUniInvEdit();
		  echo "<br><br><label for=\"ava-uniInv\">Avaliação 2007</label>		
					<input type=\"text\" name=\"ava-uniInv\" id=\"ava-uniInv\" class=\"text ui-widget-content ui-corner-all\" />	
				    <label for=\"perc-uniInv\">Percentagem</label>				    
				    <select id=\"perc-uniInv\" name=\"perc-uniInv\">";
		  	  		getPercentagemEdit();
			
			echo "	<br /><br /><label for=\"resp-uniInv\">Responsável</label>
				    <SELECT id='resp-uniInv' name='resp-uniInv' >\n
						<option>Sim</option>\n	
						<option>Não</option>\n		
					</SELECT>\n	
					</fieldset>
		  		</form>
		 </div>";

	 //Editar Publicações Manuais Patentes
	echo "<div id=\"dialog-form-pubManualPatentes\" title=\"Editar Dados Publicações Manuais - Patentes\" hidden>
			<p class=\"validateTips-pubManualPatentes\"></p>
				<form>
			    <fieldset id = \"edit\">
				    <label for=\"npatente-pubManualPatentes\">Nº Patente</label>
				    <input type=\"text\" name=\"npatente-pubManualPatentes\" id=\"npatente-pubManualPatentes\" class=\"text ui-widget-content ui-corner-all\" />

		  	  		<label for=\"ipc-pubManualPatentes\">IPC's</label>
				    <input type=\"text\" name=\"ipc-pubManualPatentes\" id=\"ipc-pubManualPatentes\" class=\"text ui-widget-content ui-corner-all\" />	

				    <label for=\"titulo-pubManualPatentes\">Título</label>				    
				    <input type=\"text\" name=\"titulo-pubManualPatentes\" id=\"titulo-pubManualPatentes\" class=\"text ui-widget-content ui-corner-all\" />

				    <label for=\"autores-pubManualPatentes\">Autores</label>				    
				    <input type=\"text\" name=\"autores-pubManualPatentes\" id=\"autores-pubManualPatentes\" class=\"text ui-widget-content ui-corner-all\" />	

				    <label for=\"data-pubManualPatentes\">Data de Publicação</label>				    
				    <input type=\"text\" name=\"data-pubManualPatentes\" id=\"data-pubManualPatentes\" class=\"text ui-widget-content ui-corner-all\" />

			     	<label for=\"link-pubManualPatentes\">Link</label>				    
				    <textarea name='link-pubManualPatentes' id='link-pubManualPatentes' style='height: 75px; width: 500px'></textarea></td>		

	                <label for=\"estado-pubManualPatentes\">Estado</label>
				    <SELECT id='estado-pubManualPatentes' name='estado-pubManualPatentes'>\n";
					getEstadoPubEdit();	
		echo "</fieldset>
	  		</form>
	 </div>";

	 //Editar Publicações Manuais Internacional - Artigos, PEER REVIEW PROCEEDINGS, sumarios
	echo "<div id=\"dialog-form-pubManualInt-Art\" title=\"Editar Dados Publicações Manuais\" hidden>
			<p class=\"validateTips-pubManualInt-Art\"></p>
				<form>
			    <fieldset id = \"edit\">
				    <label for=\"issn-pubManualInt-Art\">ISSN</label>
				    <input type=\"text\" name=\"issn-pubManualInt-Art\" id=\"issn-pubManualInt-Art\" class=\"text ui-widget-content ui-corner-all\" />

		  	  		<label for=\"revista-pubManualInt-Art\">Revista</label>
				    <input type=\"text\" name=\"revista-pubManualInt-Art\" id=\"revista-pubManualInt-Art\" class=\"text ui-widget-content ui-corner-all\" />	

				    <label for=\"titulo-pubManualInt-Art\">Título</label>				    
				    <input type=\"text\" name=\"titulo-pubManualInt-Art\" id=\"titulo-pubManualInt-Art\" class=\"text ui-widget-content ui-corner-all\" />

				    <label for=\"autores-pubManualInt-Art\">Autores</label>				    
				    <input type=\"text\" name=\"autores-pubManualInt-Art\" id=\"autores-pubManualInt-Art\" class=\"text ui-widget-content ui-corner-all\" />	

				    <label for=\"volume-pubManualInt-Art\">Volume</label>				    
				    <input type=\"text\" name=\"volume-pubManualInt-Art\" id=\"volume-pubManualInt-Art\" class=\"text ui-widget-content ui-corner-all\" />

				    <label for=\"issue-pubManualInt-Art\">Issue</label>				    
				    <input type=\"text\" name=\"issue-pubManualInt-Art\" id=\"issue-pubManualInt-Art\" class=\"text ui-widget-content ui-corner-all\" />	

				    <label for=\"prim-pubManualInt-Art\">Primeira Página</label>				    
				    <input type=\"text\" name=\"prim-pubManualInt-Art\" id=\"prim-pubManualInt-Art\" class=\"text ui-widget-content ui-corner-all\" />

				    <label for=\"ult-pubManualInt-Art\">Última Página</label>				    
				    <input type=\"text\" name=\"ult-pubManualInt-Art\" id=\"ult-pubManualInt-Art\" class=\"text ui-widget-content ui-corner-all\" />

			     	<label for=\"link-pubManualInt-Art\">Link</label>				    
				    <textarea name='link-pubManualInt-Art' id='link-pubManualInt-Art' style='height: 75px; width: 500px'></textarea></td>		

	                <label for=\"estado-pubManualInt-Art\">Estado</label>
				    <SELECT id='estado-pubManualInt-Art' name='estado-pubManualInt-Art'>\n";
					getEstadoPubEdit();	
		echo "</fieldset>
	  		</form>
	 </div>";
	
	echo "<div id=\"dialog-form-pubManualNac-Rev\" title=\"Editar Dados Publicações Manuais\" hidden>
			<p class=\"validateTips-pubManualNac-Rev\"></p>
				<form>
			    <fieldset id = \"edit\">
				    <label for=\"issn-pubManualNac-Rev\">ISSN</label>
				    <input type=\"text\" name=\"issn-pubManualNac-Rev\" id=\"issn-pubManualNac-Rev\" class=\"text ui-widget-content ui-corner-all\" />

		  	  		<label for=\"revista-pubManualNac-Rev\">Revista</label>
				    <input type=\"text\" name=\"revista-pubManualNac-Rev\" id=\"revista-pubManualNac-Rev\" class=\"text ui-widget-content ui-corner-all\" />	

				    <label for=\"titulo-pubManualNac-Rev\">Título</label>				    
				    <input type=\"text\" name=\"titulo-pubManualNac-Rev\" id=\"titulo-pubManualNac-Rev\" class=\"text ui-widget-content ui-corner-all\" />

				    <label for=\"autores-pubManualNac-Rev\">Autores</label>				    
				    <input type=\"text\" name=\"autores-pubManualNac-Rev\" id=\"autores-pubManualNac-Rev\" class=\"text ui-widget-content ui-corner-all\" />	

				    <label for=\"volume-pubManualNac-Rev\">Volume</label>				    
				    <input type=\"text\" name=\"volume-pubManualNac-Rev\" id=\"volume-pubManualNac-Rev\" class=\"text ui-widget-content ui-corner-all\" />

				    <label for=\"issue-pubManualNac-Rev\">Issue</label>				    
				    <input type=\"text\" name=\"issue-pubManualNac-Rev\" id=\"issue-pubManualNac-Rev\" class=\"text ui-widget-content ui-corner-all\" />	

				    <label for=\"prim-pubManualNac-Rev\">Primeira Página</label>				    
				    <input type=\"text\" name=\"prim-pubManualNac-Rev\" id=\"prim-pubManualNac-Rev\" class=\"text ui-widget-content ui-corner-all\" />

				    <label for=\"ult-pubManualNac-Rev\">Última Página</label>				    
				    <input type=\"text\" name=\"ult-pubManualNac-Rev\" id=\"ult-pubManualNac-Rev\" class=\"text ui-widget-content ui-corner-all\" />

			     	<label for=\"link-pubManualNac-Rev\">Link</label>				    
				    <textarea name='link-pubManualNac-Rev' id='link-pubManualNac-Rev' style='height: 75px; width: 500px'></textarea></td>		

	                <label for=\"estado-pubManualNac-Rev\">Estado</label>
				    <SELECT id='estado-pubManualNac-Rev' name='estado-pubManualInt-Art'>\n";
					getEstadoPubEdit();	
		echo "</fieldset>
	  		</form>
	 </div>";
	 
	 echo "<div id=\"dialog-form-pubManualNac-Art\" title=\"Editar Dados Publicações Manuais\" hidden>
			<p class=\"validateTips-pubManualNac-Art\"></p>
				<form>
			    <fieldset id = \"edit\">
				    <label for=\"issn-pubManualNac-Art\">ISSN</label>
				    <input type=\"text\" name=\"issn-pubManualNac-Art\" id=\"issn-pubManualNac-Art\" class=\"text ui-widget-content ui-corner-all\" />

		  	  		<label for=\"revista-pubManualNac-Art\">Revista</label>
				    <input type=\"text\" name=\"revista-pubManualNac-Art\" id=\"revista-pubManualNac-Art\" class=\"text ui-widget-content ui-corner-all\" />	

				    <label for=\"titulo-pubManualNac-Art\">Título</label>				    
				    <input type=\"text\" name=\"titulo-pubManualNac-Art\" id=\"titulo-pubManualNac-Art\" class=\"text ui-widget-content ui-corner-all\" />

				    <label for=\"autores-pubManualNac-Art\">Autores</label>				    
				    <input type=\"text\" name=\"autores-pubManualNac-Art\" id=\"autores-pubManualNac-Art\" class=\"text ui-widget-content ui-corner-all\" />	

				    <label for=\"volume-pubManualNac-Art\">Volume</label>				    
				    <input type=\"text\" name=\"volume-pubManualNac-Art\" id=\"volume-pubManualNac-Art\" class=\"text ui-widget-content ui-corner-all\" />

				    <label for=\"issue-pubManualNac-Art\">Issue</label>				    
				    <input type=\"text\" name=\"issue-pubManualNac-Art\" id=\"issue-pubManualNac-Art\" class=\"text ui-widget-content ui-corner-all\" />	

				    <label for=\"prim-pubManualNac-Art\">Primeira Página</label>				    
				    <input type=\"text\" name=\"prim-pubManualNac-Art\" id=\"prim-pubManualNac-Art\" class=\"text ui-widget-content ui-corner-all\" />

				    <label for=\"ult-pubManualNac-Art\">Última Página</label>				    
				    <input type=\"text\" name=\"ult-pubManualNac-Art\" id=\"ult-pubManualNac-Art\" class=\"text ui-widget-content ui-corner-all\" />

			     	<label for=\"link-pubManualNac-Art\">Link</label>				    
				    <textarea name='link-pubManualNac-Art' id='link-pubManualNac-Art' style='height: 75px; width: 500px'></textarea></td>		

	                <label for=\"estado-pubManualNac-Art\">Estado</label>
				    <SELECT id='estado-pubManualNac-Art' name='estado-pubManualNac-Art'>\n";
					getEstadoPubEdit();	
		echo "</fieldset>
	  		</form>
	 </div>";
	 
	 echo "<div id=\"dialog-form-pubManualNac-Sum\" title=\"Editar Dados Publicações Manuais\" hidden>
			<p class=\"validateTips-pubManualNac-Sum\"></p>
				<form>
			    <fieldset id = \"edit\">
				    <label for=\"issn-pubManualNac-Sum\">ISSN</label>
				    <input type=\"text\" name=\"issn-pubManualNac-Sum\" id=\"issn-pubManualNac-Sum\" class=\"text ui-widget-content ui-corner-all\" />

		  	  		<label for=\"revista-pubManualNac-Sum\">Revista</label>
				    <input type=\"text\" name=\"revista-pubManualNac-Sum\" id=\"revista-pubManualNac-Sum\" class=\"text ui-widget-content ui-corner-all\" />	

				    <label for=\"titulo-pubManualNac-Sum\">Título</label>				    
				    <input type=\"text\" name=\"titulo-pubManualNac-Sum\" id=\"titulo-pubManualNac-Sum\" class=\"text ui-widget-content ui-corner-all\" />

				    <label for=\"autores-pubManualNac-Sum\">Autores</label>				    
				    <input type=\"text\" name=\"autores-pubManualNac-Sum\" id=\"autores-pubManualNac-Sum\" class=\"text ui-widget-content ui-corner-all\" />	

				    <label for=\"volume-pubManualNac-Sum\">Volume</label>				    
				    <input type=\"text\" name=\"volume-pubManualNac-Sum\" id=\"volume-pubManualNac-Sum\" class=\"text ui-widget-content ui-corner-all\" />

				    <label for=\"issue-pubManualNac-Sum\">Issue</label>				    
				    <input type=\"text\" name=\"issue-pubManualNac-Sum\" id=\"issue-pubManualNac-Sum\" class=\"text ui-widget-content ui-corner-all\" />	

				    <label for=\"prim-pubManualNac-Sum\">Primeira Página</label>				    
				    <input type=\"text\" name=\"prim-pubManualNac-Sum\" id=\"prim-pubManualNac-Sum\" class=\"text ui-widget-content ui-corner-all\" />

				    <label for=\"ult-pubManualNac-Sum\">Última Página</label>				    
				    <input type=\"text\" name=\"ult-pubManualNac-Sum\" id=\"ult-pubManualNac-Sum\" class=\"text ui-widget-content ui-corner-all\" />

			     	<label for=\"link-pubManualNac-Sum\">Link</label>				    
				    <textarea name='link-pubManualNac-Sum' id='link-pubManualNac-Sum' style='height: 75px; width: 500px'></textarea></td>		

	                <label for=\"estado-pubManualNac-Sum\">Estado</label>
				    <SELECT id='estado-pubManualNac-Sum' name='estado-pubManualNac-Sum'>\n";
					getEstadoPubEdit();	
		echo "</fieldset>
	  		</form>
	 </div>";

	 //Editar Publicações Manuais Internacional - Livros
	echo "<div id=\"dialog-form-pubManualInt-Liv\" title=\"Editar Dados Publicações Manuais - Livros\" hidden>
			<p class=\"validateTips-pubManualInt-Liv\"></p>
				<form>
			    <fieldset id = \"edit\">
				    <label for=\"isbn-pubManualInt-Liv\">ISBN</label>
				    <input type=\"text\" name=\"isbn-pubManualInt-Liv\" id=\"isbn-pubManualInt-Liv\" class=\"text ui-widget-content ui-corner-all\" /

				    <label for=\"titulo-pubManualInt-Liv\">Título</label>				    
				    <input type=\"text\" name=\"titulo-pubManualInt-Liv\" id=\"titulo-pubManualInt-Liv\" class=\"text ui-widget-content ui-corner-all\" />

				    <label for=\"autores-pubManualInt-Liv\">Autores</label>				    
				    <input type=\"text\" name=\"autores-pubManualInt-Liv\" id=\"autores-pubManualInt-Liv\" class=\"text ui-widget-content ui-corner-all\" />	

				    <label for=\"editor-pubManualInt-Liv\">Editor</label>				    
				    <input type=\"text\" name=\"editor-pubManualInt-Liv\" id=\"editor-pubManualInt-Liv\" class=\"text ui-widget-content ui-corner-all\" />

				    <label for=\"editora-pubManualInt-Liv\">Editora</label>				    
				    <input type=\"text\" name=\"editora-pubManualInt-Liv\" id=\"editora-pubManualInt-Liv\" class=\"text ui-widget-content ui-corner-all\" />	

				    <label for=\"link-pubManualInt-Liv\">Link</label>				    
				    <textarea name='link-pubManualInt-Liv' id='link-pubManualInt-Liv' style='height: 75px; width: 500px'></textarea></td>		

	                <label for=\"estado-pubManualInt-Liv\">Estado</label>
				    <SELECT id='estado-pubManualInt-Liv' name='estado-pubManualInt-Liv'>\n";
					getEstadoPubEdit();	
		echo "</fieldset>
	  		</form>
	 </div>";
	 
	 //Editar Publicações Manuais Nacional - Livros
	echo "<div id=\"dialog-form-pubManualNac-Liv\" title=\"Editar Dados Publicações Manuais - Livros\" hidden>
			<p class=\"validateTips-pubManualNac-Liv\"></p>
				<form>
			    <fieldset id = \"edit\">
				    <label for=\"isbn-pubManualNac-Liv\">ISBN</label>
				    <input type=\"text\" name=\"isbn-pubManualNac-Liv\" id=\"isbn-pubManualNac-Liv\" class=\"text ui-widget-content ui-corner-all\" /

				    <label for=\"titulo-pubManualNac-Liv\">Título</label>				    
				    <input type=\"text\" name=\"titulo-pubManualNac-Liv\" id=\"titulo-pubManualNac-Liv\" class=\"text ui-widget-content ui-corner-all\" />

				    <label for=\"autores-pubManualNac-Liv\">Autores</label>				    
				    <input type=\"text\" name=\"autores-pubManualNac-Liv\" id=\"autores-pubManualNac-Liv\" class=\"text ui-widget-content ui-corner-all\" />	

				    <label for=\"editor-pubManualNac-Liv\">Editor</label>				    
				    <input type=\"text\" name=\"editor-pubManualNac-Liv\" id=\"editor-pubManualNac-Liv\" class=\"text ui-widget-content ui-corner-all\" />

				    <label for=\"editora-pubManualNac-Liv\">Editora</label>				    
				    <input type=\"text\" name=\"editora-pubManualNac-Liv\" id=\"editora-pubManualNac-Liv\" class=\"text ui-widget-content ui-corner-all\" />	

				    <label for=\"link-pubManualNac-Liv\">Link</label>				    
				    <textarea name='link-pubManualNac-Liv' id='link-pubManualNac-Liv' style='height: 75px; width: 500px'></textarea></td>		

	                <label for=\"estado-pubManualNac-Liv\">Estado</label>
				    <SELECT id='estado-pubManualNac-Liv' name='estado-pubManualNac-Liv'>\n";
					getEstadoPubEdit();	
		echo "</fieldset>
	  		</form>
	 </div>";

	 //Editar Publicações Manuais Internacional - Capitulos de Livros
	echo "<div id=\"dialog-form-pubManualInt-CLiv\" title=\"Editar Dados Publicações Manuais - Capitulos de Livros\" hidden>
			<p class=\"validateTips-pubManualInt-CLiv\"></p>
				<form>
			    <fieldset id = \"edit\">
				    <label for=\"isbn-pubManualInt-CLiv\">ISBN</label>
				    <input type=\"text\" name=\"isbn-pubManualInt-CLiv\" id=\"isbn-pubManualInt-CLiv\" class=\"text ui-widget-content ui-corner-all\" /

				    <label for=\"nomepub-pubManualInt-CLiv\">Título da Obra</label>				    
				    <input type=\"text\" name=\"nomepub-pubManualInt-CLiv\" id=\"nomepub-pubManualInt-CLiv\" class=\"text ui-widget-content ui-corner-all\" />

				    <label for=\"editora-pubManualInt-CLiv\">Editora</label>				    
				    <input type=\"text\" name=\"editora-pubManualInt-CLiv\" id=\"editora-pubManualInt-CLiv\" class=\"text ui-widget-content ui-corner-all\" />	

				    <label for=\"editor-pubManualInt-CLiv\">Editores</label>				    
				    <input type=\"text\" name=\"editor-pubManualInt-CLiv\" id=\"editor-pubManualInt-CLiv\" class=\"text ui-widget-content ui-corner-all\" />

				    <label for=\"titulo-pubManualInt-CLiv\">Título do Artigo</label>				    
				    <input type=\"text\" name=\"titulo-pubManualInt-CLiv\" id=\"titulo-pubManualInt-CLiv\" class=\"text ui-widget-content ui-corner-all\" />

				    <label for=\"autores-pubManualInt-CLiv\">Autores</label>				    
				    <input type=\"text\" name=\"autores-pubManualInt-CLiv\" id=\"autores-pubManualInt-CLiv\" class=\"text ui-widget-content ui-corner-all\" />		 				   

				    <label for=\"prim-pubManualInt-CLiv\">Primeira Página</label>				    
				    <input type=\"text\" name=\"prim-pubManualInt-CLiv\" id=\"prim-pubManualInt-CLiv\" class=\"text ui-widget-content ui-corner-all\" />

				    <label for=\"ult-pubManualInt-CLiv\">Última Página</label>				    
				    <input type=\"text\" name=\"ult-pubManualInt-CLiv\" id=\"ult-pubManualInt-CLiv\" class=\"text ui-widget-content ui-corner-all\" />

				    <label for=\"link-pubManualInt-CLiv\">Link</label>				    
				    <textarea name='link-pubManualInt-CLiv' id='link-pubManualInt-CLiv' style='height: 75px; width: 500px'></textarea></td>		

	                <label for=\"estado-pubManualInt-CLiv\">Estado</label>
				    <SELECT id='estado-pubManualInt-CLiv' name='estado-pubManualInt-CLiv'>\n";
					getEstadoPubEdit();	
		echo "</fieldset>
	  		</form>
	 </div>";
	 
	echo "<div id=\"dialog-form-pubManualNac-CLiv\" title=\"Editar Dados Publicações Manuais - Capitulos de Livros\" hidden>
			<p class=\"validateTips-pubManualNac-CLiv\"></p>
				<form>
			    <fieldset id = \"edit\">
				    <label for=\"isbn-pubManualNac-CLiv\">ISBN</label>
				    <input type=\"text\" name=\"isbn-pubManualNac-CLiv\" id=\"isbn-pubManualNac-CLiv\" class=\"text ui-widget-content ui-corner-all\" /

				    <label for=\"nomepub-pubManualNac-CLiv\">Título da Obra</label>				    
				    <input type=\"text\" name=\"nomepub-pubManualNac-CLiv\" id=\"nomepub-pubManualNac-CLiv\" class=\"text ui-widget-content ui-corner-all\" />

				    <label for=\"editora-pubManualNac-CLiv\">Editora</label>				    
				    <input type=\"text\" name=\"editora-pubManualNac-CLiv\" id=\"editora-pubManualNac-CLiv\" class=\"text ui-widget-content ui-corner-all\" />	

				    <label for=\"editor-pubManualNac-CLiv\">Editores</label>				    
				    <input type=\"text\" name=\"editor-pubManualNac-CLiv\" id=\"editor-pubManualNac-CLiv\" class=\"text ui-widget-content ui-corner-all\" />

				    <label for=\"titulo-pubManualNac-CLiv\">Título do Artigo</label>				    
				    <input type=\"text\" name=\"titulo-pubManualNac-CLiv\" id=\"titulo-pubManualNac-CLiv\" class=\"text ui-widget-content ui-corner-all\" />

				    <label for=\"autores-pubManualNac-CLiv\">Autores</label>				    
				    <input type=\"text\" name=\"autores-pubManualNac-CLiv\" id=\"autores-pubManualNac-CLiv\" class=\"text ui-widget-content ui-corner-all\" />		 				   

				    <label for=\"prim-pubManualNac-CLiv\">Primeira Página</label>				    
				    <input type=\"text\" name=\"prim-pubManualNac-CLiv\" id=\"prim-pubManualNac-CLiv\" class=\"text ui-widget-content ui-corner-all\" />

				    <label for=\"ult-pubManualNac-CLiv\">Última Página</label>				    
				    <input type=\"text\" name=\"ult-pubManualNac-CLiv\" id=\"ult-pubManualNac-CLiv\" class=\"text ui-widget-content ui-corner-all\" />

				    <label for=\"link-pubManualNac-CLiv\">Link</label>				    
				    <textarea name='link-pubManualNac-CLiv' id='link-pubManualNac-CLiv' style='height: 75px; width: 500px'></textarea></td>		

	                <label for=\"estado-pubManualNac-CLiv\">Estado</label>
				    <SELECT id='estado-pubManualNac-CLiv' name='estado-pubManualNac-CLiv'>\n";
					getEstadoPubEdit();	
		echo "</fieldset>
	  		</form>
	 </div>";
	 
	 //Editar Publicações Manuais Internacional - Sumarios
    echo "<div id=\"dialog-form-pubManualInt-Sum\" title=\"Editar Dados Publicações Manuais - Sumários\" hidden>
			<p class=\"validateTips-pubManualInt-Sum\"></p>
				<form>
			    <fieldset id = \"edit\">
				    <label for=\"issn-pubManualInt-Sum\">ISSN</label>
				    <input type=\"text\" name=\"issn-pubManualInt-Sum\" id=\"issn-pubManualInt-Sum\" class=\"text ui-widget-content ui-corner-all\" />

		  	  		<label for=\"revista-pubManualInt-Sum\">Revista</label>
				    <input type=\"text\" name=\"revista-pubManualInt-Sum\" id=\"revista-pubManualInt-Sum\" class=\"text ui-widget-content ui-corner-all\" />	

				    <label for=\"titulo-pubManualInt-Sum\">Título</label>				    
				    <input type=\"text\" name=\"titulo-pubManualInt-Sum\" id=\"titulo-pubManualInt-Sum\" class=\"text ui-widget-content ui-corner-all\" />

				    <label for=\"autores-pubManualInt-Sum\">Autores</label>				    
				    <input type=\"text\" name=\"autores-pubManualInt-Sum\" id=\"autores-pubManualInt-Sum\" class=\"text ui-widget-content ui-corner-all\" />	

				    <label for=\"volume-pubManualInt-Sum\">Volume</label>				    
				    <input type=\"text\" name=\"volume-pubManualInt-Sum\" id=\"volume-pubManualInt-Sum\" class=\"text ui-widget-content ui-corner-all\" />

				    <label for=\"issue-pubManualInt-Sum\">Issue</label>				    
				    <input type=\"text\" name=\"issue-pubManualInt-Sum\" id=\"issue-pubManualInt-Sum\" class=\"text ui-widget-content ui-corner-all\" />	

				    <label for=\"prim-pubManualInt-Sum\">Primeira Página</label>				    
				    <input type=\"text\" name=\"prim-pubManualInt-Sum\" id=\"prim-pubManualInt-Sum\" class=\"text ui-widget-content ui-corner-all\" />

				    <label for=\"ult-pubManualInt-Sum\">Última Página</label>				    
				    <input type=\"text\" name=\"ult-pubManualInt-Sum\" id=\"ult-pubManualInt-Sum\" class=\"text ui-widget-content ui-corner-all\" />

			     	<label for=\"link-pubManualInt-Sum\">Link</label>				    
				    <textarea name='link-pubManualInt-Sum' id='link-pubManualInt-Sum' style='height: 75px; width: 500px'></textarea></td>		

	                <label for=\"estado-pubManualInt-Sum\">Estado</label>
				    <SELECT id='estado-pubManualInt-Sum' name='estado-pubManualInt-Sum'>\n";
					getEstadoPubEdit();	
		echo "</fieldset>
	  		</form>
	 </div>";

//Editar Publicações Manuais Internacional - Conference Proceedings 
    echo "<div id=\"dialog-form-pubManualInt-Con\" title=\"Editar Dados Publicações Manuais - CONFERENCE PROCEEDINGS\" hidden>
                <p class=\"validateTips-pubManualInt-Con\"></p>
                    <form>
                    <fieldset id = \"edit\">
                        <label for=\"isbn-pubManualInt-Con\">ISBN</label>
                        <input type=\"text\" name=\"isbn-pubManualInt-Con\" id=\"isbn-pubManualInt-Con\" class=\"text ui-widget-content ui-corner-all\" /

                        <label for=\"nomepub-pubManualInt-Con\">Título da Obra</label>
                        <input type=\"text\" name=\"nomepub-pubManualInt-Con\" id=\"nomepub-pubManualInt-Con\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"editor-pubManualInt-Con\">Editores</label>
                        <input type=\"text\" name=\"editor-pubManualInt-Con\" id=\"editor-pubManualInt-Con\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"titulo-pubManualInt-Con\">Título do Artigo</label>
                        <input type=\"text\" name=\"titulo-pubManualInt-Con\" id=\"titulo-pubManualInt-Con\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"autores-pubManualInt-Con\">Autores</label>
                        <input type=\"text\" name=\"autores-pubManualInt-Con\" id=\"autores-pubManualInt-Con\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"prim-pubManualInt-Con\">Primeira Página</label>
                        <input type=\"text\" name=\"prim-pubManualInt-Con\" id=\"prim-pubManualInt-Con\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"ult-pubManualInt-Con\">Última Página</label>
                        <input type=\"text\" name=\"ult-pubManualInt-Con\" id=\"ult-pubManualInt-Con\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"link-pubManualInt-Con\">Link</label>
                        <textarea name='link-pubManualInt-Con' id='link-pubManualInt-Con' style='height: 75px; width: 500px'></textarea></td>

                        <label for=\"estado-pubManualInt-Con\">Estado</label>
                        <SELECT id='estado-pubManualInt-Con' name='estado-pubManualInt-Con'>\n";
                        getEstadoPubEdit();
            echo "</fieldset>
           	     </form>
         </div>";
		 
	echo "<div id=\"dialog-form-pubManualNac-Oth\" title=\"Editar Dados Publicações Manuais - Outros Sumários\" hidden>
                <p class=\"validateTips-pubManualNac-Oth\"></p>
                    <form>
                    <fieldset id = \"edit\">
                        <label for=\"isbn-pubManualNac-Oth\">ISBN</label>
                        <input type=\"text\" name=\"isbn-pubManualNac-Oth\" id=\"isbn-pubManualNac-Oth\" class=\"text ui-widget-content ui-corner-all\" /

                        <label for=\"nomepub-pubManualNac-Oth\">Título da Obra</label>
                        <input type=\"text\" name=\"nomepub-pubManualNac-Oth\" id=\"nomepub-pubManualNac-Oth\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"editor-pubManualNac-Oth\">Editores</label>
                        <input type=\"text\" name=\"editor-pubManualNac-Oth\" id=\"editor-pubManualNac-Oth\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"titulo-pubManualNac-Oth\">Título do Artigo</label>
                        <input type=\"text\" name=\"titulo-pubManualNac-Oth\" id=\"titulo-pubManualNac-Oth\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"autores-pubManualNac-Oth\">Autores</label>
                        <input type=\"text\" name=\"autores-pubManualNac-Oth\" id=\"autores-pubManualNac-Oth\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"prim-pubManualNac-Oth\">Primeira Página</label>
                        <input type=\"text\" name=\"prim-pubManualNac-Oth\" id=\"prim-pubManualNac-Oth\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"ult-pubManualNac-Oth\">Última Página</label>
                        <input type=\"text\" name=\"ult-pubManualNac-Oth\" id=\"ult-pubManualNac-Oth\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"link-pubManualNac-Oth\">Link</label>
                        <textarea name='link-pubManualNac-Oth' id='link-pubManualNac-Oth' style='height: 75px; width: 500px'></textarea></td>

                        <label for=\"estado-pubManualNac-Oth\">Estado</label>
                        <SELECT id='estado-pubManualNac-Oth' name='estado-pubManualNac-Oth'>\n";
                        getEstadoPubEdit();
            echo "</fieldset>
           	     </form>
         </div>";

//Editar Publicações Manuais Internacional - Outros sumários		 
	 echo "<div id=\"dialog-form-pubManualInt-Oth\" title=\"Editar Dados Publicações Manuais - Outros Sumários\" hidden>
                <p class=\"validateTips-pubManualInt-Oth\"></p>
                    <form>
                    <fieldset id = \"edit\">
                        <label for=\"isbn-pubManualInt-Oth\">ISBN</label>
                        <input type=\"text\" name=\"isbn-pubManualInt-Oth\" id=\"isbn-pubManualInt-Oth\" class=\"text ui-widget-Othtent ui-corner-all\" /

                        <label for=\"nomepub-pubManualInt-Oth\">Título da Obra</label>
                        <input type=\"text\" name=\"nomepub-pubManualInt-Oth\" id=\"nomepub-pubManualInt-Oth\" class=\"text ui-widget-Othtent ui-corner-all\" />

                        <label for=\"editor-pubManualInt-Oth\">Editores</label>
                        <input type=\"text\" name=\"editor-pubManualInt-Oth\" id=\"editor-pubManualInt-Oth\" class=\"text ui-widget-Othtent ui-corner-all\" />

                        <label for=\"titulo-pubManualInt-Oth\">Título do Artigo</label>
                        <input type=\"text\" name=\"titulo-pubManualInt-Oth\" id=\"titulo-pubManualInt-Oth\" class=\"text ui-widget-Othtent ui-corner-all\" />

                        <label for=\"autores-pubManualInt-Oth\">Autores</label>
                        <input type=\"text\" name=\"autores-pubManualInt-Oth\" id=\"autores-pubManualInt-Oth\" class=\"text ui-widget-Othtent ui-corner-all\" />

                        <label for=\"prim-pubManualInt-Oth\">Primeira Página</label>
                        <input type=\"text\" name=\"prim-pubManualInt-Oth\" id=\"prim-pubManualInt-Oth\" class=\"text ui-widget-Othtent ui-corner-all\" />

                        <label for=\"ult-pubManualInt-Oth\">Última Página</label>
                        <input type=\"text\" name=\"ult-pubManualInt-Oth\" id=\"ult-pubManualInt-Oth\" class=\"text ui-widget-Othtent ui-corner-all\" />

                        <label for=\"link-pubManualInt-Oth\">Link</label>
                        <textarea name='link-pubManualInt-Oth' id='link-pubManualInt-Oth' style='height: 75px; width: 500px'></textarea></td>

                        <label for=\"estado-pubManualInt-Oth\">Estado</label>
                        <SELECT id='estado-pubManualInt-Oth' name='estado-pubManualInt-Oth'>\n";
                        getEstadoPubEdit();
            echo "</fieldset>
           	     </form>
         </div>";
		 
	//Editar Publicações Manuais Internacional - Conference Proceedings 
    echo "<div id=\"dialog-form-pubManualNac-Con\" title=\"Editar Dados Publicações Manuais - CONFERENCE PROCEEDINGS\" hidden>
                <p class=\"validateTips-pubManualNac-Con\"></p>
                    <form>
                    <fieldset id = \"edit\">
                        <label for=\"isbn-pubManualNac-Con\">ISBN</label>
                        <input type=\"text\" name=\"isbn-pubManualNac-Con\" id=\"isbn-pubManualNac-Con\" class=\"text ui-widget-content ui-corner-all\" /

                        <label for=\"nomepub-pubManualNac-Con\">Título da Obra</label>
                        <input type=\"text\" name=\"nomepub-pubManualNac-Con\" id=\"nomepub-pubManualNac-Con\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"editor-pubManualNac-Con\">Editores</label>
                        <input type=\"text\" name=\"editor-pubManualNac-Con\" id=\"editor-pubManualNac-Con\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"titulo-pubManualNac-Con\">Título do Artigo</label>
                        <input type=\"text\" name=\"titulo-pubManualNac-Con\" id=\"titulo-pubManualNac-Con\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"autores-pubManualNac-Con\">Autores</label>
                        <input type=\"text\" name=\"autores-pubManualNac-Con\" id=\"autores-pubManualNac-Con\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"prim-pubManualNac-Con\">Primeira Página</label>
                        <input type=\"text\" name=\"prim-pubManualNac-Con\" id=\"prim-pubManualNac-Con\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"ult-pubManualNac-Con\">Última Página</label>
                        <input type=\"text\" name=\"ult-pubManualNac-Con\" id=\"ult-pubManualNac-Con\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"link-pubManualNac-Con\">Link</label>
                        <textarea name='link-pubManualNac-Con' id='link-pubManualNac-Con' style='height: 75px; width: 500px'></textarea></td>

                        <label for=\"estado-pubManualNac-Con\">Estado</label>
                        <SELECT id='estado-pubManualNac-Con' name='estado-pubManualNac-Con'>\n";
                        getEstadoPubEdit();
            echo "</fieldset>
           	     </form>
         </div>";

	//Editar Projectos
    echo "<div id=\"dialog-form-projectos\" title=\"Editar Projectos\" hidden>
                <p class=\"validateTips-projectos\"></p>
				<p style='color:red'>Se por acaso quando terminar a edição e clicar em \"Confirmar\" a sua ação não for validada, verifique se o campo \"Estado\" está preenchido.<p>
                    <form>
                    <fieldset id = \"edit\">
                        <label for=\"tipo-projectos\">Tipo/Entidade</label>                        
                       	<SELECT id='tipo-projectos' name='tipo-projectos'>\n";
                       	getEntidadeFinProjEdit();

                  echo "<br><br><label for=\"entidade-projectos\">Entidade Financiadora</label>
                        <input type=\"text\" name=\"entidade-projectos\" id=\"entidade-projectos\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"inst-projectos\">Instituição de Acolhimento</label>
                        <input type=\"text\" name=\"inst-projectos\" id=\"inst-projectos\" class=\"text ui-widget-content ui-corner-all\" />
                       
                        <label for=\"monsol-projectos\">Montante Total Solicitado (Em euros)</label>
                        <input type=\"text\" name=\"monsol-projectos\" id=\"monsol-projectos\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"monapr-projectos\">Montante Total Aprovado (Em euros)</label>
                        <input type=\"text\" name=\"monapr-projectos\" id=\"monapr-projectos\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"monfmup-projectos\">Montante atribuído à FMUP (Em euros)</label>
                        <input type=\"text\" name=\"monfmup-projectos\" id=\"monfmup-projectos\" class=\"text ui-widget-content ui-corner-all\" />

	                    <label for=\"resp-projectos\">É Investigador Responsável?</label>
					    <SELECT id='resp-projectos' name='resp-projectos' >\n
							<option>Sim</option>\n	
							<option>Não</option>\n		
						</SELECT>\n	

                        <br><br><label for=\"codigo-projectos\">Código/Referência</label>
                        <input type=\"text\" name=\"codigo-projectos\" id=\"codigo-projectos\" class=\"text ui-widget-content ui-corner-all\" />

                         <label for=\"titulo-projectos\">Título</label>
                        <input type=\"text\" name=\"titulo-projectos\" id=\"titulo-projectos\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"dataini-projectos\">Data de Início</label>
                        <input type=\"text\" name=\"dataini-projectos\" id=\"dataini-projectos\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"datafim-projectos\">Data de Fim</label>
                        <input type=\"text\" name=\"datafim-projectos\" id=\"datafim-projectos\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"link-projectos\">Link</label>
                        <textarea name='link-projectos' id='link-projectos' style='height: 75px; width: 500px'></textarea></td><br>

                        <label for=\"estado-projectos\">Estado</label>
                        <SELECT id='estado-projectos' name='estado-projectos'>\n";
                        getEstadoProjEdit();
            echo "</fieldset>
           	     </form>
         </div>";   

    //Editar Arbitragens Projectos
    echo "<div id=\"dialog-form-arbprojectos\" title=\"Editar Arbitragens Projectos\" hidden>
                <p class=\"validateTips-arbprojectos\"></p>
                    <form>
                    <fieldset id = \"edit\">  
                        <label for=\"api-arbprojectos\">Internacionais</label>
                        <input type=\"text\" name=\"api-arbprojectos\" id=\"api-arbprojectos\" class=\"text ui-widget-content ui-corner-all\" />
                       
                        <label for=\"apn-arbprojectos\">Nacionais</label>
                        <input type=\"text\" name=\"apn-arbprojectos\" id=\"apn-arbprojectos\" class=\"text ui-widget-content ui-corner-all\" />
                    </fieldset>
           	     </form>
         </div>";   

    //Editar Conferencias
    echo "<div id=\"dialog-form-conferencias\" title=\"Editar Conferências\" hidden>
                <p class=\"validateTips-conferencias\"></p>
                    <form>
                    <fieldset id = \"edit\">
                        <label for=\"ambito-conferencias\">Âmbito</label>                        
                        <SELECT id='ambito-conferencias' name='ambito-conferencias'>\n";
                        getAmbitoConfEdit();

           echo"<br><br><label for=\"dataini-conferencias\">Data de Início</label>
                        <input type=\"text\" name=\"dataini-conferencias\" id=\"dataini-conferencias\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"datafim-conferencias\">Data de Fim</label>
                        <input type=\"text\" name=\"datafim-conferencias\" id=\"datafim-conferencias\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"titulo-conferencias\">Título</label>
                        <input type=\"text\" name=\"titulo-conferencias\" id=\"titulo-conferencias\" class=\"text ui-widget-content ui-corner-all\" />

                      	<label for=\"local-conferencias\">Local (cidade,pais)</label>
                        <input type=\"text\" name=\"local-conferencias\" id=\"local-conferencias\" class=\"text ui-widget-content ui-corner-all\" />                      	

                        <label for=\"npart-conferencias\">Nº Participantes</label>
                        <input type=\"text\" name=\"npart-conferencias\" id=\"npart-conferencias\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"link-conferencias\">Link</label>
                        <textarea name='link-conferencias' id='link-conferencias' style='height: 75px; width: 500px'></textarea></td>
				</fieldset>
           	    </form>
         </div>";  

	//Editar Apresentacoes
    echo "<div id=\"dialog-form-apresentacoes\" title=\"Editar Apresentações\" hidden>
                <p class=\"validateTips-apresentacoes\"></p>
                    <form>
                    <fieldset id = \"edit\">
                    	<p><b>Internacionais</b></p><br>
                        <label for=\"iconfconv-apresentacoes\">Apresentações por convite</label>
                        <input type=\"text\" name=\"iconfconv-apresentacoes\" id=\"iconfconv-apresentacoes\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"iconfpart-apresentacoes\">Apresentações como participante</label>
                        <input type=\"text\" name=\"iconfpart-apresentacoes\" id=\"iconfpart-apresentacoes\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"isem-apresentacoes\">Seminários Apresentados</label>
                        <input type=\"text\" name=\"isem-apresentacoes\" id=\"isem-apresentacoes\" class=\"text ui-widget-content ui-corner-all\" />

                        <br><div class='ui-dialog-buttonpane ui-widget-content ui-helper-clearfix'/><br>

                        <p><b>Nacionais</b></p><br>
                      	<label for=\"nconfconv-apresentacoes\">Apresentações por convite</label>
                        <input type=\"text\" name=\"nconfconv-apresentacoes\" id=\"nconfconv-apresentacoes\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"nconfpart-apresentacoes\">Apresentações como participante</label>
                        <input type=\"text\" name=\"nconfpart-apresentacoes\" id=\"nconfpart-apresentacoes\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"nsem-apresentacoes\">Seminários Apresentados</label>
                        <input type=\"text\" name=\"nsem-apresentacoes\" id=\"nsem-apresentacoes\" class=\"text ui-widget-content ui-corner-all\" />
				</fieldset>
           	    </form>
         </div>";  
  
    //Editar Arbitragens Revistas
    echo "<div id=\"dialog-form-arbrevistas\" title=\"Editar Arbitragens Revistas\" hidden>
                <p class=\"validateTips-arbrevistas\"></p>
                    <form>
                    <fieldset id = \"edit\">
                        <label for=\"tipo-arbrevistas\">Tipo</label>                        
                        <SELECT id='tipo-arbrevistas' name='tipo-arbrevistas'>\n";
                        getTipoArbEdit();

           echo"<br><br><label for=\"issn-arbrevistas\">ISSN</label>
                        <input type=\"text\" name=\"issn-arbrevistas\" id=\"issn-arbrevistas\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"titulo-arbrevistas\">Revista</label>
                        <input type=\"text\" name=\"titulo-arbrevistas\" id=\"titulo-arbrevistas\" class=\"text ui-widget-content ui-corner-all\" />                       

                        <label for=\"link-arbrevistas\">Link</label>
                        <textarea name='link-arbrevistas' id='link-arbrevistas' style='height: 75px; width: 500px'></textarea></td>
				</fieldset>
           	    </form>
         </div>";

 	//Editar Premios	
    echo "<div id=\"dialog-form-premios\" title=\"Editar Prémios\" hidden>
                <p class=\"validateTips-premios\"></p>
                    <form>
                    <fieldset id = \"edit\">
                        <label for=\"tipo-premios\">Tipo</label>                        
                        <SELECT id='tipo-premios' name='tipo-premios'>\n";
                        getTipoPremioEdit();

           echo"<br><br><label for=\"nome-premios\">Nome</label>
                        <input type=\"text\" name=\"nome-premios\" id=\"nome-premios\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"contexto-premios\">Contexto</label>
                        <input type=\"text\" name=\"contexto-premios\" id=\"contexto-premios\" class=\"text ui-widget-content ui-corner-all\" />     

                        <label for=\"montante-premios\">Montante (em euros)</label>
                        <input type=\"text\" name=\"montante-premios\" id=\"montante-premios\" class=\"text ui-widget-content ui-corner-all\" />                        

                        <label for=\"link-premios\">Link</label>
                        <textarea name='link-premios' id='link-premios' style='height: 75px; width: 500px'></textarea></td>
				</fieldset>
           	    </form>
         </div>";

    //Editar Sociedades Cientificas
    echo "<div id=\"dialog-form-sc\" title=\"Editar Organizações e Sociedades Cientificas\" hidden>
                <p class=\"validateTips-sc\"></p>
                    <form>
                    <fieldset id = \"edit\">                    
                        <label for=\"nome-sc\">Nome</label>
                        <input type=\"text\" name=\"nome-sc\" id=\"nome-sc\" class=\"text ui-widget-content ui-corner-all\" />  

                        <label for=\"tipo-sc\">Tipo</label>                        
                        <SELECT id='tipo-sc' name='tipo-sc'>\n";
                        getTipoSCEdit();

           echo"<br><br><label for=\"datainicio-sc\">Data Início</label>
                        <input type=\"text\" name=\"datainicio-sc\" id=\"datainicio-sc\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"datafim-sc\">Data Fim</label>
                        <input type=\"text\" name=\"datafim-sc\" id=\"datafim-sc\" class=\"text ui-widget-content ui-corner-all\" />     

                        <label for=\"cargo-sc\">Cargo</label>
                        <input type=\"text\" name=\"cargo-sc\" id=\"cargo-sc\" class=\"text ui-widget-content ui-corner-all\" />                        

                        <label for=\"link-sc\">Link</label>
                        <textarea name='link-sc' id='link-sc' style='height: 75px; width: 500px'></textarea></td>
				</fieldset>
           	    </form>
         </div>";

     //Editar Redes
    echo "<div id=\"dialog-form-redes\" title=\"Editar Redes\" hidden>
                <p class=\"validateTips-redes\"></p>
                    <form>
                    <fieldset id = \"edit\">                    
                        <label for=\"nome-redes\">Nome</label>
                        <input type=\"text\" name=\"nome-redes\" id=\"nome-redes\" class=\"text ui-widget-content ui-corner-all\" />  

                        <label for=\"tipo-redes\">Tipo</label>                        
                        <SELECT id='tipo-redes' name='tipo-redes'>\n";
                        getTipoRedesEdit();

           echo"<br><br><label for=\"datainicio-redes\">Data Início</label>
                        <input type=\"text\" name=\"datainicio-redes\" id=\"datainicio-redes\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"datafim-redes\">Data Fim</label>
                        <input type=\"text\" name=\"datafim-redes\" id=\"datafim-redes\" class=\"text ui-widget-content ui-corner-all\" />                       

                        <label for=\"link-redes\">Link</label>
                        <textarea name='link-redes' id='link-redes' style='height: 75px; width: 500px'></textarea></td>
				</fieldset>
           	    </form>
         </div>";

      //Editar Missões
    echo "<div id=\"dialog-form-missoes\" title=\"Editar Missões\" hidden>
                <p class=\"validateTips-missoes\"></p>
                    <form>
                    <fieldset id = \"edit\">    
						<label for=\"motivacao-missoes\">Motivação</label>
                        <input type=\"text\" name=\"motivacao-missoes\" id=\"motivacao-missoes\" class=\"text ui-widget-content ui-corner-all\" />
						
                    	<label for=\"datainicio-missoes\">Data Início</label>
                        <input type=\"text\" name=\"datainicio-missoes\" id=\"datainicio-missoes\" class=\"text ui-widget-content ui-corner-all\" />

                        <label for=\"datafim-missoes\">Data Fim</label>
                        <input type=\"text\" name=\"datafim-missoes\" id=\"datafim-missoes\" class=\"text ui-widget-content ui-corner-all\" />
                          
                        <label for=\"instituicao-missoes\">Instituto de Acolhimento</label>";
                        getInstEdit();
						
				echo"<br><br><label for=\"pais-missoes\">País</label>                        
                        <SELECT id='pais-missoes' name='pais-missoes'>\n";
                        getPaisesEdit();

           echo"<br><br><label for=\"ambtese-missoes\">Âmbito de Tese</label>
					    <SELECT id='ambtese-missoes' name='ambtese-missoes' >\n
							<option>Sim</option>\n	
							<option>Não</option>\n		
						</SELECT>\n	
				</fieldset>
           	    </form>
         </div>";   

       //Editar Orientações
    echo "<div id=\"dialog-form-orientacoes\" title=\"Editar Orientações\" hidden>
                <p class=\"validateTips-orientacoes\"></p>
                    <form>
                    <fieldset id = \"edit\">    
                    	<label for=\"tipo-orientacoes\">Tipo de Orientação</label>
                    	<select id='tipo-orientacoes' name='tipo-orientacoes'>";
                    	getTipoOrientEdit();

       	  echo "<br><br><label for=\"tipoori-orientacoes\">Tipo de Orientador</label>
                        <select id='tipoori-orientacoes' name='tipoori-orientacoes'>";
                        getTipoOriOrientEdit();

          echo "<br><br><label for=\"estado-orientacoes\">Estado</label>
                        <select id='estado-orientacoes' name='estado-orientacoes'> ";
                        getEstadosOrientEdit();

    	  echo "<br><br><label for=\"titulo-orientacoes\">Título da Tese em Português</label>
                        <input type=\"text\" name=\"titulo-orientacoes\" id=\"titulo-orientacoes\" class=\"text ui-widget-content ui-corner-all\" />                          
				</fieldset>
           	    </form>
         </div>"; 

       //Editar Juris
    echo "<div id=\"dialog-form-juris\" title=\"Editar Júris\" hidden>
                <p class=\"validateTips-juris\"></p>
                    <form>
                    <fieldset id = \"edit\">  
                    	<p><u><b>Agregação</b></u></p><br>
                    	<i><u>FMUP</u></i><br><br>
                    	<label for=\"agrargfmup-juris\">Arguente</label>
                        <input type=\"text\" name=\"agrargfmup-juris\" id=\"agrargfmup-juris\" class=\"text ui-widget-content ui-corner-all\" /> 

                        <label for=\"agrvogalfmup-juris\">Outro</label>
                        <input type=\"text\" name=\"agrvogalfmup-juris\" id=\"agrvogalfmup-juris\" class=\"text ui-widget-content ui-corner-all\" />  

                        <i><u>Exterior</u></i><br><br>
                    	<label for=\"agrargext-juris\">Arguente</label>
                        <input type=\"text\" name=\"agrargext-juris\" id=\"agrargext-juris\" class=\"text ui-widget-content ui-corner-all\" /> 

                        <label for=\"agrvogalext-juris\">Outro</label>
                        <input type=\"text\" name=\"agrvogalext-juris\" id=\"agrvogalext-juris\" class=\"text ui-widget-content ui-corner-all\" />  

                        <br><div class='ui-dialog-buttonpane ui-widget-content ui-helper-clearfix'/><br>   

                        <p><u><b>Doutoramento</b></u></p><br>
                    	<i><u>FMUP</u></i><br><br>
                    	<label for=\"doutargfmup-juris\">Arguente</label>
                        <input type=\"text\" name=\"doutargfmup-juris\" id=\"doutargfmup-juris\" class=\"text ui-widget-content ui-corner-all\" /> 

                        <label for=\"doutvogalfmup-juris\">Outro</label>
                        <input type=\"text\" name=\"doutvogalfmup-juris\" id=\"doutvogalfmup-juris\" class=\"text ui-widget-content ui-corner-all\" />  

                        <i><u>Exterior</u></i><br><br>
                    	<label for=\"doutargext-juris\">Arguente</label>
                        <input type=\"text\" name=\"doutargext-juris\" id=\"doutargext-juris\" class=\"text ui-widget-content ui-corner-all\" /> 

                        <label for=\"doutvogalext-juris\">Outro</label>
                        <input type=\"text\" name=\"doutvogalext-juris\" id=\"doutvogalext-juris\" class=\"text ui-widget-content ui-corner-all\" />  

                        <br><div class='ui-dialog-buttonpane ui-widget-content ui-helper-clearfix'/><br>    

                        <p><u><b>Mestrado Científico</b></u></p><br>
                    	<i><u>FMUP</u></i><br><br>
                    	<label for=\"mesargfmup-juris\">Arguente</label>
                        <input type=\"text\" name=\"mesargfmup-juris\" id=\"mesargfmup-juris\" class=\"text ui-widget-content ui-corner-all\" /> 

                        <label for=\"mesvogalfmup-juris\">Outro</label>
                        <input type=\"text\" name=\"mesvogalfmup-juris\" id=\"mesvogalfmup-juris\" class=\"text ui-widget-content ui-corner-all\" />  

                        <i><u>Exterior</u></i><br><br>
                    	<label for=\"mesargext-juris\">Arguente</label>
                        <input type=\"text\" name=\"mesargext-juris\" id=\"mesargext-juris\" class=\"text ui-widget-content ui-corner-all\" /> 

                        <label for=\"mesvogalext-juris\">Outro</label>
                        <input type=\"text\" name=\"mesvogalext-juris\" id=\"mesvogalext-juris\" class=\"text ui-widget-content ui-corner-all\" />  

                        <br><div class='ui-dialog-buttonpane ui-widget-content ui-helper-clearfix'/><br>      

                        <p><u><b>Mestrado Integrado</b></u></p><br>
                    	<i><u>FMUP</u></i><br><br>
                    	<label for=\"mesintfmup-juris\">Arguente</label>
                        <input type=\"text\" name=\"mesintfmup-juris\" id=\"mesintfmup-juris\" class=\"text ui-widget-content ui-corner-all\" /> 

                        <label for=\"mesintvogalfmup-juris\">Outro</label>
                        <input type=\"text\" name=\"mesintvogalfmup-juris\" id=\"mesintvogalfmup-juris\" class=\"text ui-widget-content ui-corner-all\" />  

                        <i><u>Exterior</u></i><br><br>
                    	<label for=\"mesintargext-juris\">Arguente</label>
                        <input type=\"text\" name=\"mesintargext-juris\" id=\"mesintargext-juris\" class=\"text ui-widget-content ui-corner-all\" /> 

                        <label for=\"mesintvogalext-juris\">Outro</label>
                        <input type=\"text\" name=\"mesintvogalext-juris\" id=\"mesintvogalext-juris\" class=\"text ui-widget-content ui-corner-all\" />  
				</fieldset>
           	    </form>
         </div>"; 

    //Editar Outras Actividades
    echo "<div id=\"dialog-form-outact\" title=\"Editar Outras Atividades\" hidden>
                <p class=\"validateTips-outact\"></p>
                    <form>
                    <fieldset id = \"edit\">                      	
                    	<label for=\"texapoio-outact\">Monografias de apoio às aulas ou a trabalho clínico</label>
                        <input type=\"text\" name=\"texapoio-outact\" id=\"texapoio-outact\" class=\"text ui-widget-content ui-corner-all\" /> 

                        <label for=\"livdidaticos-outact\">Livros Didáticos</label>
                        <input type=\"text\" name=\"livdidaticos-outact\" id=\"livdidaticos-outact\" class=\"text ui-widget-content ui-corner-all\" />  

                    	<label for=\"livdivulgacao-outact\">Livros de Divulgação</label>
                        <input type=\"text\" name=\"livdivulgacao-outact\" id=\"livdivulgacao-outact\" class=\"text ui-widget-content ui-corner-all\" /> 

                        <label for=\"app-outact\">Vídeos, jogos, aplicações</label>
                        <input type=\"text\" name=\"app-outact\" id=\"app-outact\" class=\"text ui-widget-content ui-corner-all\" />    

                    	<label for=\"webp-outact\">Páginas Internet</label>
                        <input type=\"text\" name=\"webp-outact\" id=\"webp-outact\" class=\"text ui-widget-content ui-corner-all\" />                        
				</fieldset>
           	    </form>
         </div>";  

    //Editar Ações Divulgação
    echo "<div id=\"dialog-form-acaodivulgacao\" title=\"Editar Ação de Divulgação\" hidden>
                <p class=\"validateTips-acaodivulgacao\"></p>
                    <form>
                    <fieldset id = \"edit\">                      	
                    	<label for=\"titulo-acaodivulgacao\">Título</label>
                        <input type=\"text\" name=\"titulo-acaodivulgacao\" id=\"titulo-acaodivulgacao\" class=\"text ui-widget-content ui-corner-all\" /> 

                        <label for=\"data-acaodivulgacao\">Data</label>
                        <input type=\"text\" name=\"data-acaodivulgacao\" id=\"data-acaodivulgacao\" class=\"text ui-widget-content ui-corner-all\" />  

                    	<label for=\"local-acaodivulgacao\">Local</label>
                        <input type=\"text\" name=\"local-acaodivulgacao\" id=\"local-acaodivulgacao\" class=\"text ui-widget-content ui-corner-all\" /> 

                        <label for=\"npart-acaodivulgacao\">Número de Participantes</label>
                        <input type=\"text\" name=\"npart-acaodivulgacao\" id=\"npart-acaodivulgacao\" class=\"text ui-widget-content ui-corner-all\" />                           
				</fieldset>
           	    </form>
         </div>"; 

     //Editar Produtos
    echo "<div id=\"dialog-form-produtos\" title=\"Editar Produtos e Serviços\" hidden>
                <p class=\"validateTips-produtos\"></p>
                    <form>
                    <fieldset id = \"edit\">                      	
                    	<label for=\"tipo-produtos\">Tipo</label>
                        <input type=\"text\" name=\"tipo-produtos\" id=\"tipo-produtos\" class=\"text ui-widget-content ui-corner-all\" /> 

                        <label for=\"preco-produtos\">Preço (€)</label>
                        <input type=\"text\" name=\"preco-produtos\" id=\"preco-produtos\" class=\"text ui-widget-content ui-corner-all\" />                     	                        
				</fieldset>
           	    </form>
         </div>"; 
		 
	echo "<div id=\"dialog-form-obj\" title=\"Adicionar Observação\" hidden>
		<p class=\"validateTips-obj\"></p>
		<form>
			<fieldset id='edit'>				
				<label for=\"texto-obj\">Observação (opcional):</label>
				<input type=\"text\" name=\"texto-obj\" id=\"texto-obj\" class=\"text ui-widget-content ui-corner-all\" />
			</fieldset>
		</form>
 </div>";
 
 function getAnoEdit(){
	
		$db = new Database();
		$lValues =$db->getLookupValuesAnoInicio("lista_anoslectivos");
		
		while ($row = mysql_fetch_assoc($lValues)) {	
			echo "<option>".$row["DESCRICAO"]."</option>\n";
		}
		echo "</SELECT><br />\n";
		$db->disconnect();				
	}	
	

 function getInstEdit() {
		$db = new Database();
		$lValues =$db->getLookupValuesAnoInicio("lista_instituicoes");
		
		while ($row = mysql_fetch_assoc($lValues)) {	
			echo "<option value='".$row["DESCRICAO"]."'>".$row["DESCRICAO"]."</option>\n";
		}
		echo "</SELECT><br />\n";
		$db->disconnect();				
	}

	function getUniInvEdit() {
		$db = new Database();
		$lValues =$db->getLookupValuesAnoInicio("lista_unidadesfct");
		
		while ($row = mysql_fetch_assoc($lValues)) {	
			echo "<option value='".$row["AVALIACAO"]."' >".$row["DESCRICAO"]."</option>\n";
		}
		echo "</SELECT>";
		$db->disconnect();			
	}	

	function getGrauEdit() {
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_graucursos");		
		
		while ($row = mysql_fetch_assoc($lValues)) {	
			echo "<option>".$row["DESCRICAO"]."</option>\n";
		}
		echo "</SELECT><br />\n";
		$db->disconnect();
				
	}

	function getDistincaoEdit() {
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_distincaoValores");
		
		while ($row = mysql_fetch_assoc($lValues)) {
			echo "<option>".$row["DESCRICAO"]."</option>\n";
		}
		echo "</SELECT><br />\n";
		$db->disconnect();
	
	}

	function getBolsaEdit() {
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_TipoBolsas");	
	
		while ($row = mysql_fetch_assoc($lValues)) {
			echo "<option>".$row["DESCRICAO"]."</option>\n";
		}
		echo "</SELECT><br />\n";
		$db->disconnect();
	
	}
	
	function getPercentagemEdit(){		
	
		for($i=100;$i>=0;$i=$i-1)
			$sel=$sel."<option>".$i."</option>";
	
		$sel=$sel."</select>";
		echo $sel;
	}

	function getCatEdit() {
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_tipodocente");
	
		$resultado= $resultado."<option></option>\n";
	
		while ($row = mysql_fetch_assoc($lValues)) {
			$resultado= $resultado."<option>".$row["DESCRICAO"]."</option>\n";
		}
		$resultado= $resultado."</select>\n";
		$db->disconnect();
		echo $resultado;
	}

	function getDepartamentosEdit()
	{

		$db = new Database();
		$lValues =$db->adminGetDadosServico();
		
		$options="";
		while ($row = mysql_fetch_assoc($lValues)){
				$options=$options."<option  >".$row['DESCRICAO']."</option>";			
	    }
	    $options=$options."</select>";
	    echo $options;
	}
	
	function getPaisesEdit()
	{
		$db = new Database();
		$lValues = $db->getLookupValues("lista_paises");
	
		while ($row = mysql_fetch_assoc($lValues)) {	
			echo "<option>".$row["DESCRICAO"]."</option>\n";
		}
		$db->disconnect();
		echo "</SELECT>\n";
	}

	function getEstadoPubEdit()
	{
		$db = new Database();
		$lValues = $db->getLookupValues("lista_estadoPublicacoes");
	
		while ($row = mysql_fetch_assoc($lValues)) {	
			echo "<option>".$row["DESCRICAO"]."</option>\n";
		}
		$db->disconnect();
		echo "</SELECT>\n";
	}

	function getEstadoProjEdit()
	{
		$db = new Database();
		$lValues = $db->getLookupValues("lista_estadoProjetos");
	
		while ($row = mysql_fetch_assoc($lValues)) {	
			echo "<option value='".$row["ID"]."'>".$row["DESCRICAO"]."</option>\n";
		}
		$db->disconnect();
		echo "</SELECT>\n";
	}

	function getEntidadeFinProjEdit()
	{
		$db = new Database();
		$lValues = $db->getLookupValues("lista_tipoentidadefinanciadora");
	
		while ($row = mysql_fetch_assoc($lValues)) {	
			echo "<option>".$row["DESCRICAO"]."</option>\n";
		}
		$db->disconnect();
		echo "</SELECT>\n";
	}

	function getAmbitoConfEdit()
	{
		$db = new Database();
		$lValues = $db->getLookupValues("lista_ambitoconf");
	
		while ($row = mysql_fetch_assoc($lValues)) {	
			echo "<option>".$row["DESCRICAO"]."</option>\n";
		}
		$db->disconnect();
		echo "</SELECT>\n";
	}
	
	function getTipoArbEdit()
	{
		$db = new Database();
		$lValues = $db->getLookupValues("lista_tipoarbitragens");
	
		while ($row = mysql_fetch_assoc($lValues)) {	
			echo "<option>".$row["DESCRICAO"]."</option>\n";
		}
		$db->disconnect();
		echo "</SELECT>\n";
	}

	function getTipoPremioEdit()
	{
		$db = new Database();
		$lValues = $db->getLookupValues("lista_tipopremio");
	
		while ($row = mysql_fetch_assoc($lValues)) {	
			echo "<option>".$row["DESCRICAO"]."</option>\n";
		}
		$db->disconnect();
		echo "</SELECT>\n";
	}

	function getTipoSCEdit()
	{
		$db = new Database();
		$lValues = $db->getLookupValues("lista_tiposociedadecientifica");
	
		while ($row = mysql_fetch_assoc($lValues)) {	
			echo "<option>".$row["DESCRICAO"]."</option>\n";
		}
		$db->disconnect();
		echo "</SELECT>\n";
	}

	function getTipoRedesEdit()
	{
		$db = new Database();
		$lValues = $db->getLookupValues("lista_tiporedes");
	
		while ($row = mysql_fetch_assoc($lValues)) {	
			echo "<option>".$row["DESCRICAO"]."</option>\n";
		}
		$db->disconnect();
		echo "</SELECT>\n";		
	}

	function getEstadosOrientEdit()
	{
		$db = new Database();
		$lValues = $db->getLookupValues("lista_estadosOrientacao");
	
		while ($row = mysql_fetch_assoc($lValues)) {	
			echo "<option>".$row["DESCRICAO"]."</option>\n";
		}
		$db->disconnect();
		echo "</SELECT>\n";		
	}

	function getTipoOrientEdit()
	{
		$db = new Database();
		$lValues = $db->getLookupValues("lista_tipoOrientacao");
	
		while ($row = mysql_fetch_assoc($lValues)) {	
			echo "<option>".$row["DESCRICAO"]."</option>\n";
		}
		$db->disconnect();
		echo "</SELECT>\n";		
	}

	function getTipoOriOrientEdit()
	{
		$db = new Database();
		$lValues = $db->getLookupValues("lista_tipoOrientador");
	
		while ($row = mysql_fetch_assoc($lValues)) {	
			echo "<option>".$row["DESCRICAO"]."</option>\n";
		}
		$db->disconnect();
		echo "</SELECT>\n";		
	}
	
 ?>