<?php
	
echo "<div id='content4' style='display: inline;'>";
echo "<fieldset class='normal'>\n";
echo "<legend>Caracterização <a href='../helpfiles/Caracter.JPG' style='color: #FFFF00' target='_blank'>Ajuda</a>/Characterization <a href='../helpfiles/Caracter.JPG' style='color: #FFFF00' target='_blank'>Help</a></legend><br/>\n";

echo "<p class='ppthelp'>Indique o(s) tipo(s) de ocupação profissional que exerce na FMUP entre os seguintes. Note que deve preencher a percentagem formal dedicada a cada um. Indique, se for caso disso, se tem Bolsa.</p>";

echo "<p class='penhelp'><i>Please indicate the type(s) of professional occupation that you have at FMUP among the following. Note that you should also fill the formal percentage devoted to each. Please indicate, if applicable, if you have a scholarship</i></p>";

$inp="<input size='2' MAXLENGTH='2' type='text'  >";

echo "<table id='caracteriza' class='box-table-a'><tr><th>Tipo<p><i>Type</i></p></th><th>Percentagem<p><i>Percentage</i></p></th><th>Categoria<p><i>Category</i></p></th><th>Bolsa<p><i>Bolsa</i></p></th></tr>";
echo "<tr><td>Docente<p><i>Lecturer</i></p></td><td>".getPercentageValues("docente",$questionario->ocupacaoprofissional->docenteper)."</td><td>".getCat("docente",$questionario->ocupacaoprofissional->docentecat,$questionario->ocupacaoprofissional->docenteper)."</td><td></td></tr>";

echo "<tr><td>Investigador<p><i>Researcher</i></p></td><td>".
getPercentageValuesInvestigador("investigador",$questionario->ocupacaoprofissional->investigadorper)."</td><td>".
getCatInvestigador("investigador",$questionario->ocupacaoprofissional->investigadorcat,$questionario->ocupacaoprofissional->investigadorper,$questionario->ocupacaoprofissional->investigadorbolsa)."</td><td>".
getBolsaInvestigador("investigador",$questionario->ocupacaoprofissional->investigadorcat,$questionario->ocupacaoprofissional->investigadorper,$questionario->ocupacaoprofissional->investigadorbolsa)."</td></tr>";
//<td><input class='inp-textAuto'  size='4' MAXLENGTH='4' type='text' name='investigadormes' id='investigadormes' value='".$questionario->ocupacaoprofissional->investigadormes."' ".checkDisabled($questionario->ocupacaoprofissional->investigadorper)." onkeypress='validate(event)'></td>
//	echo "</table>";


//	echo "<table id='box-table-a'><tr><th>Tipo<p><i>Type</i></p></th><th>Percentagem<p><i>Percentage</i></p></th><th>Meses<p><i>Months</i></p></th></tr>";

//	echo "<tr><td>Estudante 3º Ciclo</td><td>".getPercentageValues("tciclo",$questionario->ocupacaoprofissional->tcicloper)."</td><td></td></tr>";
//<td><input class='inp-textAuto' size='4' MAXLENGTH='4' type='text' name='tciclomes' id='tciclomes' value='".$questionario->ocupacaoprofissional->tciclomes."' ".checkDisabled($questionario->ocupacaoprofissional->tcicloper)." onkeypress='validate(event)'></td>
//	echo "<tr><td>Estudante 2º Ciclo</td><td>".getPercentageValues("sciclo",$questionario->ocupacaoprofissional->scicloper)."</td><td></td></tr>";
//<td><input class='inp-textAuto' size='4' MAXLENGTH='4' type='text' name='sciclomes' id='sciclomes' value='".$questionario->ocupacaoprofissional->sciclomes."' ".checkDisabled($questionario->ocupacaoprofissional->scicloper)." onkeypress='validate(event)'></td>
//	echo "<tr><td>Estudante 1º Ciclo</td><td>".getPercentageValues("pciclo",$questionario->ocupacaoprofissional->pcicloper)."</td><td></td></tr>";
//<td><input class='inp-textAuto' size='4' MAXLENGTH='4' type='text' name='pciclomes' id='pciclomes' value='".$questionario->ocupacaoprofissional->pciclomes."' ".checkDisabled($questionario->ocupacaoprofissional->pcicloper)." onkeypress='validate(event)'></td>
//	echo "<tr><td>Estudante Outro</td><td>".getPercentageValues("ociclo",$questionario->ocupacaoprofissional->ocicloper)."</td><td></td></tr>";
//<td><input class='inp-textAuto' size='4' MAXLENGTH='4' type='text' name='ociclomes' id='ociclomes' value='".$questionario->ocupacaoprofissional->ociclomes."' ".checkDisabled($questionario->ocupacaoprofissional->ocicloper)." onkeypress='validate(event)'></td>

//	echo "</table>";

//	echo "<table id='box-table-a'><tr><th>Tipo<p><i>Type</i></p></th><th>Percentagem<p><i>Percentage</i></p></th><th>Meses<p><i>Months</i></p></th></tr>";
echo "<tr><td>Técnico Superior</td><td>".getPercentageValues("tec",$questionario->ocupacaoprofissional->tecper)."</td><td></td><td></td></tr>";
//<td><input class='inp-textAuto' size='4' MAXLENGTH='4' type='text' name='tecmes' id='tecmes' value='".$questionario->ocupacaoprofissional->tecmes."' ".checkDisabled($questionario->ocupacaoprofissional->tecper)." onkeypress='validate(event)'></td>
//	echo "</table>";

//	echo "<table id='box-table-'><tr><th>Tipo<p><i>Type</i></p></th><th>Percentagem<p><i>Percentage</i></p></th><th>Meses<p><i>Months</i></p></th></tr>";
echo "<tr><td>Outra<p><i>Other</i></p></td><td>".getPercentageValues("outra",$questionario->ocupacaoprofissional->outraper)."</td><td></td><td></td></tr>";
//<td><input class='inp-textAuto' size='4' MAXLENGTH='4' type='text' name='outrames' id='outrames' value='".$questionario->ocupacaoprofissional->outrames."' ".checkDisabled($questionario->ocupacaoprofissional->outraper)." onkeypress='validate(event)'></td>
echo "</table>";

echo "</fieldset>";



echo "<fieldset class='normal'>\n";
echo "<legend>Dedicação FMUP <a href='../helpfiles/Dedic.JPG' style='color: #FFFF00' target='_blank'>Ajuda</a> /<i>FMUP Tasks <a href='../helpfiles/Dedic.JPG' style='color: #FFFF00' target='_blank'>Help</a></i></legend><br/>\n";

echo "<p class='ppthelp'>Independentemente de ter uma posição a tempo inteiro ou não, utilizando para ponto de partida o nº de horas semanais em que trabalha para a FMUP, estime a percentagem dessas horas em ".$anoactual."
 que foram dedicadas a:</p>";
echo "<p class='penhelp'><i>Regardless of having a full time position or not, and starting with the total number of hours per week that you work for FMUP, please estimate the percentage of these that, in ".$anoactual."
, were devoted to:</i></p>";
echo "<table id='dedica' class='box-table-a'>";
echo "<tr><td>Investigação/<i>Research</i></td><td>".getPercentagem("fmupinv",$questionario->ocupacaoprofissional->fmupinv)."</td></tr>";
echo "<tr><td>Ensino/<i>Education</i></td><td>".getPercentagem("fmupens",$questionario->ocupacaoprofissional->fmupens)."</td></tr>";
echo "<tr><td>Gestão/<i>Management</i></td><td>".getPercentagem("fmupgest",$questionario->ocupacaoprofissional->fmupgest)."</td></tr>";
echo "<tr><td>Transferência de Conhecimento/<i>Knowledge Transfer</i></td><td>".getPercentagem("fmupdc",$questionario->ocupacaoprofissional->fmupdc)."</td></tr>";
echo "</table>";
echo "</fieldset>";



echo "<p><fieldset class='normal'>\n";
			echo "<legend>Experiência Profissional <a href='../helpfiles/Experie.JPG' style='color: #FFFF00' target='_blank'>Ajuda</a> /<i>Professional Experience <a href='../helpfiles/Experie.JPG' style='color: #FFFF00' target='_blank'>Help</a></i></legend>\n";
			
echo "<p class='ppthelp'>Preencher, em cada linha seguinte, novos contratos realizados em ".$anoactual."
 (na data final deve-se utilizar “sem termo”, se aplicável; preencha também a percentagem contratual – última caixa). De notar que se inclui atividade clínica, de consultoria e outras. Quando o empregador for a FMUP, escreva exatamente dessa forma “FMUP” e escolha um Serviço/Departamento da lista que surge em “Departamento” (basta escrever parte do nome para tal).</p>";			

echo "<p class='penhelp'><i>Please fill, in each following line, new ".$anoactual."
 contracts (as regards the end date, please use “sem termo”, if aplicable; please fill also the formal percentage in the last box). Please note that you should include any clinical activities, consulting, and others. When the employer is FMUP, please write exactly “FMUP”, and pick a Service/Department from the list that appears as you write part of its name</i></p>";
			



echo "
<table id='expp' class='box-table-b'>
<thead>
	<tr>
      <th><u></u></th>
	 <th><u>Data de Início<p><i>Start Date</i></p></u></th>
      <th><u>Data de Fim<p><i>End Date</i></p></u></th>
      <th><u>Posição<p><i>Position</i></p></u></th>
      <th><u>Empregador<p><i>Employer</i></p></u></th>
      <th><u>Departamento<p><i>Department</i></p></u></th>
      <th><u>Percentagem<p><i>Percentage</i></p></u></th>
   </tr>
	</thead>
<tbody>
";



    
    foreach ($questionario->experienciaprofissional as $i => $value){
    	
			
			echo "<tr>";
			
			echo "<td>";
			echo "<input type='image' src='../images/icon_delete_s.png' name='navOption' value='Apagar Experiência' onclick='if(formSubmited==0){return false;};document.questionario.apagaRegExperiencia.value=".$questionario->experienciaprofissional[$i]->id.";document.questionario.operacao.value=10;' >";
			echo "</td>";
			
			echo "<td>";
			echo "<input class='inp-textAuto' type='text' id='di_".$i."' size='11' name='datainicio".$i."' value='".$questionario->experienciaprofissional[$i]->datainicio."' onfocus='calendario(\"di_".$i."\");' onkeypress='validateCal(event);'>";
			echo "</td>";

			echo "<td>";
			
    		
			
			if ($questionario->experienciaprofissional[$i]->semtermo=="1"){
				echo "<div id='df_".$i."' style='visibility: hidden;'><input class='inp-textAuto' type='text'  size='11' id='epdf_".$i."' name='datafim".$i."' value='".$questionario->experienciaprofissional[$i]->datafim."' onfocus='calendario(\"epdf_".$i."\");' onkeypress='validateCal(event);'></div>";
				echo "<input class='inp-textAuto' type='checkbox' id='semtermo".$i."' name='semtermo".$i."' checked value='1'  onclick='enabledisableDatafim(".$i.")'> Sem termo";
			}else{ 
				echo "<div id='df_".$i."'><input class='inp-textAuto' type='text'  size='11' id='epdf_".$i."' name='datafim".$i."' value='".$questionario->experienciaprofissional[$i]->datafim."' onfocus='calendario(\"epdf_".$i."\");'></div>";
				echo "<input class='inp-textAuto' type='checkbox' id='semtermo".$i."' name='semtermo".$i."' value='1' onclick='enabledisableDatafim(".$i.")'> Sem termo";
			}
			
			
			echo "</td>";
			
			echo "<td>";
			echo "<input class='inp-textAuto' type='text' name='posicao".$i."' value='".$questionario->experienciaprofissional[$i]->posicao."'>\n";
			echo "</td>";
			
			echo "<td>";
			echo "<input class='inp-textAuto' type='text' id='empregador".$i."' name='empregador".$i."' size='50' value='".$questionario->experienciaprofissional[$i]->empregador."'>";
			echo "</td>";
			
			echo "<td>";
			echo "<input class='inp-textAuto' type='text' id='departamento".$i."' size='40' name='departamento".$i."' value='".$questionario->experienciaprofissional[$i]->departamento."' onkeyup='mostraDEP(this.value,".$i.");'><div  class='divlookup' id='livesearchdep".$i."'></div>";
			echo "</td>";
			
			echo "<td>";
			echo "".getPercentagem("percentagem".$i,$questionario->experienciaprofissional[$i]->percentagem);
			//echo "<input class='inp-textAuto' type='text' name='percentagem".$i."' size='3' MAXLENGTH='3' onkeypress='validate(event)' value='".$questionario->experienciaprofissional[$i]->percentagem."'>\n";
	    	echo "</td>";
	    	echo "</tr>";
	    	
	    	
    }
   	echo "</tbody><tfoot><tr><td><input input type='image' src='../images/icon_new_s.png' name='navOption' value='Novo Contrato' onclick='if(formSubmited==0){return false;};document.questionario.operacao.value=5;'></td></tr></tfoot>";
	
    echo "</tbody></table>";
echo "<input type='hidden' name='apagaRegExperiencia'/>";
     
	echo "</fieldset>";

	echo "</div>";
	
	function getPercentagem($cat,$valor){
	
		$sel="<select name='".$cat."' id='".$cat."' >";
	
		for($i=100;$i>=0;$i=$i-1)
			$sel=$sel."<option value='".$i."' ".checkSelected($i,$valor).">".$i."</option>";
	
		$sel=$sel."</select>";
		return $sel;
	}
	
	
	function checkSelected($i,$valor){
	
		if($i==$valor){
			return "SELECTED";
		}else{
			return "";
		}
	
	}
	
	
	function getPercentageValues($cat,$valor){
	
		$sel="<select name='".$cat."per' id='".$cat."per' onchange='showhideOP(\"".$cat."\")'>";
	
		for($i=100;$i>=0;$i=$i-1)
			$sel=$sel."<option value='".$i."' ".checkSelectedOP($i,$valor).">".$i."</option>";
	
		$sel=$sel."</select>";
		return $sel;
	}
	
	function getPercentageValuesInvestigador($cat,$valor){
	
		$sel="<select name='".$cat."per' id='".$cat."per' onchange='showhideOPInvestigador(\"".$cat."\")'>";
	
		for($i=100;$i>=0;$i=$i-1)
			$sel=$sel."<option value='".$i."' ".checkSelectedOP($i,$valor).">".$i."</option>";
	
		$sel=$sel."</select>";
		return $sel;
	}
	
	
	function checkSelectedOP($i,$valor){
	
		if($i==$valor){
			return " SELECTED";
		}else{
			return "";
		}
	
	}
	
	function checkDisabled($valor){
		if($valor<0)
			return "DISABLED";
		else
			return "";
	}
	
	
	function getCat($tipo,$valorcat,$valorper) {
	
		$resultado="";
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_tipo".$tipo);
	
		$resultado= $resultado."<select name='".$tipo."cat' id='".$tipo."cat' ".checkDisabled($valorper).">";
		$resultado= $resultado."<option></option>\n";
	
		while ($row = mysql_fetch_assoc($lValues)) {
			$resultado= $resultado."<option value='".$row["ID"]."'".checkSelectedOP($row["ID"],$valorcat).">".$row["DESCRICAO"]."</option>\n";
		}
		$resultado= $resultado."</SELECT>\n";
		$db->disconnect();
		return $resultado;
	}
	
	function getCatInvestigador($tipo,$valorcat,$valorper,$bolsa) {
	
		$resultado="";
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_tipo".$tipo);
	
		$resultado= $resultado."<select name='".$tipo."cat' id='".$tipo."cat' ".checkDisabled($valorper).">";
		$resultado= $resultado."<option></option>\n";
	
		while ($row = mysql_fetch_assoc($lValues)) {
			$resultado= $resultado."<option value='".$row["ID"]."'".checkSelectedOP($row["ID"],$valorcat).">".$row["DESCRICAO"]."</option>\n";
		}
		$resultado= $resultado."</SELECT>\n";
	
		$db->disconnect();
	
		return $resultado;
	}
	
	function getBolsaInvestigador($tipo,$valorcat,$valorper,$bolsa) {
	
	
		$resultado="";
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_TipoBolsas");
	
		$resultado= $resultado."<select name='".$tipo."bolsa' id='".$tipo."bolsa' ".checkDisabled($valorper).">";
		$resultado= $resultado."<option></option>\n";
	
		while ($row = mysql_fetch_assoc($lValues)) {
			$resultado= $resultado."<option value='".$row["ID"]."'".checkSelectedOP($row["ID"],$bolsa).">".$row["DESCRICAO"]."</option>\n";
		
		}
		$resultado= $resultado."</SELECT>\n";
	
		$db->disconnect();
	
		return $resultado;
	}
	

	
	?>