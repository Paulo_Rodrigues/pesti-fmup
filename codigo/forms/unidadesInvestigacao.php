<?php

echo "<div id='content15' style='display: inline;'>";
echo "<p><fieldset class='normal'>\n";
			echo "<legend>Unidades de Investigação/Research Units</legend>\n";

echo "<p class='ppthelp'>Escreva o(s) nome(s) da(s) Unidade(s) de Investigação a que pertence (enquanto escreve surge uma lista de todas as Unidades da FCT, para ajudar) e a percentagem de tempo a ela(s) oficialmente adstrito. Se foi o Responsável pela Unidade, clicar na caixa respetiva.</p>";			

echo "<p class='penhelp'><i>Please write the name(s) of the Research Unit(s) to which you belong (to help with this, a full list of FCT Units appears as you type) and fill the percentage of time officially applicable (to each one). If you were the Unit’s Head please click in the corresponding box.</i></p>";
			
			
echo "
    <table id='uniinv' class='box-table-b'>
    <!-- Results table headers -->
    <tr>
      <th></th>
      <th>Unidade de Investigação<p><i>Research Unit</i></p></th>
      <th>Avaliação 2007<p><i>2007 Evaluation</i></p></th>
      <th>Percentagem<p><i>Percentage</i></p></th>
      <th>Responsável pela Unidade<p><i>Unit Head</i></p></th>
       
    </tr>";
			    
// <th>Confirmar Avaliação<p><i>Confirm Evaluation</i></p></th>
    foreach ($questionario->unidadesinvestigacao as $i => $value){
    	
			
			echo "<tr>";
			
		   	echo "<td>";
			echo "<input type='image' src='../images/icon_delete.png' name='navOption' value='Apagar Unidade' onclick='if(formSubmited==0){return false;};document.questionario.apagaRegUnidade.value=".$i.";document.questionario.operacao.value=14;' >";
			echo "</td>";
			
			/*echo "<td>";
			//echo "<input class='inp-textAuto' type='text' name='tipo".$i."' value='".$questionario->unidadesinvestigacao[$i]->tipo."'>";
			
			
			getTipoUnidade($i);
			
			
    		echo "</td>";*/
    					
			echo "<td>";
			echo "<input type='hidden' name='id_unidade".$i."' id='id_unidade".$i."' value='".$questionario->unidadesinvestigacao[$i]->id_unidade."'>";
			echo "<input class='inp-textAuto' type='text' id='unidade".$i."' name='unidade".$i."' size='100' value='".$questionario->unidadesinvestigacao[$i]->unidade."' onkeyup='mostraResultadoUnidade(this.value,".$i.");'><div id='livesearch".$i."'></div>";
			
			//getUnidade($i);
			
//			onkeyup='mostraAvaliacaoUnidade(this.value,".$i.")'
			
			echo "</td>";
			
			echo "<td>";
			echo "<input class='inp-textAuto' type='text' size='9' id='ava2007".$i."' name='ava2007".$i."' value='".$questionario->unidadesinvestigacao[$i]->ava2007."'>\n";
			echo "</td>";
			
			echo "<td>";
			echo "<input class='inp-textAuto' type='text' name='percentagem".$i."' maxlength='3' size='3' onkeypress='validate(event)' value='".$questionario->unidadesinvestigacao[$i]->percentagem."'>";
			echo "</td>";
			
			echo "<td>";
			if ($questionario->unidadesinvestigacao[$i]->responsavel==1) {
			//echo "<input type='checkbox' name='responsavel".$i."' value='".$questionario->unidadesinvestigacao[$i]->responsavel."' checked></p>\n";
				echo "<input type='checkbox' name='responsavel".$i."' value='1' checked></p>\n";
			}else {
				echo "<input type='checkbox' name='responsavel".$i."'  value='1'>";
			}
	    	echo "</td>";
	    	
	    	/*echo "<td>";
			echo "<input class='inp-textAuto' type='text' name='avaconf".$i."'  value='".$questionario->unidadesinvestigacao[$i]->avaconf."'>\n";
	    	echo "</td>";
	    	*/

	    	echo "</tr>";

    }
   	echo "<tr><td><input input type='image' src='../images/icon_new.png' name='navOption' value='Nova Unidade' onclick='if(formSubmited==0){return false;};document.questionario.operacao.value=6;'></td></tr>";
	
    echo "</table>";

     
	echo "<input type='hidden' name='apagaRegUnidade' />";
	echo "</fieldset>";
	
	echo "</div>";
	function getTipoUnidade($i) {
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_tipouniinvestigacao");
	
		echo "<SELECT name='tipo".$i."' onchange='showOtherTipoUnidade(selectedIndex);'>\n";
		echo "<option></option>\n";
		
		while ($row = mysql_fetch_assoc($lValues)) {	
			echo "<option value='".$row["ID"]."'".checkTipoUnidade($row["ID"],$i).">".$row["DESCRICAO"]."</option>\n";
		}
		echo "</SELECT>\n";
		$db->disconnect();
				
	}	
				
	function checkTipoUnidade($id,$i){
		global $questionario;
		if($questionario->unidadesinvestigacao[$i]->tipo==$id)
			return " SELECTED";
		else 
			return "";
	}
	
	
	function getUnidade($i) {
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_unidadesfct");
	
		echo "<SELECT name='unidade".$i."' onchange='mostraAvaliacaoUnidade(this.value,".$i.")'>\n";
		echo "<option></option>\n";
		
		while ($row = mysql_fetch_assoc($lValues)) {	
			echo "<option value='".$row["ID"]."'".checkUnidade($row["ID"],$i).">".$row["DESCRICAO"]."</option>\n";
		}
		echo "</SELECT><br />\n";
		$db->disconnect();
				
	}	
				
	function checkUnidade($id,$i){
		global $questionario;
		if($questionario->unidadesinvestigacao[$i]->unidade==$id)
			return "SELECTED";
		else 
			return "";
	}
	
?>