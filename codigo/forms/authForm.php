<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" type="text/css" href="styles/style_screen.css" media="screen" />
        <title>Formulário para Levantamento Geral de Dados da FMUP 2014 !!!</title>
    </head>
    <body>
        <form name="input" action="functions/validate.php" method="post">
            <fieldset class='head'>
                <img  src='images/fmupUP_transparencia.png' alt=''>
                <h1>Bem-vindo ao Levantamento Geral de Dados da FMUP - <strong>2014<trong></h1>
                <h1><p class='penhelp'><i>Welcome to the General Survey of FMUP</i></p></h1>
                <h2>Atividade científica efetuada durante o ano de 2014<br><i>Scientific activity during 2014</i></h2>
                <!--  <h3><a href='../helpfiles/PalestraFMUP2013.wmv' target='_blank'>Vídeo de ajuda</a></h3> -->
                <br>
                <h3>Prazo único: <font color='red'>18 de Janeiro de 2015</font></h3>
                <h3><i>Unique deadline: <font color='red'>18st January 2015</font></i></h3>
            </fieldset>
            <fieldset class='head'>
                <table cellpadding='10'>
                    <tr>
                        <td>
                            <label for='nomeaaa' class='float'>Nome de Utilizador <i>(Login)</i>:</label>
                            <input class='inp-text' type="text" name="login"/></td>
                       <!-- <td> <td rowspan='2'><p>  </td></td>-->					   
                    </tr>
                    <tr>
                        <td><label for='nomeaaa' class='float'>Palavra-chave <i>(Password)</i>:</label>
                            <input class='inp-text' type="password" name="password" /></td><td></td>
                    </tr>
					
            
                    <tr><td><small>O login é a parte que precede o @ no email institucional</small></td></tr>
                </table>
					<input class='op-button' type='image' src='images/icon_key.png' value='Entrar' onclick='document.input.submit();'/>Entrar/<i>Login</i></p>
                
                <?php    
                /* Validação dos erros no link */

                
                if ($msg == '401') {
                    echo "<br><font color='red'>Login/Password inválidos (deve usar o login e password do mail)</font></br>";
                    //echo "<br><font color='red'>O prazo para o levantamento de dados terminou.</font></br>";
                }
                if ($msg == '404') {
                    echo "<br><font color='red'>Login/Password inválidos (deve usar o login e password do mail)</font></br>";
                    //echo "<br><font color='red'>O prazo para o levantamento de dados terminou.</font></br>";
                }
                if ($msg == '405') {
                    echo "<br><font color='red'>O prazo de preenchimento terminou, neste momento apenas é permitido o acesso aos investigadores que tenham lacrado o levantamento!</font></br>";
                    //echo "<br><font color='red'>O prazo para o levantamento de dados terminou.</font></br>";
                }
                ?>
                
            </fieldset>   
            <fieldset class='head'>
            	<h4>Para qualquer esclarecimento poderá contatar o Science Manager através da ext. 5748 ou por email para sciman@med.up.pt<br><i>For any clarification please contact the Science Manager on extension 5748 or via email to sciman@med.up.pt</i></h4><br><br>
                <h4>Utilize o seu login e password da FMUPNet (o mesmo do email). <br>Para qualquer esclarecimento no que respeita à autenticação poderá contactar o serviço de Helpdesk : helpdesk@med.up.pt</h4>
                <h4><i>Use your FMUPNet login and password. <br>For any question regarding authentication please contact the helpdesk service : helpdesk@med.up.pt</i></h4>
            </fieldset>
        </form>
    </body>
</html>
