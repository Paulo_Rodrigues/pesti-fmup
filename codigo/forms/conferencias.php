<?php
echo "<div id='content17' style='display: inline;'>";
echo "<p><fieldset class='normal'>\n";
echo "<legend>Conferências Organizadas <a href='../helpfiles/Confs.JPG' style='color: #FFFF00' target='_blank'>Ajuda</a> /<i>Organized Conferences <a href='../helpfiles/Confs.JPG' style='color: #FFFF00' target='_blank'>Help</a></i></legend>\n";

echo "<p class='ppthelp'>Preencha a tabela seguinte notando que se para alguma conferência fez parte do Comitê Organizador Local (LOC) e do Comitê Organizador Científico (SOC) deve repetir tudo numa outra linha (à exceção dessa diferença). No final, colocar o link da página oficial da conferência,</p>";			

echo "<p class='penhelp'><i>Please fill the following table and note that if you were a member of both the Local Organizing Committee (LOC) and the Scientific Organizing Committee (SOC) for a given conference, you must repeat all information (except that one) in a new line. At the end, please provide the link of the official page of the conference.</i></p>";


echo "  
<table id='conforg' class='box-table-b'>        
    <thead> 
	    <tr>
	    	<th></th>
    					<th><u>Âmbito<p><i>Type</i></p></u></th>
      					<th><u>Tipo<p><i>Organization</i></p></u></th>
      					<th><u>Data Início<p><i>Begin Date</i></p></u></th>
      					<th><u>Data Fim<p><i>End Date</i></p></u></th>
      					<th><u>Título<p><i>Title</i></p></u></th>
      					<th><u>Local (cidade,país)<p><i>Location (town,country)</i></p></u></th>
      					<th><u>Nº Participantes<p><i>Participant Nr.</i></p></u></th>
      					<th><u>Link<p><i>Link</i></p></u></th>
	    </tr>
    </thead>       
<tbody>    
";

    foreach ($questionario->conferencias as $i => $value){
			echo "<tr>";
		   	echo "<td><input type='image' src='../images/icon_delete.png' name='navOption' value='Apagar Conferencia' onclick='if(formSubmited==0){return false;};document.questionario.apagaRegConferencia.value=".$questionario->conferencias[$i]->id.";document.questionario.operacao.value=28;' ></td>";
			echo "<td>";
			getAmbitoConf($i);
			echo "</td>";	
			echo "<td>";
			getTipoConf($i);
			echo "</td>";
			echo "<td><input class='inp-textAuto' type='text' id='confdatainicio_".$i."' name='confdatainicio_".$i."' size='11' value='".$questionario->conferencias[$i]->datainicio."' onfocus='calendario(\"confdatainicio_".$i."\");' onkeypress='validateCal(event);'></td>";	
			echo "<td><input class='inp-textAuto' type='text' id='confdatafim_".$i."' name='confdatafim_".$i."' size='11' value='".$questionario->conferencias[$i]->datafim."' onfocus='calendario(\"confdatafim_".$i."\");' onkeypress='validateCal(event);'></td>";	
			echo "<td><input class='inp-textAuto' type='text' name='conftitulo_".$i."' value='".$questionario->conferencias[$i]->titulo."'></td>";
			echo "<td><input class='inp-textAuto' type='text' name='conflocal_".$i."' value='".$questionario->conferencias[$i]->local."'></td>";
			echo "<td><input class='inp-textAuto' type='text' size='5' name='confparticipantes_".$i."' onkeypress='validate(event)' value='".$questionario->conferencias[$i]->participantes."'></p>\n</td>";
			echo "<td><textarea rows='2' cols='20' name='conflink_".$i."'>".$questionario->conferencias[$i]->link."</textarea></td>";
			echo "</tr>";	    	
    } 
    echo "</tbody><tfoot><tr><td><input input type='image' src='../images/icon_new.png' name='navOption' value='Nova Conferência' onclick='if(formSubmited==0){return false;};document.questionario.operacao.value=29;'></td></tr>";
    
    echo "</table>";
    echo "<br />";


	echo "<input type='hidden' name='apagaRegConferencia'/>";
	echo "</fieldset>";
	echo "</div>";
	function getTipoConf($i) {
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_tipoconf");
	
		echo "<SELECT name='conftipo_".$i."' >\n";
		echo "<option></option>\n";
		
		while ($row = mysql_fetch_assoc($lValues)) {	
			echo "<option value='".$row["ID"]."'".checkTipoConf($row["ID"],$i).">".$row["DESCRICAO"]."</option>\n";
		}
		echo "</SELECT><br />\n";
		$db->disconnect();
				
	}	
				
	function checkTipoConf($id,$i){
		global $questionario;
		if($questionario->conferencias[$i]->tipo==$id)
			return " SELECTED";
		else 
			return "";
	}
	
		function getAmbitoConf($i) {
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_ambitoconf");
	
		echo "<SELECT name='confambito_".$i."' >\n";
		echo "<option></option>\n";
		
		while ($row = mysql_fetch_assoc($lValues)) {	
			echo "<option value='".$row["ID"]."'".checkAmbConf($row["ID"],$i).">".$row["DESCRICAO"]."</option>\n";
		}
		echo "</SELECT><br />\n";
		$db->disconnect();
				
	}	
				
	function checkAmbConf($id,$i){
		global $questionario;
		if($questionario->conferencias[$i]->ambito==$id)
			return " SELECTED";
		else 
			return "";
	}
		
	?>