<?php
echo "<div id='content2' style='display: inline;'>";

echo "<fieldset class='normal'>\n";
//echo "<legend>Curso(s) superior(es) a frequentar ou concluído(s)/<i>Graduate course(s) attended or finished</i></legend>\n";
echo "<legend>Curso(s) a frequentar <a href='../helpfiles/Ajuda_Cursos.JPG' style='color: #FFFF00' target='_blank'>Ajuda</a>/<i> Course(s) being attended <a href='../helpfiles/Ajuda_Cursos.JPG' style='color: #FFFF00' target='_blank'>Help</a></i></legend>\n";			
 
echo "<p class='ppthelp'>É possível introduzir ciclos de estudos frequentados em simultâneo (basta repetir o ano letivo). No caso de cursos FMUP inicie a escrita do seu nome em “Curso” – automaticamente aparece a lista e os seguintes dois campos são também preenchidos. Caso o curso não seja da FMUP, escreva parte do nome da Instituição (e.g. Barcelona): o seu nome completo pode constar na lista (selecione-o). Indique a percentagem do seu tempo formalmente dedicada ao(s) curso(s).Escreva o nome completo do(s) seu(s) orientador(es); se for(em) da FMUP aparece(m) automaticamente; se não for(em) preencha o seu nome completo e o  nome da instituição.</p>";			

echo "<p class='penhelp'><i>It is possible to introduce study cycles taken simultaneously, by repeating the school year. In the case of FMUP courses please start by writing its name in “Course” – the list shows up automatically and the next two fields will also be filled. If the course is not FMUP, please write part of the Institution name (e.g. Barcelona): its complete name might be in the list (please pick it). Please indicate the percentage of your formal week that is devoted to the course(s).Please write the full name of your supervisor(s); if member(s) of FMUP, the name(s) will automatically appear; if not, please fill the(ir) full name(s) and the(ir) institution.</i></p>";

echo "
<table id='hab' class='box-table-b'>
    <thead>
	    <tr>
    					<th data-sorter='text'></th>
      					<th><u>Ano de Início<p><i>Start Year</i></p></u></th>
      					<th><u>Ano de Fim<p><i>End Year</i></p></u></th>
      					<th><u>Curso<p><i>Course</i></p></u></th>
      					<th><u>Instituição<p><i>Institution</i></p></u></th>
      					<th><u>Grau/Título<p><i>Degree/Title</i></p></u></th>
      					<th><u>Distinção<p><i>Distinction</i></p></u></th>
						<th><u>Bolsa<p><i>Scholarship</i></p></u></th>
						<th><u>Título da Tese em Português<p><i>Thesis title in Portuguese</i></p></u></th>
						<th><u>Percentagem<p><i>Percentage</i></p></u></th>
						<th><u>Orientador<p><i>Supervisor</i></p></u></th>
						<th><u>Instituição do Orientador<p><i>Supervisor Institution</i></p></u></th>
						<th><u>COrientador<p><i>COSupervisor</i></p></u></th>
						<th><u>Instituição do Coorientador<p><i>CoSupervisor Institution</i></p></u></th>
	    </tr>
    </thead>
<tbody>
";


    foreach ($questionario->habilitacoes as $i => $value){
			echo "<tr>";
		   	echo "<td><input type='image' src='../images/icon_delete_s.png' name='navOption' value='Apagar Habilitacao' onclick='if(formSubmited==0){return false;};document.questionario.apagaRegHabilitacao.value=".$questionario->habilitacoes[$i]->id.";document.questionario.operacao.value=11;' ></td>";
			echo "<td>";
						getAnoInicio($i);
			echo "</td>";
			echo "<td>";
			getAnoFim($i);
			echo "</td>";
			//echo "<td><input class='inp-textAuto' type='text' name='curso".$i."' value='".$questionario->habilitacoes[$i]->curso."'>\n</td>";
			
			echo "<td><input class='inp-textAuto' type='text' id='habcurso_".$i."' size='20' name='habcurso_".$i."' value='".$questionario->habilitacoes[$i]->curso."' onkeyup='mostraHabCurso(this.value,".$i.");'><div class='divlookup' id='livesearchhabcurso_".$i."'></div></td>";
			echo "<td><input class='inp-textAuto' type='text' id='habinstituicao_".$i."' size='20' name='habinstituicao_".$i."' value='".$questionario->habilitacoes[$i]->instituicao."' onkeyup='mostraHabInstituicao(this.value,".$i.");'><div class='divlookup' id='livesearchhabinstituicao_".$i."'></div></td>";
				
			
			//echo "<td><input class='inp-textAuto' type='text' name='instituicao".$i."' value='".$questionario->habilitacoes[$i]->instituicao."'></td>";
			echo "<td>";
			getGrau($i);	
	    	echo "</td>";
	    	echo "<td>";
	    	getDistincao($i);
			/*if ($questionario->habilitacoes[$i]->distincao==1) {
			//echo "<input type='checkbox' name='responsavel".$i."' value='".$questionario->unidadesinvestigacao[$i]->responsavel."' checked></p>\n";
				echo "<input type='checkbox' name='distincao".$i."' value='1' checked>\n";
			}else {
				echo "<input type='checkbox' name='distincao".$i."'  value='1'>";
			}*/
	    	echo "</td>";
	    	echo "<td>";
	    	getBolsa($i);	
	    	echo "</td>";
	    	echo "<td><textarea class='inp-textAuto' name='titulo".$i."'>".$questionario->habilitacoes[$i]->titulo."</textarea>\n</td>";
		    //echo "<td><input class='inp-textAuto' type='text' name='percentagem".$i."' value='".$questionario->habilitacoes[$i]->percentagem."'>\n</td>";
		    echo "<td>".getPercentagem("percentagemHab".$i,$questionario->habilitacoes[$i]->percentagem)."\n</td>";
		    echo "<td><input class='inp-textAuto' type='text' id='haborientador_".$i."' name='haborientador_".$i."' value='".$questionario->habilitacoes[$i]->orientador."' onkeyup='mostraHabOrientador(this.value,".$i.");'><div class='divlookup' id='livesearchhaborientador_".$i."'></div></td>";
		    echo "<td><input class='inp-textAuto' type='text' id='haboinstituicao_".$i."' size='20' name='haboinstituicao_".$i."' value='".$questionario->habilitacoes[$i]->oinstituicao."' onkeyup='mostraHabOInstituicao(this.value,".$i.");'><div class='divlookup' id='livesearchhaboinstituicao_".$i."'></div></td>";
		    echo "<td><input class='inp-textAuto' type='text' id='habcorientador_".$i."' name='habcorientador_".$i."' value='".$questionario->habilitacoes[$i]->corientador."' onkeyup='mostraHabCOrientador(this.value,".$i.");'><div class='divlookup' id='livesearchhabcorientador_".$i."'></div></td>";
		    echo "<td><input class='inp-textAuto' type='text' id='habcoinstituicao_".$i."' size='20' name='habcoinstituicao_".$i."' value='".$questionario->habilitacoes[$i]->coinstituicao."' onkeyup='mostraHabCOInstituicao(this.value,".$i.");'><div class='divlookup' id='livesearchhabcoinstituicao_".$i."'></div></td>";
		    echo "</tr>";
		    	    	
    } 
    echo "</tbody><tfoot><tr><td><input type='image' src='../images/icon_new_s.png' name='navOption' value='Nova Habilitacao' onclick='if(formSubmited==0){return false;};document.questionario.operacao.value=4;'></td></tr></tfoot>";
    
    echo "</table>";
    echo "<br />";


	echo "<input type='hidden' name='apagaRegHabilitacao'/>";
	echo "</fieldset>";
	
	include 'acoesformacaoFrequentada.php';
	
	echo "</div>";
	
	
	
// 	function getAnoInicio($i){
	
// 		$db = new Database();
// 		$lValues =$db->getLookupValuesAnoInicio("lista_anoslectivos");
	
// 		echo "<select name='anoinicio".$i."' id='anoinicio".$i."'>\n";
// 		//echo "<option value=''></option>\n";
		
// 		while ($row = mysql_fetch_assoc($lValues)) {	
// 			echo "<option value='".$row["ID"]."'".checkAnoInicio($row["ID"],$i).">".$row["DESCRICAO"]."</option>\n";
// 		}
// 		echo "</select><br />\n";
// 		$db->disconnect();				
// 	}	
				
// 	function checkAnoInicio($id,$i){
// 		global $questionario;
// 		if($questionario->habilitacoes[$i]->anoinicio==$id)
// 			return " selected='selected'";
// 		else 
// 			return "";
// 	}
	
	function getAnoInicio($i) {
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_anoslectivos");
	
		echo "<select name='anoinicio".$i."' id='anoinicio".$i."'>\n";
		//echo "<option value=''></option>\n";
	
		while ($row = mysql_fetch_assoc($lValues)) {
				echo "<option value='".$row["ID"]."'".checkAnoInicio($row["ID"],$i).">".$row["DESCRICAO"]."</option>\n";
		}
		echo "</select><br />\n";
		$db->disconnect();
	
			
	}
	
	function checkAnoInicio($id,$i){
		global $questionario;
	
		if($questionario->habilitacoes[$i]->anoinicio=="" && $id=='-1')
			return " selected='selected'";
		else{
			if($questionario->habilitacoes[$i]->anoinicio==$id)
				return " selected='selected'";
			else
				return "";
		}
	}
	
	
	
	
	function getAnoFim($i) {
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_anoslectivos");
	
		echo "<select name='anofim".$i."' id='anofim".$i."'>\n";
		//echo "<option value=''></option>\n";
		
		while ($row = mysql_fetch_assoc($lValues)) {
			if($row["ID"]=='94')	
				echo "<option value='".$row["ID"]."'".checkAnoFim($row["ID"],$i).">".$row["DESCRICAO"]." - Concluído</option>\n";
			else
				echo "<option value='".$row["ID"]."'".checkAnoFim($row["ID"],$i).">".$row["DESCRICAO"]."</option>\n";
		}
		echo "</select><br />\n";
		$db->disconnect();
		
			
	}
				
	function checkAnofim($id,$i){
		global $questionario;

		if($questionario->habilitacoes[$i]->anofim=="" && $id=='-1')
			return " selected='selected'";
		else{
			if($questionario->habilitacoes[$i]->anofim==$id)
				return " selected='selected'";
			else 
				return "";
		}
	}
	
	function getGrau($i) {
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_graucursos");
	
		echo "<select id='habgrau_".$i."' name='habgrau_".$i."' >\n";
		echo "<option value=''></option>\n";
		
		while ($row = mysql_fetch_assoc($lValues)) {	
			echo "<option value='".$row["ID"]."'".checkGrau($row["ID"],$i).">".$row["DESCRICAO"]."</option>\n";
		}
		echo "</select><br />\n";
		$db->disconnect();
				
	}	
				
	function checkGrau($id,$i){
		global $questionario;
			if($questionario->habilitacoes[$i]->grau=="" && $id=='-1')
			return " selected='selected'";
		else{
			if($questionario->habilitacoes[$i]->grau==$id)
				return " selected='selected'";
			else
				return "";
		}
	}
	
	
	function getDistincao($i) {
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_distincaoValores");
	
		echo "<select name='distincao".$i."' id='distincao".$i." >\n";
		echo "<option value=''></option>\n";
	
		while ($row = mysql_fetch_assoc($lValues)) {
			echo "<option value='".$row["ID"]."'".checkDistincao($row["ID"],$i).">".$row["DESCRICAO"]."</option>\n";
		}
		echo "</select><br />\n";
		$db->disconnect();
	
	}
	
	function checkDistincao($id,$i){
		global $questionario;
			if($questionario->habilitacoes[$i]->distincao=="" && $id=='-1')
			return " selected='selected'";
		else{
			if($questionario->habilitacoes[$i]->distincao==$id)
				return " selected='selected'";
			else
				return "";
		}
	}
	
	
	function getBolsa($i) {
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_TipoBolsas");
	
		echo "<select name='bolsa".$i."' id='bolsa".$i."' >\n";
		echo "<option value=''></option>\n";
	
		while ($row = mysql_fetch_assoc($lValues)) {
			echo "<option value='".$row["ID"]."'".checkBolsa($row["ID"],$i).">".$row["DESCRICAO"]."</option>\n";
		}
		echo "</select><br />\n";
		$db->disconnect();
	
	}
	
	function checkBolsa($id,$i){
		global $questionario;
		if($questionario->habilitacoes[$i]->bolsa=="" && $id=='-1')
			return " selected='selected'";
		else{
			if($questionario->habilitacoes[$i]->bolsa==$id)
				return " selected='selected'";
			else
				return "";
		}

	}
	
	function getPercentagem($cat,$valor){
	
		$sel="<select name='".$cat."' id='".$cat."' >";
	
		for($i=100;$i>=0;$i=$i-1)
			$sel=$sel."<option value='".$i."' ".checkSelected($i,$valor).">".$i."</option>";
	
		$sel=$sel."</select>";
		return $sel;
	}
	function checkSelected($i,$valor){
	
		if($i==$valor){
			return " selected='selected'";
		}else{
			return "";
		}
	
	}
	
	
	?>