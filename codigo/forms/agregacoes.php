<?php

echo "<div id='content3' style='display: inline;'>";
echo "<fieldset class='normal'>\n";
echo "<legend>Agregação/<i>Aggregation</i></legend>\n";

echo "<table id='agrega' class='box-table-a'>";

echo "<tr><td>Se concluiu, com sucesso, Provas de Agregação em $anoactual assinale :<p><i>Please select, if you successfully finished the Aggregation Exam in $anoactual:</i></p></td><td>";

if ($questionario->agregacao->agregacao == 1) {
    echo "<input type='checkbox' id='agregacao' name='agregacao' value='1' checked onclick='showhideUnanimidade()'></p>\n";
} else {
    echo "<input type='checkbox' id='agregacao' name='agregacao'  value='1' onclick='showhideUnanimidade()'>";
}
echo "</td></tr>";

if($questionario->agregacao->agregacao == 1){
		echo "<tr id='linhanunanimidade' style='visibility: visible'><td>Se a aprovação foi por unanimidade assinale :<p><i>Please select, if the aggregation approval was unanimous:</i></p><td>";
}else{
		echo "<tr id='linhanunanimidade' style='visibility: hidden'><td>Se a aprovação foi por unanimidade assinale :<p><i>Please select, if the aggregation approval was unanimous:</i></p><td>";
}
if ($questionario->agregacao->unanimidade == 1) {
    //echo "<input type='checkbox' name='responsavel".$i."' value='".$questionario->unidadesinvestigacao[$i]->responsavel."' checked></p>\n";
    echo "<input type='checkbox' id='unanimidade' name='unanimidade' value='1' checked></p>\n";
} else {
    echo "<input type='checkbox' id='unanimidade' name='unanimidade' value='1'>";
}
echo "</td></tr>";
echo "<tr><td>
		Se pretende submeter-se no futuro a Provas de Agregação, indique o ano previsto:<p><i>If you intend in the future to make an Aggregation Exam plese indicate the year expected:</i></p>
		</td><td>";
echo "<input class='inp-textAuto' type='text' name='dataprevista' maxlength='10' size='10' onkeypress='validate(event)' value='" . $questionario->agregacao->dataprevista . "'";
echo "</td></tr>";
echo "</table>";

echo "</fieldset>";
echo "</div>";
?>