<?php
echo "<div id='content7' style='display: inline;'>";
echo "<p><fieldset class='normal'>\n";
echo "<legend>Missões <a href='../helpfiles/Missoes.JPG' style='color: #FFFF00' target='_blank'>Ajuda</a> /<i>Missions <a href='../helpfiles/Missoes.JPG' style='color: #FFFF00' target='_blank'>Help</a></i></legend>\n";
			
echo "<p class='ppthelp'>Descrever todas as missões científicas efetuadas fora da FMUP em ".$anoactual.", dentro e fora do país. Excluem-se saídas para conferências, seminários, etc.mas incluem-se estágios científicos. Basta escrever algumas letras do nome para que a Instituição apareça numa lista – selecionar. O respetivo país também aparece. Caso a Instituição seja nova, preencher tudo manualmente.</p>";			

echo "<p class='penhelp'><i>Please describe here all scientific missions that you have conducted outside of FMUP in ".$anoactual.", in Portugal or abroad. Excluded are trips to conferences, seminars, etc. but includes scientific training. It is enough to key a few characters from its name for the Institution to show up in a list – please select it. The respective country also shows up. For a new Institution please fill it all manually.</i></p>";

echo "<table id='mis' class='box-table-b'>
    					<tr><th></th>
      					<th>Data de Inicio<p><i>Start Date</i></p></th>
      					<th>Data de Fim<p><i>End Date</i></p></th>
      					<th>Motivacao<p><i>Motivation</i></p></th>
      					<th>Instituição de Acolhimento<p><i>Hosting Institution</i></p></th>
						<th>Pais<p><i>Country</i></p></th>
						<th>Âmbito de Tese ?<p><i>Context of thesis ?</i></p></th></tr>";
			    
    foreach ($questionario->missoes as $i => $value){
			echo "<tr>";
		   	echo "<td><input type='image' src='../images/icon_delete.png' name='navOption' value='Apagar Missao' onclick='if(formSubmited==0){return false;};document.questionario.apagaRegMissao.value=".$questionario->missoes[$i]->id.";document.questionario.operacao.value=65;' ></td>";

			echo "<td><input class='inp-textAuto' type='text' id='missoesdatainicio_".$i."' name='missoesdatainicio_".$i."' size='11' value='".$questionario->missoes[$i]->datainicio."' onfocus='calendario(\"missoesdatainicio_".$i."\");' onkeypress='validateCal(event);'></td>";
			echo "<td><input class='inp-textAuto' type='text' id='missoesdatafim_".$i."' name='missoesdatafim_".$i."' size='11' value='".$questionario->missoes[$i]->datafim."' onfocus='calendario(\"missoesdatafim_".$i."\");' onkeypress='validateCal(event);'></td>";
			echo "<td><input class='inp-textAuto' type='text' id='missoesmotivacao_".$i."' size='50' name='missoesmotivacao_".$i."' value='".$questionario->missoes[$i]->motivacao."'</td>";
			echo "<td><input class='inp-textAuto' type='text' id='missoesinstituicao_".$i."' size='40' name='missoesinstituicao_".$i."' value='".$questionario->missoes[$i]->instituicao."' onkeyup='mostraMissoesInstituicao(this.value,".$i.");'><div class='divlookup' id='livesearchmissoesinstituicao_".$i."'></div></td>";
			getPaisesCol($i);
			echo "<td><input class='inp-textAuto' type='checkbox' name='missoesambtese_".$i."' value='1' ".checkAmbTese($i)."></td>";
	    	echo "</tr>";	    	
    } 
    echo "<tr><td><input type='image' src='../images/icon_new.png' name='navOption' value='Nova Missao' onclick='if(formSubmited==0){return false;};document.questionario.operacao.value=66;'></td></tr>";
    
    echo "</table>";
    echo "<br />";


	echo "<input type='hidden' name='apagaRegMissao'/>";
	
	echo "</fieldset>";
	
	
	echo "</div>";
	
	function getPaisesCol($i){
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_paises");
	
		echo "<td><SELECT id='missoespais_".$i."' name='missoespais_".$i."' >\n";
		echo "<option></option>\n";
	
		while ($row = mysql_fetch_assoc($lValues)) {
			echo "<option value='".$row["ID"]."'".checkPaisCol($row["ID"],$i).">".$row["DESCRICAO"]."</option>\n";
		}
		echo "</SELECT></td><br />\n";
		$db->disconnect();
	
	}
	
	function checkPaisCol($id,$i){
		global $questionario;
		if($questionario->missoes[$i]->pais==$id)
			return " SELECTED";
		else
			return "";
	}
	
	function checkAmbTese($i){
		global $questionario;
		if($questionario->missoes[$i]->ambtese==1)
			return "CHECKED";
		else
			return "";
	}
		
	?>