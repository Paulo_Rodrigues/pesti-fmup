<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt" xml:lang="pt">
    <head>
        <meta name="description" content=""/>
        <meta name="keywords" content=""/>
        <meta name="author" content=""/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <link rel="stylesheet" type="text/css" href="../styles/style_screen2.css" media="screen"/>
        <link rel="stylesheet" type="text/css" media="all" href="../styles/jsDatePick_ltr.min.css"/>
        <title>Levantamento Geral de Dados da FMUP 2014</title>
        <script type="text/javascript" src="../js/jquery-1.11.1.js"></script>
        <script type="text/javascript" src="../js/jsDatePick.jquery.min.1.3.js"></script>
        <script type="text/javascript" src="../js/funcoes.js"></script>
        <script type='text/javascript' src='../js/jquery.tablesorter.js'></script>
        
        
    </head>

    <?php
        session_start();
    //ini_set('session.gc_maxlifetime', 1*60);
        $a = session_id();
        if (empty($a)) {
            break("erro de sessão");
        }

        require_once '../classlib/Questionario.class.inc';
        require_once '../functions/parsingISI.php';
        require_once '../functions/parsingPubmed.php';

        $anoactual = 2014;

        $operacao2 = @$_POST['operacao'];

        $activeTab = @$_POST['activeTab'];
        $saveTab = @$_POST['saveTab'];
        
        $apagaRegEx = @$_POST['apagaRegExperiencia'];
        $apagaRegHab = @$_POST['apagaRegHabilitacao'];
        $apagaPublicacaoISI = @$_POST['apagaPublicacaoISI'];
        $apagaPublicacaoPUBMED = @$_POST['apagaPublicacaoPUBMED'];
        $apagaRegUni = @$_POST['apagaRegUnidade'];
        $apagaRegNom = @$_POST['apagaRegNome'];
        $apagaRegCol = @$_POST['apagaRegColaborador'];
        $apagaRegColInt = @$_POST['apagaRegColaboradorInterno'];
        $apagaProj = @$_POST['apagaProjecto'];
        $apagaCT = @$_POST['apagaCT'];
        $apagaCargoFMUP = @$_POST['apagaRegCargoFMUP'];
        $apagaCargoUP = @$_POST['apagaRegCargoUP'];
        $apagaCargoHospital = @$_POST['apagaRegCargoHospital'];
        $apagaDirCurso = @$_POST['apagaRegDirCurso'];
        $apagaPremio = @$_POST['apagaRegPremio'];
        $apagaConf = @$_POST['apagaRegConferencia'];
        $apagaSC = @$_POST['apagaRegSC'];
        $apagaRede = @$_POST['apagaRegRede'];
        $apagaPMP = @$_POST['apagaRegPubManualPatentes'];
        $apagaPMI = @$_POST['apagaRegPubManualInternacional'];
        $apagaPMN = @$_POST['apagaRegPubManualNacional'];
        $apagaOrientacao = @$_POST['apagaRegOrientacao'];
        $apagaAF = @$_POST['apagaRegAcaoFormacao'];
        $apagaAFF = @$_POST['apagaRegAcaoFormacaoFrequentada'];
        
        $apagaProduto = @$_POST['apagaRegProduto'];
        $apagaArbRevistas = @$_POST['apagaArbitagensRevistas'];
        $apagaMissao = @$_POST['apagaRegMissao'];
        $apagaAcreditacao = @$_POST['apagaRegAcreditacao'];
        $apagaAD = @$_POST['apagaRegDivulgacao'];
        //ALTERACAO
        //Paulo Rodrigues (prodrigues@med.up.pt)
        $apagaConsultoria = @$_POST['apagaRegConsultoria'];
		$apagaCargoC = @$_POST['apagaRegCargoC'];

		
		$apagaApresentacaoConf= @$_POST['apagaRegApresentacaoConferencia'];
		
		$append_pub_isi=@$_POST['append_pub_isi'];
		$append_pub_pubmed=@$_POST['append_pub_pubmed'];
		
		//echo "<pre>"; print_r($_REQUEST); echo "</pre>";
    //echo "timeout: ".ini_get('session.gc_maxlifetime');
    //echo "<br/>session status: <br/>session name: ".session_name()." <br/> SID: ".SID."<br/>session_id(): ".session_id()."<br/>COOKIE: ".$_COOKIE["PHPSESSID"];

        if ($activeTab == "") {
            $activeTab = 0;
        }
        if ($saveTab == "") {
            $saveTab = 0;
        }

		//echo $activetab;
        echo "<body onload='makeactive(" . $activeTab . ");'>";
        
//         echo "
// 		<script type='text/javascript'>
        
//             $(document).ready(function() {
        		
//         		$('.box-table-b').tablesorter({ 
//         			debug: true,
//         			ignoreCase: true,
// 					cancelSelection: true,
//         			emptyTo: 'none/zero',
// 			        textExtraction: function(node) {
// 			            // Check if option selected is set
// 			            if ($(node).find('option:selected').text() != '') {
//         					return $(node).find('option:selected').text();
// 			            }
// 			            else{
// 							if($(node).find('input[type=text]').val() != ''){
//         						return $(node).find('input[type=text]').val();
//         					}else{
// 								return $(node).text();
// 								}
// 							}
// 			        }
// 				});

//             });
// 	</script>";
        

        echo "<div id='statusMSG'><fieldset class='warning'>Em processamento ...</fieldset></div>";

        if (isset($_SESSION['questionario'])) {
            $questionario = unserialize($_SESSION['questionario']);
            
			// echo "<p>A operação é ".$operacao2;
			// echo "<br/>O activetab é ".$activeTab;
			// echo "<br/>O savetab é ".$saveTab;
			// echo "<br/>O apaga PMP  é ".$apagaPMP;
			// echo "<br/>O apaga PMI  é ".$apagaPMI;
			// echo "<br/>O apaga PMN  é ".$apagaPMN;
			// echo "</p>";

            // print_r ($questionario->publicacoesISI);
            //echo "Operação:".$operacao2;

            switch ($operacao2) {
                case '0': { // Opção de entrada
                        updateLocalQuestionarioData();
                        saveQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '1': { // GUARDAR INFORMAÇÃO E CONTINUAR
                        updateLocalQuestionarioData();
                        saveQuestionarioData();
                        auditoria($questionario->investigador->login, "", "GRAVOU TODO INQUERITO", "", "");
                        renderForms("");
                    }
                    break;
                case '2': { // GUARDAR INFORMAÇÃO E SAIR
                        echo "Grava e sai";
                        updateLocalQuestionarioData();
                        saveQuestionarioData();
                        formLogout();
                    }
                    break;
                case '3': { // SAIR SEM GUARDAR AS ALTERAÇÕES
                        echo "sai sem gravar";
                        formLogout();
                    }
                    break;
                case '4': { // ADICIONAR NOVO GRAU
                        $questionario->addHabilitacao();
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '5': { // Novo Contrato
                        $questionario->addExperiencia();
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '6': { // Nova Unidade
                        $questionario->addUnidade();
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '7': { // Novo Nome
                        $questionario->addNome();
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '8': { // Novo Colaborador
                        $questionario->addColaborador();
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '9': { // Novo Projecto
                        $questionario->addProjecto();
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '10': { // Apagar Experiência
                        //echo "apaga: ".$apagaRegEx;
                        $questionario->apagaRegistoExperiencia($apagaRegEx);
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '11': { //Apagar Habilitacao
                        //echo "apaga: ".$apagaRegHab;
                        $questionario->apagaRegistoHabilitacao($apagaRegHab);
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '12': { //Apagar Publicacao ISI
                        //echo "apaga: ".$apagaRegHab;
                        $questionario->apagaPublicacaoISI($apagaPublicacaoISI);
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '13': { // Apagar Publicacao PUBMED
                        //echo "apaga: ".$apagaRegHab;
                        $questionario->apagaPublicacaoPUBMED($apagaPublicacaoPUBMED);
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '14': { // Apagar Unidade
                        //echo "apaga: ".$apagaRegUni;
                        $questionario->apagaRegistoUnidade($apagaRegUni);
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '15': { //Apagar Nome
                        //echo "apaga: ".$apagaRegUni;
                        $questionario->apagaRegistoNome($apagaRegNom);
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '16': { //Apagar Colaborador
                        //echo "apaga: ".$apagaRegUni;
                        $questionario->apagaRegistoColaborador($apagaRegCol);
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '17': { //Apagar Projecto
                        //echo "apaga: ".$apagaRegUni;
                        $questionario->apagaProjecto($apagaProj);
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '18': { // Enviar Ficheiro ISI
                        $msg = "";
                        $msg = parsefileISI($questionario->investigador->id,$append_pub_isi);
                        updateLocalQuestionarioData();
                        renderForms($msg);
                    }
                    break;
                case '19': { // Enviar Ficheiro PUBMED
                        $msg = "";
                        $msg = parsefilePubmed($questionario->investigador->id,$append_pub_pubmed);
                        updateLocalQuestionarioData();
                        renderForms($msg);
                    }
                    break;
                case '20': { //Apaga Cargo FMUP
                        //echo "apaga: ".$apagaRegUni;
                        $questionario->apagaCargoFMUP($apagaCargoFMUP);
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '21': { // Add CArgo FMUP
                        $questionario->addCargoFMUP();
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '22': { //Apagar Cargo UP
                        //echo "apaga: ".$apagaRegUni;
                        $questionario->apagaCargoUP($apagaCargoUP);
                        updateLocalQuestionarioData();
                        saveQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '23': { // Novo Cargo UP
                        echo "ADCIONAR CARGO";
                        $questionario->addCargoUP();
                        updateLocalQuestionarioData();
                        saveQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '24': { //Apagar Dir Cursos
                        //echo "apaga: ".$apagaRegUni;
                        $questionario->apagaDirCurso($apagaDirCurso);
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '25': { // Novo Dir Cursos
                        $questionario->addDirCurso();
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '26': { //Apagar Premio
                        //echo "apaga: ".$apagaRegUni;
                        $questionario->apagaPremio($apagaPremio);
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '27': { // Novo Premio
                        $questionario->addPremio();
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '28': { //Apagar Conferencia
                        //echo "apaga: ".$apagaRegUni;
                        $questionario->apagaConferencia($apagaConf);
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '29': { // Novo Conferencia
                        $questionario->addConferencia();
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '30': { //Apagar Sociedade Cientifica
                        //echo "apaga: ".$apagaRegUni;
                        $questionario->apagaSC($apagaSC);
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '31': { // Novo Sociedade cientifica
                        $questionario->addSC();
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                // 32 era da publicacao manual geral passou com a subdivisao deixou de existir, usar para testes
                case '32': {
                    //doStuff();
                    //updateLocalQuestionarioData();
                    //renderForms("");
                }
                case '33': { // Nova Pub Manual J
                        $questionario->addPMI("J");
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '34': { // Nova Pub Manual PRP
                        $questionario->addPMI("PRP");
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '35': { // Nova Pub Manual
                        $questionario->addPMI("JA");
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '36': { // Nova Pub Manual
                        $questionario->addPMI("CP");
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '37': { // Nova Pub Manual
                        $questionario->addPMI("OA");
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '38': { // Nova Pub Manual
                        $questionario->addPMI("L");
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '39': { // Nova Pub Manual
                        $questionario->addPMI("CL");
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '40': { // Nova Pub Manual
                        $questionario->addPM("A");
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '41': { // Nova Pub Manual
                        $questionario->addPMN("JN");
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '42': { // Nova Pub Manual
                        $questionario->addPMN("PRPN");
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '43': { // Nova Pub Manual
                        $questionario->addPMN("CPN");
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '44': { // Nova Pub Manual
                        $questionario->addPMN("JAN");
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '45': { // Nova Pub Manual
                        $questionario->addPMN("CPN");
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '46': { // Nova Pub Manual
                        $questionario->addPMN("OAN");
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '47': { // Nova Pub Manual
                        $questionario->addPMN("LN");
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '48': { //Apagar Acao
                        //echo "apaga: ".$apagaRegUni;
                        $questionario->apagaAcaoFormacao($apagaAF);
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '49': { // Novo Acao
                        $questionario->addAcaoFormacao();
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '50': { //Apagar Produto
                        //echo "apaga: ".$apagaRegUni;
                        $questionario->apagaProduto($apagaProduto);
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '51': { // Novo Produto
                        $questionario->addProduto();
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '52': { // Novo Colaborador
                        $questionario->addColaboradorInterno();
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '53': { //Apagar Colaborador
                        //echo "apaga: ".$apagaRegUni;
                        $questionario->apagaRegistoColaboradorInterno($apagaRegColInt);
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '54': { //Validação
                        updateLocalQuestionarioData();
                        saveQuestionarioData();
                        $err = validaDados();
                        auditoria($questionario->investigador->login, "", "VALIDOU DADOS INQUERITO", "", "");
                        renderForms($err);
                    }
                    break;
                case '55': { // Novo Arbitragem Revistas
                        $questionario->addArbitragensRevistas();
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '56': { //Apagar Arbitragem Revistas
                        //echo "apaga: ".$apagaRegUni;
                        $questionario->apagaArbitragensRevistas($apagaArbRevistas);
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '57': { //LACRAR
                        //echo "apaga: ".$apagaRegUni;
                        lacrarQuestionario();
                        finalForm();
                    }
                    break;
                case '58': {
                        //VOLTAR
                        renderForms("");
                    }
                    break;
                case '59': {
                        $questionario->addPMN("CLN");
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '60': {
                        $questionario->addPMP("P");
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '61': { //Apagar Pub Manual
                        //echo "apaga: ".$apagaRegUni;
                        $questionario->apagaPublicacaoMANUALPatentes($apagaPMP);
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '62': { //Apagar Pub Manual
                        //echo "apaga: ".$apagaRegUni;
                        $questionario->apagaPublicacaoMANUALInternacional($apagaPMI);
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '63': { //Apagar Pub Manual
                        //echo "apaga: ".$apagaRegUni;
                        $questionario->apagaPublicacaoMANUALNacional($apagaPMN);
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '64': { //Submeter
                        updateLocalQuestionarioData();
                        saveQuestionarioData();
                        $err = validaDados();
                        auditoria($questionario->investigador->login, "", "VALIDOU DADOS INQUERITO", "", "");
                        if ($err == "Sem Erros") {
                            finalForm();
                            auditoria($questionario->investigador->login, "", "SUBMETEU DADOS INQUERITO", "", "");
                        } else {
                            renderForms($err);
                        }
                    }
                    break;
                case '65': { //Submeter
                        //echo "apaga: ".$apagaMissao;
                        $questionario->apagaMissao($apagaMissao);
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '66': { //Submeter
                        $questionario->addMissao();
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '67': { //Apagar Orientacao
                        $questionario->apagaOrientacao($apagaOrientacao);
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '68': { // Novo Orietnacao
                        $questionario->addOrientacao();
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '69': { //Apagar Orientacao
                        $questionario->apagaAcreditacao($apagaAcreditacao);
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '70': { // Novo Orietnacao
                        $questionario->addAcreditacao();
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '71': { //Apagar Divulgacao
                        $questionario->apagaAcaoDivulgacao($apagaAD);
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '72': { // Novo Divulgacao
                        $questionario->addAcaoDivulgacao();
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '73': { //Apaga Cargo Hospital
                       // echo "apaga: ".$apagaCargoHospital;
                        $questionario->apagaCargoHospital($apagaCargoHospital);
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '74': { // Add CArgo Hospital
                        $questionario->addCargoHospital();
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '75': { //Apaga Rede
                        //echo "apaga: ".$apagaRegUni;
                        $questionario->apagaRede($apagaRede);
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                case '76': { // Add Rede
                        $questionario->addRede();
                        updateLocalQuestionarioData();
                        renderForms("");
                    }
                    break;
                //ALTERACAO
                //Paulo Rodrigues (prodrigues@med.up.pt)
                case '77': { // Apagar Consultoria				
                    $questionario->apagaConsultoria($apagaConsultoria);
                    updateLocalQuestionarioData();
                    saveQuestionarioData();
                    renderForms("");
                }
                break;
                case '78': { // Adicionar Consultoria
                    $questionario->addConsultoria();
                    updateLocalQuestionarioData();
                    saveQuestionarioData();
                    renderForms("");
                }
                break; 
				case '79': { // Add CArgo Comissao				
					$questionario->addCargoC();						
					updateLocalQuestionarioData();
					renderForms("");
				}
				break;
				case '80': { //Apaga Cargo Comissao					
					$questionario->apagaCargoC($apagaCargoC);				
					updateLocalQuestionarioData();//actualiza variavel $questionario					
					renderForms("");
				}
				break;
				case '81': { //Ordena por data de inicio Cargos-Comissoes ASC				
					updateLocalQuestionarioData();//actualiza variavel $questionario							
					$questionario->ordenaComissoesC("A");						
					renderForms("");
				}
				break;
				case '82': { //Ordena por data de inicio Cargos-Comissoes DESC				
					updateLocalQuestionarioData();//actualiza variavel $questionario							
					$questionario->ordenaComissoesC("D");				
					renderForms("");
				}
				break;
				case '83': { // Add ApreConf
					$questionario->addApresentacaoConferencia();
					updateLocalQuestionarioData();
					renderForms("");
				}
				break;
				case '84': { //Apaga ApreConf
					$questionario->apagaApresentacaoConferencia($apagaApresentacaoConf);
					updateLocalQuestionarioData();//actualiza variavel $questionario
					renderForms("");
				}
				break;
				case '85': { //Apaga CT
					$questionario->apagaClinicalTrial($apagaCT);
					updateLocalQuestionarioData();//actualiza variavel $questionario
					renderForms("");
				}
				break;
				case '86': { // Add CT
					$questionario->addClinicalTrial();
					updateLocalQuestionarioData();
					renderForms("");
				}
				break;
				case '87': { //Apaga CT
					$questionario->apagaAcaoFormacaoFrequentada($apagaAFF);
					updateLocalQuestionarioData();//actualiza variavel $questionario
					renderForms("");
				}
				break;
				case '88': { // Add CT
					$questionario->addAcaoFormacaoFrequentada();
					updateLocalQuestionarioData();
					renderForms("");
				}
				break;

				
                default: { //echo "DEFAULT";
                        //echo "Default!".$operacao;
                        //exit("AAAAAAAAAA");
                        if ($questionario->investigador->estado == "1")                           
                            finalForm();
                        else
                            renderForms("");
                    }
                    break;
            }

            echo "<fieldset class='footer'>";
            echo "<legend></legend>
            FACULDADE DE MEDICINA DA UNIVERSIDADE DO PORTO<br/>";
            echo "Serviço de Informática e Departamento de Apoio à Investigação e Pós-Graduação";
            echo "</fieldset>";
            echo "</body></html>\n";
        }else {
            print_r("NOOBJETCT");
            formLogout();
        }

        function lacrarQuestionario() {
            global $questionario;
            $questionario->lacrar();
        }

        //TRATAMENTO DE ERROS DE PREENCHIMENTO 
        function validaDados() {
            global $questionario;
            global $anoactual;
            $message = "";

            //VALIDACAO DADOS GERAIS
            if ($questionario->investigador->nummec == "") {
                if ($questionario->investigador->numidentificacao == "")
                    $message = $message . "Dados Gerais/Identificação Civil - Deve ser indicado um nº mecanográfico ou nº de identificação válidos<br/>";
            }
            if ($questionario->investigador->nome == "") {
                $message = $message . "Dados Gerais/Identificação Civil - Deve ser indicado o nome<br/>";
            }
            if ($questionario->investigador->telemovel == "") {
                $message = $message . "Dados Gerais/Identificação Civil - Deve ser indicado o telemóvel<br/>";
            }
            if ($questionario->investigador->datanasc == "") {
                $message = $message . "Dados Gerais/Identificação Civil - Deve ser indicado a data de nascimento<br/>";
            }
            if ($questionario->investigador->email == "") {
                $message = $message . "Dados Gerais/Identificação Civil - Deve ser indicado o email<br/>";
            }
            if ($questionario->investigador->servico == -1 || $questionario->investigador->servico == 0) {
                $message = $message . "Dados Gerais/Identificação Civil - Deve indicar o Departamento a que pertence<br/>";
            }
            
            //VALIDACAO CURSOS A FREQUENTAR
            $l = 0;
            foreach ($questionario->habilitacoes as $i => $value) {
                $l++;

                if ($questionario->habilitacoes[$i]->anoinicio != "" && $questionario->habilitacoes[$i]->anofim != "") {
                    if ($questionario->habilitacoes[$i]->anoinicio > $questionario->habilitacoes[$i]->anofim) {
                        $message = $message . "Dados Gerais/Cursos a frequentar - A data de fim deve ser superior à data de início (Linha " . $l . ")<br/>";
                    }                    
                    if (!($questionario->habilitacoes[$i]->anoinicio <= 95 && $questionario->habilitacoes[$i]->anofim >= 94)) {
                        $message = $message . "Dados Gerais/Cursos a frequentar - A data de inicio e fim deverá incluir " . $anoactual . " (Linha " . $l . ")<br/>";
                    }
                }
                if ($questionario->habilitacoes[$i]->curso == "") {
                    $message = $message . "Dados Gerais/Cursos a frequentar - Deve indicar o nome do curso (Linha " . $l . ")<br/>";
                }
            }

            //VALIDACAO AGREGACAO
            if ($questionario->agregacao->dataprevista != "") {
                if ($questionario->agregacao->dataprevista < $anoactual + 1) {
                    $message = $message . "Dados Gerais/Agregação - O ano previsto deve ser superior a " . $anoactual . " <br/>";
                }
                if ($questionario->agregacao->agregacao == 1) {
                    $message = $message . "Dados Gerais/Agregação - Não pode existir simultaneamente a indicação de conclusão e ano previsto de conclusão<br/>";
                }
            }

            if (sizeof($questionario->identificacaoformal) == 0) {
                $message = $message . "Outra Atividade Científica " . $anoactual . "/Identificação Científica - Deve identificar pelo menos um nome científico<br/>";
            }

            $df = intval($questionario->ocupacaoprofissional->fmupinv) +
                  intval($questionario->ocupacaoprofissional->fmupens) +
                  intval($questionario->ocupacaoprofissional->fmupgest) +
                  intval($questionario->ocupacaoprofissional->fmupdc);

            if ($df != 100) {
                $message = $message . "Dados Gerais/Experiência Profissional/Dedicação à FMUP - O total das percentagens deve ser 100% <br/>";
            }

            if ($df == 0) {
                $message = $message . "Dados Gerais/Experiência Profissional/Dedicação à FMUP - Deve indicar uma percentagem >0 em pelo menos uma das categorias<br/>";
            }
            
            $l = 0;
            foreach ($questionario->experienciaprofissional as $i => $value) {
                $l++;
                if ($questionario->experienciaprofissional[$i]->datainicio != "" && $questionario->experienciaprofissional[$i]->datafim != "" && $questionario->experienciaprofissional[$i]->semtermo != "1") {
                    if (strtotime($questionario->experienciaprofissional[$i]->datainicio) > strtotime($questionario->experienciaprofissional[$i]->datafim)) {
                        $message = $message . "Dados Gerais/Experiência Profissional/Experiência Profissional - A data de fim deve ser superior à data de início (Linha " . $l . ")<br/>";
                    }
                }                
                if (date("Y", strtotime($questionario->experienciaprofissional[$i]->datainicio)) != $anoactual) {
                    $message = $message . "Dados Gerais/Experiência Profissional/Experiência Profissional - A data de início deve ser igual a " . $anoactual . " (Linha " . $l . ")<br/>";
                }
                if ($questionario->experienciaprofissional[$i]->empregador == "") {
                    $message = $message . "Dados Gerais/Experiência Profissional/Experiência Profissional - Deve indicar o nome do empregador (Linha " . $l . ")<br/>";
                }
            }

            $l = 0;
            foreach ($questionario->cargosUP as $i => $value) {
                $l++;
                if ($questionario->cargosUP[$i]->datainicio != "" && $questionario->cargosUP[$i]->datafim != "") {
                    if (strtotime($questionario->cargosUP[$i]->datainicio) > strtotime($questionario->cargosUP[$i]->datafim)) {
                        $message = $message . "Cargos " . $anoactual . "/Cargos na UP - A data de fim deve ser superior à data de início (Linha " . $l . ")<br/>";
                    }
                    if (!(date("Y", strtotime($questionario->cargosUP[$i]->datainicio)) <= $anoactual && date("Y", strtotime($questionario->cargosUP[$i]->datafim)) >= $anoactual)) {
                        $message = $message . "Cargos " . $anoactual . "/Cargos na UP - A data de inicio e fim deverá incluir " . $anoactual . " (Linha " . $l . ")<br/>";
                    }
                }
                if ($questionario->cargosUP[$i]->cargo == "") {
                    $message = $message . "Cargos " . $anoactual . "/Cargos na UP - Deve indicar um cargo (Linha " . $l . ")<br/>";
                }
                if ($questionario->cargosUP[$i]->datainicio == "") {
                    $message = $message . "Cargos " . $anoactual . "/Cargos na UP - Deve indicar uma data de início (Linha " . $l . ")<br/>";
                }
//                 if ($questionario->cargosUP[$i]->datafim == "") {
//                     $message = $message . "Cargos " . $anoactual . "/Cargos na UP - Deve indicar uma data de fim (Linha " . $l . ")<br/>";
//                 }
            }
            
            $l = 0;
            foreach ($questionario->cargosFMUP as $i => $value) {
                $l++;

                if ($questionario->cargosFMUP[$i]->datainicio != "" && $questionario->cargosFMUP[$i]->datafim != "") {
                    if (strtotime($questionario->cargosFMUP[$i]->datainicio) > strtotime($questionario->cargosFMUP[$i]->datafim)) {
                        $message = $message . "Cargos " . $anoactual . "/Cargos na FMUP - A data de fim deve ser superior à data de início (Linha " . $l . ")<br/>";
                    }
                    if (!(date("Y", strtotime($questionario->cargosFMUP[$i]->datainicio)) <= $anoactual && date("Y", strtotime($questionario->cargosFMUP[$i]->datafim)) >= $anoactual)) {
                        $message = $message . "Cargos " . $anoactual . "/Cargos na FMUP - A data de inicio e fim deverá incluir " . $anoactual . " (Linha " . $l . "<br/>";
                    }
                }
                if ($questionario->cargosFMUP[$i]->cargo == "") {
                    $message = $message . "Cargos " . $anoactual . "/Cargos na FMUP - Deve indicar um cargo (Linha " . $l . ")<br/>";
                }              
                if ($questionario->cargosFMUP[$i]->datainicio == "") {
                    $message = $message . "Cargos " . $anoactual . "/Cargos na FMUP - Deve indicar uma data de início (Linha " . $l . ")<br/>";
                }
//                 if ($questionario->cargosFMUP[$i]->datafim == "") {
//                     $message = $message . "Cargos " . $anoactual . "/Cargos na FMUP - Deve indicar uma data de fim (Linha " . $l . ")<br/>";
//                 }
            }

            $l = 0;
            foreach ($questionario->cargosHospitais as $i => $value) {
                $l++;

                if ($questionario->cargosHospitais[$i]->datainicio != "" && $questionario->cargosHospitais[$i]->datafim != "") {
                    if (strtotime($questionario->cargosHospitais[$i]->datainicio) > strtotime($questionario->cargosHospitais[$i]->datafim)) {
                        $message = $message . "Cargos " . $anoactual . "/Direção Hospital - A data de fim deve ser superior à data de início (Linha " . $l . ")<br/>";
                    }
                    if (!(date("Y", strtotime($questionario->cargosHospitais[$i]->datainicio)) <= $anoactual && date("Y", strtotime($questionario->cargosHospitais[$i]->datafim)) >= $anoactual)) {
                        $message = $message . "Cargos " . $anoactual . "/Direção Hospital - A data de inicio e fim deverá incluir " . $anoactual . " (Linha " . $l . "<br/>";
                    }
                }
                if ($questionario->cargosHospitais[$i]->instituicao == "") {
                    $message = $message . "Cargos " . $anoactual . "/Direção Hospital - Deve indicar uma instituição (Linha " . $l . ")<br/>";
                }
                if ($questionario->cargosHospitais[$i]->datainicio == "") {
                    $message = $message . "Cargos " . $anoactual . "/Direção Hospital - Deve indicar uma data de início (Linha " . $l . ")<br/>";
                }
                
//                 if ($questionario->cargosHospitais[$i]->datafim == "") {
//                     $message = $message . "Cargos " . $anoactual . "/Direção Hospital- Deve indicar uma data de fim (Linha " . $l . ")<br/>";
//                 }
            }
            
            //ALTERACAO
            //Paulo Rodrigues (prodrigues@med.up.pt)
            //VALIDA CONSULTORIA
            $l = 0;
            foreach ($questionario->consultorias as $i => $value) {
                $l++;
                
                if ($questionario->consultorias[$i]->datainicio == "") {
                    $message = $message . "Cargos " . $anoactual . "/Consultorias - Deve indicar uma data de início (Linha " . $l . ")<br/>";
                }                
                if ($questionario->consultorias[$i]->datafim == "") {
                    $message = $message . "Cargos " . $anoactual . "/Consultorias- Deve indicar uma data de fim (Linha " . $l . ")<br/>";
                }                                
                if ($questionario->consultorias[$i]->datainicio != "" && $questionario->consultorias[$i]->datafim != "") {
                    if (strtotime($questionario->consultorias[$i]->datainicio) > strtotime($questionario->consultorias[$i]->datafim)) {
                        $message = $message . "Cargos " . $anoactual . "/Consultorias - A data de fim deve ser superior à data de início (Linha " . $l . ")<br/>";
                    }
                    if (!(date("Y", strtotime($questionario->consultorias[$i]->datainicio)) <= $anoactual && date("Y", strtotime($questionario->consultorias[$i]->datafim)) >= $anoactual)) {
                        $message = $message . "Cargos " . $anoactual . "/Consultorias - A data de inicio e fim deverá incluir " . $anoactual . " (Linha " . $l . "<br/>";
                    }
                }
                if ($questionario->consultorias[$i]->instituicao == "") {
                    $message = $message . "Cargos " . $anoactual . "/Consultorias - Deve indicar uma instituição (Linha " . $l . ")<br/>";
                }
                if ($questionario->consultorias[$i]->orgao == "") {
                    $message = $message . "Cargos " . $anoactual . "/Consultorias - Deve indicar um orgão (Linha " . $l . ")<br/>";
                }                
            }

            $l = 0;
            foreach ($questionario->dirCursos as $i => $value) {
                $l++;

                if ($questionario->dirCursos[$i]->datainicio != "" && $questionario->dirCursos[$i]->datafim != "") {
                    if (strtotime($questionario->dirCursos[$i]->datainicio) > strtotime($questionario->dirCursos[$i]->datafim)) {
                        $message = $message . "Cargos " . $anoactual . "/Direções de Curso - A data de fim deve ser superior à data de início (Linha " . $l . ")<br/>";
                    }
                    if (!(date("Y", strtotime($questionario->dirCursos[$i]->datainicio)) <= $anoactual && date("Y", strtotime($questionario->dirCursos[$i]->datafim)) >= $anoactual)) {
                        $message = $message . "Cargos " . $anoactual . "/Direções de Curso - A data de inicio e fim deverá incluir " . $anoactual . " (Linha " . $l . ")<br/>";
                    }
                }
                if ($questionario->dirCursos[$i]->nome == "") {
                    $message = $message . "Cargos " . $anoactual . "/Direções de Curso - Deve indicar um curso (Linha " . $l . ")<br/>";
                }
                /* if(strtotime($questionario->dirCursos[$i]->datafim)>strtotime(date("Y-m-d"))){
                  $message=$message."Cargos ".$anoactual."/Direções de Curso - A data de fim não deve ser superior a data atual (Linha ".$l.")<br/>";
                  } */
                if ($questionario->dirCursos[$i]->datainicio == "") {
                    $message = $message . "Cargos " . $anoactual . "/Direções de Curso - Deve indicar uma data de início (Linha " . $l . ")<br/>";
                }
                /*if ($questionario->dirCursos[$i]->datafim == "") {
                    $message = $message . "Cargos " . $anoactual . "/Direções de Curso - Deve indicar uma data de fim (Linha " . $l . ")<br/>";
                }*/
            }

            $message = $message . validaPublicacoesPatentes();
            $message = $message . validaPublicacoesInternacional();
            $message = $message . validaPublicacoesNacional();
            $message = $message . validaPublicacoesISI();
            $message = $message . validaPublicacoesPUBMED();

            $l = 0;

            foreach ($questionario->colaboradoresinternos as $i => $value) {
                $l++;

                if (!$questionario->colaboradoresinternos[$i]->validaNome()) {
                    $message = $message . "Outra Atividade Científica " . $anoactual . "/Colaboradores Actuais FMUP - O nome deve ser da forma 'Guimaraes, LP', (Linha " . $l . ")<br/>";
                }

                if ($questionario->colaboradoresinternos[$i]->departamento == $questionario->investigador->servico) {
                    $message = $message . "Outra Atividade Científica " . $anoactual . "/Colaboradores Actuais FMUP - Deve indicar apenas colaboradores de outros Departamentos, (Linha " . $l . ")<br/>";
                }
            }
            $l = 0;

            foreach ($questionario->colaboradores as $i => $value) {
                $l++;

                if (!$questionario->colaboradores[$i]->validaNome()) {
                    $message = $message . "Outra Atividade Científica " . $anoactual . "/Colaboradores Actuais Externos - O nome deve ser da forma 'Guimaraes, LP', (Linha " . $l . ")<br/>";
                }
                if ($questionario->colaboradores[$i]->instituicao == "Universidade do Porto - Faculdade de Medicina") {
                    $message = $message . "Outra Atividade Científica " . $anoactual . "/Colaboradores Actuais Externos - Deve indicar apenas instituições diferentes da FMUP, (Linha " . $l . ")<br/>";
                }
            }

            $l = 0;

            foreach ($questionario->identificacaoformal as $i => $value) {
                $l++;

                if (!$questionario->identificacaoformal[$i]->validaNomeIni()) {
                    $message = $message . "Outra Atividade Científica " . $anoactual . "/Identificação Científica - O nome com iniciais deve ser da forma 'Guimaraes, LP', (Linha " . $l . ")<br/>";
                }
                if (!$questionario->identificacaoformal[$i]->validaNomeExt()) {
                    $message = $message . "Outra Atividade Científica " . $anoactual . "/Identificação Científica - O nome completo deve ser da forma 'Guimaraes, Luis Pacos', (Linha " . $l . ")<br/>";
                }
            }



            $l = 0;

            foreach ($questionario->unidadesinvestigacao as $i => $value) {
                $l++;

                if ($questionario->unidadesinvestigacao[$i]->percentagem > 100 || $questionario->unidadesinvestigacao[$i]->percentagem <= 0) {
                    $message = $message . "Outra Atividade Científica " . $anoactual . "/Unidades de Investigação - A percentagem deve estar compreendida entre 0 e 100 (Linha " . $l . ")<br/>";
                }
                if ($questionario->unidadesinvestigacao[$i]->unidade == "") {
                    $message = $message . "Outra Atividade Científica " . $anoactual . "/Unidades de Investigação - Deve indicar o nome da Unidade (Linha " . $l . ")<br/>";
                }
            }

            $l = 0;
            foreach ($questionario->projectos as $i => $value) {
                $l++;
				if($questionario->projectos[$i]->estado==0){
	                if ($questionario->projectos[$i]->datainicio != "" && $questionario->projectos[$i]->datafim != "") {
	                    if (strtotime($questionario->projectos[$i]->datainicio) > strtotime($questionario->projectos[$i]->datafim)) {
	                        $message = $message . "Outra Atividade Científica " . $anoactual . "/Projetos - A data de fim deve ser superior à data de início (Linha " . $l . ")<br/>";
	                    }
	                    if (!(date("Y", strtotime($questionario->projectos[$i]->datainicio)) <= $anoactual && date("Y", strtotime($questionario->projectos[$i]->datafim)) >= $anoactual)) {
	                        $message = $message . "Outra Atividade Científica " . $anoactual . "/Projetos - A data de inicio e fim deverá incluir " . $anoactual . " (Linha " . $l . ")<br/>";
	                    }
	                }
	
	                if ($questionario->projectos[$i]->datainicio == "" && $questionario->projectos[$i]->estado != "1" || $questionario->projectos[$i]->datafim == "") {
	                    $message = $message . "Outra Atividade Científica " . $anoactual . "/Projetos - A data de início e fim não podem estar vazias (Linha " . $l . ")<br/>";
	                }
	                if ($questionario->projectos[$i]->codigo == "") {
	                    $message = $message . "Outra Atividade Científica " . $anoactual . "/Projetos - Deve indicar o código do Projeto (Linha " . $l . ")<br/>";
	                }
			  	}
            }
            $l = 0;
            foreach ($questionario->conferencias as $i => $value) {
                $l++;
                if ($questionario->conferencias[$i]->datainicio != "" && $questionario->conferencias[$i]->datafim != "") {
                    if (strtotime($questionario->conferencias[$i]->datainicio) > strtotime($questionario->conferencias[$i]->datafim)) {
                        $message = $message . "Outra Atividade Científica " . $anoactual . "/Conferências Organizadas - A data de fim deve ser superior à data de início (Linha " . $l . ")<br/>";
                    }
                    if (!(date("Y", strtotime($questionario->conferencias[$i]->datainicio)) <= $anoactual && date("Y", strtotime($questionario->conferencias[$i]->datafim)) >= $anoactual)) {
                        $message = $message . "Outra Atividade Científica " . $anoactual . "/Conferências Organizadas - A data de inicio e fim deverá incluir " . $anoactual . " (Linha " . $l . ")<br/>";
                    }
                }

                if ($questionario->conferencias[$i]->datainicio == "" || $questionario->conferencias[$i]->datafim == "") {
                    $message = $message . "Outra Atividade Científica " . $anoactual . "/Conferências Organizadas - As datas de início e fim não podem estar vazias (Linha " . $l . ")<br/>";
                }
                if ($questionario->conferencias[$i]->titulo == "") {
                    $message = $message . "Outra Atividade Científica " . $anoactual . "/Conferências Organizadas - Deve indicar um título (Linha " . $l . ")<br/>";
                }
            }

            $l = 0;
            foreach ($questionario->sc as $i => $value) {
                $l++;

                if ($questionario->sc[$i]->datainicio != "" && $questionario->sc[$i]->datafim != "") {
                    if (strtotime($questionario->sc[$i]->datainicio) > strtotime($questionario->sc[$i]->datafim)) {
                        $message = $message . "Outra Atividade Científica " . $anoactual . "/Sociedades Científicas - A data de fim deve ser superior à data de início (Linha " . $l . ")<br/>";
                    }
                    if (!(date("Y", strtotime($questionario->sc[$i]->datainicio)) <= $anoactual && date("Y", strtotime($questionario->sc[$i]->datafim)) >= $anoactual)) {
                        $message = $message . "Outra Atividade Científica " . $anoactual . "/Sociedades Científicas - A data de inicio e fim deverá incluir " . $anoactual . " (Linha " . $l . ")<br/>";
                    }
                }
                if ($questionario->sc[$i]->nome == "") {
                    $message = $message . "Outra Atividade Científica " . $anoactual . "/Sociedades Científicas - Deve indicar um nome (Linha " . $l . ")<br/>";
                }
                if ($questionario->sc[$i]->datainicio == "") {
                    $message = $message . "Outra Atividade Científica " . $anoactual . "/Sociedades Científicas - Deve indicar uma data de início (Linha " . $l . ")<br/>";
                }
                /*if ($questionario->sc[$i]->datafim == "") {
                    $message = $message . "Outra Atividade Científica " . $anoactual . "/Sociedades Científicas - Deve indicar uma data de fim (Linha " . $l . ")<br/>";
                }*/
            }

            $l = 0;
            foreach ($questionario->missoes as $i => $value) {
                $l++;

                if ($questionario->missoes[$i]->datainicio != "" && $questionario->missoes[$i]->datafim != "") {
                    if (strtotime($questionario->missoes[$i]->datainicio) > strtotime($questionario->missoes[$i]->datafim)) {
                        $message = $message . "Outra Atividade Científica " . $anoactual . "/Missões - A data de fim deve ser superior à data de início (Linha " . $l . ")<br/>";
                    }
                    if (!(date("Y", strtotime($questionario->missoes[$i]->datainicio)) <= $anoactual && date("Y", strtotime($questionario->missoes[$i]->datafim)) >= $anoactual)) {
                        $message = $message . "Outra Atividade Científica " . $anoactual . "/Missões - A data de inicio e fim deverá incluir " . $anoactual . " (Linha " . $l . ")<br/>";
                    }
                }

                if ($questionario->missoes[$i]->datainicio == "") {
                    $message = $message . "Outra Atividade Científica " . $anoactual . "/Missões - Deve indicar uma data de início (Linha " . $l . ")<br/>";
                }
                if ($questionario->missoes[$i]->datafim == "") {
                    $message = $message . "Outra Atividade Científica " . $anoactual . "/Missões - Deve indicar uma data de fim (Linha " . $l . ")<br/>";
                }
            }

            $l = 0;
            foreach ($questionario->redes as $i => $value) {
                $l++;

                if ($questionario->redes[$i]->datainicio != "" && $questionario->redes[$i]->datafim != "") {
                    if (strtotime($questionario->redes[$i]->datainicio) > strtotime($questionario->redes[$i]->datafim)) {
                        $message = $message . "Outra Atividade Científica " . $anoactual . "/Redes - A data de fim deve ser superior à data de início (Linha " . $l . ")<br/>";
                    }
                    if (!(date("Y", strtotime($questionario->redes[$i]->datainicio)) <= $anoactual && date("Y", strtotime($questionario->redes[$i]->datafim)) >= $anoactual)) {
                        $message = $message . "Outra Atividade Científica " . $anoactual . "/Redes - A data de inicio e fim deverá incluir " . $anoactual . " (Linha " . $l . ")<br/>";
                    }
                }

                if ($questionario->redes[$i]->datainicio == "") {
                    $message = $message . "Outra Atividade Científica " . $anoactual . "/Redes - Deve indicar uma data de início (Linha " . $l . ")<br/>";
                }
                if (date("Y", strtotime($questionario->redes[$i]->datainicio)) > $anoactual) {
                    $message = $message . "Outra Atividade Científica " . $anoactual . "/Redes - A data de início deve ser igual ou inferior a " . $anoactual . " (Linha " . $l . ")<br/>";
                }
            }
            
            $l = 0;
            foreach ($questionario->arbitragensRevistas as $i => $value) {
                $l++;


                if ($questionario->arbitragensRevistas[$i]->issn != "") {
                    if (strpos($questionario->arbitragensRevistas[$i]->issn, "-") != 4 || strlen($questionario->arbitragensRevistas[$i]->issn) != 9) {
                        $message = $message . "Outra Atividade Científica " . $anoactual . "/Editor/Árbitro - (Linha " . $l . ") - ISSN Errado<br/>";
                    }
                }
                if ($questionario->arbitragensRevistas[$i]->tituloRevista == "") {
                    $message = $message . "Outra Atividade Científica " . $anoactual . "/Editor/Árbitro - (Linha " . $l . ") - Deve indicar o nome da Revista<br/>";
                }
            }

            $l = 0;
            foreach ($questionario->colaboradoresinternos as $i => $value) {
                $l++;

                if ($questionario->colaboradoresinternos[$i]->nomecient == "") {
                    $message = $message . "Outra Atividade Científica " . $anoactual . "/Colaboradores Atuais FMUP - (Linha " . $l . ") - Nome vazio<br/>";
                }
            }

            $l = 0;
            foreach ($questionario->colaboradores as $i => $value) {
                $l++;
                if ($questionario->colaboradores[$i]->nomecient == "") {
                    $message = $message . "Outra Atividade Científica " . $anoactual . "/Colaboradores Atuais Externos- (Linha " . $l . ") - Nome vazio<br/>";
                }
            }

            $l = 0;

            foreach ($questionario->premios as $i => $value) {
                $l++;

                if ($questionario->premios[$i]->nome == "") {
                    $message = $message . "Outra Atividade Científica " . $anoactual . "/Prémios - (Linha " . $l . ") - Nome vazio<br/>";
                }
            }


            $l = 0;
            foreach ($questionario->acoesdivulgacao as $i => $value) {
                $l++;


                if (date("Y", strtotime($questionario->acoesdivulgacao[$i]->data)) != $anoactual) {
                    $message = $message . "Atividade Académica e de Extensão " . $anoactual . "/OutrasActividades/Ações de Divulgação - A data não pode ser diferente de " . $anoactual . " (Linha " . $l . ")<br/>";
                }

                if ($questionario->acoesdivulgacao[$i]->titulo == "") {
                    $message = $message . "Atividade Académica e de Extensão " . $anoactual . "/Ações de Formação - Deve indicar um título (Linha " . $l . ")<br/>";
                }
            }

            $l = 0;
            foreach ($questionario->produtos as $i => $value) {
                $l++;

                if ($questionario->produtos[$i]->tipop == "") {
                    $message = $message . "Atividade Académica e de Extensão " . $anoactual . "/Produtos e Serviços - (Linha " . $l . ") - Deve indicar o tipo<br/>";
                }
                if ($questionario->produtos[$i]->valorp == "") {
                    $message = $message . "Atividade Académica e de Extensão " . $anoactual . "/Produtos e Serviços - (Linha " . $l . ") - Deve indicar o valor<br/>";
                }
            }
			$l=0;
			foreach ($questionario->orientacoes as $i => $value){//FIXME
				$l++;
				if($value->tipo_orientacao == -1){
					$message = $message."Atividade Académica, Clinica e de Extensão ".$anoactual."/Orientações - Deve indicar um Tipo de Orientação (Linha ".$l.")<br/>";
				}
				if($value->tipo_orientador == -1){
					$message = $message."Atividade Académica, Clinica e de Extensão ".$anoactual."/Orientações - Deve indicar um Tipo de Orientador (Linha ".$l.")<br/>";
				}
				if($value->estado == -1){
					$message = $message."Atividade Académica, Clinica e de Extensão ".$anoactual."/Orientações - Deve indicar um Estado (Linha ".$l.")<br/>";
				}	
			}
	
            $l = 0;
            foreach ($questionario->acoesformacao as $i => $value) {
                $l++;

                if ($questionario->acoesformacao[$i]->dataini != "" && $questionario->acoesformacao[$i]->datafim != "") {
                    if (strtotime($questionario->acoesformacao[$i]->dataini) > strtotime($questionario->acoesformacao[$i]->datafim)) {
                        $message = $message . "Atividade Académica e de Extensão " . $anoactual . "/Ações de Formação - A data de fim deve ser superior à data de início (Linha " . $l . ")<br/>";
                    }
                    if (!(date("Y", strtotime($questionario->acoesformacao[$i]->dataini)) <= $anoactual && date("Y", strtotime($questionario->acoesformacao[$i]->datafim)) >= $anoactual)) {
                        $message = $message . "Atividade Académica e de Extensão " . $anoactual . "/Ações de Formação - A data de inicio e fim deverá incluir " . $anoactual . " (Linha " . $l . ")<br/>";
                    }
                }

                if ($questionario->acoesformacao[$i]->dataini == "" || $questionario->acoesformacao[$i]->datafim == "") {
                    $message = $message . "Atividade Académica e de Extensão " . $anoactual . "/Ações de Formação - A data de início/fim não deve estar vazia (Linha " . $l . ")<br/>";
                }
                if ($questionario->acoesformacao[$i]->titulo == "") {
                    $message = $message . "Atividade Académica e de Extensão " . $anoactual . "/Ações de Formação - Deve indicar um título (Linha " . $l . ")<br/>";
                }
            }
            
            $l = 0;
            foreach ($questionario->acoesformacaofrequentada as $i => $value) {
            	$l++;
            
            	if ($questionario->acoesformacaofrequentada[$i]->dataini != "" && $questionario->acoesformacaofrequentada[$i]->datafim != "") {
            		if (strtotime($questionario->acoesformacaofrequentada[$i]->dataini) > strtotime($questionario->acoesformacaofrequentada[$i]->datafim)) {
            			$message = $message . "Dados Gerais/Cursos a frequentar/Ações de Formação Frequentada - A data de fim deve ser superior à data de início (Linha " . $l . ")<br/>";
            		}
            		if (!(date("Y", strtotime($questionario->acoesformacaofrequentada[$i]->dataini)) <= $anoactual && date("Y", strtotime($questionario->acoesformacaofrequentada[$i]->datafim)) >= $anoactual)) {
            			$message = $message . "Dados Gerais/Cursos a frequentar/Ações de Formação Frequentada - A data de inicio e fim deverá incluir " . $anoactual . " (Linha " . $l . ")<br/>";
            		}
            	}
            
            	if ($questionario->acoesformacaofrequentada[$i]->dataini == "" || $questionario->acoesformacaofrequentada[$i]->datafim == "") {
            		$message = $message . "Dados Gerais/Cursos a frequentar/Ações de Formação Frequentada - A data de início/fim não deve estar vazia (Linha " . $l . ")<br/>";
            	}
            	if ($questionario->acoesformacaofrequentada[$i]->titulo == "") {
            		$message = $message . "Dados Gerais/Cursos a frequentar/Ações de Formação Frequentada - Deve indicar um título (Linha " . $l . ")<br/>";
            	}
            }
            


            $l = 0;
            foreach ($questionario->acreditacoes as $i => $value) {
                $l++;

                if ($questionario->acreditacoes[$i]->data != "") {

                    if (date("Y", strtotime($questionario->acreditacoes[$i]->data)) != $anoactual) {
                        $message = $message . "Atividade Académica e de Extensão " . $anoactual . "/Acreditacoes - A data de inicio e fim deverá incluir " . $anoactual . " (Linha " . $l . ")<br/>";
                    }
                }

                if ($questionario->acreditacoes[$i]->data == "") {
                    $message = $message . "Atividade Académica e de Extensão " . $anoactual . "/Acreditacoes - A data não deve estar vazia (Linha " . $l . ")<br/>";
                }

                if ($questionario->acreditacoes[$i]->tipo == "") {
                    $message = $message . "Atividade Académica e de Extensão " . $anoactual . "/Acreditacoes - Deve indicar um título (Linha " . $l . ")<br/>";
                }
            }
            
            if ($message == "")
                $message = "Sem Erros";
            return $message;
        }

        function finalForm() {
            global $questionario;

            $_SESSION['questionario'] = serialize($questionario);

            echo "<form name='questionario' action='questionario.php' method='post' enctype='multipart/form-data'>";

            echo "<input type='hidden' name='operacao' value='0'>";

            echo "<fieldset class='head'>";

            echo "<table><tr>";
            echo "<td>Está autenticado como <i><span class='redcolor'>" . $questionario->investigador->login . "</span></i><input class='op-button' type='image' src='../images/icon_off.png' name='navOption'  value='Sair' 

            onclick='var r=confirm(\"Tem a certeza?Are you sure?\");
            if (r==true){
                    document.questionario.operacao.value=3;
                    document.questionario.submit();
            } else {
                    return false;
            }'
            >Sair</td>";

            echo "<td align='right'><h2>Levantamento Geral de Dados da FMUP</h2><br/><h2><i>General Survey of FMUP</i></h2></td>";

            if ($questionario->investigador->estado == "0") {
                echo "<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td><input class='op-button' type='image' src='../images/arrowlefts.png' name='navOption'  value='Voltar' onclick='document.questionario.operacao.value=58;document.questionario.submit();'>Voltar/Return</td>";
            }

            echo "</tr></table>";
            echo "</fieldset>";
            
            echo "<div id='final37'>";
            echo " <table  id='box-table-a'><tbody>";
            echo "<tr>";
            echo "<td align='center'><a target=blank href='questionarioFinal.php'>Versão Total para Impressão</a><br><br></td></tr>";

         
            if ($questionario->investigador->estado == "0") {

                if (checkDataLacrar()) {
                	echo "<tr><td align='center' colspan='1'>Após Lacrar deverá receber um email com a indicação da data/hora do Lacre. Deve remetê-lo para sciman@med.up.pt, ficando o processo encerrado.</td></tr>";
                	echo "<tr><td align='center' colspan='1'><i>After Locking you should get an email with the Date/Time of the Lock. You must send it to sciman@med.up.pt in order to close the process.</i></td></tr>";
                	 
                    echo "<tr><td align='center'><input class='op-button' type='image' src='../images/icon_key.png' name='navOption'  value='Lacrar' onclick='var rr=confirm(\"Ao clicar “Lacrar” certifico, pela minha honra, que todas as afirmações constantes neste levantamento são verdadeiras.  On clicking “Lock” I hereby certify, by my honour, that all information in this Survey is correct.\");
                                                    if(rr==true){
                                                            document.questionario.operacao.value=57;
                                                    }else{
                                                            return false;
                                                    }'>Lacrar/Lock</td></tr></form>";
                    //echo "<tr><td colspan='2'><font color='red'>ATENÇÃO</font>: Só lacre o Levantamento quando estiver pronto para preencher a parte II (anónima), que vai surgir no imediato. Não terá outra oportunidade para a preencher.									<p><i><font color='red'>BEWARE</font>: that you should only lock the Survey when ready to go onto part II (anonymous), that will follow immediately. You will not have other chance to fill it.</i></p></td></tr>";

                } else {

                    echo "<tr><td align='center'><input class='op-button' type='image' src='../images/icon_key.png' name='navOption'  value='Lacrar' onclick='var rr=confirm(\"A operação de Lacre apenas estará disponível a partir do dia 1 de Janeiro de 2014\");
                                                    rr=false;
                                                    if(rr==true){
                                                            document.questionario.operacao.value=57;
                                                    }else{
                                                            return false;
                                                    }'>Lacrar/Lock</td></tr></form>";
                    //echo "<tr><td colspan='2'><font color='red'>ATENÇÃO</font>: Só lacre o Levantamento quando estiver pronto para preencher a parte II (anónima), que vai surgir no imediato. Não terá outra oportunidade para a preencher.									<p><i><font color='red'>BEWARE</font>: that you should only lock the Survey when ready to go onto part II (anonymous), that will follow immediately. You will not have other chance to fill it.</i></p></td></tr>";
                    //echo "<tr><td align='center' colspan='2'>Após Lacrar deverá receber um email com a indicação da data/hora do Lacre. Deve remetê-lo para sciman@med.up.pt, ficando o processo encerrado.</td></tr>";
                    //echo "<tr><td align='center' colspan='2'><i>After Locking you should get an email with the Date/Time of the Lock. You must send it to sciman@med.up.pt in order to close the process.</i><br><br></td></tr>";
                }
            } else {
                echo "<td align='center'>Lacrado a " . $questionario->investigador->datasubmissao . "<br></td></tr></form>";
                echo "<tr><td align='center' colspan='2'><br>Deverá ter recebido um email com a indicação da data de Lacre.</td></tr>";
            }
            echo "</tbody></table>";
            echo "</div>";
        }

        function renderForms($error_msg) {
            global $questionario;
            global $activeTab;
            global $saveTab;
            global $anoactual;

            $saveTab = $activeTab;

            echo "<form name='questionario' action='questionario.php' method='post' enctype='multipart/form-data'>";

            echo "<input type='hidden' name='activeTab' value='" . $activeTab . "'>";
            echo "<input type='hidden' name='saveTab' value='" . $saveTab . "'>";
            echo "<input type='hidden' name='operacao' value='0'>";

            echo "<fieldset class='head'>";

            echo "<table>";
            echo "<tr><td colspan='7'>Está autenticado como <i><span class='redcolor'>" . $questionario->investigador->login . "</span></i><br/></td></tr>";
            echo "<tr><td><input class='op-button' type='image' src='../images/icon_off.png' name='navOption'  value='Sair' 
            onclick='var r=confirm(\"Tem a certeza?Are you sure?\");
            if (r==true){
                    var rr=confirm(\"Pretende gravar (ao clicar “Cancelar” sai sem gravar) / Do you want to save (clicking “Cancel” will get you out without saving)?\");
                    if(rr==true){
                            document.questionario.operacao.value=2;
                    }else{
                            document.questionario.operacao.value=3;
                    }
                    document.questionario.submit();
            }else{
                    return false;
            }'
            >Sair<br/>Exit</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
            echo "<td align='right'><h2>Levantamento Geral de Dados da FMUP ".$anoactual."</h2><br/><h2><i>General Survey of FMUP ".$anoactual."</i></h2></td>";
            echo "<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
            echo "<td><input class='op-button' type='image' src='../images/icon_save.png' name='navOption' value='Gravar' onclick='if(formSubmited==0){return false;};document.questionario.operacao.value=1'>Gravar<br/>Save</td>";
            echo "<td>&nbsp;&nbsp;&nbsp;&nbsp;<br</td>";
            echo "<td><input class='op-button' type='image' src='../images/icon_valid.png' name='navOption' value='Validar' onclick='if(formSubmited==0){return false;};document.questionario.operacao.value=54'>Validar<br/>Validate</td>";
            echo "<td>&nbsp;&nbsp;&nbsp;&nbsp;<br</td>";
            echo "<td><input class='op-button' type='image' src='../images/green_arrow_down.png' name='navOption' value='Submeter' onclick='if(formSubmited==0){return false;};document.questionario.operacao.value=64'>Finalizar<br/>Finalize</td>";
            echo "</tr></table>";
            echo "</fieldset>";

            //ALTERACAO
            //Paulo Rodrigues (prodrigues@med.up.pt)
            if($error_msg == "Sem Erros")
            {
                echo "<div id='warnings'><fieldset class='warning'><font color='green'>" . $error_msg . "</fieldset></div>";
            }
            else 
            {
                echo "<div id='warnings'><fieldset class='warning'><span class='redcolor'>" . $error_msg . "</span></fieldset></div>";
            }

            echo "<div id='content_menu'>";
            echo "<ul class= 'menu' >";
              
            echo "<li onclick='document.questionario.activeTab.value=0;document.questionario.submit();'><a href='#' class='' id='tab0'>Preenchimento<br/><i>How to fill the survey</i></a></li>";
            echo "<li onclick='document.questionario.activeTab.value=1;document.questionario.submit();'><a href='#' class=''>Dados Gerais<br/><i>General Data</i></a></li>
                            <li id='subtab1' style='display: none;'>
                                    <ul class= 'submenu' >
                                            <li onclick='document.questionario.activeTab.value=1;document.questionario.submit();'><a href='#' class='' id='tab1' >Identificação Civil<br/><i>Civil Identification</i></a></li>
                                            <li onclick='document.questionario.activeTab.value=2;document.questionario.submit();'><a href='#' class='' id='tab2' >Cursos a Frequentar<br/><i>Courses being attended</i></a></li>
                                            <li onclick='document.questionario.activeTab.value=3;document.questionario.submit();'><a href='#' class='' id='tab3' >Agregação<br/><i>Aggregation</i></a></li>
                                            <li onclick='document.questionario.activeTab.value=4;document.questionario.submit();'><a href='#' class='' id='tab4' >Experiência Profissional<br/><i>Professional Experience</i></a></li>
                                    </ul>
                            </li>";
            //ALTERACAO
            //Paulo Rodrigues (prodrigues@med.up.pt)
            echo "<li onclick='document.questionario.activeTab.value=5;document.questionario.submit();'><a href='#' class='' >Cargos " . $anoactual . "<br/><i>Positions " . $anoactual . "</i></a></li>
                      <li  id='subtab5' style='display: none;'>
                            <ul class= 'submenu' >
                                    <li onclick='document.questionario.activeTab.value=5;document.questionario.submit();'><a href='#' class='' id='tab5'>Cargos na UP<br/><i>Positions at UP</i></a></li>
                                    <li onclick='document.questionario.activeTab.value=6;document.questionario.submit();'><a href='#' class='' id='tab6'>Cargos na FMUP<br/><i>Positions at FMUP</i></a></li>
									<li onclick='document.questionario.activeTab.value=33;document.questionario.submit();'><a href='#' class='' id='tab33'>Comissões<br/><i>Commissions</i></a></li>									
                                    <li onclick='document.questionario.activeTab.value=32;document.questionario.submit();'><a href='#' class='' id='tab32'>Consultorias<br/><i>Consultancy</i></a></li>
                                    <li onclick='document.questionario.activeTab.value=7;document.questionario.submit();'><a href='#' class='' id='tab7'>Direção Hospitalar<br/><i>Hospital Boards</i></a></li>
                                    <li onclick='document.questionario.activeTab.value=8;document.questionario.submit();'><a href='#' class='' id='tab8'>Direções de Curso<br/><i>Course Directions</i></a></li>                                    
                            </ul>
                      </li>";

            echo "<li onclick='document.questionario.activeTab.value=9;document.questionario.submit();'><a href='#' class='' >Publicações " . $anoactual . "<br/><i>Publications " . $anoactual . "</i></a></li>
                            <li  id='subtab9' style='display: none;'><ul class= 'submenu' >
                                    <li onclick='document.questionario.activeTab.value=9;document.questionario.submit();'><a href='#' class='' id='tab9'>Publicações ISI<br/><i>ISI Publications</i></a></li>
                                    <li onclick='document.questionario.activeTab.value=10;document.questionario.submit();'><a href='#' class='' id='tab10'>Publicações PubMed<br/><i>PubMed Publications</i></a></li>
                                    <li onclick='document.questionario.activeTab.value=11;document.questionario.submit();'><a href='#' class='' id='tab11'>Patentes Manuais<br/><i>Patents - by hand</i></a></li>
                                    <li onclick='document.questionario.activeTab.value=12;document.questionario.submit();'><a href='#' class='' id='tab12'>Publicações Manuais internacionais<br/><i>International Publications - by hand</i></a></li>
                                    <li onclick='document.questionario.activeTab.value=13;document.questionario.submit();'><a href='#' class='' id='tab13'>Publicações Manuais Não Inglês<br/><i>Non-english Publications - by hand</i></a></li>
                            </ul></li>";

            echo "<li onclick='document.questionario.activeTab.value=14;document.questionario.submit();'><a href='#' class='' >Outra Atividade Científica " . $anoactual . "<br/><i>Other Research Activity " . $anoactual . "</i></a>
                            </li>
                            <li id='subtab14' style='display: none;'><ul class= 'submenu'  >
                                    <li onclick='document.questionario.activeTab.value=14;document.questionario.submit();'><a href='#' class='' id='tab14'>Identificação Científica<br/><i>Scientific Identification</i></a></li>
                                    <li onclick='document.questionario.activeTab.value=15;document.questionario.submit();'><a href='#' class='' id='tab15'>Colaboradores Atuais<br/><i>Current Collaborators</i></a></li>
                                    <li onclick='document.questionario.activeTab.value=16;document.questionario.submit();'><a href='#' class='' id='tab16'>Unidades de Investigação<br/><i>Research Units</i></a></li>
                                    <li onclick='document.questionario.activeTab.value=17;document.questionario.submit();'><a href='#' class='' id='tab17'>Projetos ou Ensaios Clínicos " . $anoactual . "<br/><i>Research Projects or Clinical Trials " . $anoactual . "</i></a></li>
                                    <li onclick='document.questionario.activeTab.value=18;document.questionario.submit();'><a href='#' class='' id='tab18'>Conferências Organizadas<br/><i>Organized Conferences</i></a></li>
                                    <li onclick='document.questionario.activeTab.value=19;document.questionario.submit();'><a href='#' class='' id='tab19'>Apresentações em conferências e seminários<br/><i>Presentations in Conferences and Seminars</i></a></li>
                                    <li onclick='document.questionario.activeTab.value=20;document.questionario.submit();'><a href='#' class='' id='tab20'>Editor/Árbitro<br/><i>Editor/Referee</i></a></li>
                                    <li onclick='document.questionario.activeTab.value=21;document.questionario.submit();'><a href='#' class='' id='tab21'>Prémios<br/><i>Prizes</i></a></li>
                                    <li onclick='document.questionario.activeTab.value=22;document.questionario.submit();'><a href='#' class='' id='tab22'>Organizações e Sociedades Científicas<br/><i>Scientific Organizations and Societies</i></a></li>
                                    <li onclick='document.questionario.activeTab.value=23;document.questionario.submit();'><a href='#' class='' id='tab23'>Redes Científicas<br/><i>Research Networks</i></a></li>
                                    <li onclick='document.questionario.activeTab.value=24;document.questionario.submit();'><a href='#' class='' id='tab24'>Missões<br/><i>Missions</i></a></li>
                            </ul></li>
            ";

            echo "<li onclick='document.questionario.activeTab.value=25;document.questionario.submit();'><a href='#' class='' >Atividade Académica, Clínica e de Extensão " . $anoactual . "<br/><i>Academic, Clinical and Extension Activity " . $anoactual . "</i></a>
                            </li>
                            <li id='subtab25' style='display: none;'><ul class= 'submenu'  >
                                    <li onclick='document.questionario.activeTab.value=25;document.questionario.submit();'><a href='#' class='' id='tab25'>Aulas, Consultas, Cirurgias<br/><i>Lectures, Appointments, Surgeries</i></a></li>
                                    <li onclick='document.questionario.activeTab.value=26;document.questionario.submit();'><a href='#' class='' id='tab26'>Orientações<br/><i>Supervisions</i></a></li>
                                    <li onclick='document.questionario.activeTab.value=27;document.questionario.submit();'><a href='#' class='' id='tab27'>Júris<br/><i>Jurys</i></a></li>
                                    <li onclick='document.questionario.activeTab.value=28;document.questionario.submit();'><a href='#' class='' id='tab28'>Outras atividades (texto/palestras)<br/><i>Other Activities</i></a></li>
                                    <li onclick='document.questionario.activeTab.value=29;document.questionario.submit();'><a href='#' class='' id='tab29'>Produtos e serviços<br/><i>Products and Services</i></a></li>
                                    <li onclick='document.questionario.activeTab.value=30;document.questionario.submit();'><a href='#' class='' id='tab30'>Ações de formação e Acreditações<br/><i>Training Conducted</i></a></li>
                            </ul></li>	
            ";

            echo "<li onclick='document.questionario.activeTab.value=31;document.questionario.submit();'><a href='#' class='' id='tab31'>Observações Finais<br/><i>Final Remarks</i></a></li>";

            echo "</ul>";
            echo "</div>";

            switch ($activeTab) {
                case '0': { // Opção de entrada
                        include('informacoes.php');
                    }
                    break;
                case '1': { // Opção de entrada
                        include('investigadores.php');
                    }
                    break;
                case '2': { // Opção de entrada
                        include('habilitacoes.php');
                    }
                    break;
                case '3': { // Opção de entrada
                        include('agregacoes.php');
                    }
                    break;
                case '4': { // Opção de entrada
                        include('experienciaProfissional.php');
                    }
                    break;
                case '5': { // Opção de entrada
                        include('cargosUP.php');
                    }
                    break;
                case '6': { // Opção de entrada
                        include('cargosFMUP.php');
                    }
                    break;
                case '7': { // Opção de entrada
                        include('cargosHospitais.php');
                    }break;
                case '8': { // Opção de entrada
                        include('direcoesCurso.php');
                    }break;
                case '9': {
                        include('publicacoesISI.php');
                    }break;
                case '10': {
                        include('publicacoesPubmed.php');
                    }break;
                case '11': {
                        include('publicacoesManualPatentes.php');
                    }break;
                case '12': {
                        include('publicacoesManualInternacional.php');
                    }break;
                case '13': {
                        include('publicacoesManualNacional.php');
                    }break;
                case '14': {
                        include('identificacaoFormal.php');
                    }break;
                case '15': {
                        include('colaboradores.php');
                    }break;
                case '16': {
                        include('unidadesInvestigacao.php');
                    }break;
                case '17': {
                        include('projectos.php');
                    }break;
                case '18': {
                        include('conferencias.php');
                    }break;
                case '19': {
                        include('apresentacoes.php');
                    }break;
                case '20': {
                        include('arbitragensRevistas.php');
                    }break;
                case '21': {
                        include('premios.php');
                    }break;
                case '22': {
                        include('sc.php');
                    }break;
                case '23': {
                        include('redes.php');
                    }break;
                case '24': {
                        include('missoes.php');
                    }break;
                case '25': {
                        include('aulas.php');
                    }break;
                case '26': {
                        include('orientacoes.php');
                    }break;
                case '27': {
                        include('juris.php');
                    }break;
                case '28': {
                        include('outrasactividades.php');
                    }break;
                case '29': {
                        include('produtos.php');
                    }break;
                case '30': {
                        include('acoesformacao.php');
                    }break;
                case '31': {
                        include('observacoesfinais.php');
                    }break;
            //ALTERACAO
            //Paulo Rodrigues (prodrigues@med.up.pt)
                case '32': {
                        include('consultorias.php');
                    }break;
				case '33':{
					include('comissoes.php');					
				}break;
            }
            echo "</form>";
        }

        function updateLocalQuestionarioData() {
            global $questionario;
            global $saveTab;
            global $activeTab;
            global $anoactual;

            switch ($saveTab) {
                case '0': {
                        // No action set
                    }
                    break;
                case '1': {
                        setInvestigadorData();
                    }
                    break;
                case '2': {
                        setHabilitacaoData();
                        setAcoesFormacaoFrequentada();
                    }
                    break;
                case '3': {
                        setAgregacao();
                    }
                    break;
                case '4': {
                        setOcupacaoData();
                        setExperienciaData();
                    }
                    break;
                case '5': {
                        setCargosUP();
                    }
                    break;
                case '6': {
                        setCargosFMUP();
                    }
                    break;
                case '7': {
                        setCargosHospital();
                    }
                    break;
                case '8': {
                        setDirCursos();
                    }
                    break;
                case '9': {
                        setPublicacoesISI();
                    }
                    break;
                case '10': {
                        setPublicacoesPUBMED();
                    }
                    break;
                case '11': {
                        setPublicacoesMANUALPatentes();
                    }
                    break;
                case '12': {
                        setPublicacoesMANUALInternacional();
                    }
                    break;
                case '13': {
                        setPublicacoesMANUALNacional();
                    }
                    break;
                case '14': {
                        setNomeData();
                    }
                    break;
                case '15': {
                        setColaboradorData();
                        setColaboradorInternoData();
                    }
                    break;
                case '16': {
                        setUnidadeData();
                    }
                    break;
                case '17': {
                        setProjectos();
                        setArbitragensProjetos();
                        setCTs();
                    }
                    break;
                case '18': {
                        setConferencias();
                    }
                    break;
                case '19': {
                        setApresentacoes();
                        setApresentacoesConferencias();
                    }
                    break;
                case '20': {
                        setArbitragensRevistas();
                    }
                    break;
                case '21': {
                        setPremios();
                    }
                    break;
                case '22': {
                        setSC();
                    }
                    break;
                case '23': {
                        setRedes();
                    }
                    break;
                case '24': {
                        setMissoes();
                    }
                    break;
                case '25': {
                        setAulas();
                    }
                    break;
                case '26': {
                        setOrientacoes();
                    }
                    break;
                case '27': {
                        setJuris();
                    }
                    break;
                case '28': {
                        setOutrasActividades();
                        setAcoesDivulgacao();
                    }
                    break;
                case '29': {
                        setProdutos();
                    }
                    break;
                case '30': {
                        setAcoesFormacao();
                        setAcreditacoes();
                    }
                    break;
                case '31': {
                        setObservacoesfinais();
                    }
                    break;
                //ALTERACAO
            //Paulo Rodrigues (prodrigues@med.up.pt)
                case '32': {                    
                    setConsultorias(); 
                }
				break; 
				case '33': {
					setCargosC();
				}break;				
                default: { //echo "DEFAULT";
                        echo "ERRO : No active tab defined while setting data UPDATELOCALQUESTIONARIODATA " . $saveTab . "____" . $activeTab;
                    }
                    break;
            }
        }

        function saveQuestionarioData() {
            global $questionario;
            global $saveTab;
            global $activeTab;
            global $anoactual;

            
            
            switch ($saveTab) {
                case '0': {
                        // No action set
                    }
                    break;
                case '1': {
                        saveInvestigadorData();
                        auditoria($questionario->investigador->login, "", "Grava Tab INVESTIGADOR", "", "");
                    }
                    break;
                case '2': {
                        saveHabilitacaoData();
                        saveAcoesFormacaoFrequentada();
                        auditoria($questionario->investigador->login, "", "Grava Tab HABILITAÇÕES LITERÁRIAS", "", "");
                    }
                    break;
                case '3': {
                        saveAgregacao();
                        auditoria($questionario->investigador->login, "", "Grava Tab AGREGAÇÃO", "", "");
                    }
                    break;
                case '4': {
                        saveOcupacaoData();
                        saveExperienciaData();
                        auditoria($questionario->investigador->login, "", "Grava Tab OCUPAÇÃO e EXPERIÊNCIA PROFISSIONAL", "", "");
                    }
                    break;
                case '5': {
                        saveCargosUP();
                        auditoria($questionario->investigador->login, "", "Grava Tab CARGOS UP", "", "");
                    }
                    break;
                case '6': {
                        saveCargosFMUP();
                        auditoria($questionario->investigador->login, "", "Grava Tab CARGOS FMUP", "", "");
                    }
                    break;
                case '7': {
                        saveCargosHospital();
                        auditoria($questionario->investigador->login, "", "Grava Tab CARGOS \HOSPITAL", "", "");
                    }
                    break;
                case '8': {
                        saveDirCursos();
                        auditoria($questionario->investigador->login, "", "Grava Tab DIREÇÕES de CURSO", "", "");
                    }
                    break;
                case '9': {
                        //savePublicacoesISI();
                    }
                    break;
                case '10': {
                        //savePublicacoesPUBMED();
                    }
                    break;
                case '11': {
                        savePublicacoesMANUALPatentes();
                        auditoria($questionario->investigador->login, "", "Grava Tab PATENTES MANUAL", "", "");
                    }
                    break;
                case '12': {
                        savePublicacoesMANUALInternacional();

                        auditoria($questionario->investigador->login, "", "Grava Tab PUBLICAÇÕES INTERNACIONAIS MANUAL", "", "");
                    }
                    break;
                case '13': {
                        savePublicacoesMANUALNacional();
                        auditoria($questionario->investigador->login, "", "Grava Tab PUBLICAÇÕES NACIONAIS MANUAL", "", "");
                    }
                    break;
                case '14': {
                        saveNomeData();
                        auditoria($questionario->investigador->login, "", "Grava Tab NOME CIENTÍFICO", "", "");
                    }
                    break;
                case '15': {
                        saveColaboradorData();
                        saveColaboradorInternoData();
                        auditoria($questionario->investigador->login, "", "Grava Tab COLABORADORES INTERNOS e EXTERNOS", "", "");
                    }
                    break;
                case '16': {
                        saveUnidadeData();
                        auditoria($questionario->investigador->login, "", "Grava Tab UNIDADES de INVESTIGAÇÃO ", "", "");
                    }
                    break;
                case '17': {
                        saveProjectos();
                        saveArbitragensProjetos();
                        saveCTs();
                        auditoria($questionario->investigador->login, "", "Grava Tab PROJECTOS e ARBITRAGENS de PROJECTOS e CLINICAL TRIALS", "", "");
                    }
                    break;
                case '18': {
                        saveConferencias();
                        auditoria($questionario->investigador->login, "", "Grava Tab CONFERÊNCIAS", "", "");
                    }
                    break;
                case '19': {
                        saveApresentacoes();
                        saveApresentacoesConferencias();
                        auditoria($questionario->investigador->login, "", "Grava Tab APRESENTAÇÕES", "", "");
                    }
                    break;
                case '20': {
                        saveArbitragensRevistas();
                        auditoria($questionario->investigador->login, "", "Grava Tab ARBITRAGENS de REVISTAS", "", "");
                    }
                    break;
                case '21': {
                        savePremios();
                        auditoria($questionario->investigador->login, "", "Grava Tab PRÉMIOS", "", "");
                    }
                    break;
                case '22': {
                        saveSC();
                        auditoria($questionario->investigador->login, "", "Grava Tab SOCIEDADES CIENTÍFICAS", "", "");
                    }
                    break;
                case '23': {
                        saveRedes();
                        auditoria($questionario->investigador->login, "", "Grava Tab REDES", "", "");
                    }
                    break;
                case '24': {
                        saveMissoes();
                        auditoria($questionario->investigador->login, "", "Grava Tab MISSOES", "", "");
                    }
                    break;
                case '25': {
                        saveAulas();
                        auditoria($questionario->investigador->login, "", "Grava Tab AULAS", "", "");
                    }
                    break;
                case '26': {
                        saveOrientacoes();
                        auditoria($questionario->investigador->login, "", "Grava Tab ORIENTAÇÕES", "", "");
                    }
                    break;
                case '27': {
                        saveJuris();
                        auditoria($questionario->investigador->login, "", "Grava Tab JÚRIS", "", "");
                    }
                    break;
                case '28': {
                        saveOutrasActividades();
                        auditoria($questionario->investigador->login, "", "Grava Tab OUTRAS ACTIVIDADES", "", "");
                        saveAcoesDivulgacao();
                        auditoria($questionario->investigador->login, "", "Grava Tab OUTRAS ACTIVIDADES DIVULGACAO", "", "");
                    }
                    break;
                case '29': {
                        saveProdutos();
                        auditoria($questionario->investigador->login, "", "Grava Tab PRODUTOS", "", "");
                    }
                    break;
                case '30': {
                        saveAcoesFormacao();
                        auditoria($questionario->investigador->login, "", "Grava Tab AÇÕES de FORMAÇÃO", "", "");
                        saveAcreditacoes();
                        auditoria($questionario->investigador->login, "", "Grava Tab ACREDITACOES", "", "");
                    }
                    break;
                case '31': {
                        saveObservacoesfinais();
                        auditoria($questionario->investigador->login, "", "Grava Tab OBSERVAÇÕES FINAIS", "", "");
                    }
                    break;
                //ALTERACAO
            //Paulo Rodrigues (prodrigues@med.up.pt)
                case '32': {
                        saveConsultorias();
                        auditoria($questionario->investigador->login, "", "Grava Tab CONSULTORIAS", "", "");
                    }
                    break;
				case '33': {
					echo "SAVING COMISSOES";
					saveCargosC();
					auditoria($questionario->investigador->login,"","Grava Tab CARGOS COMISSAO","","");
				}break;
				
                default: { //echo "DEFAULT";
                        echo "ERRO : No active tab defined while setting data" . $saveTab . "____" . $activeTab;
                        auditoria($questionario->investigador->login, "Erro: No active tab defined while setting data", "Ao GRAVAR os TABS", "", "");
                    }
                    break;
            }
        }

        function saveInvestigadorData() {
            global $questionario;

            $questionario->investigador->saveData();
        }

        function saveHabilitacaoData() {
            global $questionario;


            foreach ($questionario->habilitacoes as $i => $value) {
                $value->saveData();
            }
        }

        function saveArbitragensRevistas() {
            global $questionario;


            foreach ($questionario->arbitragensRevistas as $i => $value) {
                $value->saveData();
            }
        }

        function saveExperienciaData() {
            global $questionario;

            foreach ($questionario->experienciaprofissional as $i => $value) {
                $value->saveData();
            }
        }

        function saveUnidadeData() {
            global $questionario;

            foreach ($questionario->unidadesinvestigacao as $i => $value) {

                $value->saveData();
            }
        }

        function saveAcoesFormacao() {
            global $questionario;

            foreach ($questionario->acoesformacao as $i => $value) {
                $value->saveData();
            }
        }
        
        function saveAcoesFormacaoFrequentada() {
        	global $questionario;
        
        	foreach ($questionario->acoesformacaofrequentada as $i => $value) {
        		$value->saveData();
        	}
        }

        function saveAcoesDivulgacao() {
            global $questionario;

            foreach ($questionario->acoesdivulgacao as $i => $value) {
                $value->saveData();
            }
        }

        function saveAcreditacoes() {
            global $questionario;

            foreach ($questionario->acreditacoes as $i => $value) {
                $value->saveData();
            }
        }

        function saveNomeData() {
            global $questionario;

            foreach ($questionario->identificacaoformal as $i => $value) {

                $value->saveData();
            }
        }

        function saveColaboradorData() {
            global $questionario;

            foreach ($questionario->colaboradores as $i => $value) {

                $value->saveData();
            }
        }

        function saveColaboradorInternoData() {
            global $questionario;

            foreach ($questionario->colaboradoresinternos as $i => $value) {

                $value->saveData();
            }
        }

        function saveOcupacaoData() {
            global $questionario;

            $questionario->ocupacaoprofissional->saveData();
        }

        function saveArbitragensProjetos() {
            global $questionario;

            $questionario->arbitragensProjetos->saveData();
        }

        function saveProjectos() {
            global $questionario;

            foreach ($questionario->projectos as $i => $value) {

                $value->saveData();
            }
        }
        
        function saveCTs() {
        	global $questionario;
        
        	foreach ($questionario->clinicaltrials as $i => $value) {
        
        		$value->saveData();
        	}
        }

        function saveProjectos2010() {
            global $questionario;

            foreach ($questionario->projectos2010 as $i => $value) {

                $value->saveData();
            }
        }

        function saveCargosFMUP() {
            global $questionario;

            foreach ($questionario->cargosFMUP as $i => $value) {

                $value->saveData();
            }
        }
        function saveCargosC(){
        	global $questionario;
        
        	foreach($questionario->cargosCom as $i => $value){
        		
        		$value->saveData();
        	}
        }

        function saveCargosUP() {
            global $questionario;

            foreach ($questionario->cargosUP as $i => $value) {

                $value->saveData();
            }
        }
        
        //ALTERACAO
        //Paulo Rodrigues (prodrigues@med.up.pt)
        function saveConsultorias() {
            global $questionario;

            foreach ($questionario->consultorias as $i => $value) {
                $value->saveData();
            }
        }

        function saveCargosHospital() {
            global $questionario;

            foreach ($questionario->cargosHospitais as $i => $value) {

                $value->saveData();
            }
        }

        function saveDirCursos() {
            global $questionario;
            foreach ($questionario->dirCursos as $i => $value) {
                $value->saveData();
            }
        }

        function savePremios() {
            global $questionario;
            foreach ($questionario->premios as $i => $value) {
                $value->saveData();
            }
        }

        function saveProdutos() {
            global $questionario;
            foreach ($questionario->produtos as $i => $value) {
                $value->saveData();
            }
        }

        function saveOrientacoes() {
            global $questionario;

            foreach ($questionario->orientacoes as $i => $value) {
                $value->saveData();
            }
        }

        function saveAgregacao() {
            global $questionario;

            $questionario->agregacao->saveData();
        }

        function saveObservacoesFinais() {
            global $questionario;

            $questionario->observacoesfinais->saveData();
        }

        function saveJuris() {
            global $questionario;

            $questionario->juris->saveData();
        }

        function saveOutrasActividades() {
            global $questionario;

            $questionario->outrasactividades->saveData();
        }

        function saveAulas() {
            global $questionario;

            $questionario->aulas->saveData();
        }

        function saveApresentacoes() {
            global $questionario;

            $questionario->apresentacoes->saveData();
        }

        function saveConferencias() {
            global $questionario;
            foreach ($questionario->conferencias as $i => $value) {
                $value->saveData();
            }
        }
        
        function saveApresentacoesConferencias() {
        	global $questionario;
        	foreach ($questionario->apresentacoesConferencias as $i => $value) {
        		$value->saveData();
        	}
        }

        function saveSC() {
            global $questionario;
            foreach ($questionario->sc as $i => $value) {
                $value->saveData();
            }
        }

        function saveRedes() {
            global $questionario;
            foreach ($questionario->redes as $i => $value) {
                $value->saveData();
            }
        }

        function saveMissoes() {
            global $questionario;
            foreach ($questionario->missoes as $i => $value) {
                $value->saveData();
            }
        }

        function savePublicacoesManualPatentes() {
            global $questionario;
            foreach ($questionario->publicacoesMANUALPatentes as $i => $value) {
                $value->saveData();
            }
        }

        function savePublicacoesManualInternacional() {
            global $questionario;
            foreach ($questionario->publicacoesMANUALInternacional as $i => $value) {
                $value->saveData();
            }
        }

        function savePublicacoesManualNacional() {
            global $questionario;
            foreach ($questionario->publicacoesMANUALNacional as $i => $value) {
                $value->saveData();
            }
        }

        function setInvestigadorData() {

            global $questionario;

            $questionario->investigador->setData(
                    $_POST['nome'], $_POST['nummec'], $_POST['servico'], $_POST['datanasc'], $_POST['morada'], $_POST['email'], $_POST['email_alternativo'], $_POST['telefone'], $_POST['telemovel'], $_POST['extensao'], $_POST['freguesianat'], $_POST['concelhonat'], $_POST['paisnac'], $_POST['paisnat'], $_POST['numidentificacao'], $_POST['sexo']
            );


            $_SESSION['questionario'] = serialize($questionario);
        }

        function setOcupacaoData() {

            global $questionario;

            $questionario->ocupacaoprofissional->setData(
                    $_POST['docenteper'], "", 
            		$_POST['docentecat'], "",
            		 $_POST['investigadorper'], 
            		"", $_POST['investigadorcat'], "", 
            		$_POST['investigadorbolsa'], "", "", "", "", "", "", "", "", "", $_POST['tecper'], "", $_POST['outraper'], "", "", $_POST['fmupinv'], $_POST['fmupens'], $_POST['fmupgest'], $_POST['fmupdc']
            );

            $_SESSION['questionario'] = serialize($questionario);
        }

        function setArbitragensProjetos() {

            global $questionario;

            $questionario->arbitragensProjetos->setData(
                    isset($_POST['api']) ? $_POST['api'] : "", 
            		isset($_POST['apn']) ? $_POST['apn'] : ""
            );

            $_SESSION['questionario'] = serialize($questionario);
        }

        function setHabilitacaoData() {

            global $questionario;

            foreach ($questionario->habilitacoes as $i => $value) {

                //print_r($value);
                $value->setData(
                		isset($_POST['anoinicio' . $i]) ? $_POST['anoinicio' . $i] : "",
                		isset($_POST['anofim' . $i]) ? $_POST['anofim' . $i] : "",
                		isset($_POST['habcurso_' . $i]) ? $_POST['habcurso_' . $i] : "",
                		isset($_POST['habinstituicao_' . $i]) ? $_POST['habinstituicao_' . $i] : "",
                		isset($_POST['habgrau_' . $i]) ? $_POST['habgrau_' . $i] : "",
                		isset($_POST['distincao' . $i]) ? $_POST['distincao' . $i] : "",
                		isset($_POST['bolsa' . $i]) ? $_POST['bolsa' . $i] : "",
                		isset($_POST['titulo' . $i]) ? $_POST['titulo' . $i] : "",
                		isset($_POST['percentagemHab' . $i]) ? $_POST['percentagemHab' . $i] : "",
                		isset($_POST['haborientador_' . $i]) ? $_POST['haborientador_' . $i] : "",
                		isset($_POST['haboinstituicao_' . $i]) ? $_POST['haboinstituicao_' . $i] : "",
                		isset($_POST['habcorientador_' . $i]) ? $_POST['habcorientador_' . $i] : "",
                		isset($_POST['habcoinstituicao_' . $i]) ? $_POST['habcoinstituicao_' . $i] : ""
                );
            }

            $_SESSION['questionario'] = serialize($questionario);
        }

        function setArbitragensRevistas() {

            global $questionario;

            foreach ($questionario->arbitragensRevistas as $i => $value) {

                //print_r($value);


                $value->setData(
                		isset($_POST['issnAR' . $i]) ? $_POST['issnAR' . $i] : "",
                		'', 
                		isset($_POST['tipoAR' . $i]) ? $_POST['tipoAR' . $i] : "", 
                		isset($_POST['tituloRevistaAR' . $i]) ? $_POST['tituloRevistaAR' . $i] : "",
                		isset($_POST['linkRevistaAR' . $i]) ? $_POST['linkRevistaAR' . $i] : ""
                );
            }

            $_SESSION['questionario'] = serialize($questionario);
        }

        function setUnidadeData() {

            global $questionario;
            foreach ($questionario->unidadesinvestigacao as $i => $value) {
                
            	isset($_POST['responsavel' . $i]) ? $resp=$_POST['responsavel' . $i] : $resp="0";

                $value->setData(
                        isset($_POST['id_unidade' . $i]) ? $_POST['id_unidade' . $i] : "", 
                		isset($_POST['unidade' . $i]) ? $_POST['unidade' . $i] : "", 
                		isset($_POST['ava2007' . $i]) ? $_POST['ava2007' . $i] : "", 
                		isset($_POST['percentagem' . $i]) ? $_POST['percentagem' . $i] : "", 
                		$resp, 
                		"1"
                );
            }
            $_SESSION['questionario'] = serialize($questionario);
        }

        function setAcoesFormacao() {

            global $questionario;
            foreach ($questionario->acoesformacao as $i => $value) {

                $value->setData(
                		isset($_POST['titulo_' . $i]) ? $_POST['titulo_' . $i] : "", 
                		isset($_POST['horas_' . $i]) ? $_POST['horas_' . $i] : "", 
                		isset($_POST['preco_' . $i]) ? $_POST['preco_' . $i] : "", 
                		isset($_POST['formandos_' . $i]) ? $_POST['formandos_' . $i] : "", 
                		isset($_POST['nformandos_' . $i]) ? $_POST['nformandos_' . $i] : "", 
                		isset($_POST['afdataini_' . $i]) ? $_POST['afdataini_' . $i] : "", 
                		isset($_POST['afdatafim_' . $i]) ? $_POST['afdatafim_' . $i] : "", 
                		isset($_POST['local_' . $i]) ? $_POST['local_' . $i] : "", 
                		isset($_POST['objectivo_' . $i]) ? $_POST['objectivo_' . $i] : "",
                		isset($_POST['tipo_' . $i]) ? $_POST['tipo_' . $i] : ""
                );
            }
            $_SESSION['questionario'] = serialize($questionario);
        }

        function setAcoesFormacaoFrequentada() {
        
        	global $questionario;
        	foreach ($questionario->acoesformacaofrequentada as $i => $value) {
        
        		$value->setData(
        				isset($_POST['titulo_' . $i]) ? $_POST['titulo_' . $i] : "",
        				isset($_POST['horas_' . $i]) ? $_POST['horas_' . $i] : "",
        				isset($_POST['affdataini_' . $i]) ? $_POST['affdataini_' . $i] : "",
        				isset($_POST['affdatafim_' . $i]) ? $_POST['affdatafim_' . $i] : "",
        				isset($_POST['local_' . $i]) ? $_POST['local_' . $i] : "",
        				isset($_POST['objectivo_' . $i]) ? $_POST['objectivo_' . $i] : "",
        				isset($_POST['tipo_' . $i]) ? $_POST['tipo_' . $i] : ""
        		);
        	}
        	$_SESSION['questionario'] = serialize($questionario);
        }
        
        function setAcoesDivulgacao() {

            global $questionario;
            foreach ($questionario->acoesdivulgacao as $i => $value) {

                $value->setData(
                		isset($_POST['divulgacao_titulo_' . $i]) ? $_POST['divulgacao_titulo_' . $i] : "" ,
                		isset($_POST['divulgacao_data_' . $i]) ? $_POST['divulgacao_data_' . $i] : "", 
                		isset($_POST['divulgacao_local_' . $i]) ? $_POST['divulgacao_local_' . $i] : "",
                		isset($_POST['npart_' . $i]) ? $_POST['npart_' . $i] : ""
                );
            }
            $_SESSION['questionario'] = serialize($questionario);
        }

        function setAcreditacoes() {

            global $questionario;
            foreach ($questionario->acreditacoes as $i => $value) {

                $value->setData(
                		isset($_POST['acreditacao_instituicao_' . $i]) ? $_POST['acreditacao_instituicao_' . $i] : "",
                		isset($_POST['acreditacao_tipo_' . $i]) ? $_POST['acreditacao_tipo_' . $i] : "",
                		isset($_POST['acreditacao_data_' . $i]) ? $_POST['acreditacao_data_' . $i] : ""
                );
            }
            $_SESSION['questionario'] = serialize($questionario);
        }

        function setNomeData() {

            global $questionario;

            foreach ($questionario->identificacaoformal as $i => $value) {
                $value->setData(
                		isset($_POST['nomecientini' . $i]) ? $_POST['nomecientini' . $i] : "",
                		isset($_POST['nomecientcom' . $i]) ? $_POST['nomecientcom' . $i] : ""
                );
            }

            $_SESSION['questionario'] = serialize($questionario);
        }

        function setColaboradorData() {

            global $questionario;

            foreach ($questionario->colaboradores as $i => $value) {

                //print_r($value);

                $value->setData(
                		isset($_POST['cenomecient' . $i]) ? $_POST['cenomecient' . $i] : "",
                		isset($_POST['ceinstituicao' . $i]) ? $_POST['ceinstituicao' . $i] : "", 
                		isset($_POST['cepais' . $i]) ? $_POST['cepais' . $i] : ""
                );
            }

            $_SESSION['questionario'] = serialize($questionario);
        }

        function setColaboradorInternoData() {

            global $questionario;

            foreach ($questionario->colaboradoresinternos as $i => $value) {

                //print_r($value);

                $value->setData(
                		isset($_POST['cinome' . $i]) ? $_POST['cinome' . $i] : "",
                		isset($_POST['cinomecient' . $i]) ? $_POST['cinomecient' . $i] : "", 
                		isset($_POST['cidepartamento' . $i]) ? $_POST['cidepartamento' . $i] : ""
                );
            }

            $_SESSION['questionario'] = serialize($questionario);
        }

        function setPublicacoesISI() {

            global $questionario;

            $questionario->updatePublicacoesISI();

            $_SESSION['questionario'] = serialize($questionario);
        }

        function setPublicacoesPUBMED() {

            global $questionario;

            $questionario->updatePublicacoesPUBMED();

            $_SESSION['questionario'] = serialize($questionario);
        }

        function setExperienciaData() {

            global $questionario;

            foreach ($questionario->experienciaprofissional as $i => $value) {

                //print_r($value);
                $value->setData(
                		isset($_POST['datainicio' . $i]) ? $_POST['datainicio' . $i] : "",
                		isset($_POST['datafim' . $i]) ? $_POST['datafim' . $i] : "",
                		isset($_POST['semtermo' . $i]) ? $_POST['semtermo' . $i] : "",
                		isset($_POST['posicao' . $i]) ? $_POST['posicao' . $i] : "",
                		isset($_POST['empregador' . $i]) ? $_POST['empregador' . $i] : "",
                		isset($_POST['departamento' . $i]) ? $_POST['departamento' . $i] : "",
                		isset($_POST['percentagem' . $i]) ? $_POST['percentagem' . $i] : ""
                );
            }
            

            $_SESSION['questionario'] = serialize($questionario);
        }

        function setProjectos() {

            global $questionario;

            foreach ($questionario->projectos as $i => $value) {

                //print_r($value);


                $value->setData(
                        isset($_POST['pentidade_' . $i]) ? $_POST['pentidade_' . $i] : "",
                		isset($_POST['pmontante_' . $i]) ? $_POST['pmontante_' . $i] : "",
                		isset($_POST['pmontantea_' . $i]) ? $_POST['pmontantea_' . $i] : "",
                		isset($_POST['pmontantefmup_' . $i]) ? $_POST['pmontantefmup_' . $i] : "",
                		isset($_POST['pinvresponsavel_' . $i]) ? $_POST['pinvresponsavel_' . $i] : "",
                		isset($_POST['pcodigo_' . $i]) ? $_POST['pcodigo_' . $i] : "",
                		isset($_POST['ptitulo_' . $i]) ? $_POST['ptitulo_' . $i] : "",
                		isset($_POST['pdatainicio_' . $i]) ? $_POST['pdatainicio_' . $i] : "",
                		isset($_POST['pdatafim_' . $i]) ? $_POST['pdatafim_' . $i] : "",
                		isset($_POST['pentidadefinanciadora_' . $i]) ? $_POST['pentidadefinanciadora_' . $i] : "",
                		isset($_POST['pacolhimento_' . $i]) ? $_POST['pacolhimento_' . $i] : "",
                		isset($_POST['plink_' . $i]) ? $_POST['plink_' . $i] : "",
                		isset($_POST['pestado_' . $i]) ? $_POST['pestado_' . $i] : ""
                );
            }

            $_SESSION['questionario'] = serialize($questionario);
        }
        
        function setCTs() {
        
        	global $questionario;
        
        	foreach ($questionario->clinicaltrials as $i => $value) {
        
        		//print_r($value)
        		$value->setData(
        				isset($_POST['ctentidade_' . $i]) ? $_POST['ctentidade_' . $i] : "", 
        				isset($_POST['ctmontante_' . $i]) ? $_POST['ctmontante_' . $i] : "", 
        				isset($_POST['ctmontantea_' . $i]) ? $_POST['ctmontantea_' . $i] : "", 
        				isset($_POST['ctmontantefmup_' . $i]) ? $_POST['ctmontantefmup_' . $i] : "", 
        				isset($_POST['ctinvresponsavel_' . $i]) ? $_POST['ctinvresponsavel_' . $i] : "", 
        				isset($_POST['ctcodigo_' . $i]) ? $_POST['ctcodigo_' . $i] : "", 
        				isset($_POST['cttitulo_' . $i]) ? $_POST['cttitulo_' . $i] : "", 
        				isset($_POST['ctdatainicio_' . $i]) ? $_POST['ctdatainicio_' . $i] : "", 
        				isset($_POST['ctdatafim_' . $i]) ? $_POST['ctdatafim_' . $i] : "", 
        				isset($_POST['ctentidadefinanciadora_' . $i]) ? $_POST['ctentidadefinanciadora_' . $i] : "", 
        				isset($_POST['ctacolhimento_' . $i]) ? $_POST['ctacolhimento_' . $i] : "", 
        				isset($_POST['ctlink_' . $i]) ? $_POST['ctlink_' . $i] : "", 
        				isset($_POST['ctestado_' . $i]) ? $_POST['ctestado_' . $i] : "", 
        				isset($_POST['ctestagio_' . $i]) ? $_POST['ctestagio_' . $i] : ""
        		);
        	}
        
        	$_SESSION['questionario'] = serialize($questionario);
        }

        function setCargosFMUP() {

            global $questionario;

            foreach ($questionario->cargosFMUP as $i => $value) {

                $value->setData(
                        1, 
                		isset($_POST['fmuporgao_' . $i]) ? $_POST['fmuporgao_' . $i] : "",
                		isset($_POST['fmupcargo_' . $i]) ? $_POST['fmupcargo_' . $i] : "",
                		isset($_POST['fmupcargodatainicio_' . $i]) ? $_POST['fmupcargodatainicio_' . $i] : "",
                		isset($_POST['fmupcargodatafim_' . $i]) ? $_POST['fmupcargodatafim_' . $i] : ""
                );
            }

            $_SESSION['questionario'] = serialize($questionario);
        }

        function setCargosUP() {

            global $questionario;

            foreach ($questionario->cargosUP as $i => $value) {

                //echo "ORGAO ".$_POST['uporgao_'.$i];

                $value->setData(
                        2, 
                		isset($_POST['uporgao_' . $i]) ? $_POST['uporgao_' . $i] : "",
                		isset($_POST['upcargo_' . $i]) ? $_POST['upcargo_' . $i] : "",
                		isset($_POST['upcargodatainicio_' . $i]) ? $_POST['upcargodatainicio_' . $i] : "",
                		isset($_POST['upcargodatafim_' . $i]) ? $_POST['upcargodatafim_' . $i] : ""
                );
            }

            $_SESSION['questionario'] = serialize($questionario);
        }
        
        //ALTERACAO
        //Paulo Rodrigues (prodrigues@med.up.pt)
         function setConsultorias() {

            global $questionario;

            foreach ($questionario->consultorias as $i => $value) {		
                $value->setData(
                        isset($_POST['consinstituicao_' . $i]) ? $_POST['consinstituicao_' . $i] : "",
                		isset($_POST['consorgao_' . $i]) ? $_POST['consorgao_' . $i] : "",
                		isset($_POST['consdatainicio_' . $i]) ? $_POST['consdatainicio_' . $i] : "",
                		isset($_POST['consdatafim_' . $i]) ? $_POST['consdatafim_' . $i] : ""
                );
            }
            $_SESSION['questionario'] = serialize($questionario);
        }

        function setCargosHospital() {

            global $questionario;

            foreach ($questionario->cargosHospitais as $i => $value) {

                //echo "ORGAO ".$_POST['hospitalorgao_'.$i];
            	
            	
                $value->setData(
                        isset($_POST['hospitalinstituicao_' . $i]) ? $_POST['hospitalinstituicao_' . $i] : "",
                		isset($_POST['hospitalorgao_' . $i]) ? $_POST['hospitalorgao_' . $i] : "",
                		"",
                		isset($_POST['hospitalcargodatainicio_' . $i]) ? $_POST['hospitalcargodatainicio_' . $i] : "",
                		isset($_POST['hospitalcargodatafim_' . $i]) ? $_POST['hospitalcargodatafim_' . $i] : ""
                );
            }

            $_SESSION['questionario'] = serialize($questionario);
        }

        function setDirCursos() {

            global $questionario;

            foreach ($questionario->dirCursos as $i => $value) {

                $value->setData(
                        isset($_POST['dircursosgrau_' . $i]) ? $_POST['dircursosgrau_' . $i] : "", 
                		isset($_POST['dircursonome_' . $i]) ? $_POST['dircursonome_' . $i] : "", 
                		isset($_POST['dircursodatainicio_' . $i]) ? $_POST['dircursodatainicio_' . $i] : "", 
                		isset($_POST['dircursodatafim_' . $i]) ? $_POST['dircursodatafim_' . $i] : "",
                		isset($_POST['dircursocargo_' . $i]) ? $_POST['dircursocargo_' . $i] : ""
                );
            }

            $_SESSION['questionario'] = serialize($questionario);
        }

        function setPremios() {

            global $questionario;

            foreach ($questionario->premios as $i => $value) {

                $value->setData(
                        isset($_POST['nomepremio_' . $i]) ? $_POST['nomepremio_' . $i] : "", 
                		isset($_POST['contextopremio_' . $i]) ? $_POST['contextopremio_' . $i] : "", 
                		isset($_POST['montantepremio_' . $i]) ? $_POST['montantepremio_' . $i] : "", 
                		isset($_POST['tipopremio_' . $i]) ? $_POST['tipopremio_' . $i] : "", 
                		isset($_POST['linkpremio_' . $i]) ? $_POST['linkpremio_' . $i] : ""
                );
            }

            $_SESSION['questionario'] = serialize($questionario);
        }

        function setProdutos() {

            global $questionario;

            foreach ($questionario->produtos as $i => $value) {

                $value->setData(
                        isset($_POST['tipop_' . $i]) ? $_POST['tipop_' . $i] : "",
                		isset($_POST['valorp_' . $i]) ? $_POST['valorp_' . $i] : ""
                );
            }

            $_SESSION['questionario'] = serialize($questionario);
        }

        function setOrientacoes() {

            global $questionario;

            foreach ($questionario->orientacoes as $i => $value) {

                $value->setData(
                        isset($_POST['orientacao_tipo_orientacao_' . $i]) ? $_POST['orientacao_tipo_orientacao_' . $i] : "", 
                		isset($_POST['orientacao_tipo_orientador_' . $i]) ? $_POST['orientacao_tipo_orientador_' . $i] : "", 
                		isset($_POST['orientacao_estado_' . $i]) ? $_POST['orientacao_estado_' . $i] : "", 
                		isset($_POST['orientacao_titulo_' . $i]) ? $_POST['orientacao_titulo_' . $i] : "", 
                		isset($_POST['orientacao_nome_orientando_' . $i]) ? $_POST['orientacao_nome_orientando_' . $i] : ""
                );
            }
            $_SESSION['questionario'] = serialize($questionario);
        }

        function setAgregacao() {

            global $questionario;

            $questionario->agregacao->setData(
                    isset($_POST['agregacao']) ? $_POST['agregacao'] : "",
            		isset($_POST['unanimidade']) ? $_POST['unanimidade'] : "",
            		isset($_POST['dataprevista']) ? $_POST['dataprevista'] : ""
            		);

            $_SESSION['questionario'] = serialize($questionario);
        }

        function setObservacoesfinais() {

            global $questionario;

            $questionario->observacoesfinais->setData(
                    $_POST['incluidomaisdetalhe'], $_POST['incluido'], $_POST['tempo'], $_POST['automatizacao'], $_POST['completividade'], $_POST['organizacao'], $_POST['apresentacao'], $_POST['observacoes']);

            $_SESSION['questionario'] = serialize($questionario);
        }

        function setJuris() {

            global $questionario;

            $questionario->juris->setData(
                    $_POST['agregacaoargfmup'], 
            		$_POST['agregacaovogalfmup'], 
            		$_POST['agregacaoargext'], 
            		$_POST['agregacaovogalext'], 
            		$_POST['doutoramentoargfmup'], 
            		$_POST['doutoramentovogalfmup'], 
            		$_POST['doutoramentoargext'], 
            		$_POST['doutoramentovogalext'], 
            		$_POST['mestradoargfmup'], 
            		$_POST['mestradovogalfmup'], 
            		$_POST['mestradoargext'], 
            		$_POST['mestradovogalext'], 
            		$_POST['mestradointargfmup'],
            		$_POST['mestradointvogalfmup'], 
            		$_POST['mestradointargext'], 
            		$_POST['mestradointvogalext'], 
            		$_POST['provimentovogalfmup'], 
            		$_POST['provimentovogalext']);

            $_SESSION['questionario'] = serialize($questionario);
        }

        function setOutrasActividades() {

            global $questionario;

            $questionario->outrasactividades->setData(
                    isset($_POST['texapoio']) ? $_POST['texapoio'] : "",
            		isset($_POST['livdidaticos']) ? $_POST['livdidaticos'] : "", 
            		isset($_POST['livdivulgacao']) ? $_POST['livdivulgacao'] : "",
            		isset($_POST['acoesdivulgacaoescolas']) ? $_POST['acoesdivulgacaoescolas'] : "", 
            		isset($_POST['acoesdivulgacaogeral']) ? $_POST['acoesdivulgacaogeral'] : "",
            		isset($_POST['app']) ? $_POST['app'] : "", 
            		isset($_POST['webp']) ? $_POST['webp'] : "", 
            		isset($_POST['tvradiopress']) ? $_POST['tvradiopress'] : "");

            $_SESSION['questionario'] = serialize($questionario);
        }

        function setAulas() {

            global $questionario;

            $questionario->aulas->setData(
                    $_POST['regmi'], $_POST['tmi'], $_POST['pmi'], $_POST['regpciclo'], $_POST['regsciclo'], $_POST['regtciclo'], $_POST['tpciclo'], $_POST['tsciclo'], $_POST['ttciclo'], $_POST['ppciclo'], $_POST['psciclo'], $_POST['ptciclo'], $_POST['ncm'], $_POST['nc'], $_POST['regpg'], $_POST['tpg'], $_POST['ppg'], $_POST['neap'], $_POST['nei'], $_POST['nep'], $_POST['ncsm'], $_POST['noutros']);

            $_SESSION['questionario'] = serialize($questionario);
        }

        function setApresentacoes() {

            global $questionario;

            /*$questionario->apresentacoes->setData(
                    $_POST['iconfconv'], $_POST['iconfpart'], $_POST['nconfconv'], $_POST['nconfpart'], $_POST['isem'], $_POST['nsem']);*/
            
            $questionario->apresentacoes->setData(
            		-1, -1, -1, -1, 
            		isset($_POST['isem']) ? $_POST['isem'] : "", 
            		isset($_POST['nsem']) ? $_POST['nsem'] : ""
            		);

            $_SESSION['questionario'] = serialize($questionario);
        }

        function setConferencias() {

            global $questionario;

            foreach ($questionario->conferencias as $i => $value) {

                $value->setData(
                        isset($_POST['confambito_' . $i]) ? $_POST['confambito_' . $i] : "", 
                		isset($_POST['conftipo_' . $i]) ? $_POST['conftipo_' . $i] : "", 
                		isset($_POST['confdatainicio_' . $i]) ? $_POST['confdatainicio_' . $i] : "", 
                		isset($_POST['confdatafim_' . $i]) ? $_POST['confdatafim_' . $i] : "", 
                		isset($_POST['conftitulo_' . $i]) ? $_POST['conftitulo_' . $i] : "", 
                		isset($_POST['conflocal_' . $i]) ? $_POST['conflocal_' . $i] : "", 
                		isset($_POST['confparticipantes_' . $i]) ? $_POST['confparticipantes_' . $i] : "", 
                		isset($_POST['conflink_' . $i]) ? $_POST['conflink_' . $i] : ""
                );
            }

            $_SESSION['questionario'] = serialize($questionario);
        }
        
        function setApresentacoesConferencias() {
        
        	global $questionario;
        
        	foreach ($questionario->apresentacoesConferencias as $i => $value) {
        
        		$value->setData(
        				isset($_POST['apresconfambito_' . $i]) ? $_POST['apresconfambito_' . $i] : "",
        				 isset($_POST['apresconftipo_' . $i]) ? $_POST['apresconftipo_' . $i] : "",
        				 isset($_POST['apresconfdatainicio_' . $i]) ? $_POST['apresconfdatainicio_' . $i] : "",
        				 isset($_POST['apresconfdatafim_' . $i]) ? $_POST['apresconfdatafim_' . $i] : "",
        				 isset($_POST['apresconftitulo_' . $i]) ? $_POST['apresconftitulo_' . $i] : "",
        				 isset($_POST['apresconflocal_' . $i]) ? $_POST['apresconflocal_' . $i] : "",
        				 isset($_POST['apresconfparticipantes_' . $i]) ? $_POST['apresconfparticipantes_' . $i] : "",
        				 isset($_POST['apresconflink_' . $i]) ? $_POST['apresconflink_' . $i] : ""
        		);
        	}
        
        	$_SESSION['questionario'] = serialize($questionario);
        }

        function setSC() {

            global $questionario;

            foreach ($questionario->sc as $i => $value) {

                $value->setData(
                        isset($_POST['scnome_' . $i]) ? $_POST['scnome_' . $i] : "", 
                		isset($_POST['sctipo_' . $i]) ? $_POST['sctipo_' . $i] : "", 
                		isset($_POST['scdatainicio_' . $i]) ? $_POST['scdatainicio_' . $i] : "", 
                		isset($_POST['scdatafim_' . $i]) ? $_POST['scdatafim_' . $i] : "", 
                		isset($_POST['sccargo_' . $i]) ? $_POST['sccargo_' . $i] : "", 
                		isset($_POST['sclink_' . $i]) ? $_POST['sclink_' . $i] : ""
                );
            }

            $_SESSION['questionario'] = serialize($questionario);
        }

        function setRedes() {

            global $questionario;

            foreach ($questionario->redes as $i => $value) {

                $value->setData(
                        isset($_POST['redenome_' . $i]) ? $_POST['redenome_' . $i] : "", 
                		isset($_POST['redetipo_' . $i]) ? $_POST['redetipo_' . $i] : "", 
                		isset($_POST['rededatainicio_' . $i]) ? $_POST['rededatainicio_' . $i] : "", 
                		isset($_POST['rededatafim_' . $i]) ? $_POST['rededatafim_' . $i] : "", 
                		isset($_POST['redecargo_' . $i]) ? $_POST['redecargo_' . $i] : "", 
                		isset($_POST['redelink_' . $i]) ? $_POST['redelink_' . $i] : ""
                );
            }

            $_SESSION['questionario'] = serialize($questionario);
        }

        function setMissoes() {

            global $questionario;

            foreach ($questionario->missoes as $i => $value) {

                $value->setData(
                        isset($_POST['missoesdatainicio_' . $i]) ? $_POST['missoesdatainicio_' . $i] : "", 
                		isset($_POST['missoesdatafim_' . $i]) ? $_POST['missoesdatafim_' . $i] : "", 
                		isset($_POST['missoesmotivacao_' . $i]) ? $_POST['missoesmotivacao_' . $i] : "", 
                		isset($_POST['missoesinstituicao_' . $i]) ? $_POST['missoesinstituicao_' . $i] : "", 
                		isset($_POST['missoespais_' . $i]) ? $_POST['missoespais_' . $i] : "", 
                		isset($_POST['missoesambtese_' . $i]) ? $_POST['missoesambtese_' . $i] : ""
                		);
            }

            $_SESSION['questionario'] = serialize($questionario);
        }

        function setPublicacoesMANUALPatentes() {

            global $questionario;

            foreach ($questionario->publicacoesMANUALPatentes as $i => $value) {

                if ($value->tipo != "")
                    $tipo = $value->tipo;
                else
                    $tipo = $_POST['pmtipo_' . $i];

                if (!isset($_POST['pmestado_' . $i]))
                    $estado = 0;
                else
                    $estado = $_POST['pmestado_' . $i];

                $value->setData(
                        $tipo, 
                		isset($_POST['pmnomepublicacao_' . $i]) ? $_POST['pmnomepublicacao_' . $i] : "",
                		isset($_POST['pmissn_' . $i]) ? $_POST['pmissn_' . $i] : "",
                		isset($_POST['pmissn_linking_' . $i]) ? $_POST['pmissn_linking_' . $i] : "",
                		isset($_POST['pmissn_electronic_' . $i]) ? $_POST['pmissn_electronic_' . $i] : "",
                		isset($_POST['pmisbn_' . $i]) ? $_POST['pmisbn_' . $i] : "",
                		isset($_POST['pmtitulo_' . $i]) ? $_POST['pmtitulo_' . $i] : "", 
                		isset($_POST['pmautores_' . $i]) ? $_POST['pmautores_' . $i] : "", 
                		isset($_POST['pmvolume_' . $i]) ? $_POST['pmvolume_' . $i] : "", 
                		isset($_POST['pmissue_' . $i]) ? $_POST['pmissue_' . $i] : "", 
                		isset($_POST['pmar_' . $i]) ? $_POST['pmar_' . $i] : "", 
                		isset($_POST['pmcolecao_' . $i]) ? $_POST['pmcolecao_' . $i] : "", 
                		isset($_POST['pmprimpagina_' . $i]) ? $_POST['pmprimpagina_' . $i] : "", 
                		isset($_POST['pmultpagina_' . $i]) ? $_POST['pmultpagina_' . $i] : "", 
                		isset($_POST['pmifactor_' . $i]) ? $_POST['pmifactor_' . $i] : "", 
                		isset($_POST['pmcitacoes_' . $i]) ? $_POST['pmcitacoes_' . $i] : "", 
                		isset($_POST['pmnpatente_' . $i]) ? $_POST['pmnpatente_' . $i] : "", 
                		isset($_POST['pmdatapatente_' . $i]) ? $_POST['pmdatapatente_' . $i] : "", 
                		isset($_POST['pmipc_' . $i]) ? $_POST['pmipc_' . $i] : "", 
                		isset($_POST['pmeditora_' . $i]) ? $_POST['pmeditora_' . $i] : "", 
                		isset($_POST['pmconftitle_' . $i]) ? $_POST['pmconftitle_' . $i] : "", 
                		isset($_POST['pmlanguage_' . $i]) ? $_POST['pmlanguage_' . $i] : "", 
                		isset($_POST['pmeditor_' . $i]) ? $_POST['pmeditor_' . $i] : "",
                		$tipo, 
                		isset($_POST['pmlink_' . $i]) ? $_POST['pmlink_' . $i] : "", 
                		$estado,
                		""
                );
            }

            $_SESSION['questionario'] = serialize($questionario);
        }

        function setPublicacoesMANUALInternacional() {

            global $questionario;

            foreach ($questionario->publicacoesMANUALInternacional as $i => $value) {

                if ($value->tipo != "")
                    $tipo = $value->tipo;
                else
                    $tipo = $_POST['pmtipo_' . $i];

                if (!isset($_POST['pmestado_' . $i]))
                    $estado = 0;
                else
                    $estado = $_POST['pmestado_' . $i];

                $value->setData(
                        $tipo, 
                		isset($_POST['pmnomepublicacao_' . $i]) ? $_POST['pmnomepublicacao_' . $i] : "",
                		isset($_POST['pmissn_' . $i]) ? $_POST['pmissn_' . $i] : "",
                		isset($_POST['pmissn_linking_' . $i]) ? $_POST['pmissn_linking_' . $i] : "",
                		isset($_POST['pmissn_electronic_' . $i]) ? $_POST['pmissn_electronic_' . $i] : "",
                		isset($_POST['pmisbn_' . $i]) ? $_POST['pmisbn_' . $i] : "",
                		isset($_POST['pmtitulo_' . $i]) ? $_POST['pmtitulo_' . $i] : "", 
                		isset($_POST['pmautores_' . $i]) ? $_POST['pmautores_' . $i] : "", 
                		isset($_POST['pmvolume_' . $i]) ? $_POST['pmvolume_' . $i] : "", 
                		isset($_POST['pmissue_' . $i]) ? $_POST['pmissue_' . $i] : "", 
                		isset($_POST['pmar_' . $i]) ? $_POST['pmar_' . $i] : "", 
                		isset($_POST['pmcolecao_' . $i]) ? $_POST['pmcolecao_' . $i] : "", 
                		isset($_POST['pmprimpagina_' . $i]) ? $_POST['pmprimpagina_' . $i] : "", 
                		isset($_POST['pmultpagina_' . $i]) ? $_POST['pmultpagina_' . $i] : "", 
                		isset($_POST['pmifactor_' . $i]) ? $_POST['pmifactor_' . $i] : "", 
                		isset($_POST['pmcitacoes_' . $i]) ? $_POST['pmcitacoes_' . $i] : "", 
                		isset($_POST['pmnpatente_' . $i]) ? $_POST['pmnpatente_' . $i] : "", 
                		isset($_POST['pmdatapatente_' . $i]) ? $_POST['pmdatapatente_' . $i] : "", 
                		isset($_POST['pmipc_' . $i]) ? $_POST['pmipc_' . $i] : "", 
                		isset($_POST['pmeditora_' . $i]) ? $_POST['pmeditora_' . $i] : "", 
                		isset($_POST['pmconftitle_' . $i]) ? $_POST['pmconftitle_' . $i] : "", 
                		isset($_POST['pmlanguage_' . $i]) ? $_POST['pmlanguage_' . $i] : "", 
                		isset($_POST['pmeditor_' . $i]) ? $_POST['pmeditor_' . $i] : "",
                		$tipo, 
                		isset($_POST['pmlink_' . $i]) ? $_POST['pmlink_' . $i] : "", 
                		$estado,
                		""
                );
            }

            $_SESSION['questionario'] = serialize($questionario);
        }

        function setPublicacoesMANUALNacional() {

            global $questionario;

            foreach ($questionario->publicacoesMANUALNacional as $i => $value) {

                if ($value->tipo != "")
                    $tipo = $value->tipo;
                else
                    $tipo = $_POST['pmtipo_' . $i];

                if (!isset($_POST['pmestado_' . $i]))
                    $estado = 0;
                else
                    $estado = $_POST['pmestado_' . $i];



                $value->setData(
                		$tipo, 
                		isset($_POST['pmnomepublicacao_' . $i]) ? $_POST['pmnomepublicacao_' . $i] : "",
                		isset($_POST['pmissn_' . $i]) ? $_POST['pmissn_' . $i] : "",
                		isset($_POST['pmissn_linking_' . $i]) ? $_POST['pmissn_linking_' . $i] : "",
                		isset($_POST['pmissn_electronic_' . $i]) ? $_POST['pmissn_electronic_' . $i] : "",
                		isset($_POST['pmisbn_' . $i]) ? $_POST['pmisbn_' . $i] : "",
                		isset($_POST['pmtitulo_' . $i]) ? $_POST['pmtitulo_' . $i] : "", 
                		isset($_POST['pmautores_' . $i]) ? $_POST['pmautores_' . $i] : "", 
                		isset($_POST['pmvolume_' . $i]) ? $_POST['pmvolume_' . $i] : "", 
                		isset($_POST['pmissue_' . $i]) ? $_POST['pmissue_' . $i] : "", 
                		isset($_POST['pmar_' . $i]) ? $_POST['pmar_' . $i] : "", 
                		isset($_POST['pmcolecao_' . $i]) ? $_POST['pmcolecao_' . $i] : "", 
                		isset($_POST['pmprimpagina_' . $i]) ? $_POST['pmprimpagina_' . $i] : "", 
                		isset($_POST['pmultpagina_' . $i]) ? $_POST['pmultpagina_' . $i] : "", 
                		isset($_POST['pmifactor_' . $i]) ? $_POST['pmifactor_' . $i] : "", 
                		isset($_POST['pmcitacoes_' . $i]) ? $_POST['pmcitacoes_' . $i] : "", 
                		isset($_POST['pmnpatente_' . $i]) ? $_POST['pmnpatente_' . $i] : "", 
                		isset($_POST['pmdatapatente_' . $i]) ? $_POST['pmdatapatente_' . $i] : "", 
                		isset($_POST['pmipc_' . $i]) ? $_POST['pmipc_' . $i] : "", 
                		isset($_POST['pmeditora_' . $i]) ? $_POST['pmeditora_' . $i] : "", 
                		isset($_POST['pmconftitle_' . $i]) ? $_POST['pmconftitle_' . $i] : "", 
                		isset($_POST['pmlanguage_' . $i]) ? $_POST['pmlanguage_' . $i] : "", 
                		isset($_POST['pmeditor_' . $i]) ? $_POST['pmeditor_' . $i] : "",
                		$tipo, 
                		isset($_POST['pmlink_' . $i]) ? $_POST['pmlink_' . $i] : "", 
                		$estado,
                		""
                );
            }

            $_SESSION['questionario'] = serialize($questionario);
        }

        function formLogout() {

            global $questionario;
            $err = "";

            auditoria($questionario->investigador->login, $err, 'LOGOUT', "", "");

            session_unset();
            $_SESSION = array();
            unset($_SESSION['questionario']);
            setcookie(session_name(), "", 0, "/");

            header("Location: ../index.php");
        }

        function validaPublicacoesPatentes() {
            global $questionario;
            global $anoactual;

            $erroPMAN = "";

            $PMAN = 0;

            $message = "";




            foreach ($questionario->publicacoesMANUALPatentes as $i => $value) {

                $tipo = $questionario->publicacoesMANUALPatentes[$i]->tipofmup;

                //echo $tipo;



                $PMAN++;
                if ($questionario->publicacoesMANUALPatentes[$i]->titulo == "") {
                    $erroPMAN = $erroPMAN . "Publicações " . $anoactual . "/Patentes Manuais - PATENTES (Linha " . $PMAN . ") - Título Vazio<br/>";
                }
                if ($questionario->publicacoesMANUALPatentes[$i]->npatente == "") {
                    $erroPMAN = $erroPMAN . "Publicações " . $anoactual . "/Patentes Manuais - PATENTES (Linha " . $PMAN . ") - Nº de Patente inexistente<br/>";
                }
                if ($questionario->publicacoesMANUALPatentes[$i]->ultpagina != "" && $questionario->publicacoesMANUALPatentes[$i]->primpagina != "") {
                    $erroPMAN = $erroPMAN . "Publicações " . $anoactual . "/Patentes Manuais - PATENTES (Linha " . $PMAN . ") - Deve indicar a primeira e última página <br/>";
                }


                if (date("Y", strtotime($questionario->publicacoesMANUALPatentes[$i]->datapatente)) != $anoactual) {
                    $erroPMAN = $erroPMAN . "Publicações " . $anoactual . "/Patentes Manuais - PATENTES (Linha " . $PMAN . ") - A data não inclui o ano " . $anoactual . "<br/>";
                }
                if (!checkAutor($questionario->publicacoesMANUALPatentes[$i]->autores, $questionario->investigador->id)) {
                    $erroPMAN = $erroPMAN . "Publicações " . $anoactual . "/Patentes Manuais - PATENTES (Linha " . $PMAN . ") - Nenhum dos seus nomes científicos consta da lista de autores<br/>";
                }
            }


            if ($PMAN > 0) {
                $message = $message . $erroPMAN;
            }//."Publicações ".$anoactual."/Patentes - Existem patentes com título vazio";}

            return $message;
        }

        function validaPublicacoesInternacional() {
            global $questionario;
            global $anoactual;


            $erroJMAN = "";

            $erroPRPMAN = "";

            $erroJAMAN = "";

            $erroCPMAN = "";

            $erroSMAN = "";

            $erroLMAN = "";

            $erroCLMAN = "";

            $erroPMAN = "";

            $erroOAMAN = "";

            $JMAN = 0;

            $PRPMAN = 0;

            $JAMAN = 0;

            $CPMAN = 0;

            $SMAN = 0;

            $LMAN = 0;

            $CLMAN = 0;

            $OAMAN = 0;

            $message = "";
            
            foreach ($questionario->publicacoesMANUALInternacional as $i => $value) {

                $tipo = $questionario->publicacoesMANUALInternacional[$i]->tipofmup;

                //echo $tipo;

                switch ($tipo) {
                    case 'J': { // Op√ß√£o de entrada
                            $JMAN++;
                            if ($questionario->publicacoesMANUALInternacional[$i]->titulo == "") {
                                $erroJMAN = $erroJMAN . "Publicações/Publicações Manuais Internacionais - ARTIGOS (Linha " . $JMAN . ") - título Vazio<br/>";
                            }
                            if ($questionario->publicacoesMANUALInternacional[$i]->issn != "") {
                                if (strpos($questionario->publicacoesMANUALInternacional[$i]->issn, "-") != 4 || strlen($questionario->publicacoesMANUALInternacional[$i]->issn) != 9) {
                                    $erroJMAN = $erroJMAN . "Publicações/Publicações Manuais Internacionais - ARTIGOS (Linha " . $JMAN . ") - ISSN Errado<br/>";
                                }
                            }
                            if ($questionario->publicacoesMANUALInternacional[$i]->ultpagina != "" && $questionario->publicacoesMANUALInternacional[$i]->primpagina != "") {
                                if (intval($questionario->publicacoesMANUALInternacional[$i]->ultpagina) - intval($questionario->publicacoesMANUALInternacional[$i]->primpagina) < 0) {
                                    $erroJMAN = $erroJMAN . "Publicações/Publicações Manuais Internacionais - ARTIGOS (Linha " . $JMAN . ") - Páginas Erradas<br/>";
                                }
                                if (intval($questionario->publicacoesMANUALInternacional[$i]->ultpagina) - intval($questionario->publicacoesMANUALInternacional[$i]->primpagina) < 2) {
                                    $erroJMAN = $erroJMAN . "Publicações/Publicações Manuais Internacionais - ARTIGOS (Linha " . $JMAN . ") - Nº de páginas deve superior ou igual a 2<br/>";
                                }
                            }
                            if (!checkAutor($questionario->publicacoesMANUALInternacional[$i]->autores, $questionario->investigador->id)) {
                                $erroJMAN = $erroJMAN . "Publicações/Publicações Manuais Internacionais - ARTIGOS (Linha " . $JMAN . ") - Nenhum dos seus nomes científicos consta da lista de autores<br/>";
                            }
                        }
                        break;
                    case 'PRP': { // Op√ß√£o de entrada
                            $PRPMAN++;
                            if ($questionario->publicacoesMANUALInternacional[$i]->titulo == "") {
                                $erroPRPMAN = $erroPRPMAN . "Publicações/Publicações Manuais Internacionais - PEER REVIEW PROCEEDINGS (Linha " . $PRPMAN . ") - título Vazio<br/>";
                            }
                            if ($questionario->publicacoesMANUALInternacional[$i]->issn != "") {
                                if (strpos($questionario->publicacoesMANUALInternacional[$i]->issn, "-") != 4 || strlen($questionario->publicacoesMANUALInternacional[$i]->issn) != 9) {
                                    $erroPRPMAN = $erroPRPMAN . "Publicações/Publicações Manuais Internacionais - PEER REVIEW PROCEEDINGS (Linha " . $PRPMAN . ") - ISSN Errado<br/>";
                                }
                            }
                            if ($questionario->publicacoesMANUALInternacional[$i]->ultpagina != "" && $questionario->publicacoesMANUALInternacional[$i]->primpagina != "") {
                                if (intval($questionario->publicacoesMANUALInternacional[$i]->ultpagina) - intval($questionario->publicacoesMANUALInternacional[$i]->primpagina) < 0) {
                                    $erroPRPMAN = $erroPRPMAN . "Publicações/Publicações Manuais Internacionais - PEER REVIEW PROCEEDINGS (Linha " . $PRPMAN . ") - Páginas Erradas<br/>";
                                }
                                if (intval($questionario->publicacoesMANUALInternacional[$i]->ultpagina) - intval($questionario->publicacoesMANUALInternacional[$i]->primpagina) < 2) {
                                    $erroPRPMAN = $erroPRPMAN . "Publicações/Publicações Manuais Internacionais - PEER REVIEW PROCEEDINGS (Linha " . $PRPMAN . ") - Nº de páginas deve superior ou igual a 2<br/>";
                                }
                            }
                            if (!checkAutor($questionario->publicacoesMANUALInternacional[$i]->autores, $questionario->investigador->id)) {
                                $erroPRPMAN = $erroPRPMAN . "Publicações/Publicações Manuais Internacionais - PEER REVIEW PROCEEDINGS (Linha " . $PRPMAN . ") - Nenhum dos seus nomes científicos consta da lista de autores<br/>";
                            }
                        }
                        break;
                    case 'JA': { // Op√ß√£o de entrada
                            $JAMAN++;
                            if ($questionario->publicacoesMANUALInternacional[$i]->titulo == "") {
                                $erroJAMAN = $erroJAMAN . "Publicações/Publicações Manuais Internacionais - SUMÁRIOS (Linha " . $JAMAN . ") - Título Vazio<br/>";
                            }
                            if ($questionario->publicacoesMANUALInternacional[$i]->issn != "") {
                                if (strpos($questionario->publicacoesMANUALInternacional[$i]->issn, "-") != 4 || strlen($questionario->publicacoesMANUALInternacional[$i]->issn) != 9) {
                                    $erroJAMAN = $erroJAMAN . "Publicações/Publicações Manuais Internacionais - SUMÁRIOS (Linha " . $JAMAN . ") - ISSN Errado<br/>";
                                }
                            }
                            if ($questionario->publicacoesMANUALInternacional[$i]->ultpagina != "" && $questionario->publicacoesMANUALInternacional[$i]->primpagina != "") {
                                if (intval($questionario->publicacoesMANUALInternacional[$i]->ultpagina) - intval($questionario->publicacoesMANUALInternacional[$i]->primpagina) < 0) {
                                    $erroJAMAN = $erroJAMAN . "Publicações/Publicações Manuais Internacionais - SUMÁRIOS (Linha " . $JAMAN . ") - Páginas Erradas<br/>";
                                }
                                if (intval($questionario->publicacoesMANUALInternacional[$i]->ultpagina) - intval($questionario->publicacoesMANUALInternacional[$i]->primpagina) >= 2) {
                                    $erroJAMAN = $erroJAMAN . "Publicações/Publicações Manuais Internacionais - SUMÁRIOS (Linha " . $JAMAN . ") - Nº de páginas deve ser 1<br/>";
                                }
                            }
                            if (!checkAutor($questionario->publicacoesMANUALInternacional[$i]->autores, $questionario->investigador->id)) {
                                $erroJAMAN = $erroJAMAN . "Publicações/Publicações Manuais Internacionais - SUMÁRIOS (Linha " . $JAMAN . ") - Nenhum dos seus nomes científicos consta da lista de autores<br/>";
                            }
                        }
                        break;
                    case 'CP': { // Op√ß√£o de entrada
                            $CPMAN++;
                            if ($questionario->publicacoesMANUALInternacional[$i]->titulo == "") {
                                $erroCPMAN = $erroCPMAN . "Publicações/Publicações Manuais Internacionais - CONFERENCE PROCEEDINGS (Linha " . $CPMAN . ") - Título Vazio<br/>";
                            }
                            if ($questionario->publicacoesMANUALInternacional[$i]->isbn != "") {
                                if (strlen($questionario->publicacoesMANUALInternacional[$i]->isbn) != 13) {

                                    $erroCPMAN = $erroCPMAN . "Publicações/Publicações Manuais Internacionais - CONFERENCE PROCEEDINGS (Linha " . $CPMAN . ") - ISBN Deve ser composto por 13 d√≠gitos<br/>";
                                }
                            }
                            if ($questionario->publicacoesMANUALInternacional[$i]->ultpagina != "" && $questionario->publicacoesMANUALInternacional[$i]->primpagina != "") {
                                if (intval($questionario->publicacoesMANUALInternacional[$i]->ultpagina) - intval($questionario->publicacoesMANUALInternacional[$i]->primpagina) < 0) {
                                    $erroCPMAN = $erroCPMAN . "Publicações/Publicações Manuais Internacionais - CONFERENCE PROCEEDINGS (Linha " . $CPMAN . ") - Páginas Erradas<br/>";
                                }
                                if (intval($questionario->publicacoesMANUALInternacional[$i]->ultpagina) - intval($questionario->publicacoesMANUALInternacional[$i]->primpagina) < 2) {
                                    $erroCPMAN = $erroCPMAN . "Publicações/Publicações Manuais Internacionais - CONFERENCE PROCEEDINGS (Linha " . $CPMAN . ") - Nº de páginas deve ser superior ou igual a 2<br/>";
                                }
                            }
                            if (!checkAutor($questionario->publicacoesMANUALInternacional[$i]->autores, $questionario->investigador->id)) {
                                $erroCPMAN = $erroCPMAN . "Publicações/Publicações Manuais Internacionais - CONFERENCE PROCEEDINGS (Linha " . $CPMAN . ") - Nenhum dos seus nomes científicos consta da lista de autores<br/>";
                            }
                        }
                        break;
                    case 'S': { // Op√ß√£o de entrada
                            $SMAN++;
                            if ($questionario->publicacoesMANUALInternacional[$i]->titulo == "") {
                                $erroSMAN = $erroSMAN . "Publicações/Publicações Manuais Internacionais - Sumários (Linha " . $SMAN . ") - Título Vazio<br/>";
                            }
                            if ($questionario->publicacoesMANUALInternacional[$i]->issn != "") {
                                if (strpos($questionario->publicacoesMANUALInternacional[$i]->issn, "-") != 4 || strlen($questionario->publicacoesMANUALInternacional[$i]->issn) != 9) {
                                    $erroSMAN = $erroSMAN . "Publicações/Publicações Manuais Internacionais - Sumários (Linha " . $SMAN . ") - ISSN Errado<br/>";
                                }
                            }
                            if ($questionario->publicacoesMANUALInternacional[$i]->ultpagina != "" && $questionario->publicacoesMANUALInternacional[$i]->primpagina != "") {
                                if (intval($questionario->publicacoesMANUALInternacional[$i]->ultpagina) - intval($questionario->publicacoesMANUALInternacional[$i]->primpagina) < 0) {
                                    $erroSMAN = $erroSMAN . "Publicações/Publicações Manuais Internacionais - Sumários (Linha " . $SMAN . ") - Nº de Páginas Errado<br/>";
                                }
                            }
                            if (!checkAutor($questionario->publicacoesMANUALInternacional[$i]->autores, $questionario->investigador->id)) {
                                $erroSMAN = $erroSMAN . "Publicações/Publicações Manuais Internacionais - Sumários (Linha " . $SMAN . ") - Nenhum dos seus nomes científicos consta da lista de autores<br/>";
                            }
                        }
                        break;
                    case 'L': { // Op√ß√£o de entrada
                            $LMAN++;
                            if ($questionario->publicacoesMANUALInternacional[$i]->titulo == "") {
                                $erroLMAN = $erroLMAN . "Publicações/Publicações Manuais Internacionais - LIVROS (Linha " . $LMAN . ") - Título Vazio<br/>";
                            }
                            if ($questionario->publicacoesMANUALInternacional[$i]->isbn != "") {
                                if (strlen($questionario->publicacoesMANUALInternacional[$i]->isbn) != 13) {
                                    $erroLMAN = $erroLMAN . "Publicações/Publicações Manuais Internacionais - LIVROS (Linha " . $LMAN . ") - ISBN Deve ser composto por 13 d√≠gitos<br/>";
                                }
                            }
                            if ($questionario->publicacoesMANUALInternacional[$i]->ultpagina != "" && $questionario->publicacoesMANUALInternacional[$i]->primpagina != "") {
                                if (intval($questionario->publicacoesMANUALInternacional[$i]->ultpagina) - intval($questionario->publicacoesMANUALInternacional[$i]->primpagina) < 0) {
                                    $erroLMAN = $erroLMAN . "Publicações/Publicações Manuais Internacionais - LIVROS (Linha " . $LMAN . ") - Nº de Páginas Errado<br/>";
                                }
                            }
                            if (!checkAutor($questionario->publicacoesMANUALInternacional[$i]->autores, $questionario->investigador->id)) {
                                $erroLMAN = $erroLMAN . "Publicações/Publicações Manuais Internacionais - LIVROS (Linha " . $LMAN . ") - Nenhum dos seus nomes científicos consta da lista de autores<br/>";
                            }
                        }
                        break;
                    case 'CL': { // Op√ß√£o de entrada
                            $CLMAN++;
                            if ($questionario->publicacoesMANUALInternacional[$i]->titulo == "") {
                                $erroCLMAN = $erroCLMAN . "Publicações/Publicações Manuais Internacionais - CAPÍTULOS DE LIVROS (Linha " . $CLMAN . ") - Título Vazio<br/>";
                            }
                            if ($questionario->publicacoesMANUALInternacional[$i]->isbn != "") {
                                if (strlen($questionario->publicacoesMANUALInternacional[$i]->isbn) != 13) {
                                    $erroCLMAN = $erroCLMAN . "Publicações/Publicações Manuais Internacionais - CAPÍTULOS DE LIVROS (Linha " . $CLMAN . ") - ISBN Deve ser composto por 13 d√≠gitos<br/>";
                                }
                            }
                            if ($questionario->publicacoesMANUALInternacional[$i]->ultpagina != "" && $questionario->publicacoesMANUALInternacional[$i]->primpagina != "") {
                                if (intval($questionario->publicacoesMANUALInternacional[$i]->ultpagina) - intval($questionario->publicacoesMANUALInternacional[$i]->primpagina) < 0) {
                                    $erroCLMAN = $erroCLMAN . "Publicações/Publicações Manuais Internacionais - CAPÍTULOS DE LIVROS (Linha " . $CLMAN . ") - Nº de Páginas Errado<br/>";
                                }
                            }
                            if (!checkAutor($questionario->publicacoesMANUALInternacional[$i]->autores, $questionario->investigador->id)) {
                                $erroCLMAN = $erroCLMAN . "Publicações/Publicações Manuais Internacionais - CAPÍTULOS DE LIVROS (Linha " . $CLMAN . ") - Nenhum dos seus nomes científicos consta da lista de autores<br/>";
                            }
                        }
                        break;
                    case 'OA': { // Op√ß√£o de entrada
                            $OAMAN++;
                            if ($questionario->publicacoesMANUALInternacional[$i]->titulo == "") {
                                $erroOAMAN = $erroOAMAN . "Publicações/Publicações Manuais Internacionais - OUTROS SUMÁRIOS (Linha " . $OAMAN . ") - Título Vazio<br/>";
                            }
                            if ($questionario->publicacoesMANUALInternacional[$i]->isbn != "") {
                                if (strlen($questionario->publicacoesMANUALInternacional[$i]->isbn) != 13) {
                                    $erroOAMAN = $erroOAMAN . "Publicações/Publicações Manuais Internacionais - OUTROS SUMÁRIOS (Linha " . $OAMAN . ") - ISBN Deve ser composto por 13 d√≠gitos<br/>";
                                }
                            }
                            if ($questionario->publicacoesMANUALInternacional[$i]->ultpagina != "" && $questionario->publicacoesMANUALInternacional[$i]->primpagina != "") {
                                if (intval($questionario->publicacoesMANUALInternacional[$i]->ultpagina) - intval($questionario->publicacoesMANUALInternacional[$i]->primpagina) < 0) {
                                    $erroOAMAN = $erroOAMAN . "Publicações/Publicações Manuais Internacionais - OUTROS SUMÁRIOS (Linha " . $OAMAN . ") - Páginas Erradas<br/>";
                                }
                                if (intval($questionario->publicacoesMANUALInternacional[$i]->ultpagina) - intval($questionario->publicacoesMANUALInternacional[$i]->primpagina) >= 2) {
                                    $erroOAMAN = $erroOAMAN . "Publicações/Publicações Manuais Internacionais - OUTROS SUMÁRIOS (Linha " . $OAMAN . ") - Nº de páginas deve ser 1<br/>";
                                }
                            }
                            if (!checkAutor($questionario->publicacoesMANUALInternacional[$i]->autores, $questionario->investigador->id)) {
                                $erroOAMAN = $erroOAMAN . "Publicações/Publicações Manuais Internacionais - OUTROS SUMÁRIOS (Linha " . $OAMAN . ") - Nenhum dos seus nomes científicos consta da lista de autores<br/>";
                            }
                        }
                        break;
                }
            }

            if ($erroJMAN != "") {
                $message = $message . $erroJMAN;
            }//."Publicações/Publicações Manuais Internacionais - Existem ARTIGOS com Título vazio<br/>";}

            if ($PRPMAN > 0) {
                $message = $message . $erroPRPMAN;
            }//."Publicações/Publicações Manuais Internacionais - Existem PEER REVIEW PROCEEDINGS com Título vazio<br/>";}

            if ($JAMAN > 0) {
                $message = $message . $erroJAMAN;
            }//."Publicações/Publicações Manuais Internacionais - Existem SUMÁRIOS com Título vazio<br/>";}

            if ($CPMAN > 0) {
                $message = $message . $erroCPMAN;
            }//."Publicações/Publicações Manuais Internacionais - Existem CONFERENCE PROCEEDINGS com Título vazio<br/>";}

            if ($SMAN > 0) {
                $message = $message . $erroSMAN;
            }//."Publicações/Publicações Manuais Internacionais - Existem Publicações com Título vazio<br/>";}

            if ($LMAN > 0) {
                $message = $message . $erroLMAN;
            }//."Publicações/Publicações Manuais Internacionais - Existem LIVROS com Título vazio<br/>";}

            if ($CLMAN > 0) {
                $message = $message . $erroCLMAN;
            }//."Publicações/Publicações Manuais Internacionais - Existem CAPÍTULOS DE LIVRO com Título vazio<br/>";}	

            if ($OAMAN > 0) {
                $message = $message . $erroOAMAN;
            }//."Publicações/Publicações Manuais Internacionais - Existem OUTROS SUMÁRIOS com Título vazio<br/>";}



            return $message;
        }

        function validaPublicacoesNacional() {
            global $questionario;
            global $anoactual;


            $erroCPNMAN = "";



            $erroJNMAN = "";

            $erroPRPNMAN = "";

            $erroJANMAN = "";

            $erroSNMAN = "";

            $erroLNMAN = "";

            $erroCLNMAN = "";

            $erroOANMAN = "";

            $CPNMAN = 0;


            $JNMAN = 0;

            $PRPNMAN = 0;

            $JANMAN = 0;

            $SNMAN = 0;

            $LNMAN = 0;

            $CLNMAN = 0;

            $OANMAN = 0;

            $message = "";




            foreach ($questionario->publicacoesMANUALNacional as $i => $value) {

                $tipo = $questionario->publicacoesMANUALNacional[$i]->tipofmup;

                //echo $tipo;

                switch ($tipo) {
                    case 'CPN': { // Op√ß√£o de entrada
                            $CPNMAN++;
                            if ($questionario->publicacoesMANUALNacional[$i]->titulo == "") {
                                $erroCPNMAN = $erroCPNMAN . "Publicações/Publicações Manuais Não Inglês - CONFERENCE PROCEEDINGS (Linha " . $CPNMAN . ") - Título Vazio<br/>";
                            }
                            if ($questionario->publicacoesMANUALNacional[$i]->isbn != "") {
                                if (strlen($questionario->publicacoesMANUALNacional[$i]->isbn) != 13) {

                                    $erroCPNMAN = $erroCPNMAN . "Publicações/Publicações Manuais Não Inglês - CONFERENCE PROCEEDINGS (Linha " . $CPNMAN . ") - ISBN Deve ser composto por 13 d√≠gitos<br/>";
                                }
                            }
                            if ($questionario->publicacoesMANUALNacional[$i]->ultpagina != "" && $questionario->publicacoesMANUALNacional[$i]->primpagina != "") {
                                if (intval($questionario->publicacoesMANUALNacional[$i]->ultpagina) - intval($questionario->publicacoesMANUALNacional[$i]->primpagina) < 0) {
                                    $erroCPNMAN = $erroCPNMAN . "Publicações/Publicações Manuais Não Inglês - CONFERENCE PROCEEDINGS (Linha " . $CPNMAN . ") - Páginas Erradas<br/>";
                                }
                                if (intval($questionario->publicacoesMANUALNacional[$i]->ultpagina) - intval($questionario->publicacoesMANUALNacional[$i]->primpagina) < 2) {
                                    $erroCPNMAN = $erroCPNMAN . "Publicações/Publicações Manuais Não Inglês - CONFERENCE PROCEEDINGS (Linha " . $CPNMAN . ") - Nº de páginas deve ser superior ou igual a 2<br/>";
                                }
                            }
                            if (!checkAutor($questionario->publicacoesMANUALNacional[$i]->autores, $questionario->investigador->id)) {
                                $erroCPNMAN = $erroCPNMAN . "Publicações/Publicações Manuais Não Inglês - CONFERENCE PROCEEDINGS (Linha " . $CPNMAN . ") - Nenhum dos seus nomes científicos consta da lista de autores<br/>";
                            }
                        }
                        break;
                    case 'JN': { // Op√ß√£o de entrada
                            $JNMAN++;
                            if ($questionario->publicacoesMANUALNacional[$i]->titulo == "") {
                                $erroJNMAN = $erroJNMAN . "Publicações/Publicações Manuais Não Inglês - ARTIGOS (Linha " . $JNMAN . ") - Título Vazio<br/>";
                            }
                            if ($questionario->publicacoesMANUALNacional[$i]->issn != "") {
                                if (strpos($questionario->publicacoesMANUALNacional[$i]->issn, "-") != 4 || strlen($questionario->publicacoesMANUALNacional[$i]->issn) != 9) {
                                    $erroJNMAN = $erroJNMAN . "Publicações/Publicações Manuais Não Inglês - ARTIGOS (Linha " . $JNMAN . ") - ISSN Errado<br/>";
                                }
                            }
                            if ($questionario->publicacoesMANUALNacional[$i]->ultpagina != "" && $questionario->publicacoesMANUALNacional[$i]->primpagina != "") {
                                if (intval($questionario->publicacoesMANUALNacional[$i]->ultpagina) - intval($questionario->publicacoesMANUALNacional[$i]->primpagina) < 0) {
                                    $erroJNMAN = $erroJNMAN . "Publicações/Publicações Manuais Não Inglês - ARTIGOS (Linha " . $JNMAN . ") - Páginas Erradas<br/>";
                                }
                                if (intval($questionario->publicacoesMANUALNacional[$i]->ultpagina) - intval($questionario->publicacoesMANUALNacional[$i]->primpagina) < 2) {
                                    $erroJNMAN = $erroJNMAN . "Publicações/Publicações Manuais Não Inglês - ARTIGOS (Linha " . $JNMAN . ") - Nº de páginas deve superior ou igual a 2<br/>";
                                }
                            }
                            if (!checkAutor($questionario->publicacoesMANUALNacional[$i]->autores, $questionario->investigador->id)) {
                                $erroJNMAN = $erroJNMAN . "Publicações/Publicações Manuais Não Inglês - ARTIGOS (Linha " . $JNMAN . ") - Nenhum dos seus nomes científicos consta da lista de autores<br/>";
                            }
                        }
                        break;
                    case 'PRPN': { // Op√ß√£o de entrada
                            $PRPNMAN++;
                            if ($questionario->publicacoesMANUALNacional[$i]->titulo == "") {
                                $erroPRPNMAN = $erroPRPNMAN . "Publicações/Publicações Manuais Não Inglês - PEER REVIEW PROCEEDINGS (Linha " . $PRPNMAN . ") - Título Vazio<br/>";
                            }
                            if ($questionario->publicacoesMANUALNacional[$i]->issn != "") {
                                if (strpos($questionario->publicacoesMANUALNacional[$i]->issn, "-") != 4 || strlen($questionario->publicacoesMANUALNacional[$i]->issn) != 9) {
                                    $erroPRPNMAN = $erroPRPNMAN . "Publicações/Publicações Manuais Não Inglês - PEER REVIEW PROCEEDINGS (Linha " . $PRPNMAN . ") - ISSN Errado<br/>";
                                }
                            }
                            if ($questionario->publicacoesMANUALNacional[$i]->ultpagina != "" && $questionario->publicacoesMANUALNacional[$i]->primpagina != "") {
                                if (intval($questionario->publicacoesMANUALNacional[$i]->ultpagina) - intval($questionario->publicacoesMANUALNacional[$i]->primpagina) < 0) {
                                    $erroPRPNMAN = $erroPRPNMAN . "Publicações/Publicações Manuais Não Inglês - PEER REVIEW PROCEEDINGS (Linha " . $PRPNMAN . ") - Páginas Erradas<br/>";
                                }
                                if (intval($questionario->publicacoesMANUALNacional[$i]->ultpagina) - intval($questionario->publicacoesMANUALNacional[$i]->primpagina) < 2) {
                                    $erroPRPNMAN = $erroPRPNMAN . "Publicações/Publicações Manuais Não Inglês - PEER REVIEW PROCEEDINGS (Linha " . $PRPNMAN . ") - Nº de páginas deve ser superior ou igual a 2<br/>";
                                }
                            }
                            if (!checkAutor($questionario->publicacoesMANUALNacional[$i]->autores, $questionario->investigador->id)) {
                                $erroPRPNMAN = $erroPRPNMAN . "Publicações/Publicações Manuais Não Inglês - PEER REVIEW PROCEEDINGS (Linha " . $PRPNMAN . ") - Nenhum dos seus nomes científicos consta da lista de autores<br/>";
                            }
                        }
                        break;
                    case 'JAN': { // Op√ß√£o de entrada
                            $JANMAN++;
                            if ($questionario->publicacoesMANUALNacional[$i]->titulo == "") {
                                $erroJANMAN = $erroJANMAN . "Publicações/Publicações Manuais Não Inglês - SUMÁRIOS (Linha " . $JANMAN . ") - Título Vazio<br/>";
                            }
                            if ($questionario->publicacoesMANUALNacional[$i]->issn != "") {
                                if (strpos($questionario->publicacoesMANUALNacional[$i]->issn, "-") != 4 || strlen($questionario->publicacoesMANUALNacional[$i]->issn) != 9) {
                                    $erroJANMAN = $erroJANMAN . "Publicações/Publicações Manuais Não Inglês - SUMÁRIOS (Linha " . $JANMAN . ") - ISSN Errado<br/>";
                                }
                            }
                            if ($questionario->publicacoesMANUALNacional[$i]->ultpagina != "" && $questionario->publicacoesMANUALNacional[$i]->primpagina != "") {
                                if (intval($questionario->publicacoesMANUALNacional[$i]->ultpagina) - intval($questionario->publicacoesMANUALNacional[$i]->primpagina) < 0) {
                                    $erroJANMAN = $erroJANMAN . "Publicações/Publicações Manuais Não Inglês - SUMÁRIOS (Linha " . $JANMAN . ") - Páginas Erradas<br/>";
                                }
                                if (intval($questionario->publicacoesMANUALNacional[$i]->ultpagina) - intval($questionario->publicacoesMANUALNacional[$i]->primpagina) >= 2) {
                                    $erroJANMAN = $erroJANMAN . "Publicações/Publicações Manuais Não Inglês - SUMÁRIOS (Linha " . $JANMAN . ") - Nº de páginas deve ser 1<br/>";
                                }
                            }
                            if (!checkAutor($questionario->publicacoesMANUALNacional[$i]->autores, $questionario->investigador->id)) {
                                $erroJANMAN = $erroJANMAN . "Publicações/Publicações Manuais Não Inglês - SUMÁRIOS (Linha " . $JANMAN . ") - Nenhum dos seus nomes científicos consta da lista de autores<br/>";
                            }
                        }
                        break;
                    case 'SN': { // Op√ß√£o de entrada
                            if ($questionario->publicacoesMANUALNacional[$i]->titulo == "") {
                                $erroSNMAN++;
                            }
                        }
                        break;
                    case 'LN': { // Op√ß√£o de entrada
                            $LNMAN++;
                            if ($questionario->publicacoesMANUALNacional[$i]->titulo == "") {
                                $erroLNMAN = $erroLNMAN . "Publicações/Publicações Manuais Não Inglês - LIVROS (Linha " . $LNMAN . ") - Título Vazio<br/>";
                            }
                            if ($questionario->publicacoesMANUALNacional[$i]->isbn != "") {
                                if (strlen($questionario->publicacoesMANUALNacional[$i]->isbn) != 13) {
                                    $erroLNMAN = $erroLNMAN . "Publicações/Publicações Manuais Não Inglês - LIVROS (Linha " . $LNMAN . ") - ISBN Deve ser composto por 13 d√≠gitos<br/>";
                                }
                            }
                            if ($questionario->publicacoesMANUALNacional[$i]->ultpagina != "" && $questionario->publicacoesMANUALNacional[$i]->primpagina != "") {
                                if (intval($questionario->publicacoesMANUALNacional[$i]->ultpagina) - intval($questionario->publicacoesMANUALNacional[$i]->primpagina) < 0) {
                                    $erroLNMAN = $erroLNMAN . "Publicações/Publicações Manuais Não Inglês - LIVROS (Linha " . $LNMAN . ") - Páginas Erradas<br/>";
                                }
                            }
                            if (!checkAutor($questionario->publicacoesMANUALNacional[$i]->autores, $questionario->investigador->id)) {
                                $erroLNMAN = $erroLNMAN . "Publicações/Publicações Manuais Não Inglês - LIVROS (Linha " . $LNMAN . ") - Nenhum dos seus nomes científicos consta da lista de autores<br/>";
                            }
                        }
                        break;
                    case 'CLN': { // Op√ß√£o de entrada
                            $CLNMAN++;
                            if ($questionario->publicacoesMANUALNacional[$i]->titulo == "") {
                                $erroCLNMAN = $erroCLNMAN . "Publicações/Publicações Manuais Não Inglês - CAPÍTULOS DE LIVROS (Linha " . $CLNMAN . ") - Título Vazio<br/>";
                            }
                            if ($questionario->publicacoesMANUALNacional[$i]->isbn != "") {
                                if (strlen($questionario->publicacoesMANUALNacional[$i]->isbn) != 13) {
                                    $erroCLNMAN = $erroCLNMAN . "Publicações/Publicações Manuais Não Inglês - CAPÍTULOS DE LIVROS (Linha " . $CLNMAN . ") - ISBN Deve ser composto por 13 d√≠gitos<br/>";
                                }
                            }
                            if ($questionario->publicacoesMANUALNacional[$i]->ultpagina != "" && $questionario->publicacoesMANUALNacional[$i]->primpagina != "") {
                                if (intval($questionario->publicacoesMANUALNacional[$i]->ultpagina) - intval($questionario->publicacoesMANUALNacional[$i]->primpagina) < 0) {
                                    $erroCLNMAN = $erroCLNMAN . "Publicações/Publicações Manuais Não Inglês - CAPÍTULOS DE LIVROS (Linha " . $CLNMAN . ") - Páginas Erradas<br/>";
                                }
                            }
                            if (!checkAutor($questionario->publicacoesMANUALNacional[$i]->autores, $questionario->investigador->id)) {
                                $erroCLNMAN = $erroCLNMAN . "Publicações/Publicações Manuais Não Inglês - CAPÍTULOS DE LIVROS (Linha " . $CLNMAN . ") - Nenhum dos seus nomes científicos consta da lista de autores<br/>";
                            }
                        }
                        break;
                    case 'OAN': { // Op√ß√£o de entrada
                            $OANMAN++;
                            if ($questionario->publicacoesMANUALNacional[$i]->titulo == "") {
                                $erroOANMAN = $erroOANMAN . "Publicações/Publicações Manuais Não Inglês - OUTROS SUMÁRIOS (Linha " . $OANMAN . ") - Título Vazio<br/>";
                            }
                            if ($questionario->publicacoesMANUALNacional[$i]->isbn != "") {
                                if (strlen($questionario->publicacoesMANUALNacional[$i]->isbn) != 13) {
                                    $erroOANMAN = $erroOANMAN . "Publicações/Publicações Manuais Não Inglês - OUTROS SUMÁRIOS (Linha " . $OANMAN . ") - ISBN Deve ser composto por 13 d√≠gitos<br/>";
                                }
                            }
                            if ($questionario->publicacoesMANUALNacional[$i]->ultpagina != "" && $questionario->publicacoesMANUALNacional[$i]->primpagina != "") {
                                if (intval($questionario->publicacoesMANUALNacional[$i]->ultpagina) - intval($questionario->publicacoesMANUALNacional[$i]->primpagina) < 0) {
                                    $erroOANMAN = $erroOANMAN . "Publicações/Publicações Não Inglês - OUTROS SUMÁRIOS (Linha " . $OANMAN . ") - Páginas Erradas<br/>";
                                }
                                if (intval($questionario->publicacoesMANUALNacional[$i]->ultpagina) - intval($questionario->publicacoesMANUALNacional[$i]->primpagina) >= 2) {
                                    $erroOANMAN = $erroOANMAN . "Publicações/Publicações Manuais Não Inglês - OUTROS SUMÁRIOS (Linha " . $OANMAN . ") - Nº de páginas deve ser 1<br/>";
                                }
                            }
                            if (!checkAutor($questionario->publicacoesMANUALNacional[$i]->autores, $questionario->investigador->id)) {
                                $erroOANMAN = $erroOANMAN . "Publicações/Publicações Manuais Não Inglês - OUTROS SUMÁRIOS (Linha " . $OANMAN . ") - Nenhum dos seus nomes científicos consta da lista de autores<br/>";
                            }
                        }
                        break;
                }
            }


            if ($CPNMAN > 0) {
                $message = $message . $erroCPNMAN;
            }//."Publicações/Publicações Manuais Nacionais - Existem  CONFERENCE PROCEEDINGS com Título vazio<br/>";}

            if ($JNMAN > 0) {
                $message = $message . $erroJNMAN;
            }//."Publicações/Publicações Manuais Nacionais - Existem ARTIGOS com Título vazio<br/>";}

            if ($PRPNMAN > 0) {
                $message = $message . $erroPRPNMAN;
            }//."Publicações/Publicações Manuais Nacionais - Existem PEER REVIEW PROCEEDINGS com Título vazio<br/>";}

            if ($JANMAN > 0) {
                $message = $message . $erroJANMAN;
            }//."Publicações/Publicações Manuais Nacionais - Existem SUMÁRIOS com Título vazio<br/>";}

            if ($SNMAN > 0) {
                $message = $message . $erroSNMAN;
            }//."Publicações/Publicações Manuais Nacionais - Existem Publicações com Título vazio<br/>";}

            if ($LNMAN > 0) {
                $message = $message . $erroLNMAN;
            }//."Publicações/Publicações Manuais Nacionais - Existem LIVROS com Título vazio<br/>";}

            if ($CLNMAN > 0) {
                $message = $message . $erroCLNMAN;
            }//."Publicações/Publicações Manuais Nacionais - Existem CAPÍTULOS DE LIVRO com título vazio<br/>";}

            if ($OANMAN > 0) {
                $message = $message . $erroOANMAN;
            }//."Publicações/Publicações Manuais Nacionais - Existem OUTROS SUMÁRIOS com título vazio<br/>";}







            return $message;
        }

        function validaPublicacoesISI() {
            global $questionario;
            global $anoactual;
            $erroCPNISI = "";
            $erroJNISI = "";
            $erroPRPNISI = "";
            $erroJANISI = "";
            $erroSNISI = "";
            $erroLNISI = "";
            $erroCLNISI = "";
            $erroOANISI = "";
            $CPNISI = 0;
            $JNISI = 0;
            $PRPNISI = 0;
            $JANISI = 0;
            $SNISI = 0;
            $LNISI = 0;
            $CLNISI = 0;
            $OANISI = 0;

            $message = "";

            foreach ($questionario->publicacoesISI as $i => $value) {

                $tipo = $questionario->publicacoesISI[$i]->tipofmup;

                //echo "PUB TIPO:".$tipo." Autores".$questionario->publicacoesISI[$i]->autores."<br>";

                switch ($tipo) {
                    case 'CP': { // Op√ß√£o de entrada
                            $CPNISI++;
                            if (!checkAutor($questionario->publicacoesISI[$i]->autores, $questionario->investigador->id)) {
                                $erroCPNISI = $erroCPNISI . "Publicações/Publicações ISI - CONFERENCE PROCEEDINGS (Linha " . $CPNISI . ") - Nenhum dos seus nomes científicos consta da lista de autores<br/>";
                            }
                        }
                        break;
                    case 'J': { // Op√ß√£o de entrada
                            $JNISI++;

                            if (!checkAutor($questionario->publicacoesISI[$i]->autores, $questionario->investigador->id)) {
                                $erroJNISI = $erroJNISI . "Publicações/Publicações ISI - ARTIGOS (Linha " . $JNISI . ") - Nenhum dos seus nomes científicos consta da lista de autores<br/>";
                            }
                        }
                        break;
                    case 'PRP': { // Op√ß√£o de entrada
                            $PRPNISI++;

                            if (!checkAutor($questionario->publicacoesISI[$i]->autores, $questionario->investigador->id)) {
                                $erroPRPNISI = $erroPRPNISI . "Publicações/Publicações ISI - PEER REVIEW PROCEEDINGS (Linha " . $PRPNISI . ") - Nenhum dos seus nomes científicos consta da lista de autores<br/>";
                            }
                        }
                        break;
                    case 'JA': { // Op√ß√£o de entrada
                            $JANISI++;

                            if (!checkAutor($questionario->publicacoesISI[$i]->autores, $questionario->investigador->id)) {
                                $erroJANISI = $erroJANISI . "Publicações/Publicações ISI - SUMÁRIOS (Linha " . $JANISI . ") - Nenhum dos seus nomes científicos consta da lista de autores<br/>";
                            }
                        }
                        break;
                    case 'S': { // Op√ß√£o de entrada
                        }
                        break;
                    case 'L': { // Op√ß√£o de entrada
                            $LNISI++;

                            if (!checkAutor($questionario->publicacoesISI[$i]->autores, $questionario->investigador->id)) {
                                $erroLNISI = $erroLNISI . "Publicações/Publicações ISI - LIVROS (Linha " . $LNISI . ") - Nenhum dos seus nomes científicos consta da lista de autores<br/>";
                            }
                        }
                        break;
                    case 'CL': { // Op√ß√£o de entrada
                            $CLNISI++;

                            if (!checkAutor($questionario->publicacoesISI[$i]->autores, $questionario->investigador->id)) {
                                $erroCLNISI = $erroCLNISI . "Publicações/Publicações ISI - CAPÍTULOS DE LIVROS (Linha " . $CLNISI . ") - Nenhum dos seus nomes científicos consta da lista de autores<br/>";
                            }
                        }
                        break;
                    case 'OA': { // Op√ß√£o de entrada
                            $OANISI++;

                            if (!checkAutor($questionario->publicacoesISI[$i]->autores, $questionario->investigador->id)) {
                                $erroOANISI = $erroOANISI . "Publicações/Publicações ISI - OUTROS SUMÁRIOS (Linha " . $OANISI . ") - Nenhum dos seus nomes científicos consta da lista de autores<br/>";
                            }
                        }
                        break;
                    case 'CPN': { // Op√ß√£o de entrada
                            $CPNISI++;
                            if (!checkAutor($questionario->publicacoesISI[$i]->autores, $questionario->investigador->id)) {
                                $erroCPNISI = $erroCPNISI . "Publicações/Publicações ISI - CONFERENCE PROCEEDINGS NÃO INGLÊS(Linha " . $CPNISI . ") - Nenhum dos seus nomes científicos consta da lista de autores<br/>";
                            }
                        }
                        break;
                    case 'JN': { // Op√ß√£o de entrada
                            $JNISI++;

                            if (!checkAutor($questionario->publicacoesISI[$i]->autores, $questionario->investigador->id)) {
                                $erroJNISI = $erroJNISI . "Publicações/Publicações ISI - ARTIGOS  NÃO INGLÊS(Linha " . $JNISI . ") - Nenhum dos seus nomes científicos consta da lista de autores<br/>";
                            }
                        }
                        break;
                    case 'PRPN': { // Op√ß√£o de entrada
                            $PRPNISI++;

                            if (!checkAutor($questionario->publicacoesISI[$i]->autores, $questionario->investigador->id)) {
                                $erroPRPNISI = $erroPRPNISI . "Publicações/Publicações ISI - PEER REVIEW PROCEEDINGS NÃO INGLÊS (Linha " . $PRPNISI . ") - Nenhum dos seus nomes científicos consta da lista de autores<br/>";
                            }
                        }
                        break;
                    case 'JAN': { // Op√ß√£o de entrada
                            $JANISI++;

                            if (!checkAutor($questionario->publicacoesISI[$i]->autores, $questionario->investigador->id)) {
                                $erroJANISI = $erroJANISI . "Publicações/Publicações ISI - SUMÁRIOS  NÃO INGLÊS(Linha " . $JANISI . ") - Nenhum dos seus nomes científicos consta da lista de autores<br/>";
                            }
                        }
                        break;
                    case 'S': { // Op√ß√£o de entrada
                        }
                        break;
                    case 'LN': { // Op√ß√£o de entrada
                            $LNISI++;

                            if (!checkAutor($questionario->publicacoesISI[$i]->autores, $questionario->investigador->id)) {
                                $erroLNISI = $erroLNISI . "Publicações/Publicações ISI - LIVROS  NÃO INGLÊS(Linha " . $LNISI . ") - Nenhum dos seus nomes científicos consta da lista de autores<br/>";
                            }
                        }
                        break;
                    case 'CLN': { // Op√ß√£o de entrada
                            $CLNISI++;

                            if (!checkAutor($questionario->publicacoesISI[$i]->autores, $questionario->investigador->id)) {
                                $erroCLNISI = $erroCLNISI . "Publicações/Publicações ISI - CAPÍTULOS DE LIVROS  NÃO INGLÊS(Linha " . $CLNISI . ") - Nenhum dos seus nomes científicos consta da lista de autores<br/>";
                            }
                        }
                        break;
                    case 'OAN': { // Op√ß√£o de entrada
                            $OANISI++;

                            if (!checkAutor($questionario->publicacoesISI[$i]->autores, $questionario->investigador->id)) {
                                $erroOANISI = $erroOANISI . "Publicações/Publicações ISI - OUTROS SUMÁRIOS  NÃO INGLÊS(Linha " . $OANISI . ") - Nenhum dos seus nomes científicos consta da lista de autores<br/>";
                            }
                        }
                        break;
                }
            }

            if ($CPNISI > 0) {
                $message = $message . $erroCPNISI;
            }//."Publicações/Publicações Manuais Nacionais - Existem  CONFERENCE PROCEEDINGS com Título vazio<br/>";}
            if ($JNISI > 0) {
                $message = $message . $erroJNISI;
            }//."Publicações/Publicações Manuais Nacionais - Existem ARTIGOS com Título vazio<br/>";}
            if ($PRPNISI > 0) {
                $message = $message . $erroPRPNISI;
            }//."Publicações/Publicações Manuais Nacionais - Existem PEER REVIEW PROCEEDINGS com Título vazio<br/>";}
            if ($JANISI > 0) {
                $message = $message . $erroJANISI;
            }//."Publicações/Publicações Manuais Nacionais - Existem SUMÁRIOS com Título vazio<br/>";}
            if ($SNISI > 0) {
                $message = $message . $erroSNISI;
            }//."Publicações/Publicações Manuais Nacionais - Existem Publicações com Título vazio<br/>";}
            if ($LNISI > 0) {
                $message = $message . $erroLNISI;
            }//."Publicações/Publicações Manuais Nacionais - Existem LIVROS com Título vazio<br/>";}
            if ($CLNISI > 0) {
                $message = $message . $erroCLNISI;
            }//."Publicações/Publicações Manuais Nacionais - Existem CAPÍTULOS DE LIVRO com título vazio<br/>";}
            if ($OANISI > 0) {
                $message = $message . $erroOANISI;
            }//."Publicações/Publicações Manuais Nacionais - Existem OUTROS SUMÁRIOS com título vazio<br/>";}
            return $message;
        }

        function validaPublicacoesPUBMED() {
            global $questionario;
            global $anoactual;
            $erroCPNPUBMED = "";
            $erroJNPUBMED = "";
            $erroPRPNPUBMED = "";
            $erroJANPUBMED = "";
            $erroSNPUBMED = "";
            $erroLNPUBMED = "";
            $erroCLNPUBMED = "";
            $erroOANPUBMED = "";
            $CPNPUBMED = 0;
            $JNPUBMED = 0;
            $PRPNPUBMED = 0;
            $JANPUBMED = 0;
            $SNPUBMED = 0;
            $LNPUBMED = 0;
            $CLNPUBMED = 0;
            $OANPUBMED = 0;

            $message = "";

            foreach ($questionario->publicacoesPUBMED as $i => $value) {

                $tipo = $questionario->publicacoesPUBMED[$i]->tipofmup;

                //echo "PUB TIPO:".$tipo." Autores".$questionario->publicacoesPUBMED[$i]->autores."<br>";

                switch ($tipo) {
                    case 'CP': { // Op√ß√£o de entrada
                            $CPNPUBMED++;
                            if (!checkAutor($questionario->publicacoesPUBMED[$i]->autores, $questionario->investigador->id)) {
                                $erroCPNPUBMED = $erroCPNPUBMED . "Publicações/Publicações PUBMED - CONFERENCE PROCEEDINGS (Linha " . $CPNPUBMED . ") - Nenhum dos seus nomes científicos consta da lista de autores<br/>";
                            }
                        }
                        break;
                    case 'J': { // Op√ß√£o de entrada
                            $JNPUBMED++;

                            if (!checkAutor($questionario->publicacoesPUBMED[$i]->autores, $questionario->investigador->id)) {
                                $erroJNPUBMED = $erroJNPUBMED . "Publicações/Publicações PUBMED - ARTIGOS (Linha " . $JNPUBMED . ") - Nenhum dos seus nomes científicos consta da lista de autores<br/>";
                            }
                        }
                        break;
                    case 'PRP': { // Op√ß√£o de entrada
                            $PRPNPUBMED++;

                            if (!checkAutor($questionario->publicacoesPUBMED[$i]->autores, $questionario->investigador->id)) {
                                $erroPRPNPUBMED = $erroPRPNPUBMED . "Publicações/Publicações PUBMED - PEER REVIEW PROCEEDINGS (Linha " . $PRPNPUBMED . ") - Nenhum dos seus nomes científicos consta da lista de autores<br/>";
                            }
                        }
                        break;
                    case 'JA': { // Op√ß√£o de entrada
                            $JANPUBMED++;

                            if (!checkAutor($questionario->publicacoesPUBMED[$i]->autores, $questionario->investigador->id)) {
                                $erroJANPUBMED = $erroJANPUBMED . "Publicações/Publicações PUBMED - SUMÁRIOS (Linha " . $JANPUBMED . ") - Nenhum dos seus nomes científicos consta da lista de autores<br/>";
                            }
                        }
                        break;
                    case 'S': { // Op√ß√£o de entrada
                        }
                        break;
                    case 'L': { // Op√ß√£o de entrada
                            $LNPUBMED++;

                            if (!checkAutor($questionario->publicacoesPUBMED[$i]->autores, $questionario->investigador->id)) {
                                $erroLNPUBMED = $erroLNPUBMED . "Publicações/Publicações PUBMED - LIVROS (Linha " . $LNPUBMED . ") - Nenhum dos seus nomes científicos consta da lista de autores<br/>";
                            }
                        }
                        break;
                    case 'CL': { // Op√ß√£o de entrada
                            $CLNPUBMED++;

                            if (!checkAutor($questionario->publicacoesPUBMED[$i]->autores, $questionario->investigador->id)) {
                                $erroCLNPUBMED = $erroCLNPUBMED . "Publicações/Publicações PUBMED - CAPÍTULOS DE LIVROS (Linha " . $CLNPUBMED . ") - Nenhum dos seus nomes científicos consta da lista de autores<br/>";
                            }
                        }
                        break;
                    case 'OA': { // Op√ß√£o de entrada
                            $OANPUBMED++;

                            if (!checkAutor($questionario->publicacoesPUBMED[$i]->autores, $questionario->investigador->id)) {
                                $erroOANPUBMED = $erroOANPUBMED . "Publicações/Publicações PUBMED - OUTROS SUMÁRIOS (Linha " . $OANPUBMED . ") - Nenhum dos seus nomes científicos consta da lista de autores<br/>";
                            }
                        }
                        break;
                    case 'CP': { // Op√ß√£o de entrada
                            $CPNPUBMED++;
                            if (!checkAutor($questionario->publicacoesPUBMED[$i]->autores, $questionario->investigador->id)) {
                                $erroCPNPUBMED = $erroCPNPUBMED . "Publicações/Publicações PUBMED - CONFERENCE PROCEEDINGS (Linha " . $CPNPUBMED . ") - Nenhum dos seus nomes científicos consta da lista de autores<br/>";
                            }
                        }
                        break;
                    case 'JN': { // Op√ß√£o de entrada
                            $JNPUBMED++;

                            if (!checkAutor($questionario->publicacoesPUBMED[$i]->autores, $questionario->investigador->id)) {
                                $erroJNPUBMED = $erroJNPUBMED . "Publicações/Publicações PUBMED - ARTIGOS NÃO INGLÊS (Linha " . $JNPUBMED . ") - Nenhum dos seus nomes científicos consta da lista de autores<br/>";
                            }
                        }
                        break;
                    case 'PRPN': { // Op√ß√£o de entrada
                            $PRPNPUBMED++;

                            if (!checkAutor($questionario->publicacoesPUBMED[$i]->autores, $questionario->investigador->id)) {
                                $erroPRPNPUBMED = $erroPRPNPUBMED . "Publicações/Publicações PUBMED - PEER REVIEW PROCEEDINGS  NÃO INGLÊS (Linha " . $PRPNPUBMED . ") - Nenhum dos seus nomes científicos consta da lista de autores<br/>";
                            }
                        }
                        break;
                    case 'JAN': { // Op√ß√£o de entrada
                            $JANPUBMED++;

                            if (!checkAutor($questionario->publicacoesPUBMED[$i]->autores, $questionario->investigador->id)) {
                                $erroJANPUBMED = $erroJANPUBMED . "Publicações/Publicações PUBMED - SUMÁRIOS  NÃO INGLÊS (Linha " . $JANPUBMED . ") - Nenhum dos seus nomes científicos consta da lista de autores<br/>";
                            }
                        }
                        break;
                    case 'S': { // Op√ß√£o de entrada
                        }
                        break;
                    case 'LN': { // Op√ß√£o de entrada
                            $LNPUBMED++;

                            if (!checkAutor($questionario->publicacoesPUBMED[$i]->autores, $questionario->investigador->id)) {
                                $erroLNPUBMED = $erroLNPUBMED . "Publicações/Publicações PUBMED - LIVROS  NÃO INGLÊS (Linha " . $LNPUBMED . ") - Nenhum dos seus nomes científicos consta da lista de autores<br/>";
                            }
                        }
                        break;
                    case 'CLN': { // Op√ß√£o de entrada
                            $CLNPUBMED++;

                            if (!checkAutor($questionario->publicacoesPUBMED[$i]->autores, $questionario->investigador->id)) {
                                $erroCLNPUBMED = $erroCLNPUBMED . "Publicações/Publicações PUBMED - CAPÍTULOS DE LIVROS  NÃO INGLÊS (Linha " . $CLNPUBMED . ") - Nenhum dos seus nomes científicos consta da lista de autores<br/>";
                            }
                        }
                        break;
                    case 'OAN': { // Op√ß√£o de entrada
                            $OANPUBMED++;

                            if (!checkAutor($questionario->publicacoesPUBMED[$i]->autores, $questionario->investigador->id)) {
                                $erroOANPUBMED = $erroOANPUBMED . "Publicações/Publicações PUBMED - OUTROS SUMÁRIOS (Linha " . $OANPUBMED . ") - Nenhum dos seus nomes científicos consta da lista de autores<br/>";
                            }
                        }
                        break;
                }
            }

            if ($CPNPUBMED > 0) {
                $message = $message . $erroCPNPUBMED;
            }//."Publicações/Publicações Manuais Nacionais - Existem  CONFERENCE PROCEEDINGS com Título vazio<br/>";}
            if ($JNPUBMED > 0) {
                $message = $message . $erroJNPUBMED;
            }//."Publicações/Publicações Manuais Nacionais - Existem ARTIGOS com Título vazio<br/>";}
            if ($PRPNPUBMED > 0) {
                $message = $message . $erroPRPNPUBMED;
            }//."Publicações/Publicações Manuais Nacionais - Existem PEER REVIEW PROCEEDINGS com Título vazio<br/>";}
            if ($JANPUBMED > 0) {
                $message = $message . $erroJANPUBMED;
            }//."Publicações/Publicações Manuais Nacionais - Existem SUMÁRIOS com Título vazio<br/>";}
            if ($SNPUBMED > 0) {
                $message = $message . $erroSNPUBMED;
            }//."Publicações/Publicações Manuais Nacionais - Existem Publicações com Título vazio<br/>";}
            if ($LNPUBMED > 0) {
                $message = $message . $erroLNPUBMED;
            }//."Publicações/Publicações Manuais Nacionais - Existem LIVROS com Título vazio<br/>";}
            if ($CLNPUBMED > 0) {
                $message = $message . $erroCLNPUBMED;
            }//."Publicações/Publicações Manuais Nacionais - Existem CAPÍTULOS DE LIVRO com título vazio<br/>";}
            if ($OANPUBMED > 0) {
                $message = $message . $erroOANPUBMED;
            }//."Publicações/Publicações Manuais Nacionais - Existem OUTROS SUMÁRIOS com título vazio<br/>";}
            return $message;
        }

        function checkAutor($autores) {
            global $questionario;
            $validade = false;

            foreach ($questionario->identificacaoformal as $i => $value) {
                $validade = $validade || (checkNome($autores, $questionario->identificacaoformal[$i]->nomecientini) || checkNome($autores, $questionario->identificacaoformal[$i]->nomecientcom));
            }

            return $validade;
        }

        function checkNome($autores, $nome) {
            $caracteres = array(' ', " ", ".", '.');
            $validade = false;

            $autoreslimpo = str_replace($caracteres, "", $autores);
            $autoreslimpo = preg_replace('/\s+/', '', $autoreslimpo);
            $autoreslimpo = $autoreslimpo . ";";

            $nomelimpo = str_replace($caracteres, "", $nome);
            $nomelimpo = preg_replace('/\s+/', '', $nomelimpo);
            $nomelimpo = $nomelimpo;


            $autoresSep = explode(';', $autoreslimpo);

            foreach ($autoresSep as $a => $value) {

                if (strlen($nomelimpo) > 1 && strcmp($autoresSep[$a], $nomelimpo) == 0)
                    $validade = true;
                //echo $autoresSep[$a]."<->".$nomelimpo."=".strcmp($autoresSep[$a],$nomelimpo)."<br>";
            }



            return $validade;
        }
		
		function setCargosC(){

			global $questionario;
			
			foreach($questionario->cargosCom as $i => $value){
				
						$value->setData(
							isset($_POST['instituicao_' . $i]) ? $_POST['instituicao_' . $i] : "",
							isset($_POST['orgao_' . $i]) ? $_POST['orgao_' . $i] : "",
							isset($_POST['cargo_' . $i]) ? $_POST['cargo_' . $i] : "",
							isset($_POST['cargodatainicio_' . $i]) ? $_POST['cargodatainicio_' . $i] : "",
							isset($_POST['cargodatafim_' . $i]) ? $_POST['cargodatafim_' . $i] : ""
						);
				
			
			}
			
			$_SESSION['questionario']=serialize($questionario);

		}

		

        function checkDataLacrar() {
            $lac_date = "2014-01-01";
            $todays_date = date("Y-m-d");
            //echo $todays_date." ---- ".$lac_date;
            $today = strtotime($todays_date);
            $lacrar_date = strtotime($lac_date);
            if ($today > $lacrar_date) {
                return true;
            } else {
                return false;
            }
        }
    ?>