<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt" lang="pt">

<head>


<link rel="stylesheet" type="text/css" href="../styles/style_screen2.css" media="screen" />
<link rel="stylesheet" type="text/css" href="../styles/style_print.css" media="print" />


<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<title>Levantamento Geral de Dados da FMUP</title>

</head>
<?php
session_start();

require_once '../classlib/Questionario.class.inc';

require_once '../functions/parsingISI.php';

require_once '../functions/parsingPubmed.php';

echo "<body>";

if(isset($_SESSION['questionario'])){
	$questionario=unserialize($_SESSION['questionario']);

	echo "<fieldset class='normal NOPRINT'>";
	echo "<a href='#' onclick='print();'><img src='../images/printer.png' alt=''>IMPRIMIR</a>";
	echo "<br><br>Com o Windows utilize o programa deste link para produzir o PDF
		<p><i>With Windows please use the program in this link for the production of the PDF file </i></p>
	 <a href=' http://www.pdfill.com/freewriter.html' target='_blank'>Aplicação para impressão em PDF/<i>Application for the production of the PDF file</i></a><br>";
	echo "</fieldset>";
	
	echo "<fieldset class='normal'>";
	echo "<legend>Dados de Submissão</legend>";
	if($questionario->investigador->datasubmissao!="")
		echo "Formulário LACRADO em ".$questionario->investigador->datasubmissao;
	else 
		echo "FORMULÁRIO NÃO LACRADO";
	
	echo "</fieldset>";

			
			include('../reports/investigadores.php');
		
			include('../reports/habilitacoes.php');
			
			include('../reports/acoesformacaoFrequentada.php');
		
			include('../reports/agregacoes.php');
		
			include('../reports/experienciaProfissional.php');
		
			include('../reports/cargosUP.php');
		
			include('../reports/cargosFMUP.php');

			include('../reports/comissoes.php');
			
            include('../reports/consultorias.php');
		
			include('../reports/cargosHospitais.php');
			
			include('../reports/direcoesCurso.php');
		
			include('../reports/publicacoesISI.php');
		
			include('../reports/publicacoesPubmed.php');
		
			include('../reports/publicacoesManualPatentes.php');
		
			include('../reports/publicacoesManualInternacional.php');
		
			include('../reports/publicacoesManualNacional.php');
		
			include('../reports/identificacaoFormal.php');
		
			include('../reports/colaboradores.php');
		
			include('../reports/unidadesInvestigacao.php');
		
			include('../reports/projectos.php');		
		
			include('../reports/conferencias.php');
		
			include('../reports/apresentacoes.php');
		
			include('../reports/arbitragensRevistas.php');
		
			include('../reports/premios.php');
		
			include('../reports/sc.php');
			
			include('../reports/redes.php');
		
			include('../reports/missoes.php');
		
			include('../reports/aulas.php');
		
			include('../reports/orientacoes.php');
		
			include('../reports/juris.php');
		
			include('../reports/outrasactividades.php');
		
			include('../reports/produtos.php');
		
			include('../reports/acoesformacao.php');
			
			include('../reports/observacoesfinais.php');	
                       

}


?>

</body></html>