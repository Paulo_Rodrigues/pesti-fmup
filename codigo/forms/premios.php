<?php
echo "<div id='content20' style='display: inline;'>";
echo "<p><fieldset class='normal'>\n";
echo "<legend>Prémios <a href='../helpfiles/Premios2.JPG' style='color: #FFFF00' target='_blank'>Ajuda</a> /<i> Prizes <a href='../helpfiles/Premios2.JPG' style='color: #FFFF00' target='_blank'>Help</a></i></legend>\n";

echo "<p class='ppthelp'>Na Tabela seguinte indique, para cada Prémio que lhe foi atribuído, o Tipo, o Nome e o Contexto (e.g conferência, concurso internacional). Finalmente, indique o montante atribuído em euros (mesmo que seja zero), bem como um link adequado para o anúncio oficial ou para algum documento PDF no mesmo sentido.</p>";			
echo "<p class='ppthelp'>Como utilizador da FMUPNet tem disponível uma área onde pode disponibilizar documentos na web. <a href='../helpfiles/AjudaLink.pdf' style='color: #000000' target='_blank'>Aqui</a> descrevem-se os passos necessários para o efeito.</p>";

echo "<p class='penhelp'><i>In the following table please indicate, for each prize granted, the Type, Name, and Context (e.g. conference, international contest). Finally, please indicate the total amount granted in euros (even if nill), as well as an adequate link to the official announcement or to some PDF document.</i></p>";
echo "<p class='penhelp'><i>As an FMUPNet user you have an area available where you can place documents on the web.  <a href='../helpfiles/AjudaLink.pdf' style='color: #000000' target='_blank'>Here</a>  we describe all steps needed to perform it.</i></p>";

			
echo "<table id='prem' class='box-table-b'>
    					<tr><th></th>
    					<th>Tipo<p><i>Type</i></p></th>
      					<th>Nome<p><i>Name</i></p></th>
      					<th>Contexto<p><i>Context</i></p></th>
      					<th>Montante<p><i>Amount</i></p></th>
      					<th>Link<p><i>Link</i></p></th>
						</tr>";
			    
    foreach ($questionario->premios as $i => $value){
			echo "<tr>";
		   	echo "<td><input type='image' src='../images/icon_delete.png' name='navOption' value='Apagar Premio' onclick='if(formSubmited==0){return false;};document.questionario.apagaRegPremio.value=".$questionario->premios[$i]->id.";document.questionario.operacao.value=26;' ></td>";
			echo "<td>";
			getTipoPremios($i);
			echo "</td>";	
			echo "<td><input class='inp-textAuto' type='text' name='nomepremio_".$i."' value='".$questionario->premios[$i]->nome."'></td>";
			echo "<td><input class='inp-textAuto' type='text' name='contextopremio_".$i."' value='".$questionario->premios[$i]->contexto."'></td>";
			echo "<td><input class='inp-textAuto' type='text' name='montantepremio_".$i."' onkeypress='validate(event)' value='".$questionario->premios[$i]->montante."'> €</p>\n</td>";
			echo "<td><textarea rows='2' cols='20' name='linkpremio_".$i."'>".$questionario->premios[$i]->link."</textarea></td>";
			echo "</tr>";	    	
    } 
    
    	echo "<tr><td><input input type='image' src='../images/icon_new.png' name='navOption' value='Nova Premio' onclick='if(formSubmited==0){return false;};document.questionario.operacao.value=27;'></td></tr>";
    
    echo "</table>";
    echo "<br />";


	echo "<input type='hidden' name='apagaRegPremio'/>";
	echo "</fieldset>";
	echo "</div>";
	function getTipoPremios($i) {
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_tipopremio");
	
		echo "<SELECT name='tipopremio_".$i."' >\n";
		echo "<option></option>\n";
		
		while ($row = mysql_fetch_assoc($lValues)) {	
			echo "<option value='".$row["ID"]."'".checkTipoPremio($row["ID"],$i).">".$row["DESCRICAO"]."</option>\n";
		}
		echo "</SELECT><br />\n";
		$db->disconnect();
				
	}	
				
	function checkTipoPremio($id,$i){
		global $questionario;
		if($questionario->premios[$i]->tipo==$id)
			return " SELECTED";
		else 
			return "";
	}
		
	?>