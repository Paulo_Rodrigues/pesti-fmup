<?php

echo "<div id='content16' style='display: inline;'>";
echo "<p><fieldset class='normal'>\n";
			echo "<legend>Projetos ".$anoactual." <a href='../helpfiles/Projet.JPG' style='color: #FFFF00' target='_blank'>Ajuda</a>/<i>Research Projects ".$anoactual."<a href='../helpfiles/Projet.JPG' style='color: #FFFF00' target='_blank'>Help</a></i></legend>\n";

echo "<p class='ppthelp'>Preencha, para cada Projeto em execução durante 2014, a tabela seguinte (assinale, se foi o Investigador Responsável). Indique o estado de cada projeto: se está em análise ou se já foi aprovado; neste caso, indicar um link adequado com a informação oficial do projeto.</p>";			
echo "<p class='ppthelp'>Como utilizador da FMUPNet tem disponível uma área onde pode disponibilizar documentos na web. <a href='../helpfiles/AjudaLink.pdf' style='color: #000000' target='_blank'>Aqui</a> descrevem-se os passos necessários para o efeito.</p>";

echo "<p class='penhelp'><i>Please fill the following table for each Research Project  in execution in 2014 (please check the box, if you were Principal Investigator). Please indicate the status of each project; if aproved or being analysed; in the former case, please provide a link with the projects oficial details.</i></p>";
echo "<p class='penhelp'><i>As an FMUPNet user you have an area available where you can place documents on the web.  <a href='../helpfiles/AjudaLink.pdf' style='color: #000000' target='_blank'>Here</a> we describe all steps needed to perform it.</i></p>";


			
echo "
<table id='proj' class='box-table-b'>
    <thead>
	    <tr>
	    <th><u></u></th>
		      <th><u>Tipo/Entidade<p><i>Type/Entity</i></p></u></th>
		      <th><u>Entidade Financiadora<p><i>Sponsor</i></p></u></th>
		      <th><u>Instituição de Acolhimento<p><i>Host Institution</i></p></u></th>
		      <th><u>Montante  total solicitado<p><i>Total amount requested</i></p></u></th>
		      <th><u>Montante  total aprovado<p><i>Total amount approved</i></p></u></th>
		      <th><u>Montante  atribuído à FMUP<p><i>Total amount attibuted to FMUP</i></p></u></th>
		      <th><u>É Investigador Responsável?<p><i>Are you Principal Investigator?</i></p></u></th>
		      <th><u>Código/Referência<p><i>Code/Reference</i></p></u></th>
		      <th><u>Título em INGLÊS<p><i>Title in ENGLISH</i></p></u></th>
		      <th><u>Data Início<p><i>Start Date</i></p></u></th>
		      <th><u>Data Fim<p><i>End Date</i></p></u></th>
	            <th><u>Link<p><i>Link</i></p></u></th>
	            <th><u>Estado<p><i>Status</i></p></u></th>
	    </tr>
    </thead>
<tbody>
";

    foreach ($questionario->projectos as $i => $value){
		echo "<tr>";
		echo "<td><input type='image' src='../images/icon_delete_s.png' name='navOption' value='Apagar Projecto' onclick='if(formSubmited==0){return false;};document.questionario.apagaProjecto.value=".$questionario->projectos[$i]->id.";document.questionario.operacao.value=17;' ></td>";
		echo "<td>";
		getTipoEntidade($i);
		echo "<td><input class='inp-textAuto' type='text' name='pentidadefinanciadora_".$i."' value='".$questionario->projectos[$i]->entidadefinanciadora."'></td>";
		echo "<td><input class='inp-textAuto' type='text' id='pacolhimento_".$i."' name='pacolhimento_".$i."' value='".$questionario->projectos[$i]->acolhimento."' onkeyup='mostraInstituicao(this.value,\"pacolhimento_".$i."\");'><div class='divlookup' id='divpacolhimento_".$i."'></div></td>";
		echo "<td><input class='inp-textAuto' type='text' size='5' name='pmontante_".$i."' value='".$questionario->projectos[$i]->montante."' onkeypress='validate(event);'> €</td>";
		echo "<td><input class='inp-textAuto' type='text' size='5' name='pmontantea_".$i."' value='".$questionario->projectos[$i]->montantea."' onkeypress='validate(event);'> €</td>";
		echo "<td><input class='inp-textAuto' type='text' size='5' name='pmontantefmup_".$i."' value='".$questionario->projectos[$i]->montantefmup."' onkeypress='validate(event);'> €</td>";
		echo "<td><input class='inp-textAuto' type='checkbox' name='pinvresponsavel_".$i."' value='1' ".checkInv($i)."></td>";
		echo "<td><input class='inp-textAuto' type='text' name='pcodigo_".$i."' value='".$questionario->projectos[$i]->codigo."'></td>";
		echo "<td><textarea rows='2' cols='20' name='ptitulo_".$i."'>".$questionario->projectos[$i]->titulo."</textarea></td>";
		echo "<td><input class='inp-textAuto' type='text' id='pdi_".$i."' size='11' name='pdatainicio_".$i."' value='".$questionario->projectos[$i]->datainicio."' onfocus='calendario(\"pdi_".$i."\");' onkeypress='validateCal(event);' ></td>";
		echo "<td><input class='inp-textAuto' type='text' id='pdf_".$i."' size='11' name='pdatafim_".$i."' value='".$questionario->projectos[$i]->datafim."' onfocus='calendario(\"pdf_".$i."\");' onkeypress='validateCal(event);'></td>";	
		echo "<td><textarea rows='2' cols='20' name='plink_".$i."'>".$questionario->projectos[$i]->link."</textarea></td>";
		echo "<td>".getEstadoProjeto($i)."</td>";
		
		echo "</tr>";  	
		
		
    }
    echo "</tbody><tfoot><tr><td><input input type='image' src='../images/icon_new_s.png' name='navOption' value='Novo Projecto' onclick='if(formSubmited==0){return false;};document.questionario.operacao.value=9;'></td></tr></tfoot>";
	
    echo "</table>";
    echo "<br /><p>";
    
	echo "<input type='hidden' name='apagaProjecto'/>";
	echo "</fieldset>";
	
	echo "<fieldset class='normal'>\n";
	echo "<legend>Número de projetos arbitrados por si/<i>Number of projects refereed by you</i></legend><br/>\n";
	
	//	echo "<p class='ppthelp'>Indique o(s) tipo(s) de ocupação profissional que exerce na FMUP entre os seguintes. Note que deve preencher a percentagem formal dedicada a cada um, bem como o número de meses decorrido desde que iniciou o respetivo exercício.</p>";
	
	//echo "<p class='penhelp'><i>Please indicate the type(s) of professional occupation that you have at FMUP among the following. Note that you should also fill the formal percentage devoted to each, as well as the number of months since each started.</i></p>";
	
	echo "<table id='projarb' class='box-table-a'><tr><th colspan='2'>Número de projetos arbitrados por si<p><i> Number of projects refereed by you</i></p></th></tr>";
	echo "<tr><td>Internacionais</td><td><input class='inp-textAuto' size='3' type='text' name='api' onkeypress='validate(event)' value='".$questionario->arbitragensProjetos->api."'></td>";
	echo "<tr><td>Nacionais</td><td><input class='inp-textAuto' size='3' type='text' name='apn' onkeypress='validate(event)' value='".$questionario->arbitragensProjetos->apn."'></td></tr>";
	
	
	echo "</table>";
	
	echo "</fieldset>";
	

	echo "<p><fieldset class='normal'>\n";
	echo "<legend>Ensaios Clínicos ".$anoactual." <a href='../helpfiles/Projet.JPG' style='color: #FFFF00' target='_blank'>Ajuda</a>/<i>Clinical Trials ".$anoactual." <a href='../helpfiles/Projet.JPG' style='color: #FFFF00' target='_blank'>Help</a></i></legend>\n";
	
	echo "<p class='ppthelp'>Preencha, para cada Ensaio Clínico em execução durante 2014, a tabela seguinte (assinale, se foi o Investigador Responsável). Indique o estado de cada ensaio: se está em análise ou se já foi aprovado; neste caso, indicar um link adequado com informação oficial do projeto.</p>";
	echo "<p class='ppthelp'>Como utilizador da FMUPNet tem disponível uma área onde pode disponibilizar documentos na web. <a href='../helpfiles/AjudaLink.pdf' style='color: #000000' target='_blank'>Aqui</a> descrevem-se os passos necessários para o efeito.</p>";
	
	echo "<p class='penhelp'><i>Please fill the following table for each Clinical Trial in execution in 2014 (please check the box, if you were Principal Investigator). Please indicate the status of each trial: if aproved or being analysed; in the former case, please provide a link with the projects oficial details.</i></p>";
	echo "<p class='penhelp'><i>As an FMUPNet user you have an area available where you can place documents on the web.  <a href='../helpfiles/AjudaLink.pdf' style='color: #000000' target='_blank'>Here</a> we describe all steps needed to perform it.</i></p>";
	
		
		
echo "
<table id='clintria' class='box-table-b'>
    <thead>
	    <tr>
      <th><u></u></th>
      <th><u>Tipo/Entidade<p><i>Type/Entity</i></p></u></th>
	  <th><u>Estágio<p><i>Stage</i></p></u></th>
      <th><u>Entidade Financiadora<p><i>Sponsor</i></p></u></th>
      <th><u>Instituição de Acolhimento<p><i>Host Institution</i></p></u></th>
      <th><u>Montante  total solicitado<p><i>Total amount requested</i></p></u></th>
      <th><u>Montante  total aprovado<p><i>Total amount approved</i></p></u></th>
      <th><u>Montante  atribuído à FMUP<p><i>Total amount attibuted to FMUP</i></p></u></th>
      <th><u>É Investigador Responsável?<p><i>Are you Principal Investigator?</i></p></u></th>
      <th><u>Código/Referência<p><i>Code/Reference</i></p></u></th>
      <th><u>Título em INGLÊS<p><i>Title in ENGLISH</i></p></u></th>
      <th><u>Data Início<p><i>Start Date</i></p></u></th>
      <th><u>Data Fim<p><i>End Date</i></p></u></th>
            <th><u>Link<p><i>Link</i></p></u></th>
            <th><u>Estado<p><i>Status</i></p></u></th>
	    </tr>
    </thead>
<tbody>
";
	
	foreach ($questionario->clinicaltrials as $i => $value){
		echo "<tr>";
		echo "<td><input type='image' src='../images/icon_delete_s.png' name='navOption' value='ApagarClinicalTrial' onclick='if(formSubmited==0){return false;};document.questionario.apagaCT.value=".$questionario->clinicaltrials[$i]->id.";document.questionario.operacao.value=85;' ></td>";
		echo "<td>";
		getTipoEntidadeCT($i);
		echo "</td>";
		echo "<td>";
		echo getEstagioCT($i);
		echo "</td>";
		echo "<td><input class='inp-textAuto' type='text' name='ctentidadefinanciadora_".$i."' value='".$questionario->clinicaltrials[$i]->entidadefinanciadora."'></td>";
		echo "<td><input class='inp-textAuto' type='text' id='ctacolhimento_".$i."' name='ctacolhimento_".$i."' value='".$questionario->clinicaltrials[$i]->acolhimento."' onkeyup='mostraInstituicao(this.value,\"ctacolhimento_".$i."\");'><div class='divlookup' id='divctacolhimento_".$i."'></div></td>";
		echo "<td><input class='inp-textAuto' type='text' size='5' name='ctmontante_".$i."' value='".$questionario->clinicaltrials[$i]->montante."' onkeypress='validate(event);'> €</td>";
		echo "<td><input class='inp-textAuto' type='text' size='5' name='ctmontantea_".$i."' value='".$questionario->clinicaltrials[$i]->montantea."' onkeypress='validate(event);'> €</td>";
		echo "<td><input class='inp-textAuto' type='text' size='5' name='ctmontantefmup_".$i."' value='".$questionario->clinicaltrials[$i]->montantefmup."' onkeypress='validate(event);'> €</td>";
		echo "<td><input class='inp-textAuto' type='checkbox' name='ctinvresponsavel_".$i."' value='1' ".checkInvCT($i)."></td>";
		echo "<td><input class='inp-textAuto' type='text' name='ctcodigo_".$i."' value='".$questionario->clinicaltrials[$i]->codigo."'></td>";
		echo "<td><textarea rows='2' cols='20' name='cttitulo_".$i."'>".$questionario->clinicaltrials[$i]->titulo."</textarea></td>";
		echo "<td><input class='inp-textAuto' type='text' id='ctdi_".$i."' size='11' name='ctdatainicio_".$i."' value='".$questionario->clinicaltrials[$i]->datainicio."' onfocus='calendario(\"ctdi_".$i."\");' onkeypress='validateCal(event);' ></td>";
		echo "<td><input class='inp-textAuto' type='text' id='ctdf_".$i."' size='11' name='ctdatafim_".$i."' value='".$questionario->clinicaltrials[$i]->datafim."' onfocus='calendario(\"ctdf_".$i."\");' onkeypress='validateCal(event);'></td>";
		echo "<td><textarea rows='2' cols='20' name='ctlink_".$i."'>".$questionario->clinicaltrials[$i]->link."</textarea></td>";
		echo "<td>".getEstadoCT($i)."</td>";
		echo "</tr>";
	
	
	}
	echo "</tbody><tfoot><tr><td><input input type='image' src='../images/icon_new_s.png' name='navOption' value='Novo Clinical Trial' onclick='if(formSubmited==0){return false;};document.questionario.operacao.value=86;'></td></tr></tfoot>";
	
	echo "</table>";

	echo "<input type='hidden' name='apagaCT'/>";
	echo "</fieldset>";

	echo "</div>";
	
	
	function getTipoEntidade($i) {
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_tipoentidadefinanciadora");
	
		echo "<SELECT name='pentidade_".$i."' class='inp-textAuto'>\n";
		echo "<option></option>\n";
		while ($row = mysql_fetch_assoc($lValues)) {	
			echo "<option value='".$row["ID"]."'".checkTipoEntidade($row["ID"],$i).">".$row["DESCRICAO"]."</option>\n";
		}
		echo "</SELECT>\n";
		$db->disconnect();
				
	}	
	
	function getTipoEntidadeCT($i) {
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_tipoentidadefinanciadora");
	
		echo "<SELECT name='ctentidade_".$i."' class='inp-textAuto'>\n";
		echo "<option></option>\n";
		while ($row = mysql_fetch_assoc($lValues)) {
			echo "<option value='".$row["ID"]."'".checkTipoEntidadeCT($row["ID"],$i).">".$row["DESCRICAO"]."</option>\n";
		}
		echo "</SELECT>\n";
		$db->disconnect();
	
	}
	
/*Com caixa auxiliar	function getTipoEntidade($i) {
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_tipoentidadefinanciadora");
	
		echo "<SELECT name='entidadeBox_".$i."' class='inp-textAuto' onchange='document.questionario.entidade_".$i.".value=document.questionario.entidadeBox_".$i.".selectedIndex;'>\n";
		echo "<option></option>\n";
		while ($row = mysql_fetch_assoc($lValues)) {	
			echo "<option value='".$row["ID"]."'".checkTipoEntidade($row["ID"],$i).">".$row["DESCRICAO"]."</option>\n";
		}
		echo "</SELECT>\n";
		$db->disconnect();
				
	}	*/
				
	function checkTipoEntidade($id,$i){
		global $questionario;
		if($questionario->projectos[$i]->entidade==$id)
			return " SELECTED";
		else 
			return "";
	}
	
	function checkTipoEntidadeCT($id,$i){
		global $questionario;
		if($questionario->clinicaltrials[$i]->entidade==$id)
			return " SELECTED";
		else
			return "";
	}
	
	
/*	function getUnidade($i) {
	
		$db = new Database();
		$lValues =$db->getLookupValuesUnidade("laboratoriosassociados,lista_unidadesfct ");
	
		echo "<SELECT name='unidade".$i."'  onchange='showOtherUnidade(selectedIndex);'>\n";
		echo "<option></option>\n";
		
		while ($row = mysql_fetch_assoc($lValues)) {	
			echo "<option value='".$row["ID"]."'".checkUnidade($row["ID"],$i).">".$row["DESCRICAO"]."</option>\n";
		}
		echo "</SELECT><br />\n";
		$db->disconnect();
				
	}	*/
				
	function checkInv($i){
		global $questionario;
		if($questionario->projectos[$i]->invresponsavel==1)
			return "CHECKED";
		else 
			return "";
	}
	
	function checkInvCT($i){
		global $questionario;
		if($questionario->clinicaltrials[$i]->invresponsavel==1)
			return "CHECKED";
		else
			return "";
	}
	
	function getEstadoProjeto($i) {
		global $questionario;
		$db = new Database();
		$lValues =$db->getLookupValues("lista_estadoProjetos");
		$texto="<SELECT id='pestado_".$i."' name='pestado_".$i."' >\n";
		while ($row = mysql_fetch_assoc($lValues)) {
			$texto=$texto."<option value='".$row["ID"]."'".checkEstadoProjeto($row["ID"],$i).">".$row["DESCRICAO"]."</option>\n";
		}
		$texto=$texto."</SELECT><br />\n";
		$db->disconnect();
		return $texto;
	
	}
	function checkEstadoProjeto($id,$i){
		global $questionario;
	
		if($questionario->projectos[$i]->estado==$id)
			return " SELECTED";
		else
			return "";
	}
	
	function getEstadoCT($i) {
		global $questionario;
		$db = new Database();
		$lValues =$db->getLookupValues("lista_estadoCT");
		$texto="<SELECT id='ctestado_".$i."' name='ctestado_".$i."' >\n";
		while ($row = mysql_fetch_assoc($lValues)) {
			$texto=$texto."<option value='".$row["ID"]."'".checkEstadoCT($row["ID"],$i).">".$row["DESCRICAO"]."</option>\n";
		}
		$texto=$texto."</SELECT><br />\n";
		$db->disconnect();
		return $texto;
	
	}
	function checkEstadoCT($id,$i){
		global $questionario;
	
		if($questionario->clinicaltrials[$i]->estado==$id)
			return " SELECTED";
		else
			return "";
	}
	
	function getEstagioCT($i) {
		global $questionario;
		$db = new Database();
		$lValues =$db->getLookupValues("lista_estagiosCT");
		$texto="<SELECT id='ctestagio_".$i."' name='ctestagio_".$i."' >\n";
		$texto=$texto."<option></option>\n";
		while ($row = mysql_fetch_assoc($lValues)) {
			$texto=$texto."<option value='".$row["ID"]."'".checkEstagioCT($row["ID"],$i).">".$row["DESCRICAO"]."</option>\n";
		}
		$texto=$texto."</SELECT><br />\n";
		$db->disconnect();
		return $texto;
	
	}
	function checkEstagioCT($id,$i){
		global $questionario;
	
		if($questionario->clinicaltrials[$i]->estagio==$id)
			return " SELECTED";
		else
			return "";
	}
	

?>