<?php
echo "<div id='content22' style='display: inline;'>";
echo "<fieldset class='normal'>\n";
echo "<legend>Aulas/<i>Lectures</i></legend>\n";		
echo "<table  id='aulas' class='box-table-a'>
    					<tr><th></th>
						<th>Pós-Graduação</th>
    					<th>Mestrado Integrado</th>
    					<th>1º Ciclo</th>
      					<th>2º Ciclo</th>
      					<th>3º Ciclo</th>
						</tr>";
			    
echo "<tr><td>Número de Regências em Unidades Curriculares <b> diferentes </b> (total FMUP + externas):<p><i>Number of Regências in <b> different </b> Curricular Units (total FMUP + external): </i></p></td>";
echo "<td><input class='inp-textAuto' type='text' name='regpg' maxlength='3' size='3' onkeypress='validate(event)' value='".$questionario->aulas->regpg."'></td>";
echo "<td><input class='inp-textAuto' type='text' name='regmi' maxlength='3' size='3' onkeypress='validate(event)' value='".$questionario->aulas->regmi."'></td>";
echo "<td><input class='inp-textAuto' type='text' name='regpciclo' maxlength='3' size='3' onkeypress='validate(event)' value='".$questionario->aulas->regpciclo."'></td>";
echo "<td><input class='inp-textAuto' type='text' name='regsciclo' maxlength='3' size='3' onkeypress='validate(event)' value='".$questionario->aulas->regsciclo."'></td>";
echo "<td><input class='inp-textAuto' type='text' name='regtciclo' maxlength='3' size='3' onkeypress='validate(event)' value='".$questionario->aulas->regtciclo."'></td></tr>";


echo "<tr><td>Número de Unidades Curriculares <b> diferentes </b> em que lecionou aulas teóricas (inc. Seminários), FMUP+externas:<p><i>Theoretical lectures (inc. seminars), FMUP+external:</i></p></td>";
echo "<td><input class='inp-textAuto' type='text' name='tpg' maxlength='3' size='3' onkeypress='validate(event)' value='".$questionario->aulas->tpg."'></td>";
echo "<td><input class='inp-textAuto' type='text' name='tmi' maxlength='3' size='3' onkeypress='validate(event)' value='".$questionario->aulas->tmi."'></td>";
echo "<td><input class='inp-textAuto' type='text' name='tpciclo' maxlength='3' size='3' onkeypress='validate(event)' value='".$questionario->aulas->tpciclo."'></td>";
echo "<td><input class='inp-textAuto' type='text' name='tsciclo' maxlength='3' size='3' onkeypress='validate(event)' value='".$questionario->aulas->tsciclo."'></td>";
echo "<td><input class='inp-textAuto' type='text' name='ttciclo' maxlength='3' size='3' onkeypress='validate(event)' value='".$questionario->aulas->ttciclo."'></td></tr>";


echo "<tr><td>Número de Unidades Curriculares <b> diferentes </b> em que lecionou aulas não-teóricas, FMUP+externas:<p><i>Non-theoretical lectures (inc. seminars), FMUP+external:</i></p></td>";
echo "<td><input class='inp-textAuto' type='text' name='ppg' maxlength='3' size='3' onkeypress='validate(event)' value='".$questionario->aulas->ppg."'></td>";
echo "<td><input class='inp-textAuto' type='text' name='pmi' maxlength='3' size='3' onkeypress='validate(event)' value='".$questionario->aulas->pmi."'></td>";
echo "<td><input class='inp-textAuto' type='text' name='ppciclo' maxlength='3' size='3' onkeypress='validate(event)' value='".$questionario->aulas->ppciclo."'></td>";
echo "<td><input class='inp-textAuto' type='text' name='psciclo' maxlength='3' size='3' onkeypress='validate(event)' value='".$questionario->aulas->psciclo."'></td>";
echo "<td><input class='inp-textAuto' type='text' name='ptciclo' maxlength='3' size='3' onkeypress='validate(event)' value='".$questionario->aulas->ptciclo."'></td></tr>";

echo "</table>";

echo "<br><legend>Atos Médicos<br/><i>Medical Actions</i></legend><br>";

echo "<table id='atosmedicos' class='box-table-a'>";
echo "<tr><td>Número de Consultas Médicas:<p><i>Number of Medical Appointments:</i></p></td>";
echo "<td><input class='inp-textAuto' type='text' name='ncm' maxlength='5' size='5' onkeypress='validate(event)' value='".$questionario->aulas->ncm."'></td></tr>";
echo "<tr><td>Número de Cirurgias:<p><i>Number of Surgeries:</i></p></td>";
echo "<td><input class='inp-textAuto' type='text' name='nc' maxlength='5' size='5' onkeypress='validate(event)' value='".$questionario->aulas->nc."'></td></tr>";
echo "<tr><td>Número de Exames Anátomo-patológicos:<p><i>Number of anatomical pathology examinations</i></p></td>";
echo "<td><input class='inp-textAuto' type='text' name='neap' maxlength='5' size='5' onkeypress='validate(event)' value='".$questionario->aulas->neap."'></td></tr>";
echo "<tr><td>Número de Exames Imagiológicos:<p><i>Number of imagiological examinations</i></p></td>";
echo "<td><input class='inp-textAuto' type='text' name='nei' maxlength='5' size='5' onkeypress='validate(event)' value='".$questionario->aulas->nei."'></td></tr>";
echo "<tr><td>Número de Exames Periciais:<p><i>Number of expert examinations</i></p></td>";
echo "<td><input class='inp-textAuto' type='text' name='nep' maxlength='5' size='5' onkeypress='validate(event)' value='".$questionario->aulas->nep."'></td></tr>";
echo "<tr><td>Número de Consultadoria Médica:<p><i>Number of medical consultancy</i></p></td>";
echo "<td><input class='inp-textAuto' type='text' name='ncsm' maxlength='5' size='5' onkeypress='validate(event)' value='".$questionario->aulas->ncsm."'></td></tr>";
echo "<tr><td>Número de Outros atos médicos:<p><i>Number of other medical actions</i></p></td>";
echo "<td><input class='inp-textAuto' type='text' name='noutros' maxlength='5' size='5' onkeypress='validate(event)' value='".$questionario->aulas->noutros."'></td></tr>";

echo "</table>";
echo "</fieldset>";
echo "</div>";

		
	?>