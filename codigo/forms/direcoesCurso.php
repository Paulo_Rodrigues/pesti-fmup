<?php
echo "<div id='content7' style='display: inline;'>";
echo "<p><fieldset class='normal'>\n";
echo "<legend>Direções de Curso <a href='../helpfiles/Ajuda_DirCurso.JPG' style='color: #FFFF00' target='_blank'>Ajuda</a> /<i>Course Director <a href='../helpfiles/Ajuda_DirCurso.JPG' style='color: #FFFF00' target='_blank'>Help</a></i></legend>\n";
			
echo "<p class='ppthelp'>Escreva o nome do(s) curso(s) de que é Diretor: uma lista aparece entre os cursos da FMUP para escolha.</p>";			

echo "<p class='penhelp'><i>Please write the name of the course(s) of which you are Director: a list will appear with the FMUP courses for making a choice.</i></p>";

echo "<table id='dcursos' class='box-table-b'>
		<thead>
    					<tr>
						<th></th>
      					<th><u>Data de Inicio<p><i>Start Date</i></p></u></th>
      					<th><u>Data de Fim<p><i>End Date</i></p></u></th>
      					<th><u>Curso<p><i>Course</i></p></u></th>
      					<th><u>Grau<p><i>Degree</i></p></u></th>
						<th><u>Cargo<p><i></i></p></u></th>
						</tr>
		</thead>
		<tbody>";
			    
    foreach ($questionario->dirCursos as $i => $value){
			echo "<tr>";
		   	echo "<td><input type='image' src='../images/icon_delete.png' name='navOption' value='Apagar Dir Curso' onclick='if(formSubmited==0){return false;};document.questionario.apagaRegDirCurso.value=".$questionario->dirCursos[$i]->id.";document.questionario.operacao.value=24;' ></td>";

			echo "<td><input class='inp-textAuto' type='text' id='dircursodatainicio_".$i."' name='dircursodatainicio_".$i."' size='11' value='".$questionario->dirCursos[$i]->datainicio."' onfocus='calendario(\"dircursodatainicio_".$i."\");' onkeypress='validateCal(event);'></td>";
			echo "<td><input class='inp-textAuto' type='text' id='dircursodatafim_".$i."' name='dircursodatafim_".$i."' size='11' value='".$questionario->dirCursos[$i]->datafim."' onfocus='calendario(\"dircursodatafim_".$i."\");' onkeypress='validateCal(event);'></td>";
			echo "<td><input class='inp-textAuto' type='text' id='dircursonome_".$i."' size='40' name='dircursonome_".$i."' value='".$questionario->dirCursos[$i]->nome."' onkeyup='mostraCurso(this.value,".$i.");'><div  class='divlookup' id='livesearchdircursonome_".$i."'></div></td>";
			
			
			
			echo "<td>";
			getGrauDirCursos($i);	
	    	echo "</td>";
	    	echo "<td>";
	    	getCargoDirCursos($i);
	    	echo "</td>";
	    	echo "</tr>";	    	
    } 
    echo "</tbody>";
    echo "<tfoot><tr><td><input type='image' src='../images/icon_new.png' name='navOption' value='Nova Direção' onclick='if(formSubmited==0){return false;};document.questionario.operacao.value=25;'></td></tr></tfoot>";
    
    echo "</table>";
    echo "<br />";


	echo "<input type='hidden' name='apagaRegDirCurso'/>";
	
	echo "</fieldset>";
	
	
	echo "</div>";
	
	
	function getGrauDirCursos($i) {
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_graucursos");
	
		echo "<SELECT id='dircursosgrau_".$i."' name='dircursosgrau_".$i."' >\n";
		echo "<option></option>\n";
		
		while ($row = mysql_fetch_assoc($lValues)) {	
			echo "<option value='".$row["ID"]."'".checkGrauDirCursos($row["ID"],$i).">".$row["DESCRICAO"]."</option>\n";
		}
		echo "</SELECT><br />\n";
		$db->disconnect();
				
	}	
				
	function checkGrauDirCursos($id,$i){
		global $questionario;
		if($questionario->dirCursos[$i]->grau==$id)
			return " SELECTED";
		else 
			return "";
	}
	
	function getCargoDirCursos($i) {
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_cargosDirecaoCursos");
	
		echo "<SELECT id='dircursocargo_".$i."' name='dircursocargo_".$i."' >\n";
		echo "<option></option>\n";
	
		while ($row = mysql_fetch_assoc($lValues)) {
			echo "<option value='".$row["ID"]."'".checkCargoDirCursos($row["ID"],$i).">".$row["DESCRICAO"]."</option>\n";
		}
		echo "</SELECT><br />\n";
		$db->disconnect();
	
	}
	
	function checkCargoDirCursos($id,$i){
		global $questionario;
		if($questionario->dirCursos[$i]->cargo==$id)
			return " SELECTED";
		else
			return "";
	}
	
		
	?>