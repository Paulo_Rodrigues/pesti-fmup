<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>


<head>

<meta name="description" content="" />

<meta name="keywords" content="" />

<meta name="author" content="" />

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="stylesheet" type="text/css" href="../styles/style_screen.css" media="screen" />

<title>Formulário para Levantamento Geral de Dados da FMUP 2013</title>

</head>
<body>
<form name="input" action="../functions/validateDep.php" method="post">
	<fieldset class='head'>
	 <img  src='../images/fmupUP_transparencia.png' alt=''>

	 	<h1>Relatório de Atividade científica efetuada durante o ano de 2013<br><i>Report of Scientific activity during 2013</i></h1>

    
    </fieldset>
    
    
    

<fieldset class='head'>

<table cellpadding='10'>
<tr>
<td>
<label for='nomeaaa' class='float'>Nome de Utilizador <i>(Login)</i>:</label>
   <input class='inp-text' type="text" name="login"/></td>
   <td></td>
   <td rowspan='2'><p><input class='op-button' type='image' src='../images/icon_key.png' value='Entrar' onclick='document.input.submit();'/>Entrar/<i>Login</i></p></td>
</tr>
<tr>
<td><label for='nomeaaa' class='float'>Palavra-chave <i>(Password)</i>:</label>
   <input class='inp-text' type="password" name="password" /></td><td></td>
</tr>
<tr><td><small>O login é a parte que precede o @ no email institucional</small></td></tr>
</table>
   
   
   <?php 
	
   
   if($msg=='401'){
   	echo "<br><font color='red'>Login/Password inválidos (deve usar o login e password do mail)</font></br>";
   	//echo "<br><font color='red'>O prazo para o levantamento de dados terminou.</font></br>";
   }
   
   
   
   
   ?>

   
   
 	</fieldset>   
 

 	 
   <fieldset class='head'>
   <h4>Utilize o seu login e password da FMUPNet (o mesmo do email). <br>Para qualquer esclarecimento no que respeita à autenticação poderá contactar o  <a href='http://si.med.up.pt'>Serviço de Informática.</a></h4>
   <br>
   <h4><i>Use your FMUPNet login and password. <br>For any question regarding authentication please contact: <a href='http://si.med.up.pt'>Serviço de Informática.</a></i></h4>
   
   
   </fieldset>
</form>

</body>
</html>
