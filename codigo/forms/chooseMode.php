<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt" xml:lang="pt">

<head>

<meta name="description" content=""/>

<meta name="keywords" content=""/>

<meta name="author" content=""/>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<link rel="stylesheet" type="text/css" href="../styles/style_screen2.css" media="screen"/>

<link rel="stylesheet" type="text/css" media="all" href="../styles/jsDatePick_ltr.min.css"/>

<title>Levantamento Geral de Dados da FMUP 2013</title>

</head>

<?
	ini_set('display_errors',1);
	ini_set('display_startup_errors',1);
	error_reporting(-1);
	
	require_once '../classlib/Questionario.class.inc';
	require_once '../classlib/Database.class.inc';
	session_start();
	$login=@$_SESSION['login'];
	
	if(empty($login)){
		header("Location: ../index.php?erro=404");
	}

$operacao=@$_POST['operacao'];

echo "<body>";

echo "<form name='adminform' action='chooseMode.php' method='post' enctype='multipart/form-data'>";

echo "<input type='hidden' id='operacao'  name='operacao' value='0'>";

echo "<fieldset class='head'>";

echo "<table>";
echo "<td>Está autenticado como <i><span class='redcolor'>".$login."</span></i><input class='op-button' type='image' src='../images/icon_off.png' name='navOption'  value='Sair' 
	onclick='var r=confirm(\"Tem a certeza?Are you sure?\");
	if (r==true){
		document.adminform.operacao.value=1;
		document.adminform.submit();		
	}else{
		return false;
	}'>Sair</td>";

echo "<td align='right'><h2>Levantamento Geral de Dados da FMUP 2013</h2><br/><h2><i>General Survey of FMUP 2013</i></h2></td>";
echo "</tr></table>";
echo "</fieldset>";

echo "
	<fieldset class='normal'>\n
		<table class='box-table-c'>		
				<tr>
				<th align='center'>
					<u><input type='button' style='margin-left=100px;' value='Levantamento de Dados' onclick='document.adminform.operacao.value=3;document.adminform.submit();'></u>
				</th>
				<th></th>
				<th></th>
				<th align='center'>
					<u><input type='button' style='margin-left=100px;' value='Validação/ Edição de Dados' onclick='document.adminform.operacao.value=2;document.adminform.submit();'></u>
				</th>
				<th></th>
				<th align='center'>
					<u><input type='button' style='margin-left=100px;' value='Exportação de Informação' onclick='document.adminform.operacao.value=4;document.adminform.submit();'></u>
				</th>
				<th></th>
			</tr>
		</table>
	</fieldset>";
		
verificaOperacao($operacao);

function verificaOperacao($operacao){
	global $login;
	
	switch($operacao){
		case '0': { // 
		}
		break;
		case '1': { //Logout
			header("Location: ../index.php");
		}
		break;	
		case '2': { //Edição de Informação
			header("Location: ../forms/admin/dirDep.php");
		}
		break;	
		case '3': { //Levantamento de Dados
			session_unset();
            $_SESSION = array();
            @session_destroy();
            session_start();
            $questionario = new Questionario($login);
            $_SESSION['questionario'] = serialize($questionario);
            $err = "";
            auditorialogin($login, $err, "LOGIN", "", "");			
            header("Location: questionario.php");
		}
		break;		
		case '4': { //Exportação de relatório
			header("Location: ../forms/admin/exportacao.php");
		}
		default:{ //echo "DEFAULT";			
		}
		break;
	}
}
?>

<fieldset class='footer'>
<legend></legend>
FACULDADE DE MEDICINA DA UNIVERSIDADE DO PORTO<br/>
Serviço de Informática e Departamento de Apoio à Investigação e Pós-Graduação
</fieldset>
</form>
</body></html>