<?php

require_once 'Database.class.inc';

//Tabela Missoes

class Missoes {

    var $id = "";
    var $idinv = "";
    var $motivacao = "";
    var $instituicao = "";
    var $datainicio = "";
    var $datafim = "";
    var $pais = "";
    var $ambtese = "";

    function Missoes($id, $idinv, $datainicio, $datafim, $motivacao, $instituicao, $pais, $ambtese) {
        $this->id = $id;
        $this->idinv = $idinv;
        $this->datainicio = $datainicio;
        $this->datafim = $datafim;
        $this->motivacao = $motivacao;
        $this->instituicao = $instituicao;
        $this->pais = $pais;
        $this->ambtese = $ambtese;
    }

    function setData($datainicio, $datafim, $motivacao, $instituicao, $pais, $ambtese) {
        $this->datainicio = $datainicio;
        $this->datafim = $datafim;
        $this->motivacao = $motivacao;
        $this->instituicao = $instituicao;
        $this->pais = $pais;
        $this->ambtese = $ambtese;
    }

    function saveData() {
        $db = new Database();
        $db->updateMissao($this->id, $this->idinv, $this->datainicio, $this->datafim, $this->motivacao, $this->instituicao, $this->pais, $this->ambtese);
    }

}
