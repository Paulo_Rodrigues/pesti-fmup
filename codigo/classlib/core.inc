<?php

/**
 * Configura??es da aplica??o.
 * 
 * @author Jos? Filipe Lopes Santos <jfilipe@med.up.pt>
 * @since 21-12-2011
 * @version 1.0 - data da ?lt. actualiza??o: 21-12-2011
 * @package imp_endnote
 * @subpackage configurations
 */


//***************** DB *******************

//define("DSN_SERVER","10.1.2.3");
define("DSN_SERVER","10.1.0.7");
define("DSN_USER","lgdf_2012");
define("DSN_PW","2012lgdf");
define("DSN_DB","lgdf_2014");


//**************** Paths (caminhos) e Urls *******************

////////// paths e urls principais /////////
/**
 * Url para o diret?rio principal.
 */
define("ROOT_URL","/lgdf");
/**
 * caminho para o direct?rio principal.
 */
define("ROOT_PATH",getenv("DOCUMENT_ROOT").ROOT_URL);


//////////// paths para direct?rios /////////
/**
 * Caminho para o direct?rio das configura??es.
 */
define("CONF_PATH",ROOT_PATH."conf/");
/**
 * Caminho para o direct?rio com os includes / libreries.
 */
define("LIB_PATH",ROOT_PATH."lib/");
/**
 * Caminho para o direct?rio das templates.
 */
define("TEMPLATES_PATH",ROOT_PATH."templates/");
/**
 * Caminho para o direct?rio dos ficheiros do upload.
 */
define("UPLOAD_FILES_PATH",ROOT_PATH."/formulario/pubfiles/");


////////// URLs para direct?rios /////////////
/**
 * Url para o direct?rio de estilos.
 */
define("CSS_URL",ROOT_URL."css/");
/**
 * Url para o direct?rio de imagens.
 */
define("IMAGES_URL",ROOT_URL."images/");
/**
 * Url para o direct?rio de scripts em javascript.
 */
define("JS_URL",ROOT_URL."js/");


// **************** Includes ************
//require_once 'DB.php';
//require_once 'HTML/Template/PHPLIB.php';


// ************** Base de Dados **********
/**
 * DSN de acesso ? base de dados do CI (schema CI).
 */
//define("DSN_CI","oci8://ci:sici@sbim");


//************** Datas ************

/////// datas limite ////////

/**
 * Dia limite.
 */
define("STOP_DAY",31);
/**
 * M?s limite.
 */
define("STOP_MONTH",12);
/**
 * Ano inicial.
 */
define("START_YEAR",2005);
/**
 * Ano m?nimo para datas antigas.
 */
define("START_OLD_YEAR",1980);

/////// formatos /////////////
/**
 * Formato, por default, das datas (oracle).
 */
define("FORMAT_DATE","DD-MM-YYYY");


//************ MISC ************

////////// simbolos /////////////
/**
 * S?mbolo correspondente ao campo preenchido.
 */
define("SYMBOL_NORMAL"," ");
/**
 * S?mbolo correspondente ao campo que est? por preencher.
 */
define("SYMBOL_EMPTY","*");

////////// mensagens /////////////
/**
 * Mensagem que ? mostrada, quando existem campos obrigat?rios por preencher.
 */
define("MSG_EMPTY","Os campos a * n?o est?o preenchidos e, s?o de preenchimento obrigat?rio");

/////////// outros ///////////
/**
 * N.? de registos por p?gina.
 */
define("NUM_ROWS",15);
/**
 * N.? m?ximo de caracteres de uma string.
 */
define("MAX_CHARS",70);

//////// Timeouts /////////
/**
 * Timeout, em segundos, para fechar o popup e fazer reload.
 */
define("TIMEOUT_CLOSE_AND_RELOAD",2);
/**
 * Timeout, em segundos, para fazer reload.
 */
define("TIMEOUT_RELOAD",2);

/////////// Tags ///////
/**
 * Tag de in?cio do texto a destacar.
 */
define("TAG_BEGIN_HIGHLIGHT","<span class='highlight'>");
/**
 * Tag de fim do texto a destacar.
 */
define("TAG_END_HIGHLIGHT","</span>");


//////// imagens ///////////
/**
 * Tamanho das imagens da navega??o (comprimento=altura).
 */
define("SIZE_IMAGE_NAV",13);
/**
 * Comprimento do icon para ver informa??es.
 */
define("WIDTH_ICON_VIEW",11);
/**
 * Altura do icon para ver informa??es.
 */
define("HEIGHT_ICON_VIEW",11);
/**
 * Comprimento do icon para alterar.
 */
define("WIDTH_ICON_EDIT",8);
/**
 * Altura do icon para alterar.
 */
define("HEIGHT_ICON_EDIT",14);
/**
 * Comprimento do icon para apagar.
 */
define("WIDTH_ICON_DELETE",12);
/**
 * Altura do icon para apagar.
 */
define("HEIGHT_ICON_DELETE",12);

?>