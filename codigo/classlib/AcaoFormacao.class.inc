<?php

require_once 'Database.class.inc';

//Tabela AcaoFormacao

class AcaoFormacao {

    var $id = "";
    var $idinv = "";
    var $titulo = "";
    var $horas = "";
    var $preco = "";
    var $formandos = "";
    var $nformandos = "";
    var $dataini = "";
    var $datafim = "";
    var $local = "";
    var $objectivo = "";
    var $tipo = "";

    function AcaoFormacao($id, $idinv, $titulo, $horas, $preco, $formandos, $nformandos, $dataini, $datafim, $local, $objectivo,$tipo) {
        $this->id = $id;
        $this->idinv = $idinv;
        $this->titulo = $titulo;
        $this->horas = $horas;
        $this->preco = $preco;
        $this->formandos = $formandos;
        $this->nformandos = $nformandos;
        $this->dataini = $dataini;
        $this->datafim = $datafim;
        $this->local = $local;
        $this->objectivo = $objectivo;
        $this->tipo = $tipo;
    }

    function getDataFromDB($login) {
        $db = new Database();
        $lValues = $db->getAcoesFormacao($this->login);

        if ($row = mysql_fetch_assoc($lValues)) {
            do {
                $this->id = $row['ID'];
                $this->idinv = $row['IDINV'];
                $this->titulo = $row['TITULO'];
                $this->horas = $row['HORAS'];
                $this->preco = $row['PRECO'];
                $this->formandos = $row['FORMANDOS'];
                $this->nformandos = $row['NFORMANDOS'];
                $this->dataini = $row['DATAINI'];
                $this->datafim = $row['DATAFIM'];
                $this->local = $row['LOCAL'];
                $this->objectivo = $row['OBJECTIVO'];
                $this->tipo = $row['TIPO'];
            } while ($row = mysql_fetch_assoc($lValues));
        } else {
            
        }
        $db->disconnect();
    }

    function setData($titulo, $horas, $preco, $formandos, $nformandos, $dataini, $datafim, $local, $objectivo,$tipo) {
        $this->titulo = $titulo;
        $this->horas = $horas;
        $this->preco = $preco;
        $this->formandos = $formandos;
        $this->nformandos = $nformandos;
        $this->dataini = $dataini;
        $this->datafim = $datafim;
        $this->local = $local;
        $this->objectivo = $objectivo;
        $this->tipo = $tipo;
    }

    function saveData() {
        $db = new Database();
        $db->updateAcaoFormacao($this->id, $this->idinv, $this->titulo, $this->horas, $this->preco, $this->formandos, $this->nformandos, $this->dataini, $this->datafim, $this->local, $this->objectivo,$this->tipo);
    }

}
