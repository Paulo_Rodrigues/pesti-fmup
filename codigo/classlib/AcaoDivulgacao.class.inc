<?php

require_once 'Database.class.inc';

//Tabela AcoesDivulgacao

class AcaoDivulgacao {

    var $id = "";
    var $idinv = "";
    var $titulo = "";
    var $data = "";
    var $local = "";
    var $npart = "";

    function AcaoDivulgacao($id, $idinv, $titulo, $data, $local, $npart) {
        $this->id = $id;
        $this->idinv = $idinv;
        $this->titulo = $titulo;
        $this->data = $data;
        $this->local = $local;
        $this->npart = $npart;
    }

    function getDataFromDB($login) {
        $db = new Database();
        $lValues = $db->getAcoesDivulgacao($this->login);

        if ($row = mysql_fetch_assoc($lValues)) {
            do {
                $this->id = $row['ID'];
                $this->idinv = $row['IDINV'];
                $this->titulo = $row['TITULO'];
                $this->data = $row['DATA'];
                $this->local = $row['LOCAL'];
                $this->npart = $row['NPART'];
            } while ($row = mysql_fetch_assoc($lValues));
        } else {

        }
        $db->disconnect();
    }

    function setData($titulo, $data, $local, $npart) {
        $this->titulo = $titulo;
        $this->data = $data;
        $this->local = $local;
        $this->npart = $npart;
    }

    function saveData() {
        $db = new Database();
        $db->updateAcaoDivulgacao($this->id, $this->idinv, $this->titulo, $this->data, $this->local, $this->npart);
    }

}
