<?php

require_once 'Database.class.inc';
require_once 'Questionario.class.inc';

class Departamento {

    var $id = "";
    var $iddiretor = "";
    var $nome = "";
    var $questionarios = "";
    var $ninvestigadores = "";

    function Departamento($idDep) {
        $this->id = $idDep;
        $this->ninvestigadores = $this->getDadosDepartamento($idDep);
    }

    function getDadosDepartamento($idDep) {

        $ni = 0;

        $db = new Database();
        $lValues = $db->getDadosDepFromDB($idDep);
        $this->questionarios = array();
        while ($row = mysql_fetch_assoc($lValues)) {
            $ni++;
            $this->questionarios[$row['LOGIN']] = new Questionario($row['LOGIN']);
        }

        $db->disconnect();

        return $ni;
    }

}
