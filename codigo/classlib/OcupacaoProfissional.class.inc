<?php

require_once 'Database.class.inc';

// Tabela ocupacaoprofissional

class OcupacaoProfissional {

    var $id = "";
    var $idinv = "";
    var $docenteper = "";
    var $docentemes = "";
    var $docentecat = "";
    var $docenteoutradesig = "";
    var $investigadorper = "";
    var $investigadormes = "";
    var $investigadorcat = "";
    var $investigadoroutradesig = "";
    var $investigadorbolsa = "";
    var $pcicloper = "";
    var $pciclomes = "";
    var $scicloper = "";
    var $sciclomes = "";
    var $tcicloper = "";
    var $tciclomes = "";
    var $ocicloper = "";
    var $ociclomes = "";
    var $ociclodesig = "";
    var $tecper = "";
    var $tecmes = "";
    var $outraper = "";
    var $outrames = "";
    var $outradesig = "";
    var $fmupinv = "";
    var $fmupens = "";
    var $fmupgest = "";
    var $fmupdc = "";

    function OcupacaoProfissional($idinv) {

        $this->getOcupacaoFromDB($idinv);
    }

    function getOcupacaoFromDB($idinv) {

        $db = new Database();
        $lValues = $db->getOcupacaoFromDB($idinv);

        if ($row = mysql_fetch_assoc($lValues)) {
            do {
                $this->id = $row['ID'];
                $this->idinv = $row['IDINV'];
                $this->docenteper = $row['DOCENTEPER'];
                $this->docentemes = $row['DOCENTEMES'];
                $this->docentecat = $row['DOCENTECAT'];
                $this->docenteoutradesig = $row['DOCENTEOUTRADESIG'];
                $this->investigadorper = $row['INVESTIGADORPER'];
                $this->investigadormes = $row['INVESTIGADORMES'];
                $this->investigadorcat = $row['INVESTIGADORCAT'];
                $this->investigadoroutradesig = $row['INVESTIGADOROUTRADESIG'];
                $this->investigadorbolsa = $row['INVESTIGADORBOLSA'];
                $this->pcicloper = $row['PCICLOPER'];
                $this->pciclomes = $row['PCICLOMES'];
                $this->scicloper = $row['SCICLOPER'];
                $this->sciclomes = $row['SCICLOMES'];
                $this->tcicloper = $row['TCICLOPER'];
                $this->tciclomes = $row['TCICLOMES'];
                $this->ocicloper = $row['OCICLOPER'];
                $this->ociclomes = $row['OCICLOMES'];
                $this->ociclodesig = $row['OCICLODESIG'];
                $this->tecper = $row['TECPER'];
                $this->tecmes = $row['TECMES'];
                $this->outraper = $row['OUTRAPER'];
                $this->outrames = $row['OUTRAMES'];
                $this->outradesig = $row['OUTRADESIG'];
                $this->fmupinv = $row['FMUPINV'];
                $this->fmupens = $row['FMUPENS'];
                $this->fmupgest = $row['FMUPGEST'];
                $this->fmupdc = $row['FMUPDC'];
            } while ($row = mysql_fetch_assoc($lValues));
        } else {
            die("Erro Ocupação Profissional");
        }

        $db->disconnect();
    }

    function setData($docenteper, $docentemes, $docentecat, $docenteoutradesig, $investigadorper, $investigadormes, $investigadorcat, $investigadoroutradesig, $investigadorbolsa, $pcicloper, $pciclomes, $scicloper, $sciclomes, $tcicloper, $tciclomes, $ocicloper, $ociclomes, $ociclodesig, $tecper, $tecmes, $outraper, $outrames, $outradesig, $fmupinv, $fmupens, $fmupgest, $fmupdc) {
        $this->docenteper = $docenteper;
        $this->docentemes = $docentemes;
        $this->docentecat = $docentecat;
        $this->docenteoutradesig = $docenteoutradesig;
        $this->investigadorper = $investigadorper;
        $this->investigadormes = $investigadormes;
        $this->investigadorcat = $investigadorcat;
        $this->investigadoroutradesig = $investigadoroutradesig;
        $this->investigadorbolsa = $investigadorbolsa;
        $this->pcicloper = $pcicloper;
        $this->pciclomes = $pciclomes;
        $this->scicloper = $scicloper;
        $this->sciclomes = $sciclomes;
        $this->tcicloper = $tcicloper;
        $this->tciclomes = $tciclomes;
        $this->ocicloper = $ocicloper;
        $this->ociclomes = $ociclomes;
        $this->ociclodesig = $ociclodesig;
        $this->tecper = $tecper;
        $this->tecmes = $tecmes;
        $this->outraper = $outraper;
        $this->outrames = $outrames;
        $this->outradesig = $outradesig;
        $this->fmupinv = $fmupinv;
        $this->fmupens = $fmupens;
        $this->fmupgest = $fmupgest;
        $this->fmupdc = $fmupdc;
    }

    function saveData() {
        $db = new Database();
        $db->updateOcupacao($this->id, $this->idinv, $this->docenteper, $this->docentemes, $this->docentecat, $this->docenteoutradesig, $this->investigadorper, $this->investigadormes, $this->investigadorcat, $this->investigadoroutradesig, $this->investigadorbolsa, $this->pcicloper, $this->pciclomes, $this->scicloper, $this->sciclomes, $this->tcicloper, $this->tciclomes, $this->ocicloper, $this->ociclomes, $this->ociclodesig, $this->tecper, $this->tecmes, $this->outraper, $this->outrames, $this->outradesig, $this->fmupinv, $this->fmupens, $this->fmupgest, $this->fmupdc);
    }

}
