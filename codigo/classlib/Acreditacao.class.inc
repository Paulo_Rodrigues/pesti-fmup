<?php

require_once 'Database.class.inc';

//Tabela acreditacoes

class Acreditacao {

    var $id = "";
    var $idinv = "";
    var $instituicao = "";
    var $tipo = "";
    var $data = "";

    function Acreditacao($id, $idinv, $instituicao, $tipo, $data) {
        $this->id = $id;
        $this->idinv = $idinv;
        $this->instituicao = $instituicao;
        $this->tipo = $tipo;
        $this->data = $data;
    }

    function setData($instituicao, $tipo, $data) {
        $this->instituicao = $instituicao;
        $this->tipo = $tipo;
        $this->data = $data;
    }

    function saveData() {
        $db = new Database();
        $db->updateAcreditacao($this->id, $this->idinv, $this->instituicao, $this->tipo, $this->data);
    }

}
