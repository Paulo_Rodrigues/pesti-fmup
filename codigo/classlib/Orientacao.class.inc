<?php

require_once 'Database.class.inc';

//Tabela orientacoes

class Orientacao {

    var $id = "";
    var $idinv = "";
    var $tipo_orientacao = "";
    var $tipo_orientador = "";
    var $estado = "";
    var $titulo = "";
    var $nome_orientando="";

    function Orientacao($id, $idinv, $tipo_orientacao, $tipo_orientador, $estado, $titulo,$nome_orientando) {
        $this->id = $id;
        $this->idinv = $idinv;
        $this->tipo_orientacao = $tipo_orientacao;
        $this->tipo_orientador = $tipo_orientador;
        $this->estado = $estado;
        $this->titulo = $titulo;
        $this->nome_orientando = $nome_orientando;
    }

    function setData($tipo_orientacao, $tipo_orientador, $estado, $titulo,$nome_orientando) {
        $this->tipo_orientacao = $tipo_orientacao;
        $this->tipo_orientador = $tipo_orientador;
        $this->estado = $estado;
        $this->titulo = $titulo;
        $this->nome_orientando = $nome_orientando;
    }

    function saveData() {
        $db = new Database();
        $db->updateOrientacao($this->id, $this->idinv, $this->tipo_orientacao, $this->tipo_orientador, $this->estado, $this->titulo,$this->nome_orientando);
    }

}
