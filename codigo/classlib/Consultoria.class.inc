<?php
//ALTERACAO
//Paulo Rodrigues (prodrigues@med.up.pt)

require_once 'Database.class.inc';

//Tabela Consultoria

class Consultoria {

    var $id = "";
    var $idinv = "";
    var $instituicao = "";
    var $orgao = "";
    var $datainicio = "";
    var $datafim = "";

    function Consultoria($id, $idinv, $instituicao, $orgao, $datainicio, $datafim) {
        $this->id = $id;
        $this->idinv = $idinv;
        $this->instituicao = $instituicao;
        $this->orgao = $orgao;
        $this->datainicio = $datainicio;
        $this->datafim = $datafim;
    }
    
    function setData($instituicao, $orgao, $datainicio, $datafim) {
        $this->instituicao = $instituicao;
        $this->orgao = $orgao;
        $this->datainicio = $datainicio;
        $this->datafim = $datafim;
    }

    function saveData() {
        $db = new Database();
        $db->updateConsultoria($this->id, $this->idinv, $this->instituicao, $this->orgao, $this->datainicio, $this->datafim);
    }
    
    /*static function cmp_cons($a, $b)
    {        
        $al = strtotime($a->datainicio);
        $bl = strtotime($b->datainicio);
        if ($al == $bl) {
        return 0;
        }
        return ($al > $bl) ? +1 : -1;
    }*/

}
