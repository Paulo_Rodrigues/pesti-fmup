<?php

require_once 'Database.class.inc';

//Tabela DirCurso

class DirCurso {

    var $id = "";
    var $idinv = "";
    var $grau = "";
    var $nome = "";
    var $datainicio = "";
    var $datafim = "";
    var $cargo = "";

    function DirCurso($id, $idinv, $grau, $nome, $datainicio, $datafim,$cargo) {
        $this->id = $id;
        $this->idinv = $idinv;
        $this->grau = $grau;
        $this->nome = $nome;
        $this->datainicio = $datainicio;
        $this->datafim = $datafim;
        $this->cargo = $cargo;
    }

    function setData($grau, $nome, $datainicio, $datafim,$cargo) {
        $this->grau = $grau;
        $this->nome = $nome;
        $this->datainicio = $datainicio;
        $this->datafim = $datafim;
        $this->cargo = $cargo;
    }

    function saveData() {
        $db = new Database();
        $db->updateDirCurso($this->id, $this->idinv, $this->grau, $this->nome, $this->datainicio, $this->datafim,$this->cargo);
    }

}
