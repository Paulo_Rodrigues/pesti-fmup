<?php

require_once 'Database.class.inc';

//Tabela ArbitragensRevistas

class ArbitragensRevistas {

    var $id = "";
    var $idinv = "";
    var $issn = "";
    var $numero = "";
    var $tipo = "";
    var $tituloRevista = "";
    var $link;

    function ArbitragensRevistas($id, $idinv, $issn, $numero, $tipo, $tituloRevista, $link) {
        $this->id = $id;
        $this->idinv = $idinv;
        $this->issn = $issn;
        $this->numero = $numero;
        $this->tipo = $tipo;
        $this->tituloRevista = $tituloRevista;
        $this->link = $link;
    }

    function setData($issn, $numero, $tipo, $tituloRevista, $link) {
        $this->issn = $issn;
        $this->numero = $numero;
        $this->tipo = $tipo;
        $this->tituloRevista = $tituloRevista;
        $this->link = $link;
    }

    function saveData() {
        $db = new Database();
        $db->updateArbitragensRevistas($this->id, $this->idinv, $this->issn, $this->numero, $this->tipo, $this->tituloRevista, $this->link);
    }

}
