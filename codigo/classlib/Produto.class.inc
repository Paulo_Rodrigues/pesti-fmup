<?php

require_once 'Database.class.inc';

//Tabela Produtos

class Produto {

    var $id = "";
    var $idinv = "";
    var $tipop = "";
    var $valorp = "";

    function Produto($id, $idinv, $tipop, $valorp) {
        $this->id = $id;
        $this->idinv = $idinv;
        $this->tipop = $tipop;
        $this->valorp = $valorp;
    }  

    function setData($tipop, $valorp) {
        $this->tipop = $tipop;
        $this->valorp = $valorp;
    }

    function saveData() {
        $db = new Database();
        $db->updateProduto($this->id, $this->idinv, $this->tipop, $this->valorp);
    }

}
