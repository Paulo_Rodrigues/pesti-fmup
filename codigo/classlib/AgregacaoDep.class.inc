<?php

require_once 'Database.class.inc';

class AgregacaoDep {

    var $id = "";
    var $idinv = "";
    var $agregacao = "";
    var $unanimidade = "";
    var $dataprevista = "";

    function AgregacaoDep($id, $idinv, $agregacao, $unanimidade, $dataprevista) {
        $this->id = $id;
        $this->idinv = $idinv;
        $this->agregacao = $agregacao;
        $this->unanimidade = $unanimidade;
        $this->dataprevista = $dataprevista;
    }

}
