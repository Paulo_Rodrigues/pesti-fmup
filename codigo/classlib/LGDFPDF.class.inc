<?php

class LGDFPDF extends FPDF {

    function LGDFPDF() {
        $this->SetLeftMargin(20);
        $this->SetRightMargin(20);
        $this->AliasNbPages();
        $this->AddPage();
        $this->SetFont('Times', '', 12);
    }

//Page header
    function Header() {
        global $user;
        //Logo
        //$this->Image('../images/fmupUP_transparencia3.png',20,15,33);
        //Line break
        $this->Ln(20);
        //Arial bold 15
        $this->SetFont('Arial', 'B', 15);
        //Move to the right
        $this->Cell(80);
        //Title
        $this->Cell(30, 10, iconv('UTF-8', 'windows-1252', 'Projecto Op√ß√£o - 6¬∫ Ano Mestrado Integrado em Medicina da FMUP'), 0, 0, 'C');
        //Line break
        //Line break
        $this->Ln(20);
        //Line break
    }

//Page footer
    function Footer() {
        //Position at 1.5 cm from bottom
        $this->SetY(-15);
        //Arial italic 8
        $this->SetFont('Arial', 'I', 8);
        //Page number
        //$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    }

}

?>