<?php

require_once 'Database.class.inc';

//Tabela Publicacao

class Publicacao {

    var $id = "";
    var $idinv = "";
    var $tipo = "";
    var $nomepublicacao = "";
    var $issn = "";
    var $issn_linking = "";
    var $issn_electronic = "";
    var $isbn = "";
    var $titulo = "";
    var $autores = "";
    var $volume = "";
    var $issue = "";
    var $ar = "";
    var $colecao = "";
    var $primpagina = "";
    var $ultpagina = "";
    var $ifactor = "";
    var $citacoes = "";
    var $npatente = "";
    var $datapatente = "";
    var $ipc = "";
    var $editora = "";
    var $conftitle = "";
    var $language = "";
    var $editor = "";
    var $tipofmup = "";
    var $link = "";
    var $estado = "";
    var $ano = "";

    function Publicacao($id, $idinv, $tipo, $nomepublicacao, $issn, $issn_linking, $issn_electronic, $isbn, $titulo, $autores, $volume, $issue, $ar, $colecao, $primpagina, $ultpagina, $ifactor, $citacoes, $npatente, $datapatente, $ipc, $editora, $conftitle, $language, $editor, $tipofmup, $link, $estado,$ano) {
        $this->id = $id;
        $this->idinv = $idinv;
        $this->tipo = $tipo;
        $this->nomepublicacao = $nomepublicacao;
        $this->issn = $issn;
        $this->issn_linking = $issn_linking;
        $this->issn_electronic = $issn_electronic;
        $this->isbn = $isbn;
        $this->titulo = $titulo;
        $this->autores = $autores;
        $this->volume = $volume;
        $this->issue = $issue;
        $this->ar = $ar;
        $this->colecao = $colecao;
        $this->primpagina = $primpagina;
        $this->ultpagina = $ultpagina;
        $this->ifactor = $ifactor;
        $this->citacoes = $citacoes;
        $this->npatente = $npatente;
        $this->datapatente = $datapatente;
        $this->ipc = $ipc;
        $this->editora = $editora;
        $this->conftitle = $conftitle;
        $this->language = $language;
        $this->editor = $editor;
        $this->tipofmup = $tipofmup;
        $this->link = $link;
        $this->estado = $estado;
        $this->ano = $ano;
    }

    function setData($tipo, $nomepublicacao, $issn, $issn_linking, $issn_electronic, $isbn, $titulo, $autores, $volume, $issue, $ar, $colecao, $primpagina, $ultpagina, $ifactor, $citacoes, $npatente, $datapatente, $ipc, $editora, $conftitle, $language, $editor, $tipofmup, $link, $estado,$ano) {
        $this->tipo = $tipo;
        $this->nomepublicacao = $nomepublicacao;
        $this->issn = $issn;
        $this->issn_linking = $issn_linking;
        $this->issn_electronic = $issn_electronic;
        $this->isbn = $isbn;
        $this->titulo = $titulo;
        $this->autores = $autores;
        $this->volume = $volume;
        $this->issue = $issue;
        $this->ar = $ar;
        $this->colecao = $colecao;
        $this->primpagina = $primpagina;
        $this->ultpagina = $ultpagina;
        $this->ifactor = $ifactor;
        $this->citacoes = $citacoes;
        $this->npatente = $npatente;
        $this->datapatente = $datapatente;
        $this->ipc = $ipc;
        $this->editora = $editora;
        $this->conftitle = $conftitle;
        $this->language = $language;
        $this->editor = $editor;
        $this->tipofmup = $tipofmup;
        $this->link = $link;
        $this->estado = $estado;
        $this->ano = $ano;
    }

    function saveData() {
        $db = new Database();
        $db->updatePublicacaoMANUAL($this->id, $this->idinv, $this->tipo, $this->nomepublicacao, $this->issn, $this->issn_linking, $this->issn_electronic, $this->isbn, $this->titulo, $this->autores, $this->volume, $this->issue, $this->ar, $this->colecao, $this->primpagina, $this->ultpagina, $this->ifactor, $this->citacoes, $this->npatente, $this->datapatente, $this->ipc, $this->editora, $this->conftitle, $this->language, $this->editor, $this->tipofmup, "0", "0", $this->link, $this->estado,$this->ano);
    }   

}
