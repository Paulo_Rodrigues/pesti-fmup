<?php

require_once 'Database.class.inc';

//Tabela Apresentacoes

class Apresentacao {

    var $id = "";
    var $idinv = "";
    var $iconfconv = "";
    var $iconfpart = "";
    var $nconfconv = "";
    var $nconfpart = "";
    var $isem = "";
    var $nsem = "";

    function Apresentacao($idinv) {

        $this->idinv = $idinv;
        $this->getDataFromDB($idinv);
    }

    function getDataFromDB($idinv) {
        $db = new Database();
        $lValues = $db->getApresentacoesFromDB($idinv);    
        if ($row = mysql_fetch_assoc($lValues)) {
            do {
                $this->id = $row['ID'];
                $this->iconfconv = $row['ICONFCONV'];
                $this->iconfpart = $row['ICONFPART'];
                $this->nconfconv = $row['NCONFCONV'];
                $this->nconfpart = $row['NCONFPART'];
                $this->isem = $row['ISEM'];
                $this->nsem = $row['NSEM'];
            } while ($row = mysql_fetch_assoc($lValues));
            $db->disconnect();
        } else {            
            $db->insertNovaApresentacao($idinv);
            $this->getDataFromDB($idinv);
        }
    }

    function setData($iconfconv, $iconfpart, $nconfconv, $nconfpart, $isem, $nsem) {
        $this->iconfconv = $iconfconv;
        $this->iconfpart = $iconfpart;
        $this->nconfconv = $nconfconv;
        $this->nconfpart = $nconfpart;
        $this->isem = $isem;
        $this->nsem = $nsem;
    }

    function saveData() {
        $db = new Database();
        $db->updateApresentacoes($this->id, $this->idinv, $this->iconfconv, $this->iconfpart, $this->nconfconv, $this->nconfpart, $this->isem, $this->nsem);
    }

}
