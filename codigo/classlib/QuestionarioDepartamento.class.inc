<?php

require_once 'Database.class.inc';
require_once 'Habilitacoes.class.inc';
require_once 'InvestigadorDEP.class.inc';
require_once 'ExperienciaProfissional.class.inc';
require_once 'OcupacaoProfissional.class.inc';
require_once 'UnidadesInvestigacao.class.inc';
require_once 'IdentificacaoFormal.class.inc';
require_once 'Colaboradores.class.inc';
require_once 'ColaboradoresInternos.class.inc';
require_once 'Projecto.class.inc';
require_once 'Cargo.class.inc';
require_once 'DirCurso.class.inc';
require_once 'Premio.class.inc';
require_once 'Conferencia.class.inc';
require_once 'SC.class.inc';
require_once 'Rede.class.inc';
require_once 'Publicacao.class.inc';
require_once 'Orientacao.class.inc';
require_once 'Juri.class.inc';
require_once 'Aula.class.inc';
require_once 'Apresentacao.class.inc';
require_once 'OutrasActividades.class.inc';
require_once 'AcaoFormacao.class.inc';
require_once 'Produto.class.inc';
require_once 'AgregacaoDep.class.inc';
require_once 'ObservacoesFinais.class.inc';
require_once 'ArbitragensProjetos.class.inc';
require_once 'ArbitragensRevistas.class.inc';
require_once 'Missoes.class.inc';
require_once 'Acreditacao.class.inc';
require_once 'AcaoDivulgacao.class.inc';

class QuestionarioDepartamento {

    var $investigadores = "";
    var $habilitacoes = "";
    var $experienciaprofissional = "";
    var $ocupacaoprofissional = "";
    var $unidadesinvestigacao = "";
    var $identificacaoformal = "";
    var $colaboradores = "";
    var $colaboradoresinternos = "";
    var $publicacoesISI = "";
    var $publicacoesPUBMED = "";
    var $publicacoesMANUALPatentes = "";
    var $publicacoesMANUALInternacional = "";
    var $publicacoesMANUALNacional = "";
    var $projectos = "";
    var $cargosFMUP = "";
    var $cargosUP = "";
    var $cargosHospitais = "";
    var $dirCursos = "";
    var $premios = "";
    var $conferencias = "";
    var $sc = "";
    var $orientacoes = "";
    var $juris = "";
    var $aulas = "";
    var $apresentacoes = "";
    var $outrasactividades = "";
    var $acoesformacao = "";
    var $produtos = "";
    var $agregacao = "";
    var $observacoesfinais = "";
    var $arbitragensProjetos = "";
    var $arbitragensRevistas = "";
    var $redes = "";
    var $missoes = "";
    var $acreditacoes = "";
    var $acoesdivulgacao = "";

    function QuestionarioDepartamento($dep) {
        //$this->investigador=new Investigador($login);


        $this->getDEPInvestigadores($dep);
        $this->getDEPHabilitacoes($dep);
        $this->getDEPAgregacoes($dep);
        $this->getDEPOcupacaoProfissional($dep);
        $this->getDEPDirCursos($dep);
        $this->getDEPPublicacoesISI($dep);
        $this->getDEPPublicacoesPUBMED($dep);
        $this->getDEPPublicacoesMANUALPatentes($dep);
        $this->getDEPPublicacoesMANUALInternacional($dep);
        $this->getDEPPublicacoesMANUALNacional($dep);
        $this->getDEPcolaborador($dep);
        $this->getDEPcolaboradorinterno($dep);
        $this->getDEPUnidade($dep);
        $this->getDEPProjectos($dep);
        $this->getDEPArbitragensProjectos($dep);
        $this->getDEPConferencias($dep);
        $this->getDEPApresentacoes($dep);
        $this->getDEPArbitragensRevistas($dep);
        $this->getDEPPremios($dep);

        $this->getDEPSC($dep);
        $this->getDEPRedes($dep);
        $this->getDEPMissoes($dep);
        $this->getDEPOrientacoes($dep);
        $this->getDEPJuris($dep);
        $this->getDEPOutrasActividades($dep);
        $this->getDEPAcoesDivulgacao($dep);
        $this->getDEPProdutos($dep);

    }

    function getDEPInvestigadores($dep) {

        $db = new Database();
        $lValues = $db->getDEPInvestigadoresFromDB($dep);
        $this->investigadores = array();
        while ($row = mysql_fetch_assoc($lValues)) {
            $this->investigadores[$row['ID']] = new InvestigadorDEP($row['ID']
                    , $row['NOME']
                    , $row['NUMMEC']
                    , $row['DATANASC']
                    , $row['LOGIN']
                    , $row['ESTADO']
                    , $row['DATASUBMISSAO']
                    , $row['NUMIDENTIFICACAO']);
        }

        $db->disconnect();
    }

    function getDEPHabilitacoes($dep) {

        $db = new Database();
        $lValues = $db->getDEPHabilitacoesFromDB($dep);
        $this->habilitacoes = array();
        $i = 0;
        while ($row = mysql_fetch_assoc($lValues)) {
            $this->habilitacoes[$i++] = new Habilitacoes($row['ID'], $row['IDINV'], $row['ANOINICIO'], $row['ANOFIM'], $row['CURSO'], $row['INSTITUICAO'], $row['GRAU'], $row['DISTINCAO'], $row['BOLSA'], $row['TITULO'], $row['PERCENTAGEM'],$row['ORIENTADOR'], $row['OINSTITUICAO'], $row['CORIENTADOR'], $row['COINSTITUICAO']);  													
        }
        $db->disconnect();
    }

    function getDEPAgregacoes($dep) {

        $db = new Database();
        $lValues = $db->getDEPAgregacoesFromDB($dep);
        $this->agregacoes = array();
        while ($row = mysql_fetch_assoc($lValues)) {
            $this->agregacoes[$row['ID']] = new AgregacaoDep($row['ID'], $row['IDINV'], $row['AGREGACAO'], $row['UNANIMIDADE'], $row['DATAPREVISTA']);
        }
        $db->disconnect();
    }

    function getDEPOcupacaoProfissional($dep) {

        $db = new Database();
        $lValues = $db->getDEPOcupacaoProfissionalFromDB($dep);
        $this->ocupacaoprofissional = array();
        $i = 0;
        while ($row = mysql_fetch_assoc($lValues)) {
            $this->ocupacaoprofissional[$i++] = new OcupacaoProfissional($row['IDINV']);
        }
        //$db->disconnect();
    }

    function getDEPArbitragensRevistas($iddep) {

        $db = new Database();
        $lValues = $db->getDEPArbitragensRevistasFromDB($iddep);
        $this->arbitragensRevistas = array();
        $i = 0;
        while ($row = mysql_fetch_assoc($lValues)) {
            $this->arbitragensRevistas[$i++] = new ArbitragensRevistas($row['ID'], $row['IDINV'], $row['ISSN'], $row['NUMERO'], $row['TIPO'], $row['TITULOREVISTA'], $row['LINK']);
        }
        $db->disconnect();
    }

    function getExperiencia($idInv) {

        $db = new Database();
        $lValues = $db->getExperienciaFromDB($idInv);
        $this->experienciaprofissional = array();
        $i = 0;
        while ($row = mysql_fetch_assoc($lValues)) {
            $this->experienciaprofissional[$i++] = new ExperienciaProfissional($row['ID'], $row['IDINV'], $row['DATAINICIO'], $row['DATAFIM'], $row['SEMTERMO'], $row['POSICAO'], $row['EMPREGADOR'], $row['DEPARTAMENTO'], $row['PERCENTAGEM']);
        }

        $db->disconnect();
    }

    function getUnidade($idInv) {

        $db = new Database();
        $lValues = $db->getUnidadeFromDB($idInv);
        $this->unidadesinvestigacao = array();
        $i = 0;
        while ($row = mysql_fetch_assoc($lValues)) {
            $this->unidadesinvestigacao[$i++] = new UnidadesInvestigacao($row['ID'], $row['IDINV'], $row['ID_UNIDADE'], $row['UNIDADE'], $row['AVA2007'], $row['PERCENTAGEM'], $row['RESPONSAVEL'], $row['AVACONF']);
        }

        $db->disconnect();
    }

    function getDEPUnidade($iddep) {

        $db = new Database();
        $lValues = $db->getDEPUnidadeFromDB($iddep);
        $this->unidadesinvestigacao = array();
        $i = 0;
        while ($row = mysql_fetch_assoc($lValues)) {
            $this->unidadesinvestigacao[$i++] = new UnidadesInvestigacao($row['ID'], $row['IDINV'], $row['ID_UNIDADE'], $row['UNIDADE'], $row['AVA2007'], $row['PERCENTAGEM'], $row['RESPONSAVEL'], $row['AVACONF']);
        }

        $db->disconnect();
    }

    function getAcoesFormacao($idInv) {

        $db = new Database();
        $lValues = $db->getAcaoFormacaoFromDB($idInv);
        $this->acoesformacao = array();
        while ($row = mysql_fetch_assoc($lValues)) {
            $this->acoesformacao[$row['ID']] = new AcaoFormacao($row['ID'], $row['IDINV'], $row['TITULO'], $row['HORAS'], $row['PRECO'], $row['FORMANDOS'], $row['NFORMANDOS'], $row['DATAINI'], $row['DATAFIM'], $row['LOCAL'], $row['OBJECTIVO']);
        }

        $db->disconnect();
    }

    function getDEPAcoesDivulgacao($iddep) {

        $db = new Database();
        $lValues = $db->getDEPAcaoDivulgacaoFromDB($iddep);
        $this->acoesdivulgacao = array();
        $i = 0;
        while ($row = mysql_fetch_assoc($lValues)) {
            $this->acoesdivulgacao[$i++] = new AcaoDivulgacao($row['ID'], $row['IDINV'], $row['TITULO'], $row['DATA'], $row['LOCAL'], $row['NPART']);
        }

        $db->disconnect();
    }

    function getAcreditacoes($idInv) {

        $db = new Database();
        $lValues = $db->getAcreditacoesFromDB($idInv);
        $this->acreditacoes = array();
        while ($row = mysql_fetch_assoc($lValues)) {
            $this->acreditacoes[$row['ID']] = new Acreditacao($row['ID'], $row['IDINV'], $row['INSTITUICAO'], $row['TIPO'], $row['DATA']);
        }

        $db->disconnect();
    }

    function getNome($idInv) {

        $db = new Database();
        $lValues = $db->getNomeFromDB($idInv);
        $this->identificacaoformal = array();
        while ($row = mysql_fetch_assoc($lValues)) {
            $this->identificacaoformal[$row['ID']] = new IdentificacaoFormal($row['ID'], $row['IDINV'], $row['NOMECIENTINI'], $row['NOMECIENTCOM']);
        }

        $db->disconnect();
    }

    function getColaborador($idInv) {

        $db = new Database();
        $lValues = $db->getColaboradoresFromDB($idInv);
        $this->colaboradores = array();
        while ($row = mysql_fetch_assoc($lValues)) {
            $this->colaboradores[$row['ID']] = new Colaboradores($row['ID'], $row['IDINV'], $row['NOMECIENT'], $row['INSTITUICAO'], $row['PAIS']);
        }

        $db->disconnect();
    }

    function getDEPColaborador($dep) {

        $db = new Database();
        $lValues = $db->getDEPColaboradoresFromDB($dep);
        $this->colaboradores = array();
        $i = 0;
        while ($row = mysql_fetch_assoc($lValues)) {
            $this->colaboradores[$i++] = new Colaboradores($row['ID'], $row['IDINV'], $row['NOMECIENT'], $row['INSTITUICAO'], $row['PAIS']);
        }

        $db->disconnect();
    }

    function getColaboradorInterno($idInv) {

        $db = new Database();
        $lValues = $db->getColaboradoresInternosFromDB($idInv);
        $this->colaboradoresinternos = array();
        while ($row = mysql_fetch_assoc($lValues)) {
            $this->colaboradoresinternos[$row['ID']] = new ColaboradoresInternos($row['ID'], $row['IDINV'], $row['NOME'], $row['NOMECIENT'], $row['DEPARTAMENTO']);
        }
        $db->disconnect();
    }

    function getDEPColaboradorInterno($dep) {

        $db = new Database();
        $lValues = $db->getDEPColaboradoresInternosdFromDB($dep);
        $this->colaboradoresinternos = array();
        $i = 0;
        while ($row = mysql_fetch_assoc($lValues)) {
            $this->colaboradoresinternos[$i++] = new ColaboradoresInternos($row['ID'], $row['IDINV'], $row['NOME'], $row['NOMECIENT'], $row['DEPARTAMENTO']);
        }

        $db->disconnect();
    }

    function getDEPProjectos($iddep) {

        $db = new Database();
        $lValues = $db->getDEPProjectosFromDB($iddep);
        $this->projectos = array();
        $i = 0;
        while ($row = mysql_fetch_assoc($lValues)) {
            $this->projectos[$i++] = new Projecto(
                    $row['ID'], $row['IDINV'], $row['ENTIDADE'], $row['MONTANTE'], $row['MONTANTEA'], $row['MONTANTEFMUP'], $row['INVRESPONSAVEL'], $row['CODIGO'], $row['TITULO'], $row['DATAINICIO'], $row['DATAFIM'], $row['ENTIDADEFINANCIADORA'], $row['ACOLHIMENTO'], $row['LINK'], $row['ESTADO']
            );
        }


        $db->disconnect();
    }

    function getDEPArbitragensProjectos($dep) {

        $db = new Database();
        $lValues = $db->getDEPArbitragensProjetosFromDB($dep);
        $this->arbitragensProjetos = array();
        $i = 0;
        while ($row = mysql_fetch_assoc($lValues)) {
            $this->arbitragensProjetos[$i++] = new ArbitragensProjetos($row['IDINV']);
        }

        //$db->disconnect();
    }

    function getDEPJuris($dep) {
        $db = new Database();
        $lValues = $db->getDEPJurisFromDB($dep);
        $this->juris = array();
        $i = 0;
        while ($row = mysql_fetch_assoc($lValues)) {
            $this->juris[$i++] = new Juri($row['IDINV']);
        }
        //$db->disconnect();
    }

    function getDEPOutrasActividades($dep) {
        $db = new Database();
        $lValues = $db->getDEPOutrasActividadesFromDB($dep);
        $this->outrasactividades = array();
        while ($row = mysql_fetch_assoc($lValues)) {
            $this->outrasactividades[$row['ID']] = new OutrasActividades($row['IDINV']);
        }
        //$db->disconnect();
    }

    function getDEPApresentacoes($dep) {

        $db = new Database();
        $lValues = $db->getDEPApresentacoesFromDB($dep);
        $this->apresentacoes = array();
        $i = 0;
        while ($row = mysql_fetch_assoc($lValues)) {
            $this->apresentacoes[$i++] = new Apresentacao($row['IDINV']);
        }

        //$db->disconnect();
    }

    function getDEPPublicacoesISI($dep) {

        $db = new Database();
        $lValues = $db->getDEPPublicacoesISIFromDB($dep);
        $this->publicacoesISI = array();
        while ($row = mysql_fetch_assoc($lValues)) {
            $this->publicacoesISI[$row['ID']] = new Publicacao(
                    $row['ID'], $row['IDINV'], $row['TIPO'], $row['NOMEPUBLICACAO'], $row['ISSN'], $row['ISSN_LINKING'], $row['ISSN_ELECTRONIC'], $row['ISBN'], $row['TITULO'], $row['AUTORES'], $row['VOLUME'], $row['ISSUE'], $row['AR'], $row['COLECAO'], $row['PRIMPAGINA'], $row['ULTPAGINA'], $row['IFACTOR'], $row['CITACOES'], $row['NPATENTE'], $row['DATAPATENTE'], $row['IPC'], $row['EDITORA'], $row['CONFTITLE'], $row['LANGUAGE'], $row['EDITOR'], $row['TIPOFMUP'], $row['LINK'], $row['ESTADO'],$row['ANO']);
        }

        $db->disconnect();
    }

    function getDEPPublicacoesPUBMED($idInv) {

        $db = new Database();
        $lValues = $db->getDEPPublicacoesPUBMEDFromDB($idInv);
        $this->publicacoesPUBMED = array();
        while ($row = mysql_fetch_assoc($lValues)) {
            $this->publicacoesPUBMED[$row['ID']] = new Publicacao(
                    $row['ID'], $row['IDINV'], $row['TIPO'], $row['NOMEPUBLICACAO'], $row['ISSN'], $row['ISSN_LINKING'], $row['ISSN_ELECTRONIC'], $row['ISBN'], $row['TITULO'], $row['AUTORES'], $row['VOLUME'], $row['ISSUE'], $row['AR'], $row['COLECAO'], $row['PRIMPAGINA'], $row['ULTPAGINA'], $row['IFACTOR'], $row['CITACOES'], $row['NPATENTE'], $row['DATAPATENTE'], $row['IPC'], $row['EDITORA'], $row['CONFTITLE'], $row['LANGUAGE'], $row['EDITOR'], $row['TIPOFMUP'], $row['LINK'], $row['ESTADO'],$row['ANO']);
        }

        $db->disconnect();
    }

    function getDEPPublicacoesMANUALPatentes($idInv) {

        $db = new Database();
        $lValues = $db->getDEPPublicacoesMANUALPatentesFromDB($idInv);
        $this->publicacoesMANUALPatentes = array();
        while ($row = mysql_fetch_assoc($lValues)) {
            $this->publicacoesMANUALPatentes[$row['ID']] = new Publicacao(
                    $row['ID'], $row['IDINV'], $row['TIPO'], $row['NOMEPUBLICACAO'], $row['ISSN'], $row['ISSN_LINKING'], $row['ISSN_ELECTRONIC'], $row['ISBN'], $row['TITULO'], $row['AUTORES'], $row['VOLUME'], $row['ISSUE'], $row['AR'], $row['COLECAO'], $row['PRIMPAGINA'], $row['ULTPAGINA'], $row['IFACTOR'], $row['CITACOES'], $row['NPATENTE'], $row['DATAPATENTE'], $row['IPC'], $row['EDITORA'], $row['CONFTITLE'], $row['LANGUAGE'], $row['EDITOR'], $row['TIPOFMUP'], $row['LINK'], $row['ESTADO'],$row['ANO']);
        }

        $db->disconnect();
    }

    function getDEPPublicacoesMANUALInternacional($idInv) {



        $db = new Database();
        $lValues = $db->getDEPPublicacoesMANUALInternacionalFromDB($idInv);
        $this->publicacoesMANUALInternacional = array();

        while ($row = mysql_fetch_assoc($lValues)) {
            $this->publicacoesMANUALInternacional[$row['ID']] = new Publicacao(
                    $row['ID'], $row['IDINV'], $row['TIPO'], $row['NOMEPUBLICACAO'], $row['ISSN'], $row['ISSN_LINKING'], $row['ISSN_ELECTRONIC'], $row['ISBN'], $row['TITULO'], $row['AUTORES'], $row['VOLUME'], $row['ISSUE'], $row['AR'], $row['COLECAO'], $row['PRIMPAGINA'], $row['ULTPAGINA'], $row['IFACTOR'], $row['CITACOES'], $row['NPATENTE'], $row['DATAPATENTE'], $row['IPC'], $row['EDITORA'], $row['CONFTITLE'], $row['LANGUAGE'], $row['EDITOR'], $row['TIPOFMUP'], $row['LINK'], $row['ESTADO'],$row['ANO']);
        }

        $db->disconnect();
    }

    function getDEPPublicacoesMANUALNacional($idInv) {

        $db = new Database();
        $lValues = $db->getDEPPublicacoesMANUALNacionalFromDB($idInv);
        $this->publicacoesMANUALNacional = array();
        while ($row = mysql_fetch_assoc($lValues)) {
            $this->publicacoesMANUALNacional[$row['ID']] = new Publicacao(
                    $row['ID'], $row['IDINV'], $row['TIPO'], $row['NOMEPUBLICACAO'], $row['ISSN'], $row['ISSN_LINKING'], $row['ISSN_ELECTRONIC'], $row['ISBN'], $row['TITULO'], $row['AUTORES'], $row['VOLUME'], $row['ISSUE'], $row['AR'], $row['COLECAO'], $row['PRIMPAGINA'], $row['ULTPAGINA'], $row['IFACTOR'], $row['CITACOES'], $row['NPATENTE'], $row['DATAPATENTE'], $row['IPC'], $row['EDITORA'], $row['CONFTITLE'], $row['LANGUAGE'], $row['EDITOR'], $row['TIPOFMUP'], $row['LINK'], $row['ESTADO'],$row['ANO']);
        }

        $db->disconnect();
    }

    function getCargosFMUP($idInv) {

        $db = new Database();
        $lValues = $db->getCargosFMUPFromDB($idInv);
        $this->cargosFMUP = array();
        while ($row = mysql_fetch_assoc($lValues)) {
            $this->cargosFMUP[$row['ID']] = new Cargo(
                    $row['ID'], $row['IDINV'], $row['INSTITUICAO'], $row['ORGAO'], $row['CARGO'], $row['DATAINICIO'], $row['DATAFIM']);
        }

        $db->disconnect();
    }

    function getCargosUP($idInv) {

        $db = new Database();
        $lValues = $db->getCargosUPFromDB($idInv);
        $this->cargosUP = array();
        while ($row = mysql_fetch_assoc($lValues)) {
            $this->cargosUP[$row['ID']] = new Cargo(
                    $row['ID'], $row['IDINV'], $row['INSTITUICAO'], $row['ORGAO'], $row['CARGO'], $row['DATAINICIO'], $row['DATAFIM']);
        }

        $db->disconnect();
    }

    function getCargosHospital($idInv) {

        $db = new Database();
        $lValues = $db->getCargosHospitalFromDB($idInv);
        $this->cargosHospitais = array();
        while ($row = mysql_fetch_assoc($lValues)) {
            $this->cargosHospitais[$row['ID']] = new Cargo(
                    $row['ID'], $row['IDINV'], $row['INSTITUICAO'], $row['ORGAO'], $row['CARGO'], $row['DATAINICIO'], $row['DATAFIM']);
        }

        $db->disconnect();
    }

    function getDEPDirCursos($dep) {

        $db = new Database();
        $lValues = $db->getDEPDirCursosFromDB($dep);
        $this->dirCursos = array();
        $i = 0;
        while ($row = mysql_fetch_assoc($lValues)) {
            $this->dirCursos[$i++] = new DirCurso(
                    $row['ID'], $row['IDINV'], $row['GRAU'], $row['NOME'], $row['DATAINICIO'], $row['DATAFIM'],$row['CARGO']);
        }

        $db->disconnect();
    }

    function getDEPPremios($iddep) {

        $db = new Database();
        $lValues = $db->getDEPPremiosFromDB($iddep);
        $this->premios = array();
        $i = 0;
        while ($row = mysql_fetch_assoc($lValues)) {
            $this->premios[$i++] = new Premio(
                    $row['ID'], $row['IDINV'], $row['PNOME'], $row['CONTEXTO'], $row['MONTANTE'], $row['TIPO'], $row['LINK']);
        }

        $db->disconnect();
    }

    function getDEPProdutos($iddep) {

        $db = new Database();
        $lValues = $db->getDEPProdutosFromDB($iddep);
        $this->produtos = array();
        $i = 0;
        while ($row = mysql_fetch_assoc($lValues)) {
            $this->produtos[$i++] = new Produto(
                    $row['ID'], $row['IDINV'], $row['TIPOP'], $row['VALORP']);
        }

        $db->disconnect();
    }

    function getDEPOrientacoes($idInv) {

        $db = new Database();
        $lValues = $db->getDEPOrientacoesFromDB($idInv);
        $this->orientacoes = array();
        $i = 0;
        while ($row = mysql_fetch_assoc($lValues)) {
            $this->orientacoes[$i++] = new Orientacao(
                    $row['ID'], $row['IDINV'], $row['TIPO_ORIENTACAO'], $row['TIPO_ORIENTADOR'], $row['ESTADO'], $row['TITULO'],$row['NOME_ORIENTANDO']);
        }


        $db->disconnect();
    }

    function getDEPConferencias($idInv) {

        $db = new Database();
        $lValues = $db->getDEPConferenciasFromDB($idInv);
        $this->conferencias = array();
        $i = 0;
        while ($row = mysql_fetch_assoc($lValues)) {
            $this->conferencias[$i++] = new Conferencia(
                    $row['ID'], $row['IDINV'], $row['AMBITO'], $row['TIPO'], $row['DATAINICIO'], $row['DATAFIM'], $row['TITULO'], $row['LOCAL'], $row['PARTICIPANTES'], $row['LINK']);
        }

        $db->disconnect();
    }

    function getDEPSC($iddep) {

        $db = new Database();
        $lValues = $db->getDEPSCFromDB($iddep);
        $this->sc = array();
        $i = 0;
        while ($row = mysql_fetch_assoc($lValues)) {
            $this->sc[$i++] = new SC(
                    $row['ID'], $row['IDINV'], $row['NOMESC'], $row['TIPO'], $row['DATAINICIO'], $row['DATAFIM'], $row['CARGO'], $row['LINK']);
        }

        $db->disconnect();
    }

    function getDEPRedes($iddep) {

        $db = new Database();
        $lValues = $db->getDEPRedesFromDB($iddep);
        $this->redes = array();
        $i = 0;
        while ($row = mysql_fetch_assoc($lValues)) {
            $this->redes[$i++] = new SC(
                    $row['ID'], $row['IDINV'], $row['NOME'], $row['TIPO'], $row['DATAINICIO'], $row['DATAFIM'], $row['CARGO'], $row['LINK']);
        }

        $db->disconnect();
    }

    function getDEPMissoes($iddep) {

        $db = new Database();
        $lValues = $db->getDEPMissoesFromDB($iddep);
        $this->missoes = array();
        $i = 0;
        while ($row = mysql_fetch_assoc($lValues)) {
            $this->missoes[$i++] = new Missoes(
                    $row['ID'], $row['IDINV'], $row['DATAINICIO'], $row['DATAFIM'], $row['MOTIVACAO'], $row['INSTITUICAO'], $row['PAIS'], $row['AMBTESE']);
        }

        $db->disconnect();
    }

    function updatePublicacoesISI() {

        $idInv = $this->investigador->id;
        $db = new Database();
        $lValues = $db->getPublicacoesISIFromDB($idInv);
        $this->publicacoesISI = "";
        $this->publicacoesISI = array();
        while ($row = mysql_fetch_assoc($lValues)) {
            $this->publicacoesISI[$row['ID']] = new Publicacao(
                    $row['ID'], $row['IDINV'], $row['TIPO'], $row['NOMEPUBLICACAO'], $row['ISSN'], $row['ISSN_LINKING'], $row['ISSN_ELECTRONIC'], $row['ISBN'], $row['TITULO'], $row['AUTORES'], $row['VOLUME'], $row['ISSUE'], $row['AR'], $row['COLECAO'], $row['PRIMPAGINA'], $row['ULTPAGINA'], $row['IFACTOR'], $row['CITACOES'], $row['NPATENTE'], $row['DATAPATENTE'], $row['IPC'], $row['EDITORA'], $row['CONFTITLE'], $row['LANGUAGE'], $row['EDITOR'], $row['TIPOFMUP'], $row['LINK'], $row['ESTADO']);
        }

        $db->disconnect();
    }

    function updatePublicacoesPUBMED() {

        $idInv = $this->investigador->id;
        $db = new Database();
        $lValues = $db->getPublicacoesPUBMEDFromDB($idInv);
        $this->publicacoesPUBMED = "";
        $this->publicacoesPUBMED = array();
        while ($row = mysql_fetch_assoc($lValues)) {
            $this->publicacoesPUBMED[$row['ID']] = new Publicacao(
                    $row['ID'], $row['IDINV'], $row['TIPO'], $row['NOMEPUBLICACAO'], $row['ISSN'], $row['ISSN_LINKING'], $row['ISSN_ELECTRONIC'], $row['ISBN'], $row['TITULO'], $row['AUTORES'], $row['VOLUME'], $row['ISSUE'], $row['AR'], $row['COLECAO'], $row['PRIMPAGINA'], $row['ULTPAGINA'], $row['IFACTOR'], $row['CITACOES'], $row['NPATENTE'], $row['DATAPATENTE'], $row['IPC'], $row['EDITORA'], $row['CONFTITLE'], $row['LANGUAGE'], $row['EDITOR'], $row['TIPOFMUP'], $row['LINK'], $row['ESTADO']);
        }

        $db->disconnect();
    }

    function addHabilitacao() {

        $db = new Database();

        $idNOVOINSERTHAB = $db->insertNovaHabilitacao($this->investigador->id);

        //array_push($this->habilitacoes,new Habilitacoes ($idNOVOINSERTHAB,$this->investigador->id,'','','','','','','','',''));

        $this->habilitacoes[$idNOVOINSERTHAB] = new Habilitacoes($idNOVOINSERTHAB, $this->investigador->id, '', '', '', '', '', '', '', '', '','','','','');
    }

    function addArbitragensRevistas() {

        $db = new Database();

        $idNOVOINSERT = $db->insertNovaArbitragensRevistas($this->investigador->id);

        //array_push($this->arbitragensRevistas,new ArbitragensRevistas($idNOVOINSERT,$this->investigador->id,'','','',''));
        $this->arbitragensRevistas[$idNOVOINSERT] = new ArbitragensRevistas($idNOVOINSERT, $this->investigador->id, '', '', '', '', '');
    }

    function addExperiencia() {

        $db = new Database();

        $idNOVOINSERT = $db->insertNovaExperiencia($this->investigador->id);

        //array_push($this->experienciaprofissional,new Experienciaprofissional ($idNOVOINSERT,$this->investigador->id,'','','','','','',''));
        $this->experienciaprofissional[$idNOVOINSERT] = new Experienciaprofissional($idNOVOINSERT, $this->investigador->id, '', '', '', '', '', '', '');
    }

    function addUnidade() {

        $db = new Database();

        $idNOVOINSERT = $db->insertNovaUnidade($this->investigador->id);

        $this->unidadesinvestigacao[$idNOVOINSERT] = new Unidadesinvestigacao($idNOVOINSERT, $this->investigador->id, '', '', '', '', '', '');
    }

    function addAcaoFormacao() {

        $db = new Database();

        $idNOVOINSERT = $db->insertNovaAcaoFormacao($this->investigador->id);

        //array_push($this->acoesformacao,new AcaoFormacao ($idNOVOINSERT,$this->investigador->id,'','','','','','','',''));
        $this->acoesformacao[$idNOVOINSERT] = new AcaoFormacao($idNOVOINSERT, $this->investigador->id, '', '', '', '', '', '', '', '', '');
    }

    function addAcaoDivulgacao() {

        $db = new Database();

        $idNOVOINSERT = $db->insertNovaAcaoDivulgacao($this->investigador->id);

        $this->acoesdivulgacao[$idNOVOINSERT] = new AcaoDivulgacao($idNOVOINSERT, $this->investigador->id, '', '', '', '');
    }

    function addAcreditacao() {

        $db = new Database();

        $idNOVOINSERT = $db->insertNovaAcreditacao($this->investigador->id);

        $this->acreditacoes[$idNOVOINSERT] = new Acreditacao($idNOVOINSERT, $this->investigador->id, '', '', '');
    }

    function addPremio() {

        $db = new Database();

        $idNOVOINSERT = $db->insertNovoPremio($this->investigador->id);

        //array_push($this->premios,new Premio($idNOVOINSERT,$this->investigador->id,'','','',''));
        $this->premios[$idNOVOINSERT] = new Premio($idNOVOINSERT, $this->investigador->id, '', '', '', '', '');
    }

    function addProduto() {

        $db = new Database();

        $idNOVOINSERT = $db->insertNovoProduto($this->investigador->id);

        //array_push($this->produtos,new Produto($idNOVOINSERT,$this->investigador->id,'',''));
        $this->produtos[$idNOVOINSERT] = new Produto($idNOVOINSERT, $this->investigador->id, '', '');
    }

    function addOrientacao() {

        $db = new Database();

        $idNOVOINSERT = $db->insertNovaOrientacao($this->investigador->id);

        //array_push($this->orientacoes,new Orientacao($idNOVOINSERT,$this->investigador->id,'','','','',''));
        $this->orientacoes[$idNOVOINSERT] = new Orientacao($idNOVOINSERT, $this->investigador->id, '', '', '', '', '','');
    }

    function addProjecto() {

        $db = new Database();

        $idNOVOINSERT = $db->insertNovoProjecto($this->investigador->id);

        //array_push($this->projectos,new Projecto($idNOVOINSERT,$this->investigador->id, "", "", "", "","","", "", "",""));

        $this->projectos[$idNOVOINSERT] = new Projecto($idNOVOINSERT, $this->investigador->id, "", "", "", "", "", "", "", "", "", "", "", "", "", "");
    }

    function addNome() {

        $db = new Database();

        $idNOVOINSERT = $db->insertNovoNome($this->investigador->id);

        //array_push($this->identificacaoformal,new Identificacaoformal ($idNOVOINSERT,$this->investigador->id,'',''));
        $this->identificacaoformal[$idNOVOINSERT] = new Identificacaoformal($idNOVOINSERT, $this->investigador->id, '', '');

        echo "NOVO ID " . $idNOVOINSERT;
    }

    function addColaborador() {

        $db = new Database();

        $idNOVOINSERT = $db->insertNovoColaborador($this->investigador->id);

        //array_push($this->colaboradores,new Colaboradores ($idNOVOINSERT,$this->investigador->id,'','',''));

        $this->colaboradores[$idNOVOINSERT] = new Colaboradores($idNOVOINSERT, $this->investigador->id, '', '', '');
    }

    function addColaboradorInterno() {

        $db = new Database();

        $idNOVOINSERT = $db->insertNovoColaboradorInterno($this->investigador->id);

        //array_push($this->colaboradoresinternos,new ColaboradoresInternos ($idNOVOINSERT,$this->investigador->id,'',''));

        $this->colaboradoresinternos[$idNOVOINSERT] = new ColaboradoresInternos($idNOVOINSERT, $this->investigador->id, '', '');
    }

    function addCargoFMUP() {

        $db = new Database();

        $idNOVOINSERT = $db->insertNovoCargoFMUP($this->investigador->id);

        $this->cargosFMUP[$idNOVOINSERT] = new Cargo($idNOVOINSERT, $this->investigador->id, '', '', '', '');
    }

    function addCargoUP() {

        $db = new Database();

        $idNOVOINSERT = $db->insertNovoCargoUP($this->investigador->id);

        $this->cargosUP[$idNOVOINSERT] = new Cargo($idNOVOINSERT, $this->investigador->id, '', '', '', '');
    }

    function addCargoHospital() {

        $db = new Database();

        $idNOVOINSERT = $db->insertNovoCargoHospital($this->investigador->id);

        $this->cargosHospitais[$idNOVOINSERT] = new Cargo($idNOVOINSERT, $this->investigador->id, '', '', '', '', '');
    }

    function addDirCurso() {

        $db = new Database();

        $idNOVOINSERT = $db->insertNovoDirCurso($this->investigador->id);

        //array_push($this->dirCursos,new DirCurso($idNOVOINSERT,$this->investigador->id,'','','',''));

        $this->dirCursos[$idNOVOINSERT] = new DirCurso($idNOVOINSERT, $this->investigador->id, '', '', '', '','');
       
    }

    function addConferencia() {

        $db = new Database();

        $idNOVOINSERT = $db->insertNovaConferencia($this->investigador->id);

        //array_push($this->conferencias,new Conferencia($idNOVOINSERT,$this->investigador->id, '', '', '', '', '', '', ''));

        $this->conferencias[$idNOVOINSERT] = new Conferencia($idNOVOINSERT, $this->investigador->id, '', '', '', '', '', '', '', '');
    }

    function addSC() {

        $db = new Database();

        $idNOVOINSERT = $db->insertNovaSC($this->investigador->id);

        //array_push($this->sc,new SC($idNOVOINSERT,$this->investigador->id, '', '', '', '', ''));
        $this->sc[$idNOVOINSERT] = new SC($idNOVOINSERT, $this->investigador->id, '', '', '', '', '', '');
    }

    function addRede() {

        $db = new Database();

        $idNOVOINSERT = $db->insertNovaRede($this->investigador->id);

        //array_push($this->sc,new SC($idNOVOINSERT,$this->investigador->id, '', '', '', '', ''));
        $this->redes[$idNOVOINSERT] = new Rede($idNOVOINSERT, $this->investigador->id, '', '', '', '', '', '');
    }

    function addMissao() {

        $db = new Database();

        $idNOVOINSERT = $db->insertNovaMissao($this->investigador->id);

        $this->missoes[$idNOVOINSERT] = new Missoes($idNOVOINSERT, $this->investigador->id, '', '', '', '', '', '');
    }

    function addPMI($tipo) {

        $db = new Database();

        $idNOVOINSERT = $db->insertUmaPublicacao($this->investigador->id, $tipo, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", $tipo, '0', '0', "", 0,"2014");
        //array_push($this->publicacoesMANUALInternacional,new Publicacao($idNOVOINSERT,$this->investigador->id, $tipo, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "","",$tipo,"",0));
        $this->publicacoesMANUALInternacional[$idNOVOINSERT] = new Publicacao($idNOVOINSERT, $this->investigador->id, $tipo, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", $tipo, "0", 0,"2014");
    }

    function addPMN($tipo) {

        $db = new Database();

        $idNOVOINSERT = $db->insertUmaPublicacao($this->investigador->id, $tipo, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", $tipo, '0', '0', "", 0,"2014");
        //array_push($this->publicacoesMANUALNacional,new Publicacao($idNOVOINSERT,$this->investigador->id, $tipo, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "","",$tipo,"0",0));
        $this->publicacoesMANUALNacional[$idNOVOINSERT] = new Publicacao($idNOVOINSERT, $this->investigador->id, $tipo, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", $tipo, "0", 0,"2014");
    }

    function addPMP($tipo) {

        $db = new Database();

        $idNOVOINSERT = $db->insertUmaPublicacao($this->investigador->id, $tipo, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", $tipo, '0', '0', "", 0,"2014");
        //array_push($this->publicacoesMANUALPatentes,new Publicacao($idNOVOINSERT,$this->investigador->id, $tipo, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "","",$tipo,"0",0));
        $this->publicacoesMANUALPatentes[$idNOVOINSERT] = new Publicacao($idNOVOINSERT, $this->investigador->id, $tipo, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", $tipo, "0", 0,"2014");
    }

}

function getServicoMail($i) {

    $ser = "";

    $db = new Database();
    $lValues = $db->getLookupValues("lista_servicosfmup");

    while ($row = mysql_fetch_assoc($lValues)) {
        if ($row["ID"] == $i) {
            $ser = $row["DESCRICAO"];
        }
    }

    $db->disconnect();
    return $ser;
}

function auditoria($login, $err, $acao, $query, $idinv) {

    $db = new Database();
    $db->auditoria($login, $err, $acao, $query, $idinv);
}

?>
