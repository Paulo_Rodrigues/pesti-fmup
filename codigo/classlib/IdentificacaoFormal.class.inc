<?php

require_once 'Database.class.inc';

// Tabela DadosCientificos
class IdentificacaoFormal {

    var $id = "";
    var $idinv = "";
    var $nomecientini = "";
    var $nomecientcom = "";

    function IdentificacaoFormal($id, $idinv, $nomecientini, $nomecientcom) {
        $this->id = $id;
        $this->idinv = $idinv;
        $this->nomecientini = $nomecientini;
        $this->nomecientcom = $nomecientcom;
    }
    
/* 
 * User: Paulo Rodrigues
 * Date: 5-03-2014
 * Time: 09:48
 * Changes: função comentada
    function getDataFromDB($login) {
        $db = new Database();
        $lValues = $db->getNomeFromDB($this->login);

        if ($row = mysql_fetch_assoc($lValues)) {
            do {
                $this->id = $row['ID'];
                $this->idinv = $row['IDINV'];
                $this->nomecientini = $row['NOMECIENTINI'];
                $this->nomecientcom = $row['NOMECIENTCOM'];
            } while ($row = mysql_fetch_assoc($lValues));
        }
        $db->disconnect();
    }
*/
    
    function setData($nomecientini, $nomecientcom) {
        $this->nomecientini = $nomecientini;
        $this->nomecientcom = $nomecientcom;
    }

    function saveData() {
        $db = new Database();
        $db->updateNome($this->id, $this->idinv, $this->nomecientini, $this->nomecientcom);
    }

    function validaNomeIni() {
        if (strpos($this->nomecientini, ',') != false)
            return true;
        else
            return false;
    }

    function validaNomeExt() {
        if (strpos($this->nomecientini, ',') != false)
            return true;
        else
            return false;
    }

}
