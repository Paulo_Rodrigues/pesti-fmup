<?php

require_once 'Database.class.inc';

//Tabela Expprofissional

class ExperienciaProfissional {

    var $id = "";
    var $idinv = "";
    var $datainicio = "";
    var $datafim = "";
    var $semtermo = "";
    var $posicao = "";
    var $empregador = "";
    var $departamento = "";
    var $percentagem = "";

    function ExperienciaProfissional($id, $idinv, $datainicio, $datafim, $semtermo, $posicao, $empregador, $departamento, $percentagem) {
        $this->id = $id;
        $this->idinv = $idinv;
        $this->datainicio = $datainicio;
        $this->datafim = $datafim;
        $this->semtermo = $semtermo;
        $this->posicao = $posicao;
        $this->empregador = $empregador;
        $this->departamento = $departamento;
        $this->percentagem = $percentagem;
    }
    
/* 
 * User: Paulo Rodrigues
 * Date: 5-03-2014
 * Time: 09:48
 * Changes: função comentada
 
    function getDataFromDB($login) {
        $db = new Database();
        $lValues = $db->getExperienciaprofissional($this->login);
        if ($row = mysql_fetch_assoc($lValues)) {
            do {
                $this->id = $row['ID'];
                $this->idinv = $row['IDINV'];
                $this->datainicio = $row['DATAINICIO'];
                $this->datafim = $row['DATAFIM'];
                $this->semtermo = $row['SEMTERMO'];
                $this->posicao = $row['POSICAO'];
                $this->empregador = $row['EMPREGADOR'];
                $this->empregador = $row['DEPARTAMENTO'];
                $this->percentagem = $row['PERCENTAGEM'];
            } while ($row = mysql_fetch_assoc($lValues));
        } 
    }
*/
    
    function setData($datainicio, $datafim, $semtermo, $posicao, $empregador, $departamento, $percentagem) {
        $this->datainicio = $datainicio;
        $this->datafim = $datafim;
        $this->semtermo = $semtermo;
        $this->posicao = $posicao;
        $this->empregador = $empregador;
        $this->departamento = $departamento;
        $this->percentagem = $percentagem;
    }

    function saveData() {
        $db = new Database();
        $db->updateExperienciaprofissional($this->id, $this->idinv, $this->datainicio, $this->datafim, $this->semtermo, $this->posicao, $this->empregador, $this->departamento, $this->percentagem);
    }
}
