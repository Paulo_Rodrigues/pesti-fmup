<?php

require_once 'Database.class.inc';
require_once 'Projecto.class.inc';
require_once 'Publicacao.class.inc';
require_once 'Conferencia.class.inc';
require_once 'AcaoDivulgacao.class.inc';
require_once 'Premio.class.inc';
require_once 'ArbitragensRevistas.class.inc';
require_once 'UnidadesInvestigacao.class.inc';

class ValidaRepetidos {
	var $projectos;
	var $publicacoesISI;
	var $publicacoesPUBMED;
	var $publicacoesMANUALPatentes;
	var $publicacoesMANUALInternacional;
	var $publicacoesMANUALNacional;
	var $conferencias;
	var $acoesDivulgacao;
	var $premios;
	var $arbitragensRevistas;
	var $unidadesInvestigacao;
	
	function ValidaRepetidos() {
		$this->getREPProjectos();
		$this->getREPPublicacoesISI();
		$this->getREPPublicacoesPUBMED();
		$this->getREPPublicacoesMANUALPatentes();
		$this->getREPPublicacoesMANUALInternacional();
		$this->getREPPublicacoesMANUALNacional();
		$this->getREPConferencias();
		$this->getREPAcoesDivulgacao();
		$this->getREPPremios();
		$this->getREPArbitragensRevistas();
		$this->getREPUnidadesInvestigacao();
	}
	
	function getREPProjectos() {
		$db = new Database();
		$lValues = $db->getREPProjectosFromDB();
		$this->projectos = array();
		$i = 0;
		while ($row = mysql_fetch_assoc($lValues)) {
			$this->projectos[$i++] = new Projecto(
					$row['ID'], $row['IDINV'], $row['ENTIDADE'], $row['MONTANTE'], $row['MONTANTEA'], $row['MONTANTEFMUP'], $row['INVRESPONSAVEL'], $row['CODIGO'], $row['TITULO'], $row['DATAINICIO'], $row['DATAFIM'], $row['ENTIDADEFINANCIADORA'], $row['ACOLHIMENTO'], $row['LINK'], $row['ESTADO']
			);
		}
				
		$db->disconnect();
	}
	
	function getREPPublicacoesISI() {
		$db = new Database();
		$lValues = $db->getREPPublicacoesISIFromDB();
		$this->publicacoesISI = array();
		$i = 0;
		while ($row = mysql_fetch_assoc($lValues)) {
			$this->publicacoesISI[$i++] = new Publicacao(
					$row['ID'], $row['IDINV'], $row['TIPO'], $row['NOMEPUBLICACAO'], $row['ISSN'], $row['ISSN_LINKING'], $row['ISSN_ELECTRONIC'], $row['ISBN'], $row['TITULO'], $row['AUTORES'], $row['VOLUME'], $row['ISSUE'], $row['AR'], $row['COLECAO'], $row['PRIMPAGINA'], $row['ULTPAGINA'], $row['IFACTOR'], $row['CITACOES'], $row['NPATENTE'], $row['DATAPATENTE'], $row['IPC'], $row['EDITORA'], $row['CONFTITLE'], $row['LANGUAGE'], $row['EDITOR'], $row['TIPOFMUP'], $row['LINK'], $row['ESTADO'], $row['ANO']
			);
		}
		$db->disconnect();
	}
	
	function getREPPublicacoesPUBMED() {
		$db = new Database();
		$lValues = $db->getREPPublicacoesPUBMEDFromDB();
		$this->publicacoesPUBMED = array();
		$i = 0;
		while ($row = mysql_fetch_assoc($lValues)) {
			$this->publicacoesPUBMED[$i++] = new Publicacao(
					$row['ID'], $row['IDINV'], $row['TIPO'], $row['NOMEPUBLICACAO'], $row['ISSN'], $row['ISSN_LINKING'], $row['ISSN_ELECTRONIC'], $row['ISBN'], $row['TITULO'], $row['AUTORES'], $row['VOLUME'], $row['ISSUE'], $row['AR'], $row['COLECAO'], $row['PRIMPAGINA'], $row['ULTPAGINA'], $row['IFACTOR'], $row['CITACOES'], $row['NPATENTE'], $row['DATAPATENTE'], $row['IPC'], $row['EDITORA'], $row['CONFTITLE'], $row['LANGUAGE'], $row['EDITOR'], $row['TIPOFMUP'], $row['LINK'], $row['ESTADO'], $row['ANO']
			);
		}
		$db->disconnect();
	}
	
	function getREPPublicacoesMANUALPatentes() {
		$db = new Database();
		$lValues = $db->getREPPublicacoesMANUALPatentesFromDB();
		$this->publicacoesMANUALPatentes = array();
		$i = 0;
		while ($row = mysql_fetch_assoc($lValues)) {
			$this->publicacoesMANUALPatentes[$i++] = new Publicacao(
					$row['ID'], $row['IDINV'], $row['TIPO'], $row['NOMEPUBLICACAO'], $row['ISSN'], $row['ISSN_LINKING'], $row['ISSN_ELECTRONIC'], $row['ISBN'], $row['TITULO'], $row['AUTORES'], $row['VOLUME'], $row['ISSUE'], $row['AR'], $row['COLECAO'], $row['PRIMPAGINA'], $row['ULTPAGINA'], $row['IFACTOR'], $row['CITACOES'], $row['NPATENTE'], $row['DATAPATENTE'], $row['IPC'], $row['EDITORA'], $row['CONFTITLE'], $row['LANGUAGE'], $row['EDITOR'], $row['TIPOFMUP'], $row['LINK'], $row['ESTADO'], $row['ANO']
			);
		}
		$db->disconnect();
	}
	
	function getREPPublicacoesMANUALInternacional() {
		$db = new Database();
		$lValues = $db->getREPPublicacoesMANUALInternacionalFromDB();
		$this->publicacoesMANUALInternacional = array();
		$i = 0;
		while ($row = mysql_fetch_assoc($lValues)) {
			$this->publicacoesMANUALInternacional[$i++] = new Publicacao(
					$row['ID'], $row['IDINV'], $row['TIPO'], $row['NOMEPUBLICACAO'], $row['ISSN'], $row['ISSN_LINKING'], $row['ISSN_ELECTRONIC'], $row['ISBN'], $row['TITULO'], $row['AUTORES'], $row['VOLUME'], $row['ISSUE'], $row['AR'], $row['COLECAO'], $row['PRIMPAGINA'], $row['ULTPAGINA'], $row['IFACTOR'], $row['CITACOES'], $row['NPATENTE'], $row['DATAPATENTE'], $row['IPC'], $row['EDITORA'], $row['CONFTITLE'], $row['LANGUAGE'], $row['EDITOR'], $row['TIPOFMUP'], $row['LINK'], $row['ESTADO'], $row['ANO']
			);
		}
		$db->disconnect();
	}
	
	function getREPPublicacoesMANUALNacional() {
		$db = new Database();
		$lValues = $db->getREPPublicacoesMANUALNacionalFromDB();
		$this->publicacoesMANUALNacional = array();
		$i = 0;
		while ($row = mysql_fetch_assoc($lValues)) {
			$this->publicacoesMANUALNacional[$i++] = new Publicacao(
					$row['ID'], $row['IDINV'], $row['TIPO'], $row['NOMEPUBLICACAO'], $row['ISSN'], $row['ISSN_LINKING'], $row['ISSN_ELECTRONIC'], $row['ISBN'], $row['TITULO'], $row['AUTORES'], $row['VOLUME'], $row['ISSUE'], $row['AR'], $row['COLECAO'], $row['PRIMPAGINA'], $row['ULTPAGINA'], $row['IFACTOR'], $row['CITACOES'], $row['NPATENTE'], $row['DATAPATENTE'], $row['IPC'], $row['EDITORA'], $row['CONFTITLE'], $row['LANGUAGE'], $row['EDITOR'], $row['TIPOFMUP'], $row['LINK'], $row['ESTADO'], $row['ANO']
			);
		}
		$db->disconnect();
	}
	
	function getREPConferencias() {
		$db = new Database();
		$lValues = $db->getREPConferenciasFromDB();
		$this->conferencias = array();
		$i = 0;
		while ($row = mysql_fetch_assoc($lValues)) {
			$this->conferencias[$i++] = new Conferencia(
				$row['ID'], $row['IDINV'], $row['AMBITO'], $row['TIPO'], $row['DATAINICIO'], $row['DATAFIM'], $row['TITULO'], $row['LOCAL'], $row['PARTICIPANTES'], $row['LINK']
			);
		}
		$db->disconnect();
	}
	
	function getREPAcoesDivulgacao() {
		$db = new Database();
		$lValues = $db->getREPAcoesDivulgacaoFromDB();
		$this->acoesDivulgacao = array();
		$i = 0;
		while ($row = mysql_fetch_assoc($lValues)) {
			$this->acoesDivulgacao[$i++] = new AcaoDivulgacao($row['ID'], $row['IDINV'], $row['TITULO'], $row['DATA'], $row['LOCAL'], $row['NPART']);
		}
		$db->disconnect();
	}
	
	function getREPPremios() {
		$db = new Database();
		$lValues = $db->getREPPremiosFromDB();
		$this->premios = array();
		$i = 0;
		while ($row = mysql_fetch_assoc($lValues)) {
			$this->premios[$i++] = new Premio($row['ID'], $row['IDINV'], $row['NOME'], $row['CONTEXTO'], $row['MONTANTE'], $row['TIPO'], $row['LINK']);
		}
		$db->disconnect();
	}
	
	function getREPArbitragensRevistas() {
		$db = new Database();
		$lValues = $db->getREPArbitragensRevistasFromDB();
		$this->arbitragensRevistas = array();
		$i = 0;
		while ($row = mysql_fetch_assoc($lValues)) {
			$this->arbitragensRevistas[$i++] = new ArbitragensRevistas($row['ID'], $row['IDINV'], $row['ISSN'], $row['NUMERO'], $row['TIPO'], $row['TITULOREVISTA'], $row['LINK']);
		}
		$db->disconnect();
	}
	
	function getREPUnidadesInvestigacao() {
		$db = new Database();
		$lValues = $db->getREPUnidadesInvestigacaoFromDB();
		$this->unidadesInvestigacao = array();
		$i = 0;
		while ($row = mysql_fetch_assoc($lValues)) {
			$this->unidadesInvestigacao[$i++] = new UnidadesInvestigacao($row['ID'], $row['IDINV'], $row['ID_UNIDADE'], $row['UNIDADE'], $row['AVA2007'], $row['PERCENTAGEM'], $row['RESPONSAVEL'], $row['AVACONF']);
		}
		$db->disconnect();
	}
}	
 
class ValidaRepetidosDepartamento { 

	var $projectos;
	var $publicacoesISI;
	var $publicacoesPUBMED;
	var $publicacoesMANUALPatentes;
	var $publicacoesMANUALInternacional;
	var $publicacoesMANUALNacional;
	var $conferencias;
	var $acoesDivulgacao;
	var $premios;	
	var $arbitragensRevistas;
	var $unidadesInvestigacao;
	
	function ValidaRepetidosDepartamento($iddep) {
		$this->getDEPREPProjectosDEP($iddep);
		$this->getDEPREPPublicacoesISI($iddep);
		$this->getDEPREPPublicacoesPUBMED($iddep);
		$this->getDEPREPPublicacoesMANUALPatentes($iddep);
		$this->getDEPREPPublicacoesMANUALInternacional($iddep);
		$this->getDEPREPPublicacoesMANUALNacional($iddep);
		$this->getDEPREPConferencias($iddep);
		$this->getDEPREPAcoesDivulgacao($iddep);
		$this->getDEPREPPremios($iddep);
		$this->getDEPREPArbitragensRevistas($iddep);
		$this->getDEPREPUnidadesInvestigacao($iddep);
	}
	
	function getDEPREPProjectosDEP($iddep)
	{
		$db = new Database();
		$lValues = $db->getREPProjectosDEPFromDB($iddep);
		$this->projectos = array();
		$i = 0;
		while ($row = mysql_fetch_assoc($lValues)) {
			$this->projectos[$i++] = new Projecto(
					$row['ID'], $row['IDINV'], $row['ENTIDADE'], $row['MONTANTE'], $row['MONTANTEA'], $row['MONTANTEFMUP'], $row['INVRESPONSAVEL'], $row['CODIGO'], $row['TITULO'], $row['DATAINICIO'], $row['DATAFIM'], $row['ENTIDADEFINANCIADORA'], $row['ACOLHIMENTO'], $row['LINK'], $row['ESTADO']
			);
		}
		$db->disconnect();
	}
	
	function getDEPREPPublicacoesISI($iddep) {
		$db = new Database();
		$lValues = $db->getREPDEPPublicacoesISIFromDB($iddep);
		$this->publicacoesISI = array();
		$i = 0;
		while ($row = mysql_fetch_assoc($lValues)) {
			$this->publicacoesISI[$i++] = new Publicacao(
					$row['ID'], $row['IDINV'], $row['TIPO'], $row['NOMEPUBLICACAO'], $row['ISSN'], $row['ISSN_LINKING'], $row['ISSN_ELECTRONIC'], $row['ISBN'], $row['TITULO'], $row['AUTORES'], $row['VOLUME'], $row['ISSUE'], $row['AR'], $row['COLECAO'], $row['PRIMPAGINA'], $row['ULTPAGINA'], $row['IFACTOR'], $row['CITACOES'], $row['NPATENTE'], $row['DATAPATENTE'], $row['IPC'], $row['EDITORA'], $row['CONFTITLE'], $row['LANGUAGE'], $row['EDITOR'], $row['TIPOFMUP'], $row['LINK'], $row['ESTADO'], $row['ANO']
			);
		}
		$db->disconnect();
	}
	
	function getDEPREPPublicacoesPUBMED($iddep) {
		$db = new Database();
		$lValues = $db->getREPDEPPublicacoesPUBMEDFromDB($iddep);
		$this->publicacoesPUBMED = array();
		$i = 0;
		while ($row = mysql_fetch_assoc($lValues)) {
			$this->publicacoesPUBMED[$i++] = new Publicacao(
					$row['ID'], $row['IDINV'], $row['TIPO'], $row['NOMEPUBLICACAO'], $row['ISSN'], $row['ISSN_LINKING'], $row['ISSN_ELECTRONIC'], $row['ISBN'], $row['TITULO'], $row['AUTORES'], $row['VOLUME'], $row['ISSUE'], $row['AR'], $row['COLECAO'], $row['PRIMPAGINA'], $row['ULTPAGINA'], $row['IFACTOR'], $row['CITACOES'], $row['NPATENTE'], $row['DATAPATENTE'], $row['IPC'], $row['EDITORA'], $row['CONFTITLE'], $row['LANGUAGE'], $row['EDITOR'], $row['TIPOFMUP'], $row['LINK'], $row['ESTADO'], $row['ANO']
			);
		}
		$db->disconnect();
	}
	
	function getDEPREPPublicacoesMANUALPatentes($iddep) {
		$db = new Database();
		$lValues = $db->getREPDEPPublicacoesMANUALPatentesFromDB($iddep);
		$this->publicacoesMANUALPatentes = array();
		$i = 0;
		while ($row = mysql_fetch_assoc($lValues)) {
			$this->publicacoesMANUALPatentes[$i++] = new Publicacao(
					$row['ID'], $row['IDINV'], $row['TIPO'], $row['NOMEPUBLICACAO'], $row['ISSN'], $row['ISSN_LINKING'], $row['ISSN_ELECTRONIC'], $row['ISBN'], $row['TITULO'], $row['AUTORES'], $row['VOLUME'], $row['ISSUE'], $row['AR'], $row['COLECAO'], $row['PRIMPAGINA'], $row['ULTPAGINA'], $row['IFACTOR'], $row['CITACOES'], $row['NPATENTE'], $row['DATAPATENTE'], $row['IPC'], $row['EDITORA'], $row['CONFTITLE'], $row['LANGUAGE'], $row['EDITOR'], $row['TIPOFMUP'], $row['LINK'], $row['ESTADO'], $row['ANO']
			);
		}
		$db->disconnect();
	}
	
	function getDEPREPPublicacoesMANUALInternacional($iddep) {
		$db = new Database();
		$lValues = $db->getREPDEPPublicacoesMANUALInternacionalFromDB($iddep);
		$this->publicacoesMANUALInternacional = array();
		$i = 0;
		while ($row = mysql_fetch_assoc($lValues)) {
			$this->publicacoesMANUALInternacional[$i++] = new Publicacao(
					$row['ID'], $row['IDINV'], $row['TIPO'], $row['NOMEPUBLICACAO'], $row['ISSN'], $row['ISSN_LINKING'], $row['ISSN_ELECTRONIC'], $row['ISBN'], $row['TITULO'], $row['AUTORES'], $row['VOLUME'], $row['ISSUE'], $row['AR'], $row['COLECAO'], $row['PRIMPAGINA'], $row['ULTPAGINA'], $row['IFACTOR'], $row['CITACOES'], $row['NPATENTE'], $row['DATAPATENTE'], $row['IPC'], $row['EDITORA'], $row['CONFTITLE'], $row['LANGUAGE'], $row['EDITOR'], $row['TIPOFMUP'], $row['LINK'], $row['ESTADO'], $row['ANO']
			);
		}
		$db->disconnect();
	}
	
	function getDEPREPPublicacoesMANUALNacional($iddep) {
		$db = new Database();
		$lValues = $db->getREPDEPPublicacoesMANUALNacionalFromDB($iddep);
		$this->publicacoesMANUALNacional = array();
		$i = 0;
		while ($row = mysql_fetch_assoc($lValues)) {
			$this->publicacoesMANUALNacional[$i++] = new Publicacao(
					$row['ID'], $row['IDINV'], $row['TIPO'], $row['NOMEPUBLICACAO'], $row['ISSN'], $row['ISSN_LINKING'], $row['ISSN_ELECTRONIC'], $row['ISBN'], $row['TITULO'], $row['AUTORES'], $row['VOLUME'], $row['ISSUE'], $row['AR'], $row['COLECAO'], $row['PRIMPAGINA'], $row['ULTPAGINA'], $row['IFACTOR'], $row['CITACOES'], $row['NPATENTE'], $row['DATAPATENTE'], $row['IPC'], $row['EDITORA'], $row['CONFTITLE'], $row['LANGUAGE'], $row['EDITOR'], $row['TIPOFMUP'], $row['LINK'], $row['ESTADO'], $row['ANO']
			);
		}
		$db->disconnect();
	}
	
	function getDEPREPConferencias($iddep) {
		$db = new Database();
		$lValues = $db->getREPDEPConferenciasFromDB($iddep);
		$this->conferencias = array();
		$i = 0;
		while ($row = mysql_fetch_assoc($lValues)) {
			$this->conferencias[$row['ID']] = new Conferencia(
				$row['ID'], $row['IDINV'], $row['AMBITO'], $row['TIPO'], $row['DATAINICIO'], $row['DATAFIM'], $row['TITULO'], $row['LOCAL'], $row['PARTICIPANTES'], $row['LINK']
			);
		}
		$db->disconnect();
	}
	
	function getDEPREPAcoesDivulgacao($iddep) {
		$db = new Database();
		$lValues = $db->getREPDEPAcoesDivulgacaoFromDB($iddep);
		$this->acoesDivulgacao = array();
		$i = 0;
		while ($row = mysql_fetch_assoc($lValues)) {
			$this->acoesDivulgacao[$row['ID']] = new AcaoDivulgacao($row['ID'], $row['IDINV'], $row['TITULO'], $row['DATA'], $row['LOCAL'], $row['NPART']);
		}
		$db->disconnect();
	}
	
	function getDEPREPPremios($iddep) {
		$db = new Database();
		$lValues = $db->getREPDEPPremiosFromDB($iddep);
		$this->premios = array();
		$i = 0;
		while ($row = mysql_fetch_assoc($lValues)) {
			$this->premios[$i++] = new Premio($row['ID'], $row['IDINV'], $row['NOME'], $row['CONTEXTO'], $row['MONTANTE'], $row['TIPO'], $row['LINK']);
		}
		$db->disconnect();
	}
	
	function getDEPREPArbitragensRevistas($iddep) {
		$db = new Database();
		$lValues = $db->getREPDEPArbitragensRevistasFromDB($iddep);
		$this->arbitragensRevistas = array();
		$i = 0;
		while ($row = mysql_fetch_assoc($lValues)) {
			$this->arbitragensRevistas[$i++] = new ArbitragensRevistas($row['ID'], $row['IDINV'], $row['ISSN'], $row['NUMERO'], $row['TIPO'], $row['TITULOREVISTA'], $row['LINK']);
		}
		$db->disconnect();
	}
	
	function getDEPREPUnidadesInvestigacao($iddep) {
		$db = new Database();
		$lValues = $db->getDEPREPUnidadesInvestigacaoFromDB($iddep);
		$this->unidadesInvestigacao = array();
		$i = 0;
		while ($row = mysql_fetch_assoc($lValues)) {
			$this->unidadesInvestigacao[$i++] = new UnidadesInvestigacao($row['ID'], $row['IDINV'], $row['ID_UNIDADE'], $row['UNIDADE'], $row['AVA2007'], $row['PERCENTAGEM'], $row['RESPONSAVEL'], $row['AVACONF']);
		}
		$db->disconnect();
	}
}
?>