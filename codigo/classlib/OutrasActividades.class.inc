<?php

require_once 'Database.class.inc';

//Tabela OutrasActividades

class OutrasActividades {

    var $id = "";
    var $idinv = "";
    var $texapoio = "";
    var $livdidaticos = "";
    var $livdivulgacao = "";
    var $acoesdivulgacaoescolas = "";
    var $acoesdivulgacaogeral = "";
    var $app = "";
    var $webp = "";
    var $tvradiopress = "";

    function OutrasActividades($idinv) {
        $this->getDataFromDB($idinv);
    }

    function getDataFromDB($idinv) {
        $db = new Database();
        $lValues = $db->getOutrasActividades($idinv);

        if ($row = mysql_fetch_assoc($lValues)) {
            do {
                $this->id = $row['ID'];
                $this->idinv = $row['IDINV'];
                $this->texapoio = $row['TEXAPOIO'];
                $this->livdidaticos = $row['LIVDIDATICOS'];
                $this->livdivulgacao = $row['LIVDIVULGACAO'];
                $this->acoesdivulgacaoescolas = $row['ACOESDIVULGACAOESCOLAS'];
                $this->acoesdivulgacaogeral = $row['ACOESDIVULGACAOGERAL'];
                $this->app = $row['APP'];
                $this->webp = $row['WEBP'];
                $this->tvradiopress = $row['TVRADIOPRESS'];
            } while ($row = mysql_fetch_assoc($lValues));
        } else {           
            $db->insertNovoOutrasActividades($idinv);
            $this->getDataFromDB($idinv);
        }

        $db->disconnect();
    }

    function setData($texapoio, $livdidaticos, $livdivulgacao, $acoesdivulgacaoescolas, $acoesdivulgacaogeral, $app, $webp,$tvradiopress) {
        $this->texapoio = $texapoio;
        $this->livdidaticos = $livdidaticos;
        $this->livdivulgacao = $livdivulgacao;
        $this->acoesdivulgacaoescolas = $acoesdivulgacaoescolas;
        $this->acoesdivulgacaogeral = $acoesdivulgacaogeral;
        $this->app = $app;
        $this->webp = $webp;
        $this->tvradiopress = $tvradiopress;
    }

    function saveData() {
        $db = new Database();
        $db->updateOutrasActividades($this->id, $this->idinv, $this->texapoio, $this->livdidaticos, $this->livdivulgacao, $this->acoesdivulgacaoescolas, $this->acoesdivulgacaogeral, $this->app, $this->webp,$this->tvradiopress);
    }
}
