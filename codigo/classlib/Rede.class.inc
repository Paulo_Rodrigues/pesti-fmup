<?php

require_once 'Database.class.inc';

class Rede {

    var $id = "";
    var $idinv = "";
    var $nome = "";
    var $tipo = "";
    var $datainicio = "";
    var $datafim = "";
    var $cargo = "";
    var $link = "";

    function Rede($id, $idinv, $nome, $tipo, $datainicio, $datafim, $cargo, $link) {
        $this->id = $id;
        $this->idinv = $idinv;
        $this->nome = $nome;
        $this->tipo = $tipo;
        $this->datainicio = $datainicio;
        $this->datafim = $datafim;
        $this->cargo = $cargo;
        $this->link = $link;
    }

    function setData($nome, $tipo, $datainicio, $datafim, $cargo, $link) {
        $this->nome = $nome;
        $this->tipo = $tipo;
        $this->datainicio = $datainicio;
        $this->datafim = $datafim;
        $this->cargo = $cargo;
        $this->link = $link;
    }

    function saveData() {
        $db = new Database();
        $db->updateRede($this->id, $this->idinv, $this->nome, $this->tipo, $this->datainicio, $this->datafim, $this->cargo, $this->link);
    }

}
