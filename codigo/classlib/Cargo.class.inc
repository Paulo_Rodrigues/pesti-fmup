<?php

require_once 'Database.class.inc';

//Tabela Cargos

class Cargo {

    var $id = "";
    var $idinv = "";
    var $instituicao = "";
    var $orgao = "";
    var $cargo = "";
    var $datainicio = "";
    var $datafim = "";

    function Cargo($id, $idinv, $instituicao, $orgao, $cargo, $datainicio, $datafim) {
        $this->id = $id;
        $this->idinv = $idinv;
        $this->instituicao = $instituicao;
        $this->orgao = $orgao;
        $this->cargo = $cargo;
        $this->datainicio = $datainicio;
        $this->datafim = $datafim;
    }
/* 
 * User: Paulo Rodrigues
 * Date: 5-03-2014
 * Time: 09:48
 * Changes: função comentada
  
    function getDataFromDB($login) {
        $db = new Database();
        $lValues = $db->getNomeFromDB($this->login);

        if ($row = mysql_fetch_assoc($lValues)) {
            do {
                $this->id = $row[id];
                $this->idinv = $row[idinv];
                $this->instituicao = $row[instituicao];
                $this->orgao = $row[orgao];
                $this->cargo = $row[cargo];
                $this->datainicio = $row[datainicio];
                $this->datafim = $row[datafim];
            } while ($row = mysql_fetch_assoc($lValues));
        }
        $db->disconnect();
    }
*/
    
    function setData($instituicao, $orgao, $cargo, $datainicio, $datafim) {
        $this->instituicao = $instituicao;
        $this->orgao = $orgao;
        $this->cargo = $cargo;
        $this->datainicio = $datainicio;
        $this->datafim = $datafim;
    }

    function saveData() {
        $db = new Database();
        $db->updateCargo($this->id, $this->idinv, $this->instituicao, $this->orgao, $this->cargo, $this->datainicio, $this->datafim);
    }

}
