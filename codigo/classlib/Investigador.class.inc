<?php

require_once 'Database.class.inc';

//Tabela Investigador
class Investigador {

    var $id = "";
    var $nome = "";
    var $nummec = "";
    var $servico = "";
    var $freguesianat = "";
    var $concelhonat = "";
    var $paisnac = "";
    var $paisnat = "";
    var $datanasc = "";
    var $morada = "";
    var $email = "";
    var $email_alternativo = "";
    var $telefone = "";
    var $telemovel = "";
    var $extensao = "";
    var $login = "";
    var $estado = "";
    var $datasubmissao = "";
    var $quest = "";
    var $numidentificacao = "";
    var $sexo = "";

    function Investigador($login) {
        $this->login = $login;
        $this->getDataFromDB($this->login);
    }

    function getDataFromDB($login) {
        $db = new Database();
        $lValues = $db->getInvestigador($this->login);

        if ($row = mysql_fetch_assoc($lValues)) {
            do {
                $this->id = $row['ID'];
                $this->nome = $row['NOME'];
                $this->nummec = $row['NUMMEC'];
                $this->servico = $row['SERVICO'];
                $this->freguesianat = $row['FREGUESIANAT'];
                $this->concelhonat = $row['CONCELHONAT'];
                $this->paisnac = $row['PAISNAC'];
                $this->paisnat = $row['PAISNAT'];
                $this->datanasc = $row['DATANASC'];
                $this->morada = $row['MORADA'];
                $this->email = $row['EMAIL'];
                $this->email_alternativo = $row['EMAIL_ALTERNATIVO'];
                $this->telefone = $row['TELEFONE'];
                $this->telemovel = $row['TELEMOVEL'];
                $this->extensao = $row['EXTENSAO'];
                $this->login = $row['LOGIN'];
                $this->estado = $row['ESTADO'];
                $this->datasubmissao = $row['DATASUBMISSAO'];
                $this->quest = $row['QUEST'];
                $this->numidentificacao = $row['NUMIDENTIFICACAO'];
                $this->sexo = $row['SEXO'];
            } while ($row = mysql_fetch_assoc($lValues));

            $db->disconnect();
        } else {
            $db->insertNewInvestigador($this->login);
            $this->getDataFromDB($this->login);
            $db->insertNovaOcupacaoProfissional($this->id);
            $db->insertNovaOrientacao($this->id);
            $db->insertNovoJuri($this->id);
            $db->insertNovaAula($this->id);
            $db->insertNovaApresentacao($this->id);
            $db->insertNovoOutrasActividades($this->id);
            $db->insertNovaAgregacao($this->id);
            $db->insertNovasObservacoesFinais($this->id);
            $db->insertNovaArbitragemProjetos($this->id);
        }
    }

    function setData($nome, $nummec, $servico, $datanasc, $morada, $email, $email_alternativo, $telefone, $telemovel, $extensao, $freguesianat, $concelhonat, $paisnac, $paisnat, $numidentificacao, $sexo) {
        $this->nome = $nome;
        $this->nummec = $nummec;
        $this->servico = $servico;
        $this->datanasc = $datanasc;
        $this->morada = $morada;
        $this->email = $email;
        $this->email_alternativo = $email_alternativo;
        $this->telefone = $telefone;
        $this->telemovel = $telemovel;
        $this->extensao = $extensao;
        $this->freguesianat = $freguesianat;
        $this->concelhonat = $concelhonat;
        $this->paisnac = $paisnac;
        $this->paisnat = $paisnat;
        $this->numidentificacao = $numidentificacao;
        $this->sexo = $sexo;
    }

    function saveData() {
        $db = new Database();
        $db->updateInvestigador($this->login, $this->nome, $this->nummec, $this->servico, $this->datanasc, $this->morada, $this->email, $this->email_alternativo, $this->telefone, $this->telemovel, $this->extensao, $this->freguesianat, $this->concelhonat, $this->paisnac, $this->paisnat, $this->numidentificacao, $this->sexo);
    }

}
