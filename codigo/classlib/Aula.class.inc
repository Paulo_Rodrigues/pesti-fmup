<?php

require_once 'Database.class.inc';

//Tabela Aulas

class Aula {

    var $id = "";
    var $idinv = "";
    var $regmi = "";
    var $tmi = "";
    var $pmi = "";
    var $regpciclo = "";
    var $regsciclo = "";
    var $regtciclo = "";
    var $tpciclo = "";
    var $tsciclo = "";
    var $ttciclo = "";
    var $ppciclo = "";
    var $psciclo = "";
    var $ptciclo = "";
    var $ncm = "";
    var $nc = "";
    var $regpg = "";
    var $tpg = "";
    var $ppg = "";
    
    var $neap = "";
    var $nei = "";
    var $nep = "";
    var $ncsm = "";
    var $noutros = "";
    

    function Aula($idinv) {
        $this->getDataFromDB($idinv);
    }

    function getDataFromDB($idinv) {
        $db = new Database();
        $lValues = $db->getAulasFromDB($idinv);

        if ($row = mysql_fetch_assoc($lValues)) {
            do {
                $this->id = $row['ID'];
                $this->idinv = $row['IDINV'];
                $this->regmi = $row['REGMI'];
                $this->tmi = $row['TMI'];
                $this->pmi = $row['PMI'];
                $this->regpciclo = $row['REGPCICLO'];
                $this->regsciclo = $row['REGSCICLO'];
                $this->regtciclo = $row['REGTCICLO'];
                $this->tpciclo = $row['TPCICLO'];
                $this->tsciclo = $row['TSCICLO'];
                $this->ttciclo = $row['TTCICLO'];
                $this->ppciclo = $row['PPCICLO'];
                $this->psciclo = $row['PSCICLO'];
                $this->ptciclo = $row['PTCICLO'];
                $this->ncm = $row['NCM'];
                $this->nc = $row['NC'];
                $this->regpg = $row['REGPG'];
                $this->tpg = $row['TPG'];
                $this->ppg = $row['PPG'];
                
                $this->neap = $row['NEAP'];
                $this->nei = $row['NEI'];
                $this->nep = $row['NEP'];
                $this->ncsm = $row['NCSM'];
                $this->noutros = $row['NOUTROS'];
                
                
            } while ($row = mysql_fetch_assoc($lValues));
        } else {
            $db->insertNovaAula($idinv);
            $this->getDataFromDB($idinv);
        }

        $db->disconnect();
    }

    function setData($regmi, $tmi, $pmi, $regpciclo, $regsciclo, $regtciclo, $tpciclo, $tsciclo, $ttciclo, $ppciclo, $psciclo, $ptciclo, $ncm, $nc, $regpg, $tpg, $ppg,$neap,$nei,$nep,$ncsm,$noutros) {
        $this->regmi = $regmi;
        $this->tmi = $tmi;
        $this->pmi = $pmi;
        $this->regpciclo = $regpciclo;
        $this->regsciclo = $regsciclo;
        $this->regtciclo = $regtciclo;
        $this->tpciclo = $tpciclo;
        $this->tsciclo = $tsciclo;
        $this->ttciclo = $ttciclo;
        $this->ppciclo = $ppciclo;
        $this->psciclo = $psciclo;
        $this->ptciclo = $ptciclo;
        $this->ncm = $ncm;
        $this->nc = $nc;
        $this->regpg = $regpg;
        $this->tpg = $tpg;
        $this->ppg = $ppg;
        
        $this->neap = $neap;
        $this->nei = $nei;
        $this->nep = $nep;
        $this->ncsm = $ncsm;
        $this->noutros = $noutros;
        
    }

    function saveData() {
        $db = new Database();
        $db->updateAulas($this->id, $this->idinv, $this->regmi, $this->tmi, $this->pmi, $this->regpciclo, $this->regsciclo, $this->regtciclo, $this->tpciclo, $this->tsciclo, $this->ttciclo, $this->ppciclo, $this->psciclo, $this->ptciclo, $this->ncm, $this->nc, $this->regpg, $this->tpg, $this->ppg,
        $this->neap,
        $this->nei,
        $this->nep,
        $this->ncsm,
        $this->noutros);
    }

}
