<?php

require_once 'Database.class.inc';

//Tabela Juris

class Juri {

    var $id = "";
    var $idinv = "";
    var $agregacaoargfmup = "";
    var $agregacaovogalfmup = "";
    var $agregacaoargext = "";
    var $agregacaovogalext = "";
    var $doutoramentoargfmup = "";
    var $doutoramentovogalfmup = "";
    var $doutoramentoargext = "";
    var $doutoramentovogalext = "";
    var $mestradoargfmup = "";
    var $mestradovogalfmup = "";
    var $mestradoargext = "";
    var $mestradovogalext = "";
    var $mestradointargfmup = "";
    var $mestradointvogalfmup = "";
    var $mestradointargext = "";
    var $mestradointvogalext = "";
    var $provimentovogalfmup = "";
    var $provimentovogalext = "";

    function Juri($idinv) {

        $this->getDataFromDB($idinv);
    }

    function getDataFromDB($idinv) {

        $db = new Database();
        $lValues = $db->getJurisFromDB($idinv);

        if ($row = mysql_fetch_assoc($lValues)) {
            do {
                $this->id = $row['ID'];
                $this->idinv = $row['IDINV'];
                $this->agregacaoargfmup = $row['AGREGACAOARGFMUP'];
                $this->agregacaovogalfmup = $row['AGREGACAOVOGALFMUP'];
                $this->agregacaoargext = $row['AGREGACAOARGEXT'];
                $this->agregacaovogalext = $row['AGREGACAOVOGALEXT'];
                $this->doutoramentoargfmup = $row['DOUTORAMENTOARGFMUP'];
                $this->doutoramentovogalfmup = $row['DOUTORAMENTOVOGALFMUP'];
                $this->doutoramentoargext = $row['DOUTORAMENTOARGEXT'];
                $this->doutoramentovogalext = $row['DOUTORAMENTOVOGALEXT'];
                $this->mestradoargfmup = $row['MESTRADOARGFMUP'];
                $this->mestradovogalfmup = $row['MESTRADOVOGALFMUP'];
                $this->mestradoargext = $row['MESTRADOARGEXT'];
                $this->mestradovogalext = $row['MESTRADOVOGALEXT'];
                $this->mestradointargfmup = $row['MESTRADOINTARGFMUP'];
                $this->mestradointvogalfmup = $row['MESTRADOINTVOGALFMUP'];
                $this->mestradointargext = $row['MESTRADOINTARGEXT'];
                $this->mestradointvogalext = $row['MESTRADOINTVOGALEXT'];
                $this->provimentovogalfmup = $row['PROVIMENTOVOGALFMUP'];
                $this->provimentovogalext = $row['PROVIMENTOVOGALEXT'];
            } while ($row = mysql_fetch_assoc($lValues));
        } else {
            $db->insertNovoJuri($idinv);
            $this->getDataFromDB($idinv);
        }
        $db->disconnect();
    }

    function setData($agregacaoargfmup, $agregacaovogalfmup, $agregacaoargext, $agregacaovogalext, $doutoramentoargfmup, $doutoramentovogalfmup, $doutoramentoargext, $doutoramentovogalext, $mestradoargfmup, $mestradovogalfmup, $mestradoargext, $mestradovogalext, $mestradointargfmup, $mestradointvogalfmup, $mestradointargext, $mestradointvogalext,$provimentovogalfmup,$provimentovogalext) {
        $this->agregacaoargfmup = $agregacaoargfmup;
        $this->agregacaovogalfmup = $agregacaovogalfmup;
        $this->agregacaoargext = $agregacaoargext;
        $this->agregacaovogalext = $agregacaovogalext;
        $this->doutoramentoargfmup = $doutoramentoargfmup;
        $this->doutoramentovogalfmup = $doutoramentovogalfmup;
        $this->doutoramentoargext = $doutoramentoargext;
        $this->doutoramentovogalext = $doutoramentovogalext;
        $this->mestradoargfmup = $mestradoargfmup;
        $this->mestradovogalfmup = $mestradovogalfmup;
        $this->mestradoargext = $mestradoargext;
        $this->mestradovogalext = $mestradovogalext;
        $this->mestradointargfmup = $mestradointargfmup;
        $this->mestradointvogalfmup = $mestradointvogalfmup;
        $this->mestradointargext = $mestradointargext;
        $this->mestradointvogalext = $mestradointvogalext;
        $this->provimentovogalfmup=$provimentovogalfmup;
        $this->provimentovogalext=$provimentovogalext;
    }

    function saveData() {
        $db = new Database();
        $db->updateJuri($this->id, $this->idinv, $this->agregacaoargfmup, $this->agregacaovogalfmup, $this->agregacaoargext, $this->agregacaovogalext, $this->doutoramentoargfmup, $this->doutoramentovogalfmup, $this->doutoramentoargext, $this->doutoramentovogalext, $this->mestradoargfmup, $this->mestradovogalfmup, $this->mestradoargext, $this->mestradovogalext, $this->mestradointargfmup, $this->mestradointvogalfmup, $this->mestradointargext, $this->mestradointvogalext,$this->provimentovogalfmup,$this->provimentovogalfmup);
    }

}
