<?php

require_once 'Database.class.inc';

// Tabela Projectosinv 
class ClinicalTrial {

    var $id = "";
    var $idinv = "";
    var $entidade = "";
    var $montante = "";
    var $montantea = "";
    var $montantefmup = "";
    var $invresponsavel = "";
    var $codigo = "";
    var $titulo = "";
    var $datainicio = "";
    var $datafim = "";
    var $entidadefinanciadora = "";
    var $acolhimento = "";
    var $link = "";
    var $estado = "";
    var $estagio = "";

    function ClinicalTrial($id, $idinv, $entidade, $montante, $montantea, $montantefmup, $invresponsavel, $codigo, $titulo, $datainicio, $datafim, $entidadefinanciadora, $acolhimento, $link, $estado,$estagio) {
        $this->id = $id;
        $this->idinv = $idinv;
        $this->entidade = $entidade;
        $this->montante = $montante;
        $this->montantea = $montantea;
        $this->montantefmup = $montantefmup;
        $this->invresponsavel = $invresponsavel;
        $this->codigo = $codigo;
        $this->titulo = $titulo;
        $this->datainicio = $datainicio;
        $this->datafim = $datafim;
        $this->entidadefinanciadora = $entidadefinanciadora;
        $this->acolhimento = $acolhimento;
        $this->link = $link;
        $this->estado = $estado;
        $this->estagio = $estagio;
    }

    function getDataFromDB($login) {
        $db = new Database();
        $lValues = $db->getProjectosFromDB($this->login);

        if ($row = mysql_fetch_assoc($lValues)) {
            do {
                $this->id = $row['ID'];
                $this->idinv = $row['IDINV'];
                $this->entidade = $row['ENTIDADE'];
                $this->montante = $row['MONTANTE'];
                $this->montantea = $row['MONTANTEA'];
                $this->montantefmup = $row['MONTANTEFMUP'];
                $this->invresponsavel = $row['INVRESPONSAVEL'];
                $this->codigo = $row['CODIGO'];
                $this->titulo = $row['TITULO'];
                $this->datainicio = $row['DATAINICIO'];
                $this->datafim = $row['DATAFIM'];
                $this->entidadefinanciadora = $row['ENTIDADEFINANCIADORA'];
                $this->acolhimento = $row['ACOLHIMENTO'];
                $this->link = $row['LINK'];
                $this->estado = $row['ESTADO'];
                $this->estagio = $row['ESTAGIO'];
            } while ($row = mysql_fetch_assoc($lValues));
        } else {

        }
        $db->disconnect();
    }

    function setData($entidade, $montante, $montantea, $montantefmup, $invresponsavel, $codigo, $titulo, $datainicio, $datafim, $entidadefinanciadora, $acolhimento, $link, $estado,$estagio) {
        $this->entidade = $entidade;
        $this->montante = $montante;
        $this->montantea = $montantea;
        $this->montantefmup = $montantefmup;
        $this->invresponsavel = $invresponsavel;
        $this->codigo = $codigo;
        $this->titulo = $titulo;
        $this->datainicio = $datainicio;
        $this->datafim = $datafim;
        $this->entidadefinanciadora = $entidadefinanciadora;
        $this->acolhimento = $acolhimento;
        $this->link = $link;
        $this->estado = $estado;
        $this->estagio = $estagio;
    }

    function saveData() {
        $db = new Database();
        $db->updateClinicalTrial($this->id, $this->idinv, $this->entidade, $this->montante, $this->montantea, $this->montantefmup, $this->invresponsavel, $this->codigo, $this->titulo, $this->datainicio, $this->datafim, $this->entidadefinanciadora, $this->acolhimento, $this->link, $this->estado, $this->estagio);
    }

}
