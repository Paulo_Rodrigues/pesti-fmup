<?php

require_once 'Database.class.inc';

//Tabela Premio

class Premio {

    var $id = "";
    var $idinv = "";
    var $nome = "";
    var $contexto = "";
    var $montante = "";
    var $tipo = "";
    var $link = "";

    function Premio($id, $idinv, $nome, $contexto, $montante, $tipo, $link) {
        $this->id = $id;
        $this->idinv = $idinv;
        $this->nome = $nome;
        $this->contexto = $contexto;
        $this->montante = $montante;
        $this->tipo = $tipo;
        $this->link = $link;
    }

    function setData($nome, $contexto, $montante, $tipo, $link) {
        $this->nome = $nome;
        $this->contexto = $contexto;
        $this->montante = $montante;
        $this->tipo = $tipo;
        $this->link = $link;
    }

    function saveData() {
        $db = new Database();
        $db->updatePremio($this->id, $this->idinv, $this->nome, $this->contexto, $this->montante, $this->tipo, $this->link);
    }

}
