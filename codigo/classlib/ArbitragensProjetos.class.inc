<?php

require_once 'Database.class.inc';

//Tabela ArbitragensProjectos

class ArbitragensProjetos {

    var $id = "";
    var $idinv = "";
    var $api = "";
    var $apn = "";

    function ArbitragensProjetos($idinv) {
        $this->getArbitragensProjetosFromDB($idinv);
    }

    function getArbitragensProjetosFromDB($idinv) {
        $db = new Database();
        $lValues = $db->getArbitragensProjetosFromDB($idinv);

        if ($row = mysql_fetch_assoc($lValues)) {
            do {
                $this->id = $row['ID'];
                $this->idinv = $row['IDINV'];
                $this->api = $row['API'];
                $this->apn = $row['APN'];
            } while ($row = mysql_fetch_assoc($lValues));
        } else {
            $db->insertNovaArbitragemProjetos($idinv);
            $this->getDataFromDB($idinv);
        }

        $db->disconnect();
    }

    function setData($api, $apn) {
        $this->api = $api;
        $this->apn = $apn;
    }

    function saveData() {

        $db = new Database();
        $db->updateArbitragensProjetos($this->id, $this->idinv, $this->api, $this->apn);
    }

}
