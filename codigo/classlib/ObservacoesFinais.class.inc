<?php

require_once 'Database.class.inc';

//Tabela ObservacoesFinais

class ObservacoesFinais {

    var $id = "";
    var $idinv = "";
    var $incluidomaisdetalhe = "";
    var $incluido = "";
    var $tempo = "";
    var $automatizacao = "";
    var $completividade = "";
    var $organizacao = "";
    var $apresentacao = "";
    var $observacoes = "";

    function ObservacoesFinais($idinv) {

        $this->getDataFromDB($idinv);
    }

    function getDataFromDB($idinv) {

        $db = new Database();
        $lValues = $db->getOFinaisFromDB($idinv);

        if ($row = mysql_fetch_assoc($lValues)) {
            do {
                $this->id = $row['ID'];
                $this->idinv = $row['IDINV'];
                $this->incluidomaisdetalhe = $row['INCLUIDOMAISDETALHE'];
                $this->incluido = $row['INCLUIDO'];
                $this->tempo = $row['TEMPO'];
                $this->automatizacao = $row['AUTOMATIZACAO'];
                $this->completividade = $row['COMPLETIVIDADE'];
                $this->organizacao = $row['ORGANIZACAO'];
                $this->apresentacao = $row['APRESENTACAO'];
                $this->observacoes = $row['OBSERVACOES'];
            } while ($row = mysql_fetch_assoc($lValues));
        } else {
            $db->insertNovasObservacoesFinais($idinv);
            $this->getDataFromDB($idinv);
        }

        $db->disconnect();
    }

    function setData($incluidomaisdetalhe, $incluido, $tempo, $automatizacao, $completividade, $organizacao, $apresentacao, $observacoes) {
        $this->incluidomaisdetalhe = $incluidomaisdetalhe;
        $this->incluido = $incluido;
        $this->tempo = $tempo;
        $this->automatizacao = $automatizacao;
        $this->completividade = $completividade;
        $this->organizacao = $organizacao;
        $this->apresentacao = $apresentacao;
        $this->observacoes = $observacoes;
    }

    function saveData() {
        $db = new Database();
        $db->updateObservacoesFinais($this->id, $this->idinv, $this->incluidomaisdetalhe, $this->incluido, $this->tempo, $this->automatizacao, $this->completividade, $this->organizacao, $this->apresentacao, $this->observacoes);
    }

}
