<?php

require_once 'Database.class.inc';

//Tabela ColaboradoresInternos

class ColaboradoresInternos {

    var $id = "";
    var $idinv = "";
    var $nome = "";
    var $nomecient = "";
    var $departamento = "";

    function ColaboradoresInternos($id, $idinv, $nome, $nomecient, $departamento) {
        $this->id = $id;
        $this->idinv = $idinv;
        $this->nome = $nome;
        $this->nomecient = $nomecient;
        $this->departamento = $departamento;
    }

/* 
 * User: Paulo Rodrigues
 * Date: 5-03-2014
 * Time: 09:48
 * Changes: função comentada
          
    function getDataFromDB($login) {

        $db = new Database();
        $lValues = $db->getColaboradoresInternos($this->login);

        if ($row = mysql_fetch_assoc($lValues)) {
            do {
                $this->id = $row['ID'];
                $this->idinv = $row['IDINV'];
                $this->nomecient = $row['NOMECIENT'];
                $this->departamento = $row['DEPARTAMENTO'];
            } while ($row = mysql_fetch_assoc($lValues));
        }
        $db->disconnect();
    }
*/
    function setData($nome,$nomecient, $departamento) {

    	$this->nome = $nome;
    	$this->nomecient = $nomecient;
        $this->departamento = $departamento;
    }

    function saveData() {
        $db = new Database();
        $db->updateColaboradoresInternos($this->id, $this->idinv, $this->nome,$this->nomecient, $this->departamento);
    }

    function validaNome() {
        if (strpos($this->nomecient, ',') != false and (trim(strpos($this->nomecient, ',')) != (strlen(trim($this->nomecient)) - 1) and trim(strpos($this->nomecient, ',')) != 0))
            return true;
        else
            return false;
    }

}
