<?php

require_once 'Database.class.inc';

//Tabela UnidadesInvestigacao

class UnidadesInvestigacao {

    var $id = "";
    var $idinv = "";
    var $id_unidade = "";
    var $unidade = "";
    var $ava2007 = "";
    var $percentagem = "";
    var $responsavel = "";
    var $avaconf = "";

    function UnidadesInvestigacao($id, $idinv, $id_unidade, $unidade, $ava2007, $percentagem, $responsavel, $avaconf) {
        $this->id = $id;
        $this->idinv = $idinv;
        $this->id_unidade = $id_unidade;
        $this->unidade = $unidade;
        $this->ava2007 = $ava2007;
        $this->percentagem = $percentagem;
        $this->responsavel = $responsavel;
        $this->avaconf = $avaconf;
    }
/* 
 * User: Paulo Rodrigues
 * Date: 5-03-2014
 * Time: 09:52
 * Changes: função comentada
 
    function getDataFromDB($login) {
        $db = new Database();
        $lValues = $db->getUnidades($this->login);

        if ($row = mysql_fetch_assoc($lValues)) {
            do {
                $this->id = $row['ID'];
                $this->idinv = $row['IDINV'];
                $this->id_unidade = $row['ID_UNIDADE'];
                $this->unidade = $row['UNIDADE'];
                $this->ava2007 = $row['AVA2007'];
                $this->percentagem = $row['PERCENTAGEM'];
                $this->responsavel = $row['RESPONSAVEL'];
                $this->avaconf = $row['AVACONF'];
            } while ($row = mysql_fetch_assoc($lValues));
        } else {
            
        }
        $db->disconnect();
    }
*/
    function setData($id_unidade, $unidade, $ava2007, $percentagem, $responsavel, $avaconf) {
        $this->id_unidade = $id_unidade;
        $this->unidade = $unidade;
        $this->ava2007 = $ava2007;
        $this->percentagem = $percentagem;
        $this->responsavel = $responsavel;
        $this->avaconf = $avaconf;
    }

    function saveData() {
        $db = new Database();
        $db->updateUnidades($this->id, $this->idinv, $this->id_unidade, $this->unidade, $this->ava2007, $this->percentagem, $this->responsavel, $this->avaconf);
    }

}
