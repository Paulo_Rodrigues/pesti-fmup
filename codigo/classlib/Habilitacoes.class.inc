<?php

require_once 'Database.class.inc';

//Tabela  Habilitacoesliterarias
class Habilitacoes {

    var $id = "";
    var $idinv = "";
    var $anoinicio = "-1";
    var $anofim = "-1";
    var $curso = "";
    var $instituicao = "";
    var $grau = "-1";
    var $distincao = "-1";
    var $bolsa = "-1";
    var $titulo = "";
    var $percentagem = "0";
    var $orientador = "";
    var $oinstituicao = "";
    var $corientador = "";
    var $coinstituicao = "";

    function Habilitacoes($id, $idinv, $anoinicio, $anofim, $curso, $instituicao, $grau, $distincao, $bolsa, $titulo, $percentagem,$orientador,$oinstituicao,$corientador,$coinstituicao) {
        $this->id = $id;
        $this->idinv = $idinv;
        $this->anoinicio = $anoinicio;
        $this->anofim = $anofim;
        $this->curso = $curso;
        $this->instituicao = $instituicao;
        $this->grau = $grau;
        $this->distincao = $distincao;
        $this->bolsa = $bolsa;
        $this->titulo = $titulo;
        $this->percentagem = $percentagem;
        $this->orientador = $orientador;
        $this->oinstituicao = $oinstituicao;
        $this->corientador = $corientador;
        $this->coinstituicao = $coinstituicao;
    }
 
    function setData($anoinicio, $anofim, $curso, $instituicao, $grau, $distincao, $bolsa, $titulo, $percentagem,$orientador,$oinstituicao,$corientador,$coinstituicao) {

        $this->anoinicio = $anoinicio;
        $this->anofim = $anofim;
        $this->curso = $curso;
        $this->instituicao = $instituicao;
        $this->grau = $grau;
        $this->distincao = $distincao;
        $this->bolsa = $bolsa;
        $this->titulo = $titulo;
        $this->percentagem = $percentagem;
        $this->orientador = $orientador;
        $this->oinstituicao = $oinstituicao;
        $this->corientador = $corientador;
        $this->coinstituicao = $coinstituicao;
    }
    
    function saveData() {
        $db = new Database();
        $db->updateHabilitacoes($this->id, $this->idinv, 
        		$this->anoinicio,
        		$this->anofim, 
        		$this->curso, 
        		$this->instituicao, 
        		$this->grau, 
        		$this->distincao, 
        		$this->bolsa, 
        		$this->titulo, 
        		$this->percentagem,
        		$this->orientador,
        		$this->oinstituicao,
       		 	$this->corientador,
        		$this->coinstituicao);
    }
}
