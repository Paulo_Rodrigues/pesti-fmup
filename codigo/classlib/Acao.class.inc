<?php

class Acao {
	var $id;
	var $idreg;
	var $tabela;
	var $acao;
	var $autor;
	var $data;
	var $campos;
	var $estado;
	var $descricao;
	var $departamento;

	function Acao($id, $idreg, $tabela, $acao, $autor, $data, $campos, $estado, $descricao, $departamento) {
		$this->id = $id;
        $this->idreg = $idreg;
        $this->tabela = $tabela;
        $this->acao = $acao;
        $this->autor = $autor;
        $this->data = $data;
        $this->campos = $campos;
        $this->estado = $estado;
        $this->descricao = $descricao;
        $this->departamento = $departamento;
	}
}	
?>