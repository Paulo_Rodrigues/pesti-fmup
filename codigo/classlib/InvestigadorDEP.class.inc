<?php

require_once 'Database.class.inc';

class InvestigadorDEP {

    var $id = "";
    var $nome = "";
    var $nummec = "";
    var $datanasc = "";
    var $login = "";
    var $estado = "";
    var $datasubmissao = "";
    var $numidentificacao = "";

    function InvestigadorDEP($id, $nome, $nummec, $datanasc, $login, $estado, $datasubmissao, $numidentificacao) {

        $this->id = $id;
        $this->nome = $nome;
        $this->nummec = $nummec;
        $this->datanasc = $datanasc;
        $this->login = $login;
        $this->estado = $estado;
        $this->datasubmissao = $datasubmissao;
        $this->numidentificacao = $numidentificacao;
    }

}
