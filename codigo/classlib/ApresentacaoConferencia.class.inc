<?php

require_once 'Database.class.inc';

//Tabela conferencias 

class ApresentacaoConferencia {

    var $id = "";
    var $idinv = "";
    var $ambito = "";
    var $tipo = "";
    var $datainicio = "";
    var $datafim = "";
    var $titulo = "";
    var $local = "";
    var $participantes = "";
    var $link = "";

    function ApresentacaoConferencia($id, $idinv, $ambito, $tipo, $datainicio, $datafim, $titulo, $local, $participantes, $link) {
        $this->id = $id;
        $this->idinv = $idinv;
        $this->ambito = $ambito;
        $this->tipo = $tipo;
        $this->datainicio = $datainicio;
        $this->datafim = $datafim;
        $this->titulo = $titulo;
        $this->local = $local;
        $this->participantes = $participantes;
        $this->link = $link;
    }

    function setData($ambito, $tipo, $datainicio, $datafim, $titulo, $local, $participantes, $link) {
        $this->ambito = $ambito;
        $this->tipo = $tipo;
        $this->datainicio = $datainicio;
        $this->datafim = $datafim;
        $this->titulo = $titulo;
        $this->local = $local;
        $this->participantes = $participantes;
        $this->link = $link;
    }

    function saveData() {
        $db = new Database();
        $db->updateApresentacaoConferencia($this->id, $this->idinv, $this->ambito, $this->tipo, $this->datainicio, $this->datafim, $this->titulo, $this->local, $this->participantes, $this->link);
    }

}
