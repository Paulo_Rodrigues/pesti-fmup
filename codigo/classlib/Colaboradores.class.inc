<?php

require_once 'Database.class.inc';

// Tabela Colaboradores

class Colaboradores {

    var $id = "";
    var $idinv = "";
    var $nomecient = "";
    var $instituicao = "";
    var $pais = "";

    function Colaboradores($id, $idinv, $nomecient, $instituicao, $pais) {
        $this->id = $id;
        $this->idinv = $idinv;
        $this->nomecient = $nomecient;
        $this->instituicao = $instituicao;
        $this->pais = $pais;
    }
/* 
 * User: Paulo Rodrigues
 * Date: 5-03-2014
 * Time: 09:48
 * Changes: função comentada
 *
    function getDataFromDB($login) {
        $db = new Database();
        $lValues = $db->getColaboradores($this->login);

        if ($row = mysql_fetch_assoc($lValues)) {
            do {
                $this->id = $row['ID'];
                $this->idinv = $row['IDINV'];
                $this->nomecient = $row['NOMECIENT'];
                $this->instituicao = $row['INSTITUICAO'];
                $this->pais = $row['PAIS'];
            } while ($row = mysql_fetch_assoc($lValues));
        } else {
            
        }
        $db->disconnect();
    }
*/
    function setData($nomecient, $instituicao, $pais) {

        $this->nomecient = $nomecient;
        $this->instituicao = $instituicao;
        $this->pais = $pais;
    }

    function saveData() {
        $db = new Database();
        $db->updateColaboradores($this->id, $this->idinv, $this->nomecient, $this->instituicao, $this->pais);
    }

    function validaNome() {
        if (strpos($this->nomecient, ',') != false and (trim(strpos($this->nomecient, ',')) != (strlen(trim($this->nomecient)) - 1) and trim(strpos($this->nomecient, ',')) != 0))
            return true;
        else
            return false;
    }

}
