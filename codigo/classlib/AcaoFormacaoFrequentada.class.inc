<?php

require_once 'Database.class.inc';

//Tabela AcaoFormacao

class AcaoFormacaoFrequentada {

    var $id = "";
    var $idinv = "";
    var $titulo = "";
    var $horas = "";
    var $dataini = "";
    var $datafim = "";
    var $local = "";
    var $objectivo = "";
    var $tipo = "";

    function AcaoFormacaoFrequentada($id, $idinv, $titulo, $horas, $dataini, $datafim, $local, $objectivo,$tipo) {
        $this->id = $id;
        $this->idinv = $idinv;
        $this->titulo = $titulo;
        $this->horas = $horas;
        $this->dataini = $dataini;
        $this->datafim = $datafim;
        $this->local = $local;
        $this->objectivo = $objectivo;
        $this->tipo = $tipo;
    }

    function getDataFromDB($login) {
        $db = new Database();
        $lValues = $db->getAcoesFormacaoFrequentada($this->login);

        if ($row = mysql_fetch_assoc($lValues)) {
            do {
                $this->id = $row['ID'];
                $this->idinv = $row['IDINV'];
                $this->titulo = $row['TITULO'];
                $this->horas = $row['HORAS'];
                $this->dataini = $row['DATAINI'];
                $this->datafim = $row['DATAFIM'];
                $this->local = $row['LOCAL'];
                $this->objectivo = $row['OBJECTIVO'];
                $this->tipo = $row['TIPO'];
            } while ($row = mysql_fetch_assoc($lValues));
        } else {
            
        }
        $db->disconnect();
    }

    function setData($titulo, $horas, $dataini, $datafim, $local, $objectivo,$tipo) {
        $this->titulo = $titulo;
        $this->horas = $horas;
        $this->dataini = $dataini;
        $this->datafim = $datafim;
        $this->local = $local;
        $this->objectivo = $objectivo;
        $this->tipo = $tipo;
    }

    function saveData() {
        $db = new Database();
        $db->updateAcaoFormacaoFrequentada($this->id, $this->idinv, $this->titulo, $this->horas, $this->dataini, $this->datafim, $this->local, $this->objectivo,$this->tipo);
    }

}
