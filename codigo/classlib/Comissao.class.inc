<?php

require_once 'Database.class.inc';

class Comissao
{

	var $id="";
	var $idinv="";
	var $instituicao="";
	var $orgao="";
	var $cargo="";
	var $datainicio="";
	var $datafim="";	
	
   
   function Comissao (
					$id,
					$idinv,
					$instituicao,
					$orgao,
					$cargo,
					$datainicio,
					$datafim
   				){
					
					
		$this->id=$id;
		$this->idinv=$idinv;
		$this->instituicao=$instituicao;
		$this->orgao=$orgao;
		$this->cargo=$cargo;
		$this->datainicio=$datainicio;
		$this->datafim=$datafim;						
   }
	
	function setData(
				$instituicao,
				$orgao,
				$cargo,
				$datainicio,
				$datafim){ 

			$this->instituicao=$instituicao;
			$this->orgao=$orgao;
			$this->cargo=$cargo;
			$this->datainicio=$datainicio;
			$this->datafim=$datafim;
        }
	
	function saveData(){
		
		$db = new Database();
		$db->updateCargoCom($this->id, $this->idinv, $this->instituicao, $this->orgao, $this->cargo, $this->datainicio, $this->datafim);
		
		$db->disconnect();
	}
	

}
	
