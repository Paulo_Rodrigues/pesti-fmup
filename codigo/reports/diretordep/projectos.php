<?php

/**
 * Autor: Paulo Rodrigues (prodrigues@med.up.pt/paulomiguelarodrigues@gmail.com)
 * Data: 23/04/2014
 * Alterações: Capacidade de edição e remover registos adicionada.
 */

echo "<h3>Projetos / Ensaios Clínicos 2013</h3>\n
    <div id='projectos'>
    <table id='prj' class='box-table-b'>
        <thead>
            <tr>
                <th>IDINV</th>
                <th>Nome</th>
                <th>Tipo/Entidade</th>
                <th>Entidade Financiadora</th>
                <th>Instituição de Acolhimento</th>
                <th>Montante  total solicitado</th>
                <th>Montante  total aprovado</th>
                <th>Montante  atribuído à FMUP</th>
                <th>É Investigador Responsável?</th>
                <th>Código/Referência</th>
                <th>Título</th>
                <th>Data Início</th>
                <th>Data Fim</th>
                <th>Link</th>
                <th>Estado</th>
            </tr>
        </thead>
        <tbody>";

    foreach ($dadosDep->projectos as $i => $value){
		echo "<tr>";
            echo "<td >".$dadosDep->projectos[$i]->idinv."</td>";
            echo "<td>".$dadosDep->investigadores[$dadosDep->projectos[$i]->idinv]->nome."</td>";
            echo "<td id='td_projectos_tipoentidade_". $dadosDep->projectos[$i]->id ."'>";
            getTipoEntidade($dadosDep->projectos[$i]->entidade);
            echo "</td>";
            echo "<td id='td_projectos_entidade_". $dadosDep->projectos[$i]->id ."'>".$dadosDep->projectos[$i]->entidadefinanciadora."</td>";
            echo "<td id='td_projectos_acolhimento_". $dadosDep->projectos[$i]->id ."'>".$dadosDep->projectos[$i]->acolhimento."</td>";
            echo "<td id='td_projectos_montante_". $dadosDep->projectos[$i]->id ."'>".$dadosDep->projectos[$i]->montante." €</td>";
            echo "<td id='td_projectos_montantea_". $dadosDep->projectos[$i]->id ."'>".$dadosDep->projectos[$i]->montantea." €</td>";
            echo "<td id='td_projectos_montantefmup_". $dadosDep->projectos[$i]->id ."'>".$dadosDep->projectos[$i]->montantefmup." €</td>";
            echo "<td id='td_projectos_invres_". $dadosDep->projectos[$i]->id ."'>".checkInv($dadosDep->projectos[$i]->invresponsavel)."</td>";
            echo "<td id='td_projectos_codigo_". $dadosDep->projectos[$i]->id ."'>".$dadosDep->projectos[$i]->codigo."</td>";
            echo "<td id='td_projectos_titulo_". $dadosDep->projectos[$i]->id ."'>".$dadosDep->projectos[$i]->titulo."</td>";
            echo "<td id='td_projectos_dataini_". $dadosDep->projectos[$i]->id ."'>".$dadosDep->projectos[$i]->datainicio."</td>";
            echo "<td id='td_projectos_datafim_". $dadosDep->projectos[$i]->id ."'>".$dadosDep->projectos[$i]->datafim."</td>";
            echo "<td id='td_projectos_link_". $dadosDep->projectos[$i]->id ."'>".$dadosDep->projectos[$i]->link."</td>";
            echo "<td id='td_projectos_estado_". $dadosDep->projectos[$i]->id ."'>".getEstadoProjeto($dadosDep->projectos[$i]->estado)."</td>";
            echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-projectos').text('" . $dadosDep->projectos[$i]->id. "');\"></td>";
            echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-projectos').text('" . $dadosDep->projectos[$i]->id . "');apagarProjecto();return false;\" ></center></td>";
        echo "</tr>";
    }

    echo "</tbody>
    </table>
    <p id='chave-projectos' hidden></p>
</div>";

 	echo "<h3>Número de projetos arbitrados</h3>\n";

 	echo "
    <div id='arbitragensProjectos'>
        <table id='arbprj' class='box-table-b'>
            <thead>
                <tr>
                <th>IDINV</th>
                <th>Nome</th>
                <th>Internacionais</th>
                <th>Nacionais</th>
                </tr>
            </thead>
            <tbody>";
 	
 	
 	foreach ($dadosDep->arbitragensProjetos as $i => $value){
 		if($dadosDep->arbitragensProjetos[$i]->api!=0 || $dadosDep->arbitragensProjetos[$i]->apn!=0){
 		echo "<tr>";
            echo "<td>".$dadosDep->arbitragensProjetos[$i]->idinv."</td>";
            echo "<td>".$dadosDep->investigadores[$dadosDep->arbitragensProjetos[$i]->idinv]->nome."</td>";
            echo "<td id='td_arbprojecto_api_". $dadosDep->arbitragensProjetos[$i]->id ."'>".$dadosDep->arbitragensProjetos[$i]->api."</td>";
            echo "<td id='td_arbprojecto_apn_". $dadosDep->arbitragensProjetos[$i]->id ."'>".$dadosDep->arbitragensProjetos[$i]->apn."</td>";
            echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-arb-projecto').text('" . $dadosDep->arbitragensProjetos[$i]->id . "');\"></td>";
            echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-arb-projecto').text('" . $dadosDep->arbitragensProjetos[$i]->id . "');apagarArbProjecto();return false;\" ></center></td>";
        echo "</tr>";
 		}
 	}

 	echo "</tbody>
 	</table>
 	<p id='chave-arb-projecto' hidden></p>
</div>";
	
	function getTipoEntidade($i) {
		$db = new Database();
		$lValues =$db->getLookupValues("lista_tipoentidadefinanciadora");
		while ($row = mysql_fetch_assoc($lValues)) {	
			if($i==$row["ID"])
				echo $row["DESCRICAO"];
		}
		$db->disconnect();
	}
				
	function checkInv($i){
		global $dadosDep;
		if($i==1)
			return "Sim";
		else 
			return "Não";
	}
	
	function getEstadoProjeto($i) {
		global $dadosDep;
		$db = new Database();
		$lValues =$db->getLookupValues("lista_estadoProjetos");
		$texto="";
		while ($row = mysql_fetch_assoc($lValues)) {
			if($i==$row["ID"])
				$texto=$row["DESCRICAO"];
		}
		$db->disconnect();
		return $texto;
	}
?>