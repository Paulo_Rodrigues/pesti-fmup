<?php


/**
 * Autor: Paulo Rodrigues (prodrigues@med.up.pt/paulomiguelarodrigues@gmail.com)
 * Data: 23/04/2014
 * Alterações: Capacidade de edição e remover registos adicionados.
 */

echo "<h3>Conferências</h3>\n
	  <div id='conferencias'> ";

echo "<table class='box-table-b'>
		<thead>
			<tr>
				<th>IDINV</th>
				<th>Nome</th>
				<th>Âmbito</th>
				<th>Tipo</th>
				<th>Data Início</th>
				<th>Data Fim</th>
				<th>Título</th>
				<th>Local (cidade,país)</th>
				<th>Nº Participantes</th>
				<th>Link</th>
			</tr>
		</thead>
		<tbody>";
			    
    foreach ($dadosDep->conferencias as $i => $value){
			echo "<tr>";
				echo "<td>".$dadosDep->conferencias[$i]->idinv."</td>";
				echo "<td>".$dadosDep->investigadores[$dadosDep->conferencias[$i]->idinv]->nome."</td>";
			   	echo "<td id='td_conferencias_ambito_" . $dadosDep->conferencias[$i]->id . "'>";
				getAmbitoConf($dadosDep->conferencias[$i]->ambito);
				echo "</td>";	
				echo "<td id='td_conferencias_tipo_" . $dadosDep->conferencias[$i]->id . "'>";
				getTipoConf($dadosDep->conferencias[$i]->tipo);
				echo "</td>";
				echo "<td id='td_conferencias_dataini_" . $dadosDep->conferencias[$i]->id . "'>".$dadosDep->conferencias[$i]->datainicio."</td>";	
				echo "<td id='td_conferencias_datafim_" . $dadosDep->conferencias[$i]->id . "'>".$dadosDep->conferencias[$i]->datafim."</td>";	
				echo "<td id='td_conferencias_titulo_" . $dadosDep->conferencias[$i]->id . "'>".$dadosDep->conferencias[$i]->titulo."</td>";
				echo "<td id='td_conferencias_local_" . $dadosDep->conferencias[$i]->id . "'>".$dadosDep->conferencias[$i]->local."</td>";
				echo "<td id='td_conferencias_npart_" . $dadosDep->conferencias[$i]->id . "'>".$dadosDep->conferencias[$i]->participantes."</td>";
				echo "<td id='td_conferencias_link_" . $dadosDep->conferencias[$i]->id . "'>".$dadosDep->conferencias[$i]->link."</td>";
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-conf').text('" . $dadosDep->conferencias[$i]->id. "');\"></td>";
            	echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" name='navOption' onclick=\"$('#chave-conf').text('" . $dadosDep->conferencias[$i]->id . "');apagarConferencia();return false;\" ></center></td>";
			echo "</tr>";	    	
    } 

    echo "</tbody>
    </table>
    <p id='chave-conf' ></p>
    </div>";

	function getTipoConf($i) {	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_tipoconf");
	
		while ($row = mysql_fetch_assoc($lValues)) {	
			if($i==$row["ID"])
				echo $row["DESCRICAO"];
		}

		$db->disconnect();				
	}	
	
	function getAmbitoConf($i) {
		$db = new Database();
		$lValues =$db->getLookupValues("lista_ambitoconf");
		
		while ($row = mysql_fetch_assoc($lValues)) {	
			if($i==$row["ID"])
				echo $row["DESCRICAO"];
		}

		$db->disconnect();				
	}	
				
?>