<?php

/**
* Autor: Paulo Rodrigues (prodrigues@med.up.pt/paulomiguelarodrigues@gmail.com)
* Data: 21/04/2014
* Alterações: Adicionada a capacidade de poder editar e remover registos de unidades de investigação. 
*/

echo "<h3>Unidades de Investigação</h3>\n";
		
echo "<div id='unidadesInvestigacao'>";		
echo "<table id='uniInv' class='box-table-b'>
	    <thead>
		    <tr>
				<th>Id Inv</th><th>Nome</th>
		      	<th>Unidade de Investigação<p><i>Research Unit</i></p></th>
		      	<th>Avaliação 2007<p><i>2007 Evaluation</i></p></th>
		      	<th>Percentagem<p><i>Percentage</i></p></th>
		     	<th>Responsável pela Unidade<p><i>Unit Head</i></p></th>
		    </tr>
	    </thead>
    <tbody>";

    foreach ($dadosDep->unidadesinvestigacao as $i => $value){
		echo "<tr>";
			echo "<td>";
			echo $dadosDep->unidadesinvestigacao[$i]->idinv;	
			echo "</td>";

			echo "<td>".$dadosDep->investigadores[$dadosDep->unidadesinvestigacao[$i]->idinv]->nome."</td>";
			
			echo "<td id='td_uni_inv_unidade_" . $dadosDep->unidadesinvestigacao[$i]->id . "'>";
			echo $dadosDep->unidadesinvestigacao[$i]->unidade;			
			echo "</td>";
			
			echo "<td id='td_uni_inv_ava2007_" . $dadosDep->unidadesinvestigacao[$i]->id . "'>";
			echo $dadosDep->unidadesinvestigacao[$i]->ava2007;
			echo "</td>";
			
			echo "<td id='td_uni_inv_perc_" . $dadosDep->unidadesinvestigacao[$i]->id . "'>";
			echo $dadosDep->unidadesinvestigacao[$i]->percentagem;
			echo "</td>";
			
			echo "<td id='td_uni_inv_resp_" . $dadosDep->unidadesinvestigacao[$i]->id . "'>";
			if ($dadosDep->unidadesinvestigacao[$i]->responsavel==1) {
				echo "Sim";
			}else {
				echo "Não";
			}
	    	echo "</td>";

			echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-uniInv').text('" . $dadosDep->unidadesinvestigacao[$i]->id . "');\"></td>";
	    	echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep->unidadesinvestigacao[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-uniInv').text('" . $dadosDep->unidadesinvestigacao[$i]->id . "');apagarUniInv();return false;\" ></center></td>";
			    	
    	echo "</tr>";
    }

    echo "</tbody></table>
    <p id='chave-uniInv' hidden></p>";
	echo "</div>";
?>