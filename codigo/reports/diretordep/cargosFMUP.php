<?php

echo "<p><fieldset class='normal'>\n";
	echo "<legend>Cargos - FMUP<i>Positions at FMUP </i></legend>\n";

    echo "
    <table  class='box-table-b'>
  	<tbody>
    <!-- Results table headers -->
    <tr>

      <th>Orgão<p><i>Body</i></p></th>
      <th>Cargo<p><i>Position</i></p></th>
      <th>Data Início<p><i>Start Date</i></p></th>
      <th>Data Fim<p><i>End Date</i></p></th>
    </tr>";
			    
    foreach ($questionario->cargosFMUP as $i => $value){
    	   
			
			echo "<tr>";
				echo "<td>".$questionario->cargosFMUP[$i]->orgao."</td>";
				echo "<td>".$questionario->cargosFMUP[$i]->cargo."</td>";
				echo "<td>".$questionario->cargosFMUP[$i]->datainicio."</td>";
				echo "<td>".$questionario->cargosFMUP[$i]->datafim."</td>";
			echo "</tr>";

    }
    
    echo "</tbody></table>";

echo "</fieldset>";

	?>