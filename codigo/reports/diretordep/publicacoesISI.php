<?php

/**
* Autor: Paulo Rodrigues (prodrigues@med.up.pt/paulomiguelarodrigues@gmail.com)
* Data: 21/04/2014
* Alterações: Alterados os cabeçalhos das informações para poderem surgir no accordion. 
*/

echo "<h3>Publicações ISI</h3>\n";
echo "<div id='publicacoesISI'>";

$outJISI="";
$outJAISI="";
$outPRPISI="";
$outPRPNISI="";
$outCPISI="";
$outCPNISI="";
$outOAISI="";
$outSISI="";
$outJNISI="";
$outSNISI="";
$outLNISI="";
$outCLNISI="";
$outLISI="";
$outCLISI="";
$outPISI="";
$outAISI="";
$outRestoISI="";

$tcols="<thead>
		    <tr>
		    	<th>ID</th>
				<th>Nome</th>
				<th>Ano</th>
				<th>Nome Publicação</th>
				<th>ISSN</th>
				<th>ISBN</th>
				<th>Título</th>
				<th>Autores</th>
				<th>Volume</th>
				<th>Issue</th>
				<th>AR</th>
				<th>Colecao</th>
				<th>1ª Pág.</th>
				<th>Últ. Pág.</th>
				<th>Factor de Impacto</th>
				<th>Citações</th>
				<th>Patentes</th>
				<th>Data Patente</th>
				<th>IPC</th>
				<th>Estado</th>
			</tr>
		</thead>";

$tcols_all="<thead>
			    <tr>
			    	<th>ID</th>
			    	<th>Nome</th>
					<th>Ano</th>
					<th>Tipo</th>
					<th>Nome Publicação</th>
					<th>ISSN</th>
					<th>ISSN_LINKING</th>
					<th>ISSN_ELECTRONIC</th>
					<th>ISBN</th>
					<th>Título</th>
					<th>Autores</th>
					<th>VOLUME</th>
					<th>ISSUE</th>
					<th>AR</th>
					<th>Coleção</th>
					<th>1ª Pág.</th>
					<th>Últ. Pág.</th>
					<th>Factor Impacto</th>
					<th>Citações</th>
					<th>Patentes</th>
					<th>Data Patente</th>
					<th>IPC</th>
					<th>Area</th>
					<th>Estado</th>
				</tr>
			</thead>";

$tcolsJISI="<thead>
		    	<tr>
					<th>IDINV</th>
					<th>Nome</th>
					<th>Revista</th>
					<th>ISSN</th>
					<th>Título</th>
					<th>Autores</th>
					<th>Volume</th>
					<th>Issue</th>
					<th>1ª Pág.</th>
					<th>Última Pág.</th>
					<th>Citações</th>
					<th>Estado</th>
				</tr>
		</thead>";

$tcolsCONFISI="<thead>
			    <tr>
					<th>IDINV</th>
					<th>Nome</th>
					<th>ISBN</th>
					<th>Nome Publicação</th>
					<th>Editores</th>
					<th>Coleção</th>
					<th>Volume</th>
					<th>Título do Artigo</th>
					<th>Autores</th>
					<th>1ª Pág.</th>
					<th>Última Página</th>
					<th>Estado</th>
				</tr>
			</thead>";

$tcolsCLIVISI="<thead>
			    <tr>
					<th>ISBN</th>
					<th>Nome Publicação</th>
					<th>Editora</th>		
					<th>Autores</th>
					<th>Título Artigo</th>
					<th>1ª Pág.</th>
					<th>Última Pág.</th>
					<th>Estado</th>
				</tr>
			</thead>";

$tcolsPATISI="<thead>
			    <tr>
					<th>IDINV</th>
					<th>Nome</th>
					<th>Nº Patente</th>
					<th>IPC</th>
					<th>Título</th>
					<th>Autores</th>		
					<th>Data da Patente</th>
					<th>Estado</th>
				</tr>
			</thead>";

$tcolsLIVISI="<thead>
			    <tr>
					<th>IDINV</th>
					<th>Nome</th>
					<th>ISBN</th>
					<th>Título</th>
					<th>Autores</th>		
					<th>Editor</th>
					<th>Estado</th>
				</tr>
			</thead>";

	foreach ($dadosDep->publicacoesISI as $i => $value){
			
			$copia=0;
		
			$tipo=$dadosDep->publicacoesISI[$i]->tipofmup;
			
			if($dadosDep->publicacoesISI[$i]->estado==0)
				$estado="Publicado/Published";
			else
				$estado="Em Revisão ou no Prelo/In Review or In Press";
						
			switch($tipo){
				case 'J': { 

		    		$outJISI=$outJISI."<tr>";
						$outJISI=$outJISI."<td>";
						$outJISI=$outJISI.$dadosDep->publicacoesISI[$i]->idinv;
			    		$outJISI=$outJISI."</td>";

			    		$outJISI=$outJISI."<td>";
			    		$outJISI=$outJISI.$dadosDep->investigadores[$dadosDep->publicacoesISI[$i]->idinv]->nome;
			    		$outJISI=$outJISI."</td>";	

			    		$outJISI=$outJISI."<td>";
						$outJISI=$outJISI.$dadosDep->publicacoesISI[$i]->nomepublicacao;
			    		$outJISI=$outJISI."</td>";

			    		$outJISI=$outJISI."<td>";
						$outJISI=$outJISI.$dadosDep->publicacoesISI[$i]->issn;
			    		$outJISI=$outJISI."</td>";	

			    		$outJISI=$outJISI."<td>";
						$outJISI=$outJISI.$dadosDep->publicacoesISI[$i]->titulo;
			    		$outJISI=$outJISI."</td>";

			    		$outJISI=$outJISI."<td>";
						$outJISI=$outJISI.$dadosDep->publicacoesISI[$i]->autores;
			    		$outJISI=$outJISI."</td>";

						$outJISI=$outJISI."<td>";
						$outJISI=$outJISI.$dadosDep->publicacoesISI[$i]->volume;
			    		$outJISI=$outJISI."</td>";

						$outJISI=$outJISI."<td>";
						$outJISI=$outJISI.$dadosDep->publicacoesISI[$i]->issue;
			    		$outJISI=$outJISI."</td>";

						$outJISI=$outJISI."<td>";
						$outJISI=$outJISI.$dadosDep->publicacoesISI[$i]->primpagina;
			    		$outJISI=$outJISI."</td>";

						$outJISI=$outJISI."<td>";
						$outJISI=$outJISI.$dadosDep->publicacoesISI[$i]->ultpagina;
			    		$outJISI=$outJISI."</td>";
			    		
			    		$outJISI=$outJISI."<td>";
						$outJISI=$outJISI.$dadosDep->publicacoesISI[$i]->citacoes;
			    		$outJISI=$outJISI."</td>";

			    		$outJISI=$outJISI."<td>";
			    		$outJISI=$outJISI.$estado;
			    		$outJISI=$outJISI."</td>";
			    		
   		    		$outJISI=$outJISI."</tr>";
				}
				break;
				case 'PRP': { // Opção de entrada
		    		$outPRPISI=$outPRPISI."<tr>";
						$outPRPISI=$outPRPISI."<td>";
						$outPRPISI=$outPRPISI.$dadosDep->publicacoesISI[$i]->idinv;
			    		$outPRPISI=$outPRPISI."</td>";

			    		$outPRPISI=$outPRPISI."<td>";
			    		$outPRPISI=$outPRPISI.$dadosDep->investigadores[$dadosDep->publicacoesISI[$i]->idinv]->nome;
			    		$outPRPISI=$outPRPISI."</td>";

			    		$outPRPISI=$outPRPISI."<td>";
						$outPRPISI=$outPRPISI.$dadosDep->publicacoesISI[$i]->nomepublicacao;
			    		$outPRPISI=$outPRPISI."</td>";

			    		$outPRPISI=$outPRPISI."<td>";
						$outPRPISI=$outPRPISI.$dadosDep->publicacoesISI[$i]->issn;
			    		$outPRPISI=$outPRPISI."</td>";

			    		$outPRPISI=$outPRPISI."<td>";
						$outPRPISI=$outPRPISI.$dadosDep->publicacoesISI[$i]->titulo;
			    		$outPRPISI=$outPRPISI."</td>";

			    		$outPRPISI=$outPRPISI."<td>";
						$outPRPISI=$outPRPISI.$dadosDep->publicacoesISI[$i]->autores;
			    		$outPRPISI=$outPRPISI."</td>";

						$outPRPISI=$outPRPISI."<td>";
						$outPRPISI=$outPRPISI.$dadosDep->publicacoesISI[$i]->volume;
			    		$outPRPISI=$outPRPISI."</td>";

						$outPRPISI=$outPRPISI."<td>";
						$outPRPISI=$outPRPISI.$dadosDep->publicacoesISI[$i]->issue;
			    		$outPRPISI=$outPRPISI."</td>";

						$outPRPISI=$outPRPISI."<td>";
						$outPRPISI=$outPRPISI.$dadosDep->publicacoesISI[$i]->primpagina;
			    		$outPRPISI=$outPRPISI."</td>";	

						$outPRPISI=$outPRPISI."<td>";
						$outPRPISI=$outPRPISI.$dadosDep->publicacoesISI[$i]->ultpagina;
			    		$outPRPISI=$outPRPISI."</td>";

			    		$outPRPISI=$outPRPISI."<td>";
						$outPRPISI=$outPRPISI.$dadosDep->publicacoesISI[$i]->citacoes;
			    		$outPRPISI=$outPRPISI."</td>";

			    		$outPRPISI=$outPRPISI."<td>";
			    		$outPRPISI=$outPRPISI.$estado;
			    		$outPRPISI=$outPRPISI."</td>";

		    		$outPRPISI=$outPRPISI."</tr>";
				}
				break;
				case 'JA': { // Opção de entrada
		    		$outJAISI=$outJAISI."<tr>";
						$outJAISI=$outJAISI."<td>";
						$outJAISI=$outJAISI.$dadosDep->publicacoesISI[$i]->idinv;
			    		$outJAISI=$outJAISI."</td>";

			    		$outJAISI=$outJAISI."<td>";
			    		$outJAISI=$outJAISI.$dadosDep->investigadores[$dadosDep->publicacoesISI[$i]->idinv]->nome;
			    		$outJAISI=$outJAISI."</td>";

			    		$outJAISI=$outJAISI."<td>";
						$outJAISI=$outJAISI.$dadosDep->publicacoesISI[$i]->nomepublicacao;
			    		$outJAISI=$outJAISI."</td>";

			    		$outJAISI=$outJAISI."<td>";
						$outJAISI=$outJAISI.$dadosDep->publicacoesISI[$i]->issn;
			    		$outJAISI=$outJAISI."</td>";

			    		$outJAISI=$outJAISI."<td>";
						$outJAISI=$outJAISI.$dadosDep->publicacoesISI[$i]->titulo;
			    		$outJAISI=$outJAISI."</td>";

			    		$outJAISI=$outJAISI."<td>";
						$outJAISI=$outJAISI.$dadosDep->publicacoesISI[$i]->autores;
			    		$outJAISI=$outJAISI."</td>";

						$outJAISI=$outJAISI."<td>";
						$outJAISI=$outJAISI.$dadosDep->publicacoesISI[$i]->volume;
			    		$outJAISI=$outJAISI."</td>";

						$outJAISI=$outJAISI."<td>";
						$outJAISI=$outJAISI.$dadosDep->publicacoesISI[$i]->issue;
			    		$outJAISI=$outJAISI."</td>";

						$outJAISI=$outJAISI."<td>";
						$outJAISI=$outJAISI.$dadosDep->publicacoesISI[$i]->primpagina;
			    		$outJAISI=$outJAISI."</td>";

						$outJAISI=$outJAISI."<td>";
						$outJAISI=$outJAISI.$dadosDep->publicacoesISI[$i]->ultpagina;
			    		$outJAISI=$outJAISI."</td>";

			    		$outJAISI=$outJAISI."<td>";
						$outJAISI=$outJAISI.$dadosDep->publicacoesISI[$i]->citacoes;
			    		$outJAISI=$outJAISI."</td>";

			    		$outJAISI=$outJAISI."<td>";
			    		$outJAISI=$outJAISI.$estado;
			    		$outJAISI=$outJAISI."</td>";
		    		$outJAISI=$outJAISI."</tr>";
				}
				break;
				case 'PRPN': { // Opção de entrada
		    		$outPRPNISI=$outPRPNISI."<tr>";

						$outPRPNISI=$outPRPNISI."<td>";
						$outPRPNISI=$outPRPNISI.$dadosDep->publicacoesISI[$i]->idinv;
			    		$outPRPNISI=$outPRPNISI."</td>";

			    		$outPRPNISI=$outPRPNISI."<td>";
			    		$outPRPNISI=$outPRPNISI.$dadosDep->investigadores[$dadosDep->publicacoesISI[$i]->idinv]->nome;
			    		$outPRPNISI=$outPRPNISI."</td>";

			    		$outPRPNISI=$outPRPNISI."<td>";
						$outPRPNISI=$outPRPNISI.$dadosDep->publicacoesISI[$i]->ano;
			    		$outPRPNISI=$outPRPNISI."</td>";

			    		$outPRPNISI=$outPRPNISI."<td>";
						$outPRPNISI=$outPRPNISI.$dadosDep->publicacoesISI[$i]->nomepublicacao;
			    		$outPRPNISI=$outPRPNISI."</td>";

			    		$outPRPNISI=$outPRPNISI."<td>";
						$outPRPNISI=$outPRPNISI.$dadosDep->publicacoesISI[$i]->issn;
			    		$outPRPNISI=$outPRPNISI."</td>";

			    		$outPRPNISI=$outPRPNISI."<td>";
						$outPRPNISI=$outPRPNISI.$dadosDep->publicacoesISI[$i]->isbn;
			    		$outPRPNISI=$outPRPNISI."</td>";

			    		$outPRPNISI=$outPRPNISI."<td>";
						$outPRPNISI=$outPRPNISI.$dadosDep->publicacoesISI[$i]->titulo;
			    		$outPRPNISI=$outPRPNISI."</td>";

			    		$outPRPNISI=$outPRPNISI."<td>";
						$outPRPNISI=$outPRPNISI.$dadosDep->publicacoesISI[$i]->autores;
			    		$outPRPNISI=$outPRPNISI."</td>";

						$outPRPNISI=$outPRPNISI."<td>";
						$outPRPNISI=$outPRPNISI.$dadosDep->publicacoesISI[$i]->volume;
			    		$outPRPNISI=$outPRPNISI."</td>";

						$outPRPNISI=$outPRPNISI."<td>";
						$outPRPNISI=$outPRPNISI.$dadosDep->publicacoesISI[$i]->issue;
			    		$outPRPNISI=$outPRPNISI."</td>";

						$outPRPNISI=$outPRPNISI."<td>";
						$outPRPNISI=$outPRPNISI.$dadosDep->publicacoesISI[$i]->ar;
			    		$outPRPNISI=$outPRPNISI."</td>";

						$outPRPNISI=$outPRPNISI."<td>";
						$outPRPNISI=$outPRPNISI.$dadosDep->publicacoesISI[$i]->colecao;
			    		$outPRPNISI=$outPRPNISI."</td>";

						$outPRPNISI=$outPRPNISI."<td>";
						$outPRPNISI=$outPRPNISI.$dadosDep->publicacoesISI[$i]->primpagina;
			    		$outPRPNISI=$outPRPNISI."</td>";	

						$outPRPNISI=$outPRPNISI."<td>";
						$outPRPNISI=$outPRPNISI.$dadosDep->publicacoesISI[$i]->ultpagina;
			    		$outPRPNISI=$outPRPNISI."</td>";

			    		$outPRPNISI=$outPRPNISI."<td>";
						$outPRPNISI=$outPRPNISI.$dadosDep->publicacoesISI[$i]->citacoes;
			    		$outPRPNISI=$outPRPNISI."</td>";

			    		$outPRPNISI=$outPRPNISI."<td>";
						$outPRPNISI=$outPRPNISI.$dadosDep->publicacoesISI[$i]->conftitle;
			    		$outPRPNISI=$outPRPNISI."</td>";

			    		$outPRPNISI=$outPRPNISI."<td>";
						$outPRPNISI=$outPRPNISI.$dadosDep->publicacoesISI[$i]->language;
			    		$outPRPNISI=$outPRPNISI."</td>";

			    		$outPRPNISI=$outPRPNISI."<td>";
						$outPRPNISI=$outPRPNISI.$dadosDep->publicacoesISI[$i]->editor;
			    		$outPRPNISI=$outPRPNISI."</td>";

			    		$outPRPNISI=$outPRPNISI."<td>";
			    		$outPRPNISI=$outPRPNISI.$estado;
			    		$outPRPNISI=$outPRPNISI."</td>";

		    		$outPRPNISI=$outPRPNISI."</tr>";
				}
				break;
				case 'CP': { // Opção de entrada
		    		$outCPISI=$outCPISI."<tr>";
			    		$outCPISI=$outCPISI."<td>";
			    		$outCPISI=$outCPISI.$dadosDep->publicacoesISI[$i]->idinv;
			    		$outCPISI=$outCPISI."</td>";

			    		$outCPISI=$outCPISI."<td>";
			    		$outCPISI=$outCPISI.$dadosDep->investigadores[$dadosDep->publicacoesISI[$i]->idinv]->nome;
			    		$outCPISI=$outCPISI."</td>";

			    		$outCPISI=$outCPISI."<td>";
			    		$outCPISI=$outCPISI.$dadosDep->publicacoesISI[$i]->isbn;
			    		$outCPISI=$outCPISI."</td>";

			    		$outCPISI=$outCPISI."<td>";
						$outCPISI=$outCPISI.$dadosDep->publicacoesISI[$i]->nomepublicacao;
			    		$outCPISI=$outCPISI."</td>";

			    		$outCPISI=$outCPISI."<td>";
						$outCPISI=$outCPISI.$dadosDep->publicacoesISI[$i]->editor;
			    		$outCPISI=$outCPISI."</td>";

						$outCPISI=$outCPISI."<td>";
						$outCPISI=$outCPISI.$dadosDep->publicacoesISI[$i]->colecao;
			    		$outCPISI=$outCPISI."</td>";

						$outCPISI=$outCPISI."<td>";
						$outCPISI=$outCPISI.$dadosDep->publicacoesISI[$i]->volume;
			    		$outCPISI=$outCPISI."</td>";

			    		$outCPISI=$outCPISI."<td>";
						$outCPISI=$outCPISI.$dadosDep->publicacoesISI[$i]->titulo;
			    		$outCPISI=$outCPISI."</td>";

			    		$outCPISI=$outCPISI."<td>";
						$outCPISI=$outCPISI.$dadosDep->publicacoesISI[$i]->autores;
			    		$outCPISI=$outCPISI."</td>";

						$outCPISI=$outCPISI."<td>";
						$outCPISI=$outCPISI.$dadosDep->publicacoesISI[$i]->primpagina;
			    		$outCPISI=$outCPISI."</td>";

						$outCPISI=$outCPISI."<td>";
						$outCPISI=$outCPISI.$dadosDep->publicacoesISI[$i]->ultpagina;
			    		$outCPISI=$outCPISI."</td>";			    		
			    		
	    				$outCPISI=$outCPISI."<td>";
			    		$outCPISI=$outCPISI.$estado;
			    		$outCPISI=$outCPISI."</td>";
		    		$outCPISI=$outCPISI."</tr>";
				}
				break;
				case 'CPN': { // Opção de entrada
		    		$outCPNISI=$outCPNISI."<tr>";
						$outCPNISI=$outCPNISI."<td>";
						$outCPNISI=$outCPNISI.$dadosDep->publicacoesISI[$i]->idinv;
			    		$outCPNISI=$outCPNISI."</td>";

			    		$outCPNISI=$outCPNISI."<td>";
			    		$outCPNISI=$outCPNISI.$dadosDep->investigadores[$dadosDep->publicacoesISI[$i]->idinv]->nome;
			    		$outCPNISI=$outCPNISI."</td>";

			    		$outCPNISI=$outCPNISI."<td>";
						$outCPNISI=$outCPNISI.$dadosDep->publicacoesISI[$i]->ano;
			    		$outCPNISI=$outCPNISI."</td>";	

			    		$outCPNISI=$outCPNISI."<td>";
						$outCPNISI=$outCPNISI.$dadosDep->publicacoesISI[$i]->nomepublicacao;
			    		$outCPNISI=$outCPNISI."</td>";

			    		$outCPNISI=$outCPNISI."<td>";
						$outCPNISI=$outCPNISI.$dadosDep->publicacoesISI[$i]->issn;
			    		$outCPNISI=$outCPNISI."</td>";
			    		
			    		$outCPNISI=$outCPNISI."<td>";
						$outCPNISI=$outCPNISI.$dadosDep->publicacoesISI[$i]->isbn;
			    		$outCPNISI=$outCPNISI."</td>";

			    		$outCPNISI=$outCPNISI."<td>";
						$outCPNISI=$outCPNISI.$dadosDep->publicacoesISI[$i]->titulo;
			    		$outCPNISI=$outCPNISI."</td>";

			    		$outCPNISI=$outCPNISI."<td>";
						$outCPNISI=$outCPNISI.$dadosDep->publicacoesISI[$i]->autores;
			    		$outCPNISI=$outCPNISI."</td>";

						$outCPNISI=$outCPNISI."<td>";
						$outCPNISI=$outCPNISI.$dadosDep->publicacoesISI[$i]->volume;
			    		$outCPNISI=$outCPNISI."</td>";

						$outCPNISI=$outCPNISI."<td>";
						$outCPNISI=$outCPNISI.$dadosDep->publicacoesISI[$i]->issue;
			    		$outCPNISI=$outCPNISI."</td>";

						$outCPNISI=$outCPNISI."<td>";
						$outCPNISI=$outCPNISI.$dadosDep->publicacoesISI[$i]->ar;
			    		$outCPNISI=$outCPNISI."</td>";

						$outCPNISI=$outCPNISI."<td>";
						$outCPNISI=$outCPNISI.$dadosDep->publicacoesISI[$i]->colecao;
			    		$outCPNISI=$outCPNISI."</td>";

						$outCPNISI=$outCPNISI."<td>";
						$outCPNISI=$outCPNISI.$dadosDep->publicacoesISI[$i]->primpagina;
			    		$outCPNISI=$outCPNISI."</td>";	

						$outCPNISI=$outCPNISI."<td>";
						$outCPNISI=$outCPNISI.$dadosDep->publicacoesISI[$i]->ultpagina;
			    		$outCPNISI=$outCPNISI."</td>";
			    		
			    		$outCPNISI=$outCPNISI."<td>";
						$outCPNISI=$outCPNISI.$dadosDep->publicacoesISI[$i]->citacoes;
			    		$outCPNISI=$outCPNISI."</td>";

			    		$outCPNISI=$outCPNISI."<td>";
						$outCPNISI=$outCPNISI.$dadosDep->publicacoesISI[$i]->conftitle;
			    		$outCPNISI=$outCPNISI."</td>";

			    		$outCPNISI=$outCPNISI."<td>";
						$outCPNISI=$outCPNISI.$dadosDep->publicacoesISI[$i]->language;
			    		$outCPNISI=$outCPNISI."</td>";

			    		$outCPNISI=$outCPNISI."<td>";
						$outCPNISI=$outCPNISI.$dadosDep->publicacoesISI[$i]->editor;
			    		$outCPNISI=$outCPNISI."</td>";	

		    		$outCPNISI=$outCPNISI."</tr>";
				}
				break;
				case 'OA': { // Opção de entrada
		    		$outOAISI=$outOAISI."<tr>";
			    		$outOAISI=$outOAISI."<td>";
			    		$outOAISI=$outOAISI.$dadosDep->publicacoesISI[$i]->idinv;
			    		$outOAISI=$outOAISI."</td>";

			    		$outOAISI=$outOAISI."<td>";
			    		$outOAISI=$outOAISI.$dadosDep->investigadores[$dadosDep->publicacoesISI[$i]->idinv]->nome;
			    		$outOAISI=$outOAISI."</td>";

			    		$outOAISI=$outOAISI."<td>";
						$outOAISI=$outOAISI.$dadosDep->publicacoesISI[$i]->nomepublicacao;
			    		$outOAISI=$outOAISI."</td>";

			    		$outOAISI=$outOAISI."<td>";
						$outOAISI=$outOAISI.$dadosDep->publicacoesISI[$i]->isbn;
			    		$outOAISI=$outOAISI."</td>";

			    		$outOAISI=$outOAISI."<td>";
						$outOAISI=$outOAISI.$dadosDep->publicacoesISI[$i]->editor;
			    		$outOAISI=$outOAISI."</td>";

						$outOAISI=$outOAISI."<td>";
						$outOAISI=$outOAISI.$dadosDep->publicacoesISI[$i]->colecao;
			    		$outOAISI=$outOAISI."</td>";

						$outOAISI=$outOAISI."<td>";
						$outOAISI=$outOAISI.$dadosDep->publicacoesISI[$i]->volume;
			    		$outOAISI=$outOAISI."</td>";

			    		$outOAISI=$outOAISI."<td>";
						$outOAISI=$outOAISI.$dadosDep->publicacoesISI[$i]->titulo;
			    		$outOAISI=$outOAISI."</td>";

			    		$outOAISI=$outOAISI."<td>";
						$outOAISI=$outOAISI.$dadosDep->publicacoesISI[$i]->autores;
			    		$outOAISI=$outOAISI."</td>";

						$outOAISI=$outOAISI."<td>";
						$outOAISI=$outOAISI.$dadosDep->publicacoesISI[$i]->primpagina;
			    		$outOAISI=$outOAISI."</td>";

						$outOAISI=$outOAISI."<td>";
						$outOAISI=$outOAISI.$dadosDep->publicacoesISI[$i]->ultpagina;
			    		$outOAISI=$outOAISI."</td>";

			    		$outOAISI=$outOAISI."<td>";
			    		$outOAISI=$outOAISI.$estado;
			    		$outOAISI=$outOAISI."</td>";			    		
		    		$outOAISI=$outOAISI."</tr>";
				}
				break;
				case 'S': { // Opção de entrada
		    		$outSISI=$outSISI."<tr>";
						$outSISI=$outSISI."<td>";
						$outSISI=$outSISI.$dadosDep->publicacoesISI[$i]->idinv;
			    		$outSISI=$outSISI."</td>";

			    		$outSISI=$outSISI."<td>";
			    		$outSISI=$outSISI.$dadosDep->investigadores[$dadosDep->publicacoesISI[$i]->idinv]->nome;
			    		$outSISI=$outSISI."</td>";

			    		$outSISI=$outSISI."<td>";
						$outSISI=$outSISI.$dadosDep->publicacoesISI[$i]->ano;
			    		$outSISI=$outSISI."</td>";
			    		
			    		$outSISI=$outSISI."<td>";
						$outSISI=$outSISI.$dadosDep->publicacoesISI[$i]->nomepublicacao;
			    		$outSISI=$outSISI."</td>";

			    		$outSISI=$outSISI."<td>";
						$outSISI=$outSISI.$dadosDep->publicacoesISI[$i]->issn;
			    		$outSISI=$outSISI."</td>";
			    		
			    		$outSISI=$outSISI."<td>";
						$outSISI=$outSISI.$dadosDep->publicacoesISI[$i]->isbn;
			    		$outSISI=$outSISI."</td>";

			    		$outSISI=$outSISI."<td>";
						$outSISI=$outSISI.$dadosDep->publicacoesISI[$i]->titulo;
			    		$outSISI=$outSISI."</td>";

			    		$outSISI=$outSISI."<td>";
						$outSISI=$outSISI.$dadosDep->publicacoesISI[$i]->autores;
			    		$outSISI=$outSISI."</td>";

						$outSISI=$outSISI."<td>";
						$outSISI=$outSISI.$dadosDep->publicacoesISI[$i]->volume;
			    		$outSISI=$outSISI."</td>";

						$outSISI=$outSISI."<td>";
						$outSISI=$outSISI.$dadosDep->publicacoesISI[$i]->issue;
			    		$outSISI=$outSISI."</td>";

						$outSISI=$outSISI."<td>";
						$outSISI=$outSISI.$dadosDep->publicacoesISI[$i]->ar;
			    		$outSISI=$outSISI."</td>";

						$outSISI=$outSISI."<td>";
						$outSISI=$outSISI.$dadosDep->publicacoesISI[$i]->colecao;
			    		$outSISI=$outSISI."</td>";

						$outSISI=$outSISI."<td>";
						$outSISI=$outSISI.$dadosDep->publicacoesISI[$i]->primpagina;
			    		$outSISI=$outSISI."</td>";	

						$outSISI=$outSISI."<td>";
						$outSISI=$outSISI.$dadosDep->publicacoesISI[$i]->ultpagina;
			    		$outSISI=$outSISI."</td>";
			    		
			    		$outSISI=$outSISI."<td>";
						$outSISI=$outSISI.$dadosDep->publicacoesISI[$i]->citacoes;
			    		$outSISI=$outSISI."</td>";

			    		$outSISI=$outSISI."<td>";
						$outSISI=$outSISI.$dadosDep->publicacoesISI[$i]->conftitle;
			    		$outSISI=$outSISI."</td>";

			    		$outSISI=$outSISI."<td>";
						$outSISI=$outSISI.$dadosDep->publicacoesISI[$i]->language;
			    		$outSISI=$outSISI."</td>";

			    		$outSISI=$outSISI."<td>";
						$outSISI=$outSISI.$dadosDep->publicacoesISI[$i]->editor;
			    		$outSISI=$outSISI."</td>";		    		  				    		
		    		$outSISI=$outSISI."</tr>";
				}
				break;
				case 'JN': { // Opção de entrada
		    		$outJNISI=$outJNISI."<tr>";
						$outJNISI=$outJNISI."<td>";
						$outJNISI=$outJNISI.$dadosDep->publicacoesISI[$i]->idinv;
			    		$outJNISI=$outJNISI."</td>";

			    		$outJNISI=$outJNISI."<td>";
			    		$outJNISI=$outJNISI.$dadosDep->investigadores[$dadosDep->publicacoesISI[$i]->idinv]->nome;
			    		$outJNISI=$outJNISI."</td>";
			    		$outJNISI=$outJNISI."<td>";
						$outJNISI=$outJNISI.$dadosDep->publicacoesISI[$i]->ano;
			    		$outJNISI=$outJNISI."</td>";
			    		
			    		$outJNISI=$outJNISI."<td>";
						$outJNISI=$outJNISI.$dadosDep->publicacoesISI[$i]->nomepublicacao;
			    		$outJNISI=$outJNISI."</td>";

			    		$outJNISI=$outJNISI."<td>";
						$outJNISI=$outJNISI.$dadosDep->publicacoesISI[$i]->issn;
			    		$outJNISI=$outJNISI."</td>";
			    		
			    		$outJNISI=$outJNISI."<td>";
						$outJNISI=$outJNISI.$dadosDep->publicacoesISI[$i]->isbn;
			    		$outJNISI=$outJNISI."</td>";

			    		$outJNISI=$outJNISI."<td>";
						$outJNISI=$outJNISI.$dadosDep->publicacoesISI[$i]->titulo;
			    		$outJNISI=$outJNISI."</td>";

			    		$outJNISI=$outJNISI."<td>";
						$outJNISI=$outJNISI.$dadosDep->publicacoesISI[$i]->autores;
			    		$outJNISI=$outJNISI."</td>";

						$outJNISI=$outJNISI."<td>";
						$outJNISI=$outJNISI.$dadosDep->publicacoesISI[$i]->volume;
			    		$outJNISI=$outJNISI."</td>";

						$outJNISI=$outJNISI."<td>";
						$outJNISI=$outJNISI.$dadosDep->publicacoesISI[$i]->issue;
			    		$outJNISI=$outJNISI."</td>";

						$outJNISI=$outJNISI."<td>";
						$outJNISI=$outJNISI.$dadosDep->publicacoesISI[$i]->ar;
			    		$outJNISI=$outJNISI."</td>";

						$outJNISI=$outJNISI."<td>";
						$outJNISI=$outJNISI.$dadosDep->publicacoesISI[$i]->colecao;
			    		$outJNISI=$outJNISI."</td>";

						$outJNISI=$outJNISI."<td>";
						$outJNISI=$outJNISI.$dadosDep->publicacoesISI[$i]->primpagina;
			    		$outJNISI=$outJNISI."</td>";

						$outJNISI=$outJNISI."<td>";
						$outJNISI=$outJNISI.$dadosDep->publicacoesISI[$i]->ultpagina;
			    		$outJNISI=$outJNISI."</td>";			    		

			    		$outJNISI=$outJNISI."<td>";
						$outJNISI=$outJNISI.$dadosDep->publicacoesISI[$i]->citacoes;
			    		$outJNISI=$outJNISI."</td>";

			    		$outJNISI=$outJNISI."<td>";
						$outJNISI=$outJNISI.$dadosDep->publicacoesISI[$i]->conftitle;
			    		$outJNISI=$outJNISI."</td>";

			    		$outJNISI=$outJNISI."<td>";
						$outJNISI=$outJNISI.$dadosDep->publicacoesISI[$i]->language;
			    		$outJNISI=$outJNISI."</td>";

			    		$outJNISI=$outJNISI."<td>";
						$outJNISI=$outJNISI.$dadosDep->publicacoesISI[$i]->editor;
			    		$outJNISI=$outJNISI."</td>";

		    		$outJNISI=$outJNISI."</tr>";
				}
				break;
				case 'SN': { // Opção de entrada
		    		$outSNISI=$outSNISI."<tr>";
						$outSNISI=$outSNISI."<td>";
						$outSNISI=$outSNISI.$dadosDep->publicacoesISI[$i]->idinv;
			    		$outSNISI=$outSNISI."</td>";

			    		$outSNISI=$outSNISI."<td>";
			    		$outSNISI=$outSNISI.$dadosDep->investigadores[$dadosDep->publicacoesISI[$i]->idinv]->nome;
			    		$outSNISI=$outSNISI."</td>";

			    		$outSNISI=$outSNISI."<td>";
						$outSNISI=$outSNISI.$dadosDep->publicacoesISI[$i]->ano;
			    		$outSNISI=$outSNISI."</td>";

			    		$outSNISI=$outSNISI."<td>";
						$outSNISI=$outSNISI.$dadosDep->publicacoesISI[$i]->nomepublicacao;
			    		$outSNISI=$outSNISI."</td>";

			    		$outSNISI=$outSNISI."<td>";
						$outSNISI=$outSNISI.$dadosDep->publicacoesISI[$i]->issn;
			    		$outSNISI=$outSNISI."</td>";
			    		
			    		$outSNISI=$outSNISI."<td>";
						$outSNISI=$outSNISI.$dadosDep->publicacoesISI[$i]->isbn;
			    		$outSNISI=$outSNISI."</td>";

			    		$outSNISI=$outSNISI."<td>";
						$outSNISI=$outSNISI.$dadosDep->publicacoesISI[$i]->titulo;
			    		$outSNISI=$outSNISI."</td>";

			    		$outSNISI=$outSNISI."<td>";
						$outSNISI=$outSNISI.$dadosDep->publicacoesISI[$i]->autores;
			    		$outSNISI=$outSNISI."</td>";

						$outSNISI=$outSNISI."<td>";
						$outSNISI=$outSNISI.$dadosDep->publicacoesISI[$i]->volume;
			    		$outSNISI=$outSNISI."</td>";

						$outSNISI=$outSNISI."<td>";
						$outSNISI=$outSNISI.$dadosDep->publicacoesISI[$i]->issue;
			    		$outSNISI=$outSNISI."</td>";

						$outSNISI=$outSNISI."<td>";
						$outSNISI=$outSNISI.$dadosDep->publicacoesISI[$i]->ar;
			    		$outSNISI=$outSNISI."</td>";

						$outSNISI=$outSNISI."<td>";
						$outSNISI=$outSNISI.$dadosDep->publicacoesISI[$i]->colecao;
			    		$outSNISI=$outSNISI."</td>";

						$outSNISI=$outSNISI."<td>";
						$outSNISI=$outSNISI.$dadosDep->publicacoesISI[$i]->primpagina;
			    		$outSNISI=$outSNISI."</td>";	

						$outSNISI=$outSNISI."<td>";
						$outSNISI=$outSNISI.$dadosDep->publicacoesISI[$i]->ultpagina;
			    		$outSNISI=$outSNISI."</td>";
			    		
			    		$outSNISI=$outSNISI."<td>";
						$outSNISI=$outSNISI.$dadosDep->publicacoesISI[$i]->citacoes;
			    		$outSNISI=$outSNISI."</td>";

			    		$outSNISI=$outSNISI."<td>";
						$outSNISI=$outSNISI.$dadosDep->publicacoesISI[$i]->conftitle;
			    		$outSNISI=$outSNISI."</td>";

			    		$outSNISI=$outSNISI."<td>";
						$outSNISI=$outSNISI.$dadosDep->publicacoesISI[$i]->language;
			    		$outSNISI=$outSNISI."</td>";

			    		$outSNISI=$outSNISI."<td>";
						$outSNISI=$outSNISI.$dadosDep->publicacoesISI[$i]->editor;
			    		$outSNISI=$outSNISI."</td>";

			    		$outSNISI=$outSNISI."<td>";
						$outSNISI=$outSNISI.$dadosDep->publicacoesISI[$i]->tipofmup;
			    		$outSNISI=$outSNISI."</td>";		    				    		
		    		$outSNISI=$outSNISI."</tr>";
				}
				break;
				case 'LN': { // Opção de entrada
		    		$outLNISI=$outLNISI."<tr>";
						$outLNISI=$outLNISI."<td>";
						$outLNISI=$outLNISI.$dadosDep->publicacoesISI[$i]->idinv;
			    		$outLNISI=$outLNISI."</td>";

			    		$outLNISI=$outLNISI."<td>";
			    		$outLNISI=$outLNISI.$dadosDep->investigadores[$dadosDep->publicacoesISI[$i]->idinv]->nome;
			    		$outLNISI=$outLNISI."</td>";

			    		$outLNISI=$outLNISI."<td>";
						$outLNISI=$outLNISI.$dadosDep->publicacoesISI[$i]->ano;
			    		$outLNISI=$outLNISI."</td>";
		    		
			    		$outLNISI=$outLNISI."<td>";
						$outLNISI=$outLNISI.$dadosDep->publicacoesISI[$i]->nomepublicacao;
			    		$outLNISI=$outLNISI."</td>";

			    		$outLNISI=$outLNISI."<td>";
						$outLNISI=$outLNISI.$dadosDep->publicacoesISI[$i]->issn;
			    		$outLNISI=$outLNISI."</td>";

			    		$outLNISI=$outLNISI."<td>";
						$outLNISI=$outLNISI.$dadosDep->publicacoesISI[$i]->isbn;
			    		$outLNISI=$outLNISI."</td>";

			    		$outLNISI=$outLNISI."<td>";
						$outLNISI=$outLNISI.$dadosDep->publicacoesISI[$i]->titulo;
			    		$outLNISI=$outLNISI."</td>";

			    		$outLNISI=$outLNISI."<td>";
						$outLNISI=$outLNISI.$dadosDep->publicacoesISI[$i]->autores;
			    		$outLNISI=$outLNISI."</td>";

						$outLNISI=$outLNISI."<td>";
						$outLNISI=$outLNISI.$dadosDep->publicacoesISI[$i]->volume;
			    		$outLNISI=$outLNISI."</td>";

						$outLNISI=$outLNISI."<td>";
						$outLNISI=$outLNISI.$dadosDep->publicacoesISI[$i]->issue;
			    		$outLNISI=$outLNISI."</td>";

						$outLNISI=$outLNISI."<td>";
						$outLNISI=$outLNISI.$dadosDep->publicacoesISI[$i]->ar;
			    		$outLNISI=$outLNISI."</td>";

						$outLNISI=$outLNISI."<td>";
						$outLNISI=$outLNISI.$dadosDep->publicacoesISI[$i]->colecao;
			    		$outLNISI=$outLNISI."</td>";

						$outLNISI=$outLNISI."<td>";
						$outLNISI=$outLNISI.$dadosDep->publicacoesISI[$i]->primpagina;
			    		$outLNISI=$outLNISI."</td>";

						$outLNISI=$outLNISI."<td>";
						$outLNISI=$outLNISI.$dadosDep->publicacoesISI[$i]->ultpagina;
			    		$outLNISI=$outLNISI."</td>";

			    		$outLNISI=$outLNISI."<td>";
						$outLNISI=$outLNISI.$dadosDep->publicacoesISI[$i]->citacoes;
			    		$outLNISI=$outLNISI."</td>";
			    		
			    		$outLNISI=$outLNISI."<td>";
						$outLNISI=$outLNISI.$dadosDep->publicacoesISI[$i]->conftitle;
			    		$outLNISI=$outLNISI."</td>";

			    		$outLNISI=$outLNISI."<td>";
						$outLNISI=$outLNISI.$dadosDep->publicacoesISI[$i]->language;
			    		$outLNISI=$outLNISI."</td>";

			    		$outLNISI=$outLNISI."<td>";
						$outLNISI=$outLNISI.$dadosDep->publicacoesISI[$i]->editor;
			    		$outLNISI=$outLNISI."</td>";

		    		$outLNISI=$outLNISI."</tr>";
				}
				break;
				case 'CLN': { // Opção de entrada
		    		$outCLNISI=$outCLNISI."<tr>";
						$outCLNISI=$outCLNISI."<td>";
						$outCLNISI=$outCLNISI.$dadosDep->publicacoesISI[$i]->idinv;
			    		$outCLNISI=$outCLNISI."</td>";

			    		$outCLNISI=$outCLNISI."<td>";
			    		$outCLNISI=$outCLNISI.$dadosDep->investigadores[$dadosDep->publicacoesISI[$i]->idinv]->nome;
			    		$outCLNISI=$outCLNISI."</td>";

			    		$outCLNISI=$outCLNISI."<td>";
						$outCLNISI=$outCLNISI.$dadosDep->publicacoesISI[$i]->ano;
			    		$outCLNISI=$outCLNISI."</td>";

			    		$outCLNISI=$outCLNISI."<td>";
						$outCLNISI=$outCLNISI.$dadosDep->publicacoesISI[$i]->nomepublicacao;
			    		$outCLNISI=$outCLNISI."</td>";

			    		$outCLNISI=$outCLNISI."<td>";
						$outCLNISI=$outCLNISI.$dadosDep->publicacoesISI[$i]->issn;
			    		$outCLNISI=$outCLNISI."</td>";
			    		
			    		$outCLNISI=$outCLNISI."<td>";
						$outCLNISI=$outCLNISI.$dadosDep->publicacoesISI[$i]->isbn;
			    		$outCLNISI=$outCLNISI."</td>";

			    		$outCLNISI=$outCLNISI."<td>";
						$outCLNISI=$outCLNISI.$dadosDep->publicacoesISI[$i]->titulo;
			    		$outCLNISI=$outCLNISI."</td>";

			    		$outCLNISI=$outCLNISI."<td>";
						$outCLNISI=$outCLNISI.$dadosDep->publicacoesISI[$i]->autores;
			    		$outCLNISI=$outCLNISI."</td>";

						$outCLNISI=$outCLNISI."<td>";
						$outCLNISI=$outCLNISI.$dadosDep->publicacoesISI[$i]->volume;
			    		$outCLNISI=$outCLNISI."</td>";

						$outCLNISI=$outCLNISI."<td>";
						$outCLNISI=$outCLNISI.$dadosDep->publicacoesISI[$i]->issue;
			    		$outCLNISI=$outCLNISI."</td>";

						$outCLNISI=$outCLNISI."<td>";
						$outCLNISI=$outCLNISI.$dadosDep->publicacoesISI[$i]->ar;
			    		$outCLNISI=$outCLNISI."</td>";

						$outCLNISI=$outCLNISI."<td>";
						$outCLNISI=$outCLNISI.$dadosDep->publicacoesISI[$i]->colecao;
			    		$outCLNISI=$outCLNISI."</td>";

						$outCLNISI=$outCLNISI."<td>";
						$outCLNISI=$outCLNISI.$dadosDep->publicacoesISI[$i]->primpagina;
			    		$outCLNISI=$outCLNISI."</td>";	

						$outCLNISI=$outCLNISI."<td>";
						$outCLNISI=$outCLNISI.$dadosDep->publicacoesISI[$i]->ultpagina;
			    		$outCLNISI=$outCLNISI."</td>";

			    		$outCLNISI=$outCLNISI."<td>";
						$outCLNISI=$outCLNISI.$dadosDep->publicacoesISI[$i]->citacoes;
			    		$outCLNISI=$outCLNISI."</td>";
			    		
			    		$outCLNISI=$outCLNISI."<td>";
						$outCLNISI=$outCLNISI.$dadosDep->publicacoesISI[$i]->conftitle;
			    		$outCLNISI=$outCLNISI."</td>";

			    		$outCLNISI=$outCLNISI."<td>";
						$outCLNISI=$outCLNISI.$dadosDep->publicacoesISI[$i]->language;
			    		$outCLNISI=$outCLNISI."</td>";

			    		$outCLNISI=$outCLNISI."<td>";
						$outCLNISI=$outCLNISI.$dadosDep->publicacoesISI[$i]->editor;
			    		$outCLNISI=$outCLNISI."</td>";			    			    				    		
		    		$outCLNISI=$outCLNISI."</tr>";
				}
				break;
				case 'L': { // Opção de entrada
		    		$outLISI=$outLISI."<tr>";

						$outLISI=$outLISI."<td>";
						$outLISI=$outLISI.$dadosDep->publicacoesISI[$i]->idinv;
			    		$outLISI=$outLISI."</td>";

			    		$outLISI=$outLISI."<td>";
			    		$outLISI=$outLISI.$dadosDep->investigadores[$dadosDep->publicacoesISI[$i]->idinv]->nome;
			    		$outLISI=$outLISI."</td>";

			    		$outLISI=$outLISI."<td>";
						$outLISI=$outLISI.$dadosDep->publicacoesISI[$i]->ano;
			    		$outLISI=$outLISI."</td>";

			    		$outLISI=$outLISI."<td>";
						$outLISI=$outLISI.$dadosDep->publicacoesISI[$i]->nomepublicacao;
			    		$outLISI=$outLISI."</td>";

			    		$outLISI=$outLISI."<td>";
						$outLISI=$outLISI.$dadosDep->publicacoesISI[$i]->issn;
			    		$outLISI=$outLISI."</td>";
			    		
			    		$outLISI=$outLISI."<td>";
						$outLISI=$outLISI.$dadosDep->publicacoesISI[$i]->isbn;
			    		$outLISI=$outLISI."</td>";

			    		$outLISI=$outLISI."<td>";
						$outLISI=$outLISI.$dadosDep->publicacoesISI[$i]->titulo;
			    		$outLISI=$outLISI."</td>";

			    		$outLISI=$outLISI."<td>";
						$outLISI=$outLISI.$dadosDep->publicacoesISI[$i]->autores;
			    		$outLISI=$outLISI."</td>";

						$outLISI=$outLISI."<td>";
						$outLISI=$outLISI.$dadosDep->publicacoesISI[$i]->volume;
			    		$outLISI=$outLISI."</td>";

						$outLISI=$outLISI."<td>";
						$outLISI=$outLISI.$dadosDep->publicacoesISI[$i]->issue;
			    		$outLISI=$outLISI."</td>";

						$outLISI=$outLISI."<td>";
						$outLISI=$outLISI.$dadosDep->publicacoesISI[$i]->ar;
			    		$outLISI=$outLISI."</td>";

						$outLISI=$outLISI."<td>";
						$outLISI=$outLISI.$dadosDep->publicacoesISI[$i]->colecao;
			    		$outLISI=$outLISI."</td>";

						$outLISI=$outLISI."<td>";
						$outLISI=$outLISI.$dadosDep->publicacoesISI[$i]->primpagina;
			    		$outLISI=$outLISI."</td>";	

						$outLISI=$outLISI."<td>";
						$outLISI=$outLISI.$dadosDep->publicacoesISI[$i]->ultpagina;
			    		$outLISI=$outLISI."</td>";

			    		$outLISI=$outLISI."<td>";
						$outLISI=$outLISI.$dadosDep->publicacoesISI[$i]->citacoes;
			    		$outLISI=$outLISI."</td>";
			    		
			    		$outLISI=$outLISI."<td>";
						$outLISI=$outLISI.$dadosDep->publicacoesISI[$i]->conftitle;
			    		$outLISI=$outLISI."</td>";

			    		$outLISI=$outLISI."<td>";
						$outLISI=$outLISI.$dadosDep->publicacoesISI[$i]->language;
			    		$outLISI=$outLISI."</td>";

			    		$outLISI=$outLISI."<td>";
						$outLISI=$outLISI.$dadosDep->publicacoesISI[$i]->editor;
			    		$outLISI=$outLISI."</td>";
			    		   				    		
		    		$outLISI=$outLISI."</tr>";
				}
				break;
				case 'CL': { // Opção de entrada
		    		$outCLISI=$outCLISI."<tr>";
						$outCLISI=$outCLISI."<td>";
						$outCLISI=$outCLISI.$dadosDep->publicacoesISI[$i]->idinv;
			    		$outCLISI=$outCLISI."</td>";

			    		$outCLISI=$outCLISI."<td>";
			    		$outCLISI=$outCLISI.$dadosDep->investigadores[$dadosDep->publicacoesISI[$i]->idinv]->nome;
			    		$outCLISI=$outCLISI."</td>";

			    		$outCLISI=$outCLISI."<td>";
						$outCLISI=$outCLISI.$dadosDep->publicacoesISI[$i]->ano;
			    		$outCLISI=$outCLISI."</td>";

			    		$outCLISI=$outCLISI."<td>";
						$outCLISI=$outCLISI.$dadosDep->publicacoesISI[$i]->nomepublicacao;
			    		$outCLISI=$outCLISI."</td>";

			    		$outCLISI=$outCLISI."<td>";
						$outCLISI=$outCLISI.$dadosDep->publicacoesISI[$i]->issn;
			    		$outCLISI=$outCLISI."</td>";
			    		
			    		$outCLISI=$outCLISI."<td>";
						$outCLISI=$outCLISI.$dadosDep->publicacoesISI[$i]->isbn;
			    		$outCLISI=$outCLISI."</td>";

			    		$outCLISI=$outCLISI."<td>";
						$outCLISI=$outCLISI.$dadosDep->publicacoesISI[$i]->titulo;
			    		$outCLISI=$outCLISI."</td>";

			    		$outCLISI=$outCLISI."<td>";
						$outCLISI=$outCLISI.$dadosDep->publicacoesISI[$i]->autores;
			    		$outCLISI=$outCLISI."</td>";

						$outCLISI=$outCLISI."<td>";
						$outCLISI=$outCLISI.$dadosDep->publicacoesISI[$i]->volume;
			    		$outCLISI=$outCLISI."</td>";

						$outCLISI=$outCLISI."<td>";
						$outCLISI=$outCLISI.$dadosDep->publicacoesISI[$i]->issue;
			    		$outCLISI=$outCLISI."</td>";

						$outCLISI=$outCLISI."<td>";
						$outCLISI=$outCLISI.$dadosDep->publicacoesISI[$i]->ar;
			    		$outCLISI=$outCLISI."</td>";

						$outCLISI=$outCLISI."<td>";
						$outCLISI=$outCLISI.$dadosDep->publicacoesISI[$i]->colecao;
			    		$outCLISI=$outCLISI."</td>";

						$outCLISI=$outCLISI."<td>";
						$outCLISI=$outCLISI.$dadosDep->publicacoesISI[$i]->primpagina;
			    		$outCLISI=$outCLISI."</td>";

						$outCLISI=$outCLISI."<td>";
						$outCLISI=$outCLISI.$dadosDep->publicacoesISI[$i]->ultpagina;
			    		$outCLISI=$outCLISI."</td>";

			    		$outCLISI=$outCLISI."<td>";
						$outCLISI=$outCLISI.$dadosDep->publicacoesISI[$i]->citacoes;
			    		$outCLISI=$outCLISI."</td>";

			    		$outCLISI=$outCLISI."<td>";
						$outCLISI=$outCLISI.$dadosDep->publicacoesISI[$i]->conftitle;
			    		$outCLISI=$outCLISI."</td>";

			    		$outCLISI=$outCLISI."<td>";
						$outCLISI=$outCLISI.$dadosDep->publicacoesISI[$i]->language;
			    		$outCLISI=$outCLISI."</td>";

			    		$outCLISI=$outCLISI."<td>";
						$outCLISI=$outCLISI.$dadosDep->publicacoesISI[$i]->editor;
			    		$outCLISI=$outCLISI."</td>";

		    		$outCLISI=$outCLISI."</tr>";
				}
				break;
				case 'P': { // Opção de entrada
		    		$outPISI=$outPISI."<tr>";
			    		$outPISI=$outPISI."<td>";
			    		$outPISI=$outPISI.$dadosDep->publicacoesISI[$i]->idinv;
			    		$outPISI=$outPISI."</td>";

			    		$outPISI=$outPISI."<td>";
			    		$outPISI=$outPISI.$dadosDep->investigadores[$dadosDep->publicacoesISI[$i]->idinv]->nome;
			    		$outPISI=$outPISI."</td>";

			    		$outPISI=$outPISI."<td>";
						$outPISI=$outPISI.$dadosDep->publicacoesISI[$i]->npatente;
			    		$outPISI=$outPISI."</td>";

			    		$outPISI=$outPISI."<td>";
						$outPISI=$outPISI.$dadosDep->publicacoesISI[$i]->ipc;
			    		$outPISI=$outPISI."</td>";	

			    		$outPISI=$outPISI."<td>";
						$outPISI=$outPISI.$dadosDep->publicacoesISI[$i]->titulo;
			    		$outPISI=$outPISI."</td>";	

			    		$outPISI=$outPISI."<td>";
						$outPISI=$outPISI.$dadosDep->publicacoesISI[$i]->autores;
			    		$outPISI=$outPISI."</td>";

			    		$outPISI=$outPISI."<td>";
						$outPISI=$outPISI.$dadosDep->publicacoesISI[$i]->datapatente;
			    		$outPISI=$outPISI."</td>";		    		
			    		
		    		$outPISI=$outPISI."</tr>";
				}
				break;
				case 'A': { // Opção de entrada
		    		$outAISI=$outAISI."<tr>";
						$outAISI=$outAISI."<td>";
						$outAISI=$outAISI.$dadosDep->publicacoesISI[$i]->idinv;
			    		$outAISI=$outAISI."</td>";

			    		$outAISI=$outAISI."<td>";
			    		$outAISI=$outAISI.$dadosDep->investigadores[$dadosDep->publicacoesISI[$i]->idinv]->nome;
			    		$outAISI=$outAISI."</td>";

			    		$outAISI=$outAISI."<td>";
						$outAISI=$outAISI.$dadosDep->publicacoesISI[$i]->ano;
			    		$outAISI=$outAISI."</td>";
			    		
			    		$outAISI=$outAISI."<td>";
						$outAISI=$outAISI.$dadosDep->publicacoesISI[$i]->nomepublicacao;
			    		$outAISI=$outAISI."</td>";

			    		$outAISI=$outAISI."<td>";
						$outAISI=$outAISI.$dadosDep->publicacoesISI[$i]->issn;
			    		$outAISI=$outAISI."</td>";
			    		
			    		$outAISI=$outAISI."<td>";
						$outAISI=$outAISI.$dadosDep->publicacoesISI[$i]->isbn;
			    		$outAISI=$outAISI."</td>";

			    		$outAISI=$outAISI."<td>";
						$outAISI=$outAISI.$dadosDep->publicacoesISI[$i]->titulo;
			    		$outAISI=$outAISI."</td>";

			    		$outAISI=$outAISI."<td>";
						$outAISI=$outAISI.$dadosDep->publicacoesISI[$i]->autores;
			    		$outAISI=$outAISI."</td>";

						$outAISI=$outAISI."<td>";
						$outAISI=$outAISI.$dadosDep->publicacoesISI[$i]->volume;
			    		$outAISI=$outAISI."</td>";

						$outAISI=$outAISI."<td>";
						$outAISI=$outAISI.$dadosDep->publicacoesISI[$i]->issue;
			    		$outAISI=$outAISI."</td>";

						$outAISI=$outAISI."<td>";
						$outAISI=$outAISI.$dadosDep->publicacoesISI[$i]->ar;
			    		$outAISI=$outAISI."</td>";

						$outAISI=$outAISI."<td>";
						$outAISI=$outAISI.$dadosDep->publicacoesISI[$i]->colecao;
			    		$outAISI=$outAISI."</td>";

						$outAISI=$outAISI."<td>";
						$outAISI=$outAISI.$dadosDep->publicacoesISI[$i]->primpagina;
			    		$outAISI=$outAISI."</td>";	

						$outAISI=$outAISI."<td>";
						$outAISI=$outAISI.$dadosDep->publicacoesISI[$i]->ultpagina;
			    		$outAISI=$outAISI."</td>";

			    		$outAISI=$outAISI."<td>";
						$outAISI=$outAISI.$dadosDep->publicacoesISI[$i]->citacoes;
			    		$outAISI=$outAISI."</td>";
			    		
			    		$outAISI=$outAISI."<td>";
						$outAISI=$outAISI.$dadosDep->publicacoesISI[$i]->conftitle;
			    		$outAISI=$outAISI."</td>";

			    		$outAISI=$outAISI."<td>";
						$outAISI=$outAISI.$dadosDep->publicacoesISI[$i]->language;
			    		$outAISI=$outAISI."</td>";

			    		$outAISI=$outAISI."<td>";
						$outAISI=$outAISI.$dadosDep->publicacoesISI[$i]->editor;
			    		$outAISI=$outAISI."</td>";    				    		
		    		$outAISI=$outAISI."</tr>";
				}
				break;
				default:{ //echo "DEFAULT";
			        $outRestoISI=$outRestoISI."<tr>";
						$outRestoISI=$outRestoISI."<td>";
						$outRestoISI=$outRestoISI.$dadosDep->publicacoesISI[$i]->idinv;
			    		$outRestoISI=$outRestoISI."</td>";

			    		$outRestoISI=$outRestoISI."<td>";
						$outRestoISI=$outRestoISI.$dadosDep->publicacoesISI[$i]->ano;
			    		$outRestoISI=$outRestoISI."</td>";

			    		$outRestoISI=$outRestoISI."<td>";
						$outRestoISI=$outRestoISI.$dadosDep->publicacoesISI[$i]->tipo;
			    		$outRestoISI=$outRestoISI."</td>";

			    		$outRestoISI=$outRestoISI."<td>";
						$outRestoISI=$outRestoISI.$dadosDep->publicacoesISI[$i]->nomepublicacao;
			    		$outRestoISI=$outRestoISI."</td>";

			    		$outRestoISI=$outRestoISI."<td>";
						$outRestoISI=$outRestoISI.$dadosDep->publicacoesISI[$i]->issn;
			    		$outRestoISI=$outRestoISI."</td>";

			    		$outRestoISI=$outRestoISI."<td>";
						$outRestoISI=$outRestoISI.$dadosDep->publicacoesISI[$i]->titulo;
			    		$outRestoISI=$outRestoISI."</td>";

			    		$outRestoISI=$outRestoISI."<td>";
						$outRestoISI=$outRestoISI.$dadosDep->publicacoesISI[$i]->autores;
			    		$outRestoISI=$outRestoISI."</td>";

						$outRestoISI=$outRestoISI."<td>";
						$outRestoISI=$outRestoISI.$dadosDep->publicacoesISI[$i]->volume;
			    		$outRestoISI=$outRestoISI."</td>";

						$outRestoISI=$outRestoISI."<td>";
						$outRestoISI=$outRestoISI.$dadosDep->publicacoesISI[$i]->issue;
			    		$outRestoISI=$outRestoISI."</td>";

						$outRestoISI=$outRestoISI."<td>";
						$outRestoISI=$outRestoISI.$dadosDep->publicacoesISI[$i]->ar;
			    		$outRestoISI=$outRestoISI."</td>";

						$outRestoISI=$outRestoISI."<td>";
						$outRestoISI=$outRestoISI.$dadosDep->publicacoesISI[$i]->primpagina;
			    		$outRestoISI=$outRestoISI."</td>";	

						$outRestoISI=$outRestoISI."<td>";
						$outRestoISI=$outRestoISI.$dadosDep->publicacoesISI[$i]->ultpagina;
			    		$outRestoISI=$outRestoISI."</td>";

			    		$outRestoISI=$outRestoISI."<td>";
						$outRestoISI=$outRestoISI.$dadosDep->publicacoesISI[$i]->citacoes;
			    		$outRestoISI=$outRestoISI."</td>";

			    		$outRestoISI=$outRestoISI."<td>";
						$outRestoISI=$outRestoISI.$dadosDep->publicacoesISI[$i]->conftitle;
			    		$outRestoISI=$outRestoISI."</td>";

			    		$outRestoISI=$outRestoISI."<td>";
						$outRestoISI=$outRestoISI.$dadosDep->publicacoesISI[$i]->language;
			    		$outRestoISI=$outRestoISI."</td>";

			    		$outRestoISI=$outRestoISI."<td>";
						$outRestoISI=$outRestoISI.$dadosDep->publicacoesISI[$i]->editor;
			    		$outRestoISI=$outRestoISI."</td>";

			    		$outRestoISI=$outRestoISI."<td>";
						$outRestoISI=$outRestoISI.$dadosDep->publicacoesISI[$i]->tipofmup;
			    		$outRestoISI=$outRestoISI."</td>";
		    		$outRestoISI=$outRestoISI."</tr>";
			    }
				break;
			}
	}
	
	if($outJISI!=""){echo"<div id='outJISI'><table class='box-table-b'><caption>ARTIGOS</caption>";echo $tcolsJISI;echo "<tbody>";echo $outJISI;echo "</tbody>";echo "</table></p></div><br /><br>";}
	if($outPRPISI!=""){echo"<div id='outPRPISI'><table class='box-table-b'><caption>PEER REVIEW PROCEEDINGS</caption>";echo $tcolsJISI;echo "<tbody>";echo $outPRPISI;echo "</tbody>";echo "</table></div><br /><br>";}
	if($outJAISI!=""){echo"<div id='outJAISI'><table class='box-table-b'><caption>JOURNAL ABSTRACTS</caption>";echo $tcolsJISI;echo "<tbody>";echo $outJAISI;echo "</tbody>";echo "</table></div><br /><br>";}
	if($outCPISI!=""){echo"<div id='outCPISI'><table class='box-table-b'><caption>CONFERENCE PROCEEDINGS</caption>";echo $tcolsCONFISI;echo "<tbody>";echo $outCPISI;echo "</tbody>";echo "</table></div><br /><br>";}
	if($outOAISI!=""){echo"<div id='outOAISI'><table class='box-table-b'><caption>OTHER ABSTRACTS</caption>";echo $tcolsCONFISI;echo "<tbody>";echo $outOAISI;echo "</tbody>";echo "</table></div><br /><br>";}	
	if($outLISI!=""){echo"<div id='outLISI'><table class='box-table-b'><caption>LIVROS</caption>";echo $tcols;echo "<tbody>";echo $outLISI;echo "</tbody>";echo "</table></div><br /><br>";}
	if($outCLISI!=""){echo"<div id='outCLISI'><table class='box-table-b'><caption>CAPITULOS DE LIVROS</caption>";echo $tcols;echo "<tbody>";echo $outCLISI;echo "</tbody>";echo "</table></div><br /><br>";}
	if($outSISI!=""){echo"<div id='outSISI'><table class='box-table-b'><caption>SUMÁRIOS</caption>";echo $tcols;echo "<tbody>";echo $outSISI;echo "</tbody>";echo "</table></div><br /><br>";}																															
	if($outJNISI!=""){echo"<div id='outJNISI'><table class='box-table-b'><caption>ARTIGOS - NÃO INGLÊS</caption>";echo $tcols;echo "<tbody>";echo $outJNISI;echo "</tbody>";echo "</table></div><br><br>";}
	if($outPRPNISI!=""){echo"<div id='outPRPNISI'><table class='box-table-b'><caption>PEER REVIEW PROCEEDINGS - NÃO INGLÊS</caption>";echo $tcols;echo "<tbody>";echo $outPRPNISI;echo "</tbody>";echo "</table></div><br /><br>";}
	if($outSNISI!=""){echo"<div id='outSNISI'><table class='box-table-b'><caption>SUMÁRIO - NÃO INGLÊS</caption>";echo $tcols;echo "<tbody>";echo $outSNISI;echo "</tbody>";echo "</table></div><br /><br>";}
	if($outLNISI!=""){echo"<div id='outLNISI'><table class='box-table-b'><caption>LIVROS - NÃO INGLÊS</caption>";echo $tcols;echo "<tbody>";echo $outLNISI;echo "</tbody>";echo "</table></div><br /><br>";}
	if($outCLNISI!=""){echo"<div id='outCLNISI'><table class='box-table-b'><caption>CAPITULOS DE LIVROS - NÃO INGLÊS</caption>";echo $tcols;echo "<tbody>";echo $outCLNISI;echo "</tbody>";echo "</table></div><br /><br>";}
	if($outCPNISI!=""){echo"<div id='outCPNISI'><table class='box-table-b'><caption>CONFERENCE PROCEEDINGS NÃO INGLÊS</caption>";echo $tcols;echo "<tbody>";echo $outCPNISI;echo "</tbody>";echo "</table></div><br /><br>";}	
	if($outPISI!=""){echo"<div id='outPISI'><table class='box-table-b'><caption>PATENTES</caption>";echo $tcolsPATISI;echo "<tbody>";echo $outPISI;echo "</tbody>";echo "</table></div><br /><br>";}
	if($outAISI!=""){echo"<div id='outAISI'><table class='box-table-b'><caption>AGRADECIMENTOS</caption>";echo $tcols;echo "<tbody>";echo $outAISI;echo "</tbody>";echo "</table></div><br /><br>";}	
	
	echo "</div>";

	
?>