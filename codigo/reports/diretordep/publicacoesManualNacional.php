<?php

$outJNMAN="";
$outJANMAN="";
$outPRPNMAN="";
$outCPNMAN="";
$outOANMAN="";
$outLNMAN="";
$outCLNMAN="";

$tcols="<thead>
            <tr>
                <th>IDINV</th>
                <th>Nome</th>
                <th>TIPO</th>
                <th>TÍTULO DA OBRA</th>
                <th>ISSN</th>
                <th>ISSN_LINKING</th>
                <th>ISSN_ELECTRONIC</th>
                <th>ISBN</th>
                <th>TITULO</th>
                <th>AUTORES</th>
                <th>VOLUME</th>
                <th>ISSUE</th>
                <th>AR</th>
                <th>COLECAO</th>
                <th>PRIMPAGINA</th>
                <th>ULTPAGINA</th>
                <th>IFACTOR</th>
                <th>ANO</th>
                <th>CITACOES</th>
                <th>NPATENTE</th>
                <th>DATAPATENTE</th>
                <th>IPC</th>
                <th>AREA</th>
                <th>LINK</th>
                <th>ESTADO</th>
            </tr>
        </thead>";

$tcolsJ="<thead>
            <tr>
                <th>IDINV</th>
                <th>Nome</th>
                <th>ISSN</th>
                <th>REVISTA</th>
                <th>TÍTULO</th>
                <th>AUTORES</th>
                <th>VOLUME</th>
                <th>ISSUE</th>
                <th>PRIMEIRA PÁG.</th>
                <th>ÚLTIMA PÁG.</th>
                <th>LINK</th>
                <th>ESTADO</th>
            </tr>
        </thead>";

$tcolsCONF="<thead>
                <tr>
                    <th>IDINV</th>
                    <th>Nome</th>
                    <th>ISBN</th>
                    <th>TÍTULO DA OBRA</th>
                    <th>EDITORES</th>
                    <th>TÍTULO ARTIGO</th>
                    <th>AUTORES</th>
                    <th>PRIMEIRA PÁG.</th>
                    <th>ÚLTIMA PÁG.</th>
                    <th>LINK</th>
                    <th>ESTADO</th>
                </tr>
            </thead>";

$tcolsPRP="<thead>
                <tr>
                    <th>IDINV</th>
                    <th>Nome</th>
                    <th>ISSN</th>
                    <th>TÍTULO DA OBRA</th>
                    <th>EDITORES</th>
                    <th>TÍTULO ARTIGO</th>
                    <th>AUTORES</th>
                    <th>PRIMEIRA PÁ</th>
                    <th>LINK</th>
                    <th>ESTADO</th>
                </tr>
            </thead>";

$tcolsCLIV="<thead>
                <tr>
                    <th>IDINV</th>
                    <th>Nome</th>
                    <th>ISBN</th>
                    <th>TÍTULO DA OBRA</th>
                    <th>EDITORA</th>
                    <th>EDITORES</th>
                    <th>TÍTULO ARTIGO</th>
                    <th>AUTORES</th>
                    <th>PRIMEIRA PÁG.</th>
                    <th>ÚLTIMA PÁG.</th>
                    <th>LINK</th>
                    <th>ESTADO</th>
                </tr>
            </thead>";

$tcolsPAT="<thead>
                <tr>
                    <th>IDINV</th>
                    <th>Nome</th>
                    <th>Nº PATENTE</th>
                    <th>IPC</th>
                    <th>TÍTULO</th>
                    <th>AUTORES</th>
                    <th>DATA de PUBLICAÇÃO</th>
                    <th>LINK</th>
                    <th>ESTADO</th>
                </tr>
        </thead>";

$tcolsLIV="<thead>
                <tr>
                    <th>IDINV</th>
                    <th>Nome</th>
                    <th>ISBN</th>
                    <th>TÍTULO</th>
                    <th>AUTORES</th>
                    <th>EDITOR</th>
                    <th>EDITORA</th>
                    <th>LINK</th>
                    <th>ESTADO</th>
                </tr>
            </thead>";


		    
	foreach ($dadosDep->publicacoesMANUALNacional as $i => $value){
			
        $tipo=$dadosDep->publicacoesMANUALNacional[$i]->tipofmup;

        switch($tipo){
            case 'CPN': {
                $outCPNMAN=$outCPNMAN."<tr>";
                    $outCPNMAN=$outCPNMAN."<td>".$dadosDep->publicacoesMANUALNacional[$i]->idinv."</td>";
                    $outCPNMAN=$outCPNMAN."<td>".$dadosDep->investigadores[$dadosDep->publicacoesMANUALNacional[$i]->idinv]->nome."</td>";
                    $outCPNMAN=$outCPNMAN."<td id='td_outCPNMAN_isbn_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->isbn."</td>";
                    $outCPNMAN=$outCPNMAN."<td id='td_outCPNMAN_nomepub_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->nomepublicacao."</td>";
                    $outCPNMAN=$outCPNMAN."<td id='td_outCPNMAN_editor_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->editor."</td>";
                    $outCPNMAN=$outCPNMAN."<td id='td_outCPNMAN_titulo_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->titulo."</td>";
                    $outCPNMAN=$outCPNMAN."<td id='td_outCPNMAN_autores_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->autores."</td>";
                    $outCPNMAN=$outCPNMAN."<td id='td_outCPNMAN_prim_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->primpagina."</td>";
                    $outCPNMAN=$outCPNMAN."<td id='td_outCPNMAN_ult_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->ultpagina."</td>";
                    $outCPNMAN=$outCPNMAN."<td id='td_outCPNMAN_link_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->link."</textarea></td>";
                    $outCPNMAN=$outCPNMAN."<td id='td_outCPNMAN_estado_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".getEstadoPublicacao($dadosDep->publicacoesMANUALNacional[$i]->estado)."</td>";
                    $outCPNMAN=$outCPNMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubNacMan-Con').text('" . $dadosDep->publicacoesMANUALNacional[$i]->id . "');\"></td>";
                    $outCPNMAN=$outCPNMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-pubNacMan-Con').text('" . $dadosDep->publicacoesMANUALNacional[$i]->id . "');apagarPubManNacCon();return false;\" ></center></td>";
                $outCPNMAN=$outCPNMAN."</tr>";
            }
            break;
            case 'JN': {
                $outJNMAN=$outJNMAN."<tr>";
                    $outJNMAN=$outJNMAN."<td>".$dadosDep->publicacoesMANUALNacional[$i]->idinv."</td>";
                    $outJNMAN=$outJNMAN."<td>".$dadosDep->investigadores[$dadosDep->publicacoesMANUALNacional[$i]->idinv]->nome."</td>";
                    $outJNMAN=$outJNMAN."<td id='td_outJNMAN_issn_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->issn."</td>";
                    $outJNMAN=$outJNMAN."<td id='td_outJNMAN_nomepub_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->nomepublicacao."</td>";
                    $outJNMAN=$outJNMAN."<td id='td_outJNMAN_titulo_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->titulo."</td>";
                    $outJNMAN=$outJNMAN."<td id='td_outJNMAN_autores_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->autores."</td>";
                    $outJNMAN=$outJNMAN."<td id='td_outJNMAN_volume_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->volume."</td>";
                    $outJNMAN=$outJNMAN."<td id='td_outJNMAN_issue_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->issue."</td>";
                    $outJNMAN=$outJNMAN."<td id='td_outJNMAN_prim_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->primpagina."</td>";
                    $outJNMAN=$outJNMAN."<td id='td_outJNMAN_ult_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->ultpagina."</td>";
                    $outJNMAN=$outJNMAN."<td id='td_outJNMAN_link_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->link."</td>";
                    $outJNMAN=$outJNMAN."<td id='td_outJNMAN_estado_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".getEstadoPublicacao($dadosDep->publicacoesMANUALNacional[$i]->estado)."</td>";
                    $outJNMAN=$outJNMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubNacMan-Art').text('" . $dadosDep->publicacoesMANUALNacional[$i]->id . "');\"></td>";
                    $outJNMAN=$outJNMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep->publicacoesMANUALNacional[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-pubNacMan-Art').text('" . $dadosDep->publicacoesMANUALNacional[$i]->id . "');apagarPubManNacArt();return false;\" ></center></td>";
                $outJNMAN=$outJNMAN."</tr>";
            }
            break;
            case 'PRPN': {
                $outPRPNMAN=$outPRPNMAN."<tr>";
                    $outPRPNMAN=$outPRPNMAN."<td>".$dadosDep->publicacoesMANUALNacional[$i]->idinv."</td>";
                    $outPRPNMAN=$outPRPNMAN."<td>".$dadosDep->investigadores[$dadosDep->publicacoesMANUALNacional[$i]->idinv]->nome."</td>";
                    $outPRPNMAN=$outPRPNMAN."<td id='td_outPRPNMAN_issn_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->issn."</td>";
                    $outPRPNMAN=$outPRPNMAN."<td id='td_outPRPNMAN_nomepub_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->nomepublicacao."</td>";
                    $outPRPNMAN=$outPRPNMAN."<td id='td_outPRPNMAN_titulo_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->titulo."</td>";
                    $outPRPNMAN=$outPRPNMAN."<td id='td_outPRPNMAN_autores_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->autores."</td>";
                    $outPRPNMAN=$outPRPNMAN."<td id='td_outPRPNMAN_volume_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->volume."</td>";
                    $outPRPNMAN=$outPRPNMAN."<td id='td_outPRPNMAN_issue_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->issue."</td>";
                    $outPRPNMAN=$outPRPNMAN."<td id='td_outPRPNMAN_prim_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->primpagina."</td>";
                    $outPRPNMAN=$outPRPNMAN."<td id='td_outPRPNMAN_ult_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->ultpagina."</td>";
                    $outPRPNMAN=$outPRPNMAN."<td id='td_outPRPNMAN_link_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->link."</td>";
                    $outPRPNMAN=$outPRPNMAN."<td id='td_outPRPNMAN_estado_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".getEstadoPublicacao($dadosDep->publicacoesMANUALNacional[$i]->estado)."</td>";
                    $outPRPNMAN=$outPRPNMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubNacMan-Rev').text('" . $dadosDep->publicacoesMANUALNacional[$i]->id . "');\"></td>";
                    $outPRPNMAN=$outPRPNMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-pubNacMan-Rev').text('" . $dadosDep->publicacoesMANUALNacional[$i]->id . "');apagarPubManNacRev();return false;\" ></center></td>";
                $outPRPNMAN=$outPRPNMAN."</tr>";
            }
            break;
            case 'JAN': {
                $outJANMAN=$outJANMAN."<tr>";
                    $outJANMAN=$outJANMAN."<td>".$dadosDep->publicacoesMANUALNacional[$i]->idinv."</td>";
                    $outJANMAN=$outJANMAN."<td>".$dadosDep->investigadores[$dadosDep->publicacoesMANUALNacional[$i]->idinv]->nome."</td>";
                    $outJANMAN=$outJANMAN."<td id='td_outJANMAN_issn_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->issn."</td>";
                    $outJANMAN=$outJANMAN."<td id='td_outJANMAN_nomepub_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->nomepublicacao."</td>";
                    $outJANMAN=$outJANMAN."<td id='td_outJANMAN_titulo_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->titulo."</td>";
                    $outJANMAN=$outJANMAN."<td id='td_outJANMAN_autores_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->autores."</td>";
                    $outJANMAN=$outJANMAN."<td id='td_outJANMAN_volume_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->volume."</td>";
                    $outJANMAN=$outJANMAN."<td id='td_outJANMAN_issue_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->issue."</td>";
                    $outJANMAN=$outJANMAN."<td id='td_outJANMAN_prim_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->primpagina."</td>";
                    $outJANMAN=$outJANMAN."<td id='td_outJANMAN_ult_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->ultpagina."</td>";
                    $outJANMAN=$outJANMAN."<td id='td_outJANMAN_link_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->link."</td>";
                    $outJANMAN=$outJANMAN."<td id='td_outJANMAN_estado_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".getEstadoPublicacao($dadosDep->publicacoesMANUALNacional[$i]->estado)."</td>";
                    $outJANMAN=$outJANMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubNacMan-Sum').text('" . $dadosDep->publicacoesMANUALNacional[$i]->id . "');\"></td>";
                    $outJANMAN=$outJANMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-pubNacMan-Sum').text('" . $dadosDep->publicacoesMANUALNacional[$i]->id . "');apagarPubManNacSum();return false;\" ></center></td>";
                $outJANMAN=$outJANMAN."</tr>";
            }
            break;
            case 'OAN': {
                $outOANMAN=$outOANMAN."<tr>";
                    $outOANMAN=$outOANMAN."<td>".$dadosDep->publicacoesMANUALNacional[$i]->idinv."</td>";
                    $outOANMAN=$outOANMAN."<td>".$dadosDep->investigadores[$dadosDep->publicacoesMANUALNacional[$i]->idinv]->nome."</td>";
                    $outOANMAN=$outOANMAN."<td id='td_outOANMAN_isbn_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->isbn."</td>";
                    $outOANMAN=$outOANMAN."<td id='td_outOANMAN_nomepub_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->nomepublicacao."</td>";
                    $outOANMAN=$outOANMAN."<td id='td_outOANMAN_editor_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->editor."</td>";
                    $outOANMAN=$outOANMAN."<td id='td_outOANMAN_titulo_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->titulo."</td>";
                    $outOANMAN=$outOANMAN."<td id='td_outOANMAN_autores_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->autores."</td>";
                    $outOANMAN=$outOANMAN."<td id='td_outOANMAN_prim_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->primpagina."</td>";
                    $outOANMAN=$outOANMAN."<td id='td_outOANMAN_ult_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->ultpagina."</td>";
                    $outOANMAN=$outOANMAN."<td id='td_outOANMAN_link_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->link."</td>";
                    $outOANMAN=$outOANMAN."<td id='td_outOANMAN_estado_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".getEstadoPublicacao($dadosDep->publicacoesMANUALNacional[$i]->estado)."</td>";
                    $outOANMAN=$outOANMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubNacMan-Oth').text('" . $dadosDep->publicacoesMANUALNacional[$i]->id . "');\"></td>";
                    $outOANMAN=$outOANMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-pubNacMan-Oth').text('" . $dadosDep->publicacoesMANUALNacional[$i]->id . "');apagarPubManNacOth();return false;\" ></center></td>";
                $outOANMAN=$outOANMAN."</tr>";
            }
            break;
            case 'LN': {
                $outLNMAN=$outLNMAN."<tr>";
                    $outLNMAN=$outLNMAN."<td>".$dadosDep->publicacoesMANUALNacional[$i]->idinv."</td>";
                    $outLNMAN=$outLNMAN."<td>".$dadosDep->investigadores[$dadosDep->publicacoesMANUALNacional[$i]->idinv]->nome."</td>";
                    $outLNMAN=$outLNMAN."<td id='td_outLNMAN_isbn_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->isbn."</td>";
                    $outLNMAN=$outLNMAN."<td id='td_outLNMAN_titulo_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->titulo."</td>";
                    $outLNMAN=$outLNMAN."<td id='td_outLNMAN_autores_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->autores."</td>";
                    $outLNMAN=$outLNMAN."<td id='td_outLNMAN_editor_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->editor."</td>";
                    $outLNMAN=$outLNMAN."<td id='td_outLNMAN_editora_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->editora."</td>";
                    $outLNMAN=$outLNMAN."<td id='td_outLNMAN_link_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->link."</td>";
                    $outLNMAN=$outLNMAN."<td id='td_outLNMAN_estado_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".getEstadoPublicacao($dadosDep->publicacoesMANUALNacional[$i]->estado)."</td>";
                    $outLNMAN=$outLNMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubNacMan-Liv').text('" . $dadosDep->publicacoesMANUALNacional[$i]->id . "');\"></td>";
                    $outLNMAN=$outLNMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-pubNacMan-Liv').text('" . $dadosDep->publicacoesMANUALNacional[$i]->id . "');apagarPubManNacLiv();return false;\" ></center></td>";
                $outLNMAN=$outLNMAN."</tr>";
            }
            break;
            case 'CLN': {
                $outCLNMAN=$outCLNMAN."<tr>";
                    $outCLNMAN=$outCLNMAN."<td>".$dadosDep->publicacoesMANUALNacional[$i]->idinv."</td>";
                    $outCLNMAN=$outCLNMAN."<td>".$dadosDep->investigadores[$dadosDep->publicacoesMANUALNacional[$i]->idinv]->nome."</td>";
                    $outCLNMAN=$outCLNMAN."<td id='td_outCLNMAN_isbn_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->isbn."</td>";
                    $outCLNMAN=$outCLNMAN."<td id='td_outCLNMAN_nomepub_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->nomepublicacao."</td>";
                    $outCLNMAN=$outCLNMAN."<td id='td_outCLNMAN_editora_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->editora."</td>";
                    $outCLNMAN=$outCLNMAN."<td id='td_outCLNMAN_editor_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->editor."</td>";
                    $outCLNMAN=$outCLNMAN."<td id='td_outCLNMAN_titulo_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->titulo."</td>";
                    $outCLNMAN=$outCLNMAN."<td id='td_outCLNMAN_autores_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->autores."</td>";
                    $outCLNMAN=$outCLNMAN."<td id='td_outCLNMAN_prim_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->primpagina."</td>";
                    $outCLNMAN=$outCLNMAN."<td id='td_outCLNMAN_ult_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->ultpagina."</td>";
                    $outCLNMAN=$outCLNMAN."<td id='td_outCLNMAN_link_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".$dadosDep->publicacoesMANUALNacional[$i]->link."</td>";
                    $outCLNMAN=$outCLNMAN."<td id='td_outCLNMAN_estado_" . $dadosDep->publicacoesMANUALNacional[$i]->id . "'>".getEstadoPublicacao($dadosDep->publicacoesMANUALNacional[$i]->estado)."</td>";
                    $outCLNMAN=$outCLNMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubNacMan-CLiv').text('" . $dadosDep->publicacoesMANUALNacional[$i]->id . "');\"></td>";
                    $outCLNMAN=$outCLNMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-pubNacMan-CLiv').text('" . $dadosDep->publicacoesMANUALNacional[$i]->id . "');apagarPubManNacCLiv();return false;\" ></center></td>";
                $outCLNMAN=$outCLNMAN."</tr>";
            }
            break;
        }
	}

	echo "<h3>Publicações Manuais Não Inglês</h3>\n";
    echo "<div id='pubManualNacional'>";
        echo"<div id='pubManualNac-Art'><table id='pubManNac-Art' class='box-table-b'><caption >ARTIGOS</caption>";echo $tcolsJ;echo "<tbody>";echo $outJNMAN; echo "</body></table><p id='chave-pubNacMan-Art' hidden></p></div><br>";
        echo"<div id='pubManualNac-Rev'><table id='pubManNac-Rev' class='box-table-b'><caption >PEER REVIEW PROCEEDINGS</caption>";echo $tcolsJ;echo "<tbody>";echo $outPRPNMAN;echo "</body></table><p id='chave-pubNacMan-Rev' hidden></div><br>";
        echo"<div id='pubManualNac-Sum'><table id='pubManNac-Sum' class='box-table-b'><caption >SUMÁRIOS<p><i>JOURNAL ABSTRACTS</i></p></caption>";echo $tcolsJ;echo "<tbody>";echo $outJANMAN;echo "</body></table><p id='chave-pubNacMan-Sum' hidden></div><br>";
        echo"<div id='pubManualNac-Liv'><table id='pubManNac-Liv' class='box-table-b'><caption >LIVROS<p><i>BOOKS</i></p></caption>";echo $tcolsLIV;echo "<tbody>";echo $outLNMAN;echo "</body></table><p id='chave-pubNacMan-Liv' hidden></div><br>";
        echo"<div id='pubManualNac-CLiv'><table id='pubManNac-CLiv' class='box-table-b'><caption >CAPITULOS DE LIVROS<p><i>BOOK CHAPTERS</i></p></caption>";echo $tcolsCLIV;echo "<tbody>";echo $outCLNMAN;echo "</body></table><p id='chave-pubNacMan-CLiv' hidden></div><br>";
        echo"<div id='pubManualNac-Con'><table id='pubManNac-Con' class='box-table-b'><caption >CONFERENCE PROCEEDINGS</caption>";echo $tcolsCONF;echo "<tbody>";echo $outCPNMAN;echo "</body></table><p id='chave-pubNacMan-Con' hidden></div><br>";
        echo"<div id='pubManualNac-Oth'><table id='pubManNac-Oth' class='box-table-b'><caption >OUTROS SUMÁRIOS<br>OTHER ABSTRACTS</caption>";echo $tcolsCONF;echo "<tbody>";echo $outOANMAN;echo "</body></table><p id='chave-pubNacMan-Oth' hidden></div><br>";
    echo "</div>";

	function getEstadoPublicacao($i) {
		global $dadosDep;
		$db = new Database();
		$lValues =$db->getLookupValues("lista_estadoPublicacoes");
		$texto="";
		while ($row = mysql_fetch_assoc($lValues)) {
			if($i==$row["ID"])
				$texto=$row["DESCRICAO"];
		}
		
		$db->disconnect();
		return $texto;
	
	}

?>