<?php

  echo "<h3>Investigadores Registados</h3> 
    <div id='investigadores'>   
      <table id='users' class='box-table-b' >
        <thead>
          <tr>
            <th>ID Inv<p><i>ID Inv</i></p></th>
            <th>Login</th><th>Nome</th>
            <th>Nº Mecanográfico</th>
            <th>Data Nascimento</th>
            <th>Estado</th>
            <th>Data de Lacre</th>
          </tr>
      </thead>";

  echo "<tbody>";
  foreach ($dadosDep->investigadores as $i => $value){
      echo "<tr id='user" .$dadosDep->investigadores[$i]->id. "'>";
        echo "<td>".$dadosDep->investigadores[$i]->id."</td>";
        echo "<td>".$dadosDep->investigadores[$i]->login."</td>";
        echo "<td id='td" . $dadosDep->investigadores[$i]->id . "_nome'>".$dadosDep->investigadores[$i]->nome."</td>";
        echo "<td id='td" . $dadosDep->investigadores[$i]->id . "_nummec'>".$dadosDep->investigadores[$i]->nummec."</td>";
        echo "<td id='td" . $dadosDep->investigadores[$i]->id . "_datanasc'>".$dadosDep->investigadores[$i]->datanasc."</td>";
        if($dadosDep->investigadores[$i]->estado=="0")
          echo "<td style='color:red !important;'>Não lacrado</td>";
        else
        echo "<td>Lacrado</td>";
        echo "<td>".$dadosDep->investigadores[$i]->datasubmissao."</td>";
        echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-inv').text('" . $dadosDep->investigadores[$i]->id . "');\"></td>";
        echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep->investigadores[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-inv').text('" . $dadosDep->investigadores[$i]->id . "');apagarInvestigador();return false;\" ></center></td>";
      echo "</tr>";       
  }

  echo "</tbody></table> 
      <p id='chave-inv' hidden></p>
      <input type='hidden' name='apagaRegHabilitacao'/>
    </div>";
	?>