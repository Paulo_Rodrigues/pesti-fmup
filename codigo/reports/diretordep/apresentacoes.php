<?php

/**
* Autor: Paulo Rodrigues (prodrigues@med.up.pt/paulomiguelarodrigues@gmail.com)
* Data: 24/04/2014
* Alterações: Adicionada a capacidade de poder editar e remover registos de apresentações. 
*/

echo "<h3>Apresentações</h3>\n";
echo "<div id='apresentacoes'>";
	echo "<table class='box-table-b'>
			<thead>
				<tr>
					<th></th>
					<th></th>
					<th colspan='3'>Internacional</th>
					<th colspan='3'>Nacional</th>
				</tr>
				<tr>
					<td>IDINV</td>
					<td>Nome</td>
					<td>Apresentações por convite</td>
					<td>Apresentações como participante</td>
					<td>Seminários Apresentados</td>
					<td>Apresentações por convite</td>
					<td>Apresentações como participante</td>
					<td>Seminários Apresentados</td>
				</tr>
			</thead>
			<tbody>";

	foreach ($dadosDep->apresentacoes as $i => $value){
		if($dadosDep->apresentacoes[$i]->iconfconv!=0 || 
				$dadosDep->apresentacoes[$i]->nconfconv!=0 || 
				$dadosDep->apresentacoes[$i]->iconfpart!=0 ||
				$dadosDep->apresentacoes[$i]->nconfpart!=0 ||
				$dadosDep->apresentacoes[$i]->isem!=0 ||
				$dadosDep->apresentacoes[$i]->nsem!=0){
			echo "<tr>";
				echo "<td>".$dadosDep->apresentacoes[$i]->idinv."</td>";
				echo "<td>".$dadosDep->investigadores[$dadosDep->apresentacoes[$i]->idinv]->nome."</td>";
				echo "<td id='td_apresentacoes_iconfconv_" . $dadosDep->apresentacoes[$i]->id . "'>".$dadosDep->apresentacoes[$i]->iconfconv."</td>";
				echo "<td id='td_apresentacoes_iconfpart_" . $dadosDep->apresentacoes[$i]->id . "'>".$dadosDep->apresentacoes[$i]->iconfpart."</td>";
				echo "<td id='td_apresentacoes_isem_" . $dadosDep->apresentacoes[$i]->id . "'>".$dadosDep->apresentacoes[$i]->isem."</td>";
				echo "<td id='td_apresentacoes_nconfconv_" . $dadosDep->apresentacoes[$i]->id . "'>".$dadosDep->apresentacoes[$i]->nconfconv."</td>";
				echo "<td id='td_apresentacoes_nconfpart_" . $dadosDep->apresentacoes[$i]->id . "'>".$dadosDep->apresentacoes[$i]->nconfpart."</td>";
				echo "<td id='td_apresentacoes_nsem_" . $dadosDep->apresentacoes[$i]->id . "'>".$dadosDep->apresentacoes[$i]->nsem."</td>";
				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-apresentacoes').text('" . $dadosDep->apresentacoes[$i]->id . "');\"></td>";
	        	echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep->apresentacoes[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-apresentacoes').text('" . $dadosDep->apresentacoes[$i]->id . "');apagarApresentacao();return false;\" ></center></td>";
	   		echo "</tr>";
		}
	}
	echo "<tbody>
	</table>
	<p id='chave-apresentacoes' hidden></p>
</div>";

?>