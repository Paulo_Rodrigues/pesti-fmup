<?php

echo "<p><fieldset class='normal'>\n";
echo "<legend>Atividade Científica 2013/<i>Research Activity 2013</i></legend>\n";

echo " <table  class='box-table-a'>
  <tbody>
    <!-- Results table headers -->
    <tr>
      <th colspan='2'>2011</th>
    </tr>";



$ano=2013;


//echo "<p><label for='nome' class='float'>id:</label><br /><input class='inp-textAuto' type='text' name='id_".$ano."' value='".$questionario->actividadecientifica[$ano]->id."'>";
//echo "<p><label for='nome' class='float'>idinv:</label><br /><input class='inp-textAuto' type='text' name='idinv_".$ano."' value='".$questionario->actividadecientifica[$ano]->idinv."'>";
//echo "<p><label for='nome' class='float'>ano:</label><br /><input class='inp-textAuto' type='text' name='ano_".$ano."' value='".$questionario->actividadecientifica[$ano]->ano."'>";
echo "<tr><td>PUBLICAÇÕES INDEXADAS NO ISI</td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de artigos:</label><br /><input class='inp-textAuto' type='text' name='nartigosisi_".$ano."' value='".$questionario->actividadecientifica[$ano]->nartigosisi."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de sumários:</label><br /><input class='inp-textAuto' type='text' name='nsumariosisi_".$ano."' value='".$questionario->actividadecientifica[$ano]->nsumariosisi."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de patentes:</label><br /><input class='inp-textAuto' type='text' name='npatentesisi_".$ano."' value='".$questionario->actividadecientifica[$ano]->npatentesisi."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de citações:</label><br /><input class='inp-textAuto' type='text' name='ncitacoesisi_".$ano."' value='".$questionario->actividadecientifica[$ano]->ncitacoesisi."'></td></tr>";
echo "<tr><td>PUBLICAÇÕES INDEXADAS NA PUBMED</td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de artigos:</label><br /><input class='inp-textAuto' type='text' name='nartigospubmed_".$ano."' value='".$questionario->actividadecientifica[$ano]->nartigospubmed."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de sumários:</label><br /><input class='inp-textAuto' type='text' name='nsumariospubmed_".$ano."' value='".$questionario->actividadecientifica[$ano]->nsumariospubmed."'></td></tr>";
//echo "<tr><td><p><label for='nome' class='float'>nº de patentes:</label><br /><input class='inp-textAuto' type='text' name='npatentespubmed_".$ano."' value='".$questionario->actividadecientifica[$ano]->npatentespubmed."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de citações:</label><br /><input class='inp-textAuto' type='text' name='ncitacoespubmed_".$ano."' value='".$questionario->actividadecientifica[$ano]->ncitacoespubmed."'></td></tr>";
echo "<tr><td>OUTRAS PUBLICAÇÕES</td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de outras publicações indexadas:</label><br /><input class='inp-textAuto' type='text' name='noutraspubind_".$ano."' value='".$questionario->actividadecientifica[$ano]->noutraspubind."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de outras publicações internacionais:</label><br /><input class='inp-textAuto' type='text' name='noutraspubinter_".$ano."' value='".$questionario->actividadecientifica[$ano]->noutraspubinter."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de publicações nacionais:</label><br /><input class='inp-textAuto' type='text' name='npubnacionais_".$ano."' value='".$questionario->actividadecientifica[$ano]->npubnacionais."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de sumários em revistas nacionais:</label><br /><input class='inp-textAuto' type='text' name='nsumariosrevnac_".$ano."' value='".$questionario->actividadecientifica[$ano]->nsumariosrevnac."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de livros internacionais publicados:</label><br /><input class='inp-textAuto' type='text' name='nlivrosintpub_".$ano."' value='".$questionario->actividadecientifica[$ano]->nlivrosintpub."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de livros nacionais publicados:</label><br /><input class='inp-textAuto' type='text' name='nlivrosnacpub_".$ano."' value='".$questionario->actividadecientifica[$ano]->nlivrosnacpub."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de capitulos de livros internacionais publicados:</label><br /><input class='inp-textAuto' type='text' name='ncaplivintpub_".$ano."' value='".$questionario->actividadecientifica[$ano]->ncaplivintpub."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de capitulos de livros nacionais publicados:</label><br /><input class='inp-textAuto' type='text' name='ncaplivnacpub_".$ano."' value='".$questionario->actividadecientifica[$ano]->ncaplivnacpub."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de agradecimentos em publicações indexadas (ISI/PUBMED) :</label><br /><input class='inp-textAuto' type='text' name='nagradecimentos_".$ano."' value='".$questionario->actividadecientifica[$ano]->nagradecimentos."'></td></tr>";
echo "<tr><td>REVISTAS CIENTÍFICAS</td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de arbitragens em revistas internacionais:</label><br /><input class='inp-textAuto' type='text' name='narbitragensrevint_".$ano."' value='".$questionario->actividadecientifica[$ano]->narbitragensrevint."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de arbitragens em revistas nacionais:</label><br /><input class='inp-textAuto' type='text' name='narbitragensrevnac_".$ano."' value='".$questionario->actividadecientifica[$ano]->narbitragensrevnac."'></td></tr>";
//echo "<tr><td><p><label for='nome' class='float'>neditorialrevint:</label><br /><input class='inp-textAuto' type='text' name='neditorialrevint_".$ano."' value='".$questionario->actividadecientifica[$ano]->neditorialrevint."'></td></tr>";
//echo "<tr><td><p><label for='nome' class='float'>neditorialrevnac:</label><br /><input class='inp-textAuto' type='text' name='neditorialrevnac_".$ano."' value='".$questionario->actividadecientifica[$ano]->neditorialrevnac."'></td></tr>";
//echo "<tr><td><p><label for='nome' class='float'>ncorpoeditorialint:</label><br /><input class='inp-textAuto' type='text' name='ncorpoeditorialint_".$ano."' value='".$questionario->actividadecientifica[$ano]->ncorpoeditorialint."'></td></tr>";
//echo "<tr><td><p><label for='nome' class='float'>ncorpoeditorialnac:</label><br /><input class='inp-textAuto' type='text' name='ncorpoeditorialnac_".$ano."' value='".$questionario->actividadecientifica[$ano]->ncorpoeditorialnac."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de revistas internacionais das quais fez parte no corpo editorial:</label><br /><input class='inp-textAuto' type='text' name='ncorpoeditorialrevint_".$ano."' value='".$questionario->actividadecientifica[$ano]->ncorpoeditorialrevint."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de revistas nacionais das quais fez parte no corpo editorial:</label><br /><input class='inp-textAuto' type='text' name='ncorpoeditorialrevnac_".$ano."' value='".$questionario->actividadecientifica[$ano]->ncorpoeditorialrevnac."'></td></tr>";
echo "<tr><td>ORIENTAÇÕES</td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de orientações de pós-doutoramento, a decorrer, como orientador principal:</label><br /><input class='inp-textAuto' type='text' name='norientposdocprin_".$ano."' value='".$questionario->actividadecientifica[$ano]->norientposdocprin."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de orientações de doutoramento, a decorrer, como orientador principal:</label><br /><input class='inp-textAuto' type='text' name='norientdoutprinc_".$ano."' value='".$questionario->actividadecientifica[$ano]->norientdoutprinc."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de orientações de doutoramento terminadas, como orientador principal:</label><br /><input class='inp-textAuto' type='text' name='norientdoutprincfim_".$ano."' value='".$questionario->actividadecientifica[$ano]->norientdoutprincfim."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de orientações de doutoramento, a decorrer, como co-orientador:</label><br /><input class='inp-textAuto' type='text' name='norientdoutco_".$ano."' value='".$questionario->actividadecientifica[$ano]->norientdoutco."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de orientações de doutoramento terminadas, como co-orientador:</label><br /><input class='inp-textAuto' type='text' name='norientdoutcofim_".$ano."' value='".$questionario->actividadecientifica[$ano]->norientdoutcofim."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de orientações de mestrado terminadas, como co-orientador:</label><br /><input class='inp-textAuto' type='text' name='norientmestcofim_".$ano."' value='".$questionario->actividadecientifica[$ano]->norientmestcofim."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de orientações de mestrado, a decorrer, como orientador principal:</label><br /><input class='inp-textAuto' type='text' name='norientmestprinc_".$ano."' value='".$questionario->actividadecientifica[$ano]->norientmestprinc."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de orientações de mestrado terminadas, como orientador principal:</label><br /><input class='inp-textAuto' type='text' name='norientmestprincfim_".$ano."' value='".$questionario->actividadecientifica[$ano]->norientmestprincfim."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de orientações de mestrado, a decorrer, como co-orientador:</label><br /><input class='inp-textAuto' type='text' name='norientmestco_".$ano."' value='".$questionario->actividadecientifica[$ano]->norientmestco."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de orientações de estagiarios, como orientador principal:</label><br /><input class='inp-textAuto' type='text' name='nestagiariosprinc_".$ano."' value='".$questionario->actividadecientifica[$ano]->nestagiariosprinc."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de orientações de estagiarios, como co-orientador:</label><br /><input class='inp-textAuto' type='text' name='nestagiariosco_".$ano."' value='".$questionario->actividadecientifica[$ano]->nestagiariosco."'></td></tr>";
echo "<tr><td>PRÉMIOS</td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de prémios internacionais:</label><br /><input class='inp-textAuto' type='text' name='npremiosint_".$ano."' value='".$questionario->actividadecientifica[$ano]->npremiosint."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de prémios nacionais:</label><br /><input class='inp-textAuto' type='text' name='npremiosnac_".$ano."' value='".$questionario->actividadecientifica[$ano]->npremiosnac."'></td></tr>";
echo "<tr><td>PROJECTOS DE INVESTIGAÇÃO</td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de projectos Europeus, como investigador principal:</label><br /><input class='inp-textAuto' type='text' name='nprojeurinvprinc_".$ano."' value='".$questionario->actividadecientifica[$ano]->nprojeurinvprinc."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de projectos FCT como investigador principal:</label><br /><input class='inp-textAuto' type='text' name='nprojfctinvprinc_".$ano."' value='".$questionario->actividadecientifica[$ano]->nprojfctinvprinc."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de projectos Europeus:</label><br /><input class='inp-textAuto' type='text' name='nprojeuropeu_".$ano."' value='".$questionario->actividadecientifica[$ano]->nprojeuropeu."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de projectos FCT:</label><br /><input class='inp-textAuto' type='text' name='nprojfct_".$ano."' value='".$questionario->actividadecientifica[$ano]->nprojfct."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de outros projectos como investigador principal:</label><br /><input class='inp-textAuto' type='text' name='noutrosprojinvprinc_".$ano."' value='".$questionario->actividadecientifica[$ano]->noutrosprojinvprinc."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de outros projectos:</label><br /><input class='inp-textAuto' type='text' name='noutrosproj_".$ano."' value='".$questionario->actividadecientifica[$ano]->noutrosproj."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de bolseiros como investigador principal:</label><br /><input class='inp-textAuto' type='text' name='nbolsinvprin_".$ano."' value='".$questionario->actividadecientifica[$ano]->nbolsinvprin."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de bolseiros como co-orientador:</label><br /><input class='inp-textAuto' type='text' name='nbolsinvco_".$ano."' value='".$questionario->actividadecientifica[$ano]->nbolsinvco."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de arbitragens efectuadas em projectos internacionais:</label><br /><input class='inp-textAuto' type='text' name='narbitragensprojint_".$ano."' value='".$questionario->actividadecientifica[$ano]->narbitragensprojint."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de arbitragens efectuadas em projectos nacionais:</label><br /><input class='inp-textAuto' type='text' name='narbitragensprojnac_".$ano."' value='".$questionario->actividadecientifica[$ano]->narbitragensprojnac."'></td></tr>";
echo "<tr><td><tr><td>JÚRIS</td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de presenças em júris de agregação como arguente - FMUP:</label><br /><input class='inp-textAuto' type='text' name='njuriagregargfmup_".$ano."' value='".$questionario->actividadecientifica[$ano]->njuriagregargfmup."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de presenças em júris de agregação como arguente - Exterior:</label><br /><input class='inp-textAuto' type='text' name='njuriagregargext_".$ano."' value='".$questionario->actividadecientifica[$ano]->njuriagregargext."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de presenças em júris de agregação como vogal - FMUP:</label><br /><input class='inp-textAuto' type='text' name='njuriagregvogalfmup_".$ano."' value='".$questionario->actividadecientifica[$ano]->njuriagregvogalfmup."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de presenças em júris de agregação como vogal - Exterior:</label><br /><input class='inp-textAuto' type='text' name='njuriagregvogalext_".$ano."' value='".$questionario->actividadecientifica[$ano]->njuriagregvogalext."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de presenças em júris de doutoramento como arguente - FMUP:</label><br /><input class='inp-textAuto' type='text' name='njuridoutargfmup_".$ano."' value='".$questionario->actividadecientifica[$ano]->njuridoutargfmup."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de presenças em júris de doutoramento como arguente - Exterior:</label><br /><input class='inp-textAuto' type='text' name='njuridoutargext_".$ano."' value='".$questionario->actividadecientifica[$ano]->njuridoutargext."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de presenças em júris de doutoramento como vogal - FMUP:</label><br /><input class='inp-textAuto' type='text' name='njuridoutvogalfmup_".$ano."' value='".$questionario->actividadecientifica[$ano]->njuridoutvogalfmup."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de presenças em júris de doutoramento como vogal - Exterior:</label><br /><input class='inp-textAuto' type='text' name='njuridoutvogalext_".$ano."' value='".$questionario->actividadecientifica[$ano]->njuridoutvogalext."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de presenças em júris de mestrado como arguente - FMUP:</label><br /><input class='inp-textAuto' type='text' name='njurimestargfmup_".$ano."' value='".$questionario->actividadecientifica[$ano]->njurimestargfmup."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de presenças em júris de mestrado como arguente - Exterior:</label><br /><input class='inp-textAuto' type='text' name='njurimestargext_".$ano."' value='".$questionario->actividadecientifica[$ano]->njurimestargext."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de presenças em júris de mestrado como vogal - FMUP:</label><br /><input class='inp-textAuto' type='text' name='njurimestvogalfmup_".$ano."' value='".$questionario->actividadecientifica[$ano]->njurimestvogalfmup."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de presenças em júris de mestrado como vogal - Exterior:</label><br /><input class='inp-textAuto' type='text' name='njurimestvogalext_".$ano."' value='".$questionario->actividadecientifica[$ano]->njurimestvogalext."'></td></tr>";
echo "<tr><td>CONFERÊNCIAS e SEMINÁRIOS - ORGANIZAÇÃO E APRESENTAÇÃO</td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de apresentações em conferências internacionais por convite:</label><br /><input class='inp-textAuto' type='text' name='napresconfintconv_".$ano."' value='".$questionario->actividadecientifica[$ano]->napresconfintconv."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de apresentações em conferências internacionais como participante:</label><br /><input class='inp-textAuto' type='text' name='napresconfintpart_".$ano."' value='".$questionario->actividadecientifica[$ano]->napresconfintpart."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de apresentações em conferências nacionais por convite:</label><br /><input class='inp-textAuto' type='text' name='napresconfnacconv_".$ano."' value='".$questionario->actividadecientifica[$ano]->napresconfnacconv."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de apresentações em conferências nacionais como participante:</label><br /><input class='inp-textAuto' type='text' name='napresconfnacpart_".$ano."' value='".$questionario->actividadecientifica[$ano]->napresconfnacpart."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de conferências Europeias organizadas, como membro do Comité Organizador:</label><br /><input class='inp-textAuto' type='text' name='nconforgeuropeiassoc_".$ano."' value='".$questionario->actividadecientifica[$ano]->nconforgeuropeiassoc."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de conferências Europeias organizadas, como membro do Comité Científico:</label><br /><input class='inp-textAuto' type='text' name='nconforgeuropeiasloc_".$ano."' value='".$questionario->actividadecientifica[$ano]->nconforgeuropeiasloc."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de conferências Internacionais organizadas, como membro do Comité Organizador:</label><br /><input class='inp-textAuto' type='text' name='nconforgintloc_".$ano."' value='".$questionario->actividadecientifica[$ano]->nconforgintloc."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de conferências Internacionais organizadas, como membro do Comité Científico:</label><br /><input class='inp-textAuto' type='text' name='nconforgintsoc_".$ano."' value='".$questionario->actividadecientifica[$ano]->nconforgintsoc."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de conferências CPLP organizadas, como membro do Comité Organizador:</label><br /><input class='inp-textAuto' type='text' name='nconforgcplpsoc_".$ano."' value='".$questionario->actividadecientifica[$ano]->nconforgcplpsoc."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de conferências CPLP organizadas, como membro do Comité Científico:</label><br /><input class='inp-textAuto' type='text' name='nconforgcplploc_".$ano."' value='".$questionario->actividadecientifica[$ano]->nconforgcplploc."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de conferências Ibero-Americanas organizadas, como membro do Comité Organizador:</label><br /><input class='inp-textAuto' type='text' name='nconforgiberoameloc_".$ano."' value='".$questionario->actividadecientifica[$ano]->nconforgiberoameloc."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de conferências Ibero-Americanas organizadas, como membro do Comité Científico:</label><br /><input class='inp-textAuto' type='text' name='nconforgiberoamesoc_".$ano."' value='".$questionario->actividadecientifica[$ano]->nconforgiberoamesoc."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de conferências Nacionais organizadas, como membro do Comité Organizador:</label><br /><input class='inp-textAuto' type='text' name='nconforgnacloc_".$ano."' value='".$questionario->actividadecientifica[$ano]->nconforgnacloc."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de conferências Nacionais organizadas, como membro do Comité Científico:</label><br /><input class='inp-textAuto' type='text' name='nconforgnacsoc_".$ano."' value='".$questionario->actividadecientifica[$ano]->nconforgnacsoc."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de seminários internacionais organizados:</label><br /><input class='inp-textAuto' type='text' name='nseminariosorgint_".$ano."' value='".$questionario->actividadecientifica[$ano]->nseminariosorgint."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de seminários nacionais organizados:</label><br /><input class='inp-textAuto' type='text' name='nseminariosorgnac_".$ano."' value='".$questionario->actividadecientifica[$ano]->nseminariosorgnac."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de seminarios internacionais como participante:</label><br /><input class='inp-textAuto' type='text' name='nseminariosint_".$ano."' value='".$questionario->actividadecientifica[$ano]->nseminariosint."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de seminarios nacionais como participante:</label><br /><input class='inp-textAuto' type='text' name='nseminariosnac_".$ano."' value='".$questionario->actividadecientifica[$ano]->nseminariosnac."'></td></tr>";
echo "<tr><td>SOCIEDADES CIENTÍFICAS</td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de sociedades científicas internacionais onde fez parte da direção:</label><br /><input class='inp-textAuto' type='text' name='nsoccientintdir_".$ano."' value='".$questionario->actividadecientifica[$ano]->nsoccientintdir."'></td></tr>";
echo "<tr><td><p><label for='nome' class='float'>nº de sociedades científicas nacionais onde fez parte da direção:</label><br /><input class='inp-textAuto' type='text' name='nsoccientnacdir_".$ano."' value='".$questionario->actividadecientifica[$ano]->nsoccientnacdir."'></td></tr>";
	
echo "</tbody></table>";
echo "</fieldset>";
	/*
	
	
	
	
	echo "<p><label for='anoagregacao' class='float'>Ano a submeter-se para provas Agregação:</label>";
	echo "<input class='inp-textAuto' type='text' name='anoagregacao' size='5' value='".$questionario->habilitacoes[$i]->anoagregacao."'></p>\n";
	
	echo "<p><label for='agregacao2011' class='float'>Provas de Agregação em 2011:</label><br />\n";
	echo "<p>Sim:";
	echo "<input class='inp-textAuto' type='text' name='agregacao2011' size='5' value='".$questionario->habilitacoes[$i]->agregacao2011."'>";
	echo "Não:";
	echo "<input class='inp-textAuto' type='text' name='agregacao2011' size='5' value='".$questionario->habilitacoes[$i]->agregacao2011."'></p>\n";
	*/
	?>