<?php

/**
* Autor: Paulo Rodrigues (prodrigues@med.up.pt/paulomiguelarodrigues@gmail.com)
* Data: 21/04/2014
* Alterações: Alterados os cabeçalhos das informações para poderem surgir no accordion. 
*/

echo "<h3>Publicações PubMed</h3>\n";
echo "<div id='publicacoesPUBMED'>";

$outJPUBMED="";
$outJAPUBMED="";
$outPRPPUBMED="";
$outPRPNPUBMED="";
$outCPPUBMED="";
$outCPNPUBMED="";
$outOAPUBMED="";
$outSPUBMED="";
$outJNPUBMED="";
$outSNPUBMED="";
$outLNPUBMED="";
$outCLNPUBMED="";
$outLPUBMED="";
$outCLPUBMED="";
$outPPUBMED="";
$outAPUBMED="";

$outRestoPUBMED="";

$tcols="<thead>
		    <tr>
		    	<th>ID</th><th>Nome</th>
				<th>ANO</th>
				<th>TIPO</th>
				<th>NOMEPUBLICACAO</th>
				<th>ISSN</th>
				<th>ISSN_LINKING</th>
				<th>ISSN_ELECTRONIC</th>
				<th>ISBN</th>
				<th>TITULO</th>
				<th>AUTORES</th>
				<th>VOLUME</th>
				<th>ISSUE</th>
				<th>AR</th>
				<th>COLECAO</th>
				<th>PRIMPAGINA</th>
				<th>ULTPAGINA</th>
				<th>IFACTOR</th>
				<th>CITACOES</th>
				<th>NPATENTE</th>
				<th>DATAPATENTE</th>
				<th>IPC</th>
				<th>AREA</th>
				<th>ESTADO</th>
			</tr>
		</thead>";					

$tcolsJPUBMED="<thead>
				    <tr>
						<th>IDINV</th><th>Nome</th>
						<th>Revista (Journal)</th>
						<th>ISSN</th>
						<th>Título</th>
						<th>Autores</th>
						<th>Volume</th>
						<th>Issue</th>
						<th>1ª Pág.</th>
						<th>Última Pág.</th>
						<th>Estado</th>
					</tr>
			</thead>";

$tcolsCONFPUBMED="<thead>
				    <tr>
						<th>IDINV</th><th>Nome</th>
						<th>ISBN</th>
						<th>Nome Publicação</th>
						<th>Editores</th>
						<th>Colecção</th>
						<th>Volume</th>
						<th>Título do Artigo</th>
						<th>Autores</th>
						<th>1ª Pág.</th>
						<th>Últ- Página</th>
						<th>Estado</th>
					</tr>
				</thead>";

$tcolsCLIVPUBMED="<thead>
				    <tr>
						<th>IDINV</th><th>Nome</th>
						<th>ISBN</th>
						<th>NOME PUBLICACAO</th>
						<th>EDITORA</th>		
						<th>AUTORES</th>
						<th>TÍTULO ARTIGO</th>
						<th>PRIMEIRA PÁG.</th>
						<th>ÚLTIMA PÁG.</th>
						<th>Estado</th>
					</tr>
				</thead>";

$tcolsPATPUBMED="<thead>
				    <tr>
						<th>IDINV</th><th>Nome</th>
						<th>Nº Patente</th>
						<th>IPC</th>
						<th>Título</th>
						<th>Autores</th>		
						<th>Data da Patente</th>
						<th>Estado</th>
					</tr>
				</thead>";

$tcolsLIVPUBMED="<thead>
				    <tr>
						<th>IDINV</th><th>Nome</th>
						<th>ISBN</th>
						<th>TÍTULO</th>
						<th>AUTORES</th>		
						<th>EDITOR (Publisher)</th>
						<th>Estado</th>
					</tr>
				</thead>";
			    
	foreach ($dadosDep->publicacoesPUBMED as $i => $value){
			
			$tipo=$dadosDep->publicacoesPUBMED[$i]->tipofmup;
			
	switch($tipo){
			case 'J': { 
	    		$outJPUBMED=$outJPUBMED."<tr>";

					$outJPUBMED=$outJPUBMED."<td>";
					$outJPUBMED=$outJPUBMED.$dadosDep->publicacoesPUBMED[$i]->idinv;
		    		$outJPUBMED=$outJPUBMED."</td>";

		    		$outJPUBMED=$outJPUBMED."<td>".$dadosDep->investigadores[$dadosDep->publicacoesPUBMED[$i]->idinv]->nome."</td>";		    		
		    		
		    		$outJPUBMED=$outJPUBMED."<td>";
					$outJPUBMED=$outJPUBMED.$dadosDep->publicacoesPUBMED[$i]->nomepublicacao;
		    		$outJPUBMED=$outJPUBMED."</td>";

		    		$outJPUBMED=$outJPUBMED."<td>";
					$outJPUBMED=$outJPUBMED.$dadosDep->publicacoesPUBMED[$i]->issn;
		    		$outJPUBMED=$outJPUBMED."</td>";
		    		
		    		$outJPUBMED=$outJPUBMED."<td>";
					$outJPUBMED=$outJPUBMED.$dadosDep->publicacoesPUBMED[$i]->titulo;
		    		$outJPUBMED=$outJPUBMED."</td>";

		    		$outJPUBMED=$outJPUBMED."<td>";
					$outJPUBMED=$outJPUBMED.$dadosDep->publicacoesPUBMED[$i]->autores;
		    		$outJPUBMED=$outJPUBMED."</td>";

					$outJPUBMED=$outJPUBMED."<td>";
					$outJPUBMED=$outJPUBMED.$dadosDep->publicacoesPUBMED[$i]->volume;
		    		$outJPUBMED=$outJPUBMED."</td>";

					$outJPUBMED=$outJPUBMED."<td>";
					$outJPUBMED=$outJPUBMED.$dadosDep->publicacoesPUBMED[$i]->issue;
		    		$outJPUBMED=$outJPUBMED."</td>";

					$outJPUBMED=$outJPUBMED."<td>";
					$outJPUBMED=$outJPUBMED.$dadosDep->publicacoesPUBMED[$i]->primpagina;
		    		$outJPUBMED=$outJPUBMED."</td>";	

					$outJPUBMED=$outJPUBMED."<td>";
					$outJPUBMED=$outJPUBMED.$dadosDep->publicacoesPUBMED[$i]->ultpagina;
		    		$outJPUBMED=$outJPUBMED."</td>";

		    		$outJPUBMED=$outJPUBMED."<td>";
					$outJPUBMED=$outJPUBMED.getTipoEstado($dadosDep->publicacoesPUBMED[$i]->estado);
		    		$outJPUBMED=$outJPUBMED."</td>";	

	    		$outJPUBMED=$outJPUBMED."</tr>";
			}
			break;
			case 'PRP': { 
	    		$outPRPPUBMED=$outPRPPUBMED."<tr>";

					$outPRPPUBMED=$outPRPPUBMED."<td>";
					$outPRPPUBMED=$outPRPPUBMED.$dadosDep->publicacoesPUBMED[$i]->idinv;
		    		$outPRPPUBMED=$outPRPPUBMED."</td>";

		    		$outPRPPUBMED=$outPRPPUBMED."<td>".$dadosDep->investigadores[$dadosDep->publicacoesPUBMED[$i]->idinv]->nome."</td>";
		    		
		    		$outPRPPUBMED=$outPRPPUBMED."<td>";
					$outPRPPUBMED=$outPRPPUBMED.$dadosDep->publicacoesPUBMED[$i]->nomepublicacao;
		    		$outPRPPUBMED=$outPRPPUBMED."</td>";

		    		$outPRPPUBMED=$outPRPPUBMED."<td>";
					$outPRPPUBMED=$outPRPPUBMED.$dadosDep->publicacoesPUBMED[$i]->issn;
		    		$outPRPPUBMED=$outPRPPUBMED."</td>";
		    		
		    		$outPRPPUBMED=$outPRPPUBMED."<td>";
					$outPRPPUBMED=$outPRPPUBMED.$dadosDep->publicacoesPUBMED[$i]->titulo;
		    		$outPRPPUBMED=$outPRPPUBMED."</td>";

		    		$outPRPPUBMED=$outPRPPUBMED."<td>";
					$outPRPPUBMED=$outPRPPUBMED.$dadosDep->publicacoesPUBMED[$i]->autores;
		    		$outPRPPUBMED=$outPRPPUBMED."</td>";

					$outPRPPUBMED=$outPRPPUBMED."<td>";
					$outPRPPUBMED=$outPRPPUBMED.$dadosDep->publicacoesPUBMED[$i]->volume;
		    		$outPRPPUBMED=$outPRPPUBMED."</td>";

					$outPRPPUBMED=$outPRPPUBMED."<td>";
					$outPRPPUBMED=$outPRPPUBMED.$dadosDep->publicacoesPUBMED[$i]->issue;
		    		$outPRPPUBMED=$outPRPPUBMED."</td>";
					
					$outPRPPUBMED=$outPRPPUBMED."<td>";
					$outPRPPUBMED=$outPRPPUBMED.$dadosDep->publicacoesPUBMED[$i]->primpagina;
		    		$outPRPPUBMED=$outPRPPUBMED."</td>";

					$outPRPPUBMED=$outPRPPUBMED."<td>";
					$outPRPPUBMED=$outPRPPUBMED.$dadosDep->publicacoesPUBMED[$i]->ultpagina;
		    		$outPRPPUBMED=$outPRPPUBMED."</td>";
		    		
		    		$outPRPPUBMED=$outPRPPUBMED."<td>";
					$outPRPPUBMED=$outPRPPUBMED.getTipoEstado($dadosDep->publicacoesPUBMED[$i]->estado);
		    		$outPRPPUBMED=$outPRPPUBMED."</td>";		    				    		
	    		$outPRPPUBMED=$outPRPPUBMED."</tr>";
			}
			break;
			case 'JA': { 
	    		$outJAPUBMED=$outJAPUBMED."<tr>";

					$outJAPUBMED=$outJAPUBMED."<td>";
					$outJAPUBMED=$outJAPUBMED.$dadosDep->publicacoesPUBMED[$i]->idinv;
		    		$outJAPUBMED=$outJAPUBMED."</td>";

		    		$outJAPUBMED=$outJAPUBMED."<td>".$dadosDep->investigadores[$dadosDep->publicacoesPUBMED[$i]->idinv]->nome."</td>";
		    		
		    		$outJAPUBMED=$outJAPUBMED."<td>";
					$outJAPUBMED=$outJAPUBMED.$dadosDep->publicacoesPUBMED[$i]->nomepublicacao;
		    		$outJAPUBMED=$outJAPUBMED."</td>";

		    		$outJAPUBMED=$outJAPUBMED."<td>";
					$outJAPUBMED=$outJAPUBMED.$dadosDep->publicacoesPUBMED[$i]->issn;
		    		$outJAPUBMED=$outJAPUBMED."</td>";

		    		$outJAPUBMED=$outJAPUBMED."<td>";
					$outJAPUBMED=$outJAPUBMED.$dadosDep->publicacoesPUBMED[$i]->titulo;
		    		$outJAPUBMED=$outJAPUBMED."</td>";

		    		$outJAPUBMED=$outJAPUBMED."<td>";
					$outJAPUBMED=$outJAPUBMED.$dadosDep->publicacoesPUBMED[$i]->autores;
		    		$outJAPUBMED=$outJAPUBMED."</td>";

					$outJAPUBMED=$outJAPUBMED."<td>";
					$outJAPUBMED=$outJAPUBMED.$dadosDep->publicacoesPUBMED[$i]->volume;
		    		$outJAPUBMED=$outJAPUBMED."</td>";

					$outJAPUBMED=$outJAPUBMED."<td>";
					$outJAPUBMED=$outJAPUBMED.$dadosDep->publicacoesPUBMED[$i]->issue;
		    		$outJAPUBMED=$outJAPUBMED."</td>";

					$outJAPUBMED=$outJAPUBMED."<td>";
					$outJAPUBMED=$outJAPUBMED.$dadosDep->publicacoesPUBMED[$i]->primpagina;
		    		$outJAPUBMED=$outJAPUBMED."</td>";			
					$outJAPUBMED=$outJAPUBMED."<td>";
					$outJAPUBMED=$outJAPUBMED.$dadosDep->publicacoesPUBMED[$i]->ultpagina;
		    		$outJAPUBMED=$outJAPUBMED."</td>";

		    		$outJAPUBMED=$outJAPUBMED."<td>";
					$outJAPUBMED=$outJAPUBMED.getTipoEstado($dadosDep->publicacoesPUBMED[$i]->estado);
		    		$outJAPUBMED=$outJAPUBMED."</td>";	

	    		$outJAPUBMED=$outJAPUBMED."</tr>";
			}
			break;
			case 'PRPN': { // Opção de entrada
	    		$outPRPNPUBMED=$outPRPNPUBMED."<tr>";

					$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$dadosDep->publicacoesPUBMED[$i]->idinv;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";

		    		$outPRPNPUBMED=$outPRPNPUBMED."<td>".$dadosDep->investigadores[$dadosDep->publicacoesPUBMED[$i]->idinv]->nome."</td>";
		    		
		    		$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$dadosDep->publicacoesPUBMED[$i]->ano;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";

		    		$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$dadosDep->publicacoesPUBMED[$i]->nomepublicacao;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";

		    		$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$dadosDep->publicacoesPUBMED[$i]->issn;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";

		    		$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$dadosDep->publicacoesPUBMED[$i]->isbn;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";

		    		$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$dadosDep->publicacoesPUBMED[$i]->titulo;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";

		    		$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$dadosDep->publicacoesPUBMED[$i]->autores;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";

					$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$dadosDep->publicacoesPUBMED[$i]->volume;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";

					$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$dadosDep->publicacoesPUBMED[$i]->issue;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";

					$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$dadosDep->publicacoesPUBMED[$i]->ar;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";

					$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$dadosDep->publicacoesPUBMED[$i]->colecao;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";

					$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$dadosDep->publicacoesPUBMED[$i]->primpagina;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";	

					$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$dadosDep->publicacoesPUBMED[$i]->ultpagina;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";

		    		$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$dadosDep->publicacoesPUBMED[$i]->ifactor;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";

		    		$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$dadosDep->publicacoesPUBMED[$i]->citacoes;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";

		    		$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$dadosDep->publicacoesPUBMED[$i]->conftitle;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";

		    		$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$dadosDep->publicacoesPUBMED[$i]->language;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";

		    		$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.$dadosDep->publicacoesPUBMED[$i]->editor;
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";

		    		$outPRPNPUBMED=$outPRPNPUBMED."<td>";
					$outPRPNPUBMED=$outPRPNPUBMED.getTipoEstado($dadosDep->publicacoesPUBMED[$i]->estado);
		    		$outPRPNPUBMED=$outPRPNPUBMED."</td>";		    				    		
	    		$outPRPNPUBMED=$outPRPNPUBMED."</tr>";
			}
			break;
			case 'CP': { 
	    		$outCPPUBMED=$outCPPUBMED."<tr>";

		    		$outCPPUBMED=$outCPPUBMED."<td>";
		    		$outCPPUBMED=$outCPPUBMED.$dadosDep->publicacoesPUBMED[$i]->idinv;
		    		$outCPPUBMED=$outCPPUBMED."</td>";

		    		$outCPPUBMED=$outCPPUBMED."<td>".$dadosDep->investigadores[$dadosDep->publicacoesPUBMED[$i]->idinv]->nome."</td>";

		    		$outCPPUBMED=$outCPPUBMED."<td>";
					$outCPPUBMED=$outCPPUBMED.$dadosDep->publicacoesPUBMED[$i]->nomepublicacao;
		    		$outCPPUBMED=$outCPPUBMED."</td>";	

		    		$outCPPUBMED=$outCPPUBMED."<td>";
					$outCPPUBMED=$outCPPUBMED.$dadosDep->publicacoesPUBMED[$i]->isbn;
		    		$outCPPUBMED=$outCPPUBMED."</td>";	

		    		$outCPPUBMED=$outCPPUBMED."<td>";
					$outCPPUBMED=$outCPPUBMED.$dadosDep->publicacoesPUBMED[$i]->editor;
		    		$outCPPUBMED=$outCPPUBMED."</td>";	

					$outCPPUBMED=$outCPPUBMED."<td>";
					$outCPPUBMED=$outCPPUBMED.$dadosDep->publicacoesPUBMED[$i]->colecao;
		    		$outCPPUBMED=$outCPPUBMED."</td>";

					$outCPPUBMED=$outCPPUBMED."<td>";
					$outCPPUBMED=$outCPPUBMED.$dadosDep->publicacoesPUBMED[$i]->volume;
		    		$outCPPUBMED=$outCPPUBMED."</td>";

		    		$outCPPUBMED=$outCPPUBMED."<td>";
					$outCPPUBMED=$outCPPUBMED.$dadosDep->publicacoesPUBMED[$i]->titulo;
		    		$outCPPUBMED=$outCPPUBMED."</td>";	

		    		$outCPPUBMED=$outCPPUBMED."<td>";
					$outCPPUBMED=$outCPPUBMED.$dadosDep->publicacoesPUBMED[$i]->autores;
		    		$outCPPUBMED=$outCPPUBMED."</td>";	

					$outCPPUBMED=$outCPPUBMED."<td>";
					$outCPPUBMED=$outCPPUBMED.$dadosDep->publicacoesPUBMED[$i]->primpagina;
		    		$outCPPUBMED=$outCPPUBMED."</td>";	

					$outCPPUBMED=$outCPPUBMED."<td>";
					$outCPPUBMED=$outCPPUBMED.$dadosDep->publicacoesPUBMED[$i]->ultpagina;
		    		$outCPPUBMED=$outCPPUBMED."</td>";
		    		
		    		$outCPPUBMED=$outCPPUBMED."<td>";
					$outCPPUBMED=$outCPPUBMED.getTipoEstado($dadosDep->publicacoesPUBMED[$i]->estado);
		    		$outCPPUBMED=$outCPPUBMED."</td>";		    				    		
	    		$outCPPUBMED=$outCPPUBMED."</tr>";
			}
			break;
			case 'CPN': { 
	    		$outCPNPUBMED=$outCPNPUBMED."<tr>";

		    		$outCPNPUBMED=$outCPNPUBMED."<td>";
		    		$outCPNPUBMED=$outCPNPUBMED.$dadosDep->publicacoesPUBMED[$i]->idinv;
		    		$outCPNPUBMED=$outCPNPUBMED."</td>";

		    		$outCPNPUBMED=$outCPNPUBMED."<td>".$dadosDep->investigadores[$dadosDep->publicacoesPUBMED[$i]->idinv]->nome."</td>";

		    		$outCPNPUBMED=$outCPNPUBMED."<td>";
					$outCPNPUBMED=$outCPNPUBMED.$dadosDep->publicacoesPUBMED[$i]->nomepublicacao;
		    		$outCPNPUBMED=$outCPNPUBMED."</td>";

		    		$outCPNPUBMED=$outCPNPUBMED."<td>";
					$outCPNPUBMED=$outCPNPUBMED.$dadosDep->publicacoesPUBMED[$i]->isbn;
		    		$outCPNPUBMED=$outCPNPUBMED."</td>";	

		    		$outCPNPUBMED=$outCPNPUBMED."<td>";
					$outCPNPUBMED=$outCPNPUBMED.$dadosDep->publicacoesPUBMED[$i]->editor;
		    		$outCPNPUBMED=$outCPNPUBMED."</td>";

					$outCPNPUBMED=$outCPNPUBMED."<td>";
					$outCPNPUBMED=$outCPNPUBMED.$dadosDep->publicacoesPUBMED[$i]->colecao;
		    		$outCPNPUBMED=$outCPNPUBMED."</td>";

					$outCPNPUBMED=$outCPNPUBMED."<td>";
					$outCPNPUBMED=$outCPNPUBMED.$dadosDep->publicacoesPUBMED[$i]->volume;
		    		$outCPNPUBMED=$outCPNPUBMED."</td>";

		    		$outCPNPUBMED=$outCPNPUBMED."<td>";
					$outCPNPUBMED=$outCPNPUBMED.$dadosDep->publicacoesPUBMED[$i]->titulo;
		    		$outCPNPUBMED=$outCPNPUBMED."</td>";

		    		$outCPNPUBMED=$outCPNPUBMED."<td>";
					$outCPNPUBMED=$outCPNPUBMED.$dadosDep->publicacoesPUBMED[$i]->autores;
		    		$outCPNPUBMED=$outCPNPUBMED."</td>";

					$outCPNPUBMED=$outCPNPUBMED."<td>";
					$outCPNPUBMED=$outCPNPUBMED.$dadosDep->publicacoesPUBMED[$i]->primpagina;
		    		$outCPNPUBMED=$outCPNPUBMED."</td>";

					$outCPNPUBMED=$outCPNPUBMED."<td>";
					$outCPNPUBMED=$outCPNPUBMED.$dadosDep->publicacoesPUBMED[$i]->ultpagina;
		    		$outCPNPUBMED=$outCPNPUBMED."</td>";

		    		$outCPNPUBMED=$outCPNPUBMED."<td>";
					$outCPNPUBMED=$outCPNPUBMED.getTipoEstado($dadosDep->publicacoesPUBMED[$i]->estado);
		    		$outCPNPUBMED=$outCPNPUBMED."</td>";		    				    		
	    		$outCPNPUBMED=$outCPNPUBMED."</tr>";
			}
			break;
			case 'OA': { 
	    		$outOAPUBMED=$outOAPUBMED."<tr>";
		    		$outOAPUBMED=$outOAPUBMED."<td>";
		    		$outOAPUBMED=$outOAPUBMED.$dadosDep->publicacoesPUBMED[$i]->idinv;
		    		$outOAPUBMED=$outOAPUBMED."</td>";

		    		$outOAPUBMED=$outOAPUBMED."<td>".$dadosDep->investigadores[$dadosDep->publicacoesPUBMED[$i]->idinv]->nome."</td>";

		    		$outOAPUBMED=$outOAPUBMED."<td>";
					$outOAPUBMED=$outOAPUBMED.$dadosDep->publicacoesPUBMED[$i]->nomepublicacao;
		    		$outOAPUBMED=$outOAPUBMED."</td>";		

		    		$outOAPUBMED=$outOAPUBMED."<td>";
					$outOAPUBMED=$outOAPUBMED.$dadosDep->publicacoesPUBMED[$i]->isbn;
		    		$outOAPUBMED=$outOAPUBMED."</td>";	

		    		$outOAPUBMED=$outOAPUBMED."<td>";
					$outOAPUBMED=$outOAPUBMED.$dadosDep->publicacoesPUBMED[$i]->editor;
		    		$outOAPUBMED=$outOAPUBMED."</td>";	

					$outOAPUBMED=$outOAPUBMED."<td>";
					$outOAPUBMED=$outOAPUBMED.$dadosDep->publicacoesPUBMED[$i]->colecao;
		    		$outOAPUBMED=$outOAPUBMED."</td>";

					$outOAPUBMED=$outOAPUBMED."<td>";
					$outOAPUBMED=$outOAPUBMED.$dadosDep->publicacoesPUBMED[$i]->volume;
		    		$outOAPUBMED=$outOAPUBMED."</td>";

		    		$outOAPUBMED=$outOAPUBMED."<td>";
					$outOAPUBMED=$outOAPUBMED.$dadosDep->publicacoesPUBMED[$i]->titulo;
		    		$outOAPUBMED=$outOAPUBMED."</td>";	

		    		$outOAPUBMED=$outOAPUBMED."<td>";
					$outOAPUBMED=$outOAPUBMED.$dadosDep->publicacoesPUBMED[$i]->autores;
		    		$outOAPUBMED=$outOAPUBMED."</td>";	

					$outOAPUBMED=$outOAPUBMED."<td>";
					$outOAPUBMED=$outOAPUBMED.$dadosDep->publicacoesPUBMED[$i]->primpagina;
		    		$outOAPUBMED=$outOAPUBMED."</td>";	

					$outOAPUBMED=$outOAPUBMED."<td>";
					$outOAPUBMED=$outOAPUBMED.$dadosDep->publicacoesPUBMED[$i]->ultpagina;
		    		$outOAPUBMED=$outOAPUBMED."</td>";

		    		$outOAPUBMED=$outOAPUBMED."<td>";
					$outOAPUBMED=$outOAPUBMED.getTipoEstado($dadosDep->publicacoesPUBMED[$i]->estado);
		    		$outOAPUBMED=$outOAPUBMED."</td>";		    				    		
	    		$outOAPUBMED=$outOAPUBMED."</tr>";
			}
			break;
			case 'S': { 
		    		$outSPUBMED=$outSPUBMED."<tr>";

					$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$dadosDep->publicacoesPUBMED[$i]->idinv;
		    		$outSPUBMED=$outSPUBMED."</td>";

		    		$outSPUBMED=$outSPUBMED."<td>".$dadosDep->investigadores[$dadosDep->publicacoesPUBMED[$i]->idinv]->nome."</td>";

		    		$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$dadosDep->publicacoesPUBMED[$i]->ano;
		    		$outSPUBMED=$outSPUBMED."</td>";

		    		$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$dadosDep->publicacoesPUBMED[$i]->nomepublicacao;
		    		$outSPUBMED=$outSPUBMED."</td>";

		    		$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$dadosDep->publicacoesPUBMED[$i]->issn;
		    		$outSPUBMED=$outSPUBMED."</td>";

		    		$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$dadosDep->publicacoesPUBMED[$i]->isbn;
		    		$outSPUBMED=$outSPUBMED."</td>";

		    		$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$dadosDep->publicacoesPUBMED[$i]->titulo;
		    		$outSPUBMED=$outSPUBMED."</td>";

		    		$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$dadosDep->publicacoesPUBMED[$i]->autores;
		    		$outSPUBMED=$outSPUBMED."</td>";

					$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$dadosDep->publicacoesPUBMED[$i]->volume;
		    		$outSPUBMED=$outSPUBMED."</td>";

					$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$dadosDep->publicacoesPUBMED[$i]->issue;
		    		$outSPUBMED=$outSPUBMED."</td>";

					$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$dadosDep->publicacoesPUBMED[$i]->ar;
		    		$outSPUBMED=$outSPUBMED."</td>";

					$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$dadosDep->publicacoesPUBMED[$i]->colecao;
		    		$outSPUBMED=$outSPUBMED."</td>";

					$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$dadosDep->publicacoesPUBMED[$i]->primpagina;
		    		$outSPUBMED=$outSPUBMED."</td>";

					$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$dadosDep->publicacoesPUBMED[$i]->ultpagina;
		    		$outSPUBMED=$outSPUBMED."</td>";

		    		$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$dadosDep->publicacoesPUBMED[$i]->ifactor;
		    		$outSPUBMED=$outSPUBMED."</td>";

		    		$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$dadosDep->publicacoesPUBMED[$i]->citacoes;
		    		$outSPUBMED=$outSPUBMED."</td>";

		    		$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$dadosDep->publicacoesPUBMED[$i]->conftitle;
		    		$outSPUBMED=$outSPUBMED."</td>";

		    		$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$dadosDep->publicacoesPUBMED[$i]->language;
		    		$outSPUBMED=$outSPUBMED."</td>";

		    		$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.$dadosDep->publicacoesPUBMED[$i]->editor;
		    		$outSPUBMED=$outSPUBMED."</td>";

		    		$outSPUBMED=$outSPUBMED."<td>";
					$outSPUBMED=$outSPUBMED.getTipoEstado($dadosDep->publicacoesPUBMED[$i]->estado);
		    		$outSPUBMED=$outSPUBMED."</td>";		    				    		
	    		$outSPUBMED=$outSPUBMED."</tr>";
			}
			break;
			case 'JN': {
	    		$outJNPUBMED=$outJNPUBMED."<tr>";
	    
					$outJNPUBMED=$outJNPUBMED."<td>";
					$outJNPUBMED=$outJNPUBMED.$dadosDep->publicacoesPUBMED[$i]->idinv;
		    		$outJNPUBMED=$outJNPUBMED."</td>";

		    		$outJNPUBMED=$outJNPUBMED."<td>".$dadosDep->investigadores[$dadosDep->publicacoesPUBMED[$i]->idinv]->nome."</td>";

		    		$outJNPUBMED=$outJNPUBMED."<td>";
					$outJNPUBMED=$outJNPUBMED.$dadosDep->publicacoesPUBMED[$i]->nomepublicacao;
		    		$outJNPUBMED=$outJNPUBMED."</td>";

		    		$outJNPUBMED=$outJNPUBMED."<td>";
					$outJNPUBMED=$outJNPUBMED.$dadosDep->publicacoesPUBMED[$i]->issn;
		    		$outJNPUBMED=$outJNPUBMED."</td>";
		    		
		    		$outJNPUBMED=$outJNPUBMED."<td>";
					$outJNPUBMED=$outJNPUBMED.$dadosDep->publicacoesPUBMED[$i]->titulo;
		    		$outJNPUBMED=$outJNPUBMED."</td>";

		    		$outJNPUBMED=$outJNPUBMED."<td>";
					$outJNPUBMED=$outJNPUBMED.$dadosDep->publicacoesPUBMED[$i]->autores;
		    		$outJNPUBMED=$outJNPUBMED."</td>";

					$outJNPUBMED=$outJNPUBMED."<td>";
					$outJNPUBMED=$outJNPUBMED.$dadosDep->publicacoesPUBMED[$i]->volume;
		    		$outJNPUBMED=$outJNPUBMED."</td>";

					$outJNPUBMED=$outJNPUBMED."<td>";
					$outJNPUBMED=$outJNPUBMED.$dadosDep->publicacoesPUBMED[$i]->issue;
		    		$outJNPUBMED=$outJNPUBMED."</td>";

					$outJNPUBMED=$outJNPUBMED."<td>";
					$outJNPUBMED=$outJNPUBMED.$dadosDep->publicacoesPUBMED[$i]->primpagina;
		    		$outJNPUBMED=$outJNPUBMED."</td>";	

					$outJNPUBMED=$outJNPUBMED."<td>";
					$outJNPUBMED=$outJNPUBMED.$dadosDep->publicacoesPUBMED[$i]->ultpagina;
		    		$outJNPUBMED=$outJNPUBMED."</td>";

		    		$outJNPUBMED=$outJNPUBMED."<td>";
					$outJNPUBMED=$outJNPUBMED.getTipoEstado($dadosDep->publicacoesPUBMED[$i]->estado);
		    		$outJNPUBMED=$outJNPUBMED."</td>";		    				    		
	    		$outJNPUBMED=$outJNPUBMED."</tr>";
			}
			break;
			case 'SN': { 
	    		$outSNPUBMED=$outSNPUBMED."<tr>";

					$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$dadosDep->publicacoesPUBMED[$i]->idinv;
		    		$outSNPUBMED=$outSNPUBMED."</td>";

		    		$outSNPUBMED=$outSNPUBMED."<td>".$dadosDep->investigadores[$dadosDep->publicacoesPUBMED[$i]->idinv]->nome."</td>";

		    		$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$dadosDep->publicacoesPUBMED[$i]->ano;
		    		$outSNPUBMED=$outSNPUBMED."</td>";

		    		$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$dadosDep->publicacoesPUBMED[$i]->nomepublicacao;
		    		$outSNPUBMED=$outSNPUBMED."</td>";

		    		$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$dadosDep->publicacoesPUBMED[$i]->issn;
		    		$outSNPUBMED=$outSNPUBMED."</td>";

		    		$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$dadosDep->publicacoesPUBMED[$i]->isbn;
		    		$outSNPUBMED=$outSNPUBMED."</td>";

		    		$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$dadosDep->publicacoesPUBMED[$i]->titulo;
		    		$outSNPUBMED=$outSNPUBMED."</td>";

		    		$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$dadosDep->publicacoesPUBMED[$i]->autores;
		    		$outSNPUBMED=$outSNPUBMED."</td>";

					$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$dadosDep->publicacoesPUBMED[$i]->volume;
		    		$outSNPUBMED=$outSNPUBMED."</td>";

					$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$dadosDep->publicacoesPUBMED[$i]->issue;
		    		$outSNPUBMED=$outSNPUBMED."</td>";

					$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$dadosDep->publicacoesPUBMED[$i]->ar;
		    		$outSNPUBMED=$outSNPUBMED."</td>";

					$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$dadosDep->publicacoesPUBMED[$i]->colecao;
		    		$outSNPUBMED=$outSNPUBMED."</td>";

					$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$dadosDep->publicacoesPUBMED[$i]->primpagina;
		    		$outSNPUBMED=$outSNPUBMED."</td>";

					$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$dadosDep->publicacoesPUBMED[$i]->ultpagina;
		    		$outSNPUBMED=$outSNPUBMED."</td>";

		    		$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$dadosDep->publicacoesPUBMED[$i]->ifactor;
		    		$outSNPUBMED=$outSNPUBMED."</td>";

		    		$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$dadosDep->publicacoesPUBMED[$i]->citacoes;
		    		$outSNPUBMED=$outSNPUBMED."</td>";

		    		$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$dadosDep->publicacoesPUBMED[$i]->conftitle;
		    		$outSNPUBMED=$outSNPUBMED."</td>";

		    		$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$dadosDep->publicacoesPUBMED[$i]->language;
		    		$outSNPUBMED=$outSNPUBMED."</td>";

		    		$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$dadosDep->publicacoesPUBMED[$i]->editor;
		    		$outSNPUBMED=$outSNPUBMED."</td>";

		    		$outSNPUBMED=$outSNPUBMED."<td>";
					$outSNPUBMED=$outSNPUBMED.$dadosDep->publicacoesPUBMED[$i]->tipofmup;
		    		$outSNPUBMED=$outSNPUBMED."</td>";

		    		$outSNPUBMED=$outSNPUBMED."<td>";
		    		$outSNPUBMED=$outSNPUBMED.getTipoEstado($dadosDep->publicacoesPUBMED[$i]->estado);
		    		$outSNPUBMED=$outSNPUBMED."</td>";
	    		$outSNPUBMED=$outSNPUBMED."</tr>";
			}
			break;
			case 'LN': { 
	    		$outLNPUBMED=$outLNPUBMED."<tr>";

					$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$dadosDep->publicacoesPUBMED[$i]->idinv;
		    		$outLNPUBMED=$outLNPUBMED."</td>";

		    		$outLNPUBMED=$outLNPUBMED."<td>".$dadosDep->investigadores[$dadosDep->publicacoesPUBMED[$i]->idinv]->nome."</td>";

		    		$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$dadosDep->publicacoesPUBMED[$i]->ano;
		    		$outLNPUBMED=$outLNPUBMED."</td>";

		    		$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$dadosDep->publicacoesPUBMED[$i]->nomepublicacao;
		    		$outLNPUBMED=$outLNPUBMED."</td>";

		    		$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$dadosDep->publicacoesPUBMED[$i]->issn;
		    		$outLNPUBMED=$outLNPUBMED."</td>";

		    		$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$dadosDep->publicacoesPUBMED[$i]->isbn;
		    		$outLNPUBMED=$outLNPUBMED."</td>";

		    		$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$dadosDep->publicacoesPUBMED[$i]->titulo;
		    		$outLNPUBMED=$outLNPUBMED."</td>";

		    		$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$dadosDep->publicacoesPUBMED[$i]->autores;
		    		$outLNPUBMED=$outLNPUBMED."</td>";

					$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$dadosDep->publicacoesPUBMED[$i]->volume;
		    		$outLNPUBMED=$outLNPUBMED."</td>";

					$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$dadosDep->publicacoesPUBMED[$i]->issue;
		    		$outLNPUBMED=$outLNPUBMED."</td>";

					$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$dadosDep->publicacoesPUBMED[$i]->ar;
		    		$outLNPUBMED=$outLNPUBMED."</td>";

					$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$dadosDep->publicacoesPUBMED[$i]->colecao;
		    		$outLNPUBMED=$outLNPUBMED."</td>";

					$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$dadosDep->publicacoesPUBMED[$i]->primpagina;
		    		$outLNPUBMED=$outLNPUBMED."</td>";	

					$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$dadosDep->publicacoesPUBMED[$i]->ultpagina;
		    		$outLNPUBMED=$outLNPUBMED."</td>";

		    		$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$dadosDep->publicacoesPUBMED[$i]->citacoes;
		    		$outLNPUBMED=$outLNPUBMED."</td>";

		    		$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$dadosDep->publicacoesPUBMED[$i]->ifactor;
		    		$outLNPUBMED=$outLNPUBMED."</td>";

		    		$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$dadosDep->publicacoesPUBMED[$i]->conftitle;
		    		$outLNPUBMED=$outLNPUBMED."</td>";

		    		$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$dadosDep->publicacoesPUBMED[$i]->language;
		    		$outLNPUBMED=$outLNPUBMED."</td>";

		    		$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.$dadosDep->publicacoesPUBMED[$i]->editor;
		    		$outLNPUBMED=$outLNPUBMED."</td>";

		    		$outLNPUBMED=$outLNPUBMED."<td>";
					$outLNPUBMED=$outLNPUBMED.getTipoEstado($dadosDep->publicacoesPUBMED[$i]->estado);
		    		$outLNPUBMED=$outLNPUBMED."</td>";		    				    		
	    		$outLNPUBMED=$outLNPUBMED."</tr>";
			}
			break;
			case 'CLN': { 
		    		$outCLNPUBMED=$outCLNPUBMED."<tr>";

					$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$dadosDep->publicacoesPUBMED[$i]->idinv;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";

		    		$outCLNPUBMED=$outCLNPUBMED."<td>".$dadosDep->investigadores[$dadosDep->publicacoesPUBMED[$i]->idinv]->nome."</td>";

		    		$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$dadosDep->publicacoesPUBMED[$i]->ano;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";

		    		$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$dadosDep->publicacoesPUBMED[$i]->nomepublicacao;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";

		    		$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$dadosDep->publicacoesPUBMED[$i]->issn;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";

		    		$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$dadosDep->publicacoesPUBMED[$i]->isbn;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";

		    		$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$dadosDep->publicacoesPUBMED[$i]->titulo;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";

		    		$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$dadosDep->publicacoesPUBMED[$i]->autores;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";

					$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$dadosDep->publicacoesPUBMED[$i]->volume;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";

					$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$dadosDep->publicacoesPUBMED[$i]->issue;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";

					$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$dadosDep->publicacoesPUBMED[$i]->ar;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";

					$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$dadosDep->publicacoesPUBMED[$i]->colecao;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";

					$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$dadosDep->publicacoesPUBMED[$i]->primpagina;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";	

					$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$dadosDep->publicacoesPUBMED[$i]->ultpagina;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";

		    		$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$dadosDep->publicacoesPUBMED[$i]->citacoes;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";

		    		$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$dadosDep->publicacoesPUBMED[$i]->ifactor;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";

		    		$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$dadosDep->publicacoesPUBMED[$i]->conftitle;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";

		    		$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$dadosDep->publicacoesPUBMED[$i]->language;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";

		    		$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.$dadosDep->publicacoesPUBMED[$i]->editor;
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";

		    		$outCLNPUBMED=$outCLNPUBMED."<td>";
					$outCLNPUBMED=$outCLNPUBMED.getTipoEstado($dadosDep->publicacoesPUBMED[$i]->estado);
		    		$outCLNPUBMED=$outCLNPUBMED."</td>";		    				    		
	    		$outCLNPUBMED=$outCLNPUBMED."</tr>";
			}
			break;
			case 'L': { 
	    		$outLPUBMED=$outLPUBMED."<tr>";

					$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$dadosDep->publicacoesPUBMED[$i]->idinv;
		    		$outLPUBMED=$outLPUBMED."</td>";

		    		$outLPUBMED=$outLPUBMED."<td>".$dadosDep->investigadores[$dadosDep->publicacoesPUBMED[$i]->idinv]->nome."</td>";

		    		$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$dadosDep->publicacoesPUBMED[$i]->ano;
		    		$outLPUBMED=$outLPUBMED."</td>";

		    		$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$dadosDep->publicacoesPUBMED[$i]->nomepublicacao;
		    		$outLPUBMED=$outLPUBMED."</td>";

		    		$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$dadosDep->publicacoesPUBMED[$i]->issn;
		    		$outLPUBMED=$outLPUBMED."</td>";

		    		$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$dadosDep->publicacoesPUBMED[$i]->isbn;
		    		$outLPUBMED=$outLPUBMED."</td>";

		    		$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$dadosDep->publicacoesPUBMED[$i]->titulo;
		    		$outLPUBMED=$outLPUBMED."</td>";

		    		$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$dadosDep->publicacoesPUBMED[$i]->autores;
		    		$outLPUBMED=$outLPUBMED."</td>";

					$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$dadosDep->publicacoesPUBMED[$i]->volume;
		    		$outLPUBMED=$outLPUBMED."</td>";

					$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$dadosDep->publicacoesPUBMED[$i]->issue;
		    		$outLPUBMED=$outLPUBMED."</td>";

					$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$dadosDep->publicacoesPUBMED[$i]->ar;
		    		$outLPUBMED=$outLPUBMED."</td>";

					$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$dadosDep->publicacoesPUBMED[$i]->colecao;
		    		$outLPUBMED=$outLPUBMED."</td>";

					$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$dadosDep->publicacoesPUBMED[$i]->primpagina;
		    		$outLPUBMED=$outLPUBMED."</td>";

					$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$dadosDep->publicacoesPUBMED[$i]->ultpagina;
		    		$outLPUBMED=$outLPUBMED."</td>";

		    		$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$dadosDep->publicacoesPUBMED[$i]->citacoes;
		    		$outLPUBMED=$outLPUBMED."</td>";

		    		$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$dadosDep->publicacoesPUBMED[$i]->ifactor;
		    		$outLPUBMED=$outLPUBMED."</td>";

		    		$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$dadosDep->publicacoesPUBMED[$i]->conftitle;
		    		$outLPUBMED=$outLPUBMED."</td>";

		    		$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$dadosDep->publicacoesPUBMED[$i]->language;
		    		$outLPUBMED=$outLPUBMED."</td>";

		    		$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.$dadosDep->publicacoesPUBMED[$i]->editor;
		    		$outLPUBMED=$outLPUBMED."</td>";

		    		$outLPUBMED=$outLPUBMED."<td>";
					$outLPUBMED=$outLPUBMED.getTipoEstado($dadosDep->publicacoesPUBMED[$i]->estado);
		    		$outLPUBMED=$outLPUBMED."</td>";		    				    		
	    		$outLPUBMED=$outLPUBMED."</tr>";
			}
			break;
			case 'CL': { 
		    		$outCLPUBMED=$outCLPUBMED."<tr>";

					$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$dadosDep->publicacoesPUBMED[$i]->idinv;
		    		$outCLPUBMED=$outCLPUBMED."</td>";

		    		$outCLPUBMED=$outCLPUBMED."<td>".$dadosDep->investigadores[$dadosDep->publicacoesPUBMED[$i]->idinv]->nome."</td>";

		    		$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$dadosDep->publicacoesPUBMED[$i]->ano;
		    		$outCLPUBMED=$outCLPUBMED."</td>";

		    		$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$dadosDep->publicacoesPUBMED[$i]->nomepublicacao;
		    		$outCLPUBMED=$outCLPUBMED."</td>";

		    		$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$dadosDep->publicacoesPUBMED[$i]->issn;
		    		$outCLPUBMED=$outCLPUBMED."</td>";

		    		$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$dadosDep->publicacoesPUBMED[$i]->isbn;
		    		$outCLPUBMED=$outCLPUBMED."</td>";

		    		$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$dadosDep->publicacoesPUBMED[$i]->titulo;
		    		$outCLPUBMED=$outCLPUBMED."</td>";

		    		$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$dadosDep->publicacoesPUBMED[$i]->autores;
		    		$outCLPUBMED=$outCLPUBMED."</td>";

					$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$dadosDep->publicacoesPUBMED[$i]->volume;
		    		$outCLPUBMED=$outCLPUBMED."</td>";

					$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$dadosDep->publicacoesPUBMED[$i]->issue;
		    		$outCLPUBMED=$outCLPUBMED."</td>";

					$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$dadosDep->publicacoesPUBMED[$i]->ar;
		    		$outCLPUBMED=$outCLPUBMED."</td>";

					$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$dadosDep->publicacoesPUBMED[$i]->colecao;
		    		$outCLPUBMED=$outCLPUBMED."</td>";

					$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$dadosDep->publicacoesPUBMED[$i]->primpagina;
		    		$outCLPUBMED=$outCLPUBMED."</td>";	

					$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$dadosDep->publicacoesPUBMED[$i]->ultpagina;
		    		$outCLPUBMED=$outCLPUBMED."</td>";

		    		$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$dadosDep->publicacoesPUBMED[$i]->citacoes;
		    		$outCLPUBMED=$outCLPUBMED."</td>";

		    		$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$dadosDep->publicacoesPUBMED[$i]->ifactor;
		    		$outCLPUBMED=$outCLPUBMED."</td>";

		    		$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$dadosDep->publicacoesPUBMED[$i]->conftitle;
		    		$outCLPUBMED=$outCLPUBMED."</td>";

		    		$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$dadosDep->publicacoesPUBMED[$i]->language;
		    		$outCLPUBMED=$outCLPUBMED."</td>";

		    		$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.$dadosDep->publicacoesPUBMED[$i]->editor;
		    		$outCLPUBMED=$outCLPUBMED."</td>";

		    		$outCLPUBMED=$outCLPUBMED."<td>";
					$outCLPUBMED=$outCLPUBMED.getTipoEstado($dadosDep->publicacoesPUBMED[$i]->estado);
		    		$outCLPUBMED=$outCLPUBMED."</td>";		    				    		
	    		$outCLPUBMED=$outCLPUBMED."</tr>";
			}
			break;
			case 'P': { 
	    		$outPPUBMED=$outPPUBMED."<tr>";

		    		$outPPUBMED=$outPPUBMED."<td>";
		    		$outPPUBMED=$outPPUBMED.$dadosDep->publicacoesPUBMED[$i]->idinv;
		    		$outPPUBMED=$outPPUBMED."</td>";

		    		$outPPUBMED=$outPPUBMED."<td>".$dadosDep->investigadores[$dadosDep->publicacoesPUBMED[$i]->idinv]->nome."</td>";

		    		$outPPUBMED=$outPPUBMED."<td>";
					$outPPUBMED=$outPPUBMED.$dadosDep->publicacoesPUBMED[$i]->npatente;
		    		$outPPUBMED=$outPPUBMED."</td>";

		    		$outPPUBMED=$outPPUBMED."<td>";
					$outPPUBMED=$outPPUBMED.$dadosDep->publicacoesPUBMED[$i]->ipc;
		    		$outPPUBMED=$outPPUBMED."</td>";	

		    		$outPPUBMED=$outPPUBMED."<td>";
					$outPPUBMED=$outPPUBMED.$dadosDep->publicacoesPUBMED[$i]->titulo;
		    		$outPPUBMED=$outPPUBMED."</td>";	

		    		$outPPUBMED=$outPPUBMED."<td>";
					$outPPUBMED=$outPPUBMED.$dadosDep->publicacoesPUBMED[$i]->autores;
		    		$outPPUBMED=$outPPUBMED."</td>";

		    		$outPPUBMED=$outPPUBMED."<td>";
					$outPPUBMED=$outPPUBMED.$dadosDep->publicacoesPUBMED[$i]->datapatente;
		    		$outPPUBMED=$outPPUBMED."</td>";		    		
		    		
					$outPPUBMED=$outPPUBMED."<td>";
					$outPPUBMED=$outPPUBMED.getTipoEstado($dadosDep->publicacoesPUBMED[$i]->estado);
		    		$outPPUBMED=$outPPUBMED."</td>";		    				    		
	    		$outPPUBMED=$outPPUBMED."</tr>";
			}
			break;
			case 'A': { 
	    		$outAPUBMED=$outAPUBMED."<tr>";

					$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$dadosDep->publicacoesPUBMED[$i]->idinv;
		    		$outAPUBMED=$outAPUBMED."</td>";

		    		$outAPUBMED=$outAPUBMED."<td>".$dadosDep->investigadores[$dadosDep->publicacoesPUBMED[$i]->idinv]->nome."</td>";

		    		$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$dadosDep->publicacoesPUBMED[$i]->ano;
		    		$outAPUBMED=$outAPUBMED."</td>";

		    		$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$dadosDep->publicacoesPUBMED[$i]->nomepublicacao;
		    		$outAPUBMED=$outAPUBMED."</td>";

		    		$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$dadosDep->publicacoesPUBMED[$i]->issn;
		    		$outAPUBMED=$outAPUBMED."</td>";

		    		$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$dadosDep->publicacoesPUBMED[$i]->isbn;
		    		$outAPUBMED=$outAPUBMED."</td>";

		    		$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$dadosDep->publicacoesPUBMED[$i]->titulo;
		    		$outAPUBMED=$outAPUBMED."</td>";

		    		$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$dadosDep->publicacoesPUBMED[$i]->autores;
		    		$outAPUBMED=$outAPUBMED."</td>";

					$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$dadosDep->publicacoesPUBMED[$i]->volume;
		    		$outAPUBMED=$outAPUBMED."</td>";

					$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$dadosDep->publicacoesPUBMED[$i]->issue;
		    		$outAPUBMED=$outAPUBMED."</td>";

					$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$dadosDep->publicacoesPUBMED[$i]->ar;
		    		$outAPUBMED=$outAPUBMED."</td>";

					$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$dadosDep->publicacoesPUBMED[$i]->colecao;
		    		$outAPUBMED=$outAPUBMED."</td>";

					$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$dadosDep->publicacoesPUBMED[$i]->primpagina;
		    		$outAPUBMED=$outAPUBMED."</td>";

					$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$dadosDep->publicacoesPUBMED[$i]->ultpagina;
		    		$outAPUBMED=$outAPUBMED."</td>";

		    		$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$dadosDep->publicacoesPUBMED[$i]->citacoes;
		    		$outAPUBMED=$outAPUBMED."</td>";

		    		$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$dadosDep->publicacoesPUBMED[$i]->ifactor;
		    		$outAPUBMED=$outAPUBMED."</td>";

		    		$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$dadosDep->publicacoesPUBMED[$i]->conftitle;
		    		$outAPUBMED=$outAPUBMED."</td>";

		    		$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$dadosDep->publicacoesPUBMED[$i]->language;
		    		$outAPUBMED=$outAPUBMED."</td>";

		    		$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.$dadosDep->publicacoesPUBMED[$i]->editor;
		    		$outAPUBMED=$outAPUBMED."</td>";

		    		$outAPUBMED=$outAPUBMED."<td>";
					$outAPUBMED=$outAPUBMED.getTipoEstado($dadosDep->publicacoesPUBMED[$i]->estado);
		    		$outAPUBMED=$outAPUBMED."</td>";		    				    		
	    		$outAPUBMED=$outAPUBMED."</tr>";
			}
			break;
			default:{ 
		        $outRestoPUBMED=$outRestoPUBMED."<tr>";

					$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$dadosDep->publicacoesPUBMED[$i]->idinv;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";

		    		$outRestoPUBMED=$outRestoPUBMED."<td>".$dadosDep->investigadores[$dadosDep->publicacoesPUBMED[$i]->idinv]->nome."</td>";

		    		$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$dadosDep->publicacoesPUBMED[$i]->ano;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";

		    		$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$dadosDep->publicacoesPUBMED[$i]->tipo;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";

		    		$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$dadosDep->publicacoesPUBMED[$i]->nomepublicacao;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";

		    		$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$dadosDep->publicacoesPUBMED[$i]->issn;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";

		    		$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$dadosDep->publicacoesPUBMED[$i]->titulo;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";

		    		$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$dadosDep->publicacoesPUBMED[$i]->autores;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";

					$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$dadosDep->publicacoesPUBMED[$i]->volume;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";

					$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$dadosDep->publicacoesPUBMED[$i]->issue;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";

					$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$dadosDep->publicacoesPUBMED[$i]->ar;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";

					$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$dadosDep->publicacoesPUBMED[$i]->primpagina;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";

					$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$dadosDep->publicacoesPUBMED[$i]->ultpagina;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";

		    		$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$dadosDep->publicacoesPUBMED[$i]->citacoes;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";

		    		$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$dadosDep->publicacoesPUBMED[$i]->ifactor;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";

		    		$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$dadosDep->publicacoesPUBMED[$i]->conftitle;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";

		    		$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$dadosDep->publicacoesPUBMED[$i]->language;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";

		    		$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$dadosDep->publicacoesPUBMED[$i]->editor;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";

		    		$outRestoPUBMED=$outRestoPUBMED."<td>";
					$outRestoPUBMED=$outRestoPUBMED.$dadosDep->publicacoesPUBMED[$i]->tipofmup;
		    		$outRestoPUBMED=$outRestoPUBMED."</td>";
	    		$outRestoPUBMED=$outRestoPUBMED."</tr>";
		    }
			break;
		}
	}

	
	
	if($outJPUBMED!=""){echo"<table  class='box-table-b'><caption>ARTIGOS</caption>";echo $tcolsJPUBMED;echo $outJPUBMED;echo "</table><br />";}
	if($outPRPPUBMED!=""){echo"<table  class='box-table-b'><caption>PEER REVIEW PROCEEDINGS</caption>";echo $tcolsJPUBMED;echo $outPRPPUBMED;echo "</table><br />";}
	if($outJAPUBMED!=""){echo"<table  class='box-table-b'><caption>JOURNAL ABSTRACTS</caption>";echo $tcolsJPUBMED;echo $outJAPUBMED;echo "</table><br />";}

	if($outCPPUBMED!=""){echo"<table  class='box-table-b'><caption>CONFERENCE PROCEEDINGS</caption>";echo $tcolsCONFPUBMED;echo $outCPPUBMED;echo "</table><br />";}
	if($outOAPUBMED!=""){echo"<table  class='box-table-b'><caption>OTHER ABSTRACTS</caption>";echo $tcolsCONFPUBMED;echo $outOAPUBMED;echo "</table><br />";}
	if($outLPUBMED!=""){echo"<table  class='box-table-b'><caption>LIVROS</caption>";echo $tcols;echo $outLPUBMED;echo "</table><br />";}
	if($outCLPUBMED!=""){echo"<table  class='box-table-b'><caption>CAPITULOS DE LIVROS</caption>";echo $tcols;echo $outCLPUBMED;echo "</table><br />";}
	if($outSPUBMED!=""){echo"<table  class='box-table-b'><caption>SUMÁRIOS</caption>";echo $tcols;echo $outSPUBMED;echo "</table><br />";}

	
	// NACIONAIS
	
	if($outJNPUBMED!=""){echo"<table  class='box-table-b'><caption>ARTIGOS - NÃO INGLÊS</caption>";echo $tcolsJPUBMED;echo $outJNPUBMED;echo "</table><br />";}
	if($outPRPNPUBMED!=""){echo"<table  class='box-table-b'><caption>PEER REVIEW PROCEEDINGS - NÃO INGLÊS</caption>";echo $tcolsJPUBMED;echo $outPRPNPUBMED;echo "</table><br />";}
	// FALTA JAN
	
	if($outCPNPUBMED!=""){echo"<table  class='box-table-b'><caption>CONFERENCE PROCEEDINGS NÃO INGLÊS</caption>";echo $tcolsCONFPUBMED;echo $outCPNPUBMED;echo "</table><br />";}
	if($outLNPUBMED!=""){echo"<table  class='box-table-b'><caption>LIVROS - NÃO INGLÊS</caption>";echo $tcols;echo $outLPUBMED;echo "</table><br />";}
	if($outCLNPUBMED!=""){echo"<table  class='box-table-b'><caption>CAPITULOS DE LIVROS - NÃO INGLÊS</caption>";echo $tcols;echo $outCLNPUBMED;echo "</table><br />";}
	if($outSNPUBMED!=""){echo"<table  class='box-table-b'><caption>SUMÁRIO - NÃO INGLÊS</caption>";echo $tcols;echo $outSNPUBMED;echo "</table><br />";}
	if($outPPUBMED!=""){echo"<table  class='box-table-b'><caption>PATENTES</caption>";echo $tcols;echo $outPPUBMED;echo "</table><br />";}
	if($outAPUBMED!=""){echo"<table  class='box-table-b'><caption>AGRADECIMENTOS</caption>";echo $tcols;echo $outAPUBMED;echo "</table><br />";}	
	
	echo "</div>";
	
	function getTipoEstado($i) {
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_estadoPublicacoes");
	
		while ($row = mysql_fetch_assoc($lValues)) {
			if($i==$row["ID"])
				$resp = $row["DESCRICAO"];
		}
	
		$db->disconnect();
		return $resp;
	}
	
	function checkTipoEstado($id,$i){
		global $dadosDep;
		if($dadosDep->publicacoesPUBMED[$i]->estado==$id)
			return true;
		else
			return false;
	}
	
	
?>