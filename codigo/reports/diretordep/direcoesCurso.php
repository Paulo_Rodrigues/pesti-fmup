<?php

/**
* Autor: Paulo Rodrigues (prodrigues@med.up.pt/paulomiguelarodrigues@gmail.com)
* Data: 15/04/2014
* Alterações: Adicionada a capacidade de poder editar e remover registos de direções de curso. 
*/

echo "<h3>Direções de Curso</h3>\n";
echo "<div id='direcaoCurso'>\n";

echo "<table id='dircurso' class='box-table-b'>
		<tr>
			<th>ID Inv</th>
			<th>Data de Inicio</th>
			<th>Data de Fim</th>
			<th>Curso</th>
			<th>Grau</th>
		</tr>";
			    
    foreach ($dadosDep->dirCursos as $i => $value){
			echo "<tr>";
				echo "<td>".$dadosDep->dirCursos[$i]->idinv."</td>";
				echo "<td id='td_dircurso_dataini_" . $dadosDep->dirCursos[$i]->id . "'>".$dadosDep->dirCursos[$i]->datainicio."</td>";
				echo "<td id='td_dircurso_datafim_" . $dadosDep->dirCursos[$i]->id . "'>".$dadosDep->dirCursos[$i]->datafim."</td>";
				echo "<td id='td_dircurso_curso_" . $dadosDep->dirCursos[$i]->id . "'>".$dadosDep->dirCursos[$i]->nome."</td>";
				echo "<td id='td_dircurso_grau_" . $dadosDep->dirCursos[$i]->id . "'>";
				getGrauDirCursos($dadosDep->dirCursos[$i]->grau);	
		    	echo "</td>";
		    	echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-dircurso').text('" . $dadosDep->dirCursos[$i]->id . "');\"></td>";
	        	echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep->dirCursos[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-dircurso').text('" . $dadosDep->dirCursos[$i]->id . "');apagarDirCurso();return false;\" ></center></td>";   
	    	echo "</tr>";	    	
    } 

    echo "</table>";
    echo "<p id='chave-dircurso' hidden></p>";
    echo "</div>";	
	
	function getGrauDirCursos($i) {
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_graucursos");

		while ($row = mysql_fetch_assoc($lValues)) {	
			if($i==$row["ID"])
				echo $row["DESCRICAO"];
		}

		$db->disconnect();
				
	}	
				
	function checkGrauDirCursos($id,$i){
		global $dadosDep;
		if($dadosDep->dirCursos[$i]->grau==$id)
			return true;
		else 
			return false;
	}
		
	?>