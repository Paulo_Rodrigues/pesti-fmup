<?php

/**
* Autor: Paulo Rodrigues (prodrigues@med.up.pt/paulomiguelarodrigues@gmail.com)
* Data: 15/04/2014
* Alterações: Adicionada a capacidade de poder editar e remover registos de agregações. 
*/

echo "<h3>Agregação</h3> 
    <div id='agregacoes'>";
	
echo "<table id='agreg' class='box-table-b'>
		<tr>
			<th>ID Investigador</th>
			<th>Nome</th>
			<th>Unanimidade</th>
		</tr>";
	
	foreach ($dadosDep->agregacoes as $i => $value){	
		echo "<tr>
			<td>".$dadosDep->agregacoes[$i]->idinv."</td>
			<td>".$dadosDep->investigadores[$dadosDep->agregacoes[$i]->idinv]->nome."</td>";
			if($dadosDep->agregacoes[$i]->unanimidade == 1)
				echo "<td id='td_unanimidade_agr_" .$dadosDep->agregacoes[$i]->id. "'>Sim</td>";
			else
				echo "<td id='td_unanimidade_agr_" .$dadosDep->agregacoes[$i]->id. "'>Não</td>";
			echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-agr').text('" . $dadosDep->agregacoes[$i]->id . "');\"></td>
        	<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep->agregacoes[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-agr').text('" . $dadosDep->agregacoes[$i]->id . "');apagarAgregacao();return false;\" ></center></td>     
		</tr>";
	}
	
	echo "</table>
	<p id='chave-agr' hidden></p>
	</div>";
		
	?>