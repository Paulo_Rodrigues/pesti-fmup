<?php

/**
* Autor: Paulo Rodrigues (prodrigues@med.up.pt/paulomiguelarodrigues@gmail.com)
* Data: 24/04/2014
* Alterações: Adicionada a capacidade de poder editar e remover registos de sociedades cientificas. 
*/

echo "<h3>Redes Científicas</h3>\n";
echo "<div id='redes'>";
echo "<table class='box-table-b'>
		<thead>
			<tr>
				<th>ID Inv</th>
				<th>Nome Investigador</th>
				<th>Nome Rede</th>
				<th>Tipo</th>
				<th>Data Início</th>
				<th>Data Fim</th>
				<th>Link</th>
			</tr>
		</thead>
		<tbody>";
			    
    foreach ($dadosDep->redes as $i => $value){
		echo "<tr>";
			echo "<td>".$dadosDep->redes[$i]->idinv."</td>";
			echo "<td>".$dadosDep->investigadores[$dadosDep->redes[$i]->idinv]->nome."</td>";
		   	echo "<td id='td_redes_nome_" .$dadosDep->redes[$i]->id. "'>".$dadosDep->redes[$i]->nome."</td>";
		   	echo "<td id='td_redes_tipo_" .$dadosDep->redes[$i]->id. "'>";
			getTipoRedes($dadosDep->redes[$i]->tipo);
			echo "</td>";
			echo "<td id='td_redes_datainicio_" .$dadosDep->redes[$i]->id. "'>".$dadosDep->redes[$i]->datainicio."</td>";	
			echo "<td id='td_redes_datafim_" .$dadosDep->redes[$i]->id. "'>".$dadosDep->redes[$i]->datafim."</td>";	
			echo "<td id='td_redes_link_" .$dadosDep->redes[$i]->id. "'>".$dadosDep->redes[$i]->link."</td>";
			echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-redes').text('" . $dadosDep->redes[$i]->id . "');\"></td>";
        	echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep->redes[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-redes').text('" . $dadosDep->redes[$i]->id . "');apagarRede();return false;\" ></center></td>";
    	echo "</tr>";	    	
    } 

echo "</tbody>
    </table>
    <p id='chave-redes' hidden></p>
</div>";

	
function getTipoRedes($i) {	
	$db = new Database();
	$lValues =$db->getLookupValues("lista_tiporedes");
	
	while ($row = mysql_fetch_assoc($lValues)) {
		if($i==$row["ID"])
			echo $row["DESCRICAO"];
	}
	$db->disconnect();				
	}	

?>