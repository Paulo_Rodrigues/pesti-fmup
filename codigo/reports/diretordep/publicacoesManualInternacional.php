<?php

/**
* Alteração: Permitido agora editar e apagar registos de publicações manuais - internacional.
* Autor: Paulo Rodrigues (prodrigues@med.up.pt, paulomiguelarodrigues@gmail.com)
* Data: 21/04/2014
* */

$outJMAN="";
$outPRPMAN="";
$outJAMAN="";
$outLMAN="";
$outCLMAN="";
$outCPMAN="";
$outOAMAN="";

$tcolsJ="<thead>
		    <tr>
				<th>IDINV</th>
				<th>Nome</th>
				<th>ISSN</th>
				<th>REVISTA</th>
				<th>TÍTULO</th>
				<th>AUTORES</th>
				<th>VOLUME</th>
				<th>ISSUE</th>
				<th>PRIMEIRA PÁG.</th>
				<th>ÚLTIMA PÁG.</th>
				<th>LINK</th>
				<th>ESTADO</th>
			</tr>
		</thead>";

$tcolsCONF="<thead>
			    <tr>
					<th>IDINV</th>
					<th>Nome</th>
					<th>ISBN</th>
					<th>TÍTULO DA OBRA</th>
					<th>EDITORES</th>
					<th>TÍTULO ARTIGO</th>
					<th>AUTORES</th>
					<th>PRIMEIRA PÁG.</th>
					<th>ÚLTIMA PÁG.</th>
					<th>LINK</th>
					<th>ESTADO</th>
				</tr>
			</thead>";

$tcolsPRP="<thead>
			    <tr>
					<th>IDINV</th>
					<th>Nome</th>
					<th>ISSN</th>
					<th>TÍTULO DA OBRA</th>
					<th>EDITORES</th>
					<th>TÍTULO ARTIGO</th>
					<th>AUTORES</th>
					<th>PRIMEIRA PÁG.</th>
					<th>ÚLTIMA PÁG.</th>
					<th>LINK</th>
					<th>ESTADO</th>
				</tr>
			</thead>";

$tcolsCLIV="<thead>
			    <tr>
					<th>IDINV</th>
					<th>Nome</th>
					<th>ISBN</th>
					<th>TÍTULO DA OBRA</th>
					<th>EDITORA</th>
					<th>EDITORES</th>			
					<th>TÍTULO ARTIGO</th>
					<th>AUTORES</th>
					<th>PRIMEIRA PÁG.</th>
					<th>ÚLTIMA PÁG.</th>
					<th>LINK</th>
					<th>ESTADO</th>
				</tr>
			</thead>";

$tcolsPAT="<thead>
			    <tr>
					<th>IDINV</th>
					<th>Nome</th>
					<th>Nº PATENTE</th>
					<th>IPC</th>
					<th>TÍTULO</th>
					<th>AUTORES</th>		
					<th>DATA de PUBLICAÇÃO</th>
					<th>LINK</th>
					<th>ESTADO</th>
				</tr>
			</thead>";

$tcolsLIV="<thead>
			    <tr>
					<th>IDINV</th>
					<th>Nome</th>
					<th>ISBN</th>
					<th>TÍTULO</th>
					<th>AUTORES</th>		
					<th>EDITOR</th>
					<th>EDITORA</th>
					<th>LINK</th>
					<th>ESTADO</th>
				</tr>
			</thead>";


	foreach ($dadosDep->publicacoesMANUALInternacional as $i => $value){
			
		$tipo=$dadosDep->publicacoesMANUALInternacional[$i]->tipofmup;

		switch($tipo){
			case 'J': { 
				$outJMAN=$outJMAN."<tr>";
					$outJMAN=$outJMAN."<td>".$dadosDep->publicacoesMANUALInternacional[$i]->idinv."</td>";
					$outJMAN=$outJMAN."<td>".$dadosDep->investigadores[$dadosDep->publicacoesMANUALInternacional[$i]->idinv]->nome."</td>";
					$outJMAN=$outJMAN."<td id='td_outJMAN_issn_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->issn."</td>";			
				   	$outJMAN=$outJMAN."<td id='td_outJMAN_nomepub_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->nomepublicacao."</td>";
					$outJMAN=$outJMAN."<td id='td_outJMAN_titulo_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->titulo."</td>";
					$outJMAN=$outJMAN."<td id='td_outJMAN_autores_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->autores."</td>";
					$outJMAN=$outJMAN."<td id='td_outJMAN_volume_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->volume."</td>";
					$outJMAN=$outJMAN."<td id='td_outJMAN_issue_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->issue."</td>";
					$outJMAN=$outJMAN."<td id='td_outJMAN_prim_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->primpagina."</td>";
					$outJMAN=$outJMAN."<td id='td_outJMAN_ult_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->ultpagina."</td>";
					$outJMAN=$outJMAN."<td id='td_outJMAN_link_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'><a>".$dadosDep->publicacoesMANUALInternacional[$i]->link."</a></td>";
					$outJMAN=$outJMAN."<td id='td_outJMAN_estado_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".getEstadoPublicacaoMI($dadosDep->publicacoesMANUALInternacional[$i]->estado)."</td>";						
					$outJMAN=$outJMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubIntMan-Art').text('" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "');\"></td>";
        			$outJMAN=$outJMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-pubIntMan-Art').text('" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "');apagarPubManIntArt();return false;\" ></center></td>";    							
				$outJMAN=$outJMAN."</tr>";
			}
			break;
			case 'PRP': { 
				$outPRPMAN=$outPRPMAN."<tr>";
					$outPRPMAN=$outPRPMAN."<td>".$dadosDep->publicacoesMANUALInternacional[$i]->idinv."</td>";
					$outPRPMAN=$outPRPMAN."<td>".$dadosDep->investigadores[$dadosDep->publicacoesMANUALInternacional[$i]->idinv]->nome."</td>";
				   	$outPRPMAN=$outPRPMAN."<td id='td_outPRPMAN_issn_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->issn."</td>";
					$outPRPMAN=$outPRPMAN."<td id='td_outPRPMAN_numepub_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->nomepublicacao."</td>";
					$outPRPMAN=$outPRPMAN."<td id='td_outPRPMAN_titulo_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->titulo."</td>";
					$outPRPMAN=$outPRPMAN."<td id='td_outPRPMAN_autores_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->autores."</td>";
					$outPRPMAN=$outPRPMAN."<td id='td_outPRPMAN_volume_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->volume."</td>";
					$outPRPMAN=$outPRPMAN."<td id='td_outPRPMAN_issue_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->issue."</td>";
					$outPRPMAN=$outPRPMAN."<td id='td_outPRPMAN_prim_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->primpagina."</td>";
					$outPRPMAN=$outPRPMAN."<td id='td_outPRPMAN_ult_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->ultpagina."</td>";
					$outPRPMAN=$outPRPMAN."<td id='td_outPRPMAN_link_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->link."</td>";
					$outPRPMAN=$outPRPMAN."<td id='td_outPRPMAN_estado_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".getEstadoPublicacaoMI($dadosDep->publicacoesMANUALInternacional[$i]->estado)."</td>";
					$outPRPMAN=$outPRPMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubIntMan-PRP').text('" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "');\"></td>";
        			$outPRPMAN=$outPRPMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-pubIntMan-PRP').text('" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "');apagarPubManInt();return false;\" ></center></td>";    							
				$outPRPMAN=$outPRPMAN."</tr>";
			}
			break;
			case 'JA': { 
				$outJAMAN=$outJAMAN."<tr>";
					$outJAMAN=$outJAMAN."<td>".$dadosDep->publicacoesMANUALInternacional[$i]->idinv."</td>";
					$outJAMAN=$outJAMAN."<td>".$dadosDep->investigadores[$dadosDep->publicacoesMANUALInternacional[$i]->idinv]->nome."</td>";
				   	$outJAMAN=$outJAMAN."<td id='td_outJAMAN_issn_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->issn."</td>";	
					$outJAMAN=$outJAMAN."<td id='td_outJAMAN_nomepub_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->nomepublicacao."</td>";
					$outJAMAN=$outJAMAN."<td id='td_outJAMAN_titulo_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->titulo."</td>";						
					$outJAMAN=$outJAMAN."<td id='td_outJAMAN_autores_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->autores."</td>";
					$outJAMAN=$outJAMAN."<td id='td_outJAMAN_volume_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->volume."</td>";
					$outJAMAN=$outJAMAN."<td id='td_outJAMAN_issue_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->issue."</td>";	
					$outJAMAN=$outJAMAN."<td id='td_outJAMAN_prim_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->primpagina."</td>";
					$outJAMAN=$outJAMAN."<td id='td_outJAMAN_ult_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->ultpagina."</td>";
					$outJAMAN=$outJAMAN."<td id='td_outJAMAN_link_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->link."</td>";
					$outJAMAN=$outJAMAN."<td id='td_outJAMAN_estado_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".getEstadoPublicacaoMI($dadosDep->publicacoesMANUALInternacional[$i]->estado)."</td>";
					$outJAMAN=$outJAMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubIntMan-Sum').text('" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "');\"></td>";
        			$outJAMAN=$outJAMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-pubIntMan-Sum').text('" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "');apagarPubManInt();return false;\" ></center></td>";  					
			   $outJAMAN=$outJAMAN."</tr>";
			}
			break;
			case 'CP': { 
				$outCPMAN=$outCPMAN."<tr>";
					$outCPMAN=$outCPMAN."<td>".$dadosDep->publicacoesMANUALInternacional[$i]->idinv."</td>";
					$outCPMAN=$outCPMAN."<td>".$dadosDep->investigadores[$dadosDep->publicacoesMANUALInternacional[$i]->idinv]->nome."</td>";
				   	$outCPMAN=$outCPMAN."<td id='td_outCPMAN_isbn_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->isbn."</td>";
					$outCPMAN=$outCPMAN."<td id='td_outCPMAN_nomepub_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->nomepublicacao."</td>";
					$outCPMAN=$outCPMAN."<td id='td_outCPMAN_editor_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->editor."</td>";
					$outCPMAN=$outCPMAN."<td id='td_outCPMAN_titulo_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->titulo."</td>";
					$outCPMAN=$outCPMAN."<td id='td_outCPMAN_autores_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->autores."</td>";
					$outCPMAN=$outCPMAN."<td id='td_outCPMAN_prim_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->primpagina."</td>";
					$outCPMAN=$outCPMAN."<td id='td_outCPMAN_ult_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->ultpagina."</td>";
					$outCPMAN=$outCPMAN."<td id='td_outCPMAN_link_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->link."</textarea></td>";
					$outCPMAN=$outCPMAN."<td id='td_outCPMAN_estado_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".getEstadoPublicacaoMI($dadosDep->publicacoesMANUALInternacional[$i]->estado)."</td>";
					$outCPMAN=$outCPMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubIntMan-Con').text('" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "');\"></td>";
        			$outCPMAN=$outCPMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-pubIntMan-Con').text('" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "');apagarPubManIntCon();return false;\" ></center></td>";
			   	$outCPMAN=$outCPMAN."</tr>";
			}
			break;
			case 'OA': { 
				$outOAMAN=$outOAMAN."<tr>";
					$outOAMAN=$outOAMAN."<td >".$dadosDep->publicacoesMANUALInternacional[$i]->idinv."</td>";
					$outOAMAN=$outOAMAN."<td>".$dadosDep->investigadores[$dadosDep->publicacoesMANUALInternacional[$i]->idinv]->nome."</td>";
				   	$outOAMAN=$outOAMAN."<td id='td_outOAMAN_isbn_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->isbn."</td>";
					$outOAMAN=$outOAMAN."<td id='td_outOAMAN_nomepub_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->nomepublicacao."</td>";
					$outOAMAN=$outOAMAN."<td id='td_outOAMAN_editor_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->editor."</td>";
					$outOAMAN=$outOAMAN."<td id='td_outOAMAN_titulo_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->titulo."</td>";
					$outOAMAN=$outOAMAN."<td id='td_outOAMAN_autores_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->autores."</td>";
					$outOAMAN=$outOAMAN."<td id='td_outOAMAN_prim_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->primpagina."</td>";
					$outOAMAN=$outOAMAN."<td id='td_outOAMAN_ult_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->ultpagina."</td>";
					$outOAMAN=$outOAMAN."<td id='td_outOAMAN_link_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->link."</td>";
					$outOAMAN=$outOAMAN."<td id='td_outOAMAN_estado_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".getEstadoPublicacaoMI($dadosDep->publicacoesMANUALInternacional[$i]->estado)."</td>";
					$outOAMAN=$outOAMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubIntMan-Oth').text('" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "');\"></td>";
        			$outOAMAN=$outOAMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-pubIntMan-Oth').text('" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "');apagarPubManIntOth();return false;\" ></center></td>";
			   	$outOAMAN=$outOAMAN."</tr>";
			}			
			break;
			case 'L': { 
				$outLMAN=$outLMAN."<tr>";
					$outLMAN=$outLMAN."<td>".$dadosDep->publicacoesMANUALInternacional[$i]->idinv."</td>";
					$outLMAN=$outLMAN."<td>".$dadosDep->investigadores[$dadosDep->publicacoesMANUALInternacional[$i]->idinv]->nome."</td>";
					$outLMAN=$outLMAN."<td id='td_outLMAN_isbn_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->isbn."</td>";
					$outLMAN=$outLMAN."<td id='td_outLMAN_titulo_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->titulo."</td>";
					$outLMAN=$outLMAN."<td id='td_outLMAN_autores_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->autores."</td>";
					$outLMAN=$outLMAN."<td id='td_outLMAN_editor_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->editor."</td>";
					$outLMAN=$outLMAN."<td id='td_outLMAN_editora_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->editora."</td>";
					$outLMAN=$outLMAN."<td id='td_outLMAN_link_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->link."</td>";
					$outLMAN=$outLMAN."<td id='td_outLMAN_estado_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".getEstadoPublicacaoMI($dadosDep->publicacoesMANUALInternacional[$i]->estado)."</td>";
					$outLMAN=$outLMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubIntMan-Liv').text('" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "');\"></td>";
        			$outLMAN=$outLMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-pubIntMan-Liv').text('" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "');apagarPubManInt();return false;\" ></center></td>"; 					
				$outLMAN=$outLMAN."</tr>";
			}
			break;
			case 'CL': { 
				$outCLMAN=$outCLMAN."<tr>";
					$outCLMAN=$outCLMAN."<td>".$dadosDep->publicacoesMANUALInternacional[$i]->idinv."</td>";
					$outCLMAN=$outCLMAN."<td>".$dadosDep->investigadores[$dadosDep->publicacoesMANUALInternacional[$i]->idinv]->nome."</td>";
				   	$outCLMAN=$outCLMAN."<td id='td_outCLMAN_isbn_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->isbn."</td>";
				   	$outCLMAN=$outCLMAN."<td id='td_outCLMAN_nomepub_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->nomepublicacao."</td>";
					$outCLMAN=$outCLMAN."<td id='td_outCLMAN_editora_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->editora."</td>";
				   	$outCLMAN=$outCLMAN."<td id='td_outCLMAN_editor_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->editor."</td>";						
					$outCLMAN=$outCLMAN."<td id='td_outCLMAN_titulo_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->titulo."</td>";
					$outCLMAN=$outCLMAN."<td id='td_outCLMAN_autores_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->autores."</td>";
					$outCLMAN=$outCLMAN."<td id='td_outCLMAN_prim_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->primpagina."</td>";
					$outCLMAN=$outCLMAN."<td id='td_outCLMAN_ult_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->ultpagina."</td>";
					$outCLMAN=$outCLMAN."<td id='td_outCLMAN_link_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".$dadosDep->publicacoesMANUALInternacional[$i]->link."</td>";
					$outCLMAN=$outCLMAN."<td id='td_outCLMAN_estado_" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "'>".getEstadoPublicacaoMI($dadosDep->publicacoesMANUALInternacional[$i]->estado)."</td>";						   	
					$outCLMAN=$outCLMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubIntMan-CLiv').text('" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "');\"></td>";
        			$outCLMAN=$outCLMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-pubIntMan-CLiv').text('" . $dadosDep->publicacoesMANUALInternacional[$i]->id . "');apagarPubManIntCLiv();return false;\" ></center></td>";
				$outCLMAN=$outCLMAN."</tr>";
			}
			break;				
		}
	}

	echo "<h3>Publicações Manuais Internacionais</h3>\n";
	echo "<div id='pubManualInternacional'>";
		echo"<div id='pubManualInt-Art'><table id='pubManInt-Art' class='box-table-b'><caption>ARTIGOS</caption>";echo $tcolsJ;echo "<tbody>";echo $outJMAN;echo "</table><p id='chave-pubIntMan-Art' hidden></p></div><br>";
		echo"<div id='pubManualInt-Rev'><table id='pubManInt-Rev' class='box-table-b'><caption >PEER REVIEW PROCEEDINGS</caption>";echo $tcolsJ;echo "<tbody>";echo $outPRPMAN;echo "</tbody></table><p id='chave-pubIntMan-PRP' hidden></p></div><br>";
		echo"<div id='pubManualInt-Sum'><table id='pubManInt-Sum' class='box-table-b'><caption >SUMÁRIOS<p><i>JOURNAL ABSTRACTS</i></p></caption>";echo $tcolsJ;echo "<tbody>";echo $outJAMAN;echo "</tbody></table><p id='chave-pubIntMan-Sum' hidden></p></div><br>";		
		echo"<div id='pubManualInt-Liv'><table id='pubManInt-Liv' class='box-table-b'><caption >LIVROS<p><i>BOOKS</i></p></caption>";echo $tcolsLIV;echo "<tbody>";echo $outLMAN;echo "</tbody></table><p id='chave-pubIntMan-Liv' hidden></p></div><br>";
		echo"<div id='pubManualInt-CLiv'><table id='pubManInt-CLiv' class='box-table-b'><caption >CAPITULOS DE LIVROS<p><i>BOOK CHAPTERS</i></p></caption>";echo $tcolsCLIV;echo "<tbody>";echo $outCLMAN;echo "</tbody></table><p id='chave-pubIntMan-CLiv' hidden></p></div><br>";
		echo"<div id='pubManualInt-Con'><table id='pubManInt-Con' class='box-table-b'><caption >CONFERENCE PROCEEDINGS</caption>";echo $tcolsCONF;echo "<tbody>";echo $outCPMAN;echo "</tbody></table><p id='chave-pubIntMan-Con' hidden></p></div><br>";
		echo"<div id='pubManualInt-Oth'><table id='pubManInt-Oth' class='box-table-b'><caption >OUTROS SUMÁRIOS<br>OTHER ABSTRACTS</caption>";echo $tcolsCONF;echo "<tbody>";echo $outOAMAN;echo "</tbody></table><p id='chave-pubIntMan-Oth' hidden></p></div><br>";
	echo "</div>";

	function getEstadoPublicacaoMI($i) {
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_estadoPublicacoes");
		$texto="";
		while ($row = mysql_fetch_assoc($lValues)) {
			if($i==$row["ID"])
				$texto=$row["DESCRICAO"];
		}

		$db->disconnect();
		return $texto;
	
	}
?>