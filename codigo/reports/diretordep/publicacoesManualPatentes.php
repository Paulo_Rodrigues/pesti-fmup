<?php

    /**
   * Alteração: Permitido agora editar e apagar registos de publicações manuais - patentes.
   * Autor: Paulo Rodrigues (prodrigues@med.up.pt, paulomiguelarodrigues@gmail.com)
   * Data: 21/04/2014
   * */

$outPMAN="";

$tcolsPAT="<thead>
		    <tr>
				<th>Nº PATENTE<br><i>PATENT NR.</i></th>
				<th>IPCs</th>
				<th>TÍTULO<br><i>TITLE</i></th>
				<th>AUTORES<br><i>AUTHORS</i></th>		
				<th>DATA de PUBLICAÇÃO<br><i>PUBLICATION DATE</i></th>
				<th>Link<br><i>Link</i></th>
				<th>Estado<br><i>Status</i></th>
			</tr>
		</thead>";

	foreach ($dadosDep->publicacoesMANUALPatentes as $i => $value){
		$outPMAN=$outPMAN."<tr>";
		   	$outPMAN=$outPMAN."<td id='td_pubManualPatentes_npatente_" . $dadosDep->publicacoesMANUALPatentes[$i]->id . "'>".$dadosDep->publicacoesMANUALPatentes[$i]->npatente."</td>";
			$outPMAN=$outPMAN."<td id='td_pubManualPatentes_ipc_" . $dadosDep->publicacoesMANUALPatentes[$i]->id . "'>".$dadosDep->publicacoesMANUALPatentes[$i]->ipc."</td>";
			$outPMAN=$outPMAN."<td id='td_pubManualPatentes_titulo_" . $dadosDep->publicacoesMANUALPatentes[$i]->id . "'>".$dadosDep->publicacoesMANUALPatentes[$i]->titulo."</td>";
			$outPMAN=$outPMAN."<td id='td_pubManualPatentes_autores_" . $dadosDep->publicacoesMANUALPatentes[$i]->id . "'>".$dadosDep->publicacoesMANUALPatentes[$i]->autores."</td>";										
			$outPMAN=$outPMAN."<td id='td_pubManualPatentes_datapatente_" . $dadosDep->publicacoesMANUALPatentes[$i]->id . "'>".$dadosDep->publicacoesMANUALPatentes[$i]->datapatente."</td>";
			$outPMAN=$outPMAN."<td id='td_pubManualPatentes_link_" . $dadosDep->publicacoesMANUALPatentes[$i]->id . "'>".$dadosDep->publicacoesMANUALPatentes[$i]->link."</td>";
			$outPMAN=$outPMAN."<td id='td_pubManualPatentes_estado_" . $dadosDep->publicacoesMANUALPatentes[$i]->id . "'>".getEstadoPublicacaoPatente($dadosDep->publicacoesMANUALPatentes[$i]->estado)."</td>";	
			$outPMAN=$outPMAN."<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-pubManPatentes').text('" . $dadosDep->publicacoesMANUALPatentes[$i]->id . "');\"></td>";
        	$outPMAN=$outPMAN."<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep->publicacoesMANUALPatentes[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-pubManPatentes').text('" . $dadosDep->publicacoesMANUALPatentes[$i]->id . "');apagarPubManPat();return false;\" ></center></td>";    		
		$outPMAN=$outPMAN."</tr>";	
	}
	
	echo "<h3>Patentes Manuais</h3>\n";
	echo "<div id='pubManualPatentes'>";		
		echo"<table id='pubManPat' class='box-table-b'><caption>PATENTES</caption>";
			echo $tcolsPAT;
			echo "<tbody>";
			echo $outPMAN;
			echo "</tbody>";
		echo "</table>";
		echo "<p id='chave-pubManPatentes' hidden></p>";
    echo "</div>";
	
	function getEstadoPublicacaoPatente($i) {
		global $dadoDep;
		$db = new Database();
		$lValues =$db->getLookupValues("lista_estadoPublicacoes");
		$texto="";
		while ($row = mysql_fetch_assoc($lValues)) {
			if($i==$row["ID"])
				$texto=$texto.$row["DESCRICAO"];
		}

		$db->disconnect();
		return $texto;
	}
	
?>