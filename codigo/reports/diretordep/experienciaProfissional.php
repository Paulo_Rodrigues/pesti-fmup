<?php

/**
* Autor: Paulo Rodrigues (prodrigues@med.up.pt/paulomiguelarodrigues@gmail.com)
* Data: 15/04/2014
* Alterações: Adicionada a capacidade de poder editar e remover registos de ExperienciaProfissional. 
*/

echo "<h3>Caracterização Docentes</h3>\n";
echo "<div id='caracterizacaoDocentes'>";

echo "<table id='caracDoc' class='box-table-b'>
		<tr>
			<th>IDINV</th><th>Nome</th>
			<th>Tipo</th>
			<th>Percentagem</th>
		</tr>";

foreach ($dadosDep->ocupacaoprofissional as $i => $value){
	if($dadosDep->ocupacaoprofissional[$i]->docentecat!=0 || $dadosDep->ocupacaoprofissional[$i]->docenteper!=0){
		echo "<tr>";
		echo "<td>".$dadosDep->ocupacaoprofissional[$i]->idinv."</td>";
		echo "<td>".$dadosDep->investigadores[$dadosDep->ocupacaoprofissional[$i]->idinv]->nome."</td>";
		echo "<td id='td_exp_pro_doc_cat_" . $dadosDep->ocupacaoprofissional[$i]->id . "'>".getCat("docente",$dadosDep->ocupacaoprofissional[$i]->docentecat)."</td>";
		echo "<td id='td_exp_pro_doc_per_" . $dadosDep->ocupacaoprofissional[$i]->id . "'>".$dadosDep->ocupacaoprofissional[$i]->docenteper."</td>";
		echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-exp-pro-doc').text('" . $dadosDep->ocupacaoprofissional[$i]->id. "');\"></td>";
        echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep->ocupacaoprofissional[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-exp-pro-doc').text('" . $dadosDep->ocupacaoprofissional[$i]->id . "');apagarCaracterizacaoDocente();return false;\" ></center></td>";
     
		echo "</tr>";
	}
}

echo "</table>";
echo "<p id='chave-exp-pro-doc' hidden></p>";
echo "</div>";

echo "<h3>Caracterização Pós-Doc</h3>\n";
echo "<div id='caracterizacaoPos'>";
echo "<table id='caracPos' class='box-table-b'>
		<tr>
			<th>IDINV</th>
			<th>Nome</th>
			<th>Tipo Bolsa</th>
			<th>Percentagem</th>
		</tr>";

foreach ($dadosDep->ocupacaoprofissional as $i => $value){
	if($dadosDep->ocupacaoprofissional[$i]->investigadorcat==1){
		echo "<tr>";
		echo "<td>".$dadosDep->ocupacaoprofissional[$i]->idinv."</td>";
		echo "<td>".$dadosDep->investigadores[$dadosDep->ocupacaoprofissional[$i]->idinv]->nome."</td>";
		echo "<td id='td_exp_pro_pdoc_bol_" . $dadosDep->ocupacaoprofissional[$i]->id . "'>".getBolsaInvestigador($dadosDep->ocupacaoprofissional[$i]->investigadorbolsa)."</td>";
		echo "<td id='td_exp_pro_pdoc_per_" . $dadosDep->ocupacaoprofissional[$i]->id . "'>".$dadosDep->ocupacaoprofissional[$i]->investigadorper."</td>";
		echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-exp-pro-pdoc').text('" . $dadosDep->ocupacaoprofissional[$i]->id. "');\"></td>";
        echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep->ocupacaoprofissional[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-exp-pro-pdoc').text('" . $dadosDep->ocupacaoprofissional[$i]->id . "');apagarCaracterizacaoPosDoc();return false;\" ></center></td>";
     
		echo "</tr>";
	}
}

echo "</table>";
echo "<p id='chave-exp-pro-pdoc' hidden></p>";
echo "</div>";

	
	function checkSelectedOP($i,$valor){
	
		if($i==$valor){
			return true;
		}else{
			return false;
		}
	
	}

	
	function getCat($tipo,$valorcat) {
		$resultado="";
		$db = new Database();
		$lValues =$db->getLookupValues("lista_tipo".$tipo);
	
	
		while ($row = mysql_fetch_assoc($lValues)) {
			if(checkSelectedOP($row["ID"],$valorcat))
				$resultado=$row["DESCRICAO"];
		}
		$db->disconnect();
		return $resultado;
	}
	
	function getCatInvestigador($tipo,$valorcat) {
		$resultado="";
		$db = new Database();
		$lValues =$db->getLookupValues("lista_tipo".$tipo);
		while ($row = mysql_fetch_assoc($lValues)) {
			if(checkSelectedOP($row["ID"],$valorcat))
				$resultado=$row["DESCRICAO"];
		}
	
		$db->disconnect();
		return $resultado;
	}
	
	function getBolsaInvestigador($bolsa) {
		$resultado="";
		$db = new Database();
		$lValues =$db->getLookupValues("lista_TipoBolsas");
	
		while ($row = mysql_fetch_assoc($lValues)) {
			if(checkSelectedOP($row["ID"],$bolsa))
				$resultado=$row["DESCRICAO"];
		}

		$db->disconnect();
		return $resultado;
	}
	

	
	?>