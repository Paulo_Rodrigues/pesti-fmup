<?php

echo "<h3>Ações de Divulgação</h3>\n";
		
echo "<div id='acaodivulgacao'>
	  <table class='box-table-b'>
	  <thead>
		<tr>
			<th>IDINV</th>
			<th>Nome</th>
			<th>Título</th>
			<th>Data</th>
			<th>Local</th>
			<th>Nº de Participantes</th>
		</tr>
    </thead>
    <tbody>";

    foreach ($dadosDep->acoesdivulgacao as $i => $value){
		echo "<tr>";
			echo "<td>". $dadosDep->acoesdivulgacao[$i]->idinv. "</td>";
			echo "<td>".$dadosDep->investigadores[$dadosDep->acoesdivulgacao[$i]->idinv]->nome. "</td>";
			echo "<td id='td_acaodivulgacao_titulo_" .$dadosDep->acoesdivulgacao[$i]->id. "'>". $dadosDep->acoesdivulgacao[$i]->titulo ."</td>";
			echo "<td id='td_acaodivulgacao_data_" .$dadosDep->acoesdivulgacao[$i]->id. "'>". $dadosDep->acoesdivulgacao[$i]->data ."</td>";
			echo "<td id='td_acaodivulgacao_local_" .$dadosDep->acoesdivulgacao[$i]->id. "'>". $dadosDep->acoesdivulgacao[$i]->local ."</td>";
			echo "<td id='td_acaodivulgacao_npart_" .$dadosDep->acoesdivulgacao[$i]->id. "'>". $dadosDep->acoesdivulgacao[$i]->npart ."</td>";
			echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-acaodivulgacao').text('" . $dadosDep->acoesdivulgacao[$i]->id . "');\"></td>";
			echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep->acoesdivulgacao[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-acaodivulgacao').text('" . $dadosDep->acoesdivulgacao[$i]->id . "');apagarAcaoDivulgacao();return false;\" ></center></td>";   
		echo "</tr>";
    }

    echo "</tbody>
    </table>
    <p id='chave-acaodivulgacao' hidden></p>
</div>";	
?>