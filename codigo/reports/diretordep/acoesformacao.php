<?php

echo "<fieldset class='normal'>\n";
			echo "<legend>Ações de Formação dadas /<i>Training conducted</i></legend>\n";

/*
echo "<p class='ppthelp'>Indique o(s) tipo(s) de Unidade de Investigação a que pertence, o(s) seu(s) nome(s) da lista (quando é “Outra” tem de preencher à mão) e a percentagem de tempo a ela(s) oficialmente adstrito. Se foi o Responsável pela Unidade, clicar na caixa respetiva. Finalmente, confirme a Avaliação 2007 da sua Unidade, não fazendo nada na última caixa. Caso contrário, escreva aí a avaliação e algum comentário.</p>";			

echo "<p class='penhelp'><i>Indicate the type(s) of Research Unit(s) to which you belong, pick the name(s) from the list (when “Outra” the name must be filled by hand), and fill the percentage of time officially applicable (to each one). If you were the Unit’s Head please click in the corresponding box. Finally, please confirm the 2007 Evaluation by doing nothing on the last box. Otherwise, please write there the correct Evaluation and a commentary.</i></p>";
	*/		
			
echo "
    <table  class='box-table-b'>
    <!-- Results table headers -->
    <tr>

      <th>Título<p><i>Title</i></p></th>
      <th>Nº Horas<p><i>Nr. Hours</i></p></th>
      <th>Preço por Formando(euros)<p><i>Cost by Trainee(euros)</i></p></th>
      <th>Tipo de Formandos<p><i>Type of Trainees</i></p></th>
      <th>Nº de Formandos<p><i>Number of Trainees</i></p></th>
      <th>Data Início<p><i>Begin Date</i></p></th>
      <th>Data Fim<p><i>End Date</i></p></th>
       <th>Cidade, País<p><i>City, Country</i></p></th>
       <th>Objetivo<p><i>Objective</i></p></th>
    </tr>";
			    
    foreach ($questionario->acoesformacao as $i => $value){
    	
			
			echo "<tr>";

			echo "<td>";
			echo $questionario->acoesformacao[$i]->titulo;			
			echo "</td>";
    					
			echo "<td>";
			echo $questionario->acoesformacao[$i]->horas;			
			echo "</td>";

			echo "<td>";
			echo $questionario->acoesformacao[$i]->preco;			
			echo "</td>";

			echo "<td>";
			//echo "<input class='inp-textAuto' type='text' name='_formandos_".$i."' value='".$questionario->acoesformacao[$i]->formandos."'>".getTipoFormando($i);	
			getTipoFormando($i);				
			echo "</td>";
	    	
			echo "<td>";
			$questionario->acoesformacao[$i]->nformandos;
			echo "</td>";

			
			echo "<td>";
			echo $questionario->acoesformacao[$i]->dataini;			
			echo "</td>";			

			echo "<td>";
			echo $questionario->acoesformacao[$i]->datafim;			
			echo "</td>";	
			
			echo "<td>";
			echo $questionario->acoesformacao[$i]->local;			
			echo "</td>";

			echo "<td>";
			echo $questionario->acoesformacao[$i]->objectivo;			
			echo "</td>";
			
			echo "</tr>";

    }
    echo "</table>";


	echo "</fieldset>";
	
	echo "<fieldset class='normal'>\n";
	echo "<legend>Acreditação como Formador /<i>Accreditation</i></legend>\n";
	
	/*
	 echo "<p class='ppthelp'>Indique o(s) tipo(s) de Unidade de Investigação a que pertence, o(s) seu(s) nome(s) da lista (quando é “Outra” tem de preencher à mão) e a percentagem de tempo a ela(s) oficialmente adstrito. Se foi o Responsável pela Unidade, clicar na caixa respetiva. Finalmente, confirme a Avaliação 2007 da sua Unidade, não fazendo nada na última caixa. Caso contrário, escreva aí a avaliação e algum comentário.</p>";
	
	echo "<p class='penhelp'><i>Indicate the type(s) of Research Unit(s) to which you belong, pick the name(s) from the list (when “Outra” the name must be filled by hand), and fill the percentage of time officially applicable (to each one). If you were the Unit’s Head please click in the corresponding box. Finally, please confirm the 2007 Evaluation by doing nothing on the last box. Otherwise, please write there the correct Evaluation and a commentary.</i></p>";
	*/
		
	echo "
	<table  class='box-table-b'>
	<!-- Results table headers -->
	<tr>
		
		<th>Intituicao<p><i>Institution</i></p></th>
		<th>Título da Acreditação<p><i>Accreditation Title</i></p></th>
		<th>Data Início<p><i>Begin Date</i></p></th>
		<th>Data Fim<p><i>End Date</i></p></th>
	</tr>";
	 
	foreach ($questionario->acreditacoes as $i => $value){
		 
			
		echo "<tr>";
			
		echo "<td>";
		echo $questionario->acreditacoes[$i]->instituicao;
		echo "</td>";
	
		echo "<td>";
		echo $questionario->acreditacoes[$i]->tipo;
		echo "</td>";
	
		echo "<td>";
		echo $questionario->acreditacoes[$i]->dataini;
		echo "</td>";
	
		echo "<td>";
		echo $questionario->acreditacoes[$i]->datafim;
		echo "</td>";
			
		echo "</tr>";
	
	}

	echo "</table>";

	echo "</fieldset>";
	
	

	
	function getTipoFormando($i) {
		global $questionario;
		$db = new Database();
		$lValues =$db->getLookupValues("lista_tipoformandos");
	
	
		while ($row = mysql_fetch_assoc($lValues)) {	
			if($questionario->acoesformacao[$i]->formandos==$row["ID"])
				echo $row["DESCRICAO"];
		}
		echo "</SELECT>";
		$db->disconnect();
				
	}	
				

	
?>