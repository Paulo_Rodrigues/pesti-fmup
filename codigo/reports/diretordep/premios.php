<?php

/**
* Autor: Paulo Rodrigues (prodrigues@med.up.pt/paulomiguelarodrigues@gmail.com)
* Data: 24/04/2014
* Alterações: Adicionada a capacidade de poder editar e remover registos de prémios. 
*/

echo "<h3>Prémios</h3>\n";
echo "<div id='premios'>";
		
echo "<table class='box-table-b'>
		<tr>
			<th>IDINV</th>
			<th>Nome</th>
			<th>Tipo</th>
			<th>Nome</th>
			<th>Contexto</th>
			<th>Montante</th>
			<th>Link</th>
		</tr>";
			    
    foreach ($dadosDep->premios as $i => $value){
		echo "<tr>";
			echo "<td>".$dadosDep->premios[$i]->idinv."</td>";
			
			echo "<td>".$dadosDep->investigadores[$dadosDep->premios[$i]->idinv]->nome."</td>";
		   	echo "<td id='td_premios_tipo_" . $dadosDep->premios[$i]->id . "'>";
			getTipoPremios($dadosDep->premios[$i]->tipo);
			echo "</td>";	
			echo "<td id='td_premios_nome_" . $dadosDep->premios[$i]->id . "'>".$dadosDep->premios[$i]->nome."</td>";
			echo "<td id='td_premios_contexto_" . $dadosDep->premios[$i]->id . "'>".$dadosDep->premios[$i]->contexto."</td>";
			echo "<td id='td_premios_montante_" . $dadosDep->premios[$i]->id . "'>".$dadosDep->premios[$i]->montante." €</td>";
			echo "<td id='td_premios_link_" . $dadosDep->premios[$i]->id . "'>".$dadosDep->premios[$i]->link."</td>";
			echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-premios').text('" . $dadosDep->premios[$i]->id . "');\"></td>";
        	echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep->premios[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-premios').text('" . $dadosDep->premios[$i]->id . "');apagarPremio();return false;\" ></center></td>";
		echo "</tr>";	    	
    } 

    echo "</tbody>
    </table>
    <p id='chave-premios' hidden></p>
  </div>";

	function getTipoPremios($i) {	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_tipopremio");	

		while ($row = mysql_fetch_assoc($lValues)) {	
			if($i==$row["ID"])
				echo $row["DESCRICAO"];
		}
		echo "</SELECT><br />\n";
		$db->disconnect();				
	}	
				
	?>