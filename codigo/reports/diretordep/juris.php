<?php
echo "<h3>Júris</h3>\n
	  <div id='juris'>";		
			
echo "<table class='box-table-b'>
		<thead>
			<tr>
				<th></th>
				<th></th>
				<th colspan='4'>Agregacao</th>
				<th colspan='4'>Doutoramento</i></th>
				<th colspan='4'>Mestrado Científico</i></th>
				<th colspan='4'>Mestrado Integrado</i></th>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td colspan='2'>FMUP</td>
				<td colspan='2'>Exterior</td>
				<td colspan='2'>FMUP</td>
				<td colspan='2'>Exterior</td>
				<td colspan='2'>FMUP</td>
				<td colspan='2'>Exterior</td>
				<td colspan='2'>FMUP</td>
				<td colspan='2'>Exterior</td>
			</tr>
			<tr>
				<td>IDINV</td>
				<td>Nome</td>
				<td>Arguente</td>
				<td>Outro</td>
				<td>Arguente</td>
				<td>Outro</td>
				<td>Arguente</td>
				<td>Outro</td>
				<td>Arguente</td>
				<td>Outro</td>
				<td>Arguente</td>
				<td>Outro</td>
				<td>Arguente</td>
				<td>Outro</td>
				<td>Arguente</td>
				<td>Outro</td>
				<td>Arguente</td>
				<td>Outro</td>
			</tr>
		</thead>
		<tbody>";

foreach ($dadosDep->juris as $i => $value){
	if($dadosDep->juris[$i]->agregacaoargfmup!=0 ||
			$dadosDep->juris[$i]->agregacaovogalfmup!=0 ||
			$dadosDep->juris[$i]->agregacaoargext!=0 ||
			$dadosDep->juris[$i]->agregacaovogalext!=0 ||
			$dadosDep->juris[$i]->doutoramentoargfmup!=0 ||
			$dadosDep->juris[$i]->doutoramentovogalfmup!=0 ||
			$dadosDep->juris[$i]->doutoramentoargext!=0 ||
			$dadosDep->juris[$i]->doutoramentovogalext!=0 ||
			$dadosDep->juris[$i]->mestradoargfmup!=0 ||
			$dadosDep->juris[$i]->mestradovogalfmup!=0 ||
			$dadosDep->juris[$i]->mestradoargext!=0 ||
			$dadosDep->juris[$i]->mestradovogalext!=0 ||
			$dadosDep->juris[$i]->mestradointargfmup!=0 ||
			$dadosDep->juris[$i]->mestradointvogalfmup!=0 ||
			$dadosDep->juris[$i]->mestradointargext!=0 ||
			$dadosDep->juris[$i]->mestradointvogalext!=0){
		echo "<tr>";
			echo "<td>".$dadosDep->juris[$i]->idinv."</td>";
			echo "<td>".$dadosDep->investigadores[$dadosDep->juris[$i]->idinv]->nome."</td>";
			echo "<td id='td_juris_agrargfmup_" . $dadosDep->juris[$i]->id . "'>".$dadosDep->juris[$i]->agregacaoargfmup."</td>";
			echo "<td id='td_juris_agrvogalfmup_" . $dadosDep->juris[$i]->id . "'>".$dadosDep->juris[$i]->agregacaovogalfmup."</td>";
			echo "<td id='td_juris_agrargext_" . $dadosDep->juris[$i]->id . "'>".$dadosDep->juris[$i]->agregacaoargext."</td>";
			echo "<td id='td_juris_agrvogalext_" . $dadosDep->juris[$i]->id . "'>".$dadosDep->juris[$i]->agregacaovogalext."</td>";
			echo "<td id='td_juris_doutargfmup_" . $dadosDep->juris[$i]->id . "'>".$dadosDep->juris[$i]->doutoramentoargfmup."</td>";
			echo "<td id='td_juris_doutvogalfmup_" . $dadosDep->juris[$i]->id . "'>".$dadosDep->juris[$i]->doutoramentovogalfmup."</td>";
			echo "<td id='td_juris_doutargext_" . $dadosDep->juris[$i]->id . "'>".$dadosDep->juris[$i]->doutoramentoargext."</td>";
			echo "<td id='td_juris_doutvogalext_" . $dadosDep->juris[$i]->id . "'>".$dadosDep->juris[$i]->doutoramentovogalext."</td>";
			echo "<td id='td_juris_mesargfmup_" . $dadosDep->juris[$i]->id . "'>".$dadosDep->juris[$i]->mestradoargfmup."</td>";
			echo "<td id='td_juris_mesvogalfmup_" . $dadosDep->juris[$i]->id . "'>".$dadosDep->juris[$i]->mestradovogalfmup."</td>";
			echo "<td id='td_juris_mesargext_" . $dadosDep->juris[$i]->id . "'>".$dadosDep->juris[$i]->mestradoargext."</td>";
			echo "<td id='td_juris_mesvogalext_" . $dadosDep->juris[$i]->id . "'>".$dadosDep->juris[$i]->mestradovogalext."</td>";
			echo "<td id='td_juris_mesintfmup_" . $dadosDep->juris[$i]->id . "'>".$dadosDep->juris[$i]->mestradointargfmup."</td>";
			echo "<td id='td_juris_mesintvogalfmup_" . $dadosDep->juris[$i]->id . "'>".$dadosDep->juris[$i]->mestradointvogalfmup."</td>";
			echo "<td id='td_juris_mesintargext_" . $dadosDep->juris[$i]->id . "'>".$dadosDep->juris[$i]->mestradointargext."</td>";
			echo "<td id='td_juris_mesintvogalext_" . $dadosDep->juris[$i]->id . "'>".$dadosDep->juris[$i]->mestradointvogalext."</td>";
			echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-juris').text('" . $dadosDep->juris[$i]->id . "');\"></td>";
   			echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep->juris[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-juris').text('" . $dadosDep->juris[$i]->id . "');apagarJuri();return false;\" ></center></td>";   
	
		echo "</tr>";
	}
}
	echo "</tbody>
	</table>
	<p id='chave-juris' hidden ></p>
</div>";
	?>