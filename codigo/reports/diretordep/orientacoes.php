<?php

echo "<h3>Orientações</h3>
	  <div id='orientacoes'>\n";

echo "<table class='box-table-b'>
		<thead>
			<tr>
				<th>IDINV</th>
				<th>Nome</th>
				<th>Tipo de Orientação</th>
				<th>Tipo de Orientador</th>
				<th>Estado</th>
				<th>Título da Tese em Português</th>
			</tr>
		</thead>
		<tbody>";
			    
    foreach ($dadosDep->orientacoes as $i => $value){
		echo "<tr>";
			echo "<td>".$dadosDep->orientacoes[$i]->idinv."</td>";
			echo "<td>".$dadosDep->investigadores[$dadosDep->orientacoes[$i]->idinv]->nome."</td>";
			echo "<td id='td_orientacoes_tipo_" . $dadosDep->orientacoes[$i]->id . "''>";
			getTipoOrientacao($dadosDep->orientacoes[$i]->tipo_orientacao);
			echo "</td>";
			echo "<td id='td_orientacoes_tipoori_" . $dadosDep->orientacoes[$i]->id . "''>";
			getTipoOrientador($dadosDep->orientacoes[$i]->tipo_orientador);
			echo "</td>";
			echo "<td id='td_orientacoes_estado_" . $dadosDep->orientacoes[$i]->id . "''>";
			getOrientacaoEstado($dadosDep->orientacoes[$i]->tipo_orientacao);
			echo "</td>";
			echo "<td id='td_orientacoes_titulo_" . $dadosDep->orientacoes[$i]->id . "''>".$dadosDep->orientacoes[$i]->titulo."</td>";
			echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-orientacoes').text('" . $dadosDep->orientacoes[$i]->id . "');\"></td>";
   			echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep->orientacoes[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-orientacoes').text('" . $dadosDep->orientacoes[$i]->id . "');apagarOrientacao();return false;\" ></center></td>";    
		echo "</tr>";	    	
    } 
    
    echo "</tbody>
    </table>
    <p id='chave-orientacoes' hidden></p>
</div>";   
    
	function getTipoOrientacao($i) {
		global $dadosDep;
		$db = new Database();
		$lValues =$db->getLookupValues("lista_tipoOrientacao");

		while ($row = mysql_fetch_assoc($lValues)) {	
			if($i==$row["ID"])
				echo $row["DESCRICAO"];
		}

		$db->disconnect();				
	}	
	
	function getTipoOrientador($i) {	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_tipoOrientador");	
		
		while ($row = mysql_fetch_assoc($lValues)) {	
			if($i==$row["ID"])
				echo $row["DESCRICAO"];
		}
		$db->disconnect();				
	}	
	
	function getOrientacaoEstado($i) {	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_estadosOrientacao");
	
		while ($row = mysql_fetch_assoc($lValues)) {
			if($i==$row["ID"])
				echo $row["DESCRICAO"];
		}
		
		$db->disconnect();	
	}
		
?>