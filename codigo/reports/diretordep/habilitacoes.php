<?php

echo "<h3>Curso(s) a frequentar</h3>\n";

echo "<div id='habilitacoes'>\n";

//echo "<legend>Curso(s) superior(es) a frequentar ou concluído(s)/<i>Graduate course(s) attended or finished</i></legend>\n";
			

echo "<table  class='box-table-b' id='habs'>
		<tr>
		<th>ID Inv</th><th>Nome</th>
			<th>Ano de Início<p><i>Start Year</i></p></th>
			<th>Ano de Fim<p><i>End Year</i></p></th>
			<th>Curso<p><i>Course</i></p></th>
			<th>Instituição<p><i>Institution</i></p></th>
			<th>Grau/Título<p><i>Degree/Title</i></p></th>
			<th>Distinção<p><i>Distinction</i></p></th>
		<th>Bolsa<p><i>Scholarship</i></p></th>
		<th>Título da Tese em Português<p><i>Thesis title in Portuguese</i></p></th>
		<th>Percentagem<p><i>Percentage</i></p></th>
		<th>Orientador<p><i>Supervisor</i></p></th>
		<th>Instituição do Orientador<p><i>Supervisor Institution</i></p></th>
		<th>COrientador<p><i>COSupervisor</i></p></th>
		<th>Instituição do Coorientador<p><i>CoSupervisor Institution</i></p></th>
		
		
		</tr>";
    
    foreach ($dadosDep->habilitacoes as $i => $value){
    	if($dadosDep->habilitacoes[$i]->anofim!='93'){
    		echo "<tr>";
    		echo "<td>".$dadosDep->habilitacoes[$i]->idinv . "</td>";
    		echo "<td id='td_nome_hab_" . $dadosDep->habilitacoes[$i]->id . "'>".$dadosDep->investigadores[$dadosDep->habilitacoes[$i]->idinv]->nome."</td>";
    		echo "<td id='td_ano_ini_hab_" . $dadosDep->habilitacoes[$i]->id . "'>";	
    		getAnoInicio($dadosDep->habilitacoes[$i]->anoinicio);
			echo "</td>";
			echo "<td id='td_ano_fim_hab_" . $dadosDep->habilitacoes[$i]->id . "'>";
			getAnoFim($dadosDep->habilitacoes[$i]->anofim);
			echo "</td>";			
			echo "<td id='td_curso_hab_" . $dadosDep->habilitacoes[$i]->id . "'>".$dadosDep->habilitacoes[$i]->curso."</td>";
			echo "<td id='td_instituicao_hab_" . $dadosDep->habilitacoes[$i]->id . "''>".$dadosDep->habilitacoes[$i]->instituicao."</td>";
			echo "<td id='td_grau_hab_" . $dadosDep->habilitacoes[$i]->id . "'>";
			getGrau($dadosDep->habilitacoes[$i]->grau);	
	    	echo "</td>";
	    	echo "<td id='td_dist_hab_" . $dadosDep->habilitacoes[$i]->id . "'>";
	    	getDistincao($dadosDep->habilitacoes[$i]->distincao);			
	    	echo "</td>";
	    	echo "<td id='td_bolsa_hab_" . $dadosDep->habilitacoes[$i]->id . "'>";
	    	getBolsa($dadosDep->habilitacoes[$i]->bolsa);	
	    	echo "</td>";
	    	echo "<td id='td_tit_hab_" . $dadosDep->habilitacoes[$i]->id . "'>".$dadosDep->habilitacoes[$i]->titulo."</td>";
		    echo "<td id='td_perc_hab_" . $dadosDep->habilitacoes[$i]->id . "'>".$dadosDep->habilitacoes[$i]->percentagem."</td>";
		    echo "<td id='td_orientador_hab_" . $dadosDep->habilitacoes[$i]->id . "'>".$dadosDep->habilitacoes[$i]->orientador."</td>";
		    echo "<td id='td_oinstituicao_hab_" . $dadosDep->habilitacoes[$i]->id . "'>".$dadosDep->habilitacoes[$i]->oinstituicao."</td>";
		    echo "<td id='td_corientador_hab_" . $dadosDep->habilitacoes[$i]->id . "'>".$dadosDep->habilitacoes[$i]->corientador."</td>";
		    echo "<td id='td_coinstituicao_hab_" . $dadosDep->habilitacoes[$i]->id . "'>".$dadosDep->habilitacoes[$i]->coinstituicao."</td>";
		    echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-hab').text('". $dadosDep->habilitacoes[$i]->id ."');\"></td>";
        	echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep->habilitacoes[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-hab').text('" . $dadosDep->habilitacoes[$i]->id . "');apagarHabilitacao();return false;\" ></center></td>";
   
	    	echo "</tr>";
    	}	    	
    }
    echo "<tr><td>Concluídos</td></tr>";
    foreach ($dadosDep->habilitacoes as $i => $value){    	
    	if($dadosDep->habilitacoes[$i]->anofim=='93'){
    	echo "<tr>";
    	echo "<td>".$dadosDep->habilitacoes[$i]->idinv."</td>";
    	echo "<td>".$dadosDep->investigadores[$dadosDep->habilitacoes[$i]->idinv]->nome."</td>";
    	echo "<td>";
    	getAnoInicio($dadosDep->habilitacoes[$i]->anoinicio);
    	echo "</td>";
    	echo "<td>";
    	getAnoFim($dadosDep->habilitacoes[$i]->anofim);
    	echo "</td>";
    	echo "<td>".$dadosDep->habilitacoes[$i]->curso."</td>";
    	echo "<td>".$dadosDep->habilitacoes[$i]->instituicao."</td>";
    	echo "<td>";
    	getGrau($dadosDep->habilitacoes[$i]->grau);
    	echo "</td>";
    	echo "<td>";
    	getDistincao($dadosDep->habilitacoes[$i]->distincao);
    	
    	echo "</td>";
    	echo "<td>";
    	getBolsa($dadosDep->habilitacoes[$i]->bolsa);
    	echo "</td>";
    	echo "<td>".$dadosDep->habilitacoes[$i]->titulo."</td>";
    	echo "<td>".$dadosDep->habilitacoes[$i]->percentagem."\n</td>";
    	echo "<td>".$dadosDep->habilitacoes[$i]->orientador."\n</td>";
    	echo "<td>".$dadosDep->habilitacoes[$i]->oinstituicao."\n</td>";
    	echo "<td>".$dadosDep->habilitacoes[$i]->corientador."\n</td>";
    	echo "<td>".$dadosDep->habilitacoes[$i]->coinstituicao."\n</td>";
    	echo "</tr>";
    	}
    }

    echo "</table>";
    echo "<br />";

	echo "<p id='chave-hab' hidden></p>
	<input type='hidden' name='apagaRegHabilitacao'/>";
	echo "</div>";
		
	function getAnoInicio($i){
	
		$db = new Database();
		$lValues =$db->getLookupValuesAnoInicio("lista_anoslectivos");
	
		while ($row = mysql_fetch_assoc($lValues)) {	
			if($i==$row["ID"])
				echo $row["DESCRICAO"];
		}
		
		$db->disconnect();
				
	}

	
			
	function checkAnoInicio($id,$i){
		global $dadosDep;
		if($dadosDep->habilitacoes[$i]->anoinicio==$id)
			return true;
		else 
			return false;
	}
	
	
	function getAnoFim($i) {
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_anoslectivos");
		while ($row = mysql_fetch_assoc($lValues)) {	
			if($i==$row["ID"]){
				echo $row["DESCRICAO"];
			}
		}
		$db->disconnect();	
	}
				
	function checkAnofim($id,$i){
		global $dadosDep;

		if($dadosDep->habilitacoes[$i]->anofim==$id)
			return true;
		else 
			return false;
	}
	
	function getGrau($i) {
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_graucursos");
	
		
		while ($row = mysql_fetch_assoc($lValues)) {	
			if($i==$row["ID"])
				echo $row["DESCRICAO"];
		}
		echo "</SELECT><br />\n";
		$db->disconnect();
				
	}	
				
	function checkGrau($id,$i){
		global $dadosDep;
		if($dadosDep->habilitacoes[$i]->grau==$id)
			return true;
		else 
			return false;
	}
	
	
	function getDistincao($i) {
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_distincaoValores");
	
		while ($row = mysql_fetch_assoc($lValues)) {
			if($i==$row["ID"])
				echo $row["DESCRICAO"];
		}
		$db->disconnect();
	
	}
	
	function checkDistincao($id,$i){
		global $dadosDep;
		if($dadosDep->habilitacoes[$i]->distincao==$id)
			return true;
		else
			return false;
	}
	
	
	function getBolsa($i) {
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_TipoBolsas");

	
		while ($row = mysql_fetch_assoc($lValues)) {
			if($i==$row["ID"])
				echo $row["DESCRICAO"];
		}
		$db->disconnect();
	
	}
	
	function checkBolsa($id,$i){
		global $dadosDep;
		if($dadosDep->habilitacoes[$i]->bolsa==$id)
			return true;
		else
			return false;
	}
	

	?>