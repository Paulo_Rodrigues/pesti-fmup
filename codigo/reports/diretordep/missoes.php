<?php

/**
* Autor: Paulo Rodrigues (prodrigues@med.up.pt/paulomiguelarodrigues@gmail.com)
* Data: 29/04/2014
* Alterações: Adicionada a capacidade de poder editar e remover registos de missões. 
*/

echo "<h3>Missões</h3>\n
	  <div id='missoes'>";

echo "<table class='box-table-b'>
		<thead>
			<tr>
				<th>ID Inv</th>
				<th>Nome</th>
				<th>Data de Inicio</th>
				<th>Data de Fim</th>
				<th>Motivacao</th>
				<th>Instituição de Acolhimento</th>
				<th>Pais</th>
				<th>Âmbito de Tese</th>
			</tr>
		</thead>
		<tbody>";
			    
    foreach ($dadosDep->missoes as $i => $value){
		echo "<tr>";
			echo "<td>".$dadosDep->missoes[$i]->idinv."</td>";
			echo "<td>".$dadosDep->investigadores[$dadosDep->missoes[$i]->idinv]->nome."</td>";
			echo "<td id='td_missoes_dataini_" . $dadosDep->missoes[$i]->id . "'>".$dadosDep->missoes[$i]->datainicio."</td>";
			echo "<td id='td_missoes_datafim_" . $dadosDep->missoes[$i]->id . "'>".$dadosDep->missoes[$i]->datafim."</td>";
			echo "<td id='td_missoes_motivacao_" . $dadosDep->missoes[$i]->id . "'>".$dadosDep->missoes[$i]->motivacao."</td>";
			echo "<td id='td_missoes_instituicao_" . $dadosDep->missoes[$i]->id . "'>".$dadosDep->missoes[$i]->instituicao."</td>";
			echo "<td id='td_missoes_pais_" . $dadosDep->missoes[$i]->id . "'>";
			getPaisesMissoes($dadosDep->missoes[$i]->pais);
			echo "</td>";
			echo "<td id='td_missoes_ambtese_" . $dadosDep->missoes[$i]->id . "'>".checkAmbTese($i)."</td>";
		 	echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-missoes').text('" . $dadosDep->missoes[$i]->id . "');\"></td>";
   			echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep->missoes[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-missoes').text('" . $dadosDep->missoes[$i]->id . "');apagarMissao();return false;\" ></center></td>";    
    	echo "</tr>";	    	
    } 

	echo "</tbody>
	</table>
	<p id='chave-missoes' hidden></p>
	</div>";
	
	function getPaisesMissoes($i){
		$db = new Database();
		$lValues =$db->getLookupValues("lista_paises");
		while ($row = mysql_fetch_assoc($lValues)) {
			if($i==$row["ID"])
				echo $row["DESCRICAO"];
		}
		$db->disconnect();	
	}	
		
	function checkAmbTese($i){
		global $dadosDep;
		if($dadosDep->missoes[$i]->ambtese==1)
			return "Sim";
		else
			return "Não";
	}

?>