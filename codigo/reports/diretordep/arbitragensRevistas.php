<?php

/**
* Autor: Paulo Rodrigues (prodrigues@med.up.pt/paulomiguelarodrigues@gmail.com)
* Data: 24/04/2014
* Alterações: Adicionada a capacidade de poder editar e remover registos de arbitragens de revistas. 
*/

echo "<h3>Editor/Árbitro</h3>\n";
echo"<div id='arbitragensRevistas'>";
			
echo "<table class='box-table-b'>
		<thead>
			<tr>
				<th>IDINV</th>
				<th>Nome</th>
				<th>Tipo</th>
				<th>ISSN</th>
				<th>Revista</th>
				<th>Link</th>
			</tr>
		</thead>
		<tbody>";
      					
      				
    foreach ($dadosDep->arbitragensRevistas as $i => $value){
		echo "<tr>";
			echo "<td>".$dadosDep->arbitragensRevistas[$i]->idinv."</td>";
			echo "<td>".$dadosDep->investigadores[$dadosDep->arbitragensRevistas[$i]->idinv]->nome."</td>";
		   	echo "<td id='td_arbrevistas_tipo_" . $dadosDep->arbitragensRevistas[$i]->id ."'>";
			getTipoArbitragem($dadosDep->arbitragensRevistas[$i]->tipo);
			echo "</td>";
			echo "<td id='td_arbrevistas_issn_" . $dadosDep->arbitragensRevistas[$i]->id ."'>".$dadosDep->arbitragensRevistas[$i]->issn."</td>";
			echo "<td id='td_arbrevistas_titulo_" . $dadosDep->arbitragensRevistas[$i]->id ."'>".$dadosDep->arbitragensRevistas[$i]->tituloRevista."</td>";
			echo "<td id='td_arbrevistas_link_" . $dadosDep->arbitragensRevistas[$i]->id ."'>".$dadosDep->arbitragensRevistas[$i]->link."</td>";
			echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-arb-revistas').text('" . $dadosDep->arbitragensRevistas[$i]->id . "');\"></td>";
	        echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep->arbitragensRevistas[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-arb-revistas').text('" . $dadosDep->arbitragensRevistas[$i]->id . "');apagarArbRevista();return false;\" ></center></td>";
  		echo "</tr>";	    	
    } 

    echo "</tbody>
    </table>
    <p id='chave-arb-revistas'></p>";

	echo "</div>";
	
	function getTipoArbitragem($i){
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_tipoarbitragens");
		
		while ($row = mysql_fetch_assoc($lValues)) {	
			if($i==$row["ID"])
				echo $row["DESCRICAO"];
		}

		$db->disconnect();				
	}		

?>