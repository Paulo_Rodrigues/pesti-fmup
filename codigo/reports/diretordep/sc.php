<?php

/**
* Autor: Paulo Rodrigues (prodrigues@med.up.pt/paulomiguelarodrigues@gmail.com)
* Data: 24/04/2014
* Alterações: Adicionada a capacidade de poder editar e remover registos de sociedades cientificas. 
*/

echo "<h3>Organizações e Sociedades Científicas</h3>\n";
echo "<div id='sc'>";

echo "<table class='box-table-b'>
		<thead>
			<tr>
				<th>IDINV</th>
				<th>Nome</th>
				<th>Tipo</th>
				<th>Data Início</th>
				<th>Data Fim</th>
				<th>Cargo</th>
				<th>Link</th>
			</tr>
		</thead>
		<tbody>";
			    
    foreach ($dadosDep->sc as $i => $value){
			echo "<tr>";
			echo "<td>".$dadosDep->sc[$i]->idinv."</td>";
		   	echo "<td id='td_sc_nome_" . $dadosDep->sc[$i]->id . "'>".$dadosDep->sc[$i]->nome."</td>";
			echo "<td id='td_sc_tipo_" . $dadosDep->sc[$i]->id . "'>";
			getTipoSC($dadosDep->sc[$i]->tipo);
			echo "</td>";
			echo "<td id='td_sc_datainicio_" . $dadosDep->sc[$i]->id . "'>".$dadosDep->sc[$i]->datainicio."</td>";	
			echo "<td id='td_sc_datafim_" . $dadosDep->sc[$i]->id . "'>".$dadosDep->sc[$i]->datafim."</td>";	
			echo "<td id='td_sc_cargo_" . $dadosDep->sc[$i]->id . "'>".$dadosDep->sc[$i]->cargo."</td>";
			echo "<td id='td_sc_link_" . $dadosDep->sc[$i]->id . "'>".$dadosDep->sc[$i]->link."</td>";
			echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-sc').text('" . $dadosDep->sc[$i]->id . "');\"></td>";
        	echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep->sc[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-sc').text('" . $dadosDep->sc[$i]->id . "');apagarSC();return false;\" ></center></td>";
	
	    	echo "</tr>";	    	
    } 

    echo "</tbody></table>
    <p id='chave-sc' hidden></p>";

	echo "</div>";

	function getTipoSC($i) {	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_tiposociedadecientifica");
		
		while ($row = mysql_fetch_assoc($lValues)) {	
			if($i==$row["ID"])
				echo $row["DESCRICAO"];
		}

		$db->disconnect();				
	}	
			
?>