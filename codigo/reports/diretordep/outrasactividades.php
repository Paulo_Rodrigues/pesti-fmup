<?php

echo "<h3>Outras Atividades</h3>\n
		<div id='outact'>";
			
echo "<table class='box-table-b'>
	  <thead>
	    <tr>
	     	<th>IDINV</th>
	     	<th>Nome</th>
	     	<th>Monografias de apoio às aulas ou a trabalho clínico</th>
			<th>Livros Didáticos</th>
			<th>Livros de Divulgação</th>
			<th>Vídeos, jogos, aplicações</th>
			<th>Páginas Internet</th>
	    </tr>
	  </thead>
	  <tbody>";
	
foreach ($dadosDep->outrasactividades as $i => $value){
	if($dadosDep->outrasactividades[$i]->texapoio!=0 || 
			$dadosDep->outrasactividades[$i]->livdidaticos!=0 || 
			$dadosDep->outrasactividades[$i]->livdivulgacao!=0 ||
			$dadosDep->outrasactividades[$i]->app!=0 ||
			$dadosDep->outrasactividades[$i]->webp!=0
			){
		echo "<tr>";
			echo "<td>".$dadosDep->outrasactividades[$i]->idinv."</td>";
			echo "<td>".$dadosDep->investigadores[$dadosDep->outrasactividades[$i]->idinv]->nome."</td>";
			echo "<td id='td_outact_texapoio_" . $dadosDep->outrasactividades[$i]->id . "'>".$dadosDep->outrasactividades[$i]->texapoio."</td>";
			echo "<td id='td_outact_livdidaticos_" . $dadosDep->outrasactividades[$i]->id . "'>".$dadosDep->outrasactividades[$i]->livdidaticos."</td>";
			echo "<td id='td_outact_livdivulgacao_" . $dadosDep->outrasactividades[$i]->id . "'>".$dadosDep->outrasactividades[$i]->livdivulgacao."</td>";
			echo "<td id='td_outact_app_" . $dadosDep->outrasactividades[$i]->id . "'>".$dadosDep->outrasactividades[$i]->app."</td>";
			echo "<td id='td_outact_webp_" . $dadosDep->outrasactividades[$i]->id . "'>".$dadosDep->outrasactividades[$i]->webp."</td>";
			echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-outact').text('" . $dadosDep->outrasactividades[$i]->id . "');\"></td>";
			echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep->outrasactividades[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-outact').text('" . $dadosDep->outrasactividades[$i]->id . "');apagarJuri();return false;\" ></center></td>";   
		echo "</tr>";
	}

}
    echo "</tbody>
    </table>
    <p id='chave-outact' hidden></p>
</div>";
?>