<?php

/**
* Autor: Paulo Rodrigues (prodrigues@med.up.pt/paulomiguelarodrigues@gmail.com)
* Data: 16/04/2014
* Alterações: Adicionada a capacidade de poder editar e remover registos de colaboradores. 
*/

echo "<h3>Colaboradores Atuais na FMUP</h3>";

echo "<div id='colaboradoresInternos'>

<table id='colIntAct' class='box-table-b'>
	<thead>
		<tr>
			<th>ID Inv</th>
			<th>Nome</th>	
			<th>Nome Científico</th>
			<th>Departmento</th>
		</tr>
	</thead>
	<tbody>";
 
foreach ($dadosDep->colaboradoresinternos as $i => $value){
	echo "<tr>";

		echo "<td>";
		echo $dadosDep->colaboradoresinternos[$i]->idinv;
		echo "</td>";

		echo "<td>".$dadosDep->investigadores[$dadosDep->colaboradoresinternos[$i]->idinv]->nome."</td>";

		echo "<td id='td_colIntAct_nomecient_" . $dadosDep->colaboradoresinternos[$i]->id . "'>";
		echo $dadosDep->colaboradoresinternos[$i]->nomecient;
		echo "</td>";

		echo "<td id='td_colIntAct_dep_" . $dadosDep->colaboradoresinternos[$i]->id . "'>";
		getDepartamentos($dadosDep->colaboradoresinternos[$i]->departamento);
		echo "</td>";

		echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-colIntAct').text('" . $dadosDep->colaboradoresinternos[$i]->id . "');\"></td>";
    	echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep->colaboradoresinternos[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-colIntAct').text('" . $dadosDep->colaboradoresinternos[$i]->id . "');apagarColActInt();return false;\" ></center></td>";
	
	echo "</tr>";
}

echo "</tbody>
</table>
<p id='chave-colIntAct' hidden></p>

      <input type='hidden' name='apagaRegHabilitacao'/>
</div>";

echo "<h3>Colaboradores Externos Atuais</h3>\n";

echo "<div id='colaboradoresExternos'>";

echo "<table id='colExtAct' class='box-table-b'>
		<thead>		
			<tr>
				<th>ID Inv</th>
				<th>Nome</th>	
				<th>Nome Científico com Iniciais</th>
				<th>Instituição</th>
				<th>País</th>
			</tr>
		</thead>
		<tbody>";
			    
    foreach ($dadosDep->colaboradores as $i => $value){
			echo "<tr>";
				echo "<td>";
				echo $dadosDep->colaboradores[$i]->idinv;
				echo "</td>";

				echo "<td>".$dadosDep->investigadores[$dadosDep->colaboradores[$i]->idinv]->nome."</td>";

				echo "<td id='td_colExtAct_nomecient_" . $dadosDep->colaboradores[$i]->id . "'>";
				echo $dadosDep->colaboradores[$i]->nomecient;
	    		echo "</td>";
	    					
				echo "<td id='td_colExtAct_instituicao_" . $dadosDep->colaboradores[$i]->id . "'>";
				echo $dadosDep->colaboradores[$i]->instituicao;
				echo "</td>";
				
				echo "<td id='td_colExtAct_pais_" . $dadosDep->colaboradores[$i]->id . "'>";
				getPaisesCol($dadosDep->colaboradores[$i]->pais);
				echo "</td>";

				echo "<td><img src=\"../../images/icon_edit.png\" onclick=\"$('#chave-colExtAct').text('" . $dadosDep->colaboradores[$i]->id . "');\"></td>";
    			echo "<td><center><input type='image' src=\"../../images/icon_delete.png\" id='td" . $dadosDep->colaboradores[$i]->id . "' value='apagar' name='navOption' onclick=\"$('#chave-colExtAct').text('" . $dadosDep->colaboradores[$i]->id . "');apagarColExtInt();return false;\" ></center></td>";
	
	    	echo "</tr>";
    }

    echo "</tbody>
    </table> <p id='chave-colExtAct' hidden></p>
   </div>";

	
	function getPaisesCol($i){
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_paises");

		while ($row = mysql_fetch_assoc($lValues)) {	
			if($i==$row["ID"])
				echo $row["DESCRICAO"];
		}

		$db->disconnect();
				
	}	
				
	function checkPaisCol($id,$i){
		global $dadosDep;
		if($dadosDep->colaboradores[$i]->pais==$id)
			return true;
		else 
			return false;
	}
	function getDepartamentos($i){
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_servicosfmup");
	
		while ($row = mysql_fetch_assoc($lValues)) {
			if($row["ID"]==$i)
				echo $row["DESCRICAO"];
		}
		$db->disconnect();
	}
	
	function checkDepartamento($id,$i){
		global $dadosDep;
		if($dadosDep->colaboradoresinternos[$i]->departamento==$id)
			return true;
		else
			return false;
	}
	
	
	?>