<?php
echo "<div id='content2' style='display: inline;'>";

echo "<fieldset class='normal'>\n";
//echo "<legend>Curso(s) superior(es) a frequentar ou concluído(s)/<i>Graduate course(s) attended or finished</i></legend>\n";
echo "<legend>Curso(s) a frequentar /<i> Course(s) being attended</i></legend>\n";			

echo "<table class='box-table-b'>
    					<tr>
      					<th>Ano de Início<p><i>Start Year</i></p></th>
      					<th>Ano de Fim<p><i>End Year</i></p></th>
      					<th>Curso<p><i>Course</i></p></th>
      					<th>Instituição<p><i>Institution</i></p></th>
      					<th>Grau/Título<p><i>Degree/Title</i></p></th>
      					<th>Distinção<p><i>Distinction</i></p></th>
						<th>Bolsa<p><i>Scholarship</i></p></th>
						<th>Título da Tese em Português<p><i>Thesis title in Portuguese</i></p></th>
						<th>Percentagem<p><i>Percentage</i></p></th>
						<th>Orientador<p><i>Supervisor</i></p></th>
						<th>Instituição do Orientador<p><i>Supervisor Institution</i></p></th>
						<th>COrientador<p><i>COSupervisor</i></p></th>
						<th>Instituição do Coorientador<p><i>CoSupervisor Institution</i></p></th>
						</tr>";

    foreach ($questionario->habilitacoes as $i => $value){
			echo "<td>";	
    		getAnoInicio($i);
			echo "</td>";
			echo "<td>";
			getAnoFim($i);
			echo "</td>";
			//echo "<td><input class='inp-textAuto' type='text' name='curso".$i."' value='".$questionario->habilitacoes[$i]->curso."'>\n</td>";
			
			echo "<td>".$questionario->habilitacoes[$i]->curso."</td>";
			echo "<td>".$questionario->habilitacoes[$i]->instituicao."</td>";
				
			
			//echo "<td><input class='inp-textAuto' type='text' name='instituicao".$i."' value='".$questionario->habilitacoes[$i]->instituicao."'></td>";
			echo "<td>";
			getGrau($i);	
	    	echo "</td>";
	    	echo "<td>";
	    	getDistincao($i);
			/*if ($questionario->habilitacoes[$i]->distincao==1) {
			//echo "<input type='checkbox' name='responsavel".$i."' value='".$questionario->unidadesinvestigacao[$i]->responsavel."' checked></p>\n";
				echo "<input type='checkbox' name='distincao".$i."' value='1' checked>\n";
			}else {
				echo "<input type='checkbox' name='distincao".$i."'  value='1'>";
			}*/
	    	echo "</td>";
	    	echo "<td>";
	    	getBolsa($i);	
	    	echo "</td>";
	    	echo "<td>".$questionario->habilitacoes[$i]->titulo."</td>";
		    //echo "<td><input class='inp-textAuto' type='text' name='percentagem".$i."' value='".$questionario->habilitacoes[$i]->percentagem."'>\n</td>";
		    echo "<td>".$questionario->habilitacoes[$i]->percentagem."\n</td>";
		    echo "<td>".$questionario->habilitacoes[$i]->orientador."\n</td>";
		    echo "<td>".$questionario->habilitacoes[$i]->oinstituicao."\n</td>";
		    echo "<td>".$questionario->habilitacoes[$i]->corientador."\n</td>";
		    echo "<td>".$questionario->habilitacoes[$i]->coinstituicao."\n</td>";
	    	echo "</tr>";	    	
    } 

    echo "</table>";
    echo "<br />";


	echo "<input type='hidden' name='apagaRegHabilitacao'/>";
	echo "</fieldset>";
	
	echo "</div>";
	
	
	
	function getAnoInicio($i){
	
		$db = new Database();
		$lValues =$db->getLookupValuesAnoInicio("lista_anoslectivos");
	
		while ($row = mysql_fetch_assoc($lValues)) {	
			if(checkAnoInicio($row["ID"],$i))
				echo $row["DESCRICAO"];
		}
		
		$db->disconnect();
				
	}	
				
	function checkAnoInicio($id,$i){
		global $questionario;
		if($questionario->habilitacoes[$i]->anoinicio==$id)
			return true;
		else 
			return false;
	}
	
	
	function getAnoFim($i) {
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_anoslectivos");
	
	
		while ($row = mysql_fetch_assoc($lValues)) {	
			if(checkAnoFim($row["ID"],$i)){
				echo $row["DESCRICAO"];
			}
		}

		$db->disconnect();
		
			
	}
				
	function checkAnofim($id,$i){
		global $questionario;

		if($questionario->habilitacoes[$i]->anofim==$id)
			return true;
		else 
			return false;
	}
	
	function getGrau($i) {
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_graucursos");
	
		
		while ($row = mysql_fetch_assoc($lValues)) {	
			if(checkGrau($row["ID"],$i))
				echo $row["DESCRICAO"];
		}
		echo "</SELECT><br />\n";
		$db->disconnect();
				
	}	
				
	function checkGrau($id,$i){
		global $questionario;
		if($questionario->habilitacoes[$i]->grau==$id)
			return true;
		else 
			return false;
	}
	
	
	function getDistincao($i) {
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_distincaoValores");
	

		while ($row = mysql_fetch_assoc($lValues)) {
			if(checkDistincao($row["ID"],$i))
				echo $row["DESCRICAO"];
		}
		$db->disconnect();
	
	}
	
	function checkDistincao($id,$i){
		global $questionario;
		if($questionario->habilitacoes[$i]->distincao==$id)
			return true;
		else
			return false;
	}
	
	
	function getBolsa($i) {
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_TipoBolsas");

	
		while ($row = mysql_fetch_assoc($lValues)) {
			if(checkBolsa($row["ID"],$i))
				echo $row["DESCRICAO"];
		}
		$db->disconnect();
	
	}
	
	function checkBolsa($id,$i){
		global $questionario;
		if($questionario->habilitacoes[$i]->bolsa==$id)
			return true;
		else
			return false;
	}
	
	
	
	?>