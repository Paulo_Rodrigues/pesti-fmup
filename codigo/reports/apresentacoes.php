<?php
echo "<div id='content18' style='display: inline;'>";

echo "<p><fieldset class='normal'>\n";
echo "<legend>Apresentações em Conferências /<i>Conferences</i></legend>\n";


echo "<table  class='box-table-b'>
    					<tr>
    					<th>Âmbito<p><i>Type</i></p></th>
      					<th>Tipo<p><i>Organization</i></p></th>
      					<th>Data Início<p><i>Begin Date</i></p></th>
      					<th>Data Fim<p><i>End Date</i></p></th>
      					<th>Título<p><i>Title</i></p></th>
      					<th>Local (cidade,país)<p><i>Location (town,country)</i></p></th>
      					<th>Nº Participantes<p><i>Participant Nr.</i></p></th>
      					<th>Link<p><i>Link</i></p></th>
						</tr>";

foreach ($questionario->apresentacoesConferencias as $i => $value){
	echo "<tr>";
	echo "<td>";
	getAmbitoApresentacaoConf($i);
	echo "</td>";
	echo "<td>";
	getTipoApresentacaoConf($i);
	echo "</td>";
	echo "<td>".$questionario->apresentacoesConferencias[$i]->datainicio."</td>";
	echo "<td>".$questionario->apresentacoesConferencias[$i]->datafim."</td>";
	echo "<td>".$questionario->apresentacoesConferencias[$i]->titulo."</td>";
	echo "<td>".$questionario->apresentacoesConferencias[$i]->local."</td>";
	echo "<td>".$questionario->apresentacoesConferencias[$i]->participantes."/td>";
	echo "<td>".$questionario->apresentacoesConferencias[$i]->link."</td>";
	echo "</tr>";
}
echo "</table>";

echo "</fieldset>";




echo "<p><fieldset class='normal'>\n";
echo "<legend>Apresentações/<i>Presentations</i></legend>\n";
	
echo "<table  class='box-table-a'>
    					<tr><th></th>
    					<th>Internacional</th>
      					<th>Nacional</th>
						</tr>";
			    
/*echo "<tr><td>Número de trabalhos apresentados em congressos/simpósios/workshops, por convite<p><i>Number of presented works in congresses/symposia/workshops, by invitation</i></p></td><td>".$questionario->apresentacoes->iconfconv."</td>";
echo "<td>".$questionario->apresentacoes->nconfconv."</td></tr>";

echo "<tr><td>Número de trabalhos apresentados em congressos/simpósios/workshops, como participante<p><i>Number of presented works in congresses/symposia/workshops, as participant</i></p></td><td>".$questionario->apresentacoes->iconfpart."</td>";
echo "<td>".$questionario->apresentacoes->nconfpart."</td></tr>";
*/
echo "<tr><td>Número de seminários apresentados<p><i>Number of seminars presented</i></p></td><td>".$questionario->apresentacoes->isem."</td>";
echo "<td>".$questionario->apresentacoes->nsem."</td></tr>";



echo "</table>";

	echo "</fieldset>";
	
	echo "</div>";

	function getTipoApresentacaoConf($i) {
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_tipoApresConf");
	
		while ($row = mysql_fetch_assoc($lValues)) {
			if(checkTipoApresentacaoConf($row["ID"],$i))
				echo $row["DESCRICAO"];
			}

		$db->disconnect();
	
	}
	
	function checkTipoApresentacaoConf($id,$i){
		global $questionario;
		if($questionario->apresentacoesConferencias[$i]->tipo==$id)
			return true;
		else
			return false;
	}
	
	function getAmbitoApresentacaoConf($i) {
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_ambitoconf");
	
		while ($row = mysql_fetch_assoc($lValues)) {
			if(checkAmbApresentacaoConf($row["ID"],$i))
				echo $row["DESCRICAO"];
		}
		$db->disconnect();
	
	}
	
	function checkAmbApresentacaoConf($id,$i){
		global $questionario;
		if($questionario->apresentacoesConferencias[$i]->ambito==$id)
			return true;
		else
			return false;;
	}
		
	?>