<?php

echo "<div id='content12' style='display: inline;'>";


$outJNMAN="";
$outJANMAN="";
$outPRPNMAN="";
$outCPNMAN="";
$outOANMAN="";
$outSNMAN="";
$outLNMAN="";
$outCLNMAN="";
$outAMAN="";




$tcols="
    <tr>
		<th>TIPO<p><i>TYPE</i></p></th>
		<th>TÍTULO DA OBRA<p><i>WORK TITLE</i></p></th>
		<th>ISSN</th>
		<th>ISSN_LINKING</th>
		<th>ISSN_ELECTRONIC</th>
		<th>ISBN</th>
		<th>TITULO<p><i>TITLE</i></p></th>
		<th>AUTORES<p><i>AUTHORS</i></p></th>
		<th>VOLUME</th>
		<th>ISSUE</th>
		<th>AR</th>
		<th>COLECAO<p><i>COLECTION</i></p></th>
		<th>PRIMPAGINA<p><i>FIRSTPAGE</i></p></th>
		<th>ULTPAGINA<p><i>LASTPAGE</i></p></th>
		<th>IFACTOR</th>
		<th>ANO<p><i>YEAR</i></p></th>
		<th>CITACOES<p><i>CITATIONS</i></p></th>
		<th>NPATENTE<p><i>PATENT NR.</i></p></th>
		<th>DATAPATENTE<p><i>DATE PUBLICATION</i></p></th>
		<th>IPC</th>
		<th>AREA<p><i>AREA</i></p></th>
		<th>LINK<p><i>LINK</i></p></th>
		<th>ESTADO<p><i>ESTADO</i></p></th>
		</tr>";

$tcolsJ="
    <tr>
		<th>ISSN</th>
		<th>REVISTA<p><i>JOURNAL</i></p></th>
		<th>TÍTULO<p><i>TITLE</i></p></th>
		<th>AUTORES<p><i>AUTHORS</i></p></th>
		<th>VOLUME</th>
		<th>ISSUE</th>
		<th>PRIMEIRA PÁG.<p><i>FIRST PAGE</i></p></th>
		<th>ÚLTIMA PÁG.<p><i>LAST PAGE</i></p></th>
		<th>LINK<p><i>LINK</i></p></th>
		<th>ESTADO<p><i>ESTADO</i></p></th>
		</tr>";

$tcolsCONF="
    <tr>
		<th>ISBN</th>
		<th>TÍTULO DA OBRA<p><i>WORK TITLE</i></p></th>
		<th>EDITORES<p><i>EDITORS</i></p></th>
		<th>TÍTULO ARTIGO<p><i>ARTICLE TITLE</i></p></th>
		<th>AUTORES<p><i>AUTHORS</i></p></th>
		<th>PRIMEIRA PÁG.<p><i>FIRST PAGE</i></p></th>
		<th>ÚLTIMA PÁG.<p><i>LAST PAGE</i></p></th>
		<th>LINK<p><i>LINK</i></p></th>
		<th>ESTADO<p><i>ESTADO</i></p></th>
		</tr>";

$tcolsPRP="
    <tr>
		<th>ISSN</th>
		<th>TÍTULO DA OBRA<p><i>WORK TITLE</i></p></th>
		<th>EDITORES<p><i>EDITORS</i></p></th>
		<th>TÍTULO ARTIGO<p><i>ARTICLE TITLE</i></p></th>
		<th>AUTORES<p><i>AUTHORS</i></p></th>
		<th>PRIMEIRA PÁG.<p><i>FIRST PAGE</i></p></th>
		<th>ÚLTIMA PÁG.<p><i>LAST PAGE</i></p></th>
		<th>LINK<p><i>LINK</i></p></th>
		<th>ESTADO<p><i>ESTADO</i></p></th>
		</tr>";

$tcolsCLIV="
    <tr>
		<th>ISBN</th>
		<th>TÍTULO DA OBRA<p><i>WORK TITLE</i></p></th>
		<th>EDITORA<p><i>PUBLISHER</i></p></th>
		<th>EDITORES<p><i>EDITORS</i></p></th>			
		<th>TÍTULO ARTIGO<p><i>ARTICLE TITLE</i></p></th>
		<th>AUTORES<p><i>EDITORS</i></p></th>
		<th>PRIMEIRA PÁG.<p><i>FIRST PAGE</i></p></th>
		<th>ÚLTIMA PÁG.<p><i>LAST PAGE</i></p></th>
		<th>LINK<p><i>LINK</i></p></th>
		<th>ESTADO<p><i>ESTADO</i></p></th>
		</tr>";

$tcolsPAT="
    <tr>
		<th>Nº PATENTE<p><i>PATENT NR.</i></p></th>
		<th>IPC</th>
		<th>TÍTULO<p><i>TITLE</i></p></th>
		<th>AUTORES<p><i>AUTHORS</i></p></th>		
		<th>DATA de PUBLICAÇÃO<p><i>PUBLICATION DATE</i></p></th>
		<th>LINK<p><i>LINK</i></p></th>
		<th>ESTADO<p><i>ESTADO</i></p></th>
		</tr>";

$tcolsLIV="
    <tr>
		<th>ISBN</th>
		<th>TÍTULO<p><i>TITLE</i></p></th>
		<th>AUTORES<p><i>EDITORS</i></p></th>		
		<th>EDITOR<p><i>EDITOR</i></p></th>
		<th>EDITORA<p><i>PUBLISHER</i></p></th>
		<th>LINK<p><i>LINK</i></p></th>
		<th>ESTADO<p><i>ESTADO</i></p></th>
		</tr>";


		    
	foreach ($questionario->publicacoesMANUALNacional as $i => $value){
			
			$tipo=$questionario->publicacoesMANUALNacional[$i]->tipofmup;
			
			//echo $tipo;
			
			switch($tipo){
				case 'CPN': { // Opção de entrada
							$outCPNMAN=$outCPNMAN."<tr>";
						   	$outCPNMAN=$outCPNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->isbn."</td>";
							$outCPNMAN=$outCPNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->nomepublicacao."</td>";
							$outCPNMAN=$outCPNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->editor."</td>";
							$outCPNMAN=$outCPNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->titulo."</td>";
							$outCPNMAN=$outCPNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->autores."</td>";
							$outCPNMAN=$outCPNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->primpagina."</td>";
							$outCPNMAN=$outCPNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->ultpagina."</td>";
							$outCPNMAN=$outCPNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->link."</textarea></td>";
							$outCPNMAN=$outCPNMAN."<td>".getEstadoPublicacao($i)."</td>";
						   	/*$outCPNMAN=$outCPNMAN."<td><input class='inp-textAuto' type='text' name='pmtipo_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->tipo."'></td>";
							$outCPNMAN=$outCPNMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->issn."'></td>";			
						   	$outCPNMAN=$outCPNMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_linking_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->issn_linking."'></td>";
							$outCPNMAN=$outCPNMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_electronic_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->issn_electronic."'></td>";
							$outCPNMAN=$outCPNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->volume."'></td>";
							$outCPNMAN=$outCPNMAN."<td><input class='inp-textAuto' type='text' name='pmissue_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->issue."'></td>";
							$outCPNMAN=$outCPNMAN."<td><input class='inp-textAuto' type='text' name='pmar_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->ar."'></td>";
							$outCPNMAN=$outCPNMAN."<td><input class='inp-textAuto' type='text' name='pmcolecao_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->colecao."'></td>";
							$outCPNMAN=$outCPNMAN."<td><input class='inp-textAuto' type='text' name='pmifactor_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->ifactor."'></td>";
							$outCPNMAN=$outCPNMAN."<td><input class='inp-textAuto' type='text' name='pmano_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->ano."'></td>";
							$outCPNMAN=$outCPNMAN."<td><input class='inp-textAuto' type='text' name='pmcitacoes_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->citacoes."'></td>";
							$outCPNMAN=$outCPNMAN."<td><input class='inp-textAuto' type='text' name='pmnpatente_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->npatente."'></td>";
							$outCPNMAN=$outCPNMAN."<td><input class='inp-textAuto' type='text' name='pmdatapatente_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->datapatente."'></td>";
							$outCPNMAN=$outCPNMAN."<td><input class='inp-textAuto' type='text' name='pmipc_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->ipc."'></td>";
							$outCPNMAN=$outCPNMAN."<td><input class='inp-textAuto' type='text' name='pmarea_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->area."'></td>";			
							$outCPNMAN=$outCPNMAN."<td><input class='inp-textAuto' type='text' name='pmconftitle_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->conftitle."'></td>";
							$outCPNMAN=$outCPNMAN."<td><input class='inp-textAuto' type='text' name='pmlanguage_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->language."'></td>";	
							$outCPNMAN=$outCPNMAN."<td><input class='inp-textAuto' type='text' name='pmtipofmup_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->tipofmup."'></td>";
							*/$outCPNMAN=$outCPNMAN."</tr>";
				}
				break;
				case 'S': { // Opção de entrada
							$outSMAN=$outSMAN."<tr>";
						   	$outSMAN=$outSMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->isbn."</td>";
							$outSMAN=$outSMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->nomepublicacao."</td>";
							$outSMAN=$outSMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->editor."</td>";
							$outSMAN=$outSMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->titulo."</td>";
							$outSMAN=$outSMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->autores."</td>";
							$outSMAN=$outSMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->primpagina."</td>";
							$outSMAN=$outSMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->ultpagina."</td>";
							$outSMAN=$outSMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->link."</td>";
							$outSMAN=$outSMAN."<td>".getEstadoPublicacao($i)."</td>";
						   	/*$outSMAN=$outSMAN."<td><input class='inp-textAuto' type='text' name='pmtipo_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->tipo."'></td>";
							$outSMAN=$outSMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->issn."'></td>";			
						   	$outSMAN=$outSMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_linking_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->issn_linking."'></td>";
							$outSMAN=$outSMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_electronic_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->issn_electronic."'></td>";
							$outSMAN=$outSMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->volume."'></td>";
							$outSMAN=$outSMAN."<td><input class='inp-textAuto' type='text' name='pmissue_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->issue."'></td>";
							$outSMAN=$outSMAN."<td><input class='inp-textAuto' type='text' name='pmar_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->ar."'></td>";
							$outSMAN=$outSMAN."<td><input class='inp-textAuto' type='text' name='pmcolecao_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->colecao."'></td>";
							$outSMAN=$outSMAN."<td><input class='inp-textAuto' type='text' name='pmifactor_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->ifactor."'></td>";
							$outSMAN=$outSMAN."<td><input class='inp-textAuto' type='text' name='pmano_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->ano."'></td>";
							$outSMAN=$outSMAN."<td><input class='inp-textAuto' type='text' name='pmcitacoes_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->citacoes."'></td>";
							$outSMAN=$outSMAN."<td><input class='inp-textAuto' type='text' name='pmnpatente_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->npatente."'></td>";
							$outSMAN=$outSMAN."<td><input class='inp-textAuto' type='text' name='pmdatapatente_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->datapatente."'></td>";
							$outSMAN=$outSMAN."<td><input class='inp-textAuto' type='text' name='pmipc_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->ipc."'></td>";
							$outSMAN=$outSMAN."<td><input class='inp-textAuto' type='text' name='pmarea_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->area."'></td>";			
							$outSMAN=$outSMAN."<td><input class='inp-textAuto' type='text' name='pmconftitle_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->conftitle."'></td>";
							$outSMAN=$outSMAN."<td><input class='inp-textAuto' type='text' name='pmlanguage_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->language."'></td>";	
							$outSMAN=$outSMAN."<td><input class='inp-textAuto' type='text' name='pmtipofmup_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->tipofmup."'></td>";
							*/$outSMAN=$outSMAN."</tr>";
				}
				break;
				case 'JN': { // Opção de entrada
							$outJNMAN=$outJNMAN."<tr>";
						   	//$outJNMAN=$outJNMAN."<td><input class='inp-textAuto' type='text' name='pmtipo_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->tipo."'></td>";
							$outJNMAN=$outJNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->issn."</td>";			
						   	$outJNMAN=$outJNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->nomepublicacao."</td>";
							//$outJNMAN=$outJNMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_linking_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->issn_linking."'></td>";
							//$outJNMAN=$outJNMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_electronic_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->issn_electronic."'></td>";
							//$outJNMAN=$outJNMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmisbn_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->isbn."'></td>";
							$outJNMAN=$outJNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->titulo."</td>";
							$outJNMAN=$outJNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->autores."</td>";
							$outJNMAN=$outJNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->volume."</td>";
							$outJNMAN=$outJNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->issue."</td>";
							//$outJNMAN=$outJNMAN."<td><input class='inp-textAuto' type='text' name='pmar_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->ar."'></td>";
							//$outJNMAN=$outJNMAN."<td><input class='inp-textAuto' type='text' name='pmcolecao_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->colecao."'></td>";
							$outJNMAN=$outJNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->primpagina."</td>";
							$outJNMAN=$outJNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->ultpagina."</td>";
							$outJNMAN=$outJNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->link."</td>";
							$outJNMAN=$outJNMAN."<td>".getEstadoPublicacao($i)."</td>";
							//$outJNMAN=$outJNMAN."<td><input class='inp-textAuto' type='text' name='pmifactor_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->ifactor."'></td>";
							//$outJNMAN=$outJNMAN."<td><input class='inp-textAuto' type='text' name='pmano_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->ano."'></td>";
							//$outJNMAN=$outJNMAN."<td><input class='inp-textAuto' type='text' name='pmcitacoes_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->citacoes."'></td>";
							//$outJNMAN=$outJNMAN."<td><input class='inp-textAuto' type='text' name='pmnpatente_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->npatente."'></td>";
							//$outJNMAN=$outJNMAN."<td><input class='inp-textAuto' type='text' name='pmdatapatente_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->datapatente."'></td>";
							//$outJNMAN=$outJNMAN."<td><input class='inp-textAuto' type='text' name='pmipc_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->ipc."'></td>";
							//$outJNMAN=$outJNMAN."<td><input class='inp-textAuto' type='text' name='pmarea_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->area."'></td>";			
							//$outJNMAN=$outJNMAN."<td><input class='inp-textAuto' type='text' name='pmconftitle_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->conftitle."'></td>";
							//$outJNMAN=$outJNMAN."<td><input class='inp-textAuto' type='text' name='pmlanguage_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->language."'></td>";	
							//$outJNMAN=$outJNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->editor."'></td>";
							//$outJNMAN=$outJNMAN."<td><input class='inp-textAuto' type='text' name='pmtipofmup_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->tipofmup."'></td>";
							$outJNMAN=$outJNMAN."</tr>";
				}
				break;
				case 'PRPN': { // Opção de entrada
							$outPRPNMAN=$outPRPNMAN."<tr>";
						   	$outPRPNMAN=$outPRPNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->issn."</td>";
							$outPRPNMAN=$outPRPNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->nomepublicacao."</td>";
							//$outPRPNMAN=$outPRPNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->editor."'></td>";
							$outPRPNMAN=$outPRPNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->titulo."</td>";
							$outPRPNMAN=$outPRPNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->autores."</td>";
							$outPRPNMAN=$outPRPNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->volume."</td>";
							$outPRPNMAN=$outPRPNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->issue."</td>";	
							$outPRPNMAN=$outPRPNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->primpagina."</td>";
							$outPRPNMAN=$outPRPNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->ultpagina."</td>";
							$outPRPNMAN=$outPRPNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->link."</td>";
							$outPRPNMAN=$outPRPNMAN."<td>".getEstadoPublicacao($i)."</td>";
						   	/*$outPRPNMAN=$outPRPNMAN."<td><input class='inp-textAuto' type='text' name='pmtipo_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->tipo."'></td>";
							$outPRPNMAN=$outPRPNMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->issn."'></td>";			
						   	$outPRPNMAN=$outPRPNMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_linking_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->issn_linking."'></td>";
							$outPRPNMAN=$outPRPNMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_electronic_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->issn_electronic."'></td>";
							$outPRPNMAN=$outPRPNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->volume."'></td>";
							$outPRPNMAN=$outPRPNMAN."<td><input class='inp-textAuto' type='text' name='pmar_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->ar."'></td>";
							$outPRPNMAN=$outPRPNMAN."<td><input class='inp-textAuto' type='text' name='pmcolecao_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->colecao."'></td>";
							$outPRPNMAN=$outPRPNMAN."<td><input class='inp-textAuto' type='text' name='pmifactor_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->ifactor."'></td>";
							$outPRPNMAN=$outPRPNMAN."<td><input class='inp-textAuto' type='text' name='pmano_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->ano."'></td>";
							$outPRPNMAN=$outPRPNMAN."<td><input class='inp-textAuto' type='text' name='pmcitacoes_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->citacoes."'></td>";
							$outPRPNMAN=$outPRPNMAN."<td><input class='inp-textAuto' type='text' name='pmnpatente_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->npatente."'></td>";
							$outPRPNMAN=$outPRPNMAN."<td><input class='inp-textAuto' type='text' name='pmdatapatente_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->datapatente."'></td>";
							$outPRPNMAN=$outPRPNMAN."<td><input class='inp-textAuto' type='text' name='pmipc_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->ipc."'></td>";
							$outPRPNMAN=$outPRPNMAN."<td><input class='inp-textAuto' type='text' name='pmarea_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->area."'></td>";			
							$outPRPNMAN=$outPRPNMAN."<td><input class='inp-textAuto' type='text' name='pmconftitle_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->conftitle."'></td>";
							$outPRPNMAN=$outPRPNMAN."<td><input class='inp-textAuto' type='text' name='pmlanguage_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->language."'></td>";	
							$outPRPNMAN=$outPRPNMAN."<td><input class='inp-textAuto' type='text' name='pmtipofmup_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->tipofmup."'></td>";
							*/$outPRPNMAN=$outPRPNMAN."</tr>";
				}
				break;
				case 'JAN': { // Opção de entrada
							$outJANMAN=$outJANMAN."<tr>";
						   	$outJANMAN=$outJANMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->issn."</td>";
													
							
							$outJANMAN=$outJANMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->nomepublicacao."</td>";
							$outJANMAN=$outJANMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->titulo."</td>";
							
							$outJANMAN=$outJANMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->autores."</td>";
							$outJANMAN=$outJANMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->volume."</td>";
							$outJANMAN=$outJANMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->issue."</td>";
							$outJANMAN=$outJANMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->primpagina."</td>";
							$outJANMAN=$outJANMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->ultpagina."</td>";
							$outJANMAN=$outJANMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->link."</td>";
							$outJANMAN=$outJANMAN."<td>".getEstadoPublicacao($i)."</td>";
						   	/*$outJANMAN=$outJANMAN."<td><input class='inp-textAuto' type='text' name='pmtipo_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->tipo."'></td>";
							$outJANMAN=$outJANMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->issn."'></td>";			
						   	$outJANMAN=$outJANMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_linking_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->issn_linking."'></td>";
							$outJANMAN=$outJANMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_electronic_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->issn_electronic."'></td>";
							$outJANMAN=$outJANMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->volume."'></td>";
							$outJANMAN=$outJANMAN."<td><input class='inp-textAuto' type='text' name='pmar_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->ar."'></td>";
							$outJANMAN=$outJANMAN."<td><input class='inp-textAuto' type='text' name='pmcolecao_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->colecao."'></td>";
							$outJANMAN=$outJANMAN."<td><input class='inp-textAuto' type='text' name='pmifactor_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->ifactor."'></td>";
							$outJANMAN=$outJANMAN."<td><input class='inp-textAuto' type='text' name='pmano_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->ano."'></td>";
							$outJANMAN=$outJANMAN."<td><input class='inp-textAuto' type='text' name='pmcitacoes_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->citacoes."'></td>";
							$outJANMAN=$outJANMAN."<td><input class='inp-textAuto' type='text' name='pmnpatente_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->npatente."'></td>";
							$outJANMAN=$outJANMAN."<td><input class='inp-textAuto' type='text' name='pmdatapatente_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->datapatente."'></td>";
							$outJANMAN=$outJANMAN."<td><input class='inp-textAuto' type='text' name='pmipc_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->ipc."'></td>";
							$outJANMAN=$outJANMAN."<td><input class='inp-textAuto' type='text' name='pmarea_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->area."'></td>";			
							$outJANMAN=$outJANMAN."<td><input class='inp-textAuto' type='text' name='pmconftitle_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->conftitle."'></td>";
							$outJANMAN=$outJANMAN."<td><input class='inp-textAuto' type='text' name='pmlanguage_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->language."'></td>";	
							$outJANMAN=$outJANMAN."<td><input class='inp-textAuto' type='text' name='pmtipofmup_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->tipofmup."'></td>";
							*/$outJANMAN=$outJANMAN."</tr>";
				}
				break;
				case 'OAN': { // Opção de entrada
							$outOANMAN=$outOANMAN."<tr>";
						   	$outOANMAN=$outOANMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->isbn."</td>";
							$outOANMAN=$outOANMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->nomepublicacao."</td>";
							$outOANMAN=$outOANMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->editor."</td>";
							$outOANMAN=$outOANMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->titulo."</td>";
							$outOANMAN=$outOANMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->autores."</td>";
							$outOANMAN=$outOANMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->primpagina."</td>";
							$outOANMAN=$outOANMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->ultpagina."</td>";
							$outOANMAN=$outOANMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->link."</td>";
							$outOANMAN=$outOANMAN."<td>".getEstadoPublicacao($i)."</td>";
						   	/*$outOANMAN=$outOANMAN."<td><input class='inp-textAuto' type='text' name='pmtipo_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->tipo."'></td>";
							$outOANMAN=$outOANMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->issn."'></td>";			
						   	$outOANMAN=$outOANMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_linking_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->issn_linking."'></td>";
							$outOANMAN=$outOANMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_electronic_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->issn_electronic."'></td>";
							$outOANMAN=$outOANMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->volume."'></td>";
							$outOANMAN=$outOANMAN."<td><input class='inp-textAuto' type='text' name='pmissue_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->issue."'></td>";
							$outOANMAN=$outOANMAN."<td><input class='inp-textAuto' type='text' name='pmar_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->ar."'></td>";
							$outOANMAN=$outOANMAN."<td><input class='inp-textAuto' type='text' name='pmcolecao_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->colecao."'></td>";
							$outOANMAN=$outOANMAN."<td><input class='inp-textAuto' type='text' name='pmifactor_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->ifactor."'></td>";
							$outOANMAN=$outOANMAN."<td><input class='inp-textAuto' type='text' name='pmano_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->ano."'></td>";
							$outOANMAN=$outOANMAN."<td><input class='inp-textAuto' type='text' name='pmcitacoes_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->citacoes."'></td>";
							$outOANMAN=$outOANMAN."<td><input class='inp-textAuto' type='text' name='pmnpatente_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->npatente."'></td>";
							$outOANMAN=$outOANMAN."<td><input class='inp-textAuto' type='text' name='pmdatapatente_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->datapatente."'></td>";
							$outOANMAN=$outOANMAN."<td><input class='inp-textAuto' type='text' name='pmipc_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->ipc."'></td>";
							$outOANMAN=$outOANMAN."<td><input class='inp-textAuto' type='text' name='pmarea_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->area."'></td>";			
							$outOANMAN=$outOANMAN."<td><input class='inp-textAuto' type='text' name='pmconftitle_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->conftitle."'></td>";
							$outOANMAN=$outOANMAN."<td><input class='inp-textAuto' type='text' name='pmlanguage_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->language."'></td>";	
							$outOANMAN=$outOANMAN."<td><input class='inp-textAuto' type='text' name='pmtipofmup_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->tipofmup."'></td>";
							*/$outOANMAN=$outOANMAN."</tr>";
				}
				break;
				case 'SN': { // Opção de entrada
							$outSNMAN=$outSNMAN."<tr>";
						   	$outSNMAN=$outSNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->isbn."</td>";
							$outSNMAN=$outSNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->nomepublicacao."</td>";
							$outSNMAN=$outSNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->editor."</td>";
							$outSNMAN=$outSNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->titulo."</td>";
							$outSNMAN=$outSNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->autores."</td>";
							$outSNMAN=$outSNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->primpagina."</td>";
							$outSNMAN=$outSNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->ultpagina."</td>";
							$outSNMAN=$outSNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->link."</td>";
							$outSNMAN=$outSNMAN."<td>".getEstadoPublicacao($i)."</td>";
						   	/*$outSNMAN=$outSNMAN."<td><input class='inp-textAuto' type='text' name='pmtipo_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->tipo."'></td>";
							$outSNMAN=$outSNMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->issn."'></td>";			
						   	$outSNMAN=$outSNMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_linking_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->issn_linking."'></td>";
							$outSNMAN=$outSNMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_electronic_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->issn_electronic."'></td>";
							$outSNMAN=$outSNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->volume."'></td>";
							$outSNMAN=$outSNMAN."<td><input class='inp-textAuto' type='text' name='pmissue_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->issue."'></td>";
							$outSNMAN=$outSNMAN."<td><input class='inp-textAuto' type='text' name='pmar_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->ar."'></td>";
							$outSNMAN=$outSNMAN."<td><input class='inp-textAuto' type='text' name='pmcolecao_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->colecao."'></td>";
							$outSNMAN=$outSNMAN."<td><input class='inp-textAuto' type='text' name='pmifactor_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->ifactor."'></td>";
							$outSNMAN=$outSNMAN."<td><input class='inp-textAuto' type='text' name='pmano_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->ano."'></td>";
							$outSNMAN=$outSNMAN."<td><input class='inp-textAuto' type='text' name='pmcitacoes_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->citacoes."'></td>";
							$outSNMAN=$outSNMAN."<td><input class='inp-textAuto' type='text' name='pmnpatente_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->npatente."'></td>";
							$outSNMAN=$outSNMAN."<td><input class='inp-textAuto' type='text' name='pmdatapatente_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->datapatente."'></td>";
							$outSNMAN=$outSNMAN."<td><input class='inp-textAuto' type='text' name='pmipc_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->ipc."'></td>";
							$outSNMAN=$outSNMAN."<td><input class='inp-textAuto' type='text' name='pmarea_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->area."'></td>";			
							$outSNMAN=$outSNMAN."<td><input class='inp-textAuto' type='text' name='pmconftitle_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->conftitle."'></td>";
							$outSNMAN=$outSNMAN."<td><input class='inp-textAuto' type='text' name='pmlanguage_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->language."'></td>";	
							$outSNMAN=$outSNMAN."<td><input class='inp-textAuto' type='text' name='pmtipofmup_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->tipofmup."'></td>";
							*/$outSNMAN=$outSNMAN."</tr>";
				}
				break;
				case 'LN': { // Opção de entrada
							$outLNMAN=$outLNMAN."<tr>";
							$outLNMAN=$outLNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->isbn."</td>";
							$outLNMAN=$outLNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->titulo."</td>";
							$outLNMAN=$outLNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->autores."</td>";
							$outLNMAN=$outLNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->editor."</td>";
							$outLNMAN=$outLNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->editora."</td>";
							$outLNMAN=$outLNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->link."</td>";
							$outLNMAN=$outLNMAN."<td>".getEstadoPublicacao($i)."</td>";
							/*$outLNMAN=$outLNMAN."<td><input class='inp-textAuto' type='text' name='pmtipo_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->tipo."</td>";
						   	$outLNMAN=$outLNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->nomepublicacao."'></td>";
							$outLNMAN=$outLNMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->issn."'></td>";			
							$outLNMAN=$outLNMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_linking_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->issn_linking."'></td>";
							$outLNMAN=$outLNMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_electronic_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->issn_electronic."'></td>";
							$outLNMAN=$outLNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->volume."'></td>";
							$outLNMAN=$outLNMAN."<td><input class='inp-textAuto' type='text' name='pmissue_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->issue."'></td>";
							$outLNMAN=$outLNMAN."<td><input class='inp-textAuto' type='text' name='pmar_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->ar."'></td>";
							$outLNMAN=$outLNMAN."<td><input class='inp-textAuto' type='text' name='pmcolecao_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->colecao."'></td>";
							$outLNMAN=$outLNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->primpagina."'></td>";
							$outLNMAN=$outLNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->ultpagina."'></td>";
							$outLNMAN=$outLNMAN."<td><input class='inp-textAuto' type='text' name='pmifactor_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->ifactor."'></td>";
							$outLNMAN=$outLNMAN."<td><input class='inp-textAuto' type='text' name='pmano_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->ano."'></td>";
							$outLNMAN=$outLNMAN."<td><input class='inp-textAuto' type='text' name='pmcitacoes_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->citacoes."'></td>";
							$outLNMAN=$outLNMAN."<td><input class='inp-textAuto' type='text' name='pmnpatente_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->npatente."'></td>";
							$outLNMAN=$outLNMAN."<td><input class='inp-textAuto' type='text' name='pmdatapatente_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->datapatente."'></td>";
							$outLNMAN=$outLNMAN."<td><input class='inp-textAuto' type='text' name='pmipc_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->ipc."'></td>";
							$outLNMAN=$outLNMAN."<td><input class='inp-textAuto' type='text' name='pmarea_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->area."'></td>";			
							$outLNMAN=$outLNMAN."<td><input class='inp-textAuto' type='text' name='pmconftitle_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->conftitle."'></td>";
							$outLNMAN=$outLNMAN."<td><input class='inp-textAuto' type='text' name='pmlanguage_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->language."'></td>";
							$outLNMAN=$outLNMAN."<td><input class='inp-textAuto' type='text' name='pmtipofmup_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->tipofmup."'></td>";								
							*/$outLNMAN=$outLNMAN."</tr>";
				}
				break;
				case 'CLN': { // Opção de entrada
							$outCLNMAN=$outCLNMAN."<tr>";
						   	$outCLNMAN=$outCLNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->isbn."</td>";
						   	$outCLNMAN=$outCLNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->nomepublicacao."</td>";
						  	$outCLNMAN=$outCLNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->editora."</td>";
							$outCLNMAN=$outCLNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->editor."</td>";
							$outCLNMAN=$outCLNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->titulo."</td>";
							$outCLNMAN=$outCLNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->autores."</td>";							
							$outCLNMAN=$outCLNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->primpagina."</td>";
							$outCLNMAN=$outCLNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->ultpagina."</td>";
							$outCLNMAN=$outCLNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->link."</td>";
							$outCLNMAN=$outCLNMAN."<td>".getEstadoPublicacao($i)."</td>";
						   	
						   /*	$outCLNMAN=$outCLNMAN."<td><input class='inp-textAuto' type='text' name='pmtipo_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->tipo."'></td>";
							$outCLNMAN=$outCLNMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->issn."'></td>";			
							$outCLNMAN=$outCLNMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_linking_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->issn_linking."'></td>";
							$outCLNMAN=$outCLNMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_electronic_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->issn_electronic."'></td>";
							$outCLNMAN=$outCLNMAN."<td>".$questionario->publicacoesMANUALNacional[$i]->volume."'></td>";
							$outCLNMAN=$outCLNMAN."<td><input class='inp-textAuto' type='text' name='pmissue_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->issue."'></td>";
							$outCLNMAN=$outCLNMAN."<td><input class='inp-textAuto' type='text' name='pmar_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->ar."'></td>";
							$outCLNMAN=$outCLNMAN."<td><input class='inp-textAuto' type='text' name='pmcolecao_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->colecao."'></td>";
							$outCLNMAN=$outCLNMAN."<td><input class='inp-textAuto' type='text' name='pmifactor_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->ifactor."'></td>";
							$outCLNMAN=$outCLNMAN."<td><input class='inp-textAuto' type='text' name='pmano_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->ano."'></td>";
							$outCLNMAN=$outCLNMAN."<td><input class='inp-textAuto' type='text' name='pmcitacoes_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->citacoes."'></td>";
							$outCLNMAN=$outCLNMAN."<td><input class='inp-textAuto' type='text' name='pmnpatente_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->npatente."'></td>";
							$outCLNMAN=$outCLNMAN."<td><input class='inp-textAuto' type='text' name='pmdatapatente_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->datapatente."'></td>";
							$outCLNMAN=$outCLNMAN."<td><input class='inp-textAuto' type='text' name='pmipc_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->ipc."'></td>";
							$outCLNMAN=$outCLNMAN."<td><input class='inp-textAuto' type='text' name='pmarea_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->area."'></td>";			
							$outCLNMAN=$outCLNMAN."<td><input class='inp-textAuto' type='text' name='pmconftitle_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->conftitle."'></td>";
							$outCLNMAN=$outCLNMAN."<td><input class='inp-textAuto' type='text' name='pmlanguage_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->language."'></td>";
							$outCLNMAN=$outCLNMAN."<td><input class='inp-textAuto' type='text' name='pmtipofmup_".$i."' value='".$questionario->publicacoesMANUALNacional[$i]->tipofmup."'></td>";								
							*/$outCLNMAN=$outCLNMAN."</tr>";
				}
				break;
				default:{ //echo "DEFAULT";
			       //$outREST=produzTable($outREST,$i);
			    }
				break;
			}
	}
	
	$arg1="";
	$arg2="";
	
	
	

	echo "<p><fieldset class='normal'>\n";
	echo "<legend>Publicações Manuais Não Inglês /<i>Non English Publications - by hand</i></legend>\n";
	

	echo"<table class='box-table-b'><caption >ARTIGOS<p><i>ARTICLES</i></p></caption>";echo $tcolsJ;echo $outJNMAN; echo "</table>";
	echo"<table class='box-table-b'><caption >PEER REVIEW PROCEEDINGS</caption>";echo $tcolsJ;echo $outPRPNMAN;echo "</table>";
	echo"<table class='box-table-b'><caption >SUMÁRIOS<p><i>JOURNAL ABSTRACTS</i></p></caption>";echo $tcolsJ;echo $outJANMAN;echo "</table>";

	echo"<table class='box-table-b'><caption >LIVROS<p><i>BOOKS</i></p></caption>";echo $tcolsLIV;echo $outLNMAN;echo "</table>";
	echo"<table class='box-table-b'><caption >CAPITULOS DE LIVROS<p><i>BOOK CHAPTERS</i></p></caption>";echo $tcolsCLIV;echo $outCLNMAN;echo "</table>";
	
	echo"<table class='box-table-b'><caption >CONFERENCE PROCEEDINGS</caption>";echo $tcolsCONF;echo $outCPNMAN;echo "</table>";
	echo"<table  class='box-table-b'><caption >OUTROS SUMÁRIOS<br>OTHER ABSTRACTS</caption>";echo $tcolsCONF;echo $outOANMAN;echo "</table>";
	//echo"<table id='box-table-b'><caption >SUMÁRIOS<p><i>ABSTRACTS CONFERENCE PROCEEDINGS</i></p></caption>";echo $tcolsCONF;echo $outSMAN;echo getButton("S",38);echo "</table>";
	//echo"<table id='box-table-b'><caption >AGRADECIMENTOS<p><i>ACKNOWLEDGEMENTS</i></p></caption>";echo $tcolsJ;echo $outAMAN;echo getButton("A",40);echo "</table>";
	

	


    echo "<input type='hidden' name='apagaRegPubManualNacional'>";
    echo "</fieldset>";
    echo "</div";

	
	function getButton($msg,$id){
		
		return "<tr><td><input type='image' src='../images/icon_new_s.png' name='navOption' value='".$msg."' onclick='if(formSubmited==0){return false;};document.questionario.operacao.value=".$id.";'></td></tr>";
		
	}
	
	
	function getEstadoPublicacao($i) {
		global $questionario;
		$db = new Database();
		$lValues =$db->getLookupValues("lista_estadoPublicacoes");
		$texto="";
		while ($row = mysql_fetch_assoc($lValues)) {
			if(checkEstadoPublicacao($row["ID"],$i))
				$texto=$row["DESCRICAO"];
		}
		
		$db->disconnect();
		return $texto;
	
	}
	
	function checkEstadoPublicacao($id,$i){
		global $questionario;
		
		if($questionario->publicacoesMANUALNacional[$i]->estado==$id)
			return true;
		else
			return false;
	}
	
	
	
	
	
	

?>