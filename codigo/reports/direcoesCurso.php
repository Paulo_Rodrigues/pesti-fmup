<?php
echo "<div id='content7' style='display: inline;'>";
echo "<p><fieldset class='normal'>\n";
echo "<legend>Direções de Curso <i>Course Director </i></legend>\n";

echo "<table  class='box-table-b'>
    	
      					<th>Data de Inicio<p><i>Start Date</i></p></th>
      					<th>Data de Fim<p><i>End Date</i></p></th>
      					<th>Curso<p><i>Course</i></p></th>
      					<th>Grau<p><i>Degree</i></p></th>
						<th>Cargos<p><i>Position</i></p></th></tr>";
			    
    foreach ($questionario->dirCursos as $i => $value){
			echo "<tr>";
			echo "<td>".$questionario->dirCursos[$i]->datainicio."</td>";
			echo "<td>".$questionario->dirCursos[$i]->datafim."</td>";
			echo "<td>".$questionario->dirCursos[$i]->nome."</td>";
			echo "<td>";
			getGrauDirCursos($i);	
	    	echo "</td>";
	    	echo "<td>";
	    	getCargoDirCursos($i);
	    	echo "</td>";
	    	echo "</tr>";	    	
    } 

    echo "</table>";
	echo "</fieldset>";
	echo "</div>";
	
	
	function getGrauDirCursos($i) {
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_graucursos");

		while ($row = mysql_fetch_assoc($lValues)) {	
			if(checkGrauDirCursos($row["ID"],$i))
				echo $row["DESCRICAO"];
		}

		$db->disconnect();
				
	}	
				
	function checkGrauDirCursos($id,$i){
		global $questionario;
		if($questionario->dirCursos[$i]->grau==$id)
			return true;
		else 
			return false;
	}
	
	function getCargoDirCursos($i) {
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_cargosDirecaoCursos");
	
		while ($row = mysql_fetch_assoc($lValues)) {
			if(checkGrauDirCursos($row["ID"],$i))
				echo $row["DESCRICAO"];
		}
	
		$db->disconnect();
	
	}
	
	function checkCargoDirCursos($id,$i){
		global $questionario;
		if($questionario->dirCursos[$i]->cargo==$id)
			return true;
		else
			return false;
	}
		
	?>