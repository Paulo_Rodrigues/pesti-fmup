<?php
echo "<div id='content7' style='display: inline;'>";
echo "<p><fieldset class='normal'>\n";
echo "<legend>Missões <i>Missions </i></legend>\n";

echo "<table  class='box-table-b'>
    					<tr>
      					<th>Data de Inicio<p><i>Start Date</i></p></th>
      					<th>Data de Fim<p><i>End Date</i></p></th>
      					<th>Motivacao<p><i>Motivation</i></p></th>
      					<th>Instituição de Acolhimento<p><i>Hosting Institution</i></p></th>
						<th>Pais<p><i>Country</i></p></th>
						<th>Âmbito de Tese ?<p><i>Context of thesis ?</i></p></th></tr>";
			    
    foreach ($questionario->missoes as $i => $value){
			echo "<tr>";
		   
			echo "<td>".$questionario->missoes[$i]->datainicio."</td>";
			echo "<td>".$questionario->missoes[$i]->datafim."</td>";
			echo "<td>".$questionario->missoes[$i]->motivacao."</td>";
			echo "<td>".$questionario->missoes[$i]->instituicao."</td>";
			getPaisesMissoes($i);
			echo "<td>".checkAmbTese($i)."</td>";
	    	echo "</tr>";	    	
    } 

    echo "</table>";

	echo "</fieldset>";
	
	
	echo "</div>";
	
	function getPaisesMissoes($i){
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_paises");
	

		while ($row = mysql_fetch_assoc($lValues)) {
			if(checkPaisMissoes($row["ID"],$i))
				echo "<td>".$row["DESCRICAO"]."</td>";
		}

		$db->disconnect();
	
	}
	
	function checkPaisMissoes($id,$i){
		global $questionario;
		if($questionario->missoes[$i]->pais==$id)
			return true;
		else
			return false;
	}
		
	function checkAmbTese($i){
		global $questionario;
		if($questionario->missoes[$i]->ambtese==1)
			return "Sim";
		else
			return "Não";
	}
	?>