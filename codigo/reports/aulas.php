<?php
echo "<div id='content22' style='display: inline;'>";
echo "<fieldset class='normal'>\n";
echo "<legend>Aulas/<i>Lectures</i></legend>\n";
			
			
			
echo "<table  class='box-table-a'>
    					<tr>
    					<th></th>
						<th>Pós-Graduação</th>
    					<th>Mestrado Integrado</th>
    					<th>1º Ciclo</th>
      					<th>2º Ciclo</th>
      					<th>3º Ciclo</th>
						</tr>";
			    
echo "<tr><td>Número de Regências em Unidades Curriculares <b> diferentes </b> (total FMUP + externas):<p><i>Number of Regências in <b> different </b> Curricular Units (total FMUP + external): </i></p></td>";
echo "<td>".$questionario->aulas->regpg."</td>";
echo "<td>".$questionario->aulas->regmi."</td>";
echo "<td>".$questionario->aulas->regpciclo."</td>";
echo "<td>".$questionario->aulas->regsciclo."</td>";
echo "<td>".$questionario->aulas->regtciclo."</td></tr>";


echo "<tr><td>Número de Unidades Curriculares <b> diferentes </b> em que lecionou aulas teóricas (inc. Seminários), FMUP+externas:<p><i>Theoretical lectures (inc. seminars), FMUP+external:</i></p></td>";
echo "<td>".$questionario->aulas->tpg."</td>";
echo "<td>".$questionario->aulas->tmi."</td>";
echo "<td>".$questionario->aulas->tpciclo."</td>";
echo "<td>".$questionario->aulas->tsciclo."</td>";
echo "<td>".$questionario->aulas->ttciclo."</td></tr>";


echo "<tr><td>Número de Unidades Curriculares <b> diferentes </b> em que lecionou aulas não-teóricas, FMUP+externas:<p><i>Non-theoretical lectures (inc. seminars), FMUP+external:</i></p></td>";
echo "<td>".$questionario->aulas->ppg."</td>";
echo "<td>".$questionario->aulas->pmi."</td>";
echo "<td>".$questionario->aulas->ppciclo."</td>";
echo "<td>".$questionario->aulas->psciclo."</td>";
echo "<td>".$questionario->aulas->ptciclo."</td></tr>";


echo "</table>";

echo "<legend>Consultas,Cirurgias<br/><i>Appointments,Surgeries</i></legend>\n\n\n";

echo "<table  class='box-table-a'>";
echo "<tr><td>Número de Consultas Médicas:<p><i>Number of Medical Appointments:</i></p></td>";
echo "<td>".$questionario->aulas->ncm."</td></tr>";
echo "<tr><td>Número de Cirurgias:<p><i>Number of Surgeries:</i></p></td>";
echo "<td>".$questionario->aulas->nc."</tr>";
echo "<tr><td>Número de Exames Anátomo-patológicos:<p><i>:</i></p></td>";
echo "<td>".$questionario->aulas->neap."</td></tr>";
echo "<tr><td>Número de Exames Imagiológicos:<p><i></i></p></td>";
echo "<td>".$questionario->aulas->nei."</td></tr>";
echo "<tr><td>Número de Exames Periciais:<p><i></i></p></td>";
echo "<td>".$questionario->aulas->nep."</td></tr>";
echo "<tr><td>Número de Consultadoria Médica:<p><i></i></p></td>";
echo "<td>".$questionario->aulas->ncsm."</td></tr>";
echo "<tr><td>Número de Outros atos médicos:<p><i></i></p></td>";
echo "<td>".$questionario->aulas->noutros."</td></tr>";

echo "</table>";

	echo "</fieldset>";
	echo "</div>";

		
	?>