<?php

echo "<div id='content16' style='display: inline;'>";
echo "<p><fieldset class='normal'>\n";
			echo "<legend>Projetos / Ensaios Clínicos 2013 <i>Research Projects / Clinical Trials 2013 </i></legend>\n";
		
			
			
echo "
    <table  class='box-table-b'>
  <tbody>
    <!-- Results table headers -->
    <tr>

      <th>Tipo/Entidade<p><i>Type/Entity</i></p></th>
      <th>Entidade Financiadora<p><i>Sponsor</i></p></th>
      <th>Instituição de Acolhimento<p><i>Host Institution</i></p></th>
      <th>Montante  total solicitado<p><i>Total amount requested</i></p></th>
      <th>Montante  total aprovado<p><i>Total amount approved</i></p></th>
      <th>Montante  atribuído à FMUP<p><i>Total amount attibuted to FMUP</i></p></th>
      <th>É Investigador Responsável?<p><i>Are you Principal Investigator?</i></p></th>
      <th>Código/Referência<p><i>Code/Reference</i></p></th>
      <th>Título<p><i>Title</i></p></th>
      <th>Data Início<p><i>Start Date</i></p></th>
      <th>Data Fim<p><i>End Date</i></p></th>
            <th>Link<p><i>Link</i></p></th>
            <th>Estado<p><i>Status</i></p></th>

    </tr>";


    foreach ($questionario->projectos as $i => $value){
		echo "<tr>";
	echo "<td>";
		getTipoEntidade($i);
		echo "<td>".$questionario->projectos[$i]->entidadefinanciadora."</td>";
		echo "<td>".$questionario->projectos[$i]->acolhimento."</td>";
		echo "<td>".$questionario->projectos[$i]->montante." €</td>";
		echo "<td>".$questionario->projectos[$i]->montantea." €</td>";
		echo "<td>".$questionario->projectos[$i]->montantefmup." €</td>";
		echo "<td>".checkInv($i)."</td>";
		echo "<td>".$questionario->projectos[$i]->codigo."</td>";
		echo "<td>".$questionario->projectos[$i]->titulo."</td>";
		echo "<td>".$questionario->projectos[$i]->datainicio."</td>";
		echo "<td>".$questionario->projectos[$i]->datafim."</td>";	
		echo "<td>".$questionario->projectos[$i]->link."</td>";
		echo "<td>".getEstadoProjeto($i)."</td>";
		
		echo "</tr>";  	
		
		
    }

    echo "</tbody></table>";

	echo "</fieldset>";
	
	echo "<fieldset class='normal'>\n";
	echo "<legend>Número de projetos arbitrados por si/<i>Number of projects refereed by you</i></legend><br/>\n";

	echo "<table class='box-table-a'><tr><th colspan='2'>Número de projetos arbitrados por si<p><i> Number of projects refereed by you</i></p></th></tr>";
	echo "<tr><td>Internacionais</td><td>".$questionario->arbitragensProjetos->api."</td>";
	echo "<tr><td>Nacionais</td><td>".$questionario->arbitragensProjetos->apn."</td></tr>";
	
	
	echo "</table>";
	
	echo "</fieldset>";
	
	echo "<p><fieldset class='normal'>\n";
	echo "<legend>Ensaios Clínicos ".$anoactual." /<i>Research Projects or clinical Trials".$anoactual." </i></legend>\n";
	
	
	echo "
<table class='box-table-b'>
    <thead>
	    <tr>
      <th>Tipo/Entidade<p><i>Type/Entity</i></p></th>
	  <th>Estagio<p><i></i></p></th>
      <th>Entidade Financiadora<p><i>Sponsor</i></p></th>
      <th>Instituição de Acolhimento<p><i>Host Institution</i></p></th>
      <th>Montante  total solicitado<p><i>Total amount requested</i></p></th>
      <th>Montante  total aprovado<p><i>Total amount approved</i></p></th>
      <th>Montante  atribuído à FMUP<p><i>Total amount attibuted to FMUP</i></p></th>
      <th>É Investigador Responsável?<p><i>Are you Principal Investigator?</i></p></th>
      <th>Código/Referência<p><i>Code/Reference</i></p></th>
      <th>Título em INGLÊS<p><i>Title in ENGLISH</i></p></th>
      <th>Data Início<p><i>Start Date</i></p></th>
      <th>Data Fim<p><i>End Date</i></p></th>
            <th>Link<p><i>Link</i></p></th>
            <th>Estado<p><i>Status</i></p></th>
	    </tr>
    </thead>
<tbody>
";
	
	foreach ($questionario->clinicaltrials as $i => $value){
		echo "<tr>";
		echo "<td>";
		getTipoEntidadeCT($i);
		echo "</td>";
		echo "<td>";
		echo getEstagioCT($i);
		echo "</td>";
		echo "<td>".$questionario->clinicaltrials[$i]->entidadefinanciadora."</td>";
		echo "<td>".$questionario->clinicaltrials[$i]->acolhimento."</td>";
		echo "<td>".$questionario->clinicaltrials[$i]->montante."€</td>";
		echo "<td>".$questionario->clinicaltrials[$i]->montantea."€</td>";
		echo "<td>".$questionario->clinicaltrials[$i]->montantefmup."€</td>";
		echo "<td>".checkInvCT($i)."</td>";
		echo "<td>".$questionario->clinicaltrials[$i]->codigo."</td>";
		echo "<td>".$questionario->clinicaltrials[$i]->titulo."</td>";
		echo "<td>".$questionario->clinicaltrials[$i]->datainicio."</td>";
		echo "<td>".$questionario->clinicaltrials[$i]->datafim."</td>";
		echo "<td>".$questionario->clinicaltrials[$i]->link."</td>";
		echo "<td>".getEstadoCT($i)."</td>";
		echo "</tr>";
	
	
	}
	echo "</tbody><tfoot></tfoot>";
	
	echo "</table>";

	echo "</fieldset>";
	
	echo "</div>";
	
	
	
	
	function getTipoEntidade($i) {
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_tipoentidadefinanciadora");
	

		while ($row = mysql_fetch_assoc($lValues)) {	
			if(checkTipoEntidade($row["ID"],$i))
				echo $row["DESCRICAO"];
		}

		$db->disconnect();
				
	}	
	function getTipoEntidadeCT($i) {
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_tipoentidadefinanciadora");

		while ($row = mysql_fetch_assoc($lValues)) {
			if(checkTipoEntidadeCT($row["ID"],$i))
				echo $row["DESCRICAO"];
		}
		$db->disconnect();
	
	}
	
	function checkTipoEntidadeCT($id,$i){
		global $questionario;
		if($questionario->clinicaltrials[$i]->entidade==$id)
			return true;
		else 
			return false;
	}
	

				
	function checkTipoEntidade($id,$i){
		global $questionario;
		if($questionario->projectos[$i]->entidade==$id)
			return true;
		else 
			return false;
	}

				
	function checkInv($i){
		global $questionario;
		if($questionario->projectos[$i]->invresponsavel==1)
			return "Sim";
		else 
			return "Não";
	}
	
	function checkInvCT($i){
		global $questionario;
		if($questionario->clinicaltrials[$i]->invresponsavel==1)
			return "Sim";
		else 
			return "Não";
	}
	
	
	function getEstadoProjeto($i) {
		global $questionario;
		$db = new Database();
		$lValues =$db->getLookupValues("lista_estadoProjetos");
		$texto="";
		while ($row = mysql_fetch_assoc($lValues)) {
			if(checkEstadoProjeto($row["ID"],$i))
				$texto=$row["DESCRICAO"];
		}
		
		$db->disconnect();
		return $texto;
	
	}
	function checkEstadoProjeto($id,$i){
		global $questionario;
	
		if($questionario->projectos[$i]->estado==$id)
			return true;
		else
			return false;
	}
	
	
	function getEstadoCT($i) {
		global $questionario;
		$db = new Database();
		$lValues =$db->getLookupValues("lista_estadoCT");
		$texto="";
		while ($row = mysql_fetch_assoc($lValues)) {
			if(checkEstadoCT($row["ID"],$i))
				$texto=$row["DESCRICAO"];
		}
		
		$db->disconnect();
		return $texto;

	}
	function checkEstadoCT($id,$i){
		global $questionario;
	
		if($questionario->clinicaltrials[$i]->estado==$id)
			return true;
		else
			return false;
	}
	
	function getEstagioCT($i) {
		global $questionario;
		$db = new Database();
		$lValues =$db->getLookupValues("lista_estagiosCT");
		$texto="";
		while ($row = mysql_fetch_assoc($lValues)) {
			if(checkEstagioCT($row["ID"],$i))
				$texto=$row["DESCRICAO"];
		}
		
		$db->disconnect();
		return $texto;
	
	}
	function checkEstagioCT($id,$i){
		global $questionario;
	
		if($questionario->clinicaltrials[$i]->estagio==$id)
			return true;
		else
			return false;
	}

?>