<?php
	echo "<div id='content1' style='display: inline;'>";
	echo "<fieldset class='normal'>\n";
	echo "<legend>Identificação Civil/<i>Civil Identification</i></legend>\n";

	echo "<table class='box-table-a'>";

	echo "<tr><td><label for='nome' class='float'>Nome Completo:<p><i>Full Name:</i></p></label></td>\n";
	echo "<td>".$questionario->investigador->nome."</td></tr>\n";
	
	echo "<tr><td><label for='nummec' class='float'>Número Mecanográfico :<p><i>Mecanographical Number :</i></p></label></td>\n";
	echo "<td>".$questionario->investigador->nummec."</td></tr>\n";
	
	echo "<tr><td><label for='numidentificacao' class='float'>Nº de B. Identidade / Passaporte :<p><i>ID / Passaport Number :</i></p></label></td>\n";
	echo "<td>".$questionario->investigador->numidentificacao."</td></tr>\n";

	getSexo();
	getPaisesNacionalidade();
	getPaisesNaturalidade();
	getConcelhos();
	getFreguesias();
	
	getServicos();
		
	echo "<tr><td><label for='datanasc' class='float'>Data de Nascimento:<p><i>Date of Birth:</i></p></label></td>\n";
	//echo "<input class='inp-textAuto' type='text' name='datanasc' size='50' value='".$questionario->investigador->datanasc."'></p>\n";
	echo "<td>".$questionario->investigador->datanasc."</td></tr>";
	
	echo "<tr><td><label for='morada' class='float'>Morada da residência (incluindo código postal):<p><i>Full home address, including postcode:</i></p></label></td>\n";
	echo "<td>".$questionario->investigador->morada."</td></tr>\n";
	
	echo "<tr><td><label for='email' class='float'>Email Institucional:<p><i>Institutional Email:</i></p></label></td>\n";
	echo "<td>".$questionario->investigador->email."</td></tr>\n";
	
	echo "<tr><td><label for='email_alternativo' class='float'>Email Alternativo:<p><i>Other Email:</i></p></label></td>\n";
	echo "<td>".$questionario->investigador->email_alternativo."</td></tr>\n";
	
	echo "<tr><td><label for='telefone' class='float'>Telefone:<p><i>Phone:</i></p></label></td>\n";
	echo "<td>".$questionario->investigador->telefone."</td></tr>\n";

	echo "<tr><td><label for='telefone' class='float'>Telemóvel:<p><i>Mobile:</i></p></label></td>\n";
	echo "<td>".$questionario->investigador->telemovel."</td></tr>\n";
		
	echo "<tr><td><label for='telefone' class='float'>Extensão Interna:<p><i>Extension Number:</i></p></label></td>\n";
	echo "<td>".$questionario->investigador->extensao."</td></tr>\n";
	

	echo "</table>";
	
	echo "</fieldset>";
	
	echo "</div>";
	

function getSexo(){
	global $questionario;
	echo "<tr><td><label for='sexo' class='float'>Género:<p><i>Gender:</i></p></label></td>\n";
	if($questionario->investigador->sexo==0){
		echo "<td>Feminino</td>";
	}else if ($questionario->investigador->sexo==1){
		echo "<td>Masculino</td>";
	}else{
		echo "<td>NA</td>";
	}
}	

function getPaisesNaturalidade(){
	$db = new Database();
	$lValues =$db->getLookupValues("lista_paises");
	echo "<tr><td><label for='paisnac' class='float'>País de Naturalidade:<p><i>Country of Birth:</i></p></label></td>\n";
	while ($row = mysql_fetch_assoc($lValues)) {
		if(checkPaisNaturalidade($row["ID"]))
			echo "<td>".$row["DESCRICAO"]."</td>\n";
	}

	$db->disconnect();
}

function getPaisesNacionalidade(){
	$db = new Database();
	$lValues =$db->getLookupValues("lista_paises");
	echo "<tr><td><label for='paisnac' class='float'>País de Nacionalidade:<p><i>Citizenship:</i></p></label></td>\n";
	while ($row = mysql_fetch_assoc($lValues)) {
		if(checkPaisNacionalidade($row["ID"]))
			echo "<td>".$row["DESCRICAO"]."</td>\n";
	}

	$db->disconnect();
}


function getConcelhos(){
	$db = new Database();
	$lValues =$db->getLookupValues("lista_concelhos");
	echo "<tr><td><label for='concelhonat' class='float'>Concelho de Naturalidade:</label></td>\n";
	echo "<td>";
	while ($row = mysql_fetch_assoc($lValues)) {
		if(checkConcelho($row["ID"]))
			echo $row["DESCRICAO"];
	}
	echo "</td></tr>\n";
	$db->disconnect();
}

function getFreguesias(){

	$db = new Database();
	$lValues =$db->getLookupValues("lista_freguesias");

	echo "<tr><td><label for='freguesianat' class='float'>Freguesia de Naturalidade:</label></td>\n";
	echo "<td>";
	while ($row = mysql_fetch_assoc($lValues)) {
		if(checkFreguesia($row["ID"]))
			echo $row["DESCRICAO"];
	}
	echo "</td></tr>\n";
	$db->disconnect();
}

function getServicos(){

	$db = new Database();
	$lValues =$db->getLookupValues("lista_servicosfmup");

	echo "<tr><td><label for='servico' class='float'>Serviço/Departamento:<br><i>Service/Department:</i></label></td>\n";
	echo "<td>";
	while ($row = mysql_fetch_assoc($lValues)) {
		if(checkServico($row["ID"]))
			echo $row["DESCRICAO"];
	}
	echo "</td></tr>\n";
	$db->disconnect();
}


function checkServico($id){
	global $questionario;
	if($questionario->investigador->servico==$id)
		return true;
	else
		return false;
}

function checkFreguesia($id){
	global $questionario;
	if($questionario->investigador->freguesianat==$id)
		return true;
	else
		return false;
}

function checkConcelho($id){
	global $questionario;
	if($questionario->investigador->concelhonat==$id)
		return true;
	else
		return false;
}


function checkPaisNaturalidade($id){
	global $questionario;
	if($questionario->investigador->paisnat==$id)
		return true;
	else 
		return false;
}

function checkPaisNacionalidade($id){
	global $questionario;
	if($questionario->investigador->paisnac==$id)
		return true;
	else
		return false;
}

?>