<?php

echo "<div id='content25' style='display: inline;'>";
echo "<p><fieldset class='normal'>\n";
			echo "<legend>Outras Atividades/<i>Other Activities</i></legend>\n";
	
			
echo " <table  class='box-table-a'>
  <tbody>
    <!-- Results table headers -->
    <tr>
      <th></th>
      <th>Atividades/<i>Activities</i></th>
    </tr>";
	
	
echo "<tr><td>Monografias de apoio às aulas: obras completas registadas em biblioteca ou e-platform pública<p><i>Published Lecture Notes</i></p></td><td>".$questionario->outrasactividades->texapoio."</td></tr>";
echo "<tr><td>Livros Didáticos<p><i>Textbooks</i></p></td><td>".$questionario->outrasactividades->livdidaticos."</td></tr>";
echo "<tr><td>Livros de Divulgação<p><i>Scientific books for the public</i></p></td><td>".$questionario->outrasactividades->livdivulgacao."</td></tr>";
echo "<tr><td>Vídeos, jogos, aplicações<p><i>Videos, games, applications</i></p></td><td>".$questionario->outrasactividades->app."</td></tr>";
echo "<tr><td>Páginas Internet<p><i>Webpages</i></p></td><td>".$questionario->outrasactividades->webp."</td></tr>";
echo "<tr><td>Aparições Televisão, Rádio ou Imprensa<p><i>Tv, Radio or Press</i></p></td><td>".$questionario->outrasactividades->tvradiopress."</td></tr>";

//echo "<tr><td>Ações de Divulgação em Escolas<p><i>Activities in schools</i></p></td><td><input class='inp-textAuto' type='text' name='acoesdivulgacaoescolas' maxlength='3' size='3' onkeypress='validate(event)' value='".$questionario->outrasactividades->acoesdivulgacaoescolas."'></td></tr>";
//echo "<tr><td>Ações de Divulgação para público em geral<p><i>Activities for the general public</i></p></td><td><input class='inp-textAuto' type='text' name='acoesdivulgacaogeral' maxlength='3' size='3' onkeypress='validate(event)' value='".$questionario->outrasactividades->acoesdivulgacaogeral."'></td></tr>";
   
    echo "</tbody></table>";
    echo "<br /><p>";
     //echo "<input type='submit' name='navOption' value='Nova Entidade'></p>";
	
	//echo "<input type='hidden' name='apagaRegEntidade' value='".$i."'/>";
	echo "</fieldset>";
	
	
	echo "<fieldset class='normal'>\n";
	echo "<legend>Ações de Divulgacao/<i>Training conducted</i></legend>\n";
	
	
	echo "
	<table  class='box-table-b'>
	<!-- Results table headers -->
	<tr>

	<th>Título<p><i> Title</i></p></th>
	<th>Data<p><i>Date</i></p></th>
	<th>Local<p><i>Location</i></p></th>
			<th>Nº Participantes<p><i>Number of Participants</i></p></th>
	</tr>";
	
	foreach ($questionario->acoesdivulgacao as $i => $value){
			
			
		echo "<tr>";
			

	
		echo "<td>";
		echo $questionario->acoesdivulgacao[$i]->titulo;
		echo "</td>";
	
		echo "<td>";
		echo $questionario->acoesdivulgacao[$i]->data;
		echo "</td>";
	
		echo "<td>";
		echo $questionario->acoesdivulgacao[$i]->local;
		echo "</td>";
		echo "<td>";
		echo $questionario->acoesdivulgacao[$i]->npart;
		echo "</td>";
		
		echo "</tr>";
	
	}

	echo "</table>";
	

	echo "</fieldset>";
	
	
	echo "</div>";
	
	
	echo "</div>";
	
?>