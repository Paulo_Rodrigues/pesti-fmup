<?php
echo "<div id='content14' style='display: inline;'>";

echo "<p><fieldset class='normal'>\n";
echo "<legend>Colaboradores Atuais na FMUP/<i>Current FMUP Collaborators</i></legend>\n";

echo "
<table  class='box-table-b'>
<tbody>
<!-- Results table headers -->
<tr>

<th>Nome Científico<p><i>Scientific Name with Initials (e.g. Guimaraes, LP)</i></p></th>
<th>Departmento<p><i>Department</i></p></th>
</tr>";
 
foreach ($questionario->colaboradoresinternos as $i => $value){
	 
		
	echo "<tr>";
		
	echo "<td>";
	echo $questionario->colaboradoresinternos[$i]->nomecient;
	echo "</td>";

	echo "<td>";
	getDepartamentos($i);
	echo "</td>";
		

	echo "</tr>";


}

echo "</tbody></table>";

echo "</fieldset>";


echo "<p><fieldset class='normal'>\n";
			echo "<legend>Colaboradores Externos Atuais/<i>Current External Collaborators</i></legend>\n";


			    echo "
    <table  class='box-table-b'>
  <tbody>
    <!-- Results table headers -->
    <tr>

      <th>Nome Científico com Iniciais<p><i>Scientific Name with Initials (e.g. Guimaraes, LP)</i></p></th>
      <th>Instituição<p><i>Institution</i></p></th>
      <th>País<p><i>Country</i></p></th>
    </tr>";
			    
    foreach ($questionario->colaboradores as $i => $value){
    	
			
			echo "<tr>";

			echo "<td>";
			echo $questionario->colaboradores[$i]->nomecient;
    		echo "</td>";
    					
			echo "<td>";
			echo $questionario->colaboradores[$i]->instituicao;
			echo "</td>";
			
			echo "<td>";
			getPaisesCol($i);
			echo "</td>";
			

	    	echo "</tr>";
	    	
	    	
    }

    echo "</tbody></table>";

	echo "</fieldset>";
	echo "</div>";
	
	function getPaisesCol($i){
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_paises");

		while ($row = mysql_fetch_assoc($lValues)) {	
			if(checkPaisCol($row["ID"],$i))
				echo $row["DESCRICAO"];
		}

		$db->disconnect();
				
	}	
				
	function checkPaisCol($id,$i){
		global $questionario;
		if($questionario->colaboradores[$i]->pais==$id)
			return true;
		else 
			return false;
	}
	function getDepartamentos($i){
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_servicosfmup");
	
		while ($row = mysql_fetch_assoc($lValues)) {
			if(checkDepartamento($row["ID"],$i))
				echo $row["DESCRICAO"];
		}
		$db->disconnect();
	}
	
	function checkDepartamento($id,$i){
		global $questionario;
		if($questionario->colaboradoresinternos[$i]->departamento==$id)
			return true;
		else
			return false;
	}
	
	
	?>