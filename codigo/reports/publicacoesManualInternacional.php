<?php
echo "<div id='content11' style='display: inline;'>";
$outJMAN="";;
$outJAMAN="";
$outPRPMAN="";
$outCPMAN="";
$outOAMAN="";
$outSMAN="";
$outLMAN="";
$outCLMAN="";
$outAMAN="";




$tcols="
    <tr><
		<th>TIPO<p><i>TYPE</i></p></th>
		<th>TÍTULO DA OBRA<p><i>WORK TITLE</i></p></th>
		<th>ISSN</th>
		<th>ISSN_LINKING</th>
		<th>ISSN_ELECTRONIC</th>
		<th>ISBN</th>
		<th>TITULO<p><i>TITLE</i></p></th>
		<th>AUTORES<p><i>AUTHORS</i></p></th>
		<th>VOLUME</th>
		<th>ISSUE</th>
		<th>AR</th>
		<th>COLECAO<p><i>COLECTION</i></p></th>
		<th>PRIMPAGINA<p><i>FIRSTPAGE</i></p></th>
		<th>ULTPAGINA<p><i>LASTPAGE</i></p></th>
		<th>IFACTOR</th>
		<th>ANO<p><i>YEAR</i></p></th>
		<th>CITACOES<p><i>CITATIONS</i></p></th>
		<th>NPATENTE<p><i>PATENT NR.</i></p></th>
		<th>DATAPATENTE<p><i>DATE PUBLICATION</i></p></th>
		<th>IPC</th>
		<th>AREA<p><i>AREA</i></p></th>
		<th>LINK<p><i>LINK</i></p></th>
		<th>ESTADO<p><i>ESTADO</i></p></th>
		</tr>";

$tcolsJ="
    <tr>
		<th>ISSN</th>
		<th>REVISTA<p><i>JOURNAL</i></p></th>
		<th>TÍTULO<p><i>TITLE</i></p></th>
		<th>AUTORES<p><i>AUTHORS</i></p></th>
		<th>VOLUME</th>
		<th>ISSUE</th>
		<th>PRIMEIRA PÁG.<p><i>FIRST PAGE</i></p></th>
		<th>ÚLTIMA PÁG.<p><i>LAST PAGE</i></p></th>
				<th>LINK<p><i>LINK</i></p></th>
		<th>ESTADO<p><i>ESTADO</i></p></th>
		</tr>";

$tcolsCONF="
    <tr>
		<th>ISBN</th>
		<th>TÍTULO DA OBRA<p><i>WORK TITLE</i></p></th>
		<th>EDITORES<p><i>EDITORS</i></p></th>
		<th>TÍTULO ARTIGO<p><i>ARTICLE TITLE</i></p></th>
		<th>AUTORES<p><i>AUTHORS</i></p></th>
		<th>PRIMEIRA PÁG.<p><i>FIRST PAGE</i></p></th>
		<th>ÚLTIMA PÁG.<p><i>LAST PAGE</i></p></th>
				<th>LINK<p><i>LINK</i></p></th>
		<th>ESTADO<p><i>ESTADO</i></p></th>
		</tr>";

$tcolsPRP="
    <tr>
		<th>ISSN</th>
		<th>TÍTULO DA OBRA<p><i>WORK TITLE</i></p></th>
		<th>EDITORES<p><i>EDITORS</i></p></th>
		<th>TÍTULO ARTIGO<p><i>ARTICLE TITLE</i></p></th>
		<th>AUTORES<p><i>AUTHORS</i></p></th>
		<th>PRIMEIRA PÁG.<p><i>FIRST PAGE</i></p></th>
		<th>ÚLTIMA PÁG.<p><i>LAST PAGE</i></p></th>
				<th>LINK<p><i>LINK</i></p></th>
		<th>ESTADO<p><i>ESTADO</i></p></th>
		</tr>";

$tcolsCLIV="
    <tr>
		<th>ISBN</th>
		<th>TÍTULO DA OBRA<p><i>WORK TITLE</i></p></th>
		<th>EDITORA<p><i>PUBLISHER</i></p></th>
		<th>EDITORES<p><i>EDITORS</i></p></th>			
		<th>TÍTULO ARTIGO<p><i>ARTICLE TITLE</i></p></th>
		<th>AUTORES<p><i>EDITORS</i></p></th>
		<th>PRIMEIRA PÁG.<p><i>FIRST PAGE</i></p></th>
		<th>ÚLTIMA PÁG.<p><i>LAST PAGE</i></p></th>
				<th>LINK<p><i>LINK</i></p></th>
		<th>ESTADO<p><i>ESTADO</i></p></th>
		</tr>";

$tcolsPAT="
    <tr>
		<th>Nº PATENTE<p><i>PATENT NR.</i></p></th>
		<th>IPC</th>
		<th>TÍTULO<p><i>TITLE</i></p></th>
		<th>AUTORES<p><i>AUTHORS</i></p></th>		
		<th>DATA de PUBLICAÇÃO<p><i>PUBLICATION DATE</i></p></th>
				<th>LINK<p><i>LINK</i></p></th>
		<th>ESTADO<p><i>ESTADO</i></p></th>
		</tr>";

$tcolsLIV="
    <tr>
		<th>ISBN</th>
		<th>TÍTULO<p><i>TITLE</i></p></th>
		<th>AUTORES<p><i>EDITORS</i></p></th>		
		<th>EDITOR<p><i>EDITOR</i></p></th>
		<th>EDITORA<p><i>PUBLISHER</i></p></th>
				<th>LINK<p><i>LINK</i></p></th>
		<th>ESTADO<p><i>ESTADO</i></p></th>
		</tr>";


	foreach ($questionario->publicacoesMANUALInternacional as $i => $value){
			
			$tipo=$questionario->publicacoesMANUALInternacional[$i]->tipofmup;

			switch($tipo){
				case 'J': { // Opção de entrada
							$outJMAN=$outJMAN."<tr>";
						   	//$outJMAN=$outJMAN."<td><input class='inp-textAuto' type='text' name='pmtipo_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->tipo."'></td>";
							$outJMAN=$outJMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->issn."</td>";			
						   	$outJMAN=$outJMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->nomepublicacao."</td>";
							//$outJMAN=$outJMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_linking_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->issn_linking."'></td>";
							//$outJMAN=$outJMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_electronic_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->issn_electronic."'></td>";
							//$outJMAN=$outJMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmisbn_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->isbn."'></td>";
							$outJMAN=$outJMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->titulo."</td>";
							$outJMAN=$outJMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->autores."</td>";
							$outJMAN=$outJMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->volume."</td>";
							$outJMAN=$outJMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->issue."</td>";
							//$outJMAN=$outJMAN."<td><input class='inp-textAuto' type='text' name='pmar_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->ar."'></td>";
							//$outJMAN=$outJMAN."<td><input class='inp-textAuto' type='text' name='pmcolecao_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->colecao."'></td>";
							$outJMAN=$outJMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->primpagina."</td>";
							$outJMAN=$outJMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->ultpagina."</td>";
							$outJMAN=$outJMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->link."</textarea></td>";
							$outJMAN=$outJMAN."<td>".getEstadoPublicacaoMI($i)."</td>";
							//$outJMAN=$outJMAN."<td><input class='inp-textAuto' type='text' name='pmifactor_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->ifactor."</td>";
							//$outJMAN=$outJMAN."<td><input class='inp-textAuto' type='text' name='pmano_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->ano."</td>";
							//$outJMAN=$outJMAN."<td><input class='inp-textAuto' type='text' name='pmcitacoes_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->citacoes."</td>";
							//$outJMAN=$outJMAN."<td><input class='inp-textAuto' type='text' name='pmnpatente_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->npatente."</td>";
							//$outJMAN=$outJMAN."<td><input class='inp-textAuto' type='text' name='pmdatapatente_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->datapatente."</td>";
							//$outJMAN=$outJMAN."<td><input class='inp-textAuto' type='text' name='pmipc_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->ipc."</td>";
							//$outJMAN=$outJMAN."<td><input class='inp-textAuto' type='text' name='pmarea_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->area."</td>";			
							//$outJMAN=$outJMAN."<td><input class='inp-textAuto' type='text' name='pmconftitle_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->conftitle."</td>";
							//$outJMAN=$outJMAN."<td><input class='inp-textAuto' type='text' name='pmlanguage_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->language."</td>";	
							//$outJMAN=$outJMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->editor."</td>";
							//$outJMAN=$outJMAN."<td><input class='inp-textAuto' type='text' name='pmtipofmup_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->tipofmup."</td>";
							$outJMAN=$outJMAN."</tr>";
				}
				break;
				case 'PRP': { // Opção de entrada
							$outPRPMAN=$outPRPMAN."<tr>";
						   	$outPRPMAN=$outPRPMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->issn."</td>";
							$outPRPMAN=$outPRPMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->nomepublicacao."</td>";
							//$outPRPMAN=$outPRPMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->editor."'></td>";
							$outPRPMAN=$outPRPMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->titulo."</td>";
							$outPRPMAN=$outPRPMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->autores."</td>";
							$outPRPMAN=$outPRPMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->volume."</td>";
							$outPRPMAN=$outPRPMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->issue."</td>";
							$outPRPMAN=$outPRPMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->primpagina."</td>";
							$outPRPMAN=$outPRPMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->ultpagina."</td>";
							$outPRPMAN=$outPRPMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->link."</td>";
							$outPRPMAN=$outPRPMAN."<td>".getEstadoPublicacaoMI($i)."</td>";
						   	/*$outPRPMAN=$outPRPMAN."<td><input class='inp-textAuto' type='text' name='pmtipo_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->tipo."'></td>";
							$outPRPMAN=$outPRPMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->issn."'></td>";			
						   	$outPRPMAN=$outPRPMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_linking_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->issn_linking."'></td>";
							$outPRPMAN=$outPRPMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_electronic_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->issn_electronic."'></td>";
							$outPRPMAN=$outPRPMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->volume."'></td>";
							$outPRPMAN=$outPRPMAN."<td><input class='inp-textAuto' type='text' name='pmar_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->ar."'></td>";
							$outPRPMAN=$outPRPMAN."<td><input class='inp-textAuto' type='text' name='pmcolecao_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->colecao."'></td>";
							$outPRPMAN=$outPRPMAN."<td><input class='inp-textAuto' type='text' name='pmifactor_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->ifactor."'></td>";
							$outPRPMAN=$outPRPMAN."<td><input class='inp-textAuto' type='text' name='pmano_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->ano."'></td>";
							$outPRPMAN=$outPRPMAN."<td><input class='inp-textAuto' type='text' name='pmcitacoes_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->citacoes."'></td>";
							$outPRPMAN=$outPRPMAN."<td><input class='inp-textAuto' type='text' name='pmnpatente_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->npatente."'></td>";
							$outPRPMAN=$outPRPMAN."<td><input class='inp-textAuto' type='text' name='pmdatapatente_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->datapatente."'></td>";
							$outPRPMAN=$outPRPMAN."<td><input class='inp-textAuto' type='text' name='pmipc_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->ipc."'></td>";
							$outPRPMAN=$outPRPMAN."<td><input class='inp-textAuto' type='text' name='pmarea_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->area."'></td>";			
							$outPRPMAN=$outPRPMAN."<td><input class='inp-textAuto' type='text' name='pmconftitle_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->conftitle."'></td>";
							$outPRPMAN=$outPRPMAN."<td><input class='inp-textAuto' type='text' name='pmlanguage_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->language."'></td>";	
							$outPRPMAN=$outPRPMAN."<td><input class='inp-textAuto' type='text' name='pmtipofmup_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->tipofmup."'></td>";
							*/$outPRPMAN=$outPRPMAN."</tr>";
				}
				break;
				case 'JA': { // Opção de entrada
							$outJAMAN=$outJAMAN."<tr>";
						   	$outJAMAN=$outJAMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->issn."</td>";
							
							
							$outJAMAN=$outJAMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->nomepublicacao."</td>";
							$outJAMAN=$outJAMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->titulo."</td>";
							
							$outJAMAN=$outJAMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->autores."</td>";
							$outJAMAN=$outJAMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->volume."</td>";
							$outJAMAN=$outJAMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->issue."</td>";	
							$outJAMAN=$outJAMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->primpagina."</td>";
							$outJAMAN=$outJAMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->ultpagina."</td>";
							$outJAMAN=$outJAMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->link."</td>";
							$outJAMAN=$outJAMAN."<td>".getEstadoPublicacaoMI($i)."</td>";
						   	/*$outJAMAN=$outJAMAN."<td><input class='inp-textAuto' type='text' name='pmtipo_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->tipo."'></td>";
							$outJAMAN=$outJAMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->issn."'></td>";			
						   	$outJAMAN=$outJAMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_linking_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->issn_linking."'></td>";
							$outJAMAN=$outJAMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_electronic_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->issn_electronic."'></td>";
							$outJAMAN=$outJAMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->volume."'></td>";
							$outJAMAN=$outJAMAN."<td><input class='inp-textAuto' type='text' name='pmar_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->ar."'></td>";
							$outJAMAN=$outJAMAN."<td><input class='inp-textAuto' type='text' name='pmcolecao_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->colecao."'></td>";
							$outJAMAN=$outJAMAN."<td><input class='inp-textAuto' type='text' name='pmifactor_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->ifactor."'></td>";
							$outJAMAN=$outJAMAN."<td><input class='inp-textAuto' type='text' name='pmano_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->ano."'></td>";
							$outJAMAN=$outJAMAN."<td><input class='inp-textAuto' type='text' name='pmcitacoes_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->citacoes."'></td>";
							$outJAMAN=$outJAMAN."<td><input class='inp-textAuto' type='text' name='pmnpatente_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->npatente."'></td>";
							$outJAMAN=$outJAMAN."<td><input class='inp-textAuto' type='text' name='pmdatapatente_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->datapatente."'></td>";
							$outJAMAN=$outJAMAN."<td><input class='inp-textAuto' type='text' name='pmipc_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->ipc."'></td>";
							$outJAMAN=$outJAMAN."<td><input class='inp-textAuto' type='text' name='pmarea_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->area."'></td>";			
							$outJAMAN=$outJAMAN."<td><input class='inp-textAuto' type='text' name='pmconftitle_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->conftitle."'></td>";
							$outJAMAN=$outJAMAN."<td><input class='inp-textAuto' type='text' name='pmlanguage_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->language."'></td>";	
							$outJAMAN=$outJAMAN."<td><input class='inp-textAuto' type='text' name='pmtipofmup_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->tipofmup."'></td>";
							*/$outJAMAN=$outJAMAN."</tr>";
				}
				break;
				case 'CP': { // Opção de entrada
								$outCPMAN=$outCPMAN."<tr>";
						   	$outCPMAN=$outCPMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->isbn."</td>";
							$outCPMAN=$outCPMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->nomepublicacao."</td>";
							
							$outCPMAN=$outCPMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->editor."</td>";
							$outCPMAN=$outCPMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->titulo."</td>";
							
							$outCPMAN=$outCPMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->autores."</td>";
							$outCPMAN=$outCPMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->primpagina."</td>";
							$outCPMAN=$outCPMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->ultpagina."</td>";
							$outCPMAN=$outCPMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->link."</textarea></td>";
							$outCPMAN=$outCPMAN."<td>".getEstadoPublicacaoMI($i)."</td>";
						   	/*$outCPMAN=$outCPMAN."<td><input class='inp-textAuto' type='text' name='pmtipo_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->tipo."'></td>";
							$outCPMAN=$outCPMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->issn."'></td>";			
						   	$outCPMAN=$outCPMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_linking_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->issn_linking."'></td>";
							$outCPMAN=$outCPMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_electronic_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->issn_electronic."'></td>";
							$outCPMAN=$outCPMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->volume."'></td>";
							$outCPMAN=$outCPMAN."<td><input class='inp-textAuto' type='text' name='pmissue_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->issue."'></td>";
							$outCPMAN=$outCPMAN."<td><input class='inp-textAuto' type='text' name='pmar_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->ar."'></td>";
							$outCPMAN=$outCPMAN."<td><input class='inp-textAuto' type='text' name='pmcolecao_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->colecao."'></td>";
							$outCPMAN=$outCPMAN."<td><input class='inp-textAuto' type='text' name='pmifactor_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->ifactor."'></td>";
							$outCPMAN=$outCPMAN."<td><input class='inp-textAuto' type='text' name='pmano_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->ano."'></td>";
							$outCPMAN=$outCPMAN."<td><input class='inp-textAuto' type='text' name='pmcitacoes_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->citacoes."'></td>";
							$outCPMAN=$outCPMAN."<td><input class='inp-textAuto' type='text' name='pmnpatente_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->npatente."'></td>";
							$outCPMAN=$outCPMAN."<td><input class='inp-textAuto' type='text' name='pmdatapatente_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->datapatente."'></td>";
							$outCPMAN=$outCPMAN."<td><input class='inp-textAuto' type='text' name='pmipc_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->ipc."'></td>";
							$outCPMAN=$outCPMAN."<td><input class='inp-textAuto' type='text' name='pmarea_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->area."'></td>";			
							$outCPMAN=$outCPMAN."<td><input class='inp-textAuto' type='text' name='pmconftitle_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->conftitle."'></td>";
							$outCPMAN=$outCPMAN."<td><input class='inp-textAuto' type='text' name='pmlanguage_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->language."'></td>";	
							$outCPMAN=$outCPMAN."<td><input class='inp-textAuto' type='text' name='pmtipofmup_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->tipofmup."'></td>";
							*/$outCPMAN=$outCPMAN."</tr>";
				}
				break;
				case 'OA': { // Opção de entrada
							$outOAMAN=$outOAMAN."<tr>";
						   	$outOAMAN=$outOAMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->isbn."</td>";
							$outOAMAN=$outOAMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->nomepublicacao."</td>";
							$outOAMAN=$outOAMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->editor."</td>";
							$outOAMAN=$outOAMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->titulo."</td>";
							$outOAMAN=$outOAMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->autores."</td>";
							$outOAMAN=$outOAMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->primpagina."</td>";
							$outOAMAN=$outOAMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->ultpagina."</td>";
							$outOAMAN=$outOAMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->link."</td>";
							$outOAMAN=$outOAMAN."<td>".getEstadoPublicacaoMI($i)."</td>";
						   	/*$outOAMAN=$outOAMAN."<td><input class='inp-textAuto' type='text' name='pmtipo_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->tipo."</td>";
							$outOAMAN=$outOAMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->issn."'></td>";			
						   	$outOAMAN=$outOAMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_linking_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->issn_linking."'></td>";
							$outOAMAN=$outOAMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_electronic_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->issn_electronic."'></td>";
							$outOAMAN=$outOAMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->volume."'></td>";
							$outOAMAN=$outOAMAN."<td><input class='inp-textAuto' type='text' name='pmissue_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->issue."'></td>";
							$outOAMAN=$outOAMAN."<td><input class='inp-textAuto' type='text' name='pmar_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->ar."'></td>";
							$outOAMAN=$outOAMAN."<td><input class='inp-textAuto' type='text' name='pmcolecao_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->colecao."'></td>";
							$outOAMAN=$outOAMAN."<td><input class='inp-textAuto' type='text' name='pmifactor_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->ifactor."'></td>";
							$outOAMAN=$outOAMAN."<td><input class='inp-textAuto' type='text' name='pmano_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->ano."'></td>";
							$outOAMAN=$outOAMAN."<td><input class='inp-textAuto' type='text' name='pmcitacoes_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->citacoes."'></td>";
							$outOAMAN=$outOAMAN."<td><input class='inp-textAuto' type='text' name='pmnpatente_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->npatente."'></td>";
							$outOAMAN=$outOAMAN."<td><input class='inp-textAuto' type='text' name='pmdatapatente_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->datapatente."'></td>";
							$outOAMAN=$outOAMAN."<td><input class='inp-textAuto' type='text' name='pmipc_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->ipc."'></td>";
							$outOAMAN=$outOAMAN."<td><input class='inp-textAuto' type='text' name='pmarea_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->area."'></td>";			
							$outOAMAN=$outOAMAN."<td><input class='inp-textAuto' type='text' name='pmconftitle_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->conftitle."'></td>";
							$outOAMAN=$outOAMAN."<td><input class='inp-textAuto' type='text' name='pmlanguage_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->language."'></td>";	
							$outOAMAN=$outOAMAN."<td><input class='inp-textAuto' type='text' name='pmtipofmup_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->tipofmup."'></td>";
							*/$outOAMAN=$outOAMAN."</tr>";
				}
				break;
				case 'S': { // Opção de entrada
							$outSMAN=$outSMAN."<tr>";
						   	$outSMAN=$outSMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->isbn."</td>";
							$outSMAN=$outSMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->nomepublicacao."</td>";
							$outSMAN=$outSMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->editor."</td>";
							$outSMAN=$outSMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->titulo."</td>";
							$outSMAN=$outSMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->autores."</td>";
							$outSMAN=$outSMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->primpagina."</td>";
							$outSMAN=$outSMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->ultpagina."</td>";
							$outSMAN=$outSMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->link."</td>";
							$outSMAN=$outSMAN."<td>".getEstadoPublicacaoMI($i)."</td>";
						   	/*$outSMAN=$outSMAN."<td><input class='inp-textAuto' type='text' name='pmtipo_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->tipo."</td>";
							$outSMAN=$outSMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->issn."'></td>";			
						   	$outSMAN=$outSMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_linking_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->issn_linking."'></td>";
							$outSMAN=$outSMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_electronic_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->issn_electronic."'></td>";
							$outSMAN=$outSMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->volume."'></td>";
							$outSMAN=$outSMAN."<td><input class='inp-textAuto' type='text' name='pmissue_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->issue."'></td>";
							$outSMAN=$outSMAN."<td><input class='inp-textAuto' type='text' name='pmar_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->ar."'></td>";
							$outSMAN=$outSMAN."<td><input class='inp-textAuto' type='text' name='pmcolecao_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->colecao."'></td>";
							$outSMAN=$outSMAN."<td><input class='inp-textAuto' type='text' name='pmifactor_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->ifactor."'></td>";
							$outSMAN=$outSMAN."<td><input class='inp-textAuto' type='text' name='pmano_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->ano."'></td>";
							$outSMAN=$outSMAN."<td><input class='inp-textAuto' type='text' name='pmcitacoes_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->citacoes."'></td>";
							$outSMAN=$outSMAN."<td><input class='inp-textAuto' type='text' name='pmnpatente_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->npatente."'></td>";
							$outSMAN=$outSMAN."<td><input class='inp-textAuto' type='text' name='pmdatapatente_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->datapatente."'></td>";
							$outSMAN=$outSMAN."<td><input class='inp-textAuto' type='text' name='pmipc_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->ipc."'></td>";
							$outSMAN=$outSMAN."<td><input class='inp-textAuto' type='text' name='pmarea_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->area."'></td>";			
							$outSMAN=$outSMAN."<td><input class='inp-textAuto' type='text' name='pmconftitle_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->conftitle."'></td>";
							$outSMAN=$outSMAN."<td><input class='inp-textAuto' type='text' name='pmlanguage_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->language."'></td>";	
							$outSMAN=$outSMAN."<td><input class='inp-textAuto' type='text' name='pmtipofmup_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->tipofmup."'></td>";
							*/$outSMAN=$outSMAN."</tr>";
				}
				break;
				case 'L': { // Opção de entrada
							$outLMAN=$outLMAN."<tr>";
							$outLMAN=$outLMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->isbn."</td>";
							$outLMAN=$outLMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->titulo."</td>";
							$outLMAN=$outLMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->autores."</td>";
							$outLMAN=$outLMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->editor."</td>";
							$outLMAN=$outLMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->editora."</td>";
							$outLMAN=$outLMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->link."</td>";
							$outLMAN=$outLMAN."<td>".getEstadoPublicacaoMI($i)."</td>";
							/*$outLMAN=$outLMAN."<td><input class='inp-textAuto' type='text' name='pmtipo_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->tipo."'></td>";
						   	$outLMAN=$outLMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->nomepublicacao."'></td>";
							$outLMAN=$outLMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->issn."'></td>";			
							$outLMAN=$outLMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_linking_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->issn_linking."'></td>";
							$outLMAN=$outLMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_electronic_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->issn_electronic."'></td>";
							$outLMAN=$outLMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->volume."'></td>";
							$outLMAN=$outLMAN."<td><input class='inp-textAuto' type='text' name='pmissue_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->issue."'></td>";
							$outLMAN=$outLMAN."<td><input class='inp-textAuto' type='text' name='pmar_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->ar."'></td>";
							$outLMAN=$outLMAN."<td><input class='inp-textAuto' type='text' name='pmcolecao_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->colecao."'></td>";
							$outLMAN=$outLMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->primpagina."'></td>";
							$outLMAN=$outLMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->ultpagina."'></td>";
							$outLMAN=$outLMAN."<td><input class='inp-textAuto' type='text' name='pmifactor_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->ifactor."'></td>";
							$outLMAN=$outLMAN."<td><input class='inp-textAuto' type='text' name='pmano_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->ano."'></td>";
							$outLMAN=$outLMAN."<td><input class='inp-textAuto' type='text' name='pmcitacoes_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->citacoes."'></td>";
							$outLMAN=$outLMAN."<td><input class='inp-textAuto' type='text' name='pmnpatente_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->npatente."'></td>";
							$outLMAN=$outLMAN."<td><input class='inp-textAuto' type='text' name='pmdatapatente_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->datapatente."'></td>";
							$outLMAN=$outLMAN."<td><input class='inp-textAuto' type='text' name='pmipc_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->ipc."'></td>";
							$outLMAN=$outLMAN."<td><input class='inp-textAuto' type='text' name='pmarea_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->area."'></td>";			
							$outLMAN=$outLMAN."<td><input class='inp-textAuto' type='text' name='pmconftitle_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->conftitle."'></td>";
							$outLMAN=$outLMAN."<td><input class='inp-textAuto' type='text' name='pmlanguage_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->language."'></td>";
							$outLMAN=$outLMAN."<td><input class='inp-textAuto' type='text' name='pmtipofmup_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->tipofmup."'></td>";								
							*/$outLMAN=$outLMAN."</tr>";
				}
				break;
				case 'CL': { // Opção de entrada
							$outCLMAN=$outCLMAN."<tr>";
						   	$outCLMAN=$outCLMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->isbn."</td>";
						   	$outCLMAN=$outCLMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->nomepublicacao."</td>";
							$outCLMAN=$outCLMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->editora."</td>";
						   	$outCLMAN=$outCLMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->editor."</td>";
							
							$outCLMAN=$outCLMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->titulo."</td>";
							$outCLMAN=$outCLMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->autores."</td>";
							$outCLMAN=$outCLMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->primpagina."</td>";
							$outCLMAN=$outCLMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->ultpagina."</td>";
							$outCLMAN=$outCLMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->link."</td>";
							$outCLMAN=$outCLMAN."<td>".getEstadoPublicacaoMI($i)."</td>";
						   	
						   /*	$outCLMAN=$outCLMAN."<td><input class='inp-textAuto' type='text' name='pmtipo_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->tipo."'></td>";
							$outCLMAN=$outCLMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->issn."'></td>";			
							$outCLMAN=$outCLMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_linking_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->issn_linking."'></td>";
							$outCLMAN=$outCLMAN."<td><input class='inp-textAuto' type='text' size='9' name='pmissn_electronic_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->issn_electronic."'></td>";
							$outCLMAN=$outCLMAN."<td>".$questionario->publicacoesMANUALInternacional[$i]->volume."'></td>";
							$outCLMAN=$outCLMAN."<td><input class='inp-textAuto' type='text' name='pmissue_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->issue."'></td>";
							$outCLMAN=$outCLMAN."<td><input class='inp-textAuto' type='text' name='pmar_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->ar."'></td>";
							$outCLMAN=$outCLMAN."<td><input class='inp-textAuto' type='text' name='pmcolecao_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->colecao."'></td>";
							$outCLMAN=$outCLMAN."<td><input class='inp-textAuto' type='text' name='pmifactor_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->ifactor."'></td>";
							$outCLMAN=$outCLMAN."<td><input class='inp-textAuto' type='text' name='pmano_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->ano."'></td>";
							$outCLMAN=$outCLMAN."<td><input class='inp-textAuto' type='text' name='pmcitacoes_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->citacoes."'></td>";
							$outCLMAN=$outCLMAN."<td><input class='inp-textAuto' type='text' name='pmnpatente_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->npatente."'></td>";
							$outCLMAN=$outCLMAN."<td><input class='inp-textAuto' type='text' name='pmdatapatente_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->datapatente."'></td>";
							$outCLMAN=$outCLMAN."<td><input class='inp-textAuto' type='text' name='pmipc_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->ipc."'></td>";
							$outCLMAN=$outCLMAN."<td><input class='inp-textAuto' type='text' name='pmarea_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->area."'></td>";			
							$outCLMAN=$outCLMAN."<td><input class='inp-textAuto' type='text' name='pmconftitle_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->conftitle."'></td>";
							$outCLMAN=$outCLMAN."<td><input class='inp-textAuto' type='text' name='pmlanguage_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->language."'></td>";
							$outCLMAN=$outCLMAN."<td><input class='inp-textAuto' type='text' name='pmtipofmup_".$i."' value='".$questionario->publicacoesMANUALInternacional[$i]->tipofmup."'></td>";								
							*/$outCLMAN=$outCLMAN."</tr>";
				}
				break;
				default:{ //echo "DEFAULT";
			       //$outREST=produzTable($outREST,$i);
			    }
				break;
			}
	}
	
	$arg1="";
	$arg2="";
	
	
	
	
		echo "<p><fieldset class='normal'>\n";
		echo "<legend>Publicações Manuais Internacionais/<i>International Publications - by hand</i></legend>\n";

	
		echo"<table class='box-table-b'><caption >ARTIGOS<p><i>ARTICLES</i></p></caption>";echo $tcolsJ;echo $outJMAN;echo "</table>";
		echo"<table class='box-table-b'><caption >PEER REVIEW PROCEEDINGS</caption>";echo $tcolsJ;echo $outPRPMAN;echo "</table>";
		echo"<table class='box-table-b'><caption >SUMÁRIOS<p><i>JOURNAL ABSTRACTS</i></p></caption>";echo $tcolsJ;echo $outJAMAN;echo "</table>";
		
		
		echo"<table class='box-table-b'><caption >LIVROS<p><i>BOOKS</i></p></caption>";echo $tcolsLIV;echo $outLMAN;echo "</table>";
		echo"<table class='box-table-b'><caption >CAPITULOS DE LIVROS<p><i>BOOK CHAPTERS</i></p></caption>";echo $tcolsCLIV;echo $outCLMAN;echo "</table>";
		
		echo"<table class='box-table-b'><caption >CONFERENCE PROCEEDINGS</caption>";echo $tcolsCONF;echo $outCPMAN;echo "</table>";
		echo"<table  class='box-table-b'><caption >OUTROS SUMÁRIOS<br>OTHER ABSTRACTS</caption>";echo $tcolsCONF;echo $outOAMAN;echo "</table>";
		//echo"<table id='box-table-b'><caption >SUMÁRIOS<p><i>ABSTRACTS CONFERENCE PROCEEDINGS</i></p></caption>";echo $tcolsCONF;echo $outSMAN;echo getButton("S",38);echo "</table>";
		//echo"<table id='box-table-b'><caption >AGRADECIMENTOS<p><i>ACKNOWLEDGEMENTS</i></p></caption>";echo $tcolsJ;echo $outAMAN;echo getButton("A",40);echo "</table>";
		
		echo "</fieldset>";

		echo "</div>";
	
	
	function getEstadoPublicacaoMI($i) {
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_estadoPublicacoes");
		$texto="";
		while ($row = mysql_fetch_assoc($lValues)) {
			if(checkEstadoPublicacaoMI($row["ID"],$i))
				$texto=$row["DESCRICAO"];
		}

		$db->disconnect();
		return $texto;
	
	}
	
	function checkEstadoPublicacaoMI($id,$i){
		global $questionario;
		if($questionario->publicacoesMANUALInternacional[$i]->estado==$id)
			return true;
		else
			return false;
	}
	

?>