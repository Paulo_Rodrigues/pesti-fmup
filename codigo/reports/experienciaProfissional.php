<?php

echo "<div id='content4' style='display: inline;'>";
echo "<fieldset class='normal'>\n";
echo "<legend>Caracterização/Characterization</legend><br/>\n";

echo "<table class='box-table-a'><tr><th>Tipo<p><i>Type</i></p></th><th>Percentagem<p><i>Percentage</i></p></th><th>Categoria<p><i>Category</i></p></th><th>Bolsa<p><i>Bolsa</i></p></th></tr>";
echo "<tr><td>Docente<p><i>Lecturer</i></p></td><td>".$questionario->ocupacaoprofissional->docenteper."</td><td>".getCat("docente",$questionario->ocupacaoprofissional->docentecat,$questionario->ocupacaoprofissional->docenteper)."</td><td></td></tr>";
echo "<tr><td>Investigador<p><i>Researcher</i></p></td><td>".$questionario->ocupacaoprofissional->investigadorper."</td><td>".getCatInvestigador("investigador",$questionario->ocupacaoprofissional->investigadorcat,$questionario->ocupacaoprofissional->investigadorper,$questionario->ocupacaoprofissional->investigadorbolsa)."</td><td>".getBolsaInvestigador("investigador",$questionario->ocupacaoprofissional->investigadorcat,$questionario->ocupacaoprofissional->investigadorper,$questionario->ocupacaoprofissional->investigadorbolsa)."</td></tr>";
echo "<tr><td>Técnico Superior</td><td>".$questionario->ocupacaoprofissional->tecper."</td><td></td><td></td></tr>";
echo "<tr><td>Outra<p><i>Other</i></p></td><td>".$questionario->ocupacaoprofissional->outraper."</td><td></td><td></td></tr>";
echo "</table>";
echo "</fieldset>";

echo "<fieldset class='normal'>\n";
echo "<legend>Dedicação FMUP </<i>FMUP Tasks</i></legend><br/>\n";

echo "<table class='box-table-a'>";
echo "<tr><td>Investigação/<i>Research</i></td><td>".$questionario->ocupacaoprofissional->fmupinv."</td></tr>";
echo "<tr><td>Ensino/<i>Education</i></td><td>".$questionario->ocupacaoprofissional->fmupens."</td></tr>";
echo "<tr><td>Gestão/<i>Management</i></td><td>".$questionario->ocupacaoprofissional->fmupgest."</td></tr>";
echo "<tr><td>Transferência de Conhecimento/<i>Knowledge Transfer</i></td><td>".$questionario->ocupacaoprofissional->fmupdc."</td></tr>";
echo "</table>";
echo "</fieldset>";



echo "<p><fieldset class='normal'>\n";
			echo "<legend>Experiência Profissional/<i>Professional Experience</i></legend>\n";
			
	
			    echo "
    <table  class='box-table-b'>
  <tbody>
    <!-- Results table headers -->
    <tr>
      <th>Data de Início<p><i>Start Date</i></p></th>
      <th>Data de Fim<p><i>End Date</i></p></th>
      <th>Posição<p><i>Position</i></p></th>
      <th>Empregador<p><i>Employer</i></p></th>
      <th>Departamento<p><i>Department</i></p></th>
      <th>Percentagem<p><i>Percentage</i></p></th>
    </tr>";
    
    foreach ($questionario->experienciaprofissional as $i => $value){
    	
			
			echo "<tr>";

			
			echo "<td>";
			echo $questionario->experienciaprofissional[$i]->datainicio;
			echo "</td>";

			echo "<td>";
			
    		
			
			if ($questionario->experienciaprofissional[$i]->semtermo=="1"){
				echo "Sem termo";
			}else{ 
				echo $questionario->experienciaprofissional[$i]->datafim;
			}
			
			
			echo "</td>";
			
			echo "<td>";
			echo $questionario->experienciaprofissional[$i]->posicao;
			echo "</td>";
			
			echo "<td>";
			echo $questionario->experienciaprofissional[$i]->empregador;
			echo "</td>";
			
			echo "<td>";
			echo $questionario->experienciaprofissional[$i]->departamento;
			echo "</td>";
			
			echo "<td>";
			echo $questionario->experienciaprofissional[$i]->percentagem;
			echo "</td>";
	    	echo "</tr>";
	    	
	    	
    }

    echo "</tbody></table>";
	echo "</fieldset>";

	echo "</div>";
	

	
	function checkSelectedOP($i,$valor){
	
		if($i==$valor){
			return true;
		}else{
			return false;
		}
	
	}

	
	function getCat($tipo,$valorcat,$valorper) {
		$resultado="";
		$db = new Database();
		$lValues =$db->getLookupValues("lista_tipo".$tipo);
	
	
		while ($row = mysql_fetch_assoc($lValues)) {
			if(checkSelectedOP($row["ID"],$valorcat))
				$resultado=$row["DESCRICAO"];
		}
		$db->disconnect();
		return $resultado;
	}
	
	function getCatInvestigador($tipo,$valorcat,$valorper,$bolsa) {
		$resultado="";
		$db = new Database();
		$lValues =$db->getLookupValues("lista_tipo".$tipo);
		while ($row = mysql_fetch_assoc($lValues)) {
			if(checkSelectedOP($row["ID"],$valorcat))
				$resultado=$row["DESCRICAO"];
		}
	
		$db->disconnect();
		return $resultado;
	}
	
	function getBolsaInvestigador($tipo,$valorcat,$valorper,$bolsa) {
		$resultado="";
		$db = new Database();
		$lValues =$db->getLookupValues("lista_TipoBolsas");
	
		while ($row = mysql_fetch_assoc($lValues)) {
			if(checkSelectedOP($row["ID"],$bolsa))
				$resultado=$row["DESCRICAO"];
		}

		$db->disconnect();
		return $resultado;
	}
	

	
	?>