<?php
echo "<div id='content23' style='display: inline;'>";
echo "<p><fieldset class='normal'>\n";
echo "<legend>Orientações/<i>Supervisions</i></legend>\n";

echo "<table  class='box-table-b'>
    					<tr>
    					<th>Tipo de Orientação<p><i>Type of Work</i></p></th>
      					<th>Tipo de Orientador<p><i>Supervisor Type</i></p></th>
      					<th>Estado<p><i>Status</i></p></th>
      					<th>Título da Tese em Português<p><i>Thesis title in Portuguese</i></p></th>
						<th>Nome completo do Orientando<p><i>Student’s Full Name</i></p></th>
						</tr>";
			    
    foreach ($questionario->orientacoes as $i => $value){
			echo "<tr>";
			   	echo "<td>";
				getTipoOrientacao($i);
				echo "</td>";
				echo "<td>";
				getTipoOrientador($i);
				echo "</td>";
				echo "<td>";
				getOrientacaoEstado($i);
				echo "</td>";
				echo "<td>".$questionario->orientacoes[$i]->titulo."</td>";
				echo "<td>".$questionario->orientacoes[$i]->nome_orientando."</td>";
			echo "</tr>";	    	
    } 
    
    echo "</table>";
    echo "</fieldset>";
    echo "</div>";
    
    
    
	function getTipoOrientacao($i) {
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_tipoOrientacao");
	

		
		while ($row = mysql_fetch_assoc($lValues)) {	
			if(checkTipoOrientacao($row["ID"],$i))
				echo $row["DESCRICAO"];
		}

		$db->disconnect();
				
	}	
	
	function getTipoOrientador($i) {
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_tipoOrientador");
	
		
		while ($row = mysql_fetch_assoc($lValues)) {	
			if(checkTipoOrientador($row["ID"],$i))
				echo $row["DESCRICAO"];
		}

		$db->disconnect();
				
	}	
	
	function getOrientacaoEstado($i) {
	
		$db = new Database();
		$lValues =$db->getLookupValues("lista_estadosOrientacao");

	
		while ($row = mysql_fetch_assoc($lValues)) {
			if(checkOrientacaoEstado($row["ID"],$i))
				echo $row["DESCRICAO"];
		}
		
		$db->disconnect();
	
	}
	
	function checkTipoOrientacao($id,$i){
		global $questionario;
		if($questionario->orientacoes[$i]->tipo_orientacao==$id)
			return true;
		else
			return false;
	}
	
	
	
	function checkTipoOrientador($id,$i){
		global $questionario;
		if($questionario->orientacoes[$i]->tipo_orientador==$id)
			return true;
		else
			return false;
	}
	
				
	function checkOrientacaoEstado($id,$i){
		global $questionario;
		if($questionario->orientacoes[$i]->estado==$id)
			return true;
		else 
			return false;
	}
	
	




		
	?>