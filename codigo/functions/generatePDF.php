<?php
require_once('../classlib/fpdf16/fpdf.php');
require_once('../classlib/Questionario.class.inc');



session_start();
if(isset($_SESSION['questionario'])){
  $questionario=unserialize($_SESSION['questionario']);
  $nome=$questionario->investigador->nome;
}else{
	$nome="DESCONHECIDO";
}

class PDF extends FPDF
{
//Page header
function Header()
{
	global $user;
	//Logo
    //$this->Image('../images/fmupUP_transparencia3.png',20,15,33);
    //Arial bold 15
    $this->SetFont('Arial','B',15);
    //Move to the right
    $this->Cell(80);
    //Title
    $this->Cell(30,10,iconv('UTF-8', 'windows-1252', 'Levantamento Geral de Dados da FMUP'),0,0,'C');
    //Line break
    $this->Ln(20);
    
}

//Page footer
function Footer()
{
    //Position at 1.5 cm from bottom
    $this->SetY(-15);
    //Arial italic 8
    $this->SetFont('Arial','I',8);
    //Page number
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}
}

//Instanciation of inherited class
$pdf=new PDF();
$pdf->SetLeftMargin(20);
$pdf->SetRightMargin(20);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','',8);
$pdf->Cell(5,5,iconv('UTF-8', 'windows-1252','NOME: '.$questionario->investigador->nome),0,1);
$pdf->Cell(5,5,iconv('UTF-8', 'windows-1252','NUMMEC: '.$questionario->investigador->nummec),0,1);
$pdf->Cell(5,5,iconv('UTF-8', 'windows-1252','FREGUESIANAT: '.$questionario->investigador->freguesianat),0,1);
$pdf->Cell(5,5,iconv('UTF-8', 'windows-1252','CONCELHONAT: '.$questionario->investigador->concelhonat),0,1);
$pdf->Cell(5,5,iconv('UTF-8', 'windows-1252','PAISNAT: '.$questionario->investigador->paisnat),0,1);
$pdf->Cell(5,5,iconv('UTF-8', 'windows-1252','DATANASC: '.$questionario->investigador->datanasc),0,1);
$pdf->Cell(5,5,iconv('UTF-8', 'windows-1252','MORADA: '.$questionario->investigador->morada),0,1);
$pdf->Cell(5,5,iconv('UTF-8', 'windows-1252','EMAIL: '.$questionario->investigador->email),0,1);
$pdf->Cell(5,5,iconv('UTF-8', 'windows-1252','EMAIL_ALTERNATIVO: '.$questionario->investigador->email_alternativo),0,1);
$pdf->Cell(5,5,iconv('UTF-8', 'windows-1252','TELEFONE: '.$questionario->investigador->telefone),0,1);
$pdf->Cell(5,5,iconv('UTF-8', 'windows-1252','TELEMOVEL: '.$questionario->investigador->telemovel),0,1);
$pdf->Cell(5,5,iconv('UTF-8', 'windows-1252','EXTENSAO: '.$questionario->investigador->extensao),0,1);
$pdf->Cell(5,5,iconv('UTF-8', 'windows-1252','LOGIN: '.$questionario->investigador->login),0,1);
$pdf->Cell(5,5,iconv('UTF-8', 'windows-1252','SERVICO: '.$questionario->investigador->servico),0,1);
$pdf->Cell(5,5,iconv('UTF-8', 'windows-1252','ESTADO: '.$questionario->investigador->estado),0,1);
$pdf->Cell(5,5,iconv('UTF-8', 'windows-1252','DATASUBMISSAO: '.$questionario->investigador->datasubmissao),0,1);

include "../forms/habilitacoesPDF.php";


/*$pdf->Cell(0,10,iconv('UTF-8', 'windows-1252','declaro para os devidos efeitos que foi autorizada a realiza√ß√£o de uma '.getLookup($user->tipoProjecto)),0,1);  
$pdf->Cell(0,10,iconv('UTF-8', 'windows-1252','intitulada \''.$user->tituloProjecto.'\''),0,1);  
$pdf->Cell(0,10,iconv('UTF-8', 'windows-1252','a realizar sob orienta√ß√£o de '.$user->nome_o),0,1); 

if($user->nome_co!=""){
	$pdf->Cell(0,10,iconv('UTF-8', 'windows-1252','e co-orienta√ß√£o de'.$user->nome_co),0,1);
} 
$pdf->Cell(0,10,iconv('UTF-8', 'windows-1252',''),0,1); 
$pdf->Cell(0,10,iconv('UTF-8', 'windows-1252','Data: '.date('d / m / Y')),0,1); 
$pdf->Cell(0,10,iconv('UTF-8', 'windows-1252','Data: '.$user->enviado),0,1); 
$pdf->Cell(0,10,iconv('UTF-8', 'windows-1252',''),0,1); 
$pdf->Cell(0,10,iconv('UTF-8', 'windows-1252','Assinatura do(a) Estudante:'),0,1); 
$pdf->Cell(0,10,iconv('UTF-8', 'windows-1252','Assinatura do(a) Orientador(a):'),0,1);
if($user->nome_co!=""){ 
$pdf->Cell(0,10,iconv('UTF-8', 'windows-1252','Assinatura do(a) Co-Orientador(a):'),0,1);
}
if($user->temaProjecto=='1'){
	$pdf->Cell(0,10,iconv('UTF-8', 'windows-1252','Assinatura do(a) Director(a) de Servi√ßo/Departamento da FMUP:'),0,1);
} */

$pdf->Output();

function getLookup($id){
   		$db=new Database();
   		$dataP=$db->getTipoTese($id);
	   	$row = mysql_fetch_assoc($dataP);
	   	if($row){
			$typeP=$row["descr"];
			return $typeP;		
	   	}else{
	   		return null;
	   	}
}


?>