<?php
/**
Autor: Paulo Rodrigues (prodrigues@med.up.pt; paulomiguelarodrigues@gmail.com)
Data: 03/04/2014
Uso: Validação de repetidos nas arbitragens a revistas inseridas no sistema.
*/
function validateRepetidosConferencias($conferencias)
{
	$conferencias2 = $conferencias;
	$final=array();
	
	while ( list($key, $val) = each($conferencias) ) {	
		$final[$key]=array();
		
		while (list($key2, $val2) = each($conferencias2)) {

			$equal = equalsConferencias($val,$val2);
			if ($equal)
			{
				$final[$key][]=$conferencias2[$key2];	  
				unset($conferencias2[$key2]);				  
				unset($conferencias[$key2]);
				continue;
			}	

			$cmpTipo = strcmp($val->tipo, $val2->tipo);	
			$cmpDataIni = strcmp($val->datainicio, $val2->datainicio);				
			
			similar_text(strtolower($val->titulo), strtolower($val2->titulo), $levTitulo);	
			if($levTitulo >= 85 && $cmpTipo == 0)
			{						
				$final[$key][]=$conferencias2[$key2];	  
				unset($conferencias2[$key2]);				  
				unset($conferencias[$key2]);
				continue;
			}	
			
			//tipo && datas && local
			if($cmpTipo == 0 && $cmpDataIni == 0 && strcmp($val->datafim, $val2->datafim) == 0)
			{
				$final[$key][]=$conferencias2[$key2];	  
				unset($conferencias2[$key2]);				  
				unset($conferencias[$key2]);
				continue;
			}
		}
		unset($conferencias2[$key]);
		reset($conferencias2);
	}

	$db = new Database();
	$db->executeQuery("TRUNCATE TABLE repetidos_cnf");	
	
	foreach ($final as $i => $value) {	
		if( count($final[$i]) == 1) {
			$db->executeQuery("INSERT INTO repetidos_cnf (id_original) VALUES (".$final[$i][0]->id.")");
		} else {		
			$j = 0;
			for ($j = $j + 1; $j < count($final[$i]); $j++ ) {	
				$sql = "INSERT INTO repetidos_cnf (id_original, id_repetido) VALUES (".$final[$i][0]->id.",".$final[$i][$j]->id.");";
				$db->executeQuery($sql);
			}
		}
	}
}


/**
Autor: Paulo Rodrigues (prodrigues@med.up.pt; paulomiguelarodrigues@gmail.com)
Data: 07/04/2014
Uso: Verifica se 2 Conferencias são iguais.
*/

function equalsConferencias($p1, $p2)
{
    if( $p1->ambito == $p2->ambito &&
        $p1->tipo == $p2->tipo &&
        $p1->datainicio == $p2->datainicio &&
        $p1->datafim == $p2->datafim &&
        $p1->titulo == $p2->titulo &&
        $p1->local == $p2->local &&
		$p1->participantes == $p2->participantes)
    {
        return true;
    }
    else
    {
        return false;
    }
}

?>