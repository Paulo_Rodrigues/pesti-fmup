<?php 
require_once('../classlib/Database.class.inc');

if(isset($_REQUEST['funcao'])){
	$funcao = $_REQUEST['funcao'];
	$db = new Database();
	$db->connect();
	$relatorio = array();
	$fmup = array();
	//funcoes que necessitam id da publicacao e um novo valor
	if(isset($_POST['idPub']) && isset($_POST['novoValor'])){

		$id = $_POST['idPub'];
		$novoValor= $_POST['novoValor'];
		if ($funcao == "TipoFMUP" && isset($_POST["antValor"])){
			mudarTipoFMUP($id, $novoValor,$_POST["antValor"]); 
		}/**/
		
		if($funcao == "TipoRel"){
			mudarTipoRel($id,$novoValor);
		}
	}
	if($funcao == "atualizaMenu" && isset($_REQUEST["menu"])){
            atualizaMenu($_REQUEST["menu"]);
	}
	if($funcao == "atualizaSubMenu" && isset($_REQUEST["submenu"]) 
			&& isset($_REQUEST["menu"])){
		atualizaSubMenu($_REQUEST["menu"], $_REQUEST["submenu"]);
	}
	//outras funcoes
	if ($funcao == "atualizaPublicacoes" ){
		atualizaPublicacoes();
	}
	if ($funcao == "getTipoRelatorio"){
		getTipoRelatorio();
	}
	if($funcao=="inferir"){
		infereTipoRel(true);
	}
	if($funcao=="inferir_naocorrigidas"){
		infereTipoRel(false);
	}
	$db->disconnect();/**/
} 

/**
 * Atualiza a coluna tipoFMUP de uma publicação
 * @author Catia Marques (cam@med.up.pt, catia.abmarques@gmail.com)
 * @param $id id da publicação
 * @param $novoValor novo valor que a coluna toma
 * @return novo valor de tipo do relatório
 * */
function mudarTipoFMUP($id, $novoValor, $antValor){	
	global $db;
	$db->updateTipoFMUP($id,$novoValor);
	//mudar tipo_relatorio
	$novo_rel = $db->atualizaTipoRelatorio($id, $antValor);
	publicacaoCorrigida($id);
	echo $novo_rel;
}

/**
 * Atualiza a coluna relativa ao tipo relatório de uma publicação
 * @author Catia Marques (cam@med.up.pt, catia.abmarques@gmail.com)
 * @param $id id da publicação
 * @param $novoValor novo valor que a coluna toma
 * */
function mudarTipoRel($id, $novoValor){
	global $db;
	
	$db->updateTipoRel($id, $novoValor);
	publicacaoCorrigida($id);
}

/**
 * Define publicação como corrigida
 * @author Catia Marques (cam@med.up.pt, catia.abmarques@gmail.com)
 * @param $id id da publicação
 * */
function publicacaoCorrigida($id){
	global $db;
	$db->atualizaPublicacaoCorrigida($id,1);
}

/**
 * Lista de Tipos FMUP (option tag) - Função auxiliar 
 * @author Catia Marques (cam@med.up.pt, catia.abmarques@gmail.com)
 * */
function getTipoFMUP(){
	global $db, $fmup;
	//buscar tipos fmup que existem para drop down list
	$resTipos = $db->getTiposFMUP();
	$tiposFMUP = "";
	while($row = mysql_fetch_assoc($resTipos)){
		$tiposFMUP .= "<option value='".$row['tipofmup']."'>".$row['tipofmup']."</option>";
		$fmup[] = $row['tipofmup'];
	}
	return $tiposFMUP;
}

/**
 * Lista de Tipos Relatório (option tag) - Função auxiliar
 * @author Catia Marques (cam@med.up.pt, catia.abmarques@gmail.com)
 * */
function getTipoRelatorio(){	
	global $db;
	//buscar tipos relatorio que existem para radio options
	$resTipos = $db->getTiposRelatorio();
	$tiposRel = "";
	global $relatorio;
	while($row = mysql_fetch_assoc($resTipos)){
		$tiposRel .= "<option value='".$row['tipo_relatorio']."'>".$row['tipo_relatorio']."</option>";
		$relatorio[$row["tipo_relatorio"]] = $row["Soma"];
	}
	return $tiposRel;
}

/**
 * Listagem de publicações dividida em secções conforme o tipo Relatório
 * @author Catia Marques (cam@med.up.pt, catia.abmarques@gmail.com)
 * */
function atualizaPublicacoes(){//menu, tiporelatorio
	global $relatorio, $fmup, $db;
	$tiposRel = getTipoRelatorio();
	
	$num = 0;
	$rel = array_keys($relatorio);
	foreach($rel as $tipo){
		$num = $relatorio[$tipo];
		//criar acordion para tipo relatorio desconhecido
		echo "<h3 id='$tipo'>Publicações do Tipo $tipo "
                        . "(<div style='display:inline'>".$num."</div>)</h3>";
		echo "<div id='publicacoes-$tipo'></div>";//submenus
	}
}

/**
 * Listagem de acordions (submenus) de tipos fmup de publicações 
 * com um tipo relatório definido
 * @param string $tiporel tipo relatório associado a publicações
 */
function atualizaMenu($tiporel){
    global $db, $fmup;
    getTipoFMUP();
    foreach($fmup as $id=>$tipof){
        $listaRelFmup =$db->getPublicacoesTipoREL($tiporel,$tipof);
        if(mysql_num_rows($listaRelFmup)>0){//existem publicações a analisar
              echo "<h4 id='$tipof'>$tipof</h4>"
                      . "<div id='publicacoes-$tipof'></div>";
        }
    }
}

/**
 * Listagem de submenu para cada tipo de publicação do relatório
 * @author Catia Marques (cam@med.up.pt, catia.abmarques@gmail.com)
 * */
function atualizaSubMenu($tiporel,$tipofmup){
    //listar publicações dentro de div de acordion
    global $db,$fmup;
    $columns="<thead style='border-bottom: 7px solid #FFD700;'>
            <tr><th>ID da Publicação</th>
                <th>ID Inv</th><th>Nome Publicacao</th><th>Título</th>
                <th>ISSN</th><th>ISBN</th><th>Volume</th>
                <th>Issue</th><th>Páginas</th><th>Serviço</th>
                <th>Tipo FMUP</th><th width='50'>Tipo Relatório</th>
            </tr>
        </thead>";
    //buscar lista de Publicações
    $respub =$db->getPublicacoesTipoREL($tiporel,$tipofmup);
    $tipofmup = getTipoFMUP();
    $tiporel = getTipoRelatorio();
    $pub = array();
    while ($row = mysql_fetch_assoc($respub)){
        array_push($pub,$row);
    }
    if(!empty($pub)){
            echo "<table class='box-table-b' style='table-layout:fixed;width:100%'>";
            echo $columns;
            echo "<tbody>";
            foreach ($pub as $column => $value){
                if($value["CORRIGIDO"] == 0){
                        echo "<tr id='id" .$value["ID"]. "'>";
                }else{
                        echo "<tr id='id" .$value["ID"]. "' class='publicacaoCorrigida' >";
                }
                //echo "<tr id='id" .$value["ID"]. "'>";
                echo "<td>".$value["ID"]."</td>";
                echo "<td>".$value["IDINV"]."</td>";
                echo "<td>".$value["NOMEPUBLICACAO"]."</td>";
                echo "<td>".$value["TITULO"]."</td>";
                echo "<td>".$value["ISSN"]."</td>";
                echo "<td>".$value["ISBN"]."</td>";
                echo "<td>".$value["VOLUME"]."</td>";
                echo "<td>".$value["ISSUE"]."</td>";
                echo "<td>";
                if(!empty($value["PRIMPAGINA"]) & !empty($value["ULTPAGINA"])){
                        echo $value["PRIMPAGINA"]."-".$value["ULTPAGINA"];
                }else {
                    if( !empty($value["PRIMPAGINA"]) || !empty($value["ULTPAGINA"]) ){
                        echo $value["PRIMPAGINA"]."".$value["ULTPAGINA"];
                    }
                }
                echo "</td>";
                echo "<td>".$value["servico"]."</td>";
                echo "<td><select id = 'tiposfmup_".$value["ID"]."' value='".$value["TIPOFMUP"]."' "
                        . "onchange='getSelectedFMUP(this)'>$tipofmup</select></td>";
                echo "<td><select id = 'tiposrel_".$value["ID"]."' value='".$value["TIPO_REL"]."' "
                        . "onchange='getSelectedRel(this)'>$tiporel</td>";
                echo "</tr>";
            }
            echo "</tbody></table>";
    }
}

/**
 * Inferir tipo do relatório de todas as publicações
 * @author Catia Marques (cam@med.up.pt, catia.abmarques@gmail.com)
 * @param boolean $todas se false, infere tipo relatório de apenas publicações não corrigidas
 * */
function infereTipoRel($todas){
	global $db;
	if($todas){
		$db->infereTipoRelatorio();
	}else{
		$db->infereTipoRelatorioNCorr();
	}
}

