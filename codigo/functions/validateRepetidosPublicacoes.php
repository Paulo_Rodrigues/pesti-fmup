﻿<?php

/**
Autor: Paulo Rodrigues (prodrigues@med.up.pt; paulomiguelarodrigues@gmail.com)
Data: 02/04/2014
Uso: Validação de repetidos nas publicações inseridas no sistema.
*/
function validateRepetidosPublicacoes($dadosDep)
{
	$repetidos = array();
	
	// PublicacoesISI
	$repetidos[0] = array();
	$repetidos[0] = validaRepetidosPublicacoes(array_merge($dadosDep->publicacoesISI,$dadosDep->publicacoesPUBMED,$dadosDep->publicacoesMANUALInternacional, $dadosDep->publicacoesMANUALNacional));	
			
	//Patentes Manuais	
	$repetidos[1] = array();
	$repetidos[1] = validaRepetidosManualPatentes($dadosDep->publicacoesMANUALPatentes);	
	
	$db = new Database();
	$db->executeQuery("TRUNCATE TABLE repetidos_pub");	
	
	foreach ($repetidos[0] as $i => $value) {	
		if( count($repetidos[0][$i]) == 1) {
			$db->executeQuery("INSERT INTO repetidos_pub (id_original, tipofmup) VALUES (".$repetidos[0][$i][0]->id.",'".$repetidos[0][$i][0]->tipofmup."')");
		} else {		
			$j = 0;
			for ($j = $j + 1; $j < count($repetidos[0][$i]); $j++ ) {	
				$sql = "INSERT INTO repetidos_pub (id_original, id_repetido, tipofmup) VALUES (".$repetidos[0][$i][0]->id.",".$repetidos[0][$i][$j]->id.",'".$repetidos[0][$i][0]->tipofmup."');";
				$db->executeQuery($sql);
			}
		}
	}	
	
	foreach ($repetidos[1] as $i => $value) {	
		if(count($repetidos[1][$i]) == 1) {
			$db->executeQuery("INSERT INTO repetidos_pub (id_original, tipofmup, tipo) VALUES (".$repetidos[1][$i][0]->id.",'".$repetidos[1][$i][0]->tipofmup."','PAT')");
		} else {
			$j = 0;
			for ($j = $j + 1; $j < count($repetidos[1][$i]); $j++ ) {
				$db->executeQuery("INSERT INTO repetidos_pub (id_original, id_repetido, tipofmup, tipo) VALUES (".$repetidos[1][$i][0]->id.",".$repetidos[1][$i][$j]->id.",'".$repetidos[1][$i][0]->tipofmup."','PAT')");
			}
		}
	}	
}

/**
Autor: Paulo Rodrigues (prodrigues@med.up.pt; paulomiguelarodrigues@gmail.com)
Data: 02/04/2014
Uso: Valida as patentes que são introduzidas manualmente pelo utilizador
	Nesta função, é utilizada uma função de cálculo de métrica de strings, chamada levenshtein, 
	mais info: http://www.php.net/manual/en/function.levenshtein.php
*/
	
function validaRepetidosManualPatentes($info)
{
	$info2 = $info;
	$final = array();
	while ( list($key, $val) = each($info) ) {			
		$final[$key]=array();
		while (list($key2, $val2) = each($info2)) {
			
			$equal = equalsPublicacao($val,$val2);	
			
			if ($equal)
			{											
				$final[$key][]=$info2[$key2];	  
				unset($info2[$key2]);				  
				unset($info[$key2]);
				continue;
			}

			//codigo 2 caracteres diferentes
			similar_text(strtolower($val->titulo), strtolower($val2->titulo), $levTitulo);	
						
			if($levTitulo >= 90)
			{		
				$final[$key][]=$info2[$key2];	  
				unset($info2[$key2]);				  
				unset($info[$key2]);
				continue;
			}
			
			//numpatente
			$n1 = $val->npatente;
			$n2 = $val2->npatente;

			//Se o número das patentes for igual, é provável que sejam repetidos...
			if (strcmp($n1,$n2) == 0)
			{
				$final[$key][]=$info2[$key2];	  
				unset($info2[$key2]);				  
				unset($info[$key2]);
				continue;
			}
				
			//Investigar mais a fundo se podem ser repetidos...
			if ($levTitulo >= 70)
			{				
				//data
				$d1 = $val->datapatente;
				$d2 = $val2->datapatente;
				
				$levDataPatente = levenshtein($d1, $d2);
				
				if($levDataPatente >= 0 && $levDataPatente <= 5){				
					$final[$key][]=$info2[$key2];	  
					unset($info2[$key2]);				  
					unset($info[$key2]);
					continue;
				}				
			}							
					
		}		
		unset($info2[$key]);
		reset($info2);
	}	
	return $final;	
}

/**
Autor: Paulo Rodrigues (prodrigues@med.up.pt; paulomiguelarodrigues@gmail.com)
Data: 02/04/2014
Uso: Valida as Publicações Internacionais que são introduzidas manualmente. Nesta função é igualmente utilizada a função levenshtein.
*/
function validaRepetidosPublicacoes($info)
{
	$final=array();
	$info2 = array();
	$info2 = $info;	
	
	while ( list($key, $val) = each($info) ) {			
		$final[$key]=array();
		while (list($key2, $val2) = each($info2)) {
		
			if($val->tipofmup == $val2->tipofmup) {
			
				$equal = equalsPublicacao($val,$val2);	
				
				if ($equal)
				{											
					$final[$key][]=$info2[$key2];	  
					unset($info2[$key2]);				  
					unset($info[$key2]);
					continue;
				}
				
				//Validacoes para tentar reduzir o tempo de execucao usar diferentes funcoes
				$levNome = levenshtein(strtolower($val->nomepublicacao), strtolower($val2->nomepublicacao));	
				similar_text(strtolower($val->titulo), strtolower($val2->titulo), $levTitulo);						
																					
				$levIssue = levenshtein(strtolower($val->issue), strtolower($val2->issue));				
				$levPrimPagina = levenshtein(strtolower($val->primpagina), strtolower($val2->primpagina));
				
				if($levNome <= 5 && $levTitulo >= 85 && $levIssue <= 2 && $levPrimPagina <= 2)
				{	
					$final[$key][]=$info2[$key2];	  
					unset($info2[$key2]);				  
					unset($info[$key2]);
					continue;
				}	

				if($levTitulo >= 80 && $levNome <= 5)
				{
					$final[$key][]=$info2[$key2];	  
					unset($info2[$key2]);				  
					unset($info[$key2]);
					continue;
				}	
					
				if($levTitulo >= 75)
				{
					$final[$key][]=$info2[$key2];	  
					unset($info2[$key2]);				  
					unset($info[$key2]);
					continue;
				}					
									
				if($levTitulo >= 80 && $levNome <= 5 && $levIssue <= 1 && $levPrimPagina <= 1 )
				{
					$final[$key][]=$info2[$key2];	  
					unset($info2[$key2]);				  
					unset($info[$key2]);
					continue;
				}	

				if($levTitulo >= 85 && $levIssue <= 1 && $levPrimPagina <= 1)
				{
					$final[$key][]=$info2[$key2];	  
					unset($info2[$key2]);				  
					unset($info[$key2]);
					continue;
				}	

				if($levNome >= 90 && $levIssue <= 1 && $levPrimPagina <= 1)
				{
					$final[$key][]=$info2[$key2];	  
					unset($info2[$key2]);				  
					unset($info[$key2]);
					continue;
				}	
			}		
		}
		
		unset($info2[$key]);
		reset($info2);
	}
	
	return $final;	
}

/**
Autor: Paulo Rodrigues (prodrigues@med.up.pt; paulomiguelarodrigues@gmail.com)
Data: 07/04/2014
Uso: Verifica se 2 publicações são iguais.
*/

function equalsPublicacao($p1, $p2)
    {
        if( $p1->titulo == $p2->titulo &&
            $p1->volume == $p2->volume &&
            $p1->autores == $p2->autores &&
            $p1->primpagina == $p2->primpagina &&
            $p1->ultpagina == $p2->ultpagina &&
            $p1->ultpagina == $p2->ultpagina &&
            $p1->issn == $p2->issn)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
?>