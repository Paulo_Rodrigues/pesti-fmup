<?php

/**
 * Parsing de refer?ncias.
 * 
 * Este script, faz parsing de refer?ncias. 
 * 
 * @author Jos? Filipe Lopes Santos <jfilipe@med.up.pt>
 * @since 29-12-2011
 * @version 1.0 - data da ?lt. actualiza??o: 29-12-2011
 * @package imp_endnote
 * @subpackage outputs
 */

require_once '../classlib/core.inc';

require_once '../classlib/Database.class.inc';

/* ------------------------------ oOo ----------------------------
					Inicializa??o de vari?veis
   ------------------------------ oOo ---------------------------- */


function parsefileEndnoteExport(){

global $questionario;
	
$target_path = "../pubfiles/".$questionario->investigador->id;

if(!file_exists ( $target_path)){
	mkdir($target_path);
}


$target_path = $target_path ."/". basename( $questionario->investigador->id."_".$questionario->investigador->login."_ENDNOTE_".$_FILES['uploadedfileENDNOTE']['name']."_".date("dmY_H:i")); 

if(move_uploaded_file($_FILES['uploadedfileENDNOTE']['tmp_name'], $target_path)) {
    //echo "The file ".  basename( $_FILES['uploadedfileENDNOTE']['name'])." has been uploaded";
} else{
    echo "There was an error uploading the file, please try again!";
}

if (empty($error_msg)) $error_msg = ""; // mensagem de erro
$linhas = array(); // array com as linhas do ficheiro
$refs = array(); // array com os dados das refer?ncias


/* ------------------------------ oOo ----------------------------
					Obter as refer?ncias do ficheiro
   ------------------------------ oOo ---------------------------- */

// obter os dados do ficheiro
//$linhas = file(UPLOAD_FILES_PATH.$file) or die("N?o ? poss?vel abrir o ficheiro ".UPLOAD_FILES_PATH.$file);
$linhas = file($target_path) or die("N?o ? poss?vel abrir o ficheiro ".$target_path);

/* ------------------------------ oOo ----------------------------
						Parsing das refer?ncias
   ------------------------------ oOo ---------------------------- */

$ref = ""; // string com a referÍncia
$pubmed_id = ""; // id do pubmed
$issn = ""; // n.∫ de sÈrie (ISSN)
$issn_print = ""; // n.∫ de sÈrie (ISSN)
$issn_linking = ""; // n.∫ de sÈrie (ISSN)
$issn_electronic = ""; // n.∫ de sÈrie (ISSN)
$isbn="";
$volume = ""; // volume
$issue="";
$date = ""; // data de publicaÁ„o
$titulo = ""; // tÌtulo
$primpagina = ""; // p·ginas
$abstract = ""; // abstract
$afiliacao = ""; // afiliaÁ„o
$autores = ""; // string com os autores
$autores_abrev = ""; // string com os autores abreviados
$tipo = ""; // tipo de publicaÁ„o
$revista = ""; // tÌtulo da revista 
$date_creation = ""; // data da criaÁ„o
$doi = ""; // digital object identifier
$source  = ""; // fonte
$mesh = ""; // termos do Mesh
$ar="";
$conftitle="";
$language="";
$colecao="";
$ultpagina="";
$npatente="";
$datapatente="";
$citacoes="";
$ipc="";
$area="";


$count = 0; // contador de referÍncias
$append = array(); // array de booleanos que indicam se È para concatenar
$append["titulo"] = false;
$append["abstract"] = false;
$append["afiliacao"] = false;

foreach ($linhas as $index=>$linha){
	
	
if (ereg("^(PT )([a-zA-Z]+)",$linha,$regs)){ // in?cio da refer?ncia
	$append["global"] = true;
	$type = $regs[2];
	return "FICHEIRO ERRADO";
}	
	
if (ereg("^(PMID)([- ]+)([0-9]+)",$linha,$regs)){ // inÌcio da referÍncia
		
		// fechar referÍncia anterior se existir
		if ($pubmed_id != ""){
			

			/*$refs[$count]["num_serie"] = $num_serie;
			$refs[$count]["num_serie_print"] = $num_serie_print;
			$refs[$count]["num_serie_linking"] = $num_serie_linking;
			$refs[$count]["num_serie_electronic"] = $num_serie_electronic;
					
			$refs[$count]["date"] = $date;			

			$refs[$count]["pags"] = $pags;
			$refs[$count]["abstract"] = $abstract;
			$refs[$count]["afiliacao"] = $afiliacao;			
	
			$refs[$count]["autores_abrev"] = $autores_abrev;

			$refs[$count]["revista"] = $revista;
			$refs[$count]["date_creation"] = $date_creation;
			$refs[$count]["doi"] = $doi;
			$refs[$count]["source"] = $source;
			$refs[$count]["mesh"] = $mesh;*/

			// guardar dados
		$refs[$count]["pubmed_id"] = $pubmed_id;
		$refs[$count]["ref"] = $ref;
		$refs[$count]["tipo"] = $tipo;
		$refs[$count]["nomepublicacao"] = $nomepublicacao;	
		$refs[$count]["issn"] = $issn;
		$refs[$count]["issn_linking"] = $issn_linking;
		$refs[$count]["issn_electronic"] = $issn_electronic;
		$refs[$count]["isbn"] = $isbn;
		$refs[$count]["titulo"] = $titulo;
		$refs[$count]["autores"] = $autores;	
		$refs[$count]["volume"] = $volume;
		$refs[$count]["issue"] = $issue;
		$refs[$count]["ar"] = $ar;	
		$refs[$count]["colecao"] = $colecao;	
		$refs[$count]["primpagina"] = $primpagina;
		$refs[$count]["ultpagina"] = $ultpagina;
		$refs[$count]["ano"] = $ano;
		$refs[$count]["citacoes"] = $citacoes;
		$refs[$count]["npatente"] = $npatente;
		$refs[$count]["datapatente"] = $datapatente;
		$refs[$count]["ipc"] = $ipc;
		$refs[$count]["language"] = $language;
		$refs[$count]["area"] = $area;			
			
			
			
			
			// incrementar o contador
			$count++;
			
			// inicializar vari·veis
				$tipo="";
				$nomepublicacao="";
				$issn="";
				$issn_linking="";
				$issn_electronic="";
				$isbn="";
				$titulo="";
				$autores="";
				$volume="";
				$issue="";
				$ar="";
				$colecao="";
				$primpagina="";
				$ultpagina="";
				$ifactor="";
				$ano="";
				$citacoes="";
				$npatente="";
				$datapatente="";
				$ipc="";
				$area="";
			
			$append["titulo"] = false;
			$append["abstract"] = false;
			$append["afiliacao"] = false;
		}
		
		// abrir nova referÍncia
		$pubmed_id = $regs[3];
		continue;
	}

	
	// concatenar linhas da referÍncia
	$ref .= $linha;
		
	////// TÌtulo //////
	if (ereg("^(TI)([- ]+)([a-zA-Z0-9]+.*)",$linha,$regs)){ // inÌcio da tag
		$titulo = trim($regs[3]);
		$append["titulo"] = true;
		continue;
	}
	if (ereg("^[A-Z]{2,4} ",$linha)) // se encontrar outra tag p·ra de concatenar
		$append["titulo"] = false;
	if ($append["titulo"])
		$titulo .= " ".trim($linha);
	
	////// Abstract //////
	if (ereg("^(AB)([- ]+)([a-zA-Z0-9]+.*)",$linha,$regs)){ 
		$abstract = trim($regs[3]);
		$append["abstract"] = true;
		continue;
	}
	if (ereg("^[A-Z]{2,4} ",$linha))
		$append["abstract"] = false;
	if ($append["abstract"])
		$abstract .= " ".trim($linha);

	////// AfiliaÁ„o //////
	if (ereg("^(AD)([- ]+)([a-zA-Z0-9]+.*)",$linha,$regs)){
		$afiliacao = trim($regs[3]);
		$append["afiliacao"] = true;
		continue;
	}
	if (ereg("^[A-Z]{2,4} ",$linha))
		$append["afiliacao"] = false;
	if ($append["afiliacao"])
		$afiliacao .= " ".trim($linha);
		
	///// Autores /////
	if (ereg("^(FAU)([- ]+)([a-zA-Z0-9]+.*)",$linha,$regs))
		($autores == "") ? $autores = $regs[3] : $autores .= " ; ".$regs[3];		

	///// autores abreviados /////
	if (ereg("^(AU)([- ]+)([a-zA-Z0-9]+.*)",$linha,$regs))
		($autores_abrev == "") ? $autores_abrev = $regs[3] : $autores_abrev .= " ; ".$regs[3];				
		
	///// N.∫ de sÈrie /////
	   if (ereg("^(IS)([- ]+)([a-zA-Z0-9]+.*)",$linha,$regs)){
        ($issn == "") ? $issn = $regs[3] : $issn .= " ; ".$regs[3];
        
        if (ereg("([0-9]+[-]?[0-9]+)([ ]{1})(\\(Print\\))", $regs[3], $regs2)){
            $issn_print = $regs2[1];
        }
	    if (ereg("([0-9]+[-]?[0-9]+)([ ]{1})(\\(Electronic\\))", $regs[3], $regs2)){
            $issn_electronic = $regs2[1];
        }
	    if (ereg("([0-9]+[-]?[0-9]+)([ ]{1})(\\(Linking\\))", $regs[3], $regs2)){
            $issn_linking = $regs2[1];
        }

    }   
	///// DOI - Digital Object Identifier /////
	if (ereg("^(AID)([- ]+)([a-zA-Z0-9]+.*)",$linha,$regs))
		($doi == "") ? $doi = $regs[3] : $doi .= " ; ".$regs[3];				

	///// Termos do Mesh /////
	if (ereg("^(MH)([- ]+)(\\*?[a-zA-Z0-9]+.*)",$linha,$regs))
		($mesh == "") ? $mesh = $regs[3] : $mesh .= " ; ".$regs[3];				
		
	///// Volume /////
	if (ereg("^(VI)([- ]+)([a-zA-Z0-9]+.*)",$linha,$regs))
		$volume = $regs[3];

	///// Data da publicação /////
	if (ereg("^(DP)([- ]+)([a-zA-Z0-9]+.*)",$linha,$regs))
		$ano = $regs[3];

	///// Páginas /////		
	if (ereg("^(PG)([- ]+)([a-zA-Z0-9]+.*)",$linha,$regs))
		$primpagina = $regs[3];
		
	///// Tipo de publicaÁ„o /////
	if (ereg("^(PT)([- ]+)([a-zA-Z0-9]+.*)",$linha,$regs))
		($tipo == "") ? $tipo = $regs[3] : $tipo .= " ; ".$regs[3];		
		
	///// TÌtulo da revista /////		
	if (ereg("^(JT)([- ]+)([a-zA-Z0-9]+.*)",$linha,$regs))
		$nomepublicacao = $regs[3];
		
	///// Data de criaÁ„o /////		
	if (ereg("^(CRDT)([- ]+)([a-zA-Z0-9]+.*)",$linha,$regs))
		$date_creation = $regs[3];
		
	///// Fonte /////		
	if (ereg("^(SO)([- ]+)([a-zA-Z0-9]+.*)",$linha,$regs))
		$source = $regs[3];
}

if (count($linhas) > 1){ // se tiver registos
	
	// guardar dados do ˙ltimo registo
	/*$refs[$count]["pubmed_id"] = $pubmed_id;
	$refs[$count]["ref"] = $ref;
	$refs[$count]["num_serie"] = $num_serie;
	$refs[$count]["num_serie_print"] = $num_serie_print;
	$refs[$count]["num_serie_linking"] = $num_serie_linking;
	$refs[$count]["num_serie_electronic"] = $num_serie_electronic;
	$refs[$count]["volume"] = $volume;	
	$refs[$count]["date"] = $date;
	$refs[$count]["titulo"] = $titulo;	
	$refs[$count]["pags"] = $pags;
	$refs[$count]["abstract"] = $abstract;
	$refs[$count]["afiliacao"] = $afiliacao;	
	$refs[$count]["autores"] = $autores;	
	$refs[$count]["autores_abrev"] = $autores_abrev;
	$refs[$count]["type"] = $type;
	$refs[$count]["revista"] = $revista;
	$refs[$count]["date_creation"] = $date_creation;
	$refs[$count]["doi"] = $doi;
	$refs[$count]["source"] = $source;
	$refs[$count]["mesh"] = $mesh;*/
	$refs[$count]["pubmed_id"] = $pubmed_id;
	$refs[$count]["ref"] = $ref;
	$refs[$count]["tipo"] = $tipo;
	$refs[$count]["nomepublicacao"] = $nomepublicacao;	
	$refs[$count]["issn"] = $issn;
	$refs[$count]["issn_linking"] = $issn_linking;
	$refs[$count]["issn_electronic"] = $issn_electronic;
	$refs[$count]["isbn"] = $isbn;
	$refs[$count]["titulo"] = $titulo;
	$refs[$count]["autores"] = $autores;	
	$refs[$count]["volume"] = $volume;
	$refs[$count]["issue"] = $issue;
	$refs[$count]["ar"] = $ar;	
	$refs[$count]["colecao"] = $colecao;	
	$refs[$count]["primpagina"] = $primpagina;
	$refs[$count]["ultpagina"] = $ultpagina;
	$refs[$count]["ano"] = $ano;
	$refs[$count]["citacoes"] = $citacoes;
	$refs[$count]["npatente"] = $npatente;
	$refs[$count]["datapatente"] = $datapatente;
	$refs[$count]["ipc"] = $ipc;
	$refs[$count]["language"] = $language;
	$refs[$count]["area"] = $area;
}




$db = new Database();

$db->apagaRefsENDNOTE($questionario->investigador->id);



foreach ($refs as $index=>$info){
		$num = $index + 1;
		
		/*$db->insertUmaPublicacaoENDNOTE(
			$questionario->investigador->id,
			$type,
			str_replace("'", "", $info["autores"]),
			str_replace("'", "", $info["titulo"]),
			str_replace("'", "", $info["source"]),
			$info["volume"],
			$info["issue"],
			$info["special_issue"],
			$info["num_artigo"],
			$info["doi"],
			$info["date"],
			$info["ano"],
			$info["num_citacoes"],
			$info["total_citacoes"],
			$info["num_serie_print"],
			$info["num_serie_linking"],
			$info["num_serie_electronic"],
			$info["num_adesao"],
			$info["pags"],
			$info["pags"]);*/
		
		$paginas=explode("-",trim($info["primpagina"]));
		if(count($paginas)==1){
			$np=intval($paginas[0]);
		}else{
			$np=abs($paginas[1]-substr($paginas[0],-1*strlen($paginas[1])))+1;
		}
		
	
		if(stristr($info["tipo"],"JOURNAL ARTICLE") || stristr($info["tipo"],"LETTER")){
			if($np>2){
				$tipofmup="J";
			}else{
				$tipofmup="JA";
			}
		}else if (stristr($info["tipo"],"EDITORIAL") || stristr($info["tipo"],"NEWS")){
			if($np>2){
				$tipofmup="CP";
			}else{
				$tipofmup="OA";
			}
		}
		
		
		$db->insertUmaPublicacao(
			$questionario->investigador->id,
			$info["tipo"],
			str_replace("'", "", $info["nomepublicacao"]),
			$info["issn"],
			$info["issn_linking"],
			$info["issn_electronic"],
			$info["isbn"],
			str_replace("'", "", $info["titulo"]),
			str_replace("'", "", $info["autores"]),
			$info["volume"],
			$info["issue"],
			$info["ar"],
			$info["colecao"],
			$info["primpagina"],
			$info["primpagina"],
			"",
			$info["ano"],
			$info["citacoes"],
			$info["npatente"],
			$info["datapatente"],
			$info["ipc"],
			"","",
			$info["language"],
			"editor",
			$tipofmup,
			"0","1");
		
			
	
	}



}

?>

