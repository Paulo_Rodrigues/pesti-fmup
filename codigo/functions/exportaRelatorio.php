<?php 
ini_set('max_execution_time', 300);
require_once './../classlib/Database.class.inc';
require_once './../classlib/ExportacaoRelatorio.class.inc';


if(isset($_POST['login']) && isset($_POST['departamento']) && isset($_POST['anoLevantamento'])){
	
	$login = $_POST['login'];
	$dep = $_POST['departamento'];
	$anoLevantamento = $_POST['anoLevantamento'];

	date_default_timezone_set("Europe/Lisbon");
	
	if($dep == 0){
		$dep = "";
	}
	$helper = new exportacaoRelatorio();
	/** Inicial **/
	$helper->seccaoFMUP($dep);
	$helper->seccaoDocentes($dep);
	$helper->seccaoEstudantesInvestigacao($anoLevantamento,$dep);
	$helper->seccaoAgregacoes($anoLevantamento,$dep);
	
	$helper->seccaoProjetos($anoLevantamento,$dep);
	$helper->seccaoPublicacoes($dep);
	$helper->seccaoEditoresRevistas($dep);
	$helper->seccaoArbitragemCientifica($dep);
	$helper->seccaoJuris($dep);
	$helper->seccaoPremios($dep);
	$helper->seccaoConferencias($dep);
	$helper->seccaoApresentacoes($dep);
	$helper->seccaoOrientacoes($dep);
	$helper->seccaoUnidadesInvestigacao($dep);
	$helper->seccaoColaboracoesInternas($dep);
	$helper->seccaoColaboracoesExternas($dep);
	$helper->seccaoResearchNetworks($dep);
	$helper->seccaoMissoes($dep);
	
	$helper->seccaoDirecoesCurso($dep);
	$helper->seccaoDirecaoSociedadesCientificas($dep);
	
	$helper->seccaoProdutosServicos($dep);
	$helper->seccaoPatentes($dep);
	$helper->seccaoDivulgacaoCientifica($dep);
	$helper->seccaOutrasAtividades($dep);
	
	/** Anexo **/
	$helper->seccaoFMUPAnexo($dep);
	$helper->seccaoDocentesAnexo($dep);
	$helper->seccaoEstudantesInvestigacaoAnexo($anoLevantamento,$dep);
	$helper->seccaoAgregacoesAnexo($anoLevantamento,$dep);
	
	$helper->seccaoProjetosAnexo($anoLevantamento,$dep);
	$helper->seccaoPublicacoesArtigos($dep);
	$helper->seccaoPublicacoesErratas($dep);
	$helper->seccaoPublicacoesLivros($dep);
	$helper->seccaoPublicacoesProceedings($dep);
	$helper->seccaoPublicacoesOutrosSumarios($dep);
	$helper->seccaoPublicacoesSubmetidas($dep);
	$helper->seccaoEditoresRevistasAnexo($dep);
	$helper->seccaoArbitragemCientificaAnexo($dep);
	$helper->seccaoPremiosAnexo($dep);
	$helper->seccaoConferenciasAnexo($dep);
	$helper->seccaoOrientacoesAnexo($dep);
	$helper->seccaoColaboracoesExternasAnexo($dep);
	$helper->seccaoResearchNetworksAnexo($dep);
	$helper->seccaoMissoesAnexo($dep);
	
	$helper->seccaoDirecoesCursoAnexo($dep);
	$helper->seccaoDirecaoSociedadesCientificasAnexo($dep);
	
	$helper->seccaoProdutosServicosAnexo($dep);
	$helper->seccaoPatentesAnexo($dep);
	$helper->seccaoDivulgacaoCientificaAnexo($dep);
	//Sumula
	$helper->sumulaFinalDocentesOrcamento($anoLevantamento,$dep);
	$helper->sumulaFinal($anoLevantamento,$dep);
	
	/**********************************************************************************/
	if(!empty($dep)){
            $departamento = $helper->getDepartamento($dep);
            $departamento = str_replace(" ", ".", $departamento);
            $filename="Relatorio_a_".date('d/m/Y')."_".$departamento;
	}else{
		$filename="Relatorio_a_".date('d/m/Y');
	}
	$helper->closeConn();
	$helper->sendHeaders($filename, "Excel5");
}else{
	header("Location: ../admin.php");
}