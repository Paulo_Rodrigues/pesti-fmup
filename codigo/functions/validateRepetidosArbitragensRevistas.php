<?php

/**
Autor: Paulo Rodrigues (prodrigues@med.up.pt; paulomiguelarodrigues@gmail.com)
Data: 04/04/2014
Uso: Validação de repetidos nas arbitragens a revistas inseridas no sistema.
*/
function validateRepetidosArbitragensRevistas($arbRevista)
{
	$arbRevista2 = $arbRevista;
	$final=array();
	
	while ( list($key, $val) = each($arbRevista) ) {	
		$final[$key]=array();
		
		while (list($key2, $val2) = each($arbRevista2)) {
		
			if($val->tipo === $val2->tipo) {

				$equal = equalsArbitragensRevistas($val,$val2);
				if ($equal)
				{				
					$final[$key][]=$arbRevista2[$key2];	  
					unset($arbRevista2[$key2]);				  
					unset($arbRevista[$key2]);
					continue;				
				}

				//Nome
				similar_text(strtolower($val->tituloRevista), strtolower($val2->tituloRevista), $levTitulo);
				
				if(($levTitulo >= 80))
				{				
					$final[$key][]=$arbRevista2[$key2];	  
					unset($arbRevista2[$key2]);				  
					unset($arbRevista[$key2]);
					continue;			
				} 
				
				if (($levTitulo >= 75) && ( ($val->tipo == $val2->tipo) || ($val->issn == $val2->issn) ))
				{				
					$final[$key][]=$arbRevista2[$key2];	  
					unset($arbRevista2[$key2]);				  
					unset($arbRevista[$key2]);
					continue;			
				} 
				
				if(($levTitulo >= 70))
				{				
					$final[$key][]=$arbRevista2[$key2];	  
					unset($arbRevista2[$key2]);				  
					unset($arbRevista[$key2]);
					continue;			
				} 
			}
		}
		unset($arbRevista2[$key]);
		reset($arbRevista2);
	}

	$db = new Database();
	$db->executeQuery("TRUNCATE TABLE repetidos_arbRevista");	
	
	foreach ($final as $i => $value) {	
		if( count($final[$i]) == 1) {
			$db->executeQuery("INSERT INTO repetidos_arbRevista (id_original, tipo) VALUES (".$final[$i][0]->id.", ".$final[$i][0]->tipo.")");
		} else {		
			$j = 0;
			for ($j = $j + 1; $j < count($final[$i]); $j++ ) {	
				$sql = "INSERT INTO repetidos_arbRevista (id_original, id_repetido, tipo) VALUES (".$final[$i][0]->id.",".$final[$i][$j]->id.",".$final[$i][0]->tipo.");";
				$db->executeQuery($sql);
			}
		}
	}
}

/**
Autor: Paulo Rodrigues (prodrigues@med.up.pt; paulomiguelarodrigues@gmail.com)
Data: 07/04/2014
Uso: Verifica se 2 ArbitragensRevistas são iguais.
*/

function equalsArbitragensRevistas($p1, $p2)
{
    if( $p1->issn == $p2->issn &&
        $p1->numero == $p2->numero &&
        $p1->tipo == $p2->tipo &&
        $p1->link == $p2->link &&
        $p1->tituloRevista == $p2->tituloRevista)
    {
        return true;
    }
    else
    {
        return false;
    }
}

?>