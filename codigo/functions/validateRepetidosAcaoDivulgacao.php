<?php

function validateRepetidosAcaoDivulgacao($acoesDivulgacao)
{
	$acoesDivulgacao2 = $acoesDivulgacao;
	$final=array();
	
	while ( list($key, $val) = each($acoesDivulgacao) ) {	
		$final[$key]=array();
		
		while (list($key2, $val2) = each($acoesDivulgacao2)) {

			$equal = equalsAcaoDivulgacao($val,$val2);		
			if ($equal)
			{				
				$final[$key][]=$acoesDivulgacao2[$key2];	  
				unset($acoesDivulgacao2[$key2]);				  
				unset($acoesDivulgacao[$key2]);
				continue;				
			}

			similar_text(strtolower($val->titulo), strtolower($val2->titulo), $levTitulo);	
			
			$d1 = strtolower($val->data);
			$d2 = strtolower($val2->data);
			$levData = levenshtein($d1, $d2);
			
			if($levTitulo >= 85 && $levData <= 3)
			{						
				$final[$key][]=$acoesDivulgacao2[$key2];	  
				unset($acoesDivulgacao2[$key2]);				  
				unset($acoesDivulgacao[$key2]);
				continue;
			}

			if($levData <= 4 && $levTitulo >= 70)
			{				
				$final[$key][]=$acoesDivulgacao2[$key2];	  
				unset($acoesDivulgacao2[$key2]);				  
				unset($acoesDivulgacao[$key2]);
				continue;			
			} 		
			
			if($levTitulo >= 80)
			{				
				$final[$key][]=$acoesDivulgacao2[$key2];	  
				unset($acoesDivulgacao2[$key2]);				  
				unset($acoesDivulgacao[$key2]);
				continue;			
			} 	
		}
		unset($acoesDivulgacao2[$key]);
		reset($acoesDivulgacao2);
	}
	
	$db = new Database();
	$db->executeQuery("TRUNCATE TABLE repetidos_acaoDiv");	
	
	foreach ($final as $i => $value) {	
		if( count($final[$i]) == 1) {
			$db->executeQuery("INSERT INTO repetidos_acaoDiv (id_original) VALUES (".$final[$i][0]->id.")");
		} else {		
			$j = 0;
			for ($j = $j + 1; $j < count($final[$i]); $j++ ) {	
				$sql = "INSERT INTO repetidos_acaoDiv (id_original, id_repetido) VALUES (".$final[$i][0]->id.",".$final[$i][$j]->id.");";
				$db->executeQuery($sql);
			}
		}
	}
}


/**
Autor: Paulo Rodrigues (prodrigues@med.up.pt; paulomiguelarodrigues@gmail.com)
Data: 07/04/2014
Uso: Verifica se 2 Acoes de Divulgacao são iguais.
*/

function equalsAcaoDivulgacao($p1, $p2)
{
    if($p1->titulo == $p2->titulo && $p1->data == $p2->data && $p1->npart == $p2->npart && $p1->local == $p2->local)
    {
        return true;
    }
    else
    {
        return false;
    }
}

?>