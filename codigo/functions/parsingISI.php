<?php

/**
 * Parsing de refer?ncias.
 * 
 * Este script, faz parsing de refer?ncias. 
 * 
 * @author Jos? Filipe Lopes Santos <jfilipe@med.up.pt>
 * @since 29-12-2011
 * @version 1.0 - data da ?lt. actualiza??o: 29-12-2011
 * @package imp_endnote
 * @subpackage outputs
 */

require_once '../classlib/core.inc';

require_once '../classlib/Database.class.inc';

/* ------------------------------ oOo ----------------------------
					Inicializa??o de vari?veis
   ------------------------------ oOo ---------------------------- */


function parsefileISI($idinv,$append_pub){

$target_path = "../pubfiles/".$idinv;

if(!file_exists ( $target_path)){
	mkdir($target_path);
}


$target_path = $target_path ."/". basename( $idinv."_ISI_".$_FILES['uploadedfileISI']['name']."_".date("dmY_H:i")); 

if(move_uploaded_file($_FILES['uploadedfileISI']['tmp_name'], $target_path)) {
    //echo "The file ".  basename( $_FILES['uploadedfileISI']['name'])." has been uploaded";
} else{
    $error_msg="There was an error uploading the file, please try again!";
	echo "There was an error uploading the file, please try again!";
}

if (empty($error_msg)) $error_msg = ""; // mensagem de erro
$linhas = array(); // array com as linhas do ficheiro
$refs = array(); // array com os dados das refer?ncias


/* ------------------------------ oOo ----------------------------
					Obter as refer?ncias do ficheiro
   ------------------------------ oOo ---------------------------- */

// obter os dados do ficheiro
//$linhas = file(UPLOAD_FILES_PATH.$file) or die("N?o ? poss?vel abrir o ficheiro ".UPLOAD_FILES_PATH.$file);
$linhas = file($target_path) or die("Não é possível abrir o ficheiro ".$target_path);

/* ------------------------------ oOo ----------------------------
						Parsing das refer?ncias
   ------------------------------ oOo ---------------------------- */

$ref = ""; // string com a refer?ncia
$autores = ""; // string com os autores
$titulo = ""; // t?tulo
$titulo_ext = ""; // t?tulo externo
$source  = ""; // fonte
$volume = ""; // volume
$issue = ""; // issue / sequ?ncia
$special_issue = ""; // issue / sequ?ncia especial
$num_artigo = ""; // n.? do artigo
$doi = ""; // digital object identifier
$date = ""; // data de publica??o
$ano = ""; // ano em q foi publicado
$num_citacoes = ""; // n.? de cita??es
$total_citacoes = ""; // n.? total de cita??es
$num_serie = ""; // n.? de s?rie (ISSN)
$num_adesao = ""; // n.? de ades?o (Thomson Unique Article Identifier)
$abstract = ""; // abstract

$issn="";
$isbn="";
$ar="";
$colecao="";
$citacoes="";
$npatente="";
$ipc="";
$nomepublicacao="";
$conftitle="";
$language="";
$primpagina="";
$suplement="";

$primpaginaoriginal="";
$ultpaginaoriginal="";
$ultpagina="";





$count = array(); // array com contadores de refer?ncias por tipo
$append = array(); // array de booleanos que indicam se ? para concatenar
$append["global"] = false;
$append["autores"] = false;
$append["titulo"] = false;
$append["titulo_ext"] = false;
$append["abstract"] = false;
$type = ""; // tipo de refer?ncia

foreach ($linhas as $index=>$linha){
	
	if (ereg("^(PMID)([- ]+)([0-9]+)",$linha,$regs)){
		
		return("FICHEIRO ERRADO");
		
	}
	
	
	if (ereg("^(PT )([a-zA-Z]+)",$linha,$regs)){ // in?cio da refer?ncia
		$append["global"] = true;
		$type = $regs[2];
		continue;
	}

	if (ereg("^ER",$linha)){ // fim da refer?ncia
		
		// inicializar posi??o
		if (empty($count[$type])) $count[$type] = 0;

		// guardar dados
		$refs[$type][$count[$type]]["ref"] = $ref;
		$refs[$type][$count[$type]]["nomepublicacao"] = $nomepublicacao;	
		$refs[$type][$count[$type]]["issn"] = $issn;
		$refs[$type][$count[$type]]["isbn"] = $isbn;
		$refs[$type][$count[$type]]["titulo"] = $titulo;
		$refs[$type][$count[$type]]["autores"] = $autores;	
		$refs[$type][$count[$type]]["volume"] = $volume;
		$refs[$type][$count[$type]]["issue"] = $issue;
		$refs[$type][$count[$type]]["special_issue"] = $special_issue;
		$refs[$type][$count[$type]]["suplement"] = $suplement;
		
		$refs[$type][$count[$type]]["ar"] = $ar;	
		$refs[$type][$count[$type]]["colecao"] = $colecao;	
		$refs[$type][$count[$type]]["primpagina"] = $primpagina;
		$refs[$type][$count[$type]]["ultpagina"] = $ultpagina;
		$refs[$type][$count[$type]]["primpaginaoriginal"] = $primpaginaoriginal;
		$refs[$type][$count[$type]]["ultpaginaoriginal"] = $ultpaginaoriginal;
		$refs[$type][$count[$type]]["ano"] = $ano;
		$refs[$type][$count[$type]]["citacoes"] = $num_citacoes;
		$refs[$type][$count[$type]]["npatente"] = $npatente;
		$refs[$type][$count[$type]]["datapatente"] = $datapatente;
		$refs[$type][$count[$type]]["ipc"] = $ipc;
		$refs[$type][$count[$type]]["conftitle"] = $conftitle;
		$refs[$type][$count[$type]]["language"] = $language;
		
		
		
		//$refs[$type][$count[$type]]["titulo_ext"] = $titulo_ext;
		//$refs[$type][$count[$type]]["source"] = $source;


		//$refs[$type][$count[$type]]["special_issue"] = $special_issue;
		//$refs[$type][$count[$type]]["num_artigo"] = $num_artigo;
		$refs[$type][$count[$type]]["doi"] = $doi;
		//$refs[$type][$count[$type]]["date"] = $date;


		//$refs[$type][$count[$type]]["total_citacoes"] = $total_citacoes;
		//$refs[$type][$count[$type]]["num_serie"] = $num_serie;
		//$refs[$type][$count[$type]]["num_adesao"] = $num_adesao;

		//$refs[$type][$count[$type]]["abstract"] = $abstract;
		
		// incrementar o contador desse tipo
		$count[$type]++;


			$nomepublicacao= "";
			$issn= "";
			$isbn= "";
			$titulo= "";
			$autores= "";
			$volume= "";
			$issue= "";
			$ar= "";
			$colecao= "";
			$primpagina= "";
			$ultpagina= "";
			$primpaginaoriginal= "";
			$ultpaginaoriginal= "";
			$ano= "";
			$citacoes= "";
			$npatente= "";
			$datapatente= "";
			$ipc= "";
			$conftitle="";
			$language="";
	
	
		
		// inicializar vari?veis
		$ref = "";
		$autores = ""; 
		$titulo = "";
		$titulo_ext = "";
		$source = "";
		$volume = "";
		$issue = "";
		$special_issue = "";
		$num_artigo = "";
		$doi = "";
		$date = "";
		$ano = "";
		$num_citacoes = "";
		$total_citacoes = "";
		$num_serie = "";
		$num_adesao = "";
		$pag_inicio = "";
		$pag_fim = "";
		$abstract = "";
		$suplement="";

		
		$append["global"] = false;
		$append["autores"] = false;
		$append["titulo"] = false;
		$append["titulo_ext"] = false;
		$append["nomepublicacao"] = false;
	} 
	
	if ($append["global"]){ 

		// concatenar linhas da refer?ncia
		$ref .= $linha;
		
		////// autores /////
/*		if (ereg("^(AU )([a-zA-Z,. ]+)",$linha,$regs)){ // in?cio da tag autores 
			$autores = $regs[2];
			$append["autores"] = true;
			continue;
		}*/
		if (ereg("^(AU )([a-zA-Z., \\-]+)",$linha,$regs)){ // in?cio da tag autores 
			$autores = $regs[2];
			$append["autores"] = true;
			continue;
		}
		if (ereg("^[A-Z0-9]{2} ",$linha)) // se encontrar outra tag p?ra de concatenar
			$append["autores"] = false;
			
		if ($append["autores"])
			$autores .= " ; ".substr($linha,2,strlen($linha));
		
		////// T?tulo //////
		if (ereg("^(TI )(\\[?[a-zA-Z0-9\"]+.*)",$linha,$regs)){ // in?cio da tag t?tulo  (TI )(\\[?[a-zA-Z0-9\"]+.*)
			$titulo = $regs[2];
			$append["titulo"] = true;
			continue;
		}
		if (ereg("^[A-Z0-9]{2} ",$linha)) // se encontrar outra tag p?ra de concatenar
			$append["titulo"] = false;
		if ($append["titulo"])
			$titulo .= substr($linha,2,strlen($linha));
		
		/*if (ereg("^[A-Z0-9]{2} ",$linha)) // se encontrar outra tag p?ra de concatenar
			$append["titulo"] = false;
		if ($append["titulo"])
			$titulo .= substr($linha,2,strlen($linha));*/

		////// T?tulo externo //////
		if (ereg("^(FT )([a-zA-Z0-9]+.*)",$linha,$regs)){ // in?cio da tag t?tulo externo
			$titulo_ext = $regs[2];
			$append["titulo_ext"] = true;
			continue;
		}
		if (ereg("^[A-Z0-9]{2} ",$linha)) // se encontrar outra tag p?ra de concatenar
			$append["titulo_ext"] = false;
		if ($append["titulo_ext"])
			$titulo_ext .= substr($linha,2,strlen($linha));
					
		///// Abstract /////
		if (ereg("^(AB )([a-zA-Z0-9]+.*)",$linha,$regs)){
			$abstract = $regs[2];
			$append["abstract"] = true;
			continue;
		}
		if (ereg("^[A-Z0-9]{2} ",$linha)) // se encontrar outra tag p?ra de concatenar
			$append["abstract"] = false;
		if ($append["abstract"])
			$abstract .= substr($linha,2,strlen($linha));
			
		///// Fonte /////
		/*if (ereg("^(SO )([a-zA-Z0-9]+.*)",$linha,$regs))
			$nomepublicacao = $regs[2];*/
		if (ereg("^(SO )(\\[?[a-zA-Z0-9]+.*)",$linha,$regs)){ // in?cio da tag t?tulo
			$nomepublicacao = $regs[2];
			$append["nomepublicacao"] = true;
			continue;
		}
		if (ereg("^[A-Z0-9]{2} ",$linha)) // se encontrar outra tag p?ra de concatenar
			$append["nomepublicacao"] = false;
		if ($append["nomepublicacao"])
			$nomepublicacao .= substr($linha,2,strlen($linha));
		if (ereg("^[A-Z0-9]{2} ",$linha)) // se encontrar outra tag p?ra de concatenar
			$append["nomepublicacao"] = false;
		if ($append["nomepublicacao"])
			$nomepublicacao .= substr($linha,2,strlen($linha));
		
		///// Fonte /////
		if (ereg("^(CT )([a-zA-Z0-9]+.*)",$linha,$regs))
			$conftitle = $regs[2];
			
		///// Fonte /////
		if (ereg("^(LA )([a-zA-Z0-9]+.*)",$linha,$regs))
			$language = $regs[2];

		///// Volume /////
		if (ereg("^(VL )([a-zA-Z0-9]+.*)",$linha,$regs))
			$volume = $regs[2];
			
		///// Issue / sequ?ncia /////
		if (ereg("^(IS )([a-zA-Z0-9]+.*)",$linha,$regs))
			$issue = $regs[2];
			
		///// Issue / sequ?ncia /////
		if (ereg("^(SU )([a-zA-Z0-9]+.*)",$linha,$regs))
			$suplement = $regs[2];

		///// Issue / sequ?ncia especial /////
		if (ereg("^(SI )([a-zA-Z0-9]+.*)",$linha,$regs))
			$special_issue = $regs[2];
			
		///// N.? do artigo /////
		if (ereg("^(AR )([a-zA-Z0-9]+.*)",$linha,$regs))
			$ar = $regs[2];
			
		///// DOI - Digital Object Identifier /////
		if (ereg("^(DI )(.*)",$linha,$regs))
			$doi = $regs[2];

		///// Data da publica??o /////
		if (ereg("^(PD )([a-zA-Z0-9]+.*)",$linha,$regs))
			$date = $regs[2];

		///// Ano em que foi publicado /////
		if (ereg("^(PY )([a-zA-Z0-9]+.*)",$linha,$regs))
			$ano = $regs[2];

		///// N.? de cita??es /////
		if (ereg("^(TC )([a-zA-Z0-9]+.*)",$linha,$regs))
			$num_citacoes = $regs[2];
			
		///// N.? total de cita??es /////
		if (ereg("^(Z9 )([a-zA-Z0-9]+.*)",$linha,$regs))
			$total_citacoes = $regs[2];
		
		///// N.? de s?rie /////
		if (ereg("^(SN )([a-zA-Z0-9]+.*)",$linha,$regs))
			$issn = $regs[2];

		///// N.? de s?rie /////
		if (ereg("^(SE )([a-zA-Z0-9]+.*)",$linha,$regs))
			$colecao = $regs[2];
			
		///// N.? de s?rie /////
		if (ereg("^(PN )([a-zA-Z0-9]+.*)",$linha,$regs))
			$npatente = $regs[2];

		///// N.? de s?rie /////
		if (ereg("^(IC )([a-zA-Z0-9]+.*)",$linha,$regs))
			$ipc = $regs[2];
			
		///// N.? de s?rie /////
		if (ereg("^(BN )([a-zA-Z0-9]+.*)",$linha,$regs))
			$isbn = $regs[2];

		///// KeyWords Autor /////
		if (ereg("^(DE )([a-zA-Z0-9]+.*)",$linha,$regs))
			$num_serie = $regs[2];
			
		///// KeyWords Autor /////
		if (ereg("^(BE )([a-zA-Z0-9]+.*)",$linha,$regs))
			$editores = $regs[2];

		///// N.? de ades?o /////
		if (ereg("^(UT )([a-zA-Z0-9]+.*)",$linha,$regs))
			$num_adesao = $regs[2];
			
				///// N.? de ades?o /////
		if (ereg("^(PD )([a-zA-Z0-9]+.*)",$linha,$regs))
			$datapatente = $regs[2];

		///// P?gina de in?cio /////
		if (ereg("^(BP )([a-zA-Z0-9]+.*)",$linha,$regs)){
			$primpaginaoriginal=$regs[2];
			$primpagina = ereg_replace("[^0-9]+","",$regs[2]);

		}
			
		///// P?gina de fim /////
		if (ereg("^(EP )(.*)",$linha,$regs)){
			$ultpaginaoriginal=$regs[2];
			$ultpagina = ereg_replace("[^0-9]+","",$regs[2]);
		}
		
				
			
	}
}

$anoErrado=0;

$db = new Database();

if($append_pub!=1){
	$db->apagaRefsISI($idinv);
}

foreach ($refs as $type=>$info_type){
	foreach ($info_type as $index => $info){
		$num = $index + 1;
		
		$pp=intval(trim($info["primpagina"]));
		$up=intval(trim($info["ultpagina"]));
		
		if($up<$pp){
			$np=abs($up-substr($pp,-1*strlen($up)))+1;
		}else{
			$np=$up-$pp+1;	
		}
		
		
		
		switch($type){
			case 'J': { // Opção de entrada
				if($np<=2){
					$tipofmup="JA";
				}else{
					if(trim($info["conftitle"])==""){
						$tipofmup="J";
					}else{
						$tipofmup="PRP";
					}
				}
			}
			break;
			case 'B': { // Opção de entrada		
				if($np<=2){
					$tipofmup="OA";
				}else{
					if(trim($info["conftitle"])==""){
						$tipofmup="CL";
					}else{
						$tipofmup="CP";
					}
				}
			}
			break;
			case 'S': { // Opção de entrada		
				if($np<=2){
					$tipofmup="OA";
				}else{
					if(trim($info["conftitle"])==""){
						$tipofmup="CL";
					}else{
						$tipofmup="CP";
					}
				}
			}
			break;
			case 'P': { // Opção de entrada
				$tipofmup="P";
			}
			break;
			default:{ //echo "DEFAULT";
		       $tipofmup="UND";
		    }
			break;
		}
		
		if($info["primpaginaoriginal"]=="" && $info["ar"]==""){
			$info["primpaginaoriginal"]=$info["primpaginaoriginal"].$info["doi"];
		}
		
		if($info["issue"]==""){
			if($info["suplement"]!="")
				$info["issue"]="Sup: ".$info["suplement"];
			else if ($info["special_issue"]!=""){
				$info["issue"]="Esp Iss: ".$info["special_issue"];
			}
		}	
		
		
		
		if(trim($info["ano"])=='2014' || $tipofmup=='P'){
			
			$db->insertUmaPublicacao(
				$idinv,
				$type,
				str_replace("'", "\'", $info["nomepublicacao"]),
				$info["issn"],
				"",
				"",
				$info["isbn"],
				str_replace("'", "\'", $info["titulo"]),
				str_replace("'", "\'", $info["autores"]),
				$info["volume"],
				$info["issue"],
				$info["ar"],
				$info["colecao"]."/".str_replace("'", "\'", $info["conftitle"]),
				$info["ar"].$info["primpaginaoriginal"],
				$info["ultpaginaoriginal"],
				"",
				$info["citacoes"],
				$info["npatente"],
				$info["datapatente"],
				$info["ipc"],
				"",
				str_replace("'", "\'", $info["conftitle"]),
				$info["language"],
				"editor",
				$tipofmup,
				"1","0","",0,trim($info["ano"]));
		}else{
			$anoErrado++;
		}

	}

}

if($anoErrado!=0){
	return ("Foram ignoradas ".$anoErrado." Publicações dado que o ano de publicação está errado.");
}

}

?>

