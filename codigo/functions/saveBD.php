<?php
// PASSO 4 PMARQUES
require_once "../classlib/Database.class.inc";
$action = intval(@$_POST['a']);

switch ($action) {
	case 1:
		saveInvestigadorData();
		break;	
	case 2:
		deleteInvestigador();
		break;
	case 3:
		saveHabilitacaoData();
		break;
	case 4:
		deleteHabilitacao();
		break;
	case 5:
		saveAgregacaoData();
		break;
	case 6:
		deleteAgregacao();
		break;
	case 7:
		saveCaracterizazaoDocentesData();
		break;
	case 8:
		deleteCaracterizacao();
		break;
	case 9:
		saveCaracterizacaoPosDocData();
		break;	
	case 10:
		saveDirCursoData();
		break;
	case 11:
		deleteDirCurso();
		break;
	case 12:
		saveColIntActData();
		break;
	case 13:
		deleteColIntAct();
		break;
	case 14:
		saveColIntExtData();
		break;
	case 15:
		deleteColExtAct();
		break;
	case 16:
		saveUniInvData();
		break;
	case 17:
		deleteUniInv();
		break;
	case 18:
		savePubManPatentesData();
		break;
	case 19:
		deletePublicacao();
		break;
	case 20:
		savePubIntManArtData();
		break;
	case 21:
		savePubIntManLivroData();
		break;
    case 22:
        savePubIntManCLivroData();
        break;
    case 23:
        savePubIntManCPData();
        break;
    case 24:
		saveProjectoData();
    	break;
    case 25:
    	deleteProjecto();
    	break;
    case 26:
    	saveArbProjInBD();
    	break;
    case 27:
    	deleteArbProjecto();
    	break;
    case 28:
    	saveConferenciaData();
    	break;
    case 29:
    	deleteConferencia();
    	break;
    case 30:
    	saveApresentacaoData();
    	break;
    case 31:
    	deleteApresentacao();
    	break;
    case 32:
    	saveArbRevistaData();
    	break;
    case 33:
    	deleteArbRevista();
    	break;
    case 34:
    	savePremioData();
    	break;
    case 35:
    	deletePremio();
    	break;
    case 36:
    	saveSCData();    	
    	break;
    case 37:
    	deleteSC();
    	break;
    case 38:
    	saveRedeData();
    	break;
    case 39:
    	deleteRede();
    	break;
    case 40:
    	saveMissoesData();
    	break;
    case 41:
    	deleteMissao();
    	break;
    case 42:
    	saveOrientacaoData();
    	break;
   	case 43:
   		deleteOrientacao();
   		break;
   	case 44:
   		saveJuriData();
   		break;
   	case 45:
   		deleteJuri();
   		break;
   	case 46:
   		saveOutActData();
   		break;
   	case 47:
   		deleteOutAct();
   		break;
   	case 48:
   		saveAcaoDivulgacaoData();
   		break;
	case 49:
   		deleteAcaoDivulgacao();
   		break;
   	case 50:
   		saveProdutoData();
   		break;
   	case 51:
   		deleteProduto();
   		break;
	case 52:
		validateRepetidosProjecto();
		break;
	case 53:
		validateTodosRepetidosProjecto();
		break;
	case 54:
		saveObs();
		break;
	case 55:
		validaAcao();
		break;
	case 56:
		eliminaAcao();
		break;
	case 57:
		validateRepetidosPublicacao();
		break;
	case 58:
		validateTodosRepetidosPublicacao();
		break;
	case 59:
		validateRepetidosConferencia();
		break;
	case 60:
		validateTodosRepetidosConferencia();
		break;
	case 61:
		validateRepetidosAcaoDivulgacao();
		break;
	case 62:
		validateTodosRepetidosAcaoDivulgacao();
		break;		
	case 63:
		validateRepetidosPremio();
		break;
	case 64:
		validateTodosRepetidosAcaoDivulgacao();
		break;
	case 65:
		validateRepetidosArbitragensRevistas();
		break;
	case 66:
		validateTodosRepetidosArbitragensRevistas();
		break;
	case 67:
		validateRepetidosUnidadesInvestigacao();
		break;
	case 68:
		validateTodosRepetidosUnidadesInvestigacao();
		break;
	case 69:
		insertNewInvestigador();
		break;
	case 70:		
		insertNewHabilitacao();
		break;
	case 71:		
		insertNewAgregacao();
		break;
	case 72:		
		insertNewCaracDoc();
		break;
	case 73:		
		insertNewCaracPosDoc();
		break;
	case 74:		
		insertNewDirCurso();
		break;
	case 75:		
		insertNewColIntAct();
		break;
	case 76:		
		insertNewColExtAct();
		break;
	case 77:		
		insertNewUniInv();
		break;
	case 78:		
		insertNewProjeto();
		break;
	case 79:		
		insertNewConferencia();
		break;
	case 80:		
		insertNewApresentacao();
		break;
	case 81:		
		insertNewArbRevista();
		break;
	case 82:		
		insertNewPremio();
		break;
	case 83:		
		insertNewSC();
		break;
	case 84:		
		insertNewRede();
		break;
	case 85:		
		insertNewMissao();
		break;
	case 86:		
		insertNewOrientacao();
		break;		
	case 87:		
		insertNewJuri();
		break;
	case 88:		
		insertNewOutAct();
		break;
	case 89:		
		insertNewAcaoDivulgacao();
		break;
	case 90:		
		insertNewProduto();
		break;
	case 91:		
		insertNewPublicacaoArt();
		break;
	case 92:		
		insertNewPublicacaoConf();
		break;
	case 93:		
		insertNewPublicacaoLiv();
		break;
	case 94:		
		insertNewPublicacaoPatente();
		break;
	case 95:		
		insertNewPublicacaoManLiv();
		break;
	case 96:		
		insertNewPublicacaoManCLiv();
		break;
	case 97:		
		deleteAcao();
		break;
	case 98:
		getInvData();
		break;
	default:
		# code...
		break;
}


 function getInvData() {
	$array = '';
	$db = new Database();
	$lValues = $db->getInvData();
	
	while ($row = mysql_fetch_assoc($lValues)) {	
		$array .= $row["NOME"] . ",";		
		$array .= $row["DEP"] . ",";
	}
	
	echo $array;
}

/**
* INSERT
*/

function insertNewInvestigador()
{
	$nome = @$_POST['nome'];
	$nummec = @$_POST['num'];
	$datanasc = @$_POST['data'];
	$login = @$_POST['login'];	
	$dep = @$_POST['dep'];	
	
	$db = new Database();
	
	$query = "INSERT INTO investigadores (NOME, NUMMEC, DATANASC, SERVICO, ESTADO) VALUES ('$nome',$nummec,'$datanasc','$dep',-1)";	
			
	$db->executeQuery($query);
}

function insertNewHabilitacao()
{	
	$anoinicio = @$_POST['anoinicio'];
	$anofim = @$_POST['anofim'];
	$idinv = @$_POST['idinv'];
	$curso = @$_POST['curso'];
	$instituicao = @$_POST['instituicao'];
	$grau = @$_POST['grau'];
	$dist = @$_POST['dist'];
	$bolsa = @$_POST['bolsa'];
	$titulo = @$_POST['titulo'];
	$percentagem = @$_POST['percentagem'];
	$login = @$_POST['login'];	
	$dep = @$_POST['dep'];	
	
	$db = new Database();
	//Ano Inicio
    $idAnoInicio = $db->getAnoIDFromDB($anoinicio);
	//Ano Fim
	$idAnoFim = $db->getAnoIDFromDB($anofim);

	//Grau
	$idGrau = $db->getGrauCursoIDFromDB($grau);

	//Distincao
	$idDist = $db->getDistincaoIDFromDB($dist);

	//Bolsa   
	$idBolsa = $db->getBolsaIDFromDB($bolsa);

	$query = "INSERT INTO habilitacoesliterarias (anoinicio,anofim,curso,instituicao,grau,distincao,bolsa,titulo,percentagem,idinv) VALUES ($idAnoInicio,$idAnoFim,'$curso','$instituicao',$idGrau,'$idDist','$idBolsa','$titulo',$percentagem,$idinv);";  
	 
	if ($login == "admin" || $login == "sciman")
		$sql = $query;  
	else		
		$sql = "insert into accoes (tabela,acao,autor,data,descricao,departamento) VALUES (2,4,'".$login."',SYSDATE(),\"$query\",".$dep.");";				
	
	$db->executeQuery($sql);
}

function insertNewAgregacao() {
	$idinv = @$_POST['idinv'];
	$unanimidade = @$_POST['unanimidade'];
	$login = @$_POST['login'];	
	$dep = @$_POST['dep'];
	
	$db = new Database();
	
	$query = "INSERT INTO agregacoes (unanimidade, idinv) VALUES ($unanimidade, $idinv);";
		
	if ($login == "admin" || $login == "sciman")
		$sql = $query;  
	else
		$sql = "INSERT INTO accoes (tabela,acao,autor,data,descricao,departamento) VALUES (3,4,'$login',SYSDATE(),\"$query\",$dep);";				
		
	$db->executeQuery($sql);
}

function insertNewCaracDoc() {
	$idinv = @$_POST['idinv'];
	$tipo = @$_POST['tipo'];
	$percentagem = @$_POST['percentagem'];	
	$login = @$_POST['login'];	
	$dep = @$_POST['dep'];
	
	$db = new Database();
	
	$idTipo = $db->getTipoDocID($tipo);
	
	$query = "INSERT INTO ocupacaoprofissional (DOCENTECAT,DOCENTEPER,IDINV) VALUES ($idTipo,$percentagem,$idinv);";	
	
	if ($login == "admin" || $login == "sciman")
		$sql = $query;  
	else
		$sql = "INSERT INTO accoes (tabela,acao,autor,data,descricao,departamento) VALUES (4,4,'$login',SYSDATE(),\"$query\",$dep);";					
	
	echo $sql;
	//$db->executeQuery($sql);
}

function insertNewCaracPosDoc() {
	$idinv = @$_POST['idinv'];
	$bolsa = @$_POST['bolsa'];
	$percentagem = @$_POST['percentagem'];
	$login = @$_POST['login'];	
	$dep = @$_POST['dep'];
	
	$db = new Database();
	
	$idBolsa = $db->getBolsaIDFromDB($bolsa);
	
	//UPDATE
	$query = "INSERT INTO ocupacaoprofissional (INVESTIGADORBOLSA,INVESTIGADORPER,IDINV) VALUES ($idBolsa,$percentagem,$idinv);";
	
	if ($login == "admin" || $login == "sciman")
		$sql = $query;  
	else		
		$sql = "INSERT INTO accoes (tabela,acao,autor,data,descricao,departamento) VALUES (27,4,'$login',SYSDATE(),\"$query\",$dep);";		
	
	$db->executeQuery($sql);
}

function insertNewDirCurso() {
	$idinv = @$_POST['idinv'];
	$datainicio = @$_POST['datainicio'];
	$datafim = @$_POST['datafim'];
	$curso = @$_POST['curso'];
	$grau = @$_POST['grau'];
	$login = @$_POST['login'];	
	$dep = @$_POST['dep'];	
		
	$db = new Database();
	
	$idGrau = $db->getGrauCursoIDFromDB($grau);
	
	$query = "INSERT INTO dircursos (DATAINICIO, DATAFIM, NOME, GRAU, IDINV) VALUES ('$datainicio','$datafim','$curso','$idGrau',$idinv);";
	if ($login == "admin" || $login == "sciman")
		$sql = $query;  
	else
		$sql = "INSERT INTO accoes (tabela,acao,autor,data,descricao,departamento) VALUES (5,4,'$login',SYSDATE(),\"$query\",$dep);";		
	
	$db->executeQuery($sql);
}

function insertNewColIntAct() {
	$idinv = @$_POST['idinv'];
	$nomecient = @$_POST['nomecient'];
	$departamento = @$_POST['departamento'];
	$login = @$_POST['login'];	
	$dep = @$_POST['dep'];	
	$alt = @$_POST['alt'];
		
	$db = new Database();
	
	$idDepartamento = $db->getDepID($departamento);		
	
	$query = "INSERT INTO colaboradoresinternos (NOMECIENT,DEPARTAMENTO,IDINV) VALUES ('$nomecient','$idDepartamento',$idinv);";
	if ($login == "admin" || $login == "sciman")
		$sql = $query;  
	else
		$sql = "insert into accoes (tabela,acao,autor,data,descricao,departamento) VALUES (6,4,'$login',SYSDATE(),\"$query\",$dep);";				
	
	$db->executeQuery($sql);
}

function insertNewColExtAct() {
	$idinv = @$_POST['idinv'];
	$instituicao = @$_POST['instituicao'];
	$pais = @$_POST['pais'];
	$nomecient = @$_POST['nomecient'];
	$login = @$_POST['login'];	
	$dep = @$_POST['dep'];	
	
	$db = new Database();	
	
	$idPais = $db->getPaisID($pais);

	$query = "INSERT INTO colaboradores (NOMECIENT, INSTITUICAO, PAIS, IDINV) VALUES ('$nomecient','$instituicao','$idPais',$idinv);";
	echo $query;
	if ($login == "admin" || $login == "sciman")
		$sql = $query;  
	else		
		$sql = "insert into accoes (tabela,acao,autor,data,descricao,departamento) VALUES (7,4,'$login',SYSDATE(),\"$query\",$dep);";		

	$db->executeQuery($sql);		
}

function insertNewUniInv() {
	$idinv = @$_POST['idinv'];	
	$unidade = @$_POST['unidade'];
	$avaliacao = @$_POST['avaliacao'];
	$percentagem = @$_POST['percentagem'];	
	$responsavel = @$_POST['responsavel'];
	$login = @$_POST['login'];	
	$dep = @$_POST['dep'];	
	
	$db = new Database();	
	$idUnidade = $db->getUniInvID(trim($unidade));

	$query = "INSERT INTO unidadesinvestigacao (ID_UNIDADE,UNIDADE,AVA2007,PERCENTAGEM,RESPONSAVEL,IDINV) VALUES ($idUnidade,'$unidade','$avaliacao',$percentagem,$responsavel,$idinv);";
	if ($login == "admin" || $login == "sciman")
		$sql = $query;  
	else
		$sql = "insert into accoes (tabela,acao,autor,data,descricao,departamento) VALUES (8,4,'$login',SYSDATE(),\"$query\",$dep);";				
	
	$db->executeQuery($sql);		
}

function insertNewProjeto() {
	$idinv = @$_POST['idinv'];		
	$tipo = @$_POST['tipo'];
    $entidade = @$_POST['entidade'];
    $inst = @$_POST['inst'];
    $monsol = @$_POST['monsol'];
    $monapr = @$_POST['monapr'];
    $monfmup = @$_POST['monfmup'];
    $resp = @$_POST['resp'];
    $codigo = @$_POST['codigo'];
    $titulo = @$_POST['titulo'];
    $dataini = @$_POST['dataini'];
	$datafim = @$_POST['datafim'];
	$link = @$_POST['link'];
	$estado = @$_POST['estado'];
	$login = @$_POST['login'];
	$dep = @$_POST['dep'];	
		
   	$db = new Database();	
	$idTipo = $db->getTipoEntidadeID(trim($tipo));

	$query = "INSERT INTO projectosinv (ENTIDADE,MONTANTE,MONTANTEA,MONTANTEFMUP,INVRESPONSAVEL,CODIGO,TITULO,DATAINICIO,DATAFIM,ENTIDADEFINANCIADORA,ACOLHIMENTO,LINK,ESTADO,IDINV) VALUES ($idTipo,$monsol,$monapr,$monfmup,$resp,'$codigo','$titulo','$dataini','$datafim','$entidade','$inst','$link',$estado,$idinv);";   
	if ($login == "admin" || $login == "sciman")
		$sql = $query;  
	else
		$sql = "insert into accoes (tabela,acao,autor,data,descricao,departamento) VALUES (14,4,'$login',SYSDATE(),\"$query\",$dep);";				
		
	$db->executeQuery($sql);	
}

function insertNewConferencia() {
	$idinv = @$_POST['idinv'];
	$ambito = @$_POST['ambito'];
    $dataini = @$_POST['dataini'];
    $datafim = @$_POST['datafim'];
    $titulo = @$_POST['titulo'];
    $local = @$_POST['local'];
    $npart = @$_POST['npart'];
    $link = @$_POST['link'];
	$login = @$_POST['login'];
	$dep = @$_POST['dep'];	

    $db = new Database();
	$idAmbito = $db->getAmbitoConfID($ambito);
       
	$query = "INSERT INTO conferencias (AMBITO,DATAINICIO, DATAFIM, TITULO,LOCAL,PARTICIPANTES,LINK, IDINV) VALUES ($idAmbito,'$dataini','$datafim','$titulo','$local',$npart,'$link',$idinv);";
	echo $query;
	if ($login == "admin" || $login == "sciman")
		$sql = $query;  
	else
		$sql = "insert into accoes (tabela,acao,autor,data,descricao,departamento) VALUES (15,4,'$login',SYSDATE(),\"$query\",$dep);";		
	
	$db->executeQuery($sql);
}

function insertNewApresentacao() {
	$idinv = @$_POST['idinv'];
    $iconfconv = @$_POST['iconfconv'];
    $iconfpart = @$_POST['iconfpart'];
    $isem = @$_POST['isem'];
    $nconfconv = @$_POST['nconfconv'];
    $nconfpart = @$_POST['nconfpart'];
    $nsem = @$_POST['nsem'];  
	$login = @$_POST['login'];
	$dep = @$_POST['dep'];	
	
    $db = new Database();
	
	$query = "INSERT INTO apresentacoes (ICONFCONV,ICONFPART,ISEM,NCONFCONV,NCONFPART,NSEM,IDINV) VALUES ($iconfconv,$iconfpart,$isem,$nconfconv,$nconfpart,$nsem,$idinv);";
	if ($login == "admin" || $login == "sciman")
		$sql = $query;  
	else
		$sql = "insert into accoes (tabela,acao,autor,data,descricao,departamento) VALUES (16,4,'$login',SYSDATE(),\"$query\",$dep);";		
		
	$db->executeQuery($sql);
}

function insertNewArbRevista() {
	$idinv = @$_POST['idinv'];
    $tipo = @$_POST['tipo'];
    $issn = @$_POST['issn'];
    $titulo = @$_POST['titulo'];
    $link = @$_POST['link']; 
	$login = @$_POST['login'];
	$dep = @$_POST['dep'];	

    $db = new Database();
    $idTipo = $db->getTipoArbitragemID($tipo);
		
	$query = "INSERT INTO arbitragensrevistas (TIPO,ISSN,TITULOREVISTA,LINK,IDINV) VALUES ($idTipo,'$issn','$titulo','$link',$idinv);";
	if ($login == "admin" || $login == "sciman")
		$sql = $query;  
	else		
		$sql = "insert into accoes (tabela,acao,autor,data,descricao,departamento) VALUES (17,4,'$login',SYSDATE(),\"$query\",$dep);";	
			
	$db->executeQuery($sql);
}

function insertNewPremio() {
	$idinv = @$_POST['idinv'];
	$tipo = @$_POST['tipo'];
    $nome = @$_POST['nome'];
    $contexto = @$_POST['contexto'];
    $montante = @$_POST['montante'];    
    $link = @$_POST['link'];
	$login = @$_POST['login'];
	$dep = @$_POST['dep'];	

    $db = new Database();
    $idTipo = $db->getTipoPremioID($tipo);

	$query = "INSERT INTO premios (TIPO,NOME,CONTEXTO,MONTANTE,LINK,IDINV) VALUES ('$idTipo','$nome','$contexto','$montante','$link',$idinv);";
	if ($login == "admin" || $login == "sciman")	
		$sql = $query;  
	else
		$sql = "insert into accoes (tabela,acao,autor,data,descricao,departamento) VALUES (18,4,'$login',SYSDATE(),\"$query\",$dep);";					
	
	$db->executeQuery($sql);
}

function insertNewSC() {
	$idinv = @$_POST['idinv'];
	$tipo = @$_POST['tipo'];
    $datainicio = @$_POST['datainicio'];
    $datafim = @$_POST['datafim'];
    $cargo = @$_POST['cargo'];    
    $link = @$_POST['link'];
    $nome = @$_POST['nome'];
	$login = @$_POST['login'];
	$dep = @$_POST['dep'];	

    $db = new Database();
	$idTipo = $db->getTipoSCID($tipo);

	$query = "INSERT INTO sociedadescientificas (TIPO,DATAINICIO,DATAFIM,CARGO,LINK,NOME,IDINV) VALUES ('$idTipo','$datainicio','$datafim','$cargo','$link','$nome',$idinv);";
	 if ($login == "admin" || $login == "sciman")		
		$sql = $query;  
	else		
		$sql = "insert into accoes (tabela,acao,autor,data,descricao,departamento) VALUES (19,4,'$login',SYSDATE(),\"$query\",$dep);";					
	
	$db->executeQuery($sql);
}

function insertNewRede() {
	$idinv = @$_POST['idinv'];
	$tipo = @$_POST['tipo'];
    $datainicio = @$_POST['datainicio'];
    $datafim = @$_POST['datafim'];
    $link = @$_POST['link'];
    $nome = @$_POST['nome'];
	$login = @$_POST['login'];
	$dep = @$_POST['dep'];	

    $db = new Database();
    $idTipo = $db->getTipoRedesID($tipo);

	$query = "INSERT INTO redes (TIPO,DATAINICIO,DATAFIM,LINK,NOME,IDINV) VALUES ('$idTipo','$datainicio','$datafim','$link','$nome',$idinv);";
	if ($login == "admin" || $login == "sciman")
		$sql = $query;  
	else
		$sql = "insert into accoes (tabela,acao,autor,data,descricao,departamento) VALUES (20,4,'$login',SYSDATE(),\"$query\",$dep);";						
	
	$db->executeQuery($sql);
}

function insertNewMissao() {
	$idinv = @$_POST['idinv']; 
	$datainicio = @$_POST['datainicio'];
    $datafim = @$_POST['datafim'];
    $motivacao = @$_POST['motivacao'];    
    $instituicao = @$_POST['instituicao'];
    $pais = @$_POST['pais'];
    $ambtese = @$_POST['ambtese'];
	$login = @$_POST['login'];
	$dep = @$_POST['dep'];	

    $db = new Database();
    $idPais = $db->getPaisID($pais);

	$query = "INSERT INTO missoes (PAIS,DATAINICIO,DATAFIM,INSTITUICAO,MOTIVACAO,AMBTESE,IDINV) VALUES ($idPais,'$datainicio','$datafim','$instituicao','$motivacao',$ambtese,$idinv);";
	
	if ($login == "admin" || $login == "sciman")	
		$sql = $query;  
	else
		$sql = "insert into accoes (tabela,acao,autor,data,descricao,departamento) VALUES (21,4,'$login',SYSDATE(),\"$query\",$dep);";					

	$db->executeQuery($sql);
}

function insertNewOrientacao() {
	$idinv = @$_POST['idinv']; 
	$tipo = @$_POST['tipo'];
    $tipoori = @$_POST['tipoori'];
    $estado = @$_POST['estado'];    
    $titulo = @$_POST['titulo'];
	$login = @$_POST['login'];
	$dep = @$_POST['dep'];	

    $db = new Database();
	$idTipo = $db->getTipoOrientacaoID($tipo);

	$idTipoOri = $db->getTipoOrientadorID($tipoori);

	$idEstado = $db->getEstadoOrientacaoID($estado);

	$query = "INSERT INTO orientacoes (TIPO_ORIENTACAO, TIPO_ORIENTADOR, ESTADO, TITULO, IDINV) VALUES ($idTipo, $idTipoOri, $idEstado, '$titulo', $idinv);";	
	if ($login == "admin" || $login == "sciman")
		$sql = $query;  
	else
		$sql = "insert into accoes (tabela,acao,autor,data,descricao,departamento) VALUES (22,4,'$login',SYSDATE(),\"$query\",$dep);";				
	
	$db->executeQuery($sql);
}

function insertNewJuri() {
	$idinv = @$_POST['idinv']; 
	$agrargfmup = @$_POST['agrargfmup'];
    $agrvogalfmup = @$_POST['agrvogalfmup'];
    $agrargext = @$_POST['agrargext'];
    $agrvogalext = @$_POST['agrvogalext'];

    $doutargfmup = @$_POST['doutargfmup'];
    $doutvogalfmup = @$_POST['doutvogalfmup'];
    $doutargext = @$_POST['doutargext'];
    $doutvogalext = @$_POST['doutvogalext'];

    $mesargfmup = @$_POST['mesargfmup'];
    $mesvogalfmup = @$_POST['mesvogalfmup'];
	$mesargext = @$_POST['mesargext'];
	$mesvogalext = @$_POST['mesvogalext'];

	$mesintfmup = @$_POST['mesintfmup'];
	$mesintvogalfmup = @$_POST['mesintvogalfmup'];
	$mesintargext = @$_POST['mesintargext'];
	$mesintvogalext = @$_POST['mesintvogalext'];
	
	$login = @$_POST['login'];
	$dep = @$_POST['dep'];	

    $db = new Database();
   
	$query = "INSERT INTO juris (AGREGACAOARGFMUP,AGREGACAOVOGALFMUP,AGREGACAOARGEXT,AGREGACAOVOGALEXT,DOUTORAMENTOARGFMUP,DOUTORAMENTOVOGALFMUP,DOUTORAMENTOARGEXT,DOUTORAMENTOVOGALEXT,MESTRADOARGFMUP,MESTRADOVOGALFMUP,MESTRADOARGEXT,MESTRADOVOGALEXT,MESTRADOINTARGFMUP,MESTRADOINTVOGALFMUP,MESTRADOINTARGEXT,MESTRADOINTVOGALEXT,IDINV) VALUES ($agrargfmup,$agrvogalfmup,$agrargext,$agrvogalext,$doutargfmup,$doutvogalfmup,$doutargext,$doutvogalext,$mesargfmup,$mesvogalfmup,$mesargext,$mesvogalext,$mesintfmup,$mesintvogalfmup,$mesintargext,$mesintvogalext,$idinv);";
	if ($login == "admin" || $login == "sciman")
		$sql = $query;  
	else		
		$sql = "insert into accoes (tabela,acao,autor,data,descricao,departamento) VALUES (23,4,'$login',SYSDATE(),\"$query\",$dep);";					
		
	$db->executeQuery($sql);
}

function insertNewOutAct() {	
	$idinv = @$_POST['idinv']; 
	$texapoio = @$_POST['texapoio'];
    $livdidaticos = @$_POST['livdidaticos'];
    $livdivulgacao = @$_POST['livdivulgacao'];
    $app = @$_POST['app'];
    $webp = @$_POST['webp'];
	$login = @$_POST['login'];
	$dep = @$_POST['dep'];	
    
    $db = new Database();
	$query = "INSERT INTO outrasactividades (TEXAPOIO,LIVDIDATICOS,LIVDIVULGACAO,APP,WEBP,IDINV) VALUES ($texapoio,$livdidaticos,$livdivulgacao,$app,$webp,$idinv);";
	if ($login == "admin" || $login == "sciman")	
		$sql = $query;  
	else
		$sql = "insert into accoes (tabela,acao,autor,data,descricao,departamento) VALUES (24,4,'$login',SYSDATE(),\"$query\",$dep);";						
	
	$db->executeQuery($sql);
}

function insertNewAcaoDivulgacao() {
	$idinv = @$_POST['idinv']; 
	$titulo = @$_POST['titulo'];
    $data = @$_POST['data'];
    $local = @$_POST['local'];
    $npart = @$_POST['npart'];
	$login = @$_POST['login'];
	$dep = @$_POST['dep'];	
    
    $db = new Database();
	$query = "INSERT INTO acoesdivulgacao (TITULO,DATA,LOCAL,NPART,IDINV) VALUES ('$titulo','$data','$local','$npart',$idinv);";
	if ($login == "admin" || $login == "sciman")
		$sql = $query;  
	else
		$sql = "insert into accoes (tabela,acao,autor,data,descricao,departamento) VALUES (25,4,'$login',SYSDATE(),\"$query\",$dep);";					
		
	$db->executeQuery($sql);
}

function insertNewProduto() {
	$idinv = @$_POST['idinv']; 
	$tipo = @$_POST['tipo'];
    $preco = @$_POST['preco'];
	$login = @$_POST['login'];
	$dep = @$_POST['dep'];	
    
    $db = new Database();
	
	$query = "INSERT INTO produtos (TIPOP,VALORP,IDINV) VALUES ('$tipo',$preco,$idinv);";
	if ($login == "admin" || $login == "sciman")	
		$sql = $query;  
	else
		$sql = "INSERT into accoes (tabela,acao,autor,data,descricao,departamento) VALUES (26,4,'$login',SYSDATE(),\"$query\",$dep);";								
		
	$db->executeQuery($sql);
}

function insertNewPublicacaoArt() {
	$idinv = @$_POST['idinv'];
	$issn = @$_POST['issn'];
	$revista = @$_POST['revista'];
	$titulo = @$_POST['titulo'];	
	$autores = @$_POST['autores'];
	$volume = @$_POST['volume'];
	$issue = @$_POST['issue'];	
	$prim = @$_POST['prim'];
	$ult = @$_POST['ult'];	
	$citacao = @$_POST['citacao'];
	$estado = @$_POST['estado'];
	$login = @$_POST['login'];	
	$dep = @$_POST['dep'];	
	$tipopublicacao = @$_POST['tipopublicacao'];
	$descr = @$_POST['descr'];
	
	$db = new Database();
	$query = '';
	$tabela = 0;
	
	if ($descr == 'ISI') {
		//ISI
		$tabela = 9;
	} else if ($descr == 'PUBMED') {
		//PUBMED
		$tabela = 10;
	} else if ($descr == 'INT') {
		//Internacional
		$tabela = 12;
	} else if ($descr == 'NAC') {
		//Nacional
		$tabela = 13;
	} 
	
	$query = "insert into publicacoes (idinv,TIPO,TIPOFMUP, NOMEPUBLICACAO, ISSN, TITULO, AUTORES, VOLUME, ISSUE, PRIMPAGINA, ULTPAGINA , CITACOES , ESTADO, INDEXED_ISI, INDEXED_PUBMED) 
		VALUES ('$idinv','$tipopublicacao','$tipopublicacao','$revista','$issn','$titulo','$autores','$volume','$issue','$prim','$ult','$citacao','$estado','1','0')";
	
	if ($login == "admin" || $login == "sciman")	
		$sql = $query;  
	else
		$sql = "INSERT into accoes (tabela,acao,autor,data,descricao,departamento) VALUES ($tabela,4,'$login',SYSDATE(),\"$query\",$dep);";		
	
	$db->executeQuery($sql);
}

function insertNewPublicacaoConf() { 
	$idinv = @$_POST['idinv'];
	$isbn = @$_POST['isbn'];
	$nomepub = @$_POST['nomepub'];	
	$editores = @$_POST['editores'];	
	$colecao = @$_POST['colecao'];	
	$volume = @$_POST['volume'];		
	$titulo = @$_POST['titulo'];	
	$prim = @$_POST['prim'];
	$ult = @$_POST['ult'];		
	$autores = @$_POST['autores'];	
	$estado = @$_POST['estado'];
	$login = @$_POST['login'];	
	$dep = @$_POST['dep'];	
	$tipopublicacao = @$_POST['tipopublicacao'];
	$descr = @$_POST['descr'];
	
	$db = new Database();	
	$tabela = 0;
	
	if ($descr == 'ISI') {
		//ISI
		$tabela = 9;
	} else if ($descr == 'PUBMED') {
		//PUBMED
		$tabela = 10;
	} else if ($descr == 'INT') {
		//Internacional
		$tabela = 12;
	} else if ($descr == 'NAC') {
		//Nacional
		$tabela = 13;
	} 
		
	$query = "insert into publicacoes (idinv,TIPOFMUP, NOMEPUBLICACAO, ISBN, TITULO, AUTORES, VOLUME, PRIMPAGINA, ULTPAGINA , ESTADO, EDITOR, COLECAO, INDEXED_ISI, INDEXED_PUBMED) 
		VALUES ('$idinv','$tipopublicacao','$nomepub','$isbn','$titulo','$autores','$volume','$prim','$ult','$estado','$editores','$colecao','1','0')";
			
	if ($login == "admin" || $login == "sciman")	
		$sql = $query;  
	else
		$sql = "INSERT into accoes (tabela,acao,autor,data,descricao,departamento) VALUES ($tabela,4,'$login',SYSDATE(),\"$query\",$dep);";		
	
	echo $sql;
	$db->executeQuery($sql);
}

function insertNewPublicacaoLiv() {
	$idinv = @$_POST['idinv'];	
	$nomepub = @$_POST['nomepub'];
	$issn = @$_POST['issn'];	
	$isbn = @$_POST['isbn'];	
	$titulo = @$_POST['titulo'];	
	$autores = @$_POST['autores'];
	$volume = @$_POST['volume'];	
	$issue = @$_POST['issue'];	
	$ar = @$_POST['ar'];		
	$colecao = @$_POST['colecao'];	
	$prim = @$_POST['prim'];
	$ult = @$_POST['ult'];
	$citacoes = @$_POST['citacoes'];
	$conftitle = @$_POST['conftitle'];
	$language = @$_POST['language'];
	$editor = @$_POST['editor'];
	
	$estado = @$_POST['estado'];
	$login = @$_POST['login'];	
	$dep = @$_POST['dep'];	
	$tipopublicacao = @$_POST['tipopublicacao'];
	$descr = @$_POST['descr'];
	
	$db = new Database();	
	$tabela = 0;
	
	if ($descr == 'ISI') {
		//ISI
		$tabela = 9;
	} else if ($descr == 'PUBMED') {
		//PUBMED
		$tabela = 10;
	} else if ($descr == 'INT') {
		//Internacional
		$tabela = 12;
	} else if ($descr == 'NAC') {
		//Nacional
		$tabela = 13;
	} 
		
	$query = "insert into publicacoes (idinv,TIPOFMUP, NOMEPUBLICACAO, ISSN, ISBN, TITULO, AUTORES, VOLUME, ISSUE, AR, COLECAO, PRIMPAGINA, ULTPAGINA , CITACOES, CONFTITLE, LANGUAGE, EDITOR, ESTADO, INDEXED_ISI, INDEXED_PUBMED) 
		VALUES ('$idinv','$tipopublicacao','$nomepub','$issn','$isbn','$titulo','$autores','$volume','$issue','$ar','$colecao','$prim','$ult','$citacoes','$conftitle','$language','$editor','$estado','1','0')";
			
	if ($login == "admin" || $login == "sciman")	
		$sql = $query;  
	else
		$sql = "INSERT into accoes (tabela,acao,autor,data,descricao,departamento) VALUES ($tabela,4,'$login',SYSDATE(),\"$query\",$dep);";		
		
	$db->executeQuery($sql);
}

function insertNewPublicacaoPatente() {
	$idinv = @$_POST['idinv'];	
	$npatente = @$_POST['npatente'];	
	$ipc = @$_POST['ipc'];	
	$data = @$_POST['data'];	
	$titulo = @$_POST['titulo'];	
	$autores = @$_POST['autores'];
	$link = @$_POST['link'];		
	$estado = @$_POST['estado'];	
	$login = @$_POST['login'];	
	$dep = @$_POST['dep'];	
	
	$db = new Database();	
				
	$query = "insert into publicacoes (idinv,TIPOFMUP, NPATENTE, IPC, DATAPATENTE, TITULO, AUTORES, LINK, ESTADO, INDEXED_ISI, INDEXED_PUBMED) 
		VALUES ('$idinv','P','$npatente','$ipc','$data','$titulo','$autores','$link','$estado','0','0')";
			
	if ($login == "admin" || $login == "sciman")	
		$sql = $query;  
	else
		$sql = "INSERT into accoes (tabela,acao,autor,data,descricao,departamento) VALUES (11,4,'$login',SYSDATE(),\"$query\",$dep);";		
		
	$db->executeQuery($sql);
}

function insertNewPublicacaoManLiv() {
	$idinv = @$_POST['idinv'];	
	$titulo = @$_POST['titulo'];
	$autores = @$_POST['autores'];
	$editor = @$_POST['editor'];
	$editora = @$_POST['editora'];
	$link = @$_POST['link'];		
	$estado = @$_POST['estado'];	
	$login = @$_POST['login'];	
	$dep = @$_POST['dep'];	
	$descr = @$_POST['descr'];
	
	$db = new Database();	
		
	if ($descr == 'INT') {
		//Internacional
		$tabela = 12;
	} else if ($descr == 'NAC') {
		//Nacional
		$tabela = 13;
	} 
				
	$query = "insert into publicacoes (idinv,TIPOFMUP, EDITOR, EDITORA, TITULO, AUTORES, LINK, ESTADO, INDEXED_ISI, INDEXED_PUBMED) 
		VALUES ('$idinv','L','$editor','$editora','$titulo','$autores','$link','$estado','0','0')";
			
	if ($login == "admin" || $login == "sciman")	
		$sql = $query;  
	else
		$sql = "INSERT into accoes (tabela,acao,autor,data,descricao,departamento) VALUES (12,4,'$login',SYSDATE(),\"$query\",$dep);";		
		
	$db->executeQuery($sql);
}

function insertNewPublicacaoManCLiv() {
	$idinv = @$_POST['idinv'];	
	$isbn = @$_POST['isbn'];	
	$nomepub = @$_POST['nomepub'];
	$titulo = @$_POST['titulo'];	
	$autores = @$_POST['autores'];
	$editor = @$_POST['editor'];
	$editora = @$_POST['editora'];
	$prim = @$_POST['prim'];
	$ult = @$_POST['ult'];
	$link = @$_POST['link'];		
	$estado = @$_POST['estado'];	
	$login = @$_POST['login'];	
	$dep = @$_POST['dep'];		
	$descr = @$_POST['descr'];	
	
	$db = new Database();	
	
	if ($descr == 'INT') {
		//Internacional
		$tabela = 12;
	} else if ($descr == 'NAC') {
		//Nacional
		$tabela = 13;
	} 
				
	$query = "insert into publicacoes (idinv,TIPOFMUP, NOMEPUBLICACAO, ISBN, TITULO, AUTORES, EDITOR, EDITORA, PRIMPAGINA, ULTPAGINA ,LINK, ESTADO, INDEXED_ISI, INDEXED_PUBMED) 
		VALUES ('$idinv','CL','$nomepub','$isbn','$titulo','$autores','$editor','$editora','$prim','$ult','$link','$estado','0','0')";
			
	if ($login == "admin" || $login == "sciman")	
		$sql = $query;  
	else
		$sql = "INSERT into accoes (tabela,acao,autor,data,descricao,departamento) VALUES (12,4,'$login',SYSDATE(),\"$query\",$dep);";		
		
	$db->executeQuery($sql);
}

function deleteAcao() {
	$id = @$_POST['id'];	
	$tab = @$_POST['tab'];	
	$login = @$_POST['login'];
	
	$sql = "DELETE FROM accoes WHERE IDREG=$id AND TABELA=$tab AND AUTOR='$login';";
	$db = new Database();
	$db->executeQuery($sql);
}

/**
* SAVE
*/

function saveInvestigadorData()
{
	$nome = @$_POST['nome'];
	$nummec = @$_POST['num'];
	$datanasc = @$_POST['data'];
	$id = @$_POST['id'];
	$login = @$_POST['login'];	
	$dep = @$_POST['dep'];	
	$alt = @$_POST['alt'];
	
	$login = "asd";
	$db = new Database();
	$db->adminUpdateInvestigador($nome,$nummec,$datanasc,$id, $login, $dep, $alt);
}

function saveHabilitacaoData()
{
	$anoinicio = @$_POST['anoinicio'];
	$anofim = @$_POST['anofim'];
	$id = @$_POST['id'];
	$curso = @$_POST['curso'];
	$instituicao = @$_POST['instituicao'];
	$grau = @$_POST['grau'];
	$dist = @$_POST['dist'];
	$bolsa = @$_POST['bolsa'];
	$titulo = @$_POST['titulo'];
	$percentagem = @$_POST['percentagem'];
	$orientador = @$_POST['orientador'];
	$oinstituicao = @$_POST['oinstituicao'];
	$corientador = @$_POST['corientador'];
	$coinstituicao = @$_POST['coinstituicao'];
	$login = @$_POST['login'];	
	$dep = @$_POST['dep'];	
	$alt = @$_POST['alt'];
	$db = new Database();
	$db->adminUpdateHabilitacao($anoinicio,$anofim,$id,$curso,$instituicao,$grau,$dist,$bolsa,$titulo,$percentagem,$orientador,$oinstituicao,$corientador,$coinstituicao,$login,$dep,$alt);
}

function saveAgregacaoData()
{	
	$id = @$_POST['id'];
	$unanimidade = @$_POST['unanimidade'];
	$login = @$_POST['login'];	
	$dep = @$_POST['dep'];	
	$alt = @$_POST['alt'];
	$db = new Database();
	$db->adminUpdateAgregacao($id,$unanimidade,$login,$dep,$alt);
}

function saveCaracterizazaoDocentesData()
{
	$id = @$_POST['id'];
	$tipo = @$_POST['tipo'];
	$percentagem = @$_POST['percentagem'];	
	$login = @$_POST['login'];	
	$dep = @$_POST['dep'];	
	$alt = @$_POST['alt'];
	$db = new Database();
	$db->adminUpdateCaracterizacaoDocentes($id,$tipo,$percentagem,$login,$dep,$alt);
}

function saveCaracterizacaoPosDocData()
{
	$id = @$_POST['id'];
	$bolsa = @$_POST['bolsa'];
	$percentagem = @$_POST['percentagem'];
	$login = @$_POST['login'];	
	$dep = @$_POST['dep'];	
	$alt = @$_POST['alt'];
	$db = new Database();
	$db->adminUpdateCaracterizacaoPosDoc($id,$bolsa,$percentagem,$login,$dep,$alt);
}

function saveDirCursoData()
{
	$id = @$_POST['id'];
	$datainicio = @$_POST['datainicio'];
	$datafim = @$_POST['datafim'];
	$curso = @$_POST['curso'];
	$grau = @$_POST['grau'];
	$login = @$_POST['login'];	
	$dep = @$_POST['dep'];	
	$alt = @$_POST['alt'];
	$db = new Database();
	$db->adminUpdateDirCurso($id,$datainicio,$datafim,$curso,$grau,$login,$dep,$alt);
}

function saveColIntActData()
{
	$id = @$_POST['id'];
	$nomecient = @$_POST['nomecient'];
	$departamento = @$_POST['departamento'];
	$login = @$_POST['login'];	
	$dep = @$_POST['dep'];	
	$alt = @$_POST['alt'];
	$db = new Database();	
	$db->adminUpdateColIntAct($id,$nomecient,$departamento,$login,$dep,$alt);
}

function saveColIntExtData()
{
	$id = @$_POST['id'];
	$instituicao = @$_POST['instituicao'];
	$pais = @$_POST['pais'];
	$nomecient = @$_POST['nomecient'];
	$login = @$_POST['login'];	
	$dep = @$_POST['dep'];	
	$alt = @$_POST['alt'];
	$db = new Database();	
	$db->adminUpdateColExtAct($id,$nomecient,$instituicao,$pais,$login,$dep,$alt);
}

function saveUniInvData()
{
	$id = @$_POST['id'];
	$unidade = @$_POST['unidade'];
	$avaliacao = @$_POST['avaliacao'];
	$percentagem = @$_POST['percentagem'];	
	$responsavel = @$_POST['responsavel'];
	$login = @$_POST['login'];	
	$dep = @$_POST['dep'];	
	$alt = @$_POST['alt'];
	$db = new Database();	
	$db->adminUpdateUniInv($id,$unidade,$avaliacao,$percentagem,$responsavel,$login,$dep,$alt);
}

function savePubManPatentesData()
{
	$id = @$_POST['id'];
	$npatente = @$_POST['npatente'];
	$ipc = @$_POST['ipc'];
	$titulo = @$_POST['titulo'];	
	$autores = @$_POST['autores'];
	$data = @$_POST['data'];
	$link = @$_POST['link'];
	$estado = @$_POST['estado'];
	$login = @$_POST['login'];	
	$dep = @$_POST['dep'];	
	$alt = @$_POST['alt'];
	
	$db = new Database();	
	$db->adminUpdatePubManPatentes($id,$npatente,$ipc,$titulo,$autores,$data,$link,$estado,$login,$dep,$alt);
}

function savePubIntManArtData()
{
	$id = @$_POST['id'];
	$issn = @$_POST['issn'];
	$revista = @$_POST['revista'];
	$titulo = @$_POST['titulo'];	
	$autores = @$_POST['autores'];
	$volume = @$_POST['volume'];
	$issue = @$_POST['issue'];	
	$prim = @$_POST['prim'];
	$ult = @$_POST['ult'];	
	$link = @$_POST['link'];
	$estado = @$_POST['estado'];
	$login = @$_POST['login'];	
	$dep = @$_POST['dep'];	
	$alt = @$_POST['alt'];
	$tabela = @$_POST['tabela'];
	
	$db = new Database();	
	$db->adminUpdatePubManIntArtigo($id,$issn,$revista,$titulo,$autores,$volume,$issue,$prim,$ult,$link,$estado,$login,$dep,$alt,$tabela);
}

function savePubIntManLivroData()
{
	$id = @$_POST['id'];
	$isbn = @$_POST['isbn'];
	$titulo = @$_POST['titulo'];	
	$autores = @$_POST['autores'];
	$editor = @$_POST['editor'];
	$editora = @$_POST['editora'];
	$link = @$_POST['link'];
	$estado = @$_POST['estado'];	
	$login = @$_POST['login'];	
	$dep = @$_POST['dep'];	
	$alt = @$_POST['alt'];
	$tabela = @$_POST['tabela'];
	
	$db = new Database();	
	$db->adminUpdatePubManIntLivro($id,$isbn,$titulo,$autores,$editor,$editora,$link,$estado,$login,$dep,$alt,$tabela);
}


function savePubIntManCLivroData()
{
    $id = @$_POST['id'];
    $isbn = @$_POST['isbn'];
    $nomepub = @$_POST['nomepub'];
    $editora = @$_POST['editora'];
    $editor = @$_POST['editor'];
    $titulo = @$_POST['titulo'];
    $autores = @$_POST['autores'];
    $prim = @$_POST['prim'];
	$ult = @$_POST['ult'];
    $link = @$_POST['link'];
    $estado = @$_POST['estado'];
	$login = @$_POST['login'];	
	$dep = @$_POST['dep'];	
	$alt = @$_POST['alt'];
	$tabela = @$_POST['tabela'];
	
    $db = new Database();
    $db->adminUpdatePubManIntCLivro($id, $isbn, $nomepub, $editora, $editor, $titulo, $autores, $prim, $ult, $link, $estado,$login,$dep,$alt,$tabela);
}

function savePubIntManCPData()
{
    $id = @$_POST['id'];
    $isbn = @$_POST['isbn'];
    $nomepub = @$_POST['nomepub'];
    $editora = @$_POST['editora'];
    $titulo = @$_POST['titulo'];
    $autores = @$_POST['autores'];
    $prim = @$_POST['prim'];
    $ult = @$_POST['ult'];
    $link = @$_POST['link'];
    $estado = @$_POST['estado'];
	$login = @$_POST['login'];	
	$dep = @$_POST['dep'];	
	$alt = @$_POST['alt'];
	$tabela = @$_POST['tabela'];
    $db = new Database();
    $db->adminUpdatePubManIntCP($id, $isbn, $nomepub, $editora, $titulo, $autores, $prim, $ult, $link, $estado,$login,$dep,$alt,$tabela);
}

function saveProjectoData()
{	
	$id = @$_POST['id'];
    $tipo = @$_POST['tipo'];
    $entidade = @$_POST['entidade'];
    $inst = @$_POST['inst'];
    $monsol = @$_POST['monsol'];
    $monapr = @$_POST['monapr'];
    $monfmup = @$_POST['monfmup'];
    $resp = @$_POST['resp'];
    $codigo = @$_POST['codigo'];
    $titulo = @$_POST['titulo'];
    $dataini = @$_POST['dataini'];
	$datafim = @$_POST['datafim'];
	$link = @$_POST['link'];
	$estado = @$_POST['estado'];
	$login = @$_POST['login'];
	$alt = @$_POST['alt'];
	$dep = @$_POST['dep'];	
		
    $db = new Database();
    $db->adminUpdateProjecto($id, $tipo, $entidade, $inst, $monsol, $monapr, $monfmup, $resp, $codigo, $titulo, $dataini, $datafim, $link, $estado, $login, $alt, $dep);
}

function saveArbProjInBD()
{
	$id = @$_POST['id'];
    $api = @$_POST['api'];
    $apn = @$_POST['apn'];    

    $db = new Database();
    $db->adminUpdateArbProjecto($id, $api, $apn);
}

function saveConferenciaData()
{
	$id = @$_POST['id'];
    $ambito = @$_POST['ambito'];
    $dataini = @$_POST['dataini'];
    $datafim = @$_POST['datafim'];
    $titulo = @$_POST['titulo'];
    $local = @$_POST['local'];
    $npart = @$_POST['npart'];
    $link = @$_POST['link'];
	$login = @$_POST['login'];
	$alt = @$_POST['alt'];
	$dep = @$_POST['dep'];	

    $db = new Database();
    $db->adminUpdateConferencia($id, $ambito, $dataini, $datafim, $titulo , $local, $npart, $link, $login, $dep, $alt);
}

function saveApresentacaoData()
{
    $id = @$_POST['id'];
    $iconfconv = @$_POST['iconfconv'];
    $iconfpart = @$_POST['iconfpart'];
    $isem = @$_POST['isem'];
    $nconfconv = @$_POST['nconfconv'];
    $nconfpart = @$_POST['nconfpart'];
    $nsem = @$_POST['nsem'];  
	$login = @$_POST['login'];
	$alt = @$_POST['alt'];
	$dep = @$_POST['dep'];	
	
    $db = new Database();
    $db->adminUpdateApresentacao($id, $iconfconv, $iconfpart, $isem, $nconfconv, $nconfpart , $nsem, $login, $dep, $alt);
}

function saveArbRevistaData()
{
    $id = @$_POST['id'];
    $tipo = @$_POST['tipo'];
    $issn = @$_POST['issn'];
    $titulo = @$_POST['titulo'];
    $link = @$_POST['link']; 
	$login = @$_POST['login'];
	$alt = @$_POST['alt'];
	$dep = @$_POST['dep'];	

    $db = new Database();
    $db->adminUpdateArbRevista($id, $tipo, $issn, $titulo, $link, $login, $dep, $alt);
}

function savePremioData()
{
	$id = @$_POST['id'];
    $tipo = @$_POST['tipo'];
    $nome = @$_POST['nome'];
    $contexto = @$_POST['contexto'];
    $montante = @$_POST['montante'];    
    $link = @$_POST['link'];
	$login = @$_POST['login'];
	$alt = @$_POST['alt'];
	$dep = @$_POST['dep'];	

    $db = new Database();
    $db->adminUpdatePremio($id, $tipo, $nome, $contexto, $montante, $link, $login, $dep, $alt);
}

function saveSCData()
{	
    $id = @$_POST['id'];
    $tipo = @$_POST['tipo'];
    $datainicio = @$_POST['datainicio'];
    $datafim = @$_POST['datafim'];
    $cargo = @$_POST['cargo'];    
    $link = @$_POST['link'];
    $nome = @$_POST['nome'];
	$login = @$_POST['login'];
	$alt = @$_POST['alt'];
	$dep = @$_POST['dep'];	

    $db = new Database();
    $db->adminUpdateSC($id, $tipo, $datainicio, $datafim, $cargo, $link, $nome, $login, $dep, $alt);
}

function saveRedeData()
{	
    $id = @$_POST['id'];
    $tipo = @$_POST['tipo'];
    $datainicio = @$_POST['datainicio'];
    $datafim = @$_POST['datafim'];
    $link = @$_POST['link'];
    $nome = @$_POST['nome'];
	$login = @$_POST['login'];
	$alt = @$_POST['alt'];
	$dep = @$_POST['dep'];	

    $db = new Database();
    $db->adminUpdateRede($id, $tipo, $datainicio, $datafim, $link, $nome, $login, $dep, $alt);
}

function saveMissoesData()
{
 	$id = @$_POST['id'];
 	$datainicio = @$_POST['datainicio'];
    $datafim = @$_POST['datafim'];
    $motivacao = @$_POST['motivacao'];    
    $instituicao = @$_POST['instituicao'];
    $pais = @$_POST['pais'];
    $ambtese = @$_POST['ambtese'];
	$login = @$_POST['login'];
	$alt = @$_POST['alt'];
	$dep = @$_POST['dep'];	

    $db = new Database();
    $db->adminUpdateMissao($id,$datainicio, $datafim, $motivacao, $instituicao, $pais, $ambtese, $login, $dep, $alt);
}

function saveOrientacaoData()
{	
 	$id = @$_POST['id'];
 	$tipo = @$_POST['tipo'];
    $tipoori = @$_POST['tipoori'];
    $estado = @$_POST['estado'];    
    $titulo = @$_POST['titulo'];
	$login = @$_POST['login'];
	$alt = @$_POST['alt'];
	$dep = @$_POST['dep'];	

    $db = new Database();
    $db->adminUpdateOrientacao($id,$tipo, $tipoori, $estado, $titulo, $login, $dep, $alt);
}

function saveJuriData()
{
    $id = @$_POST['id'];

    $agrargfmup = @$_POST['agrargfmup'];
    $agrvogalfmup = @$_POST['agrvogalfmup'];
    $agrargext = @$_POST['agrargext'];
    $agrvogalext = @$_POST['agrvogalext'];

    $doutargfmup = @$_POST['doutargfmup'];
    $doutvogalfmup = @$_POST['doutvogalfmup'];
    $doutargext = @$_POST['doutargext'];
    $doutvogalext = @$_POST['doutvogalext'];

    $mesargfmup = @$_POST['mesargfmup'];
    $mesvogalfmup = @$_POST['mesvogalfmup'];
	$mesargext = @$_POST['mesargext'];
	$mesvogalext = @$_POST['mesvogalext'];

	$mesintfmup = @$_POST['mesintfmup'];
	$mesintvogalfmup = @$_POST['mesintvogalfmup'];
	$mesintargext = @$_POST['mesintargext'];
	$mesintvogalext = @$_POST['mesintvogalext'];
	
	$login = @$_POST['login'];
	$alt = @$_POST['alt'];
	$dep = @$_POST['dep'];	

    $db = new Database();
    $db->adminUpdateJuri($id, $agrargfmup, $agrvogalfmup, $agrargext, $agrvogalext, $doutargfmup, $doutvogalfmup, $doutargext, $doutvogalext, $mesargfmup, $mesvogalfmup, $mesargext, $mesvogalext, $mesintfmup, $mesintvogalfmup, $mesintargext, $mesintvogalext, $login, $dep, $alt);
}

function saveOutActData()
{
	$id = @$_POST['id'];
    $texapoio = @$_POST['texapoio'];
    $livdidaticos = @$_POST['livdidaticos'];
    $livdivulgacao = @$_POST['livdivulgacao'];
    $app = @$_POST['app'];
    $webp = @$_POST['webp'];
	$login = @$_POST['login'];
	$alt = @$_POST['alt'];
	$dep = @$_POST['dep'];	
    
    $db = new Database();
    $db->adminUpdateOutAct($id, $texapoio, $livdidaticos, $livdivulgacao, $app, $webp, $login, $dep, $alt);
}

function saveAcaoDivulgacaoData()
{
    $id = @$_POST['id'];
    $titulo = @$_POST['titulo'];
    $data = @$_POST['data'];
    $local = @$_POST['local'];
    $npart = @$_POST['npart'];
	$login = @$_POST['login'];
	$alt = @$_POST['alt'];
	$dep = @$_POST['dep'];	
    
    $db = new Database();
    $db->adminUpdateAcaoDivulgacao($id, $titulo, $data, $local, $npart, $login, $dep, $alt);
}

function saveProdutoData()
{
	$id = @$_POST['id'];
    $tipo = @$_POST['tipo'];
    $preco = @$_POST['preco'];
	$login = @$_POST['login'];
	$alt = @$_POST['alt'];
	$dep = @$_POST['dep'];	
    
    $db = new Database();
    $db->adminUpdateProduto($id, $tipo, $preco, $login, $dep, $alt);
}

/**
* DELETE
*/

function deleteInvestigador()
{
	$id = @$_POST['id'];	
	$login = @$_POST['login'];
	$db = new Database();
	$db->adminDeleteInvestigador($id, $login);
	
	$sql = "UPDATE accoes SET estado = 2 WHERE idreg = $id AND tabela = 1;";
	if ($login == "admin" || $login == "sciman")
		$db->executeQuery($sql);
}

function deleteHabilitacao()
{
	$id = @$_POST['id'];
	$login = @$_POST['login'];
	$db = new Database();
	$db->adminDeleteHabilitacao($id, $login);
	
	$sql = "UPDATE accoes SET estado = 2 WHERE idreg = $id AND tabela = 2;";
	if ($login == "admin" || $login == "sciman")
		$db->executeQuery($sql);
}

function deleteAgregacao()
{
	$id = @$_POST['id'];	
	$login = @$_POST['login'];
	$db = new Database();
	$db->adminDeleteAgregacao($id,$login);
	
	$sql = "UPDATE accoes SET estado = 2 WHERE idreg = $id AND tabela = 3;";
	if ($login == "admin" || $login == "sciman")
		$db->executeQuery($sql);
}

function deleteCaracterizacao()
{
	$id = @$_POST['id'];
	$login = @$_POST['login'];
	$db = new Database();	
	$db->adminDeleteCaracterizacao($id, $login);
	
	$sql = "UPDATE accoes SET estado = 2 WHERE idreg = $id AND (tabela = 4 OR tabela = 27);";
	if ($login == "admin" || $login == "sciman")
		$db->executeQuery($sql);
}

function deleteDirCurso()
{
	$id = @$_POST['id'];
	$login = @$_POST['login'];
	$db = new Database();	
	$db->adminDeleteDirCurso($id, $login);
	
	$sql = "UPDATE accoes SET estado = 2 WHERE idreg = $id AND tabela = 5;";
	if ($login == "admin" || $login == "sciman")
		$db->executeQuery($sql);
}

function deleteColIntAct()
{
	$id = @$_POST['id'];
	$login = @$_POST['login'];
	$db = new Database();
	$db->adminDeleteColIntAct($id);
	
	$sql = "UPDATE accoes SET estado = 2 WHERE idreg = $id AND tabela = 6;";
	if ($login == "admin" || $login == "sciman")
		$db->executeQuery($sql);
}

function deleteColExtAct()
{
	$id = @$_POST['id'];
	$login = @$_POST['login'];
	$db = new Database();
	$db->adminDeleteColExtAct($id);
	
	$sql = "UPDATE accoes SET estado = 2 WHERE idreg = $id AND tabela = 7;";
	if ($login == "admin" || $login == "sciman")
		$db->executeQuery($sql);
}

function deleteUniInv()
{
	$id = @$_POST['id'];
	$login = @$_POST['login'];
	$db = new Database();
	$db->adminDeleteUniInv($id);
	
	if ($login == "admin" || $login == "sciman") 	{	
		$sql = "UPDATE accoes SET estado = 2 WHERE idreg = $id AND tabela = 8;";
		$db->executeQuery($sql);
		
		$sql = "DELETE FROM repetidos_uniInv WHERE id_original = $id OR id_repetido = $id;";
		$db->executeQuery($sql);
	}
}

function deletePublicacao()
{
	$id = @$_POST['id'];
	$login = @$_POST['login'];	
	$tabela = @$_POST['tabela'];
	$db = new Database();
	$db->adminDeletePublicacao($id,$login,$tabela);
	
	if ($login == "admin" || $login == "sciman") {		
		$sql = "UPDATE accoes SET estado = 2 WHERE idreg = $id AND tabela = $tabela;";
		$db->executeQuery($sql);
		
		$sql = "DELETE FROM repetidos_pub WHERE id_original = $id OR id_repetido = $id;";
		$db->executeQuery($sql);
	}
	
}

function deleteProjecto()
{
	$id = @$_POST['id'];
	$login = @$_POST['login'];
	$db = new Database();
    $db->adminDeleteProjecto($id, $login);
	
	if ($login == "admin" || $login == "sciman") {
		$sql = "UPDATE accoes SET estado = 2 WHERE idreg = $id AND tabela = 14;";
		$db->executeQuery($sql);
		
		$sql = "DELETE FROM repetidos_prj WHERE id_original = $id OR id_repetido = $id;";
		$db->executeQuery($sql);
	}
}

function deleteArbProjecto()
{
	$id = @$_POST['id'];
	$db = new Database();
	$db->adminDeleteArbProjecto($id);
}

function deleteConferencia()
{
	$id = @$_POST['id'];
	$login = @$_POST['login'];
	$db = new Database();
	$db->adminDeleteConferencia($id, $login);
	
	if ($login == "admin" || $login == "sciman") {
		$sql = "UPDATE accoes SET estado = 2 WHERE idreg = $id AND tabela = 15;";
		$db->executeQuery($sql);
		
		$sql = "DELETE FROM repetidos_cnf WHERE id_original = $id OR id_repetido = $id;";
		$db->executeQuery($sql);
	}
}

function deleteApresentacao()
{
	$id = @$_POST['id'];
	$login = @$_POST['login'];
	$db = new Database();
	$db->adminDeleteApresentacao($id, $login);	
	
	$sql = "UPDATE accoes SET estado = 2 WHERE idreg = $id AND tabela = 16;";
	if ($login == "admin" || $login == "sciman")
		$db->executeQuery($sql);
}

function deleteArbRevista()
{
	$id = @$_POST['id'];
	$login = @$_POST['login'];
	$db = new Database();
	$db->adminDeleteArbRevista($id, $login);	
	
	if ($login == "admin" || $login == "sciman") {
		$sql = "UPDATE accoes SET estado = 2 WHERE idreg = $id AND tabela = 17;";
		$db->executeQuery($sql);
		
		$sql = "DELETE FROM repetidos_arbRevista WHERE id_original = $id OR id_repetido = $id;";
		$db->executeQuery($sql);
	}
}

function deletePremio()
{
	$id = @$_POST['id'];
	$login = @$_POST['login'];
	$db = new Database();
	$db->adminDeletePremio($id, $login);	
	
	if ($login == "admin" || $login == "sciman") {
		$sql = "UPDATE accoes SET estado = 2 WHERE idreg = $id AND tabela = 18;";
		$db->executeQuery($sql);
		
		$sql = "DELETE FROM repetidos_premios WHERE id_original = $id OR id_repetido = $id;";
		$db->executeQuery($sql);
	}
}

function deleteSC()
{
	$id = @$_POST['id'];
	$login = @$_POST['login'];
	$db = new Database();
	$db->adminDeleteSC($id, $login);	
	
	$sql = "UPDATE accoes SET estado = 2 WHERE idreg = $id AND tabela = 19;";
	if ($login == "admin" || $login == "sciman")
		$db->executeQuery($sql);
}

function deleteRede()
{
	$id = @$_POST['id'];
	$login = @$_POST['login'];
	$db = new Database();
	$db->adminDeleteRede($id, $login);	
	
	$sql = "UPDATE accoes SET estado = 2 WHERE idreg = $id AND tabela = 20;";
	if ($login == "admin" || $login == "sciman")
		$db->executeQuery($sql);
}

function deleteMissao()
{
	$id = @$_POST['id'];
	$login = @$_POST['login'];
	$db = new Database();
	$db->adminDeleteMissao($id, $login);	
	
	$sql = "UPDATE accoes SET estado = 2 WHERE idreg = $id AND tabela = 21;";
	if ($login == "admin" || $login == "sciman")
		$db->executeQuery($sql);
}

function deleteOrientacao()
{
	$id = @$_POST['id'];
	$login = @$_POST['login'];
	$db = new Database();
	$db->adminDeleteOrientacao($id, $login);
	
	$sql = "UPDATE accoes SET estado = 2 WHERE idreg = $id AND tabela = 22;";
	if ($login == "admin" || $login == "sciman")
		$db->executeQuery($sql);
}

function deleteJuri()
{
	$id = @$_POST['id'];
	$login = @$_POST['login'];
	$db = new Database();
	
	$db->adminDeleteJuri($id, $login);$sql = "UPDATE accoes SET estado = 2 WHERE idreg = $id AND tabela = 23;";
	if ($login == "admin" || $login == "sciman")
		$db->executeQuery($sql);
}

function deleteOutAct()
{
	$id = @$_POST['id'];
	$login = @$_POST['login'];
	$db = new Database();
	$db->adminDeleteOutAct($id, $login);
	
	$sql = "UPDATE accoes SET estado = 2 WHERE idreg = $id AND tabela = 24;";
	if ($login == "admin" || $login == "sciman")
		$db->executeQuery($sql);
}

function deleteAcaoDivulgacao()
{
	$id = @$_POST['id'];
	$login = @$_POST['login'];
	$db = new Database();
	$db->adminDeleteAcaoDivulgacao($id, $login);	
	
	if ($login == "admin" || $login == "sciman") {
		$sql = "UPDATE accoes SET estado = 2 WHERE idreg = $id AND tabela = 25;";
		$db->executeQuery($sql);
		
		$sql = "DELETE FROM repetidos_acaoDiv WHERE id_original = $id OR id_repetido = $id;";
		$db->executeQuery($sql);
	}
}

function deleteProduto()
{
	$id = @$_POST['id'];
	$login = @$_POST['login'];
	$db = new Database(); 
	$db->adminDeleteProduto($id, $login);	
	
	$sql = "UPDATE accoes SET estado = 2 WHERE idreg = $id AND tabela = 26;";
	if ($login == "admin" || $login == "sciman")
		$db->executeQuery($sql);
}

/**
*	VALIDAÇÃO REPETIDOS
*/

function validateRepetidosProjecto()
{
	$orig = @$_POST['orig'];
	$rep = @$_POST['rep'];
	$db = new Database(); 
	$db->adminValidateRepetidosProjecto($orig, $rep);	
}

function validateRepetidosPublicacao()
{
	$orig = @$_POST['orig'];
	$rep = @$_POST['rep'];
	$db = new Database(); 
	$db->adminValidateRepetidosPublicacao($orig, $rep);	
}

function validateRepetidosConferencia() 
{
	$orig = @$_POST['orig'];
	$rep = @$_POST['rep'];
	$db = new Database(); 
	$db->adminValidateRepetidosConferencia($orig, $rep);	
}

function validateRepetidosAcaoDivulgacao()
{
	$orig = @$_POST['orig'];
	$rep = @$_POST['rep'];
	$db = new Database(); 
	$db->adminValidateRepetidosAcaoDivulgacao($orig, $rep);
}

function validateRepetidosPremio()
{
	$orig = @$_POST['orig'];
	$rep = @$_POST['rep'];
	$db = new Database(); 
	$db->adminValidateRepetidosPremio($orig, $rep);
}

function validateRepetidosArbitragensRevistas()
{
	$orig = @$_POST['orig'];
	$rep = @$_POST['rep'];
	$db = new Database(); 
	$db->adminValidateRepetidosArbitragensRevistas($orig, $rep);
}

function validateRepetidosUnidadesInvestigacao()
{
	$orig = @$_POST['orig'];
	$rep = @$_POST['rep'];
	$db = new Database(); 
	$db->adminValidateRepetidosUnidadesInvestigacao($orig, $rep);
}

function validateTodosRepetidosProjecto()
{
	$ids = @$_POST['ids'];
	$db = new Database(); 
	$db->adminValidateTodosRepetidosProjecto($ids);
}

function validateTodosRepetidosPublicacao()
{
	$ids = @$_POST['ids'];
	$db = new Database(); 
	$db->adminValidateTodosRepetidosPublicacao($ids);
}

function validateTodosRepetidosConferencia()
{
	$ids = @$_POST['ids'];
	$db = new Database(); 
	$db->adminValidateTodosRepetidosConferencia($ids);
}

function validateTodosRepetidosAcaoDivulgacao()
{
	$ids = @$_POST['ids'];
	$db = new Database(); 
	$db->adminValidateTodosRepetidosAcaoDivulgacao($ids);
} 

function validateTodosRepetidosPremio()
{
	$ids = @$_POST['ids'];
	$db = new Database(); 
	$db->adminValidateTodosRepetidosPremio($ids);
} 

function validateTodosRepetidosArbitragensRevistas()
{
	$ids = @$_POST['ids'];
	$db = new Database(); 
	$db->adminValidateTodosRepetidosArbitragensRevistas($ids);
}

function validateTodosRepetidosUnidadesInvestigacao()
{
	$ids = @$_POST['ids'];
	$db = new Database(); 
	$db->adminValidateTodosRepetidosUnidadesInvestigacao($ids);
}

function saveObs()
{	
	$id = @$_POST['id'];
	$login = @$_POST['login'];
	$texto = @$_POST['texto'];
	$tabela = @$_POST['tabela'];
	$dep = @$_POST['dep'];	
	$db = new Database(); 
	$db->saveObservacao($id, $login, $texto, $tabela, $dep);	
}

function validaAcao()
{
	
	$id = @$_POST['id'];
	
	error_log(">>>>>>>>>>>>>>".$id,0);
	$db = new Database(); 
	$db->adminValidaAcao($id);	
}

function eliminaAcao()
{
	$id = @$_POST['id'];
	$db = new Database(); 
	$db->adminEliminaAcao($id);	
}




?>