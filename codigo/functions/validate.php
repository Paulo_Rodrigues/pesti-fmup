<?php

	ini_set('display_errors',1);
	ini_set('display_startup_errors',1);
	error_reporting(-1);
	
require_once '../classlib/Questionario.class.inc';
require_once '../classlib/Database.class.inc';


if (isset($_POST["login"]))
    $login = @$_POST["login"];
else
    $login = "";

if (isset($_POST["password"]))
    $password = @$_POST["password"];
else
    $pass = "";
	
if ($login && $password) {	
    if ($login == 'admin' && $password == 'admin2012') {
        session_unset();
        $_SESSION = array();
        //unset($_SESSION['questionario']);
        @session_destroy();
        //setcookie(session_name(), "", 0, "/");
        session_start();
        $_SESSION['login'] = $login;
        $err = "";
        auditoria($login, $err, "LOGIN", "", "");
        header("Location: ../forms/admin/admin.php");	
  } else if ( authenticate($login, $password) && checkDirDep($login) ) {	  
		session_unset();
		@$_SESSION = array();
		@session_destroy();
		session_start();
		@$_SESSION['login'] = $login;
		$err = "";
		auditoria($login, $err, "LOGIN", "", "");
		header("Location: ../forms/chooseMode.php");
		exit();				
	} else if (authenticate($login, $password)) {
        if (dataPreenchimento() || checkLacrado($login)==1) {            
			session_unset();
            $_SESSION = array();
            //unset($_SESSION['questionario']);
            @session_destroy();
            //setcookie(session_name(), "", 0, "/");
            session_start();
            $questionario = new Questionario($login);
            $_SESSION['questionario'] = serialize($questionario);

            $err = "";
			echo "sai";
            //auditoria($login,$err,"LOGIN","","");
            auditorialogin($login, $err, "LOGIN", "", "");
			
            header("Location: ../forms/questionario.php");			
		} else {
			
			
            $err = "O prazo de preenchimento terminou, neste momento!!! apenas é permitido o acesso aos investigadores que tenham lacrado o levantamento!>".checkLacrado($login)."<".$login;
            auditoria($login, $err, "LOGIN", "", "");

            session_unset();
            $_SESSION = array();
            unset($_SESSION['questionario']);
            session_destroy();
            $erro = '405';
            $_SESSION["erros"] = $erro;
             //header("Location: ../index.php?erro=401");
            header("Location: ../index.php?erro=405");
        } 
	} else {
		$err = "Erro na autenticação!";
		auditoria($login, $err, "LOGIN", "", "");
		session_unset();
		$_SESSION = array();
		unset($_SESSION['questionario']);
		$erro = 'passerror';
		$_SESSION["erros"] = $erro;
		//header("Location: ../index.php?erro=401");
		header("Location: ../index.php?erro=404");
	}
} else {
		$err = "Erro na autenticação!";
		auditoria($login, $err, "LOGIN", "", "");
		session_unset();
		$_SESSION = array();
		unset($_SESSION['questionario']);
		$erro = 'passerror';
		$_SESSION["erros"] = $erro;
		//header("Location: ../index.php?erro=401");
		header("Location: ../index.php?erro=404");
	}

function authenticate($login, $password) {
    $message = "";

    if (pam_auth($login, $password, $message) || ($login=='altamiro' && $password=='dep')) {
        return true;
    } else {
        return false;
    }
}

function checkLacrado($login) {
    $estado = 0;

    $db = new Database();
    $lacrado = $db->checkLacrado($login);
    while ($row = mysql_fetch_assoc($lacrado)) {
        $estado = $row["estado"];
    }

    return $estado;
}

function checkDirDep($login) {
	$valid = false;
    $db = new Database();
    $dirDep = $db->checkDirDepLogin($login);
	while ($row = mysql_fetch_assoc($dirDep)) {
        $valid = true;
    }
    return $valid;
}


function dataPreenchimento() {
	
	return true;
}


?>
