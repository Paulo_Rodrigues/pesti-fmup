<?php

/**
 * Parsing de refer?ncias.
 * 
 * Este script, faz parsing de refer?ncias. 
 * 
 * @author Jos? Filipe Lopes Santos <jfilipe@med.up.pt>
 * @since 29-12-2011
 * @version 1.0 - data da ?lt. actualiza??o: 29-12-2011
 * @package imp_endnote
 * @subpackage outputs
 */

require_once '../classlib/core.inc';

require_once '../classlib/Database.class.inc';

/* ------------------------------ oOo ----------------------------
					Inicializa??o de vari?veis
   ------------------------------ oOo ---------------------------- */


function parsefileRIS($idinv){

$target_path = "../pubfiles/".$idinv;

if(!file_exists ( $target_path)){
	mkdir($target_path);
}


$target_path = $target_path ."/". basename( $idinv."_RIS_".$_FILES['uploadedfileRIS']['name']."_".date("dmY_H:i")); 

if(move_uploaded_file($_FILES['uploadedfileRIS']['tmp_name'], $target_path)) {
    //echo "The file ".  basename( $_FILES['uploadedfileISI']['name'])." has been uploaded";
} else{
    $error_msg="There was an error uploading the file, please try again!";
	echo "There was an error uploading the file, please try again!";
}

if (empty($error_msg)) $error_msg = ""; // mensagem de erro
$linhas = array(); // array com as linhas do ficheiro
$refs = array(); // array com os dados das refer?ncias


/* ------------------------------ oOo ----------------------------
					Obter as refer?ncias do ficheiro
   ------------------------------ oOo ---------------------------- */

// obter os dados do ficheiro
//$linhas = file(UPLOAD_FILES_PATH.$file) or die("N?o ? poss?vel abrir o ficheiro ".UPLOAD_FILES_PATH.$file);
$linhas = file($target_path) or die("Não é possível abrir o ficheiro ".$target_path);

/* ------------------------------ oOo ----------------------------
						Parsing das refer?ncias
   ------------------------------ oOo ---------------------------- */

$ref = ""; // string com a referÍncia
$type = ""; // tipo de referÍncia
$titulo = ""; // tÌtulo
$revista = ""; // tÌtulo da revista
$revista_abrev = ""; // tÌtulo da revista abreviado
$volume = ""; // volume
$issue = ""; // issue / sequÍncia
$pag_inicio = ""; // p·gina de inÌcio
$pag_fim = ""; // p·gina de fim
$ano = ""; // ano em que foi publicado
$num_serie = ""; // n.∫ de sÈrie (ISSN)
$autores = ""; // string com os autores
$autores_moradas = ""; // string com as moradas dos autores
$abstract = ""; // abstract
$keywords = ""; // palavras-chave
$notes = ""; // notas
$url = ""; // URL
$titulo_serie = ""; // tÌtulo da sÈrie
$data_acesso = ""; // data do acesso
$local = ""; // local de publicaÁ„o

$count = array(); // array com contadores de referÍncias por tipo
$append = array(); // array de booleanos que indicam se È para concatenar
$append["global"] = false;
$append["autores"] = false;
$append["autores_moradas"] = false;
$append["abstract"] = false;
$append["keywords"] = false;
$append["notes"] = false;

foreach ($linhas as $index=>$linha){
	    
	if (ereg("^(TY )( - )?([a-zA-Z]+)",$linha,$regs)){ // inÌcio da referÍncia
		$append["global"] = true;
		$type = $regs[3];
		continue;
	}

	if (ereg("^ER",$linha)){ // fim da referÍncia
		
		// inicializar posiÁ„o
		if (empty($count[$type])) $count[$type] = 0;

		// guardar dados
		$refs[$type][$count[$type]]["ref"] = $ref;                
		$refs[$type][$count[$type]]["titulo"] = $titulo;
        $refs[$type][$count[$type]]["revista"] = $revista;
        $refs[$type][$count[$type]]["revista_abrev"] = $revista_abrev;
        $refs[$type][$count[$type]]["volume"] = $volume;
        $refs[$type][$count[$type]]["issue"] = $issue;
		$refs[$type][$count[$type]]["pag_inicio"] = $pag_inicio;
		$refs[$type][$count[$type]]["pag_fim"] = $pag_fim;
		$refs[$type][$count[$type]]["ano"] = $ano;
        $refs[$type][$count[$type]]["num_serie"] = $num_serie;
        $refs[$type][$count[$type]]["autores"] = $autores;
        $refs[$type][$count[$type]]["autores_moradas"] = $autores_moradas;
        $refs[$type][$count[$type]]["abstract"] = $abstract;
        $refs[$type][$count[$type]]["keywords"] = $keywords;
		$refs[$type][$count[$type]]["notes"] = $notes;
        $refs[$type][$count[$type]]["url"] = $url;
		$refs[$type][$count[$type]]["titulo_serie"] = $titulo_serie;
        $refs[$type][$count[$type]]["data_acesso"] = $data_acesso;
        $refs[$type][$count[$type]]["local"] = $local;		
		
		// incrementar o contador desse tipo
		$count[$type]++;
		
		// inicializar vari·veis
		$ref = "";                
                $titulo = ""; 
                $revista = ""; 
                $revista_abrev = ""; 
                $volume = ""; 
                $issue = ""; 
                $pag_inicio = ""; 
                $pag_fim = ""; 
                $ano = ""; 
                $num_serie = ""; 
                $autores = ""; 
                $autores_moradas = ""; 
                $abstract = ""; 
                $keywords = ""; 
                $notes = ""; 
                $url = ""; 
                $titulo_serie = ""; 
                $data_acesso = ""; 
                $local = ""; 
		
                $append["global"] = false;
                $append["autores"] = false;
                $append["autores_moradas"] = false;
                $append["abstract"] = false;
                $append["keywords"] = false;
                $append["notes"] = false;                                
	} 
	
	if ($append["global"]){ 

		// concatenar linhas da referÍncia
		$ref .= $linha;

                
		////// TÌtulo //////
		if (ereg("^(T1 )( - )?(\\[?[a-zA-Z0-9]+.*)",$linha,$regs))
                    $titulo = $regs[3];

                ///// TÌtulo da revista //////
		if (ereg("^(JF )( - )?([a-zA-Z0-9]+.*)",$linha,$regs))
                    $revista = $regs[3];
                
                ///// TÌtulo da revista abreviadp //////
		if (ereg("^(JA )( - )?([a-zA-Z0-9]+.*)",$linha,$regs))
                    $revista_abrev = $regs[3];
                
		///// Volume /////
		if (ereg("^(VL )( - )?([a-zA-Z0-9]+.*)",$linha,$regs))
			$volume = $regs[3];
			
		///// Issue / sequÍncia /////
		if (ereg("^(IS )( - )?([a-zA-Z0-9]+.*)",$linha,$regs))
			$issue = $regs[3];
                
		///// P·gina de inÌcio /////
		if (ereg("^(SP )( - )?([a-zA-Z0-9]+.*)",$linha,$regs))
			$pag_inicio = $regs[3];
			
		///// P·gina de fim /////
		if (ereg("^(EP )( - )?(.*)",$linha,$regs))
			$pag_fim = $regs[3];

		///// Ano em que foi publicado /////
		if (ereg("^(PY )( - )?([a-zA-Z0-9]+.*)",$linha,$regs))
			$ano = $regs[3];
                
		///// N.∫ de sÈrie /////
		if (ereg("^(SN )( - )?([a-zA-Z0-9]+.*)",$linha,$regs))
			$num_serie = $regs[3];
                                
		////// autores /////
		if (ereg("^(AU )( - )?([a-zA-Z0-9]+.*)",$linha,$regs)){ // inÌcio da tag autores 
                        
                        if ($append["autores"])
                            $autores .= " ; ".$regs[3];
                        else
                            $autores = $regs[3];                      
                                                    
			$append["autores"] = true;
			continue;
		}
		
		////// moradas dos autores /////
		if (ereg("^(AD )( - )?([a-zA-Z,. ]+)",$linha,$regs)){ // inÌcio da tag moradas de autores 

                        if ($append["autores_moradas"])
                            $autores_moradas .= " ; ".$regs[3];
                        else
                            $autores_moradas = $regs[3];                      
                                        
			$append["autores_moradas"] = true;
			continue;
		}
                
		///// Abstract /////
		if (ereg("^(AB )( - )?([a-zA-Z0-9]+.*)",$linha,$regs)){
			$abstract = $regs[3];
			$append["abstract"] = true;
			continue;
		}
		if (ereg("^[A-Z0-9]{2} ",$linha)) // se encontrar outra tag p·ra de concatenar
			$append["abstract"] = false;
		if ($append["abstract"])
			$abstract .= substr($linha,2,strlen($linha));
                
		///// Palavras chave (keywords) /////
		if (ereg("^(KW )( - )?([a-zA-Z0-9]+.*)",$linha,$regs)){
                    
                        if ($append["keywords"])
                            $keywords .= " ; ".$regs[3];
                        else
                            $keywords = $regs[3];                      
                    
			$append["keywords"] = true;
			continue;
		}
                
		///// Notas /////
		if (ereg("^(N1 )( - )?([a-zA-Z0-9]+.*)",$linha,$regs)){
                    
                        if ($append["notes"])
                            $notes .= "<br>".$regs[3];
                        else
                            $notes = $regs[3];                      
                                    
			$append["notes"] = true;
			continue;
		}
		if (ereg("^[A-Z0-9]{2} ",$linha)) // se encontrar outra tag p·ra de concatenar
			$append["notes"] = false;
		if ($append["notes"])
			$notes .= $linha;
			
		///// URL /////
		if (ereg("^(UR )( - )?(https?://.*)",$linha,$regs))
			$url = $regs[3];

                ///// TÌtulo da sÈrie //////
		if (ereg("^(T3 )( - )?([a-zA-Z0-9]+.*)",$linha,$regs))
                    $titulo_serie = $regs[3];
                
                ///// Data de acesso //////
		if (ereg("^(Y2 )( - )?([a-zA-Z0-9]+.*)",$linha,$regs))
                    $data_acesso = $regs[3];
                
                ///// Local //////
		if (ereg("^(CY )( - )?([a-zA-Z0-9]+.*)",$linha,$regs))
                    $local = $regs[3];
	}

$anoErrado=0;

$db = new Database();

$db->apagaRefsISI($idinv);

foreach ($refs as $type=>$info_type){
	foreach ($info_type as $index => $info){
		$num = $index + 1;
		
		$pp=intval(trim($info["primpagina"]));
		$up=intval(trim($info["ultpagina"]));
		
		if($up<$pp){
			$np=abs($up-substr($pp,-1*strlen($up)))+1;
		}else{
			$np=$up-$pp+1;	
		}
		
		
		
		switch($type){
			case 'J': { // Opção de entrada
				if($np<=2){
					$tipofmup="JA";
				}else{
					if(trim($info["conftitle"])==""){
						$tipofmup="J";
					}else{
						$tipofmup="PRP";
					}
				}
			}
			break;
			case 'B': { // Opção de entrada		
				if($np<=2){
					$tipofmup="OA";
				}else{
					if(trim($info["conftitle"])==""){
						$tipofmup="CL";
					}else{
						$tipofmup="CP";
					}
				}
			}
			break;
			case 'S': { // Opção de entrada		
				if($np<=2){
					$tipofmup="OA";
				}else{
					if(trim($info["conftitle"])==""){
						$tipofmup="CL";
					}else{
						$tipofmup="CP";
					}
				}
			}
			break;
			case 'P': { // Opção de entrada
				$tipofmup="P";
			}
			break;
			default:{ //echo "DEFAULT";
		       $tipofmup="UND";
		    }
			break;
		}
		
		if($info["primpaginaoriginal"]=="" && $info["ar"]==""){
			$info["primpaginaoriginal"]=$info["primpaginaoriginal"].$info["doi"];
		}
		
		if($info["issue"]==""){
			if($info["suplement"]!="")
				$info["issue"]="Sup: ".$info["suplement"];
			else if ($info["special_issue"]!=""){
				$info["issue"]="Esp Iss: ".$info["special_issue"];
			}
		}	
		
		if(trim($info["ano"])=='2013' || $tipofmup=='P'){
			$db->insertUmaPublicacao(
				$idinv,
				$type,
				str_replace("'", "\'", $info["nomepublicacao"]),
				$info["issn"],
				"",
				"",
				$info["isbn"],
				str_replace("'", "\'", $info["titulo"]),
				str_replace("'", "\'", $info["autores"]),
				$info["volume"],
				$info["issue"],
				$info["ar"],
				$info["colecao"]."/".str_replace("'", "\'", $info["conftitle"]),
				$info["ar"].$info["primpaginaoriginal"],
				$info["ultpaginaoriginal"],
				"",
				$info["citacoes"],
				$info["npatente"],
				$info["datapatente"],
				$info["ipc"],
				"",
				str_replace("'", "\'", $info["conftitle"]),
				$info["language"],
				"editor",
				$tipofmup,
				"2","0","",0);
		}else{
			$anoErrado++;
		}

	}

}

if($anoErrado!=0){
	return ("Foram ignoradas ".$anoErrado." Publicações dado que o ano de publicação está errado.");
}

}

?>

