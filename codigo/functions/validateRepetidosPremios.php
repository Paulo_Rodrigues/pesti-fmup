<?php

function validateRepetidosPremios($premios)
{
	$premios2 = $premios;
	$final=array();
	
	while ( list($key, $val) = each($premios) ) {	
		$final[$key]=array();
		
		while (list($key2, $val2) = each($premios2)) {

			$equal = equalsPremios($val,$val2);
			if ($equal)
			{				
				$final[$key][]=$premios2[$key2];	  
				unset($premios2[$key2]);				  
				unset($premios[$key2]);
				continue;				
			}

			//Nome
			similar_text(strtolower($val->nome), strtolower($val2->nome), $levNome);

			//Contexto
			$c1 = strtolower($val->contexto);
			$c2 = strtolower($val2->contexto);
			$levContexto = levenshtein($c1, $c2);

			if(($levNome >= 80) && ($levContexto >= 0 && $levContexto <= 5))
			{				
				$final[$key][]=$premios2[$key2];	  
				unset($premios2[$key2]);				  
				unset($premios[$key2]);
				continue;			
			} 
						
			similar_text(strtolower($val->nome) . strtolower($val->contexto), strtolower($val2->nome). strtolower($val2->contexto), $levNomeContexto);
			if($levNomeContexto >= 75)
			{				
				$final[$key][]=$premios2[$key2];	  
				unset($premios2[$key2]);				  
				unset($premios[$key2]);
				continue;			
			} 

		}
		unset($premios2[$key]);
		reset($premios2);
	}
	
	$db = new Database();
	$db->executeQuery("TRUNCATE TABLE repetidos_premios");	
	
	foreach ($final as $i => $value) {	
		if( count($final[$i]) == 1) {
			$db->executeQuery("INSERT INTO repetidos_premios (id_original) VALUES (".$final[$i][0]->id.")");
		} else {		
			$j = 0;
			for ($j = $j + 1; $j < count($final[$i]); $j++ ) {	
				$sql = "INSERT INTO repetidos_premios (id_original, id_repetido) VALUES (".$final[$i][0]->id.",".$final[$i][$j]->id.");";
				$db->executeQuery($sql);
			}
		}
	}
}


/**
Autor: Paulo Rodrigues (prodrigues@med.up.pt; paulomiguelarodrigues@gmail.com)
Data: 07/04/2014
Uso: Verifica se 2 Premios são iguais.
*/

function equalsPremios($p1, $p2)
{
    if( $p1->nome == $p2->nome &&
        $p1->contexto == $p2->contexto &&
        $p1->montante == $p2->montante &&
        $p1->tipo == $p2->tipo)
    {
        return true;
    }
    else
    {
        return false;
    }
}

?>