<?php

error_reporting(E_ALL);
ini_set("display_errors", "On");
ini_set("display_startup_errors",1);/**/

require_once '../classlib/Database.class.inc';

if(isset($_POST['nomeBD']) && isset($_POST['tabelas']) && isset($_POST['anoLevantamento'])){
	$flag = true;
	$tabelasSel = $_POST['tabelas'];
	$nome = $_POST['nomeBD'];
	$ano = $_POST['anoLevantamento'];
	$todasTabelas = array();
	$tabelasIDINV = array();
	
	$db = new Database();
	$db->connect();
	
	//todas as tabelas exepto de backup
	$res = $db->nomesTabelas();
	while ($row = mysql_fetch_row($res)) {
		$todasTabelas[] = $row[0];
	}
	
	//todas as tabelas ligadas ao utilizador
	$res = $db->tabelasInformacaoUtilizador();
	while ($row = mysql_fetch_row($res)) {
		$tabelasIDINV[] = $row[0];
	}
	
	//criar DB
	try {
		try {
			//cria DB
			$db->criaDatabase($nome);
		} catch (Exception $e) {
			$flag = false;
			echo "1", $e->getMessage(), "\n";
		}
		if($flag){
			//tabelas que passaram por eliminação de duplicados
			$tabTratadas = array("fase2_Investigador_Premio"=>"premios",'fase2_Investigador_Conferencia'=>'conferencias',
					'fase2_Investigador_Divulgacao'=>'acoesdivulgacao','fase2_Investigador_Projeto'=>'projectosinv',
					'fase2_Investigador_Publicacao'=>'publicacoes','fase2_Investigador_Revista'=>'arbitragensrevistas');//unidadesinvestigacao
			
			foreach($todasTabelas as $tabela){
				if(($key = array_search($tabela, $tabelasSel)) !== false){//se for uma tabela a copiar
						//copia tabela (inclui o criar)
						$lacrados_restricao = "idinv in (select * from loginsquelacraram)";
						
						if(($key = array_search($tabela, $tabTratadas)) !== false){//se for uma tabela ligada a 'fase2_'
								if(!strcmp($tabela,'projectosinv')){//tudo, a não ser quando DATAFIM foi no ano anterior
									$db->copiaTabelaSemRepetidosEmDB($nome,$tabela,$key,"(YEAR(STR_TO_DATE(DATAFIM, '%d-%m-%Y')) >= ".$ano." or DATAFIM like '')");
								}else{
										$db->copiaTabelaSemRepetidosEmDB($nome,$tabela,$key);
								}
						}elseif(!strcmp($tabela,'habilitacoesliterarias')){//tudo para os cursos listados que não foram dados como concluídos
										$db->copiaTabelaEmDB($nome,$tabela,
														"(ANOFIM > ( select ID from anoslectivos where DESCRICAO like '%".($ano-1)."%') or anofim = 0) and $lacrados_restricao ");
						}elseif(!strcmp($tabela,'cargos') 
																						|| !strcmp($tabela,'dircursos')
																						|| !strcmp($tabela,'sociedadescientificas')
																						|| !strcmp($tabela,'redes')){//tudo, a não ser quando DATAFIM foi no ano anterior
										$db->copiaTabelaEmDB($nome,$tabela,"(YEAR(STR_TO_DATE(DATAFIM, '%d-%m-%Y')) >= ".$ano." or DATAFIM like '') and $lacrados_restricao");
						}elseif(!strcmp($tabela,'orientacoes')){
										$db->copiaTabelaEmDB($nome,$tabela,"estado <> 0 and $lacrados_restricao");
						}elseif(($key = array_search($tabela, $tabelasIDINV)) !== false){//se for uma tabela com IDINV
										$db->copiaTabelaEmDB($nome, $tabela, $lacrados_restricao );
						}else{//todas as outras -> cópia normal
										$db->copiaTabelaEmDB($nome,$tabela);
						} 
				}else{//apenas cria estrutura
						$db->criaTabelaParaNovaBD($nome, $tabela);
				}
			}
			//copia VIEWS
			$views = $db->copiaViewsParaNovaBD($nome);
			if(!empty($views)){
				echo "2";//Algumas Views não foram copiadas!
				$flag = false;
			}
			
			//copia functions
			$functions = $db->copiaFunctionsParaNovaBD("$nome");
			if(!empty($functions)){
				echo "3";//Algumas Functions não foram copiadas!
				$flag = false;
			}
			if($flag){
				echo "0"; //sucesso
			}		
		}
			/**/
	} catch (Exception $e) {
		echo $e->getMessage(), "\n";
	}/**/
}else{
	echo "FAIL";
}

?>