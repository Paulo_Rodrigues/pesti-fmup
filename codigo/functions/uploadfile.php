<?php 

/**
 * Upload do ficheiro de refer?ncias. 
 *
 * @author Jos? Filipe Lopes Santos <jfilipe@med.up.pt>
 * @package imp_endnote
 * @subpackage import
 */

require_once '../classlib/core.inc';

/* ----------------------- oOo -----------------------------
				Inicializa??o de vari?veis
   ----------------------- oOo ----------------------------- */

if (empty($fich)) $fich = ""; // nome do ficheiro
if (empty($error_msg)) $error_msg = ""; // mensagem de erro
if (empty($suc_msg)) $suc_msg = ""; // mensagem de sucesso
if (empty($note_empty)) $note_empty = false; // booleano que indica se existem campos obrigat?rios por preencher
if (empty($title)) $title = "Upload do ficheiro de refer?ncias"; // t?tulo do documento


/* ----------------------- oOo -----------------------------
		Verificar se existem campos por preencher
   ----------------------- oOo ----------------------------- */

if (!empty($submit) && $fich == "")
	$note_empty = true;
else 
	$note_empty = false; 


/* ----------------------- oOo -----------------------------
					Fazer upload do ficheiro
   ----------------------- oOo ----------------------------- */

	
if (!empty($submit) && !$note_empty){

	// nome do ficheiro
	$file = $_FILES["fich"]["name"];
	
	echo "FILE->>>".$file;
	
	// obter a extens?o
	$ext = substr($file,strrpos($file,".")+1,strlen($file));
	
	// compor o novo ficheiro de modo correcto
	$date_to_file =  date("d_m_Y_").date("H")."h_".date("i")."m_".date("s")."s";
	$newfile = "referencias_".$date_to_file.".".$ext;
	$newfile_with_path = UPLOAD_FILES_PATH.$newfile;
	
	if (!is_writable(UPLOAD_FILES_PATH))
		$error_msg = "N?o tem permiss?es de escrita no direct?rio <b>".UPLOAD_FILES_PATH."</b>";
	
	if ($error_msg == "" && !copy($_FILES["fich"]["tmp_name"], $newfile_with_path))
   		$error_msg = "Erro ao fazer upload";
   		
	if ($error_msg == "")
		$suc_msg = "Upload feito com sucesso";	
}

if($suc_msg != ""){
	echo "SUCESSO";
}else
	echo "ERRO $error_msg";
	
	echo UPLOAD_FILES_PATH;

echo "
<!-- form com os dados do upload -->
<form name='upload' method='POST' enctype='multipart/form-data'>                                                

        <!-- tabela principal -->
        <table border='0' cellpadding='0' cellspacing='1' align='center'>
                <tr>
                        <td>
                                <!-- tabela com os dados do upload -->
                                <table border='0' cellpadding='0' cellspacing='1' width='100%'>
                                        <tr>
                                                <td class='label'>
                                                        <b>Ficheiro:</b> &nbsp; 
                                                </td>
                                                <td class='bg_cell_field'>
                                                        <input type='file' name='fich' >
                                                </td>
                                        </tr>
                                </table>
                                <!-- fim da tabela com os dados do upload -->
                                                        
                                                
                        </td>
                </tr>
                <tr><td>&nbsp;</td></tr>

                
                <tr>
                        <td align='right'>
                                <input type='submit' name='submit' value='Submeter'>
                        </td>
                </tr>                                   
                <tr><td>&nbsp;</td></tr>
        </table>

                        
</form>



";
	
/* ----------------------- oOo -----------------------------
						  Output
   ----------------------- oOo ----------------------------- 

$t =& new Template_PHPLIB(TEMPLATES_PATH);

$t->setFile(array("page" => "page.html",
				  "form" => "form_upload.html",
				  "note" => "note_with_table.html",
				  "note2" => "note_without_table.html",
				  "p_link" => "p_link.html"));


//************ P?gina (in?cio) *********
$t->setVar("titulo",$title);


if ($suc_msg != ""){ // ocorreu erro
	
	// ********** mensagem de sucesso ***************
	$t->setVar("msg",$suc_msg);
	$t->parse("block_body","note",true);
	
	// *********** Link **************
	$t->setVar(array("link" => "parsing.php?file=$newfile", "text" => "Continuar"));
	$t->parse("block_body","p_link",true);
	
} else { // form para upload

	//********** Form para upload (in?cio)  ***************
	
	// destacar os campos obrigat?rios por preencher
	if (!empty($submit) && $fich == "") $t->setVar("symbol_fich",SYMBOL_EMPTY);
	else $t->setVar("symbol_fich",SYMBOL_NORMAL);
	
	
	//********** Mensagem de erro *************
	if ($error_msg == "" && $note_empty) $error_msg = MSG_EMPTY;
	if ($error_msg != ""){

		$t->setVar(array("colspan" => 1, "msg" => $error_msg));
		$t->parse("block_notes","note2",true);
	}
	
	//******** Form (fim) *************
	$t->parse("block_body","form",true);
}


//******** P?gina (fim) & output *******
$t->pparse("output","page");
*/
