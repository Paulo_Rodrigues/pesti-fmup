<?php

/**
Autor: Paulo Rodrigues (prodrigues@med.up.pt; paulomiguelarodrigues@gmail.com)
Data: 02/04/2014
Uso: Validação de repetidos nos projectos de investigação inseridos no sistema.
*/
function validateRepetidosProjectos($projectos)
{
	$projectos2 = $projectos;
	$final=array();
	
	while ( list($key, $val) = each($projectos) ) {	
		$final[$key]=array();
		
		while (list($key2, $val2) = each($projectos2)) {
			
			$equal = equalsProjectos($val,$val2);	
			
			if ($equal)
			{											
				$final[$key][]=$projectos2[$key2];	  
				unset($projectos2[$key2]);				  
				unset($projectos[$key2]);
				continue;
			}

			//codigo 2 caracteres diferentes
			similar_text(strtolower($val->titulo), strtolower($val2->titulo), $levTitulo);			
			$levCodigo = levenshtein(strtolower($val->codigo), strtolower($val2->codigo));			
			if($levTitulo >= 80 && $levCodigo <= 4)
			{		
				$final[$key][]=$projectos2[$key2];	  
				unset($projectos2[$key2]);				  
				unset($projectos[$key2]);
				continue;
			}

			if($levTitulo >= 85)
			{						
				$final[$key][]=$projectos2[$key2];	  
				unset($projectos2[$key2]);				  
				unset($projectos[$key2]);
				continue;
			}			
		}		
		unset($projectos2[$key]);
		reset($projectos2);
	}
	
	$db = new Database();
	$db->executeQuery("TRUNCATE TABLE repetidos_prj");	
	
	foreach ($final as $i => $value) {	
		if( count($final[$i]) == 1) {
			$db->executeQuery("INSERT INTO repetidos_prj (id_original) VALUES (".$final[$i][0]->id.")");
		} else {		
			$j = 0;
			for ($j = $j + 1; $j < count($final[$i]); $j++ ) {	
				$sql = "INSERT INTO repetidos_prj (id_original, id_repetido) VALUES (".$final[$i][0]->id.",".$final[$i][$j]->id.");";
				$db->executeQuery($sql);
			}
		}
	}
}

/**
Autor: Paulo Rodrigues (prodrigues@med.up.pt; paulomiguelarodrigues@gmail.com)
Data: 07/04/2014
Uso: Verifica se 2 Projectos são iguais.
*/

function equalsProjectos($p1, $p2)
{
    if( $p1->datainicio == $p2->datainicio &&
        $p1->datafim == $p2->datafim &&
        $p1->titulo == $p2->titulo &&
        $p1->codigo == $p2->codigo &&
        $p1->invresponsavel == $p2->invresponsavel &&
        $p1->entidade == $p2->entidade &&
        $p1->montante == $p2->montante &&
		$p1->link == $p2->link)
    {
        return true;
    }
    else
    {
        return false;
    }
}

?>