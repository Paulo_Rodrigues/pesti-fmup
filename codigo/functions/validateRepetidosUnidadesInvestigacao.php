<?php

function validateRepetidosUnidadesInvestigacao($uniInv)
{
	$uniInv2 = $uniInv;
	$final=array();
	
	while ( list($key, $val) = each($uniInv) ) {	
		$final[$key]=array();
		
		while (list($key2, $val2) = each($uniInv2)) {

			$equal = equalsUnidadesInvestigacao($val,$val2);
			if ($equal)
			{				
				$final[$key][]=$uniInv2[$key2];	  
				unset($uniInv2[$key2]);				  
				unset($uniInv[$key2]);
				continue;				
			}

			//Nome
			similar_text(strtolower($val->unidade), strtolower($val2->unidade), $levUnidade);
			
			if(($levUnidade >= 90))
			{				
				$final[$key][]=$uniInv2[$key2];	  
				unset($uniInv2[$key2]);				  
				unset($uniInv[$key2]);
				continue;			
			} 	

		}
		unset($uniInv2[$key]);
		reset($uniInv2);
	}
	
	$db = new Database();
	$db->executeQuery("TRUNCATE TABLE repetidos_uniInv");	
	
	foreach ($final as $i => $value) {	
		if( count($final[$i]) == 1) {
			$db->executeQuery("INSERT INTO repetidos_uniInv (id_original) VALUES (".$final[$i][0]->id.")");
		} else {		
			$j = 0;
			for ($j = $j + 1; $j < count($final[$i]); $j++ ) {	
				$sql = "INSERT INTO repetidos_uniInv (id_original, id_repetido) VALUES (".$final[$i][0]->id.",".$final[$i][$j]->id.");";
				$db->executeQuery($sql);
			}
		}
	}
}


function equalsUnidadesInvestigacao($p1, $p2)
{
    if( $p1->unidade == $p2->unidade &&
        $p1->ava2007 == $p2->ava2007 &&
        $p1->percentagem == $p2->percentagem &&
        $p1->responsavel == $p2->responsavel &&
        $p1->avaconf == $p2->avaconf)
    {
        return true;
    }
    else
    {
        return false;
    }
}

?>