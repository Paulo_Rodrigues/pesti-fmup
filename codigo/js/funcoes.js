var formSubmited=0;

function showDiv(elem)
{
	elem.style.display = 'block';
}

function hideDiv(elem)
{
	elem.style.display = 'none';
}	

function setUnidadeValue(i,id,descricao,avaliacao){

	document.getElementById("id_unidade"+i).value=id;
	document.getElementById("unidade"+i).value=descricao;
	document.getElementById("ava2007"+i).value=avaliacao;
	document.getElementById("livesearch"+i).innerHTML="";
	document.getElementById("livesearch"+i).style.border="0px";
	
	
}

function setOrgaoFMUPValue(i,id,descricao){

	//document.getElementById("fmuporgao_"+i).value=id;
	document.getElementById("fmuporgao_"+i).value=descricao;
	document.getElementById("livesearchorgaofmup"+i).innerHTML="";
	document.getElementById("livesearchorgaofmup"+i).style.border="0px";
	
}

function setCargoFMUPValue(i,id,descricao){

	//document.getElementById("fmuporgao_"+i).value=id;
	document.getElementById("fmupcargo_"+i).value=descricao;
	document.getElementById("livesearchcargofmup"+i).innerHTML="";
	document.getElementById("livesearchcargofmup"+i).style.border="0px";
	
	
}

function setOrgaoUPValue(i,id,descricao){

	//document.getElementById("fmuporgao_"+i).value=id;
	document.getElementById("uporgao_"+i).value=descricao;
	document.getElementById("livesearchorgaoup"+i).innerHTML="";
	document.getElementById("livesearchorgaoup"+i).style.border="0px";
	
	
}

function setOrgaoHospitalValue(i,id,descricao){

	document.getElementById("hospitalorgao_"+i).value=descricao;
	document.getElementById("livesearchorgaohospital"+i).innerHTML="";
	document.getElementById("livesearchorgaohospital"+i).style.border="0px";
	
	
}

function setInstituicaoHospitalValue(i,id,descricao){

	document.getElementById("hospitalinstituicao_"+i).value=descricao;
	document.getElementById("livesearchinstituicaohospital"+i).innerHTML="";
	document.getElementById("livesearchinstituicaohospital"+i).style.border="0px";
	
	
}

//ALTERACAO
//Paulo Rodrigues (prodrigues@med.up.pt)
function setInstituicaoConsultoriaValue(i,id,descricao){	
	document.getElementById("consinstituicao_"+i).value=descricao;	
	document.getElementById("livesearchinstituicaoconsultoria"+i).innerHTML="";
	document.getElementById("livesearchinstituicaoconsultoria"+i).style.border="0px";	
}

function setCargoUPValue(i,id,descricao){

	//document.getElementById("fmuporgao_"+i).value=id;
	document.getElementById("upcargo_"+i).value=descricao;
	document.getElementById("livesearchcargoup"+i).innerHTML="";
	document.getElementById("livesearchcargoup"+i).style.border="0px";
}

function setCursoValue(i,id,descricao,grau){

	//document.getElementById("fmuporgao_"+i).value=id;
	document.getElementById("dircursonome_"+i).value=descricao;
	document.getElementById("dircursosgrau_"+i).value=grau;
	document.getElementById("livesearchdircursonome_"+i).innerHTML="";
	document.getElementById("livesearchdircursonome_"+i).style.border="0px";

	
}

function setHabCursoValue(i,id,descricao,grau){

	//document.getElementById("fmuporgao_"+i).value=id;
	document.getElementById("habcurso_"+i).value=descricao;
	document.getElementById("habgrau_"+i).value=grau;
	document.getElementById("habinstituicao_"+i).value="FMUP";
	document.getElementById("livesearchhabcurso_"+i).innerHTML="";
	document.getElementById("livesearchhabcurso_"+i).style.border="0px";

	
}

function setSCValue(i,id,descricao,tipo){

	//document.getElementById("fmuporgao_"+i).value=id;
	document.getElementById("scnome_"+i).value=descricao;
	document.getElementById("sctipo_"+i).value=tipo;
	document.getElementById("livesearchsc_"+i).innerHTML="";
	document.getElementById("livesearchsc_"+i).style.border="0px";

	
}

function setSCCargoValue(i,id,descricao,tipo){

	//document.getElementById("fmuporgao_"+i).value=id;
	document.getElementById("sccargo_"+i).value=descricao;
	document.getElementById("livesearchsccargo_"+i).innerHTML="";
	document.getElementById("livesearchsccargo_"+i).style.border="0px";

	
}

		
function setTituloPublicacaoValueArbitragem(id,issn,titulo){


	document.getElementById("tituloRevistaAR"+id).value=titulo;
	document.getElementById("issnAR"+id).value=issn;
	document.getElementById("livesearchdar"+id).innerHTML="";
	document.getElementById("livesearchdar"+id).style.border="0px";

}

function setTituloPublicacaoValue(id,issn,titulo){


	document.getElementById("pmnomepublicacao_"+id).value=titulo;
	document.getElementById("pmissn_"+id).value=issn;
	document.getElementById("publistissn_"+id).innerHTML="";
	document.getElementById("publistissn_"+id).style.border="0px";
	document.getElementById("publisttitulo_"+id).innerHTML="";
	document.getElementById("publisttitulo_"+id).style.border="0px";

	
}


function setTituloPublicacaoValueArbitragem(id,issn,titulo){


	document.getElementById("tituloRevistaAR"+id).value=titulo;
	document.getElementById("issnAR"+id).value=issn;
	document.getElementById("livesearchdarn"+id).innerHTML="";
	document.getElementById("livesearchdarn"+id).style.border="0px";
	document.getElementById("livesearchdar"+id).innerHTML="";
	document.getElementById("livesearchdar"+id).style.border="0px";

	
}



		


function setHabInstituicaoValue(i,id,descricao){

	//document.getElementById("fmuporgao_"+i).value=id;
	document.getElementById("habinstituicao_"+i).value=descricao;
	document.getElementById("livesearchhabinstituicao_"+i).innerHTML="";
	document.getElementById("livesearchhabinstituicao_"+i).style.border="0px";

	
}

function setHabOInstituicaoValue(i,id,descricao){

	//document.getElementById("fmuporgao_"+i).value=id;
	document.getElementById("haboinstituicao_"+i).value=descricao;
	document.getElementById("livesearchhaboinstituicao_"+i).innerHTML="";
	document.getElementById("livesearchhaboinstituicao_"+i).style.border="0px";

	
}

function setHabOrientadorValue(i,id,descricao){

	//document.getElementById("fmuporgao_"+i).value=id;
	document.getElementById("haborientador_"+i).value=descricao;
	document.getElementById("haboinstituicao_"+i).value="Universidade do Porto - Faculdade de Medicina";
	document.getElementById("livesearchhaborientador_"+i).innerHTML="";
	document.getElementById("livesearchhaborientador_"+i).style.border="0px";

	
}

function setOriNomeValue(i,nome){

	document.getElementById("orientacao_nome_orientando_"+i).value=nome;
	document.getElementById("livesearchorinome_"+i).innerHTML="";
	document.getElementById("livesearchorinome_"+i).style.border="0px";

	
}

function setHabCOrientadorValue(i,id,descricao){

	//document.getElementById("fmuporgao_"+i).value=id;
	document.getElementById("habcorientador_"+i).value=descricao;
	document.getElementById("habcoinstituicao_"+i).value="Universidade do Porto - Faculdade de Medicina";
	document.getElementById("livesearchhabcorientador_"+i).innerHTML="";
	document.getElementById("livesearchhabcorientador_"+i).style.border="0px";

	
}



function setHabCOInstituicaoValue(i,id,descricao){

	//document.getElementById("fmuporgao_"+i).value=id;
	document.getElementById("habcoinstituicao_"+i).value=descricao;
	document.getElementById("livesearchhabcoinstituicao_"+i).innerHTML="";
	document.getElementById("livesearchhabcoinstituicao_"+i).style.border="0px";

	
}

function setColexInstituicaoValue(pais,i,id,descricao){

	//document.getElementById("fmuporgao_"+i).value=id;
	document.getElementById("cepais"+i).value=pais;
	document.getElementById("ceinstituicao"+i).value=descricao;
	document.getElementById("livesearchcolexinstituicao_"+i).innerHTML="";
	document.getElementById("livesearchcolexinstituicao_"+i).style.border="0px";

	
}

function setColNomeValue(nome,nomecient,i,departamento){

	//document.getElementById("fmuporgao_"+i).value=id;
	
	document.getElementById("cinome"+i).value=nome;
	document.getElementById("cinomecient"+i).value=nomecient;
	document.getElementById("cidepartamento"+i).value=departamento;
	document.getElementById("livesearchcolnome_"+i).innerHTML="";
	document.getElementById("livesearchcolnome_"+i).style.border="0px";

	
}

function setMissoesInstituicaoValue(pais,i,id,descricao){

	//document.getElementById("fmuporgao_"+i).value=id;
	document.getElementById("missoespais_"+i).value=pais;
	document.getElementById("missoesinstituicao_"+i).value=descricao;
	document.getElementById("livesearchmissoesinstituicao_"+i).innerHTML="";
	document.getElementById("livesearchmissoesinstituicao_"+i).style.border="0px";

	
}




function setDepEPValue(i,id,descricao,avaliacao){


	document.getElementById("departamento"+i).value=descricao;
	document.getElementById("livesearchdep"+i).innerHTML="";
	document.getElementById("livesearchdep"+i).style.border="0px";
	
}

function setDepPValue(i,id,descricao,avaliacao){

	document.getElementById("pacolhimento_"+i).value="FMUP - "+descricao;
	document.getElementById("livesearchdepp"+i).innerHTML="";
	document.getElementById("livesearchdepp"+i).style.border="0px";
	
}



function mostraDEP(str,i)
{
if (str.length==0 || (document.getElementById("empregador"+i).value).toLowerCase() != "fmup")
  { 
	document.getElementById("livesearchdep"+i).innerHTML="";
	document.getElementById("livesearchdep"+i).style.border="0px";
  return;
  }
//if (!(str=="FMUP") && !(str=="fmup"))
//{ 
//	document.getElementById("departamento"+i).value="";
//	return;
//}
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("livesearchdep"+i).innerHTML=xmlhttp.responseText;
    document.getElementById("livesearchdep"+i).style.border="1px solid #A5ACB2";
    }
  };
xmlhttp.open("GET","../functions/searchDepartamentoFMUP.php?i="+i+"&q="+str,true);

xmlhttp.send();
}

function mostraDEPP(str,i)
{
if (str.length==0)
  { 
  document.getElementById("livesearchdepp"+i).innerHTML="";
  document.getElementById("livesearchdepp"+i).style.border="0px";
  return;
  }
if (!(str=="FMUP") && !(str=="fmup"))
{ 
	//document.getElementById("pacolhimento_"+i).value="";
	return;
}
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("livesearchdepp"+i).innerHTML=xmlhttp.responseText;
    document.getElementById("livesearchdepp"+i).style.border="1px solid #A5ACB2";
    }
  };
xmlhttp.open("GET","../functions/searchDepartamentoFMUPP.php?i="+i+"&q="+str,true);

xmlhttp.send();
}

function mostraInstituicao(str,i)
{

if (str.length==0)
  { 
  document.getElementById("div"+i).innerHTML="";
  document.getElementById("div"+i).style.border="0px";
  return;
  }

if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("div"+i).innerHTML=xmlhttp.responseText;
    document.getElementById("div"+i).style.border="1px solid #A5ACB2";
    }
  };
xmlhttp.open("GET","../functions/searchInstituicao.php?i="+i+"&q="+str,true);

xmlhttp.send();
}




function mostraOrgaoFMUP(str,i)
{
if (str.length==0 )
  { 
	document.getElementById("livesearchorgaofmup"+i).innerHTML="";
	document.getElementById("livesearchorgaofmup"+i).style.border="0px";
  return;
  }
//if (!(str=="FMUP") && !(str=="fmup"))
//{ 
//	document.getElementById("departamento"+i).value="";
//	return;
//}
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("livesearchorgaofmup"+i).innerHTML=xmlhttp.responseText;
    document.getElementById("livesearchorgaofmup"+i).style.border="1px solid #A5ACB2";
    }
  };
xmlhttp.open("GET","../functions/searchOrgaoFMUP.php?i="+i+"&q="+str,true);

xmlhttp.send();
}

function mostraCargoFMUP(str,i)
{
if (str.length==0 )
  { 
	document.getElementById("livesearchcargofmup"+i).innerHTML="";
	document.getElementById("livesearchcargofmup"+i).style.border="0px";
  return;
  }
//if (!(str=="FMUP") && !(str=="fmup"))
//{ 
//	document.getElementById("departamento"+i).value="";
//	return;
//}
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("livesearchcargofmup"+i).innerHTML=xmlhttp.responseText;
    document.getElementById("livesearchcargofmup"+i).style.border="1px solid #A5ACB2";
    }
  };
xmlhttp.open("GET","../functions/searchCargoFMUP.php?i="+i+"&q="+str,true);

xmlhttp.send();
}

function mostraOrgaoUP(str,i)
{
if (str.length==0 )
  { 
	document.getElementById("livesearchorgaoup"+i).innerHTML="";
	document.getElementById("livesearchorgaoup"+i).style.border="0px";
  return;
  }
//if (!(str=="FMUP") && !(str=="fmup"))
//{ 
//	document.getElementById("departamento"+i).value="";
//	return;
//}
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("livesearchorgaoup"+i).innerHTML=xmlhttp.responseText;
    document.getElementById("livesearchorgaoup"+i).style.border="1px solid #A5ACB2";
    }
  };
xmlhttp.open("GET","../functions/searchOrgaoUP.php?i="+i+"&q="+str,true);

xmlhttp.send();
}

function mostraCargoUP(str,i)
{
if (str.length==0 )
  { 
	document.getElementById("livesearchcargoup"+i).innerHTML="";
	document.getElementById("livesearchcargoup"+i).style.border="0px";
  return;
  }
//if (!(str=="FMUP") && !(str=="fmup"))
//{ 
//	document.getElementById("departamento"+i).value="";
//	return;
//}
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("livesearchcargoup"+i).innerHTML=xmlhttp.responseText;
    document.getElementById("livesearchcargoup"+i).style.border="1px solid #A5ACB2";
    }
  };
xmlhttp.open("GET","../functions/searchCargoUP.php?i="+i+"&q="+str,true);

xmlhttp.send();
}


function mostraOrgaoHospital(str,i)
{
if (str.length==0 )
  { 
	document.getElementById("livesearchorgaohospital"+i).innerHTML="";
	document.getElementById("livesearchorgaohospital"+i).style.border="0px";
  return;
  }
//if (!(str=="FMUP") && !(str=="fmup"))
//{ 
//	document.getElementById("departamento"+i).value="";
//	return;
//}
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("livesearchorgaohospital"+i).innerHTML=xmlhttp.responseText;
    document.getElementById("livesearchorgaohospital"+i).style.border="1px solid #A5ACB2";
    }
  };
xmlhttp.open("GET","../functions/searchOrgaoHospital.php?i="+i+"&q="+str,true);

xmlhttp.send();
}


function mostraInstituicaoHospital(str,i)
{
if (str.length==0 )
  { 
	document.getElementById("livesearchinstituicaohospital"+i).innerHTML="";
	document.getElementById("livesearchinstituicaohospital"+i).style.border="0px";
  return;
  }

if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("livesearchinstituicaohospital"+i).innerHTML=xmlhttp.responseText;
    document.getElementById("livesearchinstituicaohospital"+i).style.border="1px solid #A5ACB2";
    }
  };
xmlhttp.open("GET","../functions/searchInstituicaoHospital.php?i="+i+"&q="+str,true);

xmlhttp.send();
}

//ALTERACAO
//Paulo Rodrigues (prodrigues@med.up.pt)
function mostraInstituicaoConsultoria(str,i)
{   
document.getElementById("livesearchinstituicaoconsultoria"+i).style.display = 'block';
   if (str.length==0 )
  { 
	document.getElementById("livesearchinstituicaoconsultoria"+i).innerHTML="";
	document.getElementById("livesearchinstituicaoconsultoria"+i).style.border="0px";
  return;
  }

if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("livesearchinstituicaoconsultoria"+i).innerHTML=xmlhttp.responseText;
    document.getElementById("livesearchinstituicaoconsultoria"+i).style.border="1px solid #A5ACB2";
    }
  };
xmlhttp.open("GET","../functions/searchInstituicaoConsultoria.php?i="+i+"&q="+str,true);

xmlhttp.send();

}

function mostraCurso(str,i)
{
if (str.length==0 )
  { 
	document.getElementById("livesearchdircursonome_"+i).innerHTML="";
	document.getElementById("livesearchcdircursonome_"+i).style.border="0px";
  return;
  }
//if (!(str=="FMUP") && !(str=="fmup"))
//{ 
//	document.getElementById("departamento"+i).value="";
//	return;
//}
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("livesearchdircursonome_"+i).innerHTML=xmlhttp.responseText;
    document.getElementById("livesearchdircursonome_"+i).style.border="1px solid #A5ACB2";
    }
  };
xmlhttp.open("GET","../functions/searchCurso.php?i="+i+"&q="+str,true);

xmlhttp.send();
}




function mostraHabCurso(str,i)
{
if (str.length==0 )
  { 
	document.getElementById("livesearchhabcurso_"+i).innerHTML="";
	document.getElementById("livesearchhabcurso_"+i).style.border="0px";
  return;
  }
//if (!(str=="FMUP") && !(str=="fmup"))
//{ 
//	document.getElementById("departamento"+i).value="";
//	return;
//}
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("livesearchhabcurso_"+i).innerHTML=xmlhttp.responseText;
    document.getElementById("livesearchhabcurso_"+i).style.border="1px solid #A5ACB2";
    }
  };
xmlhttp.open("GET","../functions/searchHabCurso.php?i="+i+"&q="+str,true);

xmlhttp.send();
}

function mostraSC(str,i)
{
if (str.length==0 )
  { 
	document.getElementById("livesearchsc_"+i).innerHTML="";
	document.getElementById("livesearchsc_"+i).style.border="0px";
  return;
  }
//if (!(str=="FMUP") && !(str=="fmup"))
//{ 
//	document.getElementById("departamento"+i).value="";
//	return;
//}
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("livesearchsc_"+i).innerHTML=xmlhttp.responseText;
    document.getElementById("livesearchsc_"+i).style.border="1px solid #A5ACB2";
    }
  };
xmlhttp.open("GET","../functions/searchSC.php?i="+i+"&q="+str,true);

xmlhttp.send();
}

function mostraSCCargo(str,i)
{
if (str.length==0 )
  { 
	document.getElementById("livesearchsccargo_"+i).innerHTML="";
	document.getElementById("livesearchsccargo_"+i).style.border="0px";
  return;
  }

a = document.getElementById("sctipo_"+i).value;

if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("livesearchsccargo_"+i).innerHTML=xmlhttp.responseText;
    document.getElementById("livesearchsccargo_"+i).style.border="1px solid #A5ACB2";
    }
  };
xmlhttp.open("GET","../functions/searchSCCargo.php?a="+a+"&i="+i+"&q="+str,true);

xmlhttp.send();
}


function mostraHabInstituicao(str,i)
{
if (str.length==0 )
  { 
	document.getElementById("livesearchhabinstituicao_"+i).innerHTML="";
	document.getElementById("livesearchhabinstituicao_"+i).style.border="0px";
  return;
  }
//if (!(str=="FMUP") && !(str=="fmup"))
//{ 
//	document.getElementById("departamento"+i).value="";
//	return;
//}
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("livesearchhabinstituicao_"+i).innerHTML=xmlhttp.responseText;
    document.getElementById("livesearchhabinstituicao_"+i).style.border="1px solid #A5ACB2";
    }
  };
xmlhttp.open("GET","../functions/searchHabInstituicao.php?i="+i+"&q="+str,true);

xmlhttp.send();
}

function mostraHabOInstituicao(str,i)
{
if (str.length==0 )
  { 
	document.getElementById("livesearchhaboinstituicao_"+i).innerHTML="";
	document.getElementById("livesearchhaboinstituicao_"+i).style.border="0px";
  return;
  }
//if (!(str=="FMUP") && !(str=="fmup"))
//{ 
//	document.getElementById("departamento"+i).value="";
//	return;
//}
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("livesearchhaboinstituicao_"+i).innerHTML=xmlhttp.responseText;
    document.getElementById("livesearchhaboinstituicao_"+i).style.border="1px solid #A5ACB2";
    }
  };
xmlhttp.open("GET","../functions/searchHabOInstituicao.php?i="+i+"&q="+str,true);

xmlhttp.send();
}

function mostraHabCOInstituicao(str,i)
{
if (str.length==0 )
  { 
	document.getElementById("livesearchhabcoinstituicao_"+i).innerHTML="";
	document.getElementById("livesearchhabcoinstituicao_"+i).style.border="0px";
  return;
  }
//if (!(str=="FMUP") && !(str=="fmup"))
//{ 
//	document.getElementById("departamento"+i).value="";
//	return;
//}
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("livesearchhabcoinstituicao_"+i).innerHTML=xmlhttp.responseText;
    document.getElementById("livesearchhabcoinstituicao_"+i).style.border="1px solid #A5ACB2";
    }
  };
xmlhttp.open("GET","../functions/searchHabCOInstituicao.php?i="+i+"&q="+str,true);

xmlhttp.send();
}


function mostraHabOrientador(str,i)
{
if (str.length==0 )
  { 
	document.getElementById("livesearchhaborientador_"+i).innerHTML="";
	document.getElementById("livesearchhaborientador_"+i).style.border="0px";
  return;
  }
//if (!(str=="FMUP") && !(str=="fmup"))
//{ 
//	document.getElementById("departamento"+i).value="";
//	return;
//}
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("livesearchhaborientador_"+i).innerHTML=xmlhttp.responseText;
    document.getElementById("livesearchhaborientador_"+i).style.border="1px solid #A5ACB2";
    }
  };
xmlhttp.open("GET","../functions/searchHabOrientador.php?i="+i+"&q="+str,true);

xmlhttp.send();
}

function mostraHabCOrientador(str,i)
{
if (str.length==0 )
  { 
	document.getElementById("livesearchhabcorientador_"+i).innerHTML="";
	document.getElementById("livesearchhabcorientador_"+i).style.border="0px";
  return;
  }
//if (!(str=="FMUP") && !(str=="fmup"))
//{ 
//	document.getElementById("departamento"+i).value="";
//	return;
//}
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("livesearchhabcorientador_"+i).innerHTML=xmlhttp.responseText;
    document.getElementById("livesearchhabcorientador_"+i).style.border="1px solid #A5ACB2";
    }
  };
xmlhttp.open("GET","../functions/searchHabCOrientador.php?i="+i+"&q="+str,true);

xmlhttp.send();
}


function mostraColexInstituicao(str,i)
{
if (str.length==0 )
  { 
	document.getElementById("livesearchcolexinstituicao_"+i).innerHTML="";
	document.getElementById("livesearchcolexinstituicao_"+i).style.border="0px";
  return;
  }
//if (!(str=="FMUP") && !(str=="fmup"))
//{ 
//	document.getElementById("departamento"+i).value="";
//	return;
//}
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("livesearchcolexinstituicao_"+i).innerHTML=xmlhttp.responseText;
    document.getElementById("livesearchcolexinstituicao_"+i).style.border="1px solid #A5ACB2";
    }
  };
xmlhttp.open("GET","../functions/searchColexInstituicao.php?i="+i+"&q="+str,true);

xmlhttp.send();
}

function mostraOriNome(str,i)
{
if (str.length==0 )
  { 
	document.getElementById("livesearchorinome_"+i).innerHTML="";
	document.getElementById("livesearchorinome_"+i).style.border="0px";
  return;
  }
//if (!(str=="FMUP") && !(str=="fmup"))
//{ 
//	document.getElementById("departamento"+i).value="";
//	return;
//}
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("livesearchorinome_"+i).innerHTML=xmlhttp.responseText;
    document.getElementById("livesearchorinome_"+i).style.border="1px solid #A5ACB2";
    }
  };
xmlhttp.open("GET","../functions/searchOriNome.php?i="+i+"&q="+str,true);

xmlhttp.send();
}


function mostraColNome(str,i)
{
if (str.length==0 )
  { 
	document.getElementById("livesearchcolnome_"+i).innerHTML="";
	document.getElementById("livesearchcolnome_"+i).style.border="0px";
  return;
  }
//if (!(str=="FMUP") && !(str=="fmup"))
//{ 
//	document.getElementById("departamento"+i).value="";
//	return;
//}
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("livesearchcolnome_"+i).innerHTML=xmlhttp.responseText;
    document.getElementById("livesearchcolnome_"+i).style.border="1px solid #A5ACB2";
    }
  };
xmlhttp.open("GET","../functions/searchColNome.php?i="+i+"&q="+str,true);

xmlhttp.send();
}


function mostraMissoesInstituicao(str,i)
{
if (str.length==0 )
  { 
	document.getElementById("livesearchmissoesinstituicao_"+i).innerHTML="";
	document.getElementById("livesearchmissoesinstituicao_"+i).style.border="0px";
  return;
  }

if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("livesearchmissoesinstituicao_"+i).innerHTML=xmlhttp.responseText;
    document.getElementById("livesearchmissoesinstituicao_"+i).style.border="1px solid #A5ACB2";
    }
  };
xmlhttp.open("GET","../functions/searchMissoesInstituicao.php?i="+i+"&q="+str,true);

xmlhttp.send();
}









function mostraResultadoUnidade(str,i)
{
if (str.length==0)
  { 
  document.getElementById("livesearch"+i).innerHTML="";
  document.getElementById("livesearch"+i).style.border="0px";
  return;
  }
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("livesearch"+i).innerHTML=xmlhttp.responseText;
    document.getElementById("livesearch"+i).style.border="1px solid #A5ACB2";
    }
  };
xmlhttp.open("GET","../functions/searchUnidade.php?i="+i+"&q="+str,true);

xmlhttp.send();
}

function mostraNomeRevista(str,i)
{
if (str=="" || str.length<4)
  {
  document.getElementById("publistissn_"+i).innerHTML="";
  return;
  } 
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }

xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("publistissn_"+i).innerHTML=xmlhttp.responseText;
    document.getElementById("publistissn_"+i).style.border="1px solid #A5ACB2";
    }
  };
xmlhttp.open("GET","../functions/getPubNameByISSN.php?i="+i+"&q="+str,true);
xmlhttp.send();
}

function mostraNomeRevistaArbitro(str,i)
{
if (str=="" || str.length<4)
  {
  document.getElementById("publistissn_"+i).innerHTML="";
  return;
  } 
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }

xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("publistissn_"+i).innerHTML=xmlhttp.responseText;
    document.getElementById("publistissn_"+i).style.border="1px solid #A5ACB2";
    }
  };
xmlhttp.open("GET","../functions/getPubNameByISSN.php?i="+i+"&q="+str,true);
xmlhttp.send();
}


function mostraNomeRevistaTitulo(str,i)
{
if (str=="" || str.length<4)
  {
  document.getElementById("publisttitulo_"+i).innerHTML="";
  return;
  } 
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }

xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("publisttitulo_"+i).innerHTML=xmlhttp.responseText;
    document.getElementById("publisttitulo_"+i).style.border="1px solid #A5ACB2";
    }
  };
xmlhttp.open("GET","../functions/getPubNameByTitulo.php?i="+i+"&q="+str,true);
xmlhttp.send();
}

function mostraNomeRevistaTituloArbitragens(str,i)
{
if (str=="" || str.length<2)
  {
  document.getElementById("livesearchdarn"+i).innerHTML="";
  return;
  } 
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }

xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("livesearchdarn"+i).innerHTML=xmlhttp.responseText;
    document.getElementById("livesearchdarn"+i).style.border="1px solid #A5ACB2";
    }
  };
xmlhttp.open("GET","../functions/getPubNameByTitulo_Arbitragem.php?i="+i+"&q="+str,true);
xmlhttp.send();
}

function mostraNomeRevistaArbitragens(str,i)
{
	
if (str=="" || str.length<3)
  {
  document.getElementById("livesearchdar"+i).innerHTML="";
  return;
  } 
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }

xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("livesearchdar"+i).innerHTML=xmlhttp.responseText;
    document.getElementById("livesearchdar"+i).style.border="1px solid #A5ACB2";
    }
  };
xmlhttp.open("GET","../functions/getPubNameByISSN_Arbitragem.php?i="+i+"&q="+str,true);
xmlhttp.send();
}


function mostraAvaliacaoUnidade(str,i)
{
if (str=="")
  {
  document.getElementById("ava2007"+i).innerHTML="";
  return;
  } 
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }

xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("ava2007"+i).value=xmlhttp.responseText;
    }
  };
xmlhttp.open("GET","../functions/getAvaliacaoByUINome.php?q="+str,true);
xmlhttp.send();
}


function updateQUESTSTATUS(inv)
{
if (inv.length==0)
  { 

  return;
  }
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
	  document.getElementById("questpt").innerHTML="";
	  document.getElementById("questen").innerHTML="";
    }
  };
xmlhttp.open("GET","../functions/updateQUESTSTATUS.php?i="+inv,true);

xmlhttp.send();
}



function calendario(name){
		new JsDatePick({
			useMode:2,
			target:""+name,
			dateFormat:"%d-%m-%Y",
			cellColorScheme:"beige",
			yearsRange:[1910,2080],
			imgPath:"../styles/img/"
			/*selectedDate:{				This is an example of what the full configuration offers.
				day:5,						For full documentation about these settings please see the full version of the code.
				month:9,
				year:2006
			},
			yearsRange:[1978,2020],
			limitToToday:false,
			cellColorScheme:"beige",
			dateFormat:"%m-%d-%Y",
			imgPath:"../styles/img/",
			weekStartDay:1*/
		});
	
	};
	
function showhideUnanimidade(){

	if(document.getElementById('agregacao').checked==true){
		document.getElementById('linhanunanimidade').style.visibility='visible';
	}else{
		document.getElementById('unanimidade').checked=false;
		document.getElementById('linhanunanimidade').style.visibility='hidden';
	}
	
}

function enabledisableDatafim(i){

	if(document.getElementById('semtermo'+i).checked==true){
		document.getElementById('df_'+i).style.visibility='hidden';
	}else{
		document.getElementById('df_'+i).style.visibility='visible';
	}
	
}

function changeCF(){
	if(document.getElementById('paisnat').value!=177){
		document.getElementById('concelhonat').options[1].selected=true;
		document.getElementById('freguesiasnat').options[1].selected=true;
	}
}

         
function makeactive(tab) {
    var i = 0;
	//alert (tab);
    
    if (document.getElementById("content_menu") != null) {

		//Muda cor tab seleccionada para amarelo
        for (i = 0; i <= 33; i++)
        {		
            if (i != tab) {
                document.getElementById("tab" + i).className = "";
            } else {
                document.getElementById("tab" + i).className = "active";
            }
        }
		
		
		//mostrar submenus das tabs
        if (tab == 0) {
			//tab Preenchimento
            document.getElementById("subtab1").style.display = 'none';
            document.getElementById("subtab5").style.display = 'none';
            document.getElementById("subtab9").style.display = 'none';
            document.getElementById("subtab14").style.display = 'none';
            document.getElementById("subtab25").style.display = 'none';
        }
        else if (tab >= 1 && tab <= 4) {
			//tab Dados Gerais
            document.getElementById("subtab1").style.display = 'inline';
            document.getElementById("subtab5").style.display = 'none';
            document.getElementById("subtab9").style.display = 'none';
            document.getElementById("subtab14").style.display = 'none';
            document.getElementById("subtab25").style.display = 'none';
        } else if (tab >= 5 && tab <= 8 || tab == 32 || tab == 33) {
			//Tab Cargos
            document.getElementById("subtab1").style.display = 'none';
            document.getElementById("subtab5").style.display = 'inline';
            document.getElementById("subtab9").style.display = 'none';
            document.getElementById("subtab14").style.display = 'none';
            document.getElementById("subtab25").style.display = 'none';
        } else if (tab >= 9 && tab <= 13) {
			//Tab Publicações
            document.getElementById("subtab1").style.display = 'none';
            document.getElementById("subtab5").style.display = 'none';
            document.getElementById("subtab9").style.display = 'inline';
            document.getElementById("subtab14").style.display = 'none';
            document.getElementById("subtab25").style.display = 'none';
        } else if (tab >= 14 && tab <= 24) {
			//Tab Outra Actividade Científica
            document.getElementById("subtab1").style.display = 'none';
            document.getElementById("subtab5").style.display = 'none';
            document.getElementById("subtab9").style.display = 'none';
            document.getElementById("subtab14").style.display = 'inline';
            document.getElementById("subtab25").style.display = 'none';
        } else if (tab >= 25 && tab <= 30) {
			//Tab Actividade Académica
            document.getElementById("subtab1").style.display = 'none';
            document.getElementById("subtab5").style.display = 'none';
            document.getElementById("subtab9").style.display = 'none';
            document.getElementById("subtab14").style.display = 'none';
            document.getElementById("subtab25").style.display = 'inline';
        } else {
			//Outra tab
            document.getElementById("subtab1").style.display = 'none';
            document.getElementById("subtab5").style.display = 'none';
            document.getElementById("subtab9").style.display = 'none';
            document.getElementById("subtab14").style.display = 'none';
            document.getElementById("subtab25").style.display = 'none';
        }

    }

    formSubmited = 1;
    document.getElementById("statusMSG").innerHTML = "";

    //alert('The page has loaded completely!');

}

function mask(f){ 
	tel='('; 
	var val =f.value.split(''); 
		for(var i=0;i<val.length;i++){ 
			if(i==2){
				val[i]=val[i]+') ';
				} 
			
			tel=tel+val[i];
		} 
	f.value=tel; 
}

function validateCal(evt) {
	  var theEvent = evt || window.event;
	  var key = theEvent.keyCode || theEvent.which;
	  key = String.fromCharCode( key );
	  var regex = /[0-9]|\./;
	  if( !(key==null) ) {
	    theEvent.returnValue = false;
	    if(theEvent.preventDefault) theEvent.preventDefault();
	  }
	}


function validate(evt) {
	  var theEvent = evt || window.event;
	  var key = theEvent.keyCode || theEvent.which;
	  var key2 = theEvent.keyCode;
	  var key3 = theEvent.which;
	  key = String.fromCharCode( key );
	  //alert(key);
	  var regex = /[0-9]|\,/;
	  if( !(key3 == 8  ||  key3 == 27  || key3 == 46  || key3 == 37 || key3 == 39 || key2 == 8  ||  key2 == 27  || key2 == 46  || key2 == 37 || key2 == 39) ){
		  if( !regex.test(key) ) {
		    theEvent.returnValue = false;
		    if(theEvent.preventDefault) theEvent.preventDefault();
		  }
	  }
	}

function validatephone(evt) {
	  var theEvent = evt || window.event;
	  var key = theEvent.keyCode || theEvent.which;
	  key = String.fromCharCode( key );
	  var regex = /[0-9]|\+/;
	  if( !(key3 == 8  ||  key3 == 27  || key3 == 46  || key3 == 37 || key3 == 39 || key2 == 8  ||  key2 == 27  || key2 == 46  || key2 == 37 || key2 == 39) ){
		  if( !regex.test(key) ) {
			  theEvent.returnValue = false;
			  if(theEvent.preventDefault) theEvent.preventDefault();
		  }
	  }
	}




/*function validateValue() {

		if(document.questionario.perestudante.value == "0" || document.questionario.perestudante.value == ""){
			document.getElementById('estudantes').style.display='none';
			document.questionario.perestudante.value="0";
		}else{
			document.getElementById('estudantes').style.display='inline';
			}

		if(document.questionario.perdocente.value == "0" || document.questionario.perdocente.value == ""){
			document.getElementById('docentes').style.display='none';
			document.questionario.perdocente.value="0";
		}else{
			document.getElementById('docentes').style.display='inline';
			}

		if(document.questionario.perinvestigador.value == "0" || document.questionario.perinvestigador.value == ""){
			document.getElementById('investigadores').style.display='none';
			document.questionario.perinvestigador.value="0";
		}else{
			document.getElementById('investigadores').style.display='inline';
			}
	} */
	
function validateRequiredFields() {

	var erro=false;
	var message=" Existem erros no formulário:\n";
	
	
	
	var totOP=0;
	totOP=parseInt(document.questionario.docenteper.value)+
			parseInt(document.questionario.investigadorper.value)+
			parseInt(document.questionario.pcicloper.value)+
			parseInt(document.questionario.scicloper.value)+
			parseInt(document.questionario.tcicloper.value)+
			parseInt(document.questionario.ocicloper.value)+
			parseInt(document.questionario.tecper.value)+
			parseInt(document.questionario.outraper.value)
			;
	
	if(totOP>100){
		message=message+"- Caraterização - O total das percentagens é superior a 100% \n";	
		erro=true;
	}
	
	if(totOP==0){
		message=message+"- Carateriação - Deve indicar uma percentagem >0 em pelo menos uma das categorias \n";	
		erro=true;
	}

	if(!erro){
		document.questionario.operacao.value=1;
		document.questionario.submit();
	}else{
		alert(message);
	}

}


function showhideOP(cat){
	
	//var campomes=cat+'mes';
	var campocat=cat+'cat';
	var campoper=cat+'per';
	
	if(document.getElementById(campoper).value<0){
		//document.getElementById(campomes).value=0;
		//document.getElementById(campomes).disabled=true;
		if(document.getElementById(campocat)!=null){
			document.getElementById(campocat).disabled=true;
		}
	}else{
		//document.getElementById(campomes).disabled=false;
		if(document.getElementById(campocat)!=null){
			document.getElementById(campocat).disabled=false;
		}
	}
	
	
	
}

function showhideOPInvestigador(cat){
	
	//var campomes=cat+'mes';
	var campocat=cat+'cat';
	var campoper=cat+'per';
	var campobolsa=cat+'bolsa';
	
	if(document.getElementById(campoper).value<0){
		//document.getElementById(campomes).value=0;
		//document.getElementById(campomes).disabled=true;
		if(document.getElementById(campocat)!=null){
			document.getElementById(campocat).disabled=true;
			document.getElementById(campobolsa).disabled=true;
		}
	}else{
		//document.getElementById(campomes).disabled=false;
		if(document.getElementById(campocat)!=null){
			document.getElementById(campocat).disabled=false;
			document.getElementById(campobolsa).disabled=false;
		}
	}
	
	
	
}

function validateMail()
{
var x=document.forms["questionario"]["email"].value;
var atpos=x.indexOf("@");
var dotpos=x.lastIndexOf(".");
if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length)
  {
  alert("O email introduzido não é válido");
  return false;
  }
}

function validateMailAlt()
{
var x=document.forms["questionario"]["email_alternativo"].value;
var atpos=x.indexOf("@");
var dotpos=x.lastIndexOf(".");
if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length)
  {
  alert("O email introduzido não é válido");
  return false;
  }
}

function validateTelefone(){
	var regexObj = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
	
	subjectString=document.getElementById("telefone").value;

	if (regexObj.test(subjectString)) {
	    var formattedPhoneNumber =
	        subjectString.replace(regexObj, "($1) $2-$3");
	    document.getElementById("telefone").value=subjectString;
	} else {
	    // Invalid phone number
	}
	
	
}


function procuraConcelho(pais)
{
		
	if (pais!="1")
	  {
	  document.getElementById("concelhodiv").innerHTML="<select><option>Desconhecido</option></select>";
	  document.getElementById("freguesiadiv").innerHTML="<select><option>Desconhecido</option></select>"
	  return;
	  } 
	if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	  }
	else
	  {// code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }

	xmlhttp.onreadystatechange=function()
	  {
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
	    {
	    document.getElementById("concelhodiv").innerHTML=xmlhttp.responseText;
	    }
	  };
	xmlhttp.open("GET","../formulario/functions/procuraConcelho.php",true);
	xmlhttp.send();
}

function procuraFreguesia(concelho)
{
	if ( concelho=="")
	  {
	  document.getElementById("freguesiadiv").innerHTML="";
	  return;
	  } 
	if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	  }
	else
	  {// code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }

	xmlhttp.onreadystatechange=function()
	  {
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
	    {
	    document.getElementById("freguesiadiv").innerHTML=xmlhttp.responseText;
	    }
	  };
	xmlhttp.open("GET","../formulario/functions/procuraFreguesia.php?concelho="+concelho,true);
	xmlhttp.send();
}

/**
 * @param i		indice da linha
 * @param id	id do cargo (tabela cargos)
 * @param descricao		descriçao da instituicao
 * */
function setInstituicaoCValue(i,id,descricao){
	document.getElementById("instituicao_"+i).value=descricao;
	document.getElementById("livesearchinstituicao"+i).innerHTML="";

	document.getElementById("livesearchinstituicao"+i).style.border="0px";
}

function setInstituicaoValue(i,id,descricao){
	document.getElementById(i).value=descricao;
	document.getElementById("div"+i).innerHTML="";
	document.getElementById("div"+i).style.border="0px";
}


/**
 * @param i		indice da linha
 * @param id	id do cargo (tabela cargos)
 * @param descricao		descriçao do cargo
 * */
function setCargoCValue(i,id,descricao){
	document.getElementById("cargo_"+i).value=descricao;
	document.getElementById("livesearchcargo"+i).innerHTML="";

	document.getElementById("livesearchcargo"+i).style.border="0px";
}

/**
 * Mostra cargos (formulário, tab Cargos-Cargos na Comissao)
 * 
 * @param str	Informação inserida pelo utilizador na coluna Cargo
 * @param i 	Numero da linha correspondente à informação
 * */
function mostraCargosC(str,i){
	if (str.length==0 )
	{
		document.getElementById("livesearchcargo"+i).innerHTML="";
		document.getElementById("livesearchcargo"+i).style.border="0px";
		return;
	}
	//if (!(str=="FMUP") && !(str=="fmup"))
	//{ 
	//	document.getElementById("departamento"+i).value="";
	//	return;
	//}
	if (window.XMLHttpRequest)
	{// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	}
	else
	{// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	xmlhttp.onreadystatechange=function()
	{
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
	    {
			//popula div com links recebidos
			document.getElementById("livesearchcargo"+i).innerHTML=xmlhttp.responseText;
			
			document.getElementById("livesearchcargo"+i).style.border="1px solid #A5ACB2";
	    }

	  };
	  
	xmlhttp.open("GET","../functions/searchCargosC.php?i="+i+"&q="+str,true);
	
	xmlhttp.send();
}

/**
 * Mostra instituicoes (formulario tab Cargos-Cargos na FMUP)
 * 
 * @param str	Informacao inserida pelo utilizador na coluna Instituicao
 * @param i 	Numero da linha correspondente á informacao
 * */
function mostraInstituicoesC(str, i){
	if (str.length==0 )
	{
		document.getElementById("livesearchinstituicao"+i).innerHTML="";
		document.getElementById("livesearchinstituicao"+i).style.border="0px";
		return;
	}
	
	if (window.XMLHttpRequest)
	{// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	}
	else
	{// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	xmlhttp.onreadystatechange=function()
	{
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
	    {
			//popula div com links recebidos
			document.getElementById("livesearchinstituicao"+i).innerHTML=xmlhttp.responseText;
			
			document.getElementById("livesearchinstituicao"+i).style.border="1px solid #A5ACB2";
	    }

	  };
	  
	xmlhttp.open("GET","../functions/searchInstituicoes.php?i="+i+"&q="+str,true);
	
	xmlhttp.send();
}
/**
 * @param i		indice da linha
 * @param id	id do orgao (tabela cargos)
 * @param descricao		descrição do orgao
 * */
function setOrgaoValue(i,id,descricao){
	document.getElementById("orgao_"+i).value=descricao;
	document.getElementById("livesearchorgao"+i).innerHTML="";

	document.getElementById("livesearchorgao"+i).style.border="0px";
}


/**
 * Mostra orgaos (formulário, tab Cargos-Cargos na FMUP)
 * 
 * @param str	Informação inserida pelo utilizador na coluna Orgao
 * @param i 	Numero da linha correspondente à informação
 * */
function mostraOrgaosC(str,i)
{
	if (str.length==0 )
	{
		document.getElementById("livesearchorgao"+i).innerHTML="";
		document.getElementById("livesearchorgao"+i).style.border="0px";
		return;
	}
	
	if (window.XMLHttpRequest)
	{// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	}
	else
	{// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	xmlhttp.onreadystatechange=function()
	{
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
	    {
			//popula div com links recebidos
			document.getElementById("livesearchorgao"+i).innerHTML=xmlhttp.responseText;
			
			document.getElementById("livesearchorgao"+i).style.border="1px solid #A5ACB2";
	    }

	  };
	  
	xmlhttp.open("GET","../functions/searchOrgaos.php?i="+i+"&q="+str,true);
	
	xmlhttp.send();
}

/**
 * Ordena Lista de Comissoes
 * @author Catia Marques (cam@med.up.pt, catia.abmarques@gmail.com)
 * @param acao	Valor da operação a realizar
 * */
function ordenaComissao(acao){
	 document.forms['questionario'].operacao.value = acao;
	 document.forms['questionario'].submit();
}

/**
 * Edita o tipoFMUP de uma publicação e o tipo relatorio
 * @author Catia Marques (cam@med.up.pt, catia.abmarques@gmail.com)
 * @param sel - elemento select
 */
function getSelectedFMUP(sel) {
    var value = sel.value;
    var idoption = sel.id;
    var prev = $('#'+idoption).attr('value');
    var id = idoption.substring(idoption.indexOf('_')+1); //+1 para nao incluir o simbolo '_'
    
    //perguntar se quer mesmo mudar o tipo
    var conf = confirm("Mudar tipoFMUP da publicação ["+id+"] de '"+prev+"' para '"+value+"'?");
    if(conf){
        var data = {idPub:id,novoValor:value,antValor:prev,funcao:"TipoFMUP"};
        
	    getDataAjax('../../functions/corrigePublicacoes.php', data, 'html', function(info){
	    	$('#'+idoption).attr('value', value);
	    	$('#'+idoption).closest('td').css( "background", "#FFD700" );
            var this_rel = $('#tiposrel_'+id).attr('value'); 
	       	//se data nao for null, mudar tipo relatorio
	       	if(!!info){
                    $('#tiposrel_'+id).attr('value', info);
                    document.getElementById('tiposrel_'+id).value=info;
                    $('#tiposrel_'+id).closest('td').css( "background", "#FFD700" );
					
                    //actualiza menu onde a pub estava
                    var num = Number($('h3[id="'+this_rel+'"] div').html()) - 1; 
                    $('h3[id="'+this_rel+'"] div').html(num);
                    //actualiza menu para onde a pub foi mudada
                    num = Number($('h3[id="'+info+'"] div').html()) + 1; 
                    $('h3[id="'+info+'"] div').html(num);
	       	}else{
                    var submenu = $('div[id="publicacoes-'+this_rel+'"] h4[id="'+value+'"]').length;
                    if(submenu===0){//nao existe
                            //update accordion
                    }/**/
                }
            });
    }else{
    	//cancelou - muda valor da dropdown list
    	document.getElementById(idoption).value=prev;
    }
}


/**
 * Edita o tipoRelatorio de uma publicação
 * @author Catia Marques (cam@med.up.pt, catia.abmarques@gmail.com)
 * @param sel - elemento select
 */
function getSelectedRel(sel) {
    var value = sel.value; 
    var idoption = sel.id;
    var prev = $('#'+idoption).attr('value');
    id = idoption.substring(idoption.indexOf('_')+1); //+1 para nao incluir o simbolo '_'
    var num = 0;
    //perguntar se quer mesmo mudar o tipo
    var conf = confirm("Mudar tipo relatorio da publicação "+id+" de "+prev+" para "+value+"?");
    if(conf){
        var data = {idPub:id,novoValor:value,funcao:"TipoRel"};
        
	    getDataAjax('../../functions/corrigePublicacoes.php', data, 'html', function(info){
	    	$('#'+idoption).attr('value', value);
	    	$('#'+idoption).closest('td').css( "background", "#FFD700" );
	    });
            
	    //actualiza menu para onde a pub estava
	    var num = Number($('h3[id="'+prev+'"] div').text()) - 1;
	    $('h3[id="'+prev+'"] div').text(num);
	    //actualiza menu para onde a pub foi mudada
	    num = Number($('h3[id="'+value+'"] div').text()) + 1;
	    $('h3[id="'+value+'"] div').text(num);
    }else{
    	//cancelou - muda valor da dropdown list
    	document.getElementById(idoption).value=prev;
    }
}

/**
 * Preenche div-accordion principal com cada tipo das publicações e trata das chamadas dos menus
 * @author Catia Marques (cam@med.up.pt, catia.abmarques@gmail.com)
 */
function atualizaPub() {
    //loading image dentro do div #espera!! e button #atualizaPub desaparece!
    $('#accordionpub').empty();
    $('#espera').css('visibility','visible');
    var data = {funcao:"atualizaPublicacoes"};
    getDataAjax('../../functions/corrigePublicacoes.php',data,'html',function(info){//preenche com menus (h3)
        $('#espera').css('visibility','hidden');
        $('#accordionpub').append(info).accordion(
                { header: 'h3', collapsible: true, active: false, autoheight: false, 
                    alwaysOpen: false, fillspace: false, 
                    clearStyle: true , animated: true, showSpeed: 400, 
                    hideSpeed: 800, heightStyle: 'content',   //auto, fill, content
                    activate: function(event, ui){
                        var tiporel = $(this).find(ui.newHeader).attr('id');//tiporel do h3
                        var newPanel = $(this).find(ui.newPanel);
                        var prevPanel = $(this).find(ui.oldPanel);
                        prevPanel.html('');
                        preencheMenu(newPanel,tiporel);
                    }//fim de activate de menu
                }).accordion('refresh');//accordion de submenus
            });//ajax call
}

/**
 * Preenche menus com cada tipo de publicações tipofmup  
 * @author Catia Marques (cam@med.up.pt, catia.abmarques@gmail.com)
 */
function preencheMenu(menu, tiporel){
    var data = {funcao:'atualizaMenu','menu':tiporel};
    getDataAjax('../../functions/corrigePublicacoes.php',
                    data,'html',
            function(info){
                menu.append(info).accordion(
                        {header:'h4', collapsible:true, active:false,
                         autoHeight: false, clearStyle: true , animated: true, 
                         showSpeed: 400, hideSpeed: 800,autoActivate:true,
                         heightStyle: "content",
                         activate: function(event,ui){
                             var prevPanel = $(this).find(ui.oldPanel);
                             prevPanel.html('');
                             var tipof = menu.find(ui.newHeader).attr('id');//tipofmup
                             var subMenu = menu.find(ui.newPanel);
                             preencheSubMenu(subMenu, tipof, tiporel);
                         }                                            
                    }).accordion('refresh');
            });//fim de segundo getAjax
}

/**
 * Preenche accordion de um Submenu dos Menus de correção do tipo 
 * a ser utilizado no relatório de investigação
 * @param {object} ui objecto que permite acesso a funcoes de manipulacao de um accordion
 * @param {jQuery} newPanel o conteudo do header que acabou de ser ativado
 * @param {string} tiporel tipo do relatório - menu usado
 * */
function preencheSubMenu(subMenu, tipof, tiporel){//Corr
    //var tipof = newPanel.find(ui.newHeader).attr('id');//tipofmup
    //var subMenu = newPanel.find(ui.newPanel);
    var data = {funcao:'atualizaSubMenu','menu':tiporel,'submenu':tipof};
    getDataAjax('../../functions/corrigePublicacoes.php',data,'html',
        function(info){
            subMenu.append(info).accordion(
                    {header:'h4', active:false,
                         clearStyle: true , animated: true, 
                         showSpeed: 400, hideSpeed: 800,autoActivate:true,
                         collapsible:true, heightStyle: "content",
                         autoHeight: false,alwaysOpen:false
                }).accordion('refresh');
            $('select[id^=\"tiposfmup_\"]').each( function(){ $(this).val($(this).attr('value')); } );
            $('select[id^=\"tiposrel_\"]').each( function(){ $(this).val($(this).attr('value')); } );
        });
}

/**
 * Infere tipo relatório de todas as publicações 
 * */
function infereTodasPublicacoes(){
	//certificar
    var conf = confirm("Tem a certeza que pretende reanalisar o tipo relatório para todas as publicações?");
	if(conf){
		var data = {funcao:'inferir'};
		getDataAjax('../../functions/corrigePublicacoes.php',data,'html', function(info){
			atualizaPub();
		});
	}
}

/**
 * Infere tipo relatório de publicações não corrigidas
 * */
function inferePublicacoesNCorrigidas(){
	//certificar
    var conf = confirm("Tem a certeza que pretende reanalisar o tipo relatório das publicações não corrigidas?");
	if(conf){
		var data = {funcao:'inferir_naocorrigidas'};
		getDataAjax('../../functions/corrigePublicacoes.php',data,'html', function(info){
			atualizaPub();
		});
	}
}

/**
 * Função de auxilio para escolha do ano 
 * @author Catia Marques (cam@med.up.pt, catia.abmarques@gmail.com)
 * */
function anos(){
	var today = new Date();
	var ano = today.getFullYear();
	
	var desde = 2000;
	var ate = ano-1;
	var options = "";
	for(var a=desde; a<=ate; a++){
	  options += "<option value="+a+">"+ a +"</option>";
	}
	//alert(options);
	$('#anoLevantamento').html(options);
	$('#anoLevantamento').val(ate);
}

/**
 * Função de ligação entre html e PHP (ajax) para criação da nova BD
 * @author Catia Marques (cam@med.up.pt, catia.abmarques@gmail.com) 
 * */
function criarBD(){
var data = $('#transicao').serialize();
	var nome = $('#nomeBD').val();
	var anoLevantamento = $('#anoLevantamento').val();
	var semErros = true;
	$('#aviso').hide();
	$('input').focus( function() {
		  $(this).removeClass('error');
	});
	nome = nome.trim();
	if(!nome){//empty
		semErros = false;
		$("#nomeBD").addClass("error");
		$('#aviso').html('<p>Nome da base de dados é necessário!!</p>'); 
		$('#aviso').show();
	}
	if(isNaN(anoLevantamento) || !(anoLevantamento)){
		semErros = false;
		$("#anoLevantamento").addClass("error");
		$('#aviso').append('<p>Por favor insira um ano válido!!</p>'); 
		$('#aviso').show();
	}
	if(semErros){
		//unclicable
		$("#paraTras").attr('disabled', 'disabled');
		//loading
		getDataAjax('../../functions/criarNovaBD.php',data,'html',function(info){
			/*/**/
			var option = info[0];
			switch(option){
				case '1'://erros com nome
					$('#aviso').html('<p>'+ info.substring(1) +'</p>'); 
					$('#aviso').show();
					$('#nomeBD').addClass("error");
					break;
				case '2':
				case '3':
					$('#aviso').html('<p>Alguma(s) tabela(s) de visualização(VIEWS)/funções não foram copiadas!</p>'); 
					$('#aviso').show();
				case '0':
					$('form').hide("slow",function(){
						$('#sucesso').html('<p>Base de dados criada com sucesso!</p>');
						$('#sucesso').show("slow");
					});
					break;
				default:
					$('#aviso').html('<p>'+info+'</p>');
					$('#aviso').show();
			}
			$("#paraTras").removeAttr("disabled");
		});
	}
}

/**
 * Despoletado por click em checkbox (id) e seleciona todas as checkbox dentro de um elemento com id = checkid
 * @author Catia Marques (cam@med.up.pt, catia.abmarques@gmail.com) 
 * @param string id - atributo id do elemento (checkbox) que despoleta ação
 * @param string checkid - atributo id de elemento (div) que contém o conjunto de checkbox-alvo a selecionar
 * */
function selecionaTodas(id, checkid){
	if(document.getElementById(id).checked) { 
        $('#'+checkid).find(':checkbox').each(function() { //para todas as checkbox de id
            this.checked = true;                
        });
    }else{
        $('#'+checkid).find(':checkbox').each(function() { //para todas as checkbox de id
            this.checked = false;                        
        });         
    }
}

/**
 * Trata de chamadas AJAX
 * @author Catia Marques (cam@med.up.pt, catia.abmarques@gmail.com)
 * @param url chamada da página
 * @param data informação a enviar
 * @param datatype tipo do valor retornado
 * @param handleData função que trata a informação após chamada ajax em caso de sucesso 
 * */
function getDataAjax(url, data, datatype, handleData){
	$.ajax({
		type:'POST',
		url:url,
		async: false,
		data:data,
		dataType: datatype,
		success: function(info){
			handleData(info);
		},
        fail: function(){
        	alert("Failed :/");
        },
        error: function( xhr, status, errorThrown ) {
            alert( "Sorry, there was a problem!" );
        }
        });

}




