var id;

var rep = [];
var orig = '';

var issn= '',
revista= '' ,
titulo= '' ,
autores= '',
volume= '' ,
issue= '' ,
prim= '' ,
ult= '',
link= '' ,
estado= '';

var content = [];

function editArt() {
	id = $("#chave-pubIntMan-Art").text();	
	
	issn = $("#issn-pubManualInt-Art");
	revista = $("#revista-pubManualInt-Art");
	titulo = $("#titulo-pubManualInt-Art");
	autores = $("#autores-pubManualInt-Art");
	volume = $("#volume-pubManualInt-Art");
	issue = $("#issue-pubManualInt-Art");
	prim = $("#prim-pubManualInt-Art");
	ult = $("#ult-pubManualInt-Art");
	link = $("#link-pubManualInt-Art");
	estado = $("#estado-pubManualInt-Art");

	issn.val($("#pubManualInt-Art #td_outJMAN_issn_" + id).text());
	revista.val($("#pubManualInt-Art #td_outJMAN_nomepub_" + id).text());
	titulo.val($("#pubManualInt-Art #td_outJMAN_titulo_" + id).text().trim());
	autores.val($("#pubManualInt-Art #td_outJMAN_autores_" + id).text().trim());
	volume.val($("#pubManualInt-Art #td_outJMAN_volume_" + id).text().trim());
	issue.val($("#pubManualInt-Art #td_outJMAN_issue_" + id).text().trim());
	prim.val($("#pubManualInt-Art #td_outJMAN_prim_" + id).text().trim());
	ult.val($("#pubManualInt-Art #td_outJMAN_ult_" + id).text().trim());
	link.val($("#pubManualInt-Art #td_outJMAN_link_" + id).text().trim());
	estado.val($("#pubManualInt-Art #td_outJMAN_estado_" + id).text().trim());

	tips = $(".validateTips-pubManualInt-Art");

	$("#dialog-form-pubManualInt-Art").dialog({
        autoOpen: false,
        width: 600,
        length: 500,
        modal: true,
        buttons: {
            Confirmar: function () {
                var bValid = true;
				
                if (bValid) {
                    $("#pubManualInt-Art #td_outJMAN_issn_" + id).replaceWith("<td id='td_outJMAN_issn_" + id + "'>" + issn.val() + "</td>");
                    $("#pubManualInt-Art #td_outJMAN_nomepub_" + id).replaceWith("<td id='td_outJMAN_nomepub_" + id + "'>" + revista.val() + "</td>");
                    $("#pubManualInt-Art #td_outJMAN_titulo_" + id).replaceWith("<td id='td_outJMAN_titulo_" + id + "'>" + titulo.val() + "</td>");
                    $("#pubManualInt-Art #td_outJMAN_autores_" + id).replaceWith("<td id='td_outJMAN_autores_" + id + "'>" + autores.val() + "</td>");
                    $("#pubManualInt-Art #td_outJMAN_volume_" + id).replaceWith("<td id='td_outJMAN_volume_" + id + "'>" + volume.val() + "</td>");

                    $("#pubManualInt-Art #td_outJMAN_issue_" + id).replaceWith("<td id='td_outJMAN_issue_" + id + "'>" + issue.val() + "</td>");
                    $("#pubManualInt-Art #td_outJMAN_prim_" + id).replaceWith("<td id='td_outJMAN_prim_" + id + "'>" + prim.val() + "</td>");
                    $("#pubManualInt-Art #td_outJMAN_ult_" + id).replaceWith("<td id='td_outJMAN_ult_" + id + "'>" + ult.val() + "</td>");

                    $("#pubManualInt-Art #td_outJMAN_link_" + id).replaceWith("<td id='td_outJMAN_link_" + id + "'>" + link.val() + "</td>");
                    $("#pubManualInt-Art #td_outJMAN_estado_" + id).replaceWith("<td id='td_outJMAN_estado_" + id + "'>" + estado.val() + "</td>");
					
                    savePubManIntArtigoInBD(id, issn.val(), revista.val(), titulo.val(), autores.val(), volume.val(), issue.val(), prim.val(), ult.val(), link.val(), estado.val(), $('#login').text(), $('#dep').text(), '', $('#tabela').text());
                    $(this).dialog("close");
                }
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        }
    });
		
	$("#dialog-form-pubManualInt-Art").dialog("open");
}

function editSum () {
	id = $("#chave-pubIntMan-Sum").text();

	issn = $("#issn-pubManualInt-Sum");
	revista = $("#revista-pubManualInt-Sum");
	titulo = $("#titulo-pubManualInt-Sum");
	autores = $("#autores-pubManualInt-Sum");
	volume = $("#volume-pubManualInt-Sum");
	issue = $("#issue-pubManualInt-Sum");
	prim = $("#prim-pubManualInt-Sum");
	ult = $("#ult-pubManualInt-Sum");
	link = $("#link-pubManualInt-Sum");
	estado = $("#estado-pubManualInt-Sum");
	
	issn.val($("#pubManualInt-Sum #td_outJAMAN_issn_" + id).text());
	revista.val($("#pubManualInt-Sum #td_outJAMAN_nomepub_" + id).text());
	titulo.val($("#pubManualInt-Sum #td_outJAMAN_titulo_" + id).text().trim());
	autores.val($("#pubManualInt-Sum #td_outJAMAN_autores_" + id).text().trim());
	volume.val($("#pubManualInt-Sum #td_outJAMAN_volume_" + id).text().trim());
	issue.val($("#pubManualInt-Sum #td_outJAMAN_issue_" + id).text().trim());
	prim.val($("#pubManualInt-Sum #td_outJAMAN_prim_" + id).text().trim());
	ult.val($("#pubManualInt-Sum #td_outJAMAN_ult_" + id).text().trim());
	link.val($("#pubManualInt-Sum #td_outJAMAN_link_" + id).text().trim());
	estado.val($("#pubManualInt-Sum #td_outJAMAN_estado_" + id).text().trim());

	$("#dialog-form-pubManualInt-Sum").dialog({
        autoOpen: false,
        width: 600,
        length: 500,
        modal: true,
        buttons: {
            Confirmar: function () {
                var bValid = true;
				
                if (bValid) {
                    $("#pubManualInt-Con #td_outJAMAN_issn_" + id).replaceWith("<td id='td_outJAMAN_issn_" + id + "'>" + issn.val() + "</td>");
                    $("#pubManualInt-Con #td_outJAMAN_nomepub_" + id).replaceWith("<td id='td_outJAMAN_nomepub_" + id + "'>" + revista.val() + "</td>");
                    $("#pubManualInt-Con #td_outJAMAN_titulo_" + id).replaceWith("<td id='td_outJAMAN_titulo_" + id + "'>" + titulo.val() + "</td>");
                    $("#pubManualInt-Con #td_outJAMAN_autores_" + id).replaceWith("<td id='td_outJAMAN_autores_" + id + "'>" + autores.val() + "</td>");
                    $("#pubManualInt-Con #td_outJAMAN_volume_" + id).replaceWith("<td id='td_outJAMAN_volume_" + id + "'>" + volume.val() + "</td>");
                    $("#pubManualInt-Con #td_outJAMAN_issue_" + id).replaceWith("<td id='td_outJAMAN_issue_" + id + "'>" + issue.val() + "</td>");
                    $("#pubManualInt-Con #td_outJAMAN_prim_" + id).replaceWith("<td id='td_outJAMAN_prim_" + id + "'>" + prim.val() + "</td>");
                    $("#pubManualInt-Con #td_outJAMAN_ult_" + id).replaceWith("<td id='td_outJAMAN_ult_" + id + "'>" + ult.val() + "</td>");
                    $("#pubManualInt-Con #td_outJAMAN_link_" + id).replaceWith("<td id='td_outJAMAN_link_" + id + "'>" + link.val() + "</td>");
                    $("#pubManualInt-Con #td_outJAMAN_estado_" + id).replaceWith("<td id='td_outJAMAN_estado_" + id + "'>" + estado.val() + "</td>");

                    savePubManIntArtigoInBD(id, issn.val(), revista.val(), titulo.val(), autores.val(), volume.val(), issue.val(), prim.val(), ult.val(), link.val(), estado.val(), $('#login').text(), $('#dep').text(), '', $('#tabela').text());
					
                    $(this).dialog("close");
                }
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        }
    });
	
	$("#dialog-form-pubManualInt-Sum").dialog("open");
}

function editPRP() {
	id = $("#chave-pubIntMan-PRP").text();

	issn = $("#issn-pubManualInt-Art");
	revista = $("#revista-pubManualInt-Art");
	titulo = $("#titulo-pubManualInt-Art");
	autores = $("#autores-pubManualInt-Art");
	volume = $("#volume-pubManualInt-Art");
	issue = $("#issue-pubManualInt-Art");
	prim = $("#prim-pubManualInt-Art");
	ult = $("#ult-pubManualInt-Art");
	link = $("#link-pubManualInt-Art");
	estado = $("#estado-pubManualInt-Art");
	
	issn.val($("#pubManualInt-Rev #td_outPRPMAN_issn_" + id).text());
	revista.val($("#pubManualInt-Rev #td_outPRPMAN_nomepub_" + id).text());
	titulo.val($("#pubManualInt-Rev #td_outPRPMAN_titulo_" + id).text().trim());
	autores.val($("#pubManualInt-Rev #td_outPRPMAN_autores_" + id).text().trim());
	volume.val($("#pubManualInt-Rev #td_outPRPMAN_volume_" + id).text().trim());
	issue.val($("#pubManualInt-Rev #td_outPRPMAN_issue_" + id).text().trim());
	prim.val($("#pubManualInt-Rev #td_outPRPMAN_prim_" + id).text().trim());
	ult.val($("#pubManualInt-Rev #td_outPRPMAN_ult_" + id).text().trim());
	link.val($("#pubManualInt-Rev #td_outPRPMAN_link_" + id).text().trim());
	estado.val($("#pubManualInt-Rev #td_outPRPMAN_estado_" + id).text().trim());

	tips = $(".validateTips-pubManualInt-Art");
	
	 $("#dialog-form-pubManualInt-Art").dialog({
        autoOpen: false,
        width: 600,
        length: 500,
        modal: true,
        buttons: {
            Confirmar: function () {
                var bValid = true;
				
                if (bValid) {
                    $("#pubManualInt-Art #td_outPRPMAN_issn_" + id).replaceWith("<td id='td_outPRPMAN_issn_" + id + "'>" + issn.val() + "</td>");
                    $("#pubManualInt-Art #td_outPRPMAN_nomepub_" + id).replaceWith("<td id='td_outPRPMAN_nomepub_" + id + "'>" + revista.val() + "</td>");
                    $("#pubManualInt-Art #td_outPRPMAN_titulo_" + id).replaceWith("<td id='td_outPRPMAN_titulo_" + id + "'>" + titulo.val() + "</td>");
                    $("#pubManualInt-Art #td_outPRPMAN_autores_" + id).replaceWith("<td id='td_outPRPMAN_autores_" + id + "'>" + autores.val() + "</td>");
                    $("#pubManualInt-Art #td_outPRPMAN_volume_" + id).replaceWith("<td id='td_outPRPMAN_volume_" + id + "'>" + volume.val() + "</td>");

                    $("#pubManualInt-Art #td_outPRPMAN_issue_" + id).replaceWith("<td id='td_outPRPMAN_issue_" + id + "'>" + issue.val() + "</td>");
                    $("#pubManualInt-Art #td_outPRPMAN_prim_" + id).replaceWith("<td id='td_outPRPMAN_prim_" + id + "'>" + prim.val() + "</td>");
                    $("#pubManualInt-Art #td_outPRPMAN_ult_" + id).replaceWith("<td id='td_outPRPMAN_ult_" + id + "'>" + ult.val() + "</td>");

                    $("#pubManualInt-Art #td_outPRPMAN_link_" + id).replaceWith("<td id='td_outPRPMAN_link_" + id + "'>" + link.val() + "</td>");
                    $("#pubManualInt-Art #td_outPRPMAN_estado_" + id).replaceWith("<td id='td_outPRPMAN_estado_" + id + "'>" + estado.val() + "</td>");
					
                    savePubManIntArtigoInBD(id, issn.val(), revista.val(), titulo.val(), autores.val(), volume.val(), issue.val(), prim.val(), ult.val(), link.val(), estado.val(), $('#login').text(), $('#dep').text(), '', $('#tabela').text());
                    $(this).dialog("close");
                }
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        }
    });

	$("#dialog-form-pubManualInt-Art").dialog("open");
}
   
function editCP() {
	id = $("#chave-pubIntMan-Con").text();

	isbn = $("#isbn-pubManualInt-Con");
	nomepub = $("#nomepub-pubManualInt-Con");
	editora = $("#editor-pubManualInt-Con");
	titulo = $("#titulo-pubManualInt-Con");
	autores = $("#autores-pubManualInt-Con");
	prim = $("#prim-pubManualInt-Con");
	ult = $("#ult-pubManualInt-Con");
	link = $("#link-pubManualInt-Con");
	estado = $("#estado-pubManualInt-Con");

	isbn.val($("#pubManualInt-Con #td_outCPMAN_isbn_" + id).text());
	nomepub.val($("#pubManualInt-Con #td_outCPMAN_nomepub_" + id).text());
	editora.val($("#pubManualInt-Con #td_outCPMAN_editor_" + id).text().trim());
	titulo.val($("#pubManualInt-Con #td_outCPMAN_titulo_" + id).text().trim());
	autores.val($("#pubManualInt-Con #td_outCPMAN_autores_" + id).text().trim());
	prim.val($("#pubManualInt-Con #td_outCPMAN_prim_" + id).text().trim());
	ult.val($("#pubManualInt-Con #td_outCPMAN_ult_" + id).text().trim());
	link.val($("#pubManualInt-Con #td_outCPMAN_link_" + id).text().trim());
	estado.val($("#pubManualInt-Con #td_outCPMAN_estado_" + id).text().trim());

	$("#dialog-form-pubManualInt-Con").dialog({
        autoOpen: false,
        width: 600,
        length: 500,
        modal: true,
        buttons: {
            Confirmar: function () {
                var bValid = true;

                if (bValid) {
                    $("#pubManualInt-Con #td_outCPMAN_isbn_" + id).replaceWith("<td id='td_outCPMAN_isbn_" + id + "'>" + isbn.val() + "</td>");
                    $("#pubManualInt-Con #td_outCPMAN_nomepub_" + id).replaceWith("<td id='td_outCPMAN_nomepub_" + id + "'>" + nomepub.val() + "</td>");
                    $("#pubManualInt-Con #td_outCPMAN_editor_" + id).replaceWith("<td id='td_outCPMAN_editor_" + id + "'>" + editora.val() + "</td>");
                    $("#pubManualInt-Con #td_outCPMAN_titulo_" + id).replaceWith("<td id='td_outCPMAN_titulo_" + id + "'>" + titulo.val() + "</td>");
                    $("#pubManualInt-Con #td_outCPMAN_autores_" + id).replaceWith("<td id='td_outCPMAN_autores_" + id + "'>" + autores.val() + "</td>");
                    $("#pubManualInt-Con #td_outCPMAN_prim_" + id).replaceWith("<td id='td_outCPMAN_prim_" + id + "'>" + prim.val() + "</td>");
                    $("#pubManualInt-Con #td_outCPMAN_ult_" + id).replaceWith("<td id='td_outCPMAN_ult_" + id + "'>" + ult.val() + "</td>");
                    $("#pubManualInt-Con #td_outCPMAN_link_" + id).replaceWith("<td id='td_outCPMAN_link_" + id + "'>" + link.val() + "</td>");
                    $("#pubManualInt-Con #td_outCPMAN_estado_" + id).replaceWith("<td id='td_outCPMAN_estado_" + id + "'>" + estado.val() + "</td>");

                    savePubManIntCPInBD(id, isbn.val(), nomepub.val(), editora.val(), titulo.val(), autores.val(), prim.val(), ult.val(), link.val(), estado.val(), $('#login').text(), $('#dep').text(), '', $('#tabela').text());

                    $(this).dialog("close");
                }
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        }
    });
	
	$("#dialog-form-pubManualInt-Con").dialog("open");
}

function editOA() {

	id = $("#chave-pubIntMan-Oth").text();

	isbn = $("#isbn-pubManualInt-Oth");
	nomepub = $("#nomepub-pubManualInt-Oth");
	editora = $("#editor-pubManualInt-Oth");
	titulo = $("#titulo-pubManualInt-Oth");
	autores = $("#autores-pubManualInt-Oth");
	prim = $("#prim-pubManualInt-Oth");
	ult = $("#ult-pubManualInt-Oth");
	link = $("#link-pubManualInt-Oth");
	estado = $("#estado-pubManualInt-Oth");

	isbn.val($("#pubManualInt-Oth #td_outOAMAN_isbn_" + id).text());
	nomepub.val($("#pubManualInt-Oth #td_outOAMAN_nomepub_" + id).text());
	editora.val($("#pubManualInt-Oth #td_outOAMAN_editor_" + id).text().trim());
	titulo.val($("#pubManualInt-Oth #td_outOAMAN_titulo_" + id).text().trim());
	autores.val($("#pubManualInt-Oth #td_outOAMAN_autores_" + id).text().trim());
	prim.val($("#pubManualInt-Oth #td_outOAMAN_prim_" + id).text().trim());
	ult.val($("#pubManualInt-Oth #td_outOAMAN_ult_" + id).text().trim());
	link.val($("#pubManualInt-Oth #td_outOAMAN_link_" + id).text().trim());
	estado.val($("#pubManualInt-Oth #td_outOAMAN_estado_" + id).text().trim());

    $("#dialog-form-pubManualInt-Oth").dialog({
        autoOpen: false,
        width: 600,
        length: 500,
        modal: true,
        buttons: {
            Confirmar: function () {
                var bValid = true;                

                if (bValid) {
                    $("#pubManualInt-Oth #td_outOAMAN_isbn_" + id).replaceWith("<td id='td_outOAMAN_isbn_" + id + "'>" + isbn.val() + "</td>");
                    $("#pubManualInt-Oth #td_outOAMAN_nomepub_" + id).replaceWith("<td id='td_outOAMAN_nomepub_" + id + "'>" + nomepub.val() + "</td>");
                    $("#pubManualInt-Oth #td_outOAMAN_editor_" + id).replaceWith("<td id='td_outOAMAN_editor_" + id + "'>" + editora.val() + "</td>");
                    $("#pubManualInt-Oth #td_outOAMAN_titulo_" + id).replaceWith("<td id='td_outOAMAN_titulo_" + id + "'>" + titulo.val() + "</td>");
                    $("#pubManualInt-Oth #td_outOAMAN_autores_" + id).replaceWith("<td id='td_outOAMAN_autores_" + id + "'>" + autores.val() + "</td>");
                    $("#pubManualInt-Oth #td_outOAMAN_prim_" + id).replaceWith("<td id='td_outOAMAN_prim_" + id + "'>" + prim.val() + "</td>");
                    $("#pubManualInt-Oth #td_outOAMAN_ult_" + id).replaceWith("<td id='td_outOAMAN_ult_" + id + "'>" + ult.val() + "</td>");
                    $("#pubManualInt-Oth #td_outOAMAN_link_" + id).replaceWith("<td id='td_outOAMAN_link_" + id + "'>" + link.val() + "</td>");
                    $("#pubManualInt-Oth #td_outOAMAN_estado_" + id).replaceWith("<td id='td_outOAMAN_estado_" + id + "'>" + estado.val() + "</td>");

                    savePubManIntCPInBD(id, isbn.val(), nomepub.val(), editora.val(), titulo.val(), autores.val(), prim.val(), ult.val(), link.val(), estado.val(), $('#login').text(), $('#dep').text(), '', $('#tabela').text());
                    $(this).dialog("close");
                }
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        }
    });	
	
	$("#dialog-form-pubManualInt-Oth").dialog("open");
}

function editL() {

   id = $("#chave-pubIntMan-Liv").text();

	isbn = $("#isbn-pubManualInt-Liv");
	titulo = $("#titulo-pubManualInt-Liv");
	autores = $("#autores-pubManualInt-Liv");
	editor = $("#editor-pubManualInt-Liv");
	editora = $("#editora-pubManualInt-Liv");
	link = $("#link-pubManualInt-Liv");
	estado = $("#estado-pubManualInt-Liv");

	isbn.val($("#pubManualInt-Liv #td_outLMAN_isbn_" + id).text());
	titulo.val($("#pubManualInt-Liv #td_outLMAN_titulo_" + id).text().trim());
	autores.val($("#pubManualInt-Liv #td_outLMAN_autores_" + id).text().trim());
	editor.val($("#pubManualInt-Liv #td_outLMAN_editor_" + id).text().trim());
	editora.val($("#pubManualInt-Liv #td_outLMAN_editora_" + id).text().trim());
	link.val($("#pubManualInt-Liv #td_outLMAN_link_" + id).text().trim());
	estado.val($("#pubManualInt-Liv #td_outLMAN_estado_" + id).text().trim());
		
	 $("#dialog-form-pubManualInt-Liv").dialog({
			autoOpen: false,
			width: 600,
			length: 500,
			modal: true,
			buttons: {
				Confirmar: function () {
					var bValid = true;

					if (bValid) {
						$("#pubManualInt-Liv #td_outLMAN_isbn_" + id).replaceWith("<td id='td_outLMAN_isbn_" + id + "'>" + isbn.val() + "</td>");
						$("#pubManualInt-Liv #td_outLMAN_titulo_" + id).replaceWith("<td id='td_outLMAN_titulo_" + id + "'>" + titulo.val() + "</td>");
						$("#pubManualInt-Liv #td_outLMAN_autores_" + id).replaceWith("<td id='td_outLMAN_autores_" + id + "'>" + autores.val() + "</td>");
						$("#pubManualInt-Liv #td_outLMAN_editor_" + id).replaceWith("<td id='td_outLMAN_editor_" + id + "'>" + editor.val() + "</td>");
						$("#pubManualInt-Liv #td_outLMAN_editora_" + id).replaceWith("<td id='td_outLMAN_editora_" + id + "'>" + editora.val() + "</td>");
						$("#pubManualInt-Liv #td_outLMAN_link_" + id).replaceWith("<td id='td_outLMAN_link_" + id + "'>" + link.val() + "</td>");
						$("#pubManualInt-Liv #td_outLMAN_estado_" + id).replaceWith("<td id='td_outLMAN_estado_" + id + "'>" + estado.val() + "</td>");

						savePubManIntLivroInBD(id, isbn.val(), titulo.val(), autores.val(), editor.val(), editora.val(), link.val(), estado.val(), $('#login').text(), $('#dep').text(), '', $('#tabela').text());
						$(this).dialog("close");
					}
				},
				Cancelar: function () {
					$(this).dialog("close");
				}
			}
		});
	
	$("#dialog-form-pubManualInt-Liv").dialog("open");
}

function editCL () {
	id = $("#chave-pubIntMan-CLiv").text();

	isbn = $("#isbn-pubManualInt-CLiv");
	nomepub = $("#nomepub-pubManualInt-CLiv");
	editora = $("#editora-pubManualInt-CLiv");
	editor = $("#editor-pubManualInt-CLiv");
	titulo = $("#titulo-pubManualInt-CLiv");
	autores = $("#autores-pubManualInt-CLiv");
	prim = $("#prim-pubManualInt-CLiv");
	ult = $("#ult-pubManualInt-CLiv");
	link = $("#link-pubManualInt-CLiv");
	estado = $("#estado-pubManualInt-CLiv");

	isbn.val($("#pubManualInt-CLiv #td_outCLMAN_isbn_" + id).text());
	nomepub.val($("#pubManualInt-CLiv #td_outCLMAN_nomepub_" + id).text());
	editora.val($("#pubManualInt-CLiv #td_outCLMAN_editora_" + id).text().trim());
	editor.val($("#pubManualInt-CLiv #td_outCLMAN_editor_" + id).text().trim());
	titulo.val($("#pubManualInt-CLiv #td_outCLMAN_titulo_" + id).text().trim());
	autores.val($("#pubManualInt-CLiv #td_outCLMAN_autores_" + id).text().trim());
	prim.val($("#pubManualInt-CLiv #td_outCLMAN_prim_" + id).text().trim());
	ult.val($("#pubManualInt-CLiv #td_outCLMAN_ult_" + id).text().trim());
	link.val($("#pubManualInt-CLiv #td_outCLMAN_link_" + id).text().trim());
	estado.val($("#pubManualInt-CLiv #td_outCLMAN_estado_" + id).text().trim());

	$("#dialog-form-pubManualInt-CLiv").dialog({
		autoOpen: false,
		width: 600,
		length: 500,
		modal: true,
		buttons: {
			Confirmar: function () {
				var bValid = true;

				if (bValid) {
					$("#pubManualInt-CLiv #td_outCLMAN_isbn_" + id).replaceWith("<td id='td_outCLMAN_isbn_" + id + "'>" + isbn.val() + "</td>");
					$("#pubManualInt-CLiv #td_outCLMAN_nomepub_" + id).replaceWith("<td id='td_outCLMAN_nomepub_" + id + "'>" + nomepub.val() + "</td>");
					$("#pubManualInt-CLiv #td_outCLMAN_editora_" + id).replaceWith("<td id='td_outCLMAN_editora_" + id + "'>" + editora.val() + "</td>");
					$("#pubManualInt-CLiv #td_outCLMAN_editor_" + id).replaceWith("<td id='td_outCLMAN_editor_" + id + "'>" + editor.val() + "</td>");
					$("#pubManualInt-CLiv #td_outCLMAN_titulo_" + id).replaceWith("<td id='td_outCLMAN_titulo_" + id + "'>" + titulo.val() + "</td>");
					$("#pubManualInt-CLiv #td_outCLMAN_autores_" + id).replaceWith("<td id='td_outCLMAN_autores_" + id + "'>" + autores.val() + "</td>");
					$("#pubManualInt-CLiv #td_outCLMAN_prim_" + id).replaceWith("<td id='td_outCLMAN_prim_" + id + "'>" + prim.val() + "</td>");
					$("#pubManualInt-CLiv #td_outCLMAN_ult_" + id).replaceWith("<td id='td_outCLMAN_ult_" + id + "'>" + ult.val() + "</td>");
					$("#pubManualInt-CLiv #td_outCLMAN_link_" + id).replaceWith("<td id='td_outCLMAN_link_" + id + "'>" + link.val() + "</td>");
					$("#pubManualInt-CLiv #td_outCLMAN_estado_" + id).replaceWith("<td id='td_outCLMAN_estado_" + id + "'>" + estado.val() + "</td>");

					savePubManIntCLivroInBD(id, isbn.val(), nomepub.val(), editora.val(), editor.val(), titulo.val(), autores.val(), prim.val(), ult.val(), link.val(), estado.val(), $('#login').text(), $('#dep').text(), '', $('#tabela').text());
					$(this).dialog("close");
				}
			},
			Cancelar: function () {
				$(this).dialog("close");
			}
		}
	});
	
	$("#dialog-form-pubManualInt-CLiv").dialog("open");        
}

function editManArt() {

	id = $("#chave-pubNacMan-Art").text();
	
	issn = $("#issn-pubManualNac-Art");
	revista = $("#revista-pubManualNac-Art");
	titulo = $("#titulo-pubManualNac-Art");
	autores = $("#autores-pubManualNac-Art");
	volume = $("#volume-pubManualNac-Art");
	issue = $("#issue-pubManualNac-Art");
	prim = $("#prim-pubManualNac-Art");
	ult = $("#ult-pubManualNac-Art");
	link = $("#link-pubManualNac-Art");
	estado = $("#estado-pubManualNac-Art");

	issn.val($("#pubManualNac-Art #td_outJNMAN_issn_" + id).text());
	revista.val($("#pubManualNac-Art #td_outJNMAN_nomepub_" + id).text());
	titulo.val($("#pubManualNac-Art #td_outJNMAN_titulo_" + id).text().trim());
	autores.val($("#pubManualNac-Art #td_outJNMAN_autores_" + id).text().trim());
	volume.val($("#pubManualNac-Art #td_outJNMAN_volume_" + id).text().trim());
	issue.val($("#pubManualNac-Art #td_outJNMAN_issue_" + id).text().trim());
	prim.val($("#pubManualNac-Art #td_outJNMAN_prim_" + id).text().trim());
	ult.val($("#pubManualNac-Art #td_outJNMAN_ult_" + id).text().trim());
	link.val($("#pubManualNac-Art #td_outJNMAN_link_" + id).text().trim());
	estado.val($("#pubManualNac-Art #td_outJNMAN_estado_" + id).text().trim());
	
	 $("#dialog-form-pubManualNac-Art").dialog({
        autoOpen: false,
        width: 600,
        length: 500,
        modal: true,
        buttons: {
            Confirmar: function () {
                var bValid = true;

                allFields.removeClass("ui-state-error");
				
                if (bValid) {
                    $("#pubManualNac-Art #td_outJNMAN_issn_" + id).replaceWith("<td id='td_outJNMAN_issn_" + id + "'>" + issn.val() + "</td>");
                    $("#pubManualNac-Art #td_outJNMAN_nomepub_" + id).replaceWith("<td id='td_outJNMAN_nomepub_" + id + "'>" + revista.val() + "</td>");
                    $("#pubManualNac-Art #td_outJNMAN_titulo_" + id).replaceWith("<td id='td_outJNMAN_titulo_" + id + "'>" + titulo.val() + "</td>");
                    $("#pubManualNac-Art #td_outJNMAN_autores_" + id).replaceWith("<td id='td_outJNMAN_autores_" + id + "'>" + autores.val() + "</td>");
                    $("#pubManualNac-Art #td_outJNMAN_volume_" + id).replaceWith("<td id='td_outJNMAN_volume_" + id + "'>" + volume.val() + "</td>");

                    $("#pubManualNac-Art #td_outJNMAN_issue_" + id).replaceWith("<td id='td_outJNMAN_issue_" + id + "'>" + issue.val() + "</td>");
                    $("#pubManualNac-Art #td_outJNMAN_prim_" + id).replaceWith("<td id='td_outJNMAN_prim_" + id + "'>" + prim.val() + "</td>");
                    $("#pubManualNac-Art #td_outJNMAN_ult_" + id).replaceWith("<td id='td_outJNMAN_ult_" + id + "'>" + ult.val() + "</td>");

                    $("#pubManualNac-Art #td_outJNMAN_link_" + id).replaceWith("<td id='td_outJNMAN_link_" + id + "'>" + link.val() + "</td>");
                    $("#pubManualNac-Art #td_outJNMAN_estado_" + id).replaceWith("<td id='td_outJNMAN_estado_" + id + "'>" + estado.val() + "</td>");

                    savePubManIntArtigoInBD(id, issn.val(), revista.val(), titulo.val(), autores.val(), volume.val(), issue.val(), prim.val(), ult.val(), link.val(), estado.val(), $('#login').text(), $('#dep').text(), '', $('#tabela').text());
                    $(this).dialog("close");
                }
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        }
    });
	

	$("#dialog-form-pubManualNac-Art").dialog("open");
}

function editManSum() {
	id = $("#chave-pubNacMan-Sum").text();

	issn = $("#issn-pubManualNac-Sum");
	revista = $("#revista-pubManualNac-Sum");
	titulo = $("#titulo-pubManualNac-Sum");
	autores = $("#autores-pubManualNac-Sum");
	volume = $("#volume-pubManualNac-Sum");
	issue = $("#issue-pubManualNac-Sum");
	prim = $("#prim-pubManualNac-Sum");
	ult = $("#ult-pubManualNac-Sum");
	link = $("#link-pubManualNac-Sum");
	estado = $("#estado-pubManualNac-Sum");

	issn.val($("#pubManualNac-Sum #td_outJANMAN_issn_" + id).text());
	revista.val($("#pubManualNac-Sum #td_outJANMAN_nomepub_" + id).text());
	titulo.val($("#pubManualNac-Sum #td_outJANMAN_titulo_" + id).text().trim());
	autores.val($("#pubManualNac-Sum #td_outJANMAN_autores_" + id).text().trim());
	volume.val($("#pubManualNac-Sum #td_outJANMAN_volume_" + id).text().trim());
	issue.val($("#pubManualNac-Sum #td_outJANMAN_issue_" + id).text().trim());
	prim.val($("#pubManualNac-Sum #td_outJANMAN_prim_" + id).text().trim());
	ult.val($("#pubManualNac-Sum #td_outJANMAN_ult_" + id).text().trim());
	link.val($("#pubManualNac-Sum #td_outJANMAN_link_" + id).text().trim());
	estado.val($("#pubManualNac-Sum #td_outJANMAN_estado_" + id).text().trim());

	$("#dialog-form-pubManualNac-Sum").dialog({
        autoOpen: false,
        width: 600,
        length: 500,
        modal: true,
        buttons: {
            Confirmar: function () {
				var bValid = true;
				
                if (bValid) {
					$("#pubManualNac-Sum #td_outJANMAN_issn_" + id).replaceWith("<td id='td_outJANMAN_issn_" + id + "'>" + issn.val() + "</td>");
					$("#pubManualNac-Sum #td_outJANMAN_nomepub_" + id).replaceWith("<td id='td_outJANMAN_nomepub_" + id + "'>" + revista.val() + "</td>");
					$("#pubManualNac-Sum #td_outJANMAN_titulo_" + id).replaceWith("<td id='td_outJANMAN_titulo_" + id + "'>" + titulo.val() + "</td>");
					$("#pubManualNac-Sum #td_outJANMAN_autores_" + id).replaceWith("<td id='td_outJANMAN_autores_" + id + "'>" + autores.val() + "</td>");
					$("#pubManualNac-Sum #td_outJANMAN_volume_" + id).replaceWith("<td id='td_outJANMAN_volume_" + id + "'>" + volume.val() + "</td>");

					$("#pubManualNac-Sum #td_outJANMAN_issue_" + id).replaceWith("<td id='td_outJANMAN_issue_" + id + "'>" + issue.val() + "</td>");
					$("#pubManualNac-Sum #td_outJANMAN_prim_" + id).replaceWith("<td id='td_outJANMAN_prim_" + id + "'>" + prim.val() + "</td>");
					$("#pubManualNac-Sum #td_outJANMAN_ult_" + id).replaceWith("<td id='td_outJANMAN_ult_" + id + "'>" + ult.val() + "</td>");

					$("#pubManualNac-Sum #td_outJANMAN_link_" + id).replaceWith("<td id='td_outJANMAN_link_" + id + "'>" + link.val() + "</td>");
					$("#pubManualNac-Sum #td_outJANMAN_estado_" + id).replaceWith("<td id='td_outJANMAN_estado_" + id + "'>" + estado.val() + "</td>");

					savePubManIntArtigoInBD(id, issn.val(), revista.val(), titulo.val(), autores.val(), volume.val(), issue.val(), prim.val(), ult.val(), link.val(), estado.val(), $('#login').text(), $('#dep').text(), '', $('#tabela').text());
					$(this).dialog("close");
				}
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        }
    });
	
	$("#dialog-form-pubManualNac-Sum").dialog("open");       
}

function editManPRP() {
	 id = $("#chave-pubNacMan-Rev").text();

	issn = $("#issn-pubManualNac-Rev");
	revista = $("#revista-pubManualNac-Rev");
	titulo = $("#titulo-pubManualNac-Rev");
	autores = $("#autores-pubManualNac-Rev");
	volume = $("#volume-pubManualNac-Rev");
	issue = $("#issue-pubManualNac-Rev");
	prim = $("#prim-pubManualNac-Rev");
	ult = $("#ult-pubManualNac-Rev");
	link = $("#link-pubManualNac-Rev");
	estado = $("#estado-pubManualNac-Rev");

	issn.val($("#pubManualNac-Rev #td_outPRPNMAN_issn_" + id).text());
	revista.val($("#pubManualNac-Rev #td_outPRPNMAN_nomepub_" + id).text());
	titulo.val($("#pubManualNac-Rev #td_outPRPNMAN_titulo_" + id).text().trim());
	autores.val($("#pubManualNac-Rev #td_outPRPNMAN_autores_" + id).text().trim());
	volume.val($("#pubManualNac-Rev #td_outPRPNMAN_volume_" + id).text().trim());
	issue.val($("#pubManualNac-Rev #td_outPRPNMAN_issue_" + id).text().trim());
	prim.val($("#pubManualNac-Rev #td_outPRPNMAN_prim_" + id).text().trim());
	ult.val($("#pubManualNac-Rev #td_outPRPNMAN_ult_" + id).text().trim());
	link.val($("#pubManualNac-Rev #td_outPRPNMAN_link_" + id).text().trim());
	estado.val($("#pubManualNac-Rev #td_outPRPNMAN_estado_" + id).text().trim());
	
	$("#dialog-form-pubManualNac-Rev").dialog({
        autoOpen: false,
        width: 600,
        length: 500,
        modal: true,
        buttons: {
            Confirmar: function () {
				var bValid = true;

                allFields.removeClass("ui-state-error");

                bValid = bValid && checkName(issn);
                bValid = bValid && checkName(revista);
                bValid = bValid && checkName(titulo);
                bValid = bValid && checkName(autores);
				
                if (bValid) {					
					$("#pubManualNac-Rev #td_outPRPNMAN_issn_" + id).replaceWith("<td id='td_outPRPNMAN_issn_" + id + "'>" + issn.val() + "</td>");
					$("#pubManualNac-Rev #td_outPRPNMAN_nomepub_" + id).replaceWith("<td id='td_outPRPNMAN_nomepub_" + id + "'>" + revista.val() + "</td>");
					$("#pubManualNac-Rev #td_outPRPNMAN_titulo_" + id).replaceWith("<td id='td_outPRPNMAN_titulo_" + id + "'>" + titulo.val() + "</td>");
					$("#pubManualNac-Rev #td_outPRPNMAN_autores_" + id).replaceWith("<td id='td_outPRPNMAN_autores_" + id + "'>" + autores.val() + "</td>");
					$("#pubManualNac-Rev #td_outPRPNMAN_volume_" + id).replaceWith("<td id='td_outPRPNMAN_volume_" + id + "'>" + volume.val() + "</td>");

					$("#pubManualNac-Rev #td_outPRPNMAN_issue_" + id).replaceWith("<td id='td_outPRPNMAN_issue_" + id + "'>" + issue.val() + "</td>");
					$("#pubManualNac-Rev #td_outPRPNMAN_prim_" + id).replaceWith("<td id='td_outPRPNMAN_prim_" + id + "'>" + prim.val() + "</td>");
					$("#pubManualNac-Rev #td_outPRPNMAN_ult_" + id).replaceWith("<td id='td_outPRPNMAN_ult_" + id + "'>" + ult.val() + "</td>");

					$("#pubManualNac-Rev #td_outPRPNMAN_link_" + id).replaceWith("<td id='td_outPRPNMAN_link_" + id + "'>" + link.val() + "</td>");
					$("#pubManualNac-Rev #td_outPRPNMAN_estado_" + id).replaceWith("<td id='td_outPRPNMAN_estado_" + id + "'>" + estado.val() + "</td>");

					savePubManIntArtigoInBD(id, issn.val(), revista.val(), titulo.val(), autores.val(), volume.val(), issue.val(), prim.val(), ult.val(), link.val(), estado.val(), $('#login').text(), $('#dep').text(), '', $('#tabela').text());
					$(this).dialog("close");
				}
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        }
    });

	$("#dialog-form-pubManualNac-Rev").dialog("open");       
}

function editManL() {
	id = $("#chave-pubNacMan-Liv").text();

	isbn = $("#isbn-pubManualNac-Liv");
	titulo = $("#titulo-pubManualNac-Liv");
	autores = $("#autores-pubManualNac-Liv");
	editor = $("#editor-pubManualNac-Liv");
	editora = $("#editora-pubManualNac-Liv");
	link = $("#link-pubManualNac-Liv");
	estado = $("#estado-pubManualNac-Liv");

	isbn.val($("#pubManualNac-Liv #td_outLNMAN_isbn_" + id).text());
	titulo.val($("#pubManualNac-Liv #td_outLNMAN_titulo_" + id).text().trim());
	autores.val($("#pubManualNac-Liv #td_outLNMAN_autores_" + id).text().trim());
	editor.val($("#pubManualNac-Liv #td_outLNMAN_editor_" + id).text().trim());
	editora.val($("#pubManualNac-Liv #td_outLNMAN_editora_" + id).text().trim());
	link.val($("#pubManualNac-Liv #td_outLNMAN_link_" + id).text().trim());
	estado.val($("#pubManualNac-Liv #td_outLNMAN_estado_" + id).text().trim());
	
	$("#dialog-form-pubManualNac-Liv").dialog({
		autoOpen: false,
		width: 600,
		length: 500,
		modal: true,
		buttons: {
			Confirmar: function () {
				var bValid = true;

				if (bValid) {
					$("#pubManualNac-Liv #td_outLNMAN_isbn_" + id).replaceWith("<td id='td_outLNMAN_isbn_" + id + "'>" + isbn.val() + "</td>");
					$("#pubManualNac-Liv #td_outLNMAN_titulo_" + id).replaceWith("<td id='td_outLNMAN_titulo_" + id + "'>" + titulo.val() + "</td>");
					$("#pubManualNac-Liv #td_outLNMAN_autores_" + id).replaceWith("<td id='td_outLNMAN_autores_" + id + "'>" + autores.val() + "</td>");
					$("#pubManualNac-Liv #td_outLNMAN_editor_" + id).replaceWith("<td id='td_outLNMAN_editor_" + id + "'>" + editor.val() + "</td>");
					$("#pubManualNac-Liv #td_outLNMAN_editora_" + id).replaceWith("<td id='td_outLNMAN_editora_" + id + "'>" + editora.val() + "</td>");
					$("#pubManualNac-Liv #td_outLNMAN_link_" + id).replaceWith("<td id='td_outLNMAN_link_" + id + "'>" + link.val() + "</td>");
					$("#pubManualNac-Liv #td_outLNMAN_estado_" + id).replaceWith("<td id='td_outLNMAN_estado_" + id + "'>" + estado.val() + "</td>");

					savePubManIntLivroInBD(id, isbn.val(), titulo.val(), autores.val(), editor.val(), editora.val(), link.val(), estado.val(), $('#login').text(), $('#dep').text(), '', $('#tabela').text());
					$(this).dialog("close");
				}
			},
			Cancelar: function () {
				$(this).dialog("close");
			}
		}
	});
	
	$("#dialog-form-pubManualNac-Liv").dialog("open");        
}

function editManCP() {

	id = $("#chave-pubNacMan-Con").text();

	isbn = $("#isbn-pubManualNac-Con");
	nomepub = $("#nomepub-pubManualNac-Con");
	editora = $("#editor-pubManualNac-Con");
	titulo = $("#titulo-pubManualNac-Con");
	autores = $("#autores-pubManualNac-Con");
	prim = $("#prim-pubManualNac-Con");
	ult = $("#ult-pubManualNac-Con");
	link = $("#link-pubManualNac-Con");
	estado = $("#estado-pubManualNac-Con");

	isbn.val($("#pubManualNac-Con #td_outCPNMAN_isbn_" + id).text());
	nomepub.val($("#pubManualNac-Con #td_outCPNMAN_nomepub_" + id).text());
	editora.val($("#pubManualNac-Con #td_outCPNMAN_editor_" + id).text().trim());
	titulo.val($("#pubManualNac-Con #td_outCPNMAN_titulo_" + id).text().trim());
	autores.val($("#pubManualNac-Con #td_outCPNMAN_autores_" + id).text().trim());
	prim.val($("#pubManualNac-Con #td_outCPNMAN_prim_" + id).text().trim());
	ult.val($("#pubManualNac-Con #td_outCPNMAN_ult_" + id).text().trim());
	link.val($("#pubManualNac-Con #td_outCPNMAN_link_" + id).text().trim());
	estado.val($("#pubManualNac-Con #td_outCPNMAN_estado_" + id).text().trim());
	
	$("#dialog-form-pubManualNac-Con").dialog({
        autoOpen: false,
        width: 600,
        length: 500,
        modal: true,
        buttons: {
            Confirmar: function () {	
				var bValid = true;
				allFields.removeClass("ui-state-error");

                if (bValid) {
                    $("#pubManualNac-Con #td_outCPNMAN_isbn_" + id).replaceWith("<td id='td_outCPNMAN_isbn_" + id + "'>" + isbn.val() + "</td>");
					$("#pubManualNac-Con #td_outCPNMAN_nomepub_" + id).replaceWith("<td id='td_outCPNMAN_nomepub_" + id + "'>" + nomepub.val() + "</td>");
					$("#pubManualNac-Con #td_outCPNMAN_editor_" + id).replaceWith("<td id='td_outCPNMAN_editor_" + id + "'>" + editora.val() + "</td>");
					$("#pubManualNac-Con #td_outCPNMAN_titulo_" + id).replaceWith("<td id='td_outCPNMAN_titulo_" + id + "'>" + titulo.val() + "</td>");
					$("#pubManualNac-Con #td_outCPNMAN_autores_" + id).replaceWith("<td id='td_outCPNMAN_autores_" + id + "'>" + autores.val() + "</td>");
					$("#pubManualNac-Con #td_outCPNMAN_prim_" + id).replaceWith("<td id='td_outCPNMAN_prim_" + id + "'>" + prim.val() + "</td>");
					$("#pubManualNac-Con #td_outCPNMAN_ult_" + id).replaceWith("<td id='td_outCPNMAN_ult_" + id + "'>" + ult.val() + "</td>");
					$("#pubManualNac-Con #td_outCPNMAN_link_" + id).replaceWith("<td id='td_outCPNMAN_link_" + id + "'>" + link.val() + "</td>");
					$("#pubManualNac-Con #td_outCPNMAN_estado_" + id).replaceWith("<td id='td_outCPNMAN_estado_" + id + "'>" + estado.val() + "</td>");
					savePubManIntCPInBD(id, isbn.val(), nomepub.val(), editora.val(), titulo.val(), autores.val(), prim.val(), ult.val(), link.val(), estado.val(), $('#login').text(), $('#dep').text(), '', $('#tabela').text());
					$(this).dialog("close");
                }
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        }
    });

	$("#dialog-form-pubManualNac-Con").dialog("open");
}

function editManOA() {
	id = $("#chave-pubNacMan-Oth").text();

	isbn = $("#isbn-pubManualNac-Oth");
	nomepub = $("#nomepub-pubManualNac-Oth");
	editora = $("#editor-pubManualNac-Oth");
	titulo = $("#titulo-pubManualNac-Oth");
	autores = $("#autores-pubManualNac-Oth");
	prim = $("#prim-pubManualNac-Oth");
	ult = $("#ult-pubManualNac-Oth");
	link = $("#link-pubManualNac-Oth");
	estado = $("#estado-pubManualNac-Oth");

	isbn.val($("#pubManualNac-Oth #td_outOANMAN_isbn_" + id).text());
	nomepub.val($("#pubManualNac-Oth #td_outOANMAN_nomepub_" + id).text());
	editora.val($("#pubManualNac-Oth #td_outOANMAN_editor_" + id).text().trim());
	titulo.val($("#pubManualNac-Oth #td_outOANMAN_titulo_" + id).text().trim());
	autores.val($("#pubManualNac-Oth #td_outOANMAN_autores_" + id).text().trim());
	prim.val($("#pubManualNac-Oth #td_outOANMAN_prim_" + id).text().trim());
	ult.val($("#pubManualNac-Oth #td_outOANMAN_ult_" + id).text().trim());
	link.val($("#pubManualNac-Oth #td_outOANMAN_link_" + id).text().trim());
	estado.val($("#pubManualNac-Oth #td_outOANMAN_estado_" + id).text().trim());
		
	$("#dialog-form-pubManualNac-Oth").dialog({
        autoOpen: false,
        width: 600,
        length: 500,
        modal: true,
        buttons: {
            Confirmar: function () {
                var bValid = true;

                if (bValid) {
                    $("#pubManualInt-Oth #td_outOANMAN_isbn_" + id).replaceWith("<td id='td_outOANMAN_isbn_" + id + "'>" + isbn.val() + "</td>");
                    $("#pubManualInt-Oth #td_outOANNMAN_nomepub_" + id).replaceWith("<td id='td_outOANMAN_nomepub_" + id + "'>" + nomepub.val() + "</td>");
                    $("#pubManualInt-Oth #td_outOANMAN_editor_" + id).replaceWith("<td id='td_outOANMAN_editor_" + id + "'>" + editora.val() + "</td>");
                    $("#pubManualInt-Oth #td_outOANMAN_titulo_" + id).replaceWith("<td id='td_outOANMAN_titulo_" + id + "'>" + titulo.val() + "</td>");
                    $("#pubManualInt-Oth #td_outOANMAN_autores_" + id).replaceWith("<td id='td_outOANMAN_autores_" + id + "'>" + autores.val() + "</td>");
                    $("#pubManualInt-Oth #td_outOANMAN_prim_" + id).replaceWith("<td id='td_outOANMAN_prim_" + id + "'>" + prim.val() + "</td>");
                    $("#pubManualInt-Oth #td_outOANMAN_ult_" + id).replaceWith("<td id='td_outOANMAN_ult_" + id + "'>" + ult.val() + "</td>");
                    $("#pubManualInt-Oth #td_outOANMAN_link_" + id).replaceWith("<td id='td_outOANMAN_link_" + id + "'>" + link.val() + "</td>");
                    $("#pubManualInt-Oth #td_outOANMAN_estado_" + id).replaceWith("<td id='td_outOANMAN_estado_" + id + "'>" + estado.val() + "</td>");

                    savePubManIntCPInBD(id, isbn.val(), nomepub.val(), editora.val(), titulo.val(), autores.val(), prim.val(), ult.val(), link.val(), estado.val(), $('#login').text(), $('#dep').text(), '', $('#tabela').text());
                    $(this).dialog("close");
                }
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        }
    });	
	
	$("#dialog-form-pubManualNac-Oth").dialog("open");
}

function editManCL() {

	id = $("#chave-pubNacMan-CLiv").text();
	isbn = $("#isbn-pubManualNac-CLiv");
	nomepub = $("#nomepub-pubManualNac-CLiv");
	editora = $("#editora-pubManualNac-CLiv");
	editor = $("#editor-pubManualNac-CLiv");
	titulo = $("#titulo-pubManualNac-CLiv");
	autores = $("#autores-pubManualNac-CLiv");
	prim = $("#prim-pubManualNac-CLiv");
	ult = $("#ult-pubManualNac-CLiv");
	link = $("#link-pubManualNac-CLiv");
	estado = $("#estado-pubManualNac-CLiv");

	isbn.val($("#pubManualNac-CLiv #td_outCLNMAN_isbn_" + id).text());
	nomepub.val($("#pubManualNac-CLiv #td_outCLNMAN_nomepub_" + id).text());
	editora.val($("#pubManualNac-CLiv #td_outCLNMAN_editora_" + id).text().trim());
	editor.val($("#pubManualNac-CLiv #td_outCLNMAN_editor_" + id).text().trim());
	titulo.val($("#pubManualNac-CLiv #td_outCLNMAN_titulo_" + id).text().trim());
	autores.val($("#pubManualNac-CLiv #td_outCLNMAN_autores_" + id).text().trim());
	prim.val($("#pubManualNac-CLiv #td_outCLNMAN_prim_" + id).text().trim());
	ult.val($("#pubManualNac-CLiv #td_outCLNMAN_ult_" + id).text().trim());
	link.val($("#pubManualNac-CLiv #td_outCLNMAN_link_" + id).text().trim());
	estado.val($("#pubManualNac-CLiv #td_outCLNMAN_estado_" + id).text().trim());
	
	$("#dialog-form-pubManualNac-CLiv").dialog({
        autoOpen: false,
        width: 600,
        length: 500,
        modal: true,
        buttons: {
            Confirmar: function () {
				var bValid = true;

                if (bValid) {
					$("#pubManualNac-CLiv #td_outCLNMAN_isbn_" + id).replaceWith("<td id='td_outCLNMAN_isbn_" + id + "'>" + isbn.val() + "</td>");
					$("#pubManualNac-CLiv #td_outCLNMAN_nomepub_" + id).replaceWith("<td id='td_outCLNMAN_nomepub_" + id + "'>" + nomepub.val() + "</td>");
					$("#pubManualNac-CLiv #td_outCLNMAN_editora_" + id).replaceWith("<td id='td_outCLNMAN_editora_" + id + "'>" + editora.val() + "</td>");
					$("#pubManualNac-CLiv #td_outCLNMAN_editor_" + id).replaceWith("<td id='td_outCLNMAN_editor_" + id + "'>" + editor.val() + "</td>");
					$("#pubManualNac-CLiv #td_outCLNMAN_titulo_" + id).replaceWith("<td id='td_outCLNMAN_titulo_" + id + "'>" + titulo.val() + "</td>");
					$("#pubManualNac-CLiv #td_outCLNMAN_autores_" + id).replaceWith("<td id='td_outCLNMAN_autores_" + id + "'>" + autores.val() + "</td>");
					$("#pubManualNac-CLiv #td_outCLNMAN_prim_" + id).replaceWith("<td id='td_outCLNMAN_prim_" + id + "'>" + prim.val() + "</td>");
					$("#pubManualNac-CLiv #td_outCLNMAN_ult_" + id).replaceWith("<td id='td_outCLNMAN_ult_" + id + "'>" + ult.val() + "</td>");
					$("#pubManualNac-CLiv #td_outCLNMAN_link_" + id).replaceWith("<td id='td_outCLNMAN_link_" + id + "'>" + link.val() + "</td>");
					$("#pubManualNac-CLiv #td_outCLNMAN_estado_" + id).replaceWith("<td id='td_outCLNMAN_estado_" + id + "'>" + estado.val() + "</td>");

					savePubManIntCLivroInBD(id, isbn.val(), nomepub.val(), editora.val(), editor.val(), titulo.val(), autores.val(), prim.val(), ult.val(), link.val(), estado.val(), $('#login').text(), $('#dep').text(), '', $('#tabela').text());
					$(this).dialog("close");
				}
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        }
    });
	
	$("#dialog-form-pubManualNac-CLiv").dialog("open");
}


/**
 * Interface de Eliminaè¤¯ de Registos!!
 * Autor: Paulo Miguel Rodrigues (prodrigues@med.up.pt, paulomiguelarodrigues@gmail.com)
 */

function apagarArbProjecto() {
    var action = confirm('Está prestes a eliminar uma arbitragem projecto. Tem a certeza que pretende eliminar? Esta acção é destrutiva e definitiva.');

    if (action) {
        //deleteArbProjecto($('#chave-arb-projecto').text());
    }
}

/**
 * Funè¶¥s AJAX de Eliminaè¤¯ de Registos!!
 * Autor: Paulo Miguel Rodrigues (prodrigues@med.up.pt, paulomiguelarodrigues@gmail.com)
 */

function deleteInvestigadorInBD(id, login) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    var params = "a=2&id=" + id + "&login=" + login;
    xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function deleteHabilitacaoInBD(id, login) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    var params = "a=4&id=" + id + "&login=" + login;
    xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function deleteAgregacaoInBD(id, login) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    var params = "a=6&id=" + id + "&login=" + login;
    xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function deleteCaracterizacaoInBD(id, login) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    var params = "a=8&id=" + id + "&login=" + login;
    xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function deleteDirCursoInBD(id, login) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    var params = "a=11&id=" + id + "&login=" + login;
    xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function deleteColIntActInBD(id, login) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    var params = "a=13&id=" + id + "&login=" + login;
    xmlhttp.open("POST", "../../functions/saveBD.php", false);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function deleteColExtActInBD(id, login) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    var params = "a=15&id=" + id + "&login=" + login;
    xmlhttp.open("POST", "../../functions/saveBD.php", false);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function deleteUniInvInBD(id, login) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    var params = "a=17&id=" + id + "&login=" + login;
    xmlhttp.open("POST", "../../functions/saveBD.php", false);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function deletePubManIntArt(id) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    var params = "a=19&id=" + id;
    xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function deleteProjecto(id, login) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    var params = "a=25&id=" + id + "&login=" + login;
    xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function deletePublicacao(id, login, tabela) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
	
    var params = "a=19&id=" + id + "&login=" + login + "&tabela=" + tabela;
	xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function deleteArbProjecto(id) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    var params = "a=27&id=" + id;
    xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function deleteConferencia(id, login) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    var params = "a=29&id=" + id + "&login=" + login;
    xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function deleteApresentacao(id, login) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    var params = "a=31&id=" + id + "&login=" + login;
    xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function deleteArbRevista(id, login) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    var params = "a=33&id=" + id + "&login=" + login;
    xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function deletePremio(id, login) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    var params = "a=35&id=" + id + "&login=" + login;
    xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function deleteSC(id, login) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    var params = "a=37&id=" + id + "&login=" + login;
    xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function deleteRede(id, login) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    var params = "a=39&id=" + id + "&login=" + login;
    xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function deleteMissao(id, login) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    var params = "a=41&id=" + id + "&login=" + login;
    xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function deleteOrientacao(id, login) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    var params = "a=43&id=" + id + "&login=" + login;
    xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function deleteJuri(id, login) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    var params = "a=45&id=" + id + "&login=" + login;
    xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function deleteOutAct(id, login) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    var params = "a=47&id=" + id + "&login=" + login;
    xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function deleteAcaoDivulgacao(id, login) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    var params = "a=49&id=" + id + "&login=" + login;
    xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function deleteProduto(id, login) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    var params = "a=51&id=" + id + "&login=" + login;
    xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function insertInvestigadorInBD(nome, num, data, login, dep) {
	var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    var params = "a=69&nome=" + nome + "&num=" + num + "&data=" + data + "&login=" + login + "&dep=" + dep;
    xmlhttp.open("POST", "../../functions/saveBD.php", false);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function insertHabilitacaoInBD(idinv, anoinicio, anofim, curso, instituicao, grau, dist, bolsa, titulo, percentagem, orientador, oinstituicao,corientador, coinstituicao, login, dep) {
	var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
	
    var params = "a=70&anoinicio=" + anoinicio + "&anofim=" + anofim + "&curso=" + curso + "&instituicao=" + instituicao + "&grau=" + grau + "&dist=" + dist + "&bolsa=" + bolsa + "&titulo=" + titulo + "&percentagem=" + percentagem + "&orientador=" + orientador + "&oinstituicao=" + oinstituicao + "&corientador=" + corientador + "&coinstituicao=" + coinstituicao + "&login=" + login + "&dep=" + dep + "&idinv=" + idinv;
	xmlhttp.open("POST", "../../functions/saveBD.php", true);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}   

function insertAgregacaoInBD(idinv, unanimidade, login, dep) {
	var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
		
	 if (unanimidade == 'Sim')
        var params = "a=71&idinv=" + idinv + "&unanimidade=1" + "&login=" + login + "&dep=" + dep;
    else
        var params = "a=71&idinv=" + idinv + "&unanimidade=0" + "&login=" + login + "&dep=" + dep;
	
	xmlhttp.open("POST", "../../functions/saveBD.php", true);		
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}     

function insertCaracterizacaoDocentesInBD(idinv, tipo, percentagem, login, dep) {
	var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
	
    var params = "a=72&idinv=" + idinv + "&tipo=" + tipo + "&percentagem=" + percentagem + "&login=" + login + "&dep=" + dep;
	xmlhttp.open("POST", "../../functions/saveBD.php", true);		
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}    

function insertCaracterizacaoPosDocInBD(idinv, bolsa, percentagem, login, dep) {
	var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
		  
	var params = "a=73&idinv=" + idinv + "&bolsa=" + bolsa + "&percentagem=" + percentagem + "&login=" + login + "&dep=" + dep;
	xmlhttp.open("POST", "../../functions/saveBD.php", true);		
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}     


function insertDirCursoInBD(idinv, datainicio, datafim, curso, grau, login, dep) {
	var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
	   
	var params = "a=74&idinv=" + idinv + "&datainicio=" + datainicio + "&datafim=" + datafim + "&curso=" + curso + "&grau=" + grau + "&login=" + login + "&dep=" + dep;
	xmlhttp.open("POST", "../../functions/saveBD.php", true);		
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function insertColIntActInBD(idinv, nomecient, departamento, login, dep) {
	var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
	
	var params = "a=75&idinv=" + idinv + "&nomecient=" + nomecient + "&departamento=" + departamento + "&login=" + login + "&dep=" + dep;
	xmlhttp.open("POST", "../../functions/saveBD.php", true);		
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function insertColExtActInBD(idinv, nomecient, instituicao, pais, login, dep) {
	var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
	   
	var params = "a=76&idinv=" + idinv + "&nomecient=" + nomecient + "&instituicao=" + instituicao + "&pais=" + pais + "&login=" + login + "&dep=" + dep;
	xmlhttp.open("POST", "../../functions/saveBD.php", true);		
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function insertUniInvInBD(idinv, unidade, avaliacao, percentagem, responsavel, login, dep) {
	var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
	
	var params = "a=77&idinv=" + idinv + "&unidade=" + unidade + "&avaliacao=" + avaliacao + "&percentagem=" + percentagem + "&responsavel=" + responsavel + "&login=" + login + "&dep=" + dep;
	
	xmlhttp.open("POST", "../../functions/saveBD.php", true);		
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function insertProjectoInBD(idinv, tipo, entidade, inst, monsol, monapr, monfmup, resp, codigo, titulo, dataini, datafim, link, estado, login, dep) {
	var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
	
	var params = "a=78&idinv=" + idinv + "&tipo=" + tipo + "&entidade=" + entidade + "&inst=" + inst + "&monsol=" + monsol + "&monapr=" + monapr + "&monfmup=" + monfmup + "&resp=" + resp + "&codigo=" + codigo + "&titulo=" + titulo + "&dataini=" + dataini + "&datafim=" + datafim + "&link=" + link + "&estado=" + estado + "&login=" + login + "&dep=" + dep;
	xmlhttp.open("POST", "../../functions/saveBD.php", true);		
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}
	
function insertConferenciaInBD(idinv, ambito, dataini, datafim, titulo, local, npart, link, login, dep) {
	var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
			
	var params = "a=79&idinv=" + idinv + "&ambito=" + ambito + "&titulo=" + titulo + "&local=" + local 
				   + "&npart=" + npart + "&link=" + link + "&dataini=" + dataini + "&datafim=" + datafim 
				   + "&login=" + login + "&dep=" + dep;

	xmlhttp.open("POST", "../../functions/saveBD.php", true);		
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}	

function insertApresentacaoInBD(idinv, iconfconv, iconfpart, isem, nconfconv, nconfpart, nsem, login, dep) {
	var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
	
	var params = "a=80&idinv=" + idinv + "&iconfconv=" + iconfconv + "&iconfpart=" + iconfpart + "&isem=" + isem + "&nconfconv=" + nconfconv + "&nconfpart=" + nconfpart + "&nsem=" + nsem + "&login=" + login + "&dep=" + dep;
	xmlhttp.open("POST", "../../functions/saveBD.php", true);		
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}
         
function insertArbRevistaInBD(idinv, tipo, issn, titulo, link, login, dep) {
	var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
	
	var params = "a=81&idinv=" + idinv + "&tipo=" + tipo + "&issn=" + issn + "&titulo=" + titulo + "&link=" + link + "&login=" + login + "&dep=" + dep;
	xmlhttp.open("POST", "../../functions/saveBD.php", true);		
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}    

function insertPremioInBD(idinv, tipo, nome, contexto, montante, link, login, dep) {
	var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
	
	var params = "a=82&idinv=" + idinv + "&tipo=" + tipo + "&nome=" + nome + "&contexto=" + contexto + "&montante=" + montante + "&link=" + link + "&login=" + login + "&dep=" + dep;
	xmlhttp.open("POST", "../../functions/saveBD.php", true);		
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}
 
function insertSCInBD(idinv, tipo, datainicio, datafim, cargo, link, nome, login, dep) {
	var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
	
	var params = "a=83&idinv=" + idinv + "&tipo=" + tipo + "&datainicio=" + datainicio + "&datafim=" + datafim + "&cargo=" + cargo + "&link=" + link + "&nome=" + nome + "&login=" + login + "&dep=" + dep;
	xmlhttp.open("POST", "../../functions/saveBD.php", true);		
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
} 

function insertRedesInBD(idinv, tipo, datainicio, datafim, link, nome, login, dep) {
	var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
	
	var params = "a=84&idinv=" + idinv + "&tipo=" + tipo + "&datainicio=" + datainicio + "&datafim=" + datafim + "&link=" + link + "&nome=" + nome + "&login=" + login + "&dep=" + dep;
	xmlhttp.open("POST", "../../functions/saveBD.php", true);		
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function insertMissoesInBD(idinv, datainicio, datafim, motivacao, instituicao, pais, ambtese, login, dep) {
	var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
	
	if (ambtese == 'Sim')
		var params = "a=85&idinv=" + idinv + "&datainicio=" + datainicio + "&datafim=" + datafim + "&motivacao=" + motivacao + "&instituicao=" + instituicao + "&pais=" + pais + "&ambtese=1" + "&login=" + login + "&dep=" + dep;
	else
		var params = "a=85&idinv=" + idinv + "&datainicio=" + datainicio + "&datafim=" + datafim + "&motivacao=" + motivacao + "&instituicao=" + instituicao + "&pais=" + pais + "&ambtese=0" + "&login=" + login + "&dep=" + dep;
	
	xmlhttp.open("POST", "../../functions/saveBD.php", true);		
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}    

function insertOrientacoesInBD(idinv, tipo, tipoori, estado, titulo, login, dep) {
	var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
	var params = "a=86&idinv=" + idinv + "&tipo=" + tipo + "&tipoori=" + tipoori + "&estado=" + estado + "&titulo=" + titulo + "&login=" + login + "&dep=" + dep;	
	xmlhttp.open("POST", "../../functions/saveBD.php", true);		
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}    

function insertJurisInBD(idinv, agrargfmup, agrvogalfmup, agrargext, agrvogalext, doutargfmup, doutvogalfmup, doutargext, doutvogalext, mesargfmup, mesvogalfmup, mesargext, mesvogalext, mesintfmup, mesintvogalfmup, mesintargext, mesintvogalext, login, dep) {
	var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
	
	var params = "a=87&idinv=" + idinv + "&agrargfmup=" + agrargfmup + "&agrvogalfmup=" + agrvogalfmup + "&agrargext=" + agrargext + "&agrvogalext=" + agrvogalext + "&doutargfmup=" + doutargfmup + "&doutvogalfmup=" + doutvogalfmup + "&doutargext=" + doutargext + "&doutvogalext=" + doutvogalext + "&mesargfmup=" + mesargfmup + "&mesvogalfmup=" + mesvogalfmup + "&mesargext=" + mesargext + "&mesvogalext=" + mesvogalext + "&mesintfmup=" + mesintfmup + "&mesintvogalfmup=" + mesintvogalfmup + "&mesintargext=" + mesintargext + "&mesintvogalext=" + mesintvogalext + "&login=" + login + "&dep=" + dep;
	xmlhttp.open("POST", "../../functions/saveBD.php", true);		
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function insertOutActInBD(idinv, texapoio, livdidaticos, livdivulgacao, app, webp, login, dep) {
	var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
	  
	var params = "a=88&idinv=" + idinv + "&texapoio=" + texapoio + "&livdidaticos=" + livdidaticos + "&livdivulgacao=" + livdivulgacao + "&app=" + app + "&webp=" + webp + "&login=" + login + "&dep=" + dep;
	xmlhttp.open("POST", "../../functions/saveBD.php", true);		
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function insertAcaoDivulgacaoInBD(idinv, titulo, data, local, npart, login, dep) {
	var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
  
  	var params = "a=89&idinv=" + idinv + "&titulo=" + titulo + "&data=" + data + "&local=" + local + "&npart=" + npart + "&login=" + login + "&dep=" + dep;
	xmlhttp.open("POST", "../../functions/saveBD.php", true);		
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}	

function insertProdutoInBD(idinv, tipo, preco, login, dep) {
	var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
	  
  	var params = "a=90&idinv=" + idinv + "&tipo=" + tipo + "&preco=" + preco + "&login=" + login + "&dep=" + dep;
	xmlhttp.open("POST", "../../functions/saveBD.php", true);		
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function insertPubArtInBD(idinv, issn, revista, titulo, autores, volume, issue, prim, ult, citacoes, estado, login, dep, tipopublicacao, descr) {
	var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
	
	
   if (estado == "Publicado/Published")
        var params = "a=91&idinv=" + idinv + "&issn=" + issn + "&revista=" + revista + "&titulo=" + titulo + "&autores=" + autores + "&volume=" + volume + "&issue=" + issue + "&prim=" + prim + "&ult=" + ult + "&citacao=" + citacoes + "&estado=0" + "&login=" + login + "&dep=" + dep + "&tipopublicacao=" + tipopublicacao + "&descr=" + descr;
    else
        var params = "a=91&idinv=" + idinv + "&issn=" + issn + "&revista=" + revista + "&titulo=" + titulo + "&autores=" + autores + "&volume=" + volume + "&issue=" + issue + "&prim=" + prim + "&ult=" + ult + "&citacao=" + citacoes + "&estado=1" + "&login=" + login + "&dep=" + dep + "&tipopublicacao=" + tipopublicacao + "&descr=" + descr;
	
	xmlhttp.open("POST", "../../functions/saveBD.php", true);		
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function insertPubConfInBD(idinv, isbn, nomepub, editores, colecao, volume, titulo, autores, prim, ult, estado, login, dep, tipopublicacao, descr) {
	var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }	
	
    if (estado == "Publicado/Published")
        var params = "a=92&idinv=" + idinv + "&isbn=" + isbn + "&nomepub=" + nomepub + "&editores=" + editores + "&colecao=" + colecao + "&volume=" + volume + "&titulo=" + titulo + "&prim=" + prim + "&ult=" + ult + "&autores=" + autores + "&estado=0" + "&login=" + login + "&dep=" + dep + "&tipopublicacao=" + tipopublicacao + "&descr=" + descr;
    else
        var params = "a=92&idinv=" + idinv + "&isbn=" + isbn + "&nomepub=" + nomepub + "&editores=" + editores + "&colecao=" + colecao + "&volume=" + volume + "&titulo=" + titulo + "&prim=" + prim + "&ult=" + ult + "&autores=" + autores + "&estado=1" + "&login=" + login + "&dep=" + dep + "&tipopublicacao=" + tipopublicacao + "&descr=" + descr;
	
	xmlhttp.open("POST", "../../functions/saveBD.php", true);		
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}
	
function insertPubLivInBD(idinv, nomepub, issn, isbn, titulo, autores, volume, issue, ar, colecao, prim, ult, citacoes, conftitle, language, editor, estado, login, dep, tipopublicacao, descr) {
	var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
	
	
   if (estado == "Publicado/Published")
        var params = "a=93&idinv=" + idinv + "&nomepub=" + nomepub + "&issn=" + issn + "&isbn=" + isbn + "&titulo=" + titulo + "&autores=" + autores + "&volume=" + volume + "&issue=" + issue + "&ar=" + ar + "&colecao=" + colecao + "&prim=" + prim + "&ult=" + ult + "&citacoes=" + citacoes + "&conftitle=" + conftitle + "&language=" + language + "&editor=" + editor + "&estado=0" + "&login=" + login + "&dep=" + dep + "&tipopublicacao=" + tipopublicacao + "&descr=" + descr;
    else
        var params = "a=93&idinv=" + idinv + "&nomepub=" + nomepub + "&issn=" + issn + "&isbn=" + isbn + "&titulo=" + titulo + "&autores=" + autores + "&volume=" + volume + "&issue=" + issue + "&ar=" + ar + "&colecao=" + colecao + "&prim=" + prim + "&ult=" + ult + "&citacoes=" + citacoes + "&conftitle=" + conftitle + "&language=" + language + "&editor=" + editor + "&estado=1" + "&login=" + login + "&dep=" + dep + "&tipopublicacao=" + tipopublicacao + "&descr=" + descr;
	
	
	xmlhttp.open("POST", "../../functions/saveBD.php", true);		
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}	

function insertPubManPatentesInBD(idinv, npatente, ipc, titulo, autores, data, link, estado, login, dep) {
	var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
	
   if (estado == "Publicado/Published")
        var params = "a=94&idinv=" + idinv + "&npatente=" + npatente + "&ipc=" + ipc + "&titulo=" + titulo + "&autores=" + autores + "&data=" + data + "&link=" + link + "&estado=0" + "&login=" + login + "&dep=" + dep;
    else
        var params = "a=94&idinv=" + idinv + "&npatente=" + npatente + "&ipc=" + ipc + "&titulo=" + titulo + "&autores=" + autores + "&data=" + data + "&link=" + link + "&estado=1" + "&login=" + login + "&dep=" + dep;
 
	xmlhttp.open("POST", "../../functions/saveBD.php", true);		
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}   

function insertPubManLivInBD(idinv, titulo, autores, editor, editora, link, estado, login, dep, descr) {
	var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
	
   if (estado == "Publicado/Published")
        var params = "a=95&idinv=" + idinv + "&titulo=" + titulo + "&autores=" + autores + "&editor=" + editor + "&editora=" + editora + "&link=" + link + "&estado=0" + "&login=" + login + "&dep=" + dep + "&descr=" + descr;
    else
        var params = "a=95&idinv=" + idinv + "&titulo=" + titulo + "&autores=" + autores + "&editor=" + editor + "&editora=" + editora + "&link=" + link + "&estado=1" + "&login=" + login + "&dep=" + dep + "&descr=" + descr;
 
	xmlhttp.open("POST", "../../functions/saveBD.php", true);		
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function  insertPubManCLivInBD(idinv, isbn, nomepub, editora, editor, titulo, autores, prim, ult, link, estado, login, dep, descr) {
	var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
	
   if (estado == "Publicado/Published")
        var params = "a=96&idinv=" + idinv + "&isbn=" + isbn + "&nomepub=" + nomepub + "&titulo=" + titulo + "&autores=" + autores + "&editor=" + editor + "&prim=" + prim + "&ult=" + ult + "&editora=" + editora + "&link=" + link + "&estado=0" + "&login=" + login + "&dep=" + dep + "&descr=" + descr;
    else
        var params = "a=96&idinv=" + idinv + "&isbn=" + isbn + "&nomepub=" + nomepub + "&titulo=" + titulo + "&autores=" + autores + "&editor=" + editor + "&prim=" + prim + "&ult=" + ult + "&editora=" + editora + "&link=" + link + "&estado=1" + "&login=" + login + "&dep=" + dep + "&descr=" + descr;

	xmlhttp.open("POST", "../../functions/saveBD.php", true);		
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}                 
                                  
																		               
/**
 * Funè¶¥s AJAX de actualizaè¤¯ de Informaè¤¯!!
 * Autor: Paulo Miguel Rodrigues (prodrigues@med.up.pt, paulomiguelarodrigues@gmail.com)
 */

function saveInvestigadorInBD(nome, num, data, login, dep, alt) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
	
    var params = "a=1&nome=" + nome + "&num=" + num + "&data=" + data + "&id=" + id + "&login=" + login + "&dep=" + dep + "&alt=" + alt;
    xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function saveHabilitacaoInBD(id, anoinicio, anofim, curso, instituicao, grau, dist, bolsa, titulo, percentagem, orientador, oinstituicao, corientador ,coinstituicao, login, dep, alt) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
		
    var params = "a=3&anoinicio=" + anoinicio + "&anofim=" + anofim + "&id=" + id + "&curso=" + curso + "&instituicao=" + instituicao + "&grau=" + grau + "&dist=" + dist + "&bolsa=" + bolsa + "&titulo=" + titulo + "&percentagem=" + percentagem+ "&orientador=" + orientador + "&oinstituicao=" + oinstituicao + "&corientador=" + corientador + "&coinstituicao=" + coinstituicao + "&login=" + login + "&dep=" + dep + "&alt=" + alt;
    xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function saveAgregacaoInBD(id, unanimidade, login, dep, alt) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
	
    if (unanimidade == 'Sim')
        var params = "a=5&id=" + id + "&unanimidade=1" + "&login=" + login + "&dep=" + dep + "&alt=" + alt;
    else
        var params = "a=5&id=" + id + "&unanimidade=0" + "&login=" + login + "&dep=" + dep + "&alt=" + alt;

    xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function saveCaracterizacaoDocentesInBD(id, tipo, percentagem, login, dep, alt) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    var params = "a=7&id=" + id + "&tipo=" + tipo + "&percentagem=" + percentagem + "&login=" + login + "&dep=" + dep + "&alt=" + alt;    
    xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function saveCaracterizacaoPosDocInBD(id, bolsa, percentagem, login, dep, alt) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
	
    var params = "a=9&id=" + id + "&bolsa=" + bolsa + "&percentagem=" + percentagem + "&login=" + login + "&dep=" + dep + "&alt=" + alt;
    xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function saveDirCursoInBD(id, datainicio, datafim, curso, grau, login, dep, alt) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    var params = "a=10&id=" + id + "&datainicio=" + datainicio + "&datafim=" + datafim + "&curso=" + curso + "&grau=" + grau + "&login=" + login + "&dep=" + dep + "&alt=" + alt;
    xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function saveColIntActInBD(id, nomecient, departamento, login, dep, alt) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
		
    var params = "a=12&id=" + id + "&nomecient=" + nomecient + "&departamento=" + departamento + "&login=" + login + "&dep=" + dep + "&alt=" + alt;
    xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function saveColExtActInBD(id, nomecient, instituicao, pais, login, dep, alt) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
	
    var params = "a=14&id=" + id + "&nomecient=" + nomecient + "&instituicao=" + instituicao + "&pais=" + pais + "&login=" + login + "&dep=" + dep + "&alt=" + alt;
    xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function saveUniInvInBD(id, unidade, avaliacao, percentagem, responsavel, login, dep, alt) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    if (responsavel == 'Sim')
        var params = "a=16&id=" + id + "&unidade=" + unidade + "&avaliacao=" + avaliacao + "&percentagem=" + percentagem + "&responsavel=1" + "&login=" + login + "&dep=" + dep + "&alt=" + alt;
    else
        var params = "a=16&id=" + id + "&unidade=" + unidade + "&avaliacao=" + avaliacao + "&percentagem=" + percentagem + "&responsavel=0" + "&login=" + login + "&dep=" + dep + "&alt=" + alt;

    xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function savePubManPatentesInBD(id, npatente, ipc, titulo, autores, data, link, estado, login, dep, alt) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
	
    if (estado == 'Publicado/Published')
        var params = "a=18&id=" + id + "&npatente=" + npatente + "&ipc=" + ipc + "&titulo=" + titulo + "&autores=" + autores + "&data=" + data + "&link=" + link + "&estado=0" + "&login=" + login + "&dep=" + dep + "&alt=" + alt;
    else
        var params = "a=18&id=" + id + "&npatente=" + npatente + "&ipc=" + ipc + "&titulo=" + titulo + "&autores=" + autores + "&data=" + data + "&link=" + link + "&estado=1" + "&login=" + login + "&dep=" + dep + "&alt=" + alt;

    xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function savePubManIntArtigoInBD(id, issn, revista, titulo, autores, volume, issue, prim, ult, link, estado, login, dep, alt, tabela) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
	
    if (estado == "Publicado/Published")
        var params = "a=20&id=" + id + "&issn=" + issn + "&revista=" + revista + "&titulo=" + titulo + "&autores=" + autores + "&volume=" + volume + "&issue=" + issue + "&prim=" + prim + "&ult=" + ult + "&link=" + link + "&estado=0" + "&login=" + login + "&dep=" + dep + "&alt=" + alt + "&tabela=" + tabela;
    else
        var params = "a=20&id=" + id + "&issn=" + issn + "&revista=" + revista + "&titulo=" + titulo + "&autores=" + autores + "&volume=" + volume + "&issue=" + issue + "&prim=" + prim + "&ult=" + ult + "&link=" + link + "&estado=1" + "&login=" + login + "&dep=" + dep + "&alt=" + alt + "&tabela=" + tabela;
	
	xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function savePubManIntLivroInBD(id, isbn, titulo, autores, editor, editora, link, estado, login, dep, alt,tabela) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
	
    if (estado == 'Publicado/Published')
        var params = "a=21&id=" + id + "&isbn=" + isbn + "&titulo=" + titulo + "&autores=" + autores + "&editor=" + editor + "&editora=" + editora + "&link=" + link + "&estado=0" + "&login=" + login + "&dep=" + dep + "&alt=" + alt + "&tabela=" + tabela;
    else
        var params = "a=21&id=" + id + "&isbn=" + isbn + "&titulo=" + titulo + "&autores=" + autores + "&editor=" + editor + "&editora=" + editora + "&link=" + link + "&estado=1" + "&login=" + login + "&dep=" + dep + "&alt=" + alt + "&tabela=" + tabela;
		
    xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function savePubManIntCLivroInBD(id, isbn, nomepub, editora, editor, titulo, autores, prim, ult, link, estado, login, dep, alt,tabela) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
	
    if (estado == 'Publicado/Published')
        var params = "a=22&id=" + id + "&isbn=" + isbn + "&nomepub=" + nomepub + "&editora=" + editora + "&editor=" + editor + "&titulo=" + titulo + "&autores=" + autores + "&prim=" + prim + "&ult=" + ult + "&link=" + link + "&estado=0" + "&login=" + login + "&dep=" + dep + "&alt=" + alt + "&tabela=" + tabela;
    else
        var params = "a=22&id=" + id + "&isbn=" + isbn + "&nomepub=" + nomepub + "&editora=" + editora + "&editor=" + editor + "&titulo=" + titulo + "&autores=" + autores + "&prim=" + prim + "&ult=" + ult + "&link=" + link + "&estado=1" + "&login=" + login + "&dep=" + dep + "&alt=" + alt + "&tabela=" + tabela;

    xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function savePubManIntCPInBD(id, isbn, nomepub, editora, titulo, autores, prim, ult, link, estado, login, dep, alt,tabela) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
		
    if (estado == 'Publicado/Published')
        var params = "a=23&id=" + id + "&isbn=" + isbn + "&nomepub=" + nomepub + "&editora=" + editora + "&titulo=" + titulo + "&autores=" + autores + "&prim=" + prim + "&ult=" + ult + "&link=" + link + "&estado=0" + "&login=" + login + "&dep=" + dep + "&alt=" + alt + "&tabela=" + tabela;
    else
        var params = "a=23&id=" + id + "&isbn=" + isbn + "&nomepub=" + nomepub + "&editora=" + editora + "&titulo=" + titulo + "&autores=" + autores + "&prim=" + prim + "&ult=" + ult + "&link=" + link + "&estado=1" + "&login=" + login + "&dep=" + dep + "&alt=" + alt + "&tabela=" + tabela;
	
	xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function saveProjectoInBD(id, tipo, entidade, inst, monsol, monapr, monfmup, resp, codigo, titulo, dataini, datafim, link, estado, login, alt, dep) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    if (resp == 'Sim')
        resp = 1;
    else
        resp = 0;
   
    var params = "a=24&id=" + id + "&tipo=" + tipo + "&entidade=" + entidade + "&inst=" + inst + "&monsol=" + monsol + "&monapr=" + monapr + "&monfmup=" + monfmup + "&resp=" + resp + "&codigo=" + codigo + "&titulo=" + titulo + "&dataini=" + dataini + "&datafim=" + datafim + "&link=" + link + "&estado=" + estado + "&login=" + login + "&alt=" + alt + "&dep=" + dep;

    xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function saveArbProjectoInBD(id, api, apn) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    var params = "a=26&id=" + id + "&api=" + api + "&apn=" + apn;

    xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function saveConferenciaInBD(id, ambito, tipo, dataini, datafim, titulo, local, npart, link, login, dep, alt) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    var params = "a=28&id=" + id + "&ambito=" + ambito + "&tipo=" + tipo 
							+ "&dataini=" + dataini + "&datafim=" + datafim 
							+ "&titulo=" + titulo + "&local=" + local 
							+ "&npart=" + npart + "&link=" + link 
							+ "&login=" + login + "&dep=" + dep + "&alt=" + alt;
							
	xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function saveApresentacaoInBD(id, iconfconv, iconfpart, isem, nconfconv, nconfpart, nsem, login, dep, alt) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    var params = "a=30&id=" + id + "&iconfconv=" + iconfconv + "&iconfpart=" + iconfpart + "&isem=" + isem + "&nconfconv=" + nconfconv + "&nconfpart=" + nconfpart + "&nsem=" + nsem + "&login=" + login + "&dep=" + dep + "&alt=" + alt;

    xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function saveArbRevistaInBD(id, tipo, issn, titulo, link, login, dep, alt) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

   
   var params = "a=32&id=" + id + "&tipo=" + tipo + "&issn=" + issn + "&titulo=" + titulo + "&link=" + link + "&login=" + login + "&dep=" + dep + "&alt=" + alt;
	xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function savePremioInBD(id, tipo, nome, contexto, montante, link, login, dep, alt) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    var params = "a=34&id=" + id + "&tipo=" + tipo + "&nome=" + nome + "&contexto=" + contexto + "&montante=" + montante + "&link=" + link + "&login=" + login + "&dep=" + dep + "&alt=" + alt;
	xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function saveSCInBD(id, tipo, datainicio, datafim, cargo, link, nome, login, dep, alt) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    var params = "a=36&id=" + id + "&tipo=" + tipo + "&datainicio=" + datainicio + "&datafim=" + datafim + "&cargo=" + cargo + "&link=" + link + "&nome=" + nome + "&login=" + login + "&dep=" + dep + "&alt=" + alt;
	xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function saveRedesInBD(id, tipo, datainicio, datafim, link, nome, login, dep, alt) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    var params = "a=38&id=" + id + "&tipo=" + tipo + "&datainicio=" + datainicio + "&datafim=" + datafim + "&link=" + link + "&nome=" + nome + "&login=" + login + "&dep=" + dep + "&alt=" + alt;
    xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function saveMissoesInBD(id, datainicio, datafim, motivacao, instituicao, pais, ambtese, login, dep, alt) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

	if (ambtese == 'Sim')
		var params = "a=40&id=" + id + "&datainicio=" + datainicio + "&datafim=" + datafim + "&motivacao=" + motivacao + "&instituicao=" + instituicao + "&pais=" + pais + "&ambtese=1" + "&login=" + login + "&dep=" + dep + "&alt=" + alt;
	else
		var params = "a=40&id=" + id + "&datainicio=" + datainicio + "&datafim=" + datafim + "&motivacao=" + motivacao + "&instituicao=" + instituicao + "&pais=" + pais + "&ambtese=0" + "&login=" + login + "&dep=" + dep + "&alt=" + alt;
	
	xmlhttp.open("POST", "../../functions/saveBD.php", true);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send(params);
    
}

function saveOrientacoesInBD(id, tipo, tipoori, estado, titulo, login, dep, alt) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
	
    var params = "a=42&id=" + id + "&tipo=" + tipo + "&tipoori=" + tipoori + "&estado=" + estado + "&titulo=" + titulo + "&login=" + login + "&dep=" + dep + "&alt=" + alt;

    xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function saveJurisInBD(id, agrargfmup, agrvogalfmup, agrargext, agrvogalext, doutargfmup, doutvogalfmup, doutargext, doutvogalext, mesargfmup, mesvogalfmup, mesargext, mesvogalext, mesintfmup, mesintvogalfmup, mesintargext, mesintvogalext, login, dep, alt) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
	
    var params = "a=44&id=" + id + "&agrargfmup=" + agrargfmup + "&agrvogalfmup=" + agrvogalfmup + "&agrargext=" + agrargext + "&agrvogalext=" + agrvogalext + "&doutargfmup=" + doutargfmup + "&doutvogalfmup=" + doutvogalfmup + "&doutargext=" + doutargext + "&doutvogalext=" + doutvogalext + "&mesargfmup=" + mesargfmup + "&mesvogalfmup=" + mesvogalfmup + "&mesargext=" + mesargext + "&mesvogalext=" + mesvogalext + "&mesintfmup=" + mesintfmup + "&mesintvogalfmup=" + mesintvogalfmup + "&mesintargext=" + mesintargext + "&mesintvogalext=" + mesintvogalext + "&login=" + login + "&dep=" + dep + "&alt=" + alt;

    xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function saveOutActInBD(id, texapoio, livdidaticos, livdivulgacao, app, webp, login, dep, alt) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
	
    var params = "a=46&id=" + id + "&texapoio=" + texapoio + "&livdidaticos=" + livdidaticos + "&livdivulgacao=" + livdivulgacao + "&app=" + app + "&webp=" + webp + "&login=" + login + "&dep=" + dep + "&alt=" + alt;

    xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function saveAcaoDivulgacaoInBD(id, titulo, data, local, npart, login, dep, alt) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var params = "a=48&id=" + id + "&titulo=" + titulo + "&data=" + data + "&local=" + local + "&npart=" + npart + "&login=" + login + "&dep=" + dep + "&alt=" + alt;

    xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function saveProdutoInBD(id, tipo, preco, login, dep, alt) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    var params = "a=50&id=" + id + "&tipo=" + tipo + "&preco=" + preco + "&login=" + login + "&dep=" + dep + "&alt=" + alt;

    xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function saveObsInBD(id, login, texto, tabela, dep) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
	
    var params = "a=54&id=" + id + "&login=" + login + "&texto=" + texto + "&tabela=" + tabela + "&dep=" + dep;
    xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function validateAcao(id) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    var params = "a=55&id=" + id;

    xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function removeAcao(id) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
	
    var params = "a=56&id=" + id;

    xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function deleteAcao(id,tab, login) {
	var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
	
    var params = "a=97&id=" + id + "&tab=" + tab + "&login=" + login;
    xmlhttp.open("POST", "../../functions/saveBD.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

/**
 * EDIÈƒO DE INFORMACAO jQuery!!
 * Autor: Paulo Miguel Rodrigues (prodrigues@med.up.pt, paulomiguelarodrigues@gmail.com)
 */
$(function () {
    var nome = $("#nome-inv"),
        nummec = $("#nummec-inv"),
        datanasc = $("#datanasc-inv"),
        allFields = $([]).add(nome).add(nummec).add(datanasc),
        tips = $(".validateTips");

    var anoinicio,
        anofim,
        curso,
        instituicao,
        grau,
        dist,
        bolsa,
        titulo,
        percentagem,
        unanimidade,
        datainicio,
        datafim,
        nomecient,
        departamento;

    function updateTips(t) {
        tips
            .text(t)
            .addClass("ui-state-highlight");
        setTimeout(function () {
            tips.removeClass("ui-state-highlight", 1500);
        }, 500);
    }

    function checkName(nome) {
        if (!nome.val()) {
            nome.addClass("ui-state-error");
            updateTips("Os campos de escrita não podem estar vazios.");
            return false;
        } else {
            return true;
        }
    }

    function checkNumMec(nummec) {
        if (!nummec.val() || isNaN(nummec.val())) {
            nummec.addClass("ui-state-error");
            updateTips("Este campo é constituido por números apenas.");
            return false;
        } else {
            return true;
        }
    }

    //INVESTIGADORES !!!
    $("#dialog-form-inv").dialog({
        autoOpen: false,
        width: 500,
        length: 500,
        modal: true,
        buttons: {
            Confirmar: function () {

                var bValid = true;

                allFields.removeClass("ui-state-error");

                bValid = bValid && checkName(nome);
                bValid = bValid && checkNumMec(nummec);

                if (bValid) {
                    $("#users tbody #user" + id + " #td" + id + "_nome").replaceWith("<td id='td" + id + "_nome'>" + nome.val() + "</td>");
                    $("#users tbody #user" + id + " #td" + id + "_nummec").replaceWith("<td id='td" + id + "_nummec'>" + nummec.val() + "</td>");
                    $("#users tbody #user" + id + " #td" + id + "_datanasc").replaceWith("<td id='td" + id + "_datanasc'>" + datanasc.val() + "</td>");

                    saveInvestigadorInBD(nome.val(), nummec.val(), datanasc.val(), $('#login').text(), $('#dep').text(), '');
                    $(this).dialog("close");
                }
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        },
        close: function () {
            allFields.val("").removeClass("ui-state-error");
        }
    });

    $('#investigadores img')
        .click(function () {
            id = $("#chave-inv").text();
            $("#nome-inv").val($("#users #td" + id + "_nome").text());
            $("#nummec-inv").val($("#users #td" + id + "_nummec").text());
            $("#datanasc-inv").val($("#users #td" + id + "_datanasc").text());
            $("#dialog-form-inv").dialog("open");
        });

    //HABILITACOES !!!
    /* PEDRO Inicio de operações HABILITACOES */
    
    $("#dialog-form-hab").dialog({
        autoOpen: false,
        width: 600,
        height: 900,
        modal: true,
        buttons: {
            Confirmar: function () {
                var bValid = true;

                allFields.removeClass("ui-state-error");

                bValid = bValid && checkName(curso);
                bValid = bValid && checkName(instituicao);
                bValid = bValid && checkName(titulo);

                /* PEDRO Actualiza a página depois de se fechar o interface de edição */
                if (bValid) {
                    $("#habs #td_ano_ini_hab_" + id).replaceWith("<td id='td_ano_ini_hab_" + id + "'>" + anoinicio.val() + "</td>");
                    $("#habs #td_ano_fim_hab_" + id).replaceWith("<td id='td_ano_fim_hab_" + id + "'>" + anofim.val() + "</td>");
                    $("#habs #td_curso_hab_" + id).replaceWith("<td id='td_curso_hab_" + id + "'>" + curso.val() + "</td>");
                    $("#habs #td_instituicao_hab_" + id).replaceWith("<td id='td_instituicao_hab_" + id + "'>" + instituicao.val() + "</td>");
                    $("#habs #td_grau_hab_" + id).replaceWith("<td id='td_grau_hab_" + id + "'>" + grau.val() + "</td>");
                    $("#habs #td_dist_hab_" + id).replaceWith("<td id='td_dist_hab_" + id + "'>" + dist.val() + "</td>");
                    $("#habs #td_bolsa_hab_" + id).replaceWith("<td id='td_bolsa_hab_" + id + "'>" + bolsa.val() + "</td>");
                    $("#habs #td_tit_hab_" + id).replaceWith("<td id='td_tit_hab_" + id + "'>" + titulo.val() + "</td>");
                    $("#habs #td_perc_hab_" + id).replaceWith("<td id='td_ori_inst_hab_" + id + "'>" + percentagem.val() + "</td>");
                    $("#habs #td_ori_hab_" + id).replaceWith("<td id='td_ori_hab_" + id + "'>" + orientador.val() + "</td>");
                    $("#habs #td_ori_inst_hab_" + id).replaceWith("<td id='td_ori_inst_hab_" + id + "'>" + oinstituicao.val() + "</td>");
                    $("#habs #td_cori_hab_" + id).replaceWith("<td id='td_cori_hab_" + id + "'>" + orientador.val() + "</td>");
                    $("#habs #td_cori_inst_hab_" + id).replaceWith("<td id='td_cori_inst_hab_" + id + "'>" + coinstituicao.val() + "</td>");
                    saveHabilitacaoInBD(id, anoinicio.val(), anofim.val(), curso.val(), instituicao.val(), grau.val(), dist.val(), bolsa.val(), titulo.val(), percentagem.val(),orientador.val(),oinstituicao.val(),corientador.val(),coinstituicao.val(), $('#login').text(), $('#dep').text(), '');
                    $(this).dialog("close");
                }
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        },
        close: function () {
            allFields.val("").removeClass("ui-state-error");
        }
    });

    $('#habilitacoes img')
        .click(function () {
		
            /*   PEDRO Vai buscar o id do registo que se pretende editar     */
        	
        	id = $("#chave-hab").text();
        	
        	/* PEDRO Vai buscar os campos da interface de edição para colocar os dados */
            anoinicio = $("#ano-ini-hab");
            anofim = $("#ano-fim-hab");
            curso = $("#curso-hab");
            instituicao = $("#inst-hab");
            grau = $("#grau-hab");
            dist = $("#dist-hab");
            bolsa = $("#bolsa-hab");
            titulo = $("#tit-hab");
            percentagem = $("#perc-hab");
            orientador = $("#ori-hab");
            oinstituicao = $("#ori_inst-hab");
            corientador = $("#cori-hab");
            coinstituicao = $("#cori_inst-hab");

            /* PEDRO Vai buscar os valores que estão na página */
            
            anoinicio.val($("#habs #td_ano_ini_hab_" + id).text());
            anofim.val($("#habs #td_ano_fim_hab_" + id).text());
            curso.val($("#habs #td_curso_hab_" + id).text());
            instituicao.val($("#habs #td_instituicao_hab_" + id).text());
            grau.val($("#habs #td_grau_hab_" + id).text().trim());
            dist.val($("#habs #td_dist_hab_" + id).text().trim());
            bolsa.val($("#habs #td_bolsa_hab_" + id).text().trim());
            titulo.text($("#habs #td_tit_hab_" + id).text());
            percentagem.val($("#habs #td_perc_hab_" + id).text());
            orientador.val($("#habs #td_ori_hab_" + id).text());
            oinstituicao.val($("#habs #td_ori_inst_hab_" + id).text());
            corientador.val($("#habs #td_cori_hab_" + id).text());
            coinstituicao.val($("#habs #td_cori_inst_hab_" + id).text());

            $("#dialog-form-hab").dialog("open");
        });
    
    
    $("#dialog-insert-habilitacao").dialog({
        autoOpen: false,
        resizable: false,
        width: 550,
        length: 500,
        modal: true,
        buttons: {
            "Confirmar": function () {			
				anoinicio = $("#ano-ini-ins-hab");
				anofim = $("#ano-fim-ins-hab");
				curso = $("#curso-ins-hab");
				instituicao = $("#inst-ins-hab");
				grau = $("#grau-ins-hab");
				dist = $("#dist-ins-hab");
				bolsa = $("#bolsa-ins-hab");
				titulo = $("#tit-ins-hab");
				percentagem = $("#perc-ins-hab");
				
                var bValid = true;

                allFields.removeClass("ui-state-error");

                bValid = bValid && checkName(curso);
                bValid = bValid && checkName(instituicao);
                bValid = bValid && checkName(titulo);

                if (bValid) {
                    $("#habs #td_ano_ini_hab_" + id).replaceWith("<td id='td_ano_ini_hab_" + id + "'>" + anoinicio.val() + "</td>");
                    $("#habs #td_ano_fim_hab_" + id).replaceWith("<td id='td_ano_fim_hab_" + id + "'>" + anofim.val() + "</td>");
                    $("#habs #td_curso_hab_" + id).replaceWith("<td id='td_curso_hab_" + id + "'>" + curso.val() + "</td>");
                    $("#habs #td_instituicao_hab_" + id).replaceWith("<td id='td_instituicao_hab_" + id + "'>" + instituicao.val() + "</td>");
                    $("#habs #td_grau_hab_" + id).replaceWith("<td id='td_grau_hab_" + id + "'>" + grau.val() + "</td>");
                    $("#habs #td_dist_hab_" + id).replaceWith("<td id='td_dist_hab_" + id + "'>" + dist.val() + "</td>");
                    $("#habs #td_bolsa_hab_" + id).replaceWith("<td id='td_bolsa_hab_" + id + "'>" + bolsa.val() + "</td>");
                    $("#habs #td_tit_hab_" + id).replaceWith("<td id='td_tit_hab_" + id + "'>" + titulo.val() + "</td>");
                    $("#habs #td_perc_hab_" + id).replaceWith("<td id='td_perc_hab_" + id + "'>" + percentagem.val() + "</td>");
					
                    insertHabilitacaoInBD(10000, anoinicio.val(), anofim.val(), curso.val(), instituicao.val(), grau.val(), dist.val(), bolsa.val(), titulo.val(), percentagem.val(), $('#login').text(), $('#dep').text());
                    $(this).dialog("close");
                }			
            },
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });
    
    
    /* PEDRO Fim de operações HABILITACOES */

    //AGREGACOES !!!
    $("#dialog-form-agr").dialog({
        autoOpen: false,
        width: 500,
        length: 500,
        modal: true,
        buttons: {
            Confirmar: function () {
                var alt = '';
                if (unanimidade.val() != $("#agreg #td_unanimidade_agr_" + id).text())
                    alt = 'unanimidade';

                $("#agreg #td_unanimidade_agr_" + id).replaceWith("<td id='td_unanimidade_agr_" + id + "'>" + unanimidade.val() + "</td>");

                saveAgregacaoInBD(id, unanimidade.val(), $('#login').text(), $('#dep').text(), alt);
                $(this).dialog("close");
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        },
        close: function () {
            allFields.val("").removeClass("ui-state-error");
        }
    });

    $('#agregacoes img')
        .click(function () {

            id = $("#chave-agr").text();
            unanimidade = $("#unanimidade-agr");

            unanimidade.val($("#agregacoes #td_unanimidade_agr_" + id).text());

            $("#dialog-form-agr").dialog("open");
        });

    //CARACTERIZAÈƒO DOCENTES !!!
    $("#dialog-form-exp-pro-doc").dialog({
        autoOpen: false,
        width: 500,
        length: 500,
        modal: true,
        buttons: {
            Confirmar: function () {
                var alt = '';
                if (tipo.val() != $("#caracDoc #td_exp_pro_doc_cat_" + id).text())
                    alt = 'tipo,';

                if (percentagem.val() != $("#caracDoc #td_exp_pro_doc_per_" + id).text())
                    alt = 'percentagem';

                $("#caracDoc #td_exp_pro_doc_cat_" + id).replaceWith("<td id='td_exp_pro_doc_cat_" + id + "'>" + tipo.val() + "</td>");
                $("#caracDoc #td_exp_pro_doc_per_" + id).replaceWith("<td id='td_exp_pro_doc_per_" + id + "'>" + percentagem.val() + "</td>");                
                login = $('#login').text();
                saveCaracterizacaoDocentesInBD(id, tipo.val(), percentagem.val(), login, $('#dep').text(), alt);
                $(this).dialog("close");
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        },
        close: function () {
            allFields.val("").removeClass("ui-state-error");
        }
    });

    $('#caracterizacaoDocentes img')
        .click(function () {
            id = $("#chave-exp-pro-doc").text();
            tipo = $("#tipo-doc");
            percentagem = $("#perc-doc");

            tipo.val($("#caracterizacaoDocentes #td_exp_pro_doc_cat_" + id).text());
            percentagem.val($("#caracterizacaoDocentes #td_exp_pro_doc_per_" + id).text());

            $("#dialog-form-exp-pro-doc").dialog("open");
        });

    //CARACTERIZAÈƒO PRO-DOC !!!
    $("#dialog-form-exp-pro-pdoc").dialog({
        autoOpen: false,
        width: 500,
        length: 500,
        modal: true,
        buttons: {
            Confirmar: function () {
                var alt = '';
                if (bolsa.val() != $("#caracPos #td_exp_pro_pdoc_bol_" + id).text())
                    alt = 'bolsa,';

                if (percentagem.val() != $("#caracPos #td_exp_pro_pdoc_per_" + id).text())
                    alt = 'percentagem';

                $("#caracPos #td_exp_pro_pdoc_bol_" + id).replaceWith("<td id='td_exp_pro_pdoc_bol_" + id + "'>" + bolsa.val() + "</td>");
                $("#caracPos #td_exp_pro_pdoc_per_" + id).replaceWith("<td id='td_exp_pro_pdoc_per_" + id + "'>" + percentagem.val() + "</td>");
                saveCaracterizacaoPosDocInBD(id, bolsa.val(), percentagem.val(), $('#login').text(), $('#dep').text(), alt);
                $(this).dialog("close");
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        },
        close: function () {
            allFields.val("").removeClass("ui-state-error");
        }
    });

    $('#caracterizacaoPos img')
        .click(function () {
            id = $("#chave-exp-pro-pdoc").text();
            bolsa = $("#bolsa-pdoc");
            percentagem = $("#per-pdoc");

            bolsa.val($("#caracterizacaoPos #td_exp_pro_pdoc_bol_" + id).text());
            percentagem.val($("#caracterizacaoPos #td_exp_pro_pdoc_per_" + id).text());

            $("#dialog-form-exp-pro-pdoc").dialog("open");
        });

    //DIRECAO CURSO !!!
    $("#dialog-form-dircurso").dialog({
        autoOpen: false,
        width: 500,
        length: 500,
        modal: true,
        buttons: {
            Confirmar: function () {
                var alt = '';
                if (datainicio.val() != $("#dircurso #td_dircurso_dataini_" + id).text())
                    alt += 'datainicio,';

                if (datafim.val() != $("#dircurso #td_dircurso_datafim_" + id).text())
                    alt += 'datafim';

                if (curso.val() != $("#dircurso #td_dircurso_curso_" + id).text())
                    alt += 'curso,';

                if (grau.val() != $("#dircurso #td_dircurso_grau_" + id).text())
                    alt += 'grau';

                var bValid = true;

                if (bValid) {
                    $("#dircurso #td_dircurso_dataini_" + id).replaceWith("<td id='td_dircurso_dataini_" + id + "'>" + datainicio.val() + "</td>");
                    $("#dircurso #td_dircurso_datafim_" + id).replaceWith("<td id='td_dircurso_datafim_" + id + "'>" + datafim.val() + "</td>");
                    $("#dircurso #td_dircurso_curso_" + id).replaceWith("<td id='td_dircurso_curso_" + id + "'>" + curso.val() + "</td>");
                    $("#dircurso #td_dircurso_grau_" + id).replaceWith("<td id='td_dircurso_grau_" + id + "'>" + grau.val() + "</td>");

                    saveDirCursoInBD(id, datainicio.val(), datafim.val(), curso.val(), grau.val(), $('#login').text(), $('#dep').text(), alt);
                    $(this).dialog("close");
                }
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        },
        close: function () {
            allFields.val("").removeClass("ui-state-error");
        }
    });

    $('#direcaoCurso img')
        .click(function () {
            id = $("#chave-dircurso").text();
            datainicio = $("#dataini-dircurso");
            datafim = $("#datafim-dircurso");
            curso = $("#curso-dircurso");
            grau = $("#grau-dircurso");

            datainicio.val($("#direcaoCurso #td_dircurso_dataini_" + id).text());
            datafim.val($("#direcaoCurso #td_dircurso_datafim_" + id).text());
            curso.val($("#direcaoCurso #td_dircurso_curso_" + id).text());
            grau.val($("#direcaoCurso #td_dircurso_grau_" + id).text());

            allFields = $([]).add(datainicio).add(datafim).add(curso).add(grau),
                tips = $(".validateTips-dircurso");

            $("#dialog-form-dircurso").dialog("open");
        });

    //COLABORADORES ACTUAIS FMUP !!!
    $("#dialog-form-colIntAct").dialog({
        autoOpen: false,
        width: 600,
        length: 500,
        modal: true,
        buttons: {
            Confirmar: function () {
                var bValid = true;

                allFields.removeClass("ui-state-error");

                bValid = bValid && checkName(nomecient);

                if (bValid) {
                    var alt = '';
                    if (nomecient.val() != $("#colIntAct #td_colIntAct_nomecient_" + id).text())
                        alt += 'nomecient,';

                    if (departamento.val() != $("#colIntAct #td_colIntAct_dep_" + id).text())
                        alt += 'departamento';
                    $("#colIntAct #td_colIntAct_nome_" + id).replaceWith("<td id='td_colIntAct_nome_" + id + "'>" + nome.val() + "</td>");
                    $("#colIntAct #td_colIntAct_nomecient_" + id).replaceWith("<td id='td_colIntAct_nomecient_" + id + "'>" + nomecient.val() + "</td>");
                    $("#colIntAct #td_colIntAct_dep_" + id).replaceWith("<td id='td_colIntAct_dep_" + id + "'>" + departamento.val() + "</td>");

                    saveColIntActInBD(id, nome.val(),nomecient.val(), departamento.val(), $('#login').text(), $('#dep').text(), alt);
                    $(this).dialog("close");
                }
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        },
        close: function () {
            allFields.val("").removeClass("ui-state-error");
        }
    });

    $('#colaboradoresInternos img')
        .click(function () {

            id = $("#chave-colIntAct").text();
            nomecient = $("#nomecient-colIntAct");
            departamento = $("#dep-colIntAct");
            nome = $("#nome-colIntAct");
            
            nome.val($("#colaboradoresInternos #td_colIntAct_nome_" + id).text());
            nomecient.val($("#colaboradoresInternos #td_colIntAct_nomecient_" + id).text());
            departamento.val($("#colaboradoresInternos #td_colIntAct_dep_" + id).text().trim());

            allFields = $([]).add(nomecient).add(departamento),
                tips = $(".validateTips-colIntAct");

            $("#dialog-form-colIntAct").dialog("open");
        });

    //COLABORADORES EXTERNOS !!!
    $("#dialog-form-colExtAct").dialog({
        autoOpen: false,
        width: 600,
        length: 500,
        modal: true,
        buttons: {
            Confirmar: function () {
                var bValid = true;

                allFields.removeClass("ui-state-error");

                bValid = bValid && checkName(nomecient);

                if (bValid) {
                    var alt = '';
                    if (nomecient.val() != $("#colExtAct #td_colExtAct_nomecient_" + id).text())
                        alt += 'nomecient,';

                    if (instituicao.val() != $("#colExtAct #td_colExtAct_instituicao_" + id).text())
                        alt += 'instituicao';

                    if (pais.val() != $("#colExtAct #td_colExtAct_pais_" + id).text())
                        alt += 'pais';

                    $("#colExtAct #td_colExtAct_nomecient_" + id).replaceWith("<td id='td_colExtAct_nomecient_" + id + "'>" + nomecient.val() + "</td>");
                    $("#colExtAct #td_colExtAct_instituicao_" + id).replaceWith("<td id='td_colExtAct_instituicao_" + id + "'>" + instituicao.val() + "</td>");
                    $("#colExtAct #td_colExtAct_pais_" + id).replaceWith("<td id='td_colExtAct_pais_" + id + "'>" + pais.val() + "</td>");

                    saveColExtActInBD(id, nomecient.val(), instituicao.val(), pais.val(), $('#login').text(), $('#dep').text(), alt);
                    $(this).dialog("close");
                }
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        },
        close: function () {
            allFields.val("").removeClass("ui-state-error");
        }
    });

    $('#colaboradoresExternos img')
        .click(function () {

            id = $("#chave-colExtAct").text();
            nomecient = $("#nomecient-colExtAct");
            instituicao = $("#inst-colExtAct");
            pais = $("#pais-colExtAct");

            nomecient.val($("#colaboradoresExternos #td_colExtAct_nomecient_" + id).text());
            instituicao.val($("#colaboradoresExternos #td_colExtAct_instituicao_" + id).text());
            pais.val($("#colaboradoresExternos #td_colExtAct_pais_" + id).text().trim());

            allFields = $([]).add(nomecient).add(departamento),
                tips = $(".validateTips-colExtAct");

            $("#dialog-form-colExtAct").dialog("open");
        });


    //UNIDADES INVESTIGACAO
    $("#dialog-form-uniInv").dialog({
        autoOpen: false,
        width: 850,
        length: 700,
        modal: true,
        buttons: {
            Confirmar: function () {
                var bValid = true;

                allFields.removeClass("ui-state-error");

                bValid = bValid && checkName(unidade);
                bValid = bValid && checkName(avaliacao);

                if (bValid) {
                    var alt = '';
                    if (unidade.val() != $("#uniInv #td_uni_inv_unidade_" + id).text())
                        alt += 'unidade,';

                    if (avaliacao.val() != $("#uniInv #td_uni_inv_ava2007_" + id).text())
                        alt += 'avaliacao';

                    if (percentagem.val() != $("#uniInv #td_uni_inv_perc_" + id).text())
                        alt += 'percentagem';

                    if (responsavel.val() != $("#uniInv #td_uni_inv_resp_" + id).text())
                        alt += 'responsavel';

                    $("#uniInv #td_uni_inv_unidade_" + id).replaceWith("<td id='td_uni_inv_unidade_" + id + "'>" + unidade.val() + "</td>");
                    $("#uniInv #td_uni_inv_ava2007_" + id).replaceWith("<td id='td_uni_inv_ava2007_" + id + "'>" + avaliacao.val() + "</td>");
                    $("#uniInv #td_uni_inv_perc_" + id).replaceWith("<td id='td_uni_inv_perc_" + id + "'>" + percentagem.val() + "</td>");
                    $("#uniInv #td_uni_inv_resp_" + id).replaceWith("<td id='td_uni_inv_resp_" + id + "'>" + responsavel.val() + "</td>");

                    saveUniInvInBD(id, unidade.val(), avaliacao.val(), percentagem.val(), responsavel.val(), $('#login').text(), $('#dep').text(), alt);
                    $(this).dialog("close");
                }
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        },
        close: function () {
            allFields.val("").removeClass("ui-state-error");
        }
    });

    $('#unidadesInvestigacao img')
        .click(function () {
            id = $("#chave-uniInv").text();
            unidade = $("#uni-uniInv");
            avaliacao = $("#ava-uniInv");
            percentagem = $("#perc-uniInv");
            responsavel = $("#resp-uniInv");

            unidade.val($("#unidadesInvestigacao #td_uni_inv_unidade_" + id).text().trim());
            avaliacao.val($("#unidadesInvestigacao #td_uni_inv_ava2007_" + id).text());
            percentagem.val($("#unidadesInvestigacao #td_uni_inv_perc_" + id).text().trim());
            responsavel.val($("#unidadesInvestigacao #td_uni_inv_resp_" + id).text().trim());

            allFields = $([]).add(unidade).add(avaliacao).add(percentagem).add(responsavel),
                tips = $(".validateTips-uniInv");

            $("#dialog-form-uniInv").dialog("open");
        });

    //Publicaè¶¥s Manuais Patentes
    $("#dialog-form-pubManualPatentes").dialog({
        autoOpen: false,
        width: 600,
        length: 500,
        modal: true,
        buttons: {
            Confirmar: function () {

                var bValid = true;

                allFields.removeClass("ui-state-error");

                bValid = bValid && checkName(npatente);
                bValid = bValid && checkName(ipc);
                bValid = bValid && checkName(titulo);
                bValid = bValid && checkName(autores);
				bValid = bValid && checkName(data);
                bValid = bValid && checkName(link);				

                if (bValid) {				
                    $("#pubManualPatentes #td_pubManualPatentes_npatente_" + id).replaceWith("<td id='td_pubManualPatentes_npatente_" + id + "'>" + npatente.val() + "</td>");
                    $("#pubManualPatentes #td_pubManualPatentes_ipc_" + id).replaceWith("<td id='td_pubManualPatentes_ipc_" + id + "'>" + ipc.val() + "</td>");
                    $("#pubManualPatentes #td_pubManualPatentes_titulo_" + id).replaceWith("<td id='td_pubManualPatentes_titulo_" + id + "'>" + titulo.val() + "</td>");
                    $("#pubManualPatentes #td_pubManualPatentes_autores_" + id).replaceWith("<td id='td_pubManualPatentes_autores_" + id + "'>" + autores.val() + "</td>");
                    $("#pubManualPatentes #td_pubManualPatentes_datapatente_" + id).replaceWith("<td id='td_pubManualPatentes_datapatente_" + id + "'>" + data.val() + "</td>");
                    $("#pubManualPatentes #td_pubManualPatentes_link_" + id).replaceWith("<td id='td_pubManualPatentes_link_" + id + "'>" + link.val() + "</td>");
                    $("#pubManualPatentes #td_pubManualPatentes_estado_" + id).replaceWith("<td id='td_pubManualPatentes_estado_" + id + "'>" + estado.val() + "</td>");

                    savePubManPatentesInBD(id, npatente.val(), ipc.val(), titulo.val(), autores.val(), data.val(), link.val(), estado.val(), $('#login').text(), $('#dep').text(), '');
                    	         
					$(this).dialog("close");
                }
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        },
        close: function () {
            allFields.val("").removeClass("ui-state-error");
        }
    });

    $('#pubManualPatentes img')
        .click(function () {
            id = $("#chave-pubManPatentes").text();
            npatente = $("#npatente-pubManualPatentes");
            ipc = $("#ipc-pubManualPatentes");
            titulo = $("#titulo-pubManualPatentes");
            autores = $("#autores-pubManualPatentes");
            data = $("#data-pubManualPatentes");
            link = $("#link-pubManualPatentes");
            estado = $("#estado-pubManualPatentes");

            npatente.val($("#pubManualPatentes #td_pubManualPatentes_npatente_" + id).text());
            ipc.val($("#pubManualPatentes #td_pubManualPatentes_ipc_" + id).text());
            titulo.val($("#pubManualPatentes #td_pubManualPatentes_titulo_" + id).text().trim());
            autores.val($("#pubManualPatentes #td_pubManualPatentes_autores_" + id).text().trim());
            data.val($("#pubManualPatentes #td_pubManualPatentes_datapatente_" + id).text().trim());
            link.val($("#pubManualPatentes #td_pubManualPatentes_link_" + id).text().trim());
            estado.val($("#pubManualPatentes #td_pubManualPatentes_estado_" + id).text().trim());

            allFields = $([]).add(npatente).add(ipc).add(titulo).add(autores).add(data).add(link).add(estado),
			tips = $(".validateTips-pubManualPatentes");

            $("#dialog-form-pubManualPatentes").dialog("open");
        });
		
    //Publicaè¶¥s Manuais Internacional - Artigos
    $("#dialog-form-pubManualInt-Art").dialog({
        autoOpen: false,
        width: 600,
        length: 500,
        modal: true,
        buttons: {
            Confirmar: function () {
                var bValid = true;
				
				alert(titulo);

                allFields.removeClass("ui-state-error");
				
                if (bValid) {
                    $("#pubManualInt-Art #td_outJMAN_issn_" + id).replaceWith("<td id='td_outJMAN_issn_" + id + "'>" + issn.val() + "</td>");
                    $("#pubManualInt-Art #td_outJMAN_nomepub_" + id).replaceWith("<td id='td_outJMAN_nomepub_" + id + "'>" + revista.val() + "</td>");
                    $("#pubManualInt-Art #td_outJMAN_titulo_" + id).replaceWith("<td id='td_outJMAN_titulo_" + id + "'>" + titulo.val() + "</td>");
                    $("#pubManualInt-Art #td_outJMAN_autores_" + id).replaceWith("<td id='td_outJMAN_autores_" + id + "'>" + autores.val() + "</td>");
                    $("#pubManualInt-Art #td_outJMAN_volume_" + id).replaceWith("<td id='td_outJMAN_volume_" + id + "'>" + volume.val() + "</td>");

                    $("#pubManualInt-Art #td_outJMAN_issue_" + id).replaceWith("<td id='td_outJMAN_issue_" + id + "'>" + issue.val() + "</td>");
                    $("#pubManualInt-Art #td_outJMAN_prim_" + id).replaceWith("<td id='td_outJMAN_prim_" + id + "'>" + prim.val() + "</td>");
                    $("#pubManualInt-Art #td_outJMAN_ult_" + id).replaceWith("<td id='td_outJMAN_ult_" + id + "'>" + ult.val() + "</td>");

                    $("#pubManualInt-Art #td_outJMAN_link_" + id).replaceWith("<td id='td_outJMAN_link_" + id + "'>" + link.val() + "</td>");
                    $("#pubManualInt-Art #td_outJMAN_estado_" + id).replaceWith("<td id='td_outJMAN_estado_" + id + "'>" + estado.val() + "</td>");
					
                    savePubManIntArtigoInBD(id, issn.val(), revista.val(), titulo.val(), autores.val(), volume.val(), issue.val(), prim.val(), ult.val(), link.val(), estado.val(), $('#login').text(), $('#dep').text(), '', $('#tabela').text());
                    $(this).dialog("close");
                }
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        },
        close: function () {
            allFields.val("").removeClass("ui-state-error");
        }
    });

    $('#pubManualInt-Art img')
        .click(function () {
            id = $("#chave-pubIntMan-Art").text();

            issn = $("#issn-pubManualInt-Art");
            revista = $("#revista-pubManualInt-Art");
            titulo = $("#titulo-pubManualInt-Art");
            autores = $("#autores-pubManualInt-Art");
            volume = $("#volume-pubManualInt-Art");
            issue = $("#issue-pubManualInt-Art");
            prim = $("#prim-pubManualInt-Art");
            ult = $("#ult-pubManualInt-Art");
            link = $("#link-pubManualInt-Art");
            estado = $("#estado-pubManualInt-Art");

            issn.val($("#pubManualInt-Art #td_outJMAN_issn_" + id).text());
            revista.val($("#pubManualInt-Art #td_outJMAN_nomepub_" + id).text());
            titulo.val($("#pubManualInt-Art #td_outJMAN_titulo_" + id).text().trim());
            autores.val($("#pubManualInt-Art #td_outJMAN_autores_" + id).text().trim());
            volume.val($("#pubManualInt-Art #td_outJMAN_volume_" + id).text().trim());
            issue.val($("#pubManualInt-Art #td_outJMAN_issue_" + id).text().trim());
            prim.val($("#pubManualInt-Art #td_outJMAN_prim_" + id).text().trim());
            ult.val($("#pubManualInt-Art #td_outJMAN_ult_" + id).text().trim());
            link.val($("#pubManualInt-Art #td_outJMAN_link_" + id).text().trim());
            estado.val($("#pubManualInt-Art #td_outJMAN_estado_" + id).text().trim());

            tips = $(".validateTips-pubManualInt-Art");

            $("#dialog-form-pubManualInt-Art").dialog("open");
        });
	
	
	 $('#pubISI_J #pubManualInt-Art #JISI img')
        .click(function () {
        });

    $('#pubManualInt-Rev img')
        .click(function () {

            id = $("#chave-pubIntMan-PRP").text();

            issn = $("#issn-pubManualInt-Art");
            revista = $("#revista-pubManualInt-Art");
            titulo = $("#titulo-pubManualInt-Art");
            autores = $("#autores-pubManualInt-Art");
            volume = $("#volume-pubManualInt-Art");
            issue = $("#issue-pubManualInt-Art");
            prim = $("#prim-pubManualInt-Art");
            ult = $("#ult-pubManualInt-Art");
            link = $("#link-pubManualInt-Art");
            estado = $("#estado-pubManualInt-Art");
			
            issn.val($("#pubManualInt-Rev #td_outPRPMAN_issn_" + id).text());
            revista.val($("#pubManualInt-Rev #td_outPRPMAN_nomepub_" + id).text());
            titulo.val($("#pubManualInt-Rev #td_outPRPMAN_titulo_" + id).text().trim());
            autores.val($("#pubManualInt-Rev #td_outPRPMAN_autores_" + id).text().trim());
            volume.val($("#pubManualInt-Rev #td_outPRPMAN_volume_" + id).text().trim());
            issue.val($("#pubManualInt-Rev #td_outPRPMAN_issue_" + id).text().trim());
            prim.val($("#pubManualInt-Rev #td_outPRPMAN_prim_" + id).text().trim());
            ult.val($("#pubManualInt-Rev #td_outPRPMAN_ult_" + id).text().trim());
            link.val($("#pubManualInt-Rev #td_outPRPMAN_link_" + id).text().trim());
            estado.val($("#pubManualInt-Rev #td_outPRPMAN_estado_" + id).text().trim());

            tips = $(".validateTips-pubManualInt-Art");

            $("#dialog-form-pubManualInt-Art").dialog("open");
        });

    $('#pubManualInt-Sum img')
        .click(function () {

            id = $("#chave-pubIntMan-Sum").text();

            issn = $("#issn-pubManualInt-Sum");
            revista = $("#revista-pubManualInt-Sum");
            titulo = $("#titulo-pubManualInt-Sum");
            autores = $("#autores-pubManualInt-Sum");
            volume = $("#volume-pubManualInt-Sum");
            issue = $("#issue-pubManualInt-Sum");
            prim = $("#prim-pubManualInt-Sum");
            ult = $("#ult-pubManualInt-Sum");
            link = $("#link-pubManualInt-Sum");
            estado = $("#estado-pubManualInt-Sum");

            issn.val($("#pubManualInt-Sum #td_outJAMAN_issn_" + id).text());
            revista.val($("#pubManualInt-Sum #td_outJAMAN_nomepub_" + id).text());
            titulo.val($("#pubManualInt-Sum #td_outJAMAN_titulo_" + id).text().trim());
            autores.val($("#pubManualInt-Sum #td_outJAMAN_autores_" + id).text().trim());
            volume.val($("#pubManualInt-Sum #td_outJAMAN_volume_" + id).text().trim());
            issue.val($("#pubManualInt-Sum #td_outJAMAN_issue_" + id).text().trim());
            prim.val($("#pubManualInt-Sum #td_outJAMAN_prim_" + id).text().trim());
            ult.val($("#pubManualInt-Sum #td_outJAMAN_ult_" + id).text().trim());
            link.val($("#pubManualInt-Sum #td_outJAMAN_link_" + id).text().trim());
            estado.val($("#pubManualInt-Sum #td_outJAMAN_estado_" + id).text().trim());

            tips = $(".validateTips-pubManualInt-Sum");

            $("#dialog-form-pubManualInt-Sum").dialog("open");
        });
		
	$("#dialog-form-pubManualInt-Sum").dialog({
        autoOpen: false,
        width: 600,
        length: 500,
        modal: true,
        buttons: {
            Confirmar: function () {
                var bValid = true;

                allFields.removeClass("ui-state-error");

                bValid = bValid && checkName(titulo);
                bValid = bValid && checkName(autores);

                if (bValid) {
                    $("#pubManualInt-Con #td_outJAMAN_issn_" + id).replaceWith("<td id='td_outJAMAN_issn_" + id + "'>" + issn.val() + "</td>");
                    $("#pubManualInt-Con #td_outJAMAN_nomepub_" + id).replaceWith("<td id='td_outJAMAN_nomepub_" + id + "'>" + revista.val() + "</td>");
                    $("#pubManualInt-Con #td_outJAMAN_titulo_" + id).replaceWith("<td id='td_outJAMAN_titulo_" + id + "'>" + titulo.val() + "</td>");
                    $("#pubManualInt-Con #td_outJAMAN_autores_" + id).replaceWith("<td id='td_outJAMAN_autores_" + id + "'>" + autores.val() + "</td>");
                    $("#pubManualInt-Con #td_outJAMAN_volume_" + id).replaceWith("<td id='td_outJAMAN_volume_" + id + "'>" + volume.val() + "</td>");
                    $("#pubManualInt-Con #td_outJAMAN_issue_" + id).replaceWith("<td id='td_outJAMAN_issue_" + id + "'>" + issue.val() + "</td>");
                    $("#pubManualInt-Con #td_outJAMAN_prim_" + id).replaceWith("<td id='td_outJAMAN_prim_" + id + "'>" + prim.val() + "</td>");
                    $("#pubManualInt-Con #td_outJAMAN_ult_" + id).replaceWith("<td id='td_outJAMAN_ult_" + id + "'>" + ult.val() + "</td>");
                    $("#pubManualInt-Con #td_outJAMAN_link_" + id).replaceWith("<td id='td_outJAMAN_link_" + id + "'>" + link.val() + "</td>");
                    $("#pubManualInt-Con #td_outJAMAN_estado_" + id).replaceWith("<td id='td_outJAMAN_estado_" + id + "'>" + estado.val() + "</td>");

                    savePubManIntArtigoInBD(id, issn.val(), revista.val(), titulo.val(), autores.val(), volume.val(), issue.val(), prim.val(), ult.val(), link.val(), estado.val(), $('#login').text(), $('#dep').text(), '', $('#tabela').text());
					
                    $(this).dialog("close");
                }
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        },
        close: function () {
            allFields.val("").removeClass("ui-state-error");
        }
    });

    //Publicaè¶¥s Manuais Internacional - Livros
    $("#dialog-form-pubManualInt-Liv").dialog({
        autoOpen: false,
        width: 600,
        length: 500,
        modal: true,
        buttons: {
            Confirmar: function () {
                var bValid = true;

                allFields.removeClass("ui-state-error");

                bValid = bValid && checkName(titulo);
                bValid = bValid && checkName(autores);
                bValid = bValid && checkName(editor);
                bValid = bValid && checkName(editora);

                if (bValid) {
                    $("#pubManualInt-Liv #td_outLMAN_isbn_" + id).replaceWith("<td id='td_outLMAN_isbn_" + id + "'>" + isbn.val() + "</td>");
                    $("#pubManualInt-Liv #td_outLMAN_titulo_" + id).replaceWith("<td id='td_outLMAN_titulo_" + id + "'>" + titulo.val() + "</td>");
                    $("#pubManualInt-Liv #td_outLMAN_autores_" + id).replaceWith("<td id='td_outLMAN_autores_" + id + "'>" + autores.val() + "</td>");
                    $("#pubManualInt-Liv #td_outLMAN_editor_" + id).replaceWith("<td id='td_outLMAN_editor_" + id + "'>" + editor.val() + "</td>");
                    $("#pubManualInt-Liv #td_outLMAN_editora_" + id).replaceWith("<td id='td_outLMAN_editora_" + id + "'>" + editora.val() + "</td>");
                    $("#pubManualInt-Liv #td_outLMAN_link_" + id).replaceWith("<td id='td_outLMAN_link_" + id + "'>" + link.val() + "</td>");
                    $("#pubManualInt-Liv #td_outLMAN_estado_" + id).replaceWith("<td id='td_outLMAN_estado_" + id + "'>" + estado.val() + "</td>");

                    savePubManIntLivroInBD(id, isbn.val(), titulo.val(), autores.val(), editor.val(), editora.val(), link.val(), estado.val(), $('#login').text(), $('#dep').text(), '', $('#tabela').text());
                    $(this).dialog("close");
                }
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        },
        close: function () {
            allFields.val("").removeClass("ui-state-error");
        }
    });
	
    $('#pubManualInt-Liv img')
        .click(function () {

            id = $("#chave-pubIntMan-Liv").text();

            isbn = $("#isbn-pubManualInt-Liv");
            titulo = $("#titulo-pubManualInt-Liv");
            autores = $("#autores-pubManualInt-Liv");
            editor = $("#editor-pubManualInt-Liv");
            editora = $("#editora-pubManualInt-Liv");
            link = $("#link-pubManualInt-Liv");
            estado = $("#estado-pubManualInt-Liv");

            isbn.val($("#pubManualInt-Liv #td_outLMAN_isbn_" + id).text());
            titulo.val($("#pubManualInt-Liv #td_outLMAN_titulo_" + id).text().trim());
            autores.val($("#pubManualInt-Liv #td_outLMAN_autores_" + id).text().trim());
            editor.val($("#pubManualInt-Liv #td_outLMAN_editor_" + id).text().trim());
            editora.val($("#pubManualInt-Liv #td_outLMAN_editora_" + id).text().trim());
            link.val($("#pubManualInt-Liv #td_outLMAN_link_" + id).text().trim());
            estado.val($("#pubManualInt-Liv #td_outLMAN_estado_" + id).text().trim());

            tips = $(".validateTips-pubManualInt-Liv");

            $("#dialog-form-pubManualInt-Liv").dialog("open");
        });

    //Publicaè¶¥s Manuais Internacional - Capitulos de Livros
    $("#dialog-form-pubManualInt-CLiv").dialog({
        autoOpen: false,
        width: 600,
        length: 500,
        modal: true,
        buttons: {
            Confirmar: function () {
                var bValid = true;

                allFields.removeClass("ui-state-error");

                bValid = bValid && checkName(nomepub);
                bValid = bValid && checkName(editora);
                bValid = bValid && checkName(editor);
                bValid = bValid && checkName(titulo);
                bValid = bValid && checkName(autores);
                bValid = bValid && checkNumMec(prim);
                bValid = bValid && checkNumMec(ult);

                if (bValid) {
                    $("#pubManualInt-CLiv #td_outCLMAN_isbn_" + id).replaceWith("<td id='td_outCLMAN_isbn_" + id + "'>" + isbn.val() + "</td>");
                    $("#pubManualInt-CLiv #td_outCLMAN_nomepub_" + id).replaceWith("<td id='td_outCLMAN_nomepub_" + id + "'>" + nomepub.val() + "</td>");
                    $("#pubManualInt-CLiv #td_outCLMAN_editora_" + id).replaceWith("<td id='td_outCLMAN_editora_" + id + "'>" + editora.val() + "</td>");
                    $("#pubManualInt-CLiv #td_outCLMAN_editor_" + id).replaceWith("<td id='td_outCLMAN_editor_" + id + "'>" + editor.val() + "</td>");
                    $("#pubManualInt-CLiv #td_outCLMAN_titulo_" + id).replaceWith("<td id='td_outCLMAN_titulo_" + id + "'>" + titulo.val() + "</td>");
                    $("#pubManualInt-CLiv #td_outCLMAN_autores_" + id).replaceWith("<td id='td_outCLMAN_autores_" + id + "'>" + autores.val() + "</td>");
                    $("#pubManualInt-CLiv #td_outCLMAN_prim_" + id).replaceWith("<td id='td_outCLMAN_prim_" + id + "'>" + prim.val() + "</td>");
                    $("#pubManualInt-CLiv #td_outCLMAN_ult_" + id).replaceWith("<td id='td_outCLMAN_ult_" + id + "'>" + ult.val() + "</td>");
                    $("#pubManualInt-CLiv #td_outCLMAN_link_" + id).replaceWith("<td id='td_outCLMAN_link_" + id + "'>" + link.val() + "</td>");
                    $("#pubManualInt-CLiv #td_outCLMAN_estado_" + id).replaceWith("<td id='td_outCLMAN_estado_" + id + "'>" + estado.val() + "</td>");

                    savePubManIntCLivroInBD(id, isbn.val(), nomepub.val(), editora.val(), editor.val(), titulo.val(), autores.val(), prim.val(), ult.val(), link.val(), estado.val(), $('#login').text(), $('#dep').text(), '', $('#tabela').text());
                    $(this).dialog("close");
                }
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        },
        close: function () {
            allFields.val("").removeClass("ui-state-error");
        }
    });
	
    $('#pubManualInt-CLiv img')
        .click(function () {
            id = $("#chave-pubIntMan-CLiv").text();

            isbn = $("#isbn-pubManualInt-CLiv");
            nomepub = $("#nomepub-pubManualInt-CLiv");
            editora = $("#editora-pubManualInt-CLiv");
            editor = $("#editor-pubManualInt-CLiv");
            titulo = $("#titulo-pubManualInt-CLiv");
            autores = $("#autores-pubManualInt-CLiv");
            prim = $("#prim-pubManualInt-CLiv");
            ult = $("#ult-pubManualInt-CLiv");
            link = $("#link-pubManualInt-CLiv");
            estado = $("#estado-pubManualInt-CLiv");

            isbn.val($("#pubManualInt-CLiv #td_outCLMAN_isbn_" + id).text());
            nomepub.val($("#pubManualInt-CLiv #td_outCLMAN_nomepub_" + id).text());
            editora.val($("#pubManualInt-CLiv #td_outCLMAN_editora_" + id).text().trim());
            editor.val($("#pubManualInt-CLiv #td_outCLMAN_editor_" + id).text().trim());
            titulo.val($("#pubManualInt-CLiv #td_outCLMAN_titulo_" + id).text().trim());
            autores.val($("#pubManualInt-CLiv #td_outCLMAN_autores_" + id).text().trim());
            prim.val($("#pubManualInt-CLiv #td_outCLMAN_prim_" + id).text().trim());
            ult.val($("#pubManualInt-CLiv #td_outCLMAN_ult_" + id).text().trim());
            link.val($("#pubManualInt-CLiv #td_outCLMAN_link_" + id).text().trim());
            estado.val($("#pubManualInt-CLiv #td_outCLMAN_estado_" + id).text().trim());

            tips = $(".validateTips-pubManualInt-CLiv");

            $("#dialog-form-pubManualInt-CLiv").dialog("open");
        });
		
    //Publicaè¶¥s Manuais Internacional - Conference Proceedings
    $("#dialog-form-pubManualInt-Con").dialog({
        autoOpen: false,
        width: 600,
        length: 500,
        modal: true,
        buttons: {
            Confirmar: function () {
                var bValid = true;

                allFields.removeClass("ui-state-error");

                bValid = bValid && checkName(nomepub);
                bValid = bValid && checkName(editora);
                bValid = bValid && checkName(titulo);
                bValid = bValid && checkName(autores);

                if (bValid) {
                    $("#pubManualInt-Con #td_outCPMAN_isbn_" + id).replaceWith("<td id='td_outCPMAN_isbn_" + id + "'>" + isbn.val() + "</td>");
                    $("#pubManualInt-Con #td_outCPMAN_nomepub_" + id).replaceWith("<td id='td_outCPMAN_nomepub_" + id + "'>" + nomepub.val() + "</td>");
                    $("#pubManualInt-Con #td_outCPMAN_editor_" + id).replaceWith("<td id='td_outCPMAN_editor_" + id + "'>" + editora.val() + "</td>");
                    $("#pubManualInt-Con #td_outCPMAN_titulo_" + id).replaceWith("<td id='td_outCPMAN_titulo_" + id + "'>" + titulo.val() + "</td>");
                    $("#pubManualInt-Con #td_outCPMAN_autores_" + id).replaceWith("<td id='td_outCPMAN_autores_" + id + "'>" + autores.val() + "</td>");
                    $("#pubManualInt-Con #td_outCPMAN_prim_" + id).replaceWith("<td id='td_outCPMAN_prim_" + id + "'>" + prim.val() + "</td>");
                    $("#pubManualInt-Con #td_outCPMAN_ult_" + id).replaceWith("<td id='td_outCPMAN_ult_" + id + "'>" + ult.val() + "</td>");
                    $("#pubManualInt-Con #td_outCPMAN_link_" + id).replaceWith("<td id='td_outCPMAN_link_" + id + "'>" + link.val() + "</td>");
                    $("#pubManualInt-Con #td_outCPMAN_estado_" + id).replaceWith("<td id='td_outCPMAN_estado_" + id + "'>" + estado.val() + "</td>");

                    savePubManIntCPInBD(id, isbn.val(), nomepub.val(), editora.val(), titulo.val(), autores.val(), prim.val(), ult.val(), link.val(), estado.val(), $('#login').text(), $('#dep').text(), '', $('#tabela').text());

                    $(this).dialog("close");
                }
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        },
        close: function () {
            allFields.val("").removeClass("ui-state-error");
        }
    });

    $('#pubManualInt-Con img')
        .click(function () {
            id = $("#chave-pubIntMan-Con").text();

            isbn = $("#isbn-pubManualInt-Con");
            nomepub = $("#nomepub-pubManualInt-Con");
            editora = $("#editor-pubManualInt-Con");
            titulo = $("#titulo-pubManualInt-Con");
            autores = $("#autores-pubManualInt-Con");
            prim = $("#prim-pubManualInt-Con");
            ult = $("#ult-pubManualInt-Con");
            link = $("#link-pubManualInt-Con");
            estado = $("#estado-pubManualInt-Con");

            isbn.val($("#pubManualInt-Con #td_outCPMAN_isbn_" + id).text());
            nomepub.val($("#pubManualInt-Con #td_outCPMAN_nomepub_" + id).text());
            editora.val($("#pubManualInt-Con #td_outCPMAN_editor_" + id).text().trim());
            titulo.val($("#pubManualInt-Con #td_outCPMAN_titulo_" + id).text().trim());
            autores.val($("#pubManualInt-Con #td_outCPMAN_autores_" + id).text().trim());
            prim.val($("#pubManualInt-Con #td_outCPMAN_prim_" + id).text().trim());
            ult.val($("#pubManualInt-Con #td_outCPMAN_ult_" + id).text().trim());
            link.val($("#pubManualInt-Con #td_outCPMAN_link_" + id).text().trim());
            estado.val($("#pubManualInt-Con #td_outCPMAN_estado_" + id).text().trim());

            tips = $(".validateTips-pubManualInt-Con");

            $("#dialog-form-pubManualInt-Con").dialog("open");
        });
		
		//Publicaè¶¥s Manuais Nacional - Outros Sumâ³©os
    $("#dialog-form-pubManualNac-Oth").dialog({
        autoOpen: false,
        width: 600,
        length: 500,
        modal: true,
        buttons: {
            Confirmar: function () {
                var bValid = true;
                allFields.removeClass("ui-state-error");

                bValid = bValid && checkName(nomepub);
                bValid = bValid && checkName(editora);
                bValid = bValid && checkName(titulo);
                bValid = bValid && checkName(autores);
                bValid = bValid && checkNumMec(prim);
                bValid = bValid && checkNumMec(ult);

                if (bValid) {
                    $("#pubManualInt-Oth #td_outOANMAN_isbn_" + id).replaceWith("<td id='td_outOANMAN_isbn_" + id + "'>" + isbn.val() + "</td>");
                    $("#pubManualInt-Oth #td_outOANNMAN_nomepub_" + id).replaceWith("<td id='td_outOANMAN_nomepub_" + id + "'>" + nomepub.val() + "</td>");
                    $("#pubManualInt-Oth #td_outOANMAN_editor_" + id).replaceWith("<td id='td_outOANMAN_editor_" + id + "'>" + editora.val() + "</td>");
                    $("#pubManualInt-Oth #td_outOANMAN_titulo_" + id).replaceWith("<td id='td_outOANMAN_titulo_" + id + "'>" + titulo.val() + "</td>");
                    $("#pubManualInt-Oth #td_outOANMAN_autores_" + id).replaceWith("<td id='td_outOANMAN_autores_" + id + "'>" + autores.val() + "</td>");
                    $("#pubManualInt-Oth #td_outOANMAN_prim_" + id).replaceWith("<td id='td_outOANMAN_prim_" + id + "'>" + prim.val() + "</td>");
                    $("#pubManualInt-Oth #td_outOANMAN_ult_" + id).replaceWith("<td id='td_outOANMAN_ult_" + id + "'>" + ult.val() + "</td>");
                    $("#pubManualInt-Oth #td_outOANMAN_link_" + id).replaceWith("<td id='td_outOANMAN_link_" + id + "'>" + link.val() + "</td>");
                    $("#pubManualInt-Oth #td_outOANMAN_estado_" + id).replaceWith("<td id='td_outOANMAN_estado_" + id + "'>" + estado.val() + "</td>");

                    savePubManIntCPInBD(id, isbn.val(), nomepub.val(), editora.val(), titulo.val(), autores.val(), prim.val(), ult.val(), link.val(), estado.val(), $('#login').text(), $('#dep').text(), '', $('#tabela').text());
                    $(this).dialog("close");
                }
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        },
        close: function () {
            allFields.val("").removeClass("ui-state-error");
        }
    });
	
    //Publicaè¶¥s Manuais Internacional - Outros Sumâ³©os
    $("#dialog-form-pubManualInt-Oth").dialog({
        autoOpen: false,
        width: 600,
        length: 500,
        modal: true,
        buttons: {
            Confirmar: function () {
                var bValid = true;
                allFields.removeClass("ui-state-error");

                bValid = bValid && checkName(nomepub);
                bValid = bValid && checkName(editora);
                bValid = bValid && checkName(titulo);
                bValid = bValid && checkName(autores);
                bValid = bValid && checkNumMec(prim);
                bValid = bValid && checkNumMec(ult);

                if (bValid) {
                    $("#pubManualInt-Oth #td_outOAMAN_isbn_" + id).replaceWith("<td id='td_outOAMAN_isbn_" + id + "'>" + isbn.val() + "</td>");
                    $("#pubManualInt-Oth #td_outOAMAN_nomepub_" + id).replaceWith("<td id='td_outOAMAN_nomepub_" + id + "'>" + nomepub.val() + "</td>");
                    $("#pubManualInt-Oth #td_outOAMAN_editor_" + id).replaceWith("<td id='td_outOAMAN_editor_" + id + "'>" + editora.val() + "</td>");
                    $("#pubManualInt-Oth #td_outOAMAN_titulo_" + id).replaceWith("<td id='td_outOAMAN_titulo_" + id + "'>" + titulo.val() + "</td>");
                    $("#pubManualInt-Oth #td_outOAMAN_autores_" + id).replaceWith("<td id='td_outOAMAN_autores_" + id + "'>" + autores.val() + "</td>");
                    $("#pubManualInt-Oth #td_outOAMAN_prim_" + id).replaceWith("<td id='td_outOAMAN_prim_" + id + "'>" + prim.val() + "</td>");
                    $("#pubManualInt-Oth #td_outOAMAN_ult_" + id).replaceWith("<td id='td_outOAMAN_ult_" + id + "'>" + ult.val() + "</td>");
                    $("#pubManualInt-Oth #td_outOAMAN_link_" + id).replaceWith("<td id='td_outOAMAN_link_" + id + "'>" + link.val() + "</td>");
                    $("#pubManualInt-Oth #td_outOAMAN_estado_" + id).replaceWith("<td id='td_outOAMAN_estado_" + id + "'>" + estado.val() + "</td>");

                    savePubManIntCPInBD(id, isbn.val(), nomepub.val(), editora.val(), titulo.val(), autores.val(), prim.val(), ult.val(), link.val(), estado.val(), $('#login').text(), $('#dep').text(), '', $('#tabela').text());
                    $(this).dialog("close");
                }
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        },
        close: function () {
            allFields.val("").removeClass("ui-state-error");
        }
    });
	
    $('#pubManualInt-Oth img')
        .click(function () {
            id = $("#chave-pubIntMan-Oth").text();

            isbn = $("#isbn-pubManualInt-Oth");
            nomepub = $("#nomepub-pubManualInt-Oth");
            editora = $("#editor-pubManualInt-Oth");
            titulo = $("#titulo-pubManualInt-Oth");
            autores = $("#autores-pubManualInt-Oth");
            prim = $("#prim-pubManualInt-Oth");
            ult = $("#ult-pubManualInt-Oth");
            link = $("#link-pubManualInt-Oth");
            estado = $("#estado-pubManualInt-Oth");

            isbn.val($("#pubManualInt-Oth #td_outOAMAN_isbn_" + id).text());
            nomepub.val($("#pubManualInt-Oth #td_outOAMAN_nomepub_" + id).text());
            editora.val($("#pubManualInt-Oth #td_outOAMAN_editor_" + id).text().trim());
            titulo.val($("#pubManualInt-Oth #td_outOAMAN_titulo_" + id).text().trim());
            autores.val($("#pubManualInt-Oth #td_outOAMAN_autores_" + id).text().trim());
            prim.val($("#pubManualInt-Oth #td_outOAMAN_prim_" + id).text().trim());
            ult.val($("#pubManualInt-Oth #td_outOAMAN_ult_" + id).text().trim());
            link.val($("#pubManualInt-Oth #td_outOAMAN_link_" + id).text().trim());
            estado.val($("#pubManualInt-Oth #td_outOAMAN_estado_" + id).text().trim());

            tips = $(".validateTips-pubManualInt-Oth");

            $("#dialog-form-pubManualInt-Oth").dialog("open");
        });

    //Publicaè¶¥s Manuais Nacional - Artigos	
    $("#dialog-form-pubManualNac-Art").dialog({
        autoOpen: false,
        width: 600,
        length: 500,
        modal: true,
        buttons: {
            Confirmar: function () {
                var bValid = true;

                allFields.removeClass("ui-state-error");
				
                if (bValid) {
                    $("#pubManualNac-Art #td_outJNMAN_issn_" + id).replaceWith("<td id='td_outJNMAN_issn_" + id + "'>" + issn.val() + "</td>");
                    $("#pubManualNac-Art #td_outJNMAN_nomepub_" + id).replaceWith("<td id='td_outJNMAN_nomepub_" + id + "'>" + revista.val() + "</td>");
                    $("#pubManualNac-Art #td_outJNMAN_titulo_" + id).replaceWith("<td id='td_outJNMAN_titulo_" + id + "'>" + titulo.val() + "</td>");
                    $("#pubManualNac-Art #td_outJNMAN_autores_" + id).replaceWith("<td id='td_outJNMAN_autores_" + id + "'>" + autores.val() + "</td>");
                    $("#pubManualNac-Art #td_outJNMAN_volume_" + id).replaceWith("<td id='td_outJNMAN_volume_" + id + "'>" + volume.val() + "</td>");

                    $("#pubManualNac-Art #td_outJNMAN_issue_" + id).replaceWith("<td id='td_outJNMAN_issue_" + id + "'>" + issue.val() + "</td>");
                    $("#pubManualNac-Art #td_outJNMAN_prim_" + id).replaceWith("<td id='td_outJNMAN_prim_" + id + "'>" + prim.val() + "</td>");
                    $("#pubManualNac-Art #td_outJNMAN_ult_" + id).replaceWith("<td id='td_outJNMAN_ult_" + id + "'>" + ult.val() + "</td>");

                    $("#pubManualNac-Art #td_outJNMAN_link_" + id).replaceWith("<td id='td_outJNMAN_link_" + id + "'>" + link.val() + "</td>");
                    $("#pubManualNac-Art #td_outJNMAN_estado_" + id).replaceWith("<td id='td_outJNMAN_estado_" + id + "'>" + estado.val() + "</td>");

                    savePubManIntArtigoInBD(id, issn.val(), revista.val(), titulo.val(), autores.val(), volume.val(), issue.val(), prim.val(), ult.val(), link.val(), estado.val(), $('#login').text(), $('#dep').text(), '', $('#tabela').text());
                    $(this).dialog("close");
                }
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        },
        close: function () {
            allFields.val("").removeClass("ui-state-error");
        }
    });
	

    $('#pubManualNac-Art img')
        .click(function () {
            id = $("#chave-pubNacMan-Art").text();
            issn = $("#issn-pubManualNac-Art");
            revista = $("#revista-pubManualNac-Art");
            titulo = $("#titulo-pubManualNac-Art");
            autores = $("#autores-pubManualNac-Art");
            volume = $("#volume-pubManualNac-Art");
            issue = $("#issue-pubManualNac-Art");
            prim = $("#prim-pubManualNac-Art");
            ult = $("#ult-pubManualNac-Art");
            link = $("#link-pubManualNac-Art");
            estado = $("#estado-pubManualNac-Art");

            issn.val($("#pubManualNac-Art #td_outJNMAN_issn_" + id).text());
            revista.val($("#pubManualNac-Art #td_outJNMAN_nomepub_" + id).text());
            titulo.val($("#pubManualNac-Art #td_outJNMAN_titulo_" + id).text().trim());
            autores.val($("#pubManualNac-Art #td_outJNMAN_autores_" + id).text().trim());
            volume.val($("#pubManualNac-Art #td_outJNMAN_volume_" + id).text().trim());
            issue.val($("#pubManualNac-Art #td_outJNMAN_issue_" + id).text().trim());
            prim.val($("#pubManualNac-Art #td_outJNMAN_prim_" + id).text().trim());
            ult.val($("#pubManualNac-Art #td_outJNMAN_ult_" + id).text().trim());
            link.val($("#pubManualNac-Art #td_outJNMAN_link_" + id).text().trim());
            estado.val($("#pubManualNac-Art #td_outJNMAN_estado_" + id).text().trim());

            tips = $(".validateTips-pubManualNac-Art");

            $("#dialog-form-pubManualNac-Art").dialog("open");
        });

    //Publicaè¶¥s Manuais Nacional - PEER REVIEW PROCEEDINGS
    $("#dialog-form-pubManualNac-Rev").dialog({
        autoOpen: false,
        width: 600,
        length: 500,
        modal: true,
        buttons: {
            Confirmar: function () {
				var bValid = true;

                allFields.removeClass("ui-state-error");

                bValid = bValid && checkName(issn);
                bValid = bValid && checkName(revista);
                bValid = bValid && checkName(titulo);
                bValid = bValid && checkName(autores);
				
                if (bValid) {
					
					$("#pubManualNac-Rev #td_outPRPNMAN_issn_" + id).replaceWith("<td id='td_outPRPNMAN_issn_" + id + "'>" + issn.val() + "</td>");
					$("#pubManualNac-Rev #td_outPRPNMAN_nomepub_" + id).replaceWith("<td id='td_outPRPNMAN_nomepub_" + id + "'>" + revista.val() + "</td>");
					$("#pubManualNac-Rev #td_outPRPNMAN_titulo_" + id).replaceWith("<td id='td_outPRPNMAN_titulo_" + id + "'>" + titulo.val() + "</td>");
					$("#pubManualNac-Rev #td_outPRPNMAN_autores_" + id).replaceWith("<td id='td_outPRPNMAN_autores_" + id + "'>" + autores.val() + "</td>");
					$("#pubManualNac-Rev #td_outPRPNMAN_volume_" + id).replaceWith("<td id='td_outPRPNMAN_volume_" + id + "'>" + volume.val() + "</td>");

					$("#pubManualNac-Rev #td_outPRPNMAN_issue_" + id).replaceWith("<td id='td_outPRPNMAN_issue_" + id + "'>" + issue.val() + "</td>");
					$("#pubManualNac-Rev #td_outPRPNMAN_prim_" + id).replaceWith("<td id='td_outPRPNMAN_prim_" + id + "'>" + prim.val() + "</td>");
					$("#pubManualNac-Rev #td_outPRPNMAN_ult_" + id).replaceWith("<td id='td_outPRPNMAN_ult_" + id + "'>" + ult.val() + "</td>");

					$("#pubManualNac-Rev #td_outPRPNMAN_link_" + id).replaceWith("<td id='td_outPRPNMAN_link_" + id + "'>" + link.val() + "</td>");
					$("#pubManualNac-Rev #td_outPRPNMAN_estado_" + id).replaceWith("<td id='td_outPRPNMAN_estado_" + id + "'>" + estado.val() + "</td>");

					savePubManIntArtigoInBD(id, issn.val(), revista.val(), titulo.val(), autores.val(), volume.val(), issue.val(), prim.val(), ult.val(), link.val(), estado.val(), $('#login').text(), $('#dep').text(), '', $('#tabela').text());
					$(this).dialog("close");
				}
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        },
        close: function () {
            allFields.val("").removeClass("ui-state-error");
        }
    });

    $('#pubManualNac-Rev img')
        .click(function () {

            id = $("#chave-pubNacMan-Rev").text();

            issn = $("#issn-pubManualNac-Rev");
            revista = $("#revista-pubManualNac-Rev");
            titulo = $("#titulo-pubManualNac-Rev");
            autores = $("#autores-pubManualNac-Rev");
            volume = $("#volume-pubManualNac-Rev");
            issue = $("#issue-pubManualNac-Rev");
            prim = $("#prim-pubManualNac-Rev");
            ult = $("#ult-pubManualNac-Rev");
            link = $("#link-pubManualNac-Rev");
            estado = $("#estado-pubManualNac-Rev");

            issn.val($("#pubManualNac-Rev #td_outPRPNMAN_issn_" + id).text());
            revista.val($("#pubManualNac-Rev #td_outPRPNMAN_nomepub_" + id).text());
            titulo.val($("#pubManualNac-Rev #td_outPRPNMAN_titulo_" + id).text().trim());
            autores.val($("#pubManualNac-Rev #td_outPRPNMAN_autores_" + id).text().trim());
            volume.val($("#pubManualNac-Rev #td_outPRPNMAN_volume_" + id).text().trim());
            issue.val($("#pubManualNac-Rev #td_outPRPNMAN_issue_" + id).text().trim());
            prim.val($("#pubManualNac-Rev #td_outPRPNMAN_prim_" + id).text().trim());
            ult.val($("#pubManualNac-Rev #td_outPRPNMAN_ult_" + id).text().trim());
            link.val($("#pubManualNac-Rev #td_outPRPNMAN_link_" + id).text().trim());
            estado.val($("#pubManualNac-Rev #td_outPRPNMAN_estado_" + id).text().trim());

            tips = $(".validateTips-pubManualNac-Rev");

            $("#dialog-form-pubManualNac-Rev").dialog("open");
        });

    //Publicaè¶¥s Manuais Nacionais - Sumarios
    $("#dialog-form-pubManualNac-Sum").dialog({
        autoOpen: false,
        width: 600,
        length: 500,
        modal: true,
        buttons: {
            Confirmar: function () {
				var bValid = true;

                allFields.removeClass("ui-state-error");
				
                if (bValid) {
					$("#pubManualNac-Sum #td_outJANMAN_issn_" + id).replaceWith("<td id='td_outJANMAN_issn_" + id + "'>" + issn.val() + "</td>");
					$("#pubManualNac-Sum #td_outJANMAN_nomepub_" + id).replaceWith("<td id='td_outJANMAN_nomepub_" + id + "'>" + revista.val() + "</td>");
					$("#pubManualNac-Sum #td_outJANMAN_titulo_" + id).replaceWith("<td id='td_outJANMAN_titulo_" + id + "'>" + titulo.val() + "</td>");
					$("#pubManualNac-Sum #td_outJANMAN_autores_" + id).replaceWith("<td id='td_outJANMAN_autores_" + id + "'>" + autores.val() + "</td>");
					$("#pubManualNac-Sum #td_outJANMAN_volume_" + id).replaceWith("<td id='td_outJANMAN_volume_" + id + "'>" + volume.val() + "</td>");

					$("#pubManualNac-Sum #td_outJANMAN_issue_" + id).replaceWith("<td id='td_outJANMAN_issue_" + id + "'>" + issue.val() + "</td>");
					$("#pubManualNac-Sum #td_outJANMAN_prim_" + id).replaceWith("<td id='td_outJANMAN_prim_" + id + "'>" + prim.val() + "</td>");
					$("#pubManualNac-Sum #td_outJANMAN_ult_" + id).replaceWith("<td id='td_outJANMAN_ult_" + id + "'>" + ult.val() + "</td>");

					$("#pubManualNac-Sum #td_outJANMAN_link_" + id).replaceWith("<td id='td_outJANMAN_link_" + id + "'>" + link.val() + "</td>");
					$("#pubManualNac-Sum #td_outJANMAN_estado_" + id).replaceWith("<td id='td_outJANMAN_estado_" + id + "'>" + estado.val() + "</td>");

					savePubManIntArtigoInBD(id, issn.val(), revista.val(), titulo.val(), autores.val(), volume.val(), issue.val(), prim.val(), ult.val(), link.val(), estado.val(), $('#login').text(), $('#dep').text(), '', $('#tabela').text());
					$(this).dialog("close");
				}
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        },
        close: function () {
            allFields.val("").removeClass("ui-state-error");
        }
    });

    $('#pubManualNac-Sum img')
        .click(function () {

            id = $("#chave-pubNacMan-Sum").text();

            issn = $("#issn-pubManualNac-Sum");
            revista = $("#revista-pubManualNac-Sum");
            titulo = $("#titulo-pubManualNac-Sum");
            autores = $("#autores-pubManualNac-Sum");
            volume = $("#volume-pubManualNac-Sum");
            issue = $("#issue-pubManualNac-Sum");
            prim = $("#prim-pubManualNac-Sum");
            ult = $("#ult-pubManualNac-Sum");
            link = $("#link-pubManualNac-Sum");
            estado = $("#estado-pubManualNac-Sum");

            issn.val($("#pubManualNac-Sum #td_outJANMAN_issn_" + id).text());
            revista.val($("#pubManualNac-Sum #td_outJANMAN_nomepub_" + id).text());
            titulo.val($("#pubManualNac-Sum #td_outJANMAN_titulo_" + id).text().trim());
            autores.val($("#pubManualNac-Sum #td_outJANMAN_autores_" + id).text().trim());
            volume.val($("#pubManualNac-Sum #td_outJANMAN_volume_" + id).text().trim());
            issue.val($("#pubManualNac-Sum #td_outJANMAN_issue_" + id).text().trim());
            prim.val($("#pubManualNac-Sum #td_outJANMAN_prim_" + id).text().trim());
            ult.val($("#pubManualNac-Sum #td_outJANMAN_ult_" + id).text().trim());
            link.val($("#pubManualNac-Sum #td_outJANMAN_link_" + id).text().trim());
            estado.val($("#pubManualNac-Sum #td_outJANMAN_estado_" + id).text().trim());

            tips = $(".validateTips-pubManualNac-Sum");

            $("#dialog-form-pubManualNac-Sum").dialog("open");
        });

    //Publicaè¶¥s Manuais Nacional - Livros
    $("#dialog-form-pubManualNac-Liv").dialog({
        autoOpen: false,
        width: 600,
        length: 500,
        modal: true,
        buttons: {
            Confirmar: function () {
				var bValid = true;

                allFields.removeClass("ui-state-error");

                bValid = bValid && checkName(titulo);
                bValid = bValid && checkName(autores);
                bValid = bValid && checkName(editor);
                bValid = bValid && checkName(editora);

                if (bValid) {
					$("#pubManualNac-Liv #td_outLNMAN_isbn_" + id).replaceWith("<td id='td_outLNMAN_isbn_" + id + "'>" + isbn.val() + "</td>");
					$("#pubManualNac-Liv #td_outLNMAN_titulo_" + id).replaceWith("<td id='td_outLNMAN_titulo_" + id + "'>" + titulo.val() + "</td>");
					$("#pubManualNac-Liv #td_outLNMAN_autores_" + id).replaceWith("<td id='td_outLNMAN_autores_" + id + "'>" + autores.val() + "</td>");
					$("#pubManualNac-Liv #td_outLNMAN_editor_" + id).replaceWith("<td id='td_outLNMAN_editor_" + id + "'>" + editor.val() + "</td>");
					$("#pubManualNac-Liv #td_outLNMAN_editora_" + id).replaceWith("<td id='td_outLNMAN_editora_" + id + "'>" + editora.val() + "</td>");
					$("#pubManualNac-Liv #td_outLNMAN_link_" + id).replaceWith("<td id='td_outLNMAN_link_" + id + "'>" + link.val() + "</td>");
					$("#pubManualNac-Liv #td_outLNMAN_estado_" + id).replaceWith("<td id='td_outLNMAN_estado_" + id + "'>" + estado.val() + "</td>");

					savePubManIntLivroInBD(id, isbn.val(), titulo.val(), autores.val(), editor.val(), editora.val(), link.val(), estado.val(), $('#login').text(), $('#dep').text(), '', $('#tabela').text());
					$(this).dialog("close");
				}
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        },
        close: function () {
            allFields.val("").removeClass("ui-state-error");
        }
    });

    $('#pubManualNac-Liv img')
        .click(function () {

            id = $("#chave-pubNacMan-Liv").text();

            isbn = $("#isbn-pubManualNac-Liv");
            titulo = $("#titulo-pubManualNac-Liv");
            autores = $("#autores-pubManualNac-Liv");
            editor = $("#editor-pubManualNac-Liv");
            editora = $("#editora-pubManualNac-Liv");
            link = $("#link-pubManualNac-Liv");
            estado = $("#estado-pubManualNac-Liv");

            isbn.val($("#pubManualNac-Liv #td_outLNMAN_isbn_" + id).text());
            titulo.val($("#pubManualNac-Liv #td_outLNMAN_titulo_" + id).text().trim());
            autores.val($("#pubManualNac-Liv #td_outLNMAN_autores_" + id).text().trim());
            editor.val($("#pubManualNac-Liv #td_outLNMAN_editor_" + id).text().trim());
            editora.val($("#pubManualNac-Liv #td_outLNMAN_editora_" + id).text().trim());
            link.val($("#pubManualNac-Liv #td_outLNMAN_link_" + id).text().trim());
            estado.val($("#pubManualNac-Liv #td_outLNMAN_estado_" + id).text().trim());

            tips = $(".validateTips-pubManualNac-Liv");

            $("#dialog-form-pubManualNac-Liv").dialog("open");
        });

    //Publicaè¶¥s Manuais Nacional - Capitulos de Livros
    $("#dialog-form-pubManualNac-CLiv").dialog({
        autoOpen: false,
        width: 600,
        length: 500,
        modal: true,
        buttons: {
            Confirmar: function () {
				var bValid = true;

                allFields.removeClass("ui-state-error");

                bValid = bValid && checkName(nomepub);
                bValid = bValid && checkName(editora);
                bValid = bValid && checkName(editor);
                bValid = bValid && checkName(titulo);
                bValid = bValid && checkName(autores);
                bValid = bValid && checkNumMec(prim);
                bValid = bValid && checkNumMec(ult);

                if (bValid) {
					$("#pubManualNac-CLiv #td_outCLNMAN_isbn_" + id).replaceWith("<td id='td_outCLNMAN_isbn_" + id + "'>" + isbn.val() + "</td>");
					$("#pubManualNac-CLiv #td_outCLNMAN_nomepub_" + id).replaceWith("<td id='td_outCLNMAN_nomepub_" + id + "'>" + nomepub.val() + "</td>");
					$("#pubManualNac-CLiv #td_outCLNMAN_editora_" + id).replaceWith("<td id='td_outCLNMAN_editora_" + id + "'>" + editora.val() + "</td>");
					$("#pubManualNac-CLiv #td_outCLNMAN_editor_" + id).replaceWith("<td id='td_outCLNMAN_editor_" + id + "'>" + editor.val() + "</td>");
					$("#pubManualNac-CLiv #td_outCLNMAN_titulo_" + id).replaceWith("<td id='td_outCLNMAN_titulo_" + id + "'>" + titulo.val() + "</td>");
					$("#pubManualNac-CLiv #td_outCLNMAN_autores_" + id).replaceWith("<td id='td_outCLNMAN_autores_" + id + "'>" + autores.val() + "</td>");
					$("#pubManualNac-CLiv #td_outCLNMAN_prim_" + id).replaceWith("<td id='td_outCLNMAN_prim_" + id + "'>" + prim.val() + "</td>");
					$("#pubManualNac-CLiv #td_outCLNMAN_ult_" + id).replaceWith("<td id='td_outCLNMAN_ult_" + id + "'>" + ult.val() + "</td>");
					$("#pubManualNac-CLiv #td_outCLNMAN_link_" + id).replaceWith("<td id='td_outCLNMAN_link_" + id + "'>" + link.val() + "</td>");
					$("#pubManualNac-CLiv #td_outCLNMAN_estado_" + id).replaceWith("<td id='td_outCLNMAN_estado_" + id + "'>" + estado.val() + "</td>");

					savePubManIntCLivroInBD(id, isbn.val(), nomepub.val(), editora.val(), editor.val(), titulo.val(), autores.val(), prim.val(), ult.val(), link.val(), estado.val(), $('#login').text(), $('#dep').text(), '', $('#tabela').text());
					$(this).dialog("close");
				}
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        },
        close: function () {
            allFields.val("").removeClass("ui-state-error");
        }
    });

    $('#pubManualNac-CLiv img')
        .click(function () {
            id = $("#chave-pubNacMan-CLiv").text();
            isbn = $("#isbn-pubManualNac-CLiv");
            nomepub = $("#nomepub-pubManualNac-CLiv");
            editora = $("#editora-pubManualNac-CLiv");
            editor = $("#editor-pubManualNac-CLiv");
            titulo = $("#titulo-pubManualNac-CLiv");
            autores = $("#autores-pubManualNac-CLiv");
            prim = $("#prim-pubManualNac-CLiv");
            ult = $("#ult-pubManualNac-CLiv");
            link = $("#link-pubManualNac-CLiv");
            estado = $("#estado-pubManualNac-CLiv");

            isbn.val($("#pubManualNac-CLiv #td_outCLNMAN_isbn_" + id).text());
            nomepub.val($("#pubManualNac-CLiv #td_outCLNMAN_nomepub_" + id).text());
            editora.val($("#pubManualNac-CLiv #td_outCLNMAN_editora_" + id).text().trim());
            editor.val($("#pubManualNac-CLiv #td_outCLNMAN_editor_" + id).text().trim());
            titulo.val($("#pubManualNac-CLiv #td_outCLNMAN_titulo_" + id).text().trim());
            autores.val($("#pubManualNac-CLiv #td_outCLNMAN_autores_" + id).text().trim());
            prim.val($("#pubManualNac-CLiv #td_outCLNMAN_prim_" + id).text().trim());
            ult.val($("#pubManualNac-CLiv #td_outCLNMAN_ult_" + id).text().trim());
            link.val($("#pubManualNac-CLiv #td_outCLNMAN_link_" + id).text().trim());
            estado.val($("#pubManualNac-CLiv #td_outCLNMAN_estado_" + id).text().trim());

            tips = $(".validateTips-pubManualNac-CLiv");

            $("#dialog-form-pubManualNac-CLiv").dialog("open");
        });

    //Publicaè¶¥s Manuais Nacional - Conference Proceedings	
    $("#dialog-form-pubManualNac-Con").dialog({
        autoOpen: false,
        width: 600,
        length: 500,
        modal: true,
        buttons: {
            Confirmar: function () {	
				var bValid = true;
				allFields.removeClass("ui-state-error");

                bValid = bValid && checkName(nomepub);
                bValid = bValid && checkName(editora);
                bValid = bValid && checkName(titulo);
                bValid = bValid && checkName(autores);

                if (bValid) {
                    $("#pubManualNac-Con #td_outCPNMAN_isbn_" + id).replaceWith("<td id='td_outCPNMAN_isbn_" + id + "'>" + isbn.val() + "</td>");
					$("#pubManualNac-Con #td_outCPNMAN_nomepub_" + id).replaceWith("<td id='td_outCPNMAN_nomepub_" + id + "'>" + nomepub.val() + "</td>");
					$("#pubManualNac-Con #td_outCPNMAN_editor_" + id).replaceWith("<td id='td_outCPNMAN_editor_" + id + "'>" + editora.val() + "</td>");
					$("#pubManualNac-Con #td_outCPNMAN_titulo_" + id).replaceWith("<td id='td_outCPNMAN_titulo_" + id + "'>" + titulo.val() + "</td>");
					$("#pubManualNac-Con #td_outCPNMAN_autores_" + id).replaceWith("<td id='td_outCPNMAN_autores_" + id + "'>" + autores.val() + "</td>");
					$("#pubManualNac-Con #td_outCPNMAN_prim_" + id).replaceWith("<td id='td_outCPNMAN_prim_" + id + "'>" + prim.val() + "</td>");
					$("#pubManualNac-Con #td_outCPNMAN_ult_" + id).replaceWith("<td id='td_outCPNMAN_ult_" + id + "'>" + ult.val() + "</td>");
					$("#pubManualNac-Con #td_outCPNMAN_link_" + id).replaceWith("<td id='td_outCPNMAN_link_" + id + "'>" + link.val() + "</td>");
					$("#pubManualNac-Con #td_outCPNMAN_estado_" + id).replaceWith("<td id='td_outCPNMAN_estado_" + id + "'>" + estado.val() + "</td>");
					savePubManIntCPInBD(id, isbn.val(), nomepub.val(), editora.val(), titulo.val(), autores.val(), prim.val(), ult.val(), link.val(), estado.val(), $('#login').text(), $('#dep').text(), '', $('#tabela').text());
					$(this).dialog("close");
                }
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        },
        close: function () {
            allFields.val("").removeClass("ui-state-error");
        }
    });
	
    $('#pubManualNac-Con img')
        .click(function () {
            id = $("#chave-pubNacMan-Con").text();

            isbn = $("#isbn-pubManualNac-Con");
            nomepub = $("#nomepub-pubManualNac-Con");
            editora = $("#editor-pubManualNac-Con");
            titulo = $("#titulo-pubManualNac-Con");
            autores = $("#autores-pubManualNac-Con");
            prim = $("#prim-pubManualNac-Con");
            ult = $("#ult-pubManualNac-Con");
            link = $("#link-pubManualNac-Con");
            estado = $("#estado-pubManualNac-Con");

            isbn.val($("#pubManualNac-Con #td_outCPNMAN_isbn_" + id).text());
            nomepub.val($("#pubManualNac-Con #td_outCPNMAN_nomepub_" + id).text());
            editora.val($("#pubManualNac-Con #td_outCPNMAN_editor_" + id).text().trim());
            titulo.val($("#pubManualNac-Con #td_outCPNMAN_titulo_" + id).text().trim());
            autores.val($("#pubManualNac-Con #td_outCPNMAN_autores_" + id).text().trim());
            prim.val($("#pubManualNac-Con #td_outCPNMAN_prim_" + id).text().trim());
            ult.val($("#pubManualNac-Con #td_outCPNMAN_ult_" + id).text().trim());
            link.val($("#pubManualNac-Con #td_outCPNMAN_link_" + id).text().trim());
            estado.val($("#pubManualNac-Con #td_outCPNMAN_estado_" + id).text().trim());

            tips = $(".validateTips-pubManualNac-Con");

            $("#dialog-form-pubManualNac-Con").dialog("open");
        });
	
    $('#pubManualNac-Oth img')
        .click(function () {
            id = $("#chave-pubNacMan-Oth").text();

            isbn = $("#isbn-pubManualNac-Oth");
            nomepub = $("#nomepub-pubManualNac-Oth");
            editora = $("#editor-pubManualNac-Oth");
            titulo = $("#titulo-pubManualNac-Oth");
            autores = $("#autores-pubManualNac-Oth");
            prim = $("#prim-pubManualNac-Oth");
            ult = $("#ult-pubManualNac-Oth");
            link = $("#link-pubManualNac-Oth");
            estado = $("#estado-pubManualNac-Oth");

            isbn.val($("#pubManualNac-Oth #td_outOANMAN_isbn_" + id).text());
            nomepub.val($("#pubManualNac-Oth #td_outOANMAN_nomepub_" + id).text());
            editora.val($("#pubManualNac-Oth #td_outOANMAN_editor_" + id).text().trim());
            titulo.val($("#pubManualNac-Oth #td_outOANMAN_titulo_" + id).text().trim());
            autores.val($("#pubManualNac-Oth #td_outOANMAN_autores_" + id).text().trim());
            prim.val($("#pubManualNac-Oth #td_outOANMAN_prim_" + id).text().trim());
            ult.val($("#pubManualNac-Oth #td_outOANMAN_ult_" + id).text().trim());
            link.val($("#pubManualNac-Oth #td_outOANMAN_link_" + id).text().trim());
            estado.val($("#pubManualNac-Oth #td_outOANMAN_estado_" + id).text().trim());

            tips = $(".validateTips-pubManualNac-Oth");

            $("#dialog-form-pubManualNac-Oth").dialog("open");
        });

    //Projectos
    $("#dialog-form-projectos").dialog({
        autoOpen: false,
        width: 600,
        length: 500,
        modal: true,
        buttons: {
            Confirmar: function () {
                $("#projectos #td_projectos_idinv_" + id).css("background-color", "red");
                $("#projectos #td_projectos_tipoentidade_" + id).replaceWith("<td style='overflow:hidden;' id='td_projectos_tipoentidade_" + id + "'>" + tipo.val() + "</td>");
                $("#projectos #td_projectos_entidade_" + id).replaceWith("<td style='overflow:hidden;' id='td_projectos_entidade_" + id + "'>" + entidade.val() + "</td>");
                $("#projectos #td_projectos_acolhimento_" + id).replaceWith("<td style='overflow:hidden;' id='td_projectos_acolhimento_" + id + "'>" + inst.val() + "</td>");
                $("#projectos #td_projectos_montante_" + id).replaceWith("<td style='overflow:hidden;' id='td_projectos_montante_" + id + "'>" + monsol.val() + " </td>");
                $("#projectos #td_projectos_montantea_" + id).replaceWith("<td style='overflow:hidden;' id='td_projectos_montantea_" + id + "'>" + monapr.val() + " </td>");
                $("#projectos #td_projectos_montantefmup_" + id).replaceWith("<td style='overflow:hidden;' id='td_projectos_montantefmup_" + id + "'>" + monfmup.val() + " </td>");
                $("#projectos #td_projectos_invres_" + id).replaceWith("<td style='overflow:hidden;' id='td_projectos_invres_" + id + "'>" + resp.val() + "</td>");
                $("#projectos #td_projectos_codigo_" + id).replaceWith("<td style='overflow:hidden;' id='td_projectos_codigo_" + id + "'>" + codigo.val() + "</td>");
                $("#projectos #td_projectos_titulo_" + id).replaceWith("<td style='overflow:hidden;' id='td_projectos_titulo_" + id + "'>" + titulo.val() + "</td>");
                $("#projectos #td_projectos_dataini_" + id).replaceWith("<td style='overflow:hidden;' id='td_projectos_dataini_" + id + "'>" + dataini.val() + "</td>");
                $("#projectos #td_projectos_datafim_" + id).replaceWith("<td style='overflow:hidden;' id='td_projectos_datafim_" + id + "'>" + datafim.val() + "</td>");
                $("#projectos #td_projectos_link_" + id).replaceWith("<td style='overflow:hidden;' id='td_projectos_link_" + id + "'>" + link.val() + "</td>");
                $("#projectos #td_projectos_estado_" + id).replaceWith("<td style='overflow:hidden;' id='td_projectos_estado_" + id + "'>" + estado.val() + "</td>");

                login = $("#login").text();	
                dep = $("#dep").text();
                saveProjectoInBD(id, tipo.val().trim(), entidade.val().trim(), inst.val().trim(), monsol.val().trim(), monapr.val().trim(), monfmup.val().trim(), resp.val().trim(), codigo.val().trim(), titulo.val().trim(), dataini.val().trim(), datafim.val().trim(), link.val().trim(), estado.val().trim(), login, '', dep);

                $(this).dialog("close");
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        },
        close: function () {
            allFields.val("").removeClass("ui-state-error");
        }
    });

    $('#projectos img').click(function () {
            id = $("#chave-projectos").text();

            tipo = $("#tipo-projectos");
            entidade = $("#entidade-projectos");
            inst = $("#inst-projectos");
            monsol = $("#monsol-projectos");
            monapr = $("#monapr-projectos");
            monfmup = $("#monfmup-projectos");
            resp = $("#resp-projectos");
            codigo = $("#codigo-projectos");
            titulo = $("#titulo-projectos");
            dataini = $("#dataini-projectos");
            datafim = $("#datafim-projectos");
            link = $("#link-projectos");
            estado = $("#estado-projectos");

            tipo.val($("#projectos #td_projectos_tipoentidade_" + id).text());
            entidade.val($("#projectos #td_projectos_entidade_" + id).text());
            inst.val($("#projectos #td_projectos_acolhimento_" + id).text());
            monsol.val($("#projectos #td_projectos_montante_" + id).text().split('')[0]);
            monapr.val($("#projectos #td_projectos_montantea_" + id).text().split('')[0]);
            monfmup.val($("#projectos #td_projectos_montantefmup_" + id).text().split('')[0]);
            resp.val($("#projectos #td_projectos_invres_" + id).text().trim());
            codigo.val($("#projectos #td_projectos_codigo_" + id).text());
            titulo.val($("#projectos #td_projectos_titulo_" + id).text());
            dataini.val($("#projectos #td_projectos_dataini_" + id).text());
            datafim.val($("#projectos #td_projectos_datafim_" + id).text());
            link.val($("#projectos #td_projectos_link_" + id).text());
            estado.val($("#projectos #td_projectos_estado_" + id).text().trim());

            tips = $(".validateTips-projectos");

            $("#dialog-form-projectos").dialog("open");
        });

    //Arbitragens Projectos
    $("#dialog-form-arbprojectos").dialog({
        autoOpen: false,
        width: 300,
        length: 500,
        modal: true,
        buttons: {
            Confirmar: function () {
                $("#arbitragensProjectos #td_arbprojecto_api_" + id).replaceWith("<td id='td_arbprojecto_api_" + id + "'>" + api.val() + "</td>");
                $("#arbitragensProjectos #td_arbprojecto_apn_" + id).replaceWith("<td id='td_arbprojecto_apn_" + id + "'>" + apn.val() + "</td>");

                saveArbProjectoInBD(id, api.val(), apn.val());
                $(this).dialog("close");
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        },
        close: function () {
            allFields.val("").removeClass("ui-state-error");
        }
    });

    $('#arbitragensProjectos img')
        .click(function () {
            id = $("#chave-arb-projecto").text();

            api = $("#api-arbprojectos");
            apn = $("#apn-arbprojectos");

            api.val($("#arbitragensProjectos #td_arbprojecto_api_" + id).text());
            apn.val($("#arbitragensProjectos #td_arbprojecto_apn_" + id).text());

            tips = $(".validateTips-arbprojectos");

            $("#dialog-form-arbprojectos").dialog("open");
        });

    //Conferencias
    $("#dialog-form-conferencias").dialog({
        autoOpen: false,
        width: 600,
        length: 500,
        modal: true,
        buttons: {
            Confirmar: function () {               
				$("#conferencias #td_conferencias_ambito_" + id).replaceWith("<td id='td_conferencias_ambito_" + id + "'>" + ambito.val() + "</td>");
				$("#conferencias #td_conferencias_tipo_" + id).replaceWith("<td id='td_conferencias_tipo_" + id + "'>" + tipo.val() + "</td>");
				$("#conferencias #td_conferencias_dataini_" + id).replaceWith("<td id='td_conferencias_dataini_" + id + "'>" + dataini.val() + "</td>");
				$("#conferencias #td_conferencias_datafim_" + id).replaceWith("<td id='td_conferencias_datafim_" + id + "'>" + datafim.val() + " </td>");
				$("#conferencias #td_conferencias_titulo_" + id).replaceWith("<td id='td_conferencias_titulo_" + id + "'>" + titulo.val() + " </td>");
				$("#conferencias #td_conferencias_local_" + id).replaceWith("<td id='td_conferencias_local_" + id + "'>" + local.val() + " </td>");
				$("#conferencias #td_conferencias_npart_" + id).replaceWith("<td id='td_conferencias_npart_" + id + "'>" + npart.val() + "</td>");
				$("#conferencias #td_conferencias_link_" + id).replaceWith("<td id='td_conferencias_link_" + id + "'>" + link.val() + "</td>");

				saveConferenciaInBD(id, ambito.val(), tipo.val(), dataini.val(), datafim.val(),
				titulo.val(), local.val(), npart.val(), link.val(), $('#login').text(), $('#dep').text(), '');
				
				$(this).dialog("close");
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        }
    });

    $('#conferencias img').click(function () {
		id = $("#chave-conf").text();

		ambito = $("#ambito-conferencias");
		tipo = $("#tipo-conferencias");
		dataini = $("#dataini-conferencias");
		datafim = $("#datafim-conferencias");
		titulo = $("#titulo-conferencias");
		local = $("#local-conferencias");
		npart = $("#npart-conferencias");
		link = $("#link-conferencias");

		ambito.val($("#conferencias #td_conferencias_ambito_" + id).text().trim());
		tipo.val($("#conferencias #td_conferencias_tipo_" + id).text().trim());
		dataini.val($("#conferencias #td_conferencias_dataini_" + id).text());
		datafim.val($("#conferencias #td_conferencias_datafim_" + id).text());
		titulo.val($("#conferencias #td_conferencias_titulo_" + id).text());
		local.val($("#conferencias #td_conferencias_local_" + id).text());
		npart.val($("#conferencias #td_conferencias_npart_" + id).text());
		link.val($("#conferencias #td_conferencias_link_" + id).text());

		tips = $(".validateTips-conferencias");

		$("#dialog-form-conferencias").dialog("open");
	});

    //Apresentaè¶¥s
    $("#dialog-form-apresentacoes").dialog({
        autoOpen: false,
        width: 400,
        length: 500,
        modal: true,
        buttons: {
            Confirmar: function () {
                var bValid = true;

                allFields.removeClass("ui-state-error");

                bValid = bValid && checkName(iconfconv);
                bValid = bValid && checkName(iconfpart);
                bValid = bValid && checkName(isem);
                bValid = bValid && checkName(nconfconv);
                bValid = bValid && checkName(nconfpart);
                bValid = bValid && checkName(nsem);

                if (bValid) {
                    $("#apresentacoes #td_apresentacoes_iconfconv_" + id).replaceWith("<td id='td_apresentacoes_iconfconv_" + id + "'>" + iconfconv.val() + "</td>");
                    $("#apresentacoes #td_apresentacoes_iconfpart_" + id).replaceWith("<td id='td_apresentacoes_iconfpart_" + id + "'>" + iconfpart.val() + "</td>");
                    $("#apresentacoes #td_apresentacoes_isem_" + id).replaceWith("<td id='td_apresentacoes_isem_" + id + "'>" + isem.val() + "</td>");
                    $("#apresentacoes #td_apresentacoes_nconfconv_" + id).replaceWith("<td id='td_apresentacoes_nconfconv_" + id + "'>" + nconfconv.val() + " </td>");
                    $("#apresentacoes #td_apresentacoes_nconfpart_" + id).replaceWith("<td id='td_apresentacoes_nconfpart_" + id + "'>" + nconfpart.val() + " </td>");
                    $("#apresentacoes #td_apresentacoes_nsem_" + id).replaceWith("<td id='td_apresentacoes_nsem_" + id + "'>" + nsem.val() + " </td>");

                    saveApresentacaoInBD(id, iconfconv.val(), iconfpart.val(), isem.val(), nconfconv.val(), nconfpart.val(), nsem.val(), $('#login').text(), $('#dep').text(), '');
                    $(this).dialog("close");
                }
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        },
        close: function () {
            allFields.val("").removeClass("ui-state-error");
        }
    });

    $('#apresentacoes img')
        .click(function () {
            id = $("#chave-apresentacoes").text();

            iconfconv = $("#iconfconv-apresentacoes");
            iconfpart = $("#iconfpart-apresentacoes");
            isem = $("#isem-apresentacoes");
            nconfconv = $("#nconfconv-apresentacoes");
            nconfpart = $("#nconfpart-apresentacoes");
            nsem = $("#nsem-apresentacoes");

            iconfconv.val($("#apresentacoes #td_apresentacoes_iconfconv_" + id).text().trim());
            iconfpart.val($("#apresentacoes #td_apresentacoes_iconfpart_" + id).text().trim());
            isem.val($("#apresentacoes #td_apresentacoes_isem_" + id).text());
            nconfconv.val($("#apresentacoes #td_apresentacoes_nconfconv_" + id).text());
            nconfpart.val($("#apresentacoes #td_apresentacoes_nconfpart_" + id).text());
            nsem.val($("#apresentacoes #td_apresentacoes_nsem_" + id).text());

            tips = $(".validateTips-apresentacoes");

            $("#dialog-form-apresentacoes").dialog("open");
        });


    //Aprbitragens Revistas
    $("#dialog-form-arbrevistas").dialog({
        autoOpen: false,
        width: 550,
        length: 500,
        modal: true,
        buttons: {
            Confirmar: function () {
                var bValid = true;

                allFields.removeClass("ui-state-error");

                if (bValid) {
                    $("#arbitragensRevistas #td_arbrevistas_tipo_" + id).replaceWith("<td id='td_arbrevistas_tipo_" + id + "'>" + tipo.val() + "</td>");
                    $("#arbitragensRevistas #td_arbrevistas_issn_" + id).replaceWith("<td id='td_arbrevistas_issn_" + id + "'>" + issn.val() + "</td>");
                    $("#arbitragensRevistas #td_arbrevistas_titulo_" + id).replaceWith("<td id='td_arbrevistas_titulo_" + id + "'>" + titulo.val() + "</td>");
                    $("#arbitragensRevistas #td_arbrevistas_link_" + id).replaceWith("<td id='td_arbrevistas_link_" + id + "'>" + link.val() + " </td>");

                    saveArbRevistaInBD(id, tipo.val(), issn.val(), titulo.val(), link.val(), $('#login').text(), $('#dep').text(), '');
                    $(this).dialog("close");
                }
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        },
        close: function () {
            allFields.val("").removeClass("ui-state-error");
        }
    });

    $('#arbitragensRevistas img')
        .click(function () {
            id = $("#chave-arb-revistas").text();

            tipo = $("#tipo-arbrevistas");
            issn = $("#issn-arbrevistas");
            titulo = $("#titulo-arbrevistas");
            link = $("#link-arbrevistas");

            tipo.val($("#arbitragensRevistas #td_arbrevistas_tipo_" + id).text().trim());
            issn.val($("#arbitragensRevistas #td_arbrevistas_issn_" + id).text().trim());
            titulo.val($("#arbitragensRevistas #td_arbrevistas_titulo_" + id).text());
            link.val($("#arbitragensRevistas #td_arbrevistas_link_" + id).text());

            tips = $(".validateTips-arbrevistas");

            $("#dialog-form-arbrevistas").dialog("open");
        });

    //Premios
    $("#dialog-form-premios").dialog({
        autoOpen: false,
        width: 550,
        length: 500,
        modal: true,
        buttons: {
            Confirmar: function () {

                var bValid = true;

                allFields.removeClass("ui-state-error");

                bValid = bValid && checkName(tipo);
                bValid = bValid && checkName(nome);
                bValid = bValid && checkName(contexto);
                bValid = bValid && checkName(montante);
                bValid = bValid && checkName(link);

                if (bValid) {
                    $("#premios #td_premios_tipo_" + id).replaceWith("<td id='td_premios_tipo_" + id + "'>" + tipo.val() + "</td>");
                    $("#premios #td_premios_nome_" + id).replaceWith("<td id='td_premios_nome_" + id + "'>" + nome.val() + "</td>");
                    $("#premios #td_premios_contexto_" + id).replaceWith("<td id='td_premios_contexto_" + id + "'>" + contexto.val() + "</td>");
                    $("#premios #td_premios_montante_" + id).replaceWith("<td id='td_premios_montante_" + id + "'>" + montante.val() + " </td>");
                    $("#premios #td_premios_link_" + id).replaceWith("<td id='td_premios_link_" + id + "'>" + link.val() + " </td>");

                    savePremioInBD(id, tipo.val(), nome.val(), contexto.val(), montante.val(), link.val(), $('#login').text(), $('#dep').text(), '');
                    $(this).dialog("close");
                }
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        },
        close: function () {
            allFields.val("").removeClass("ui-state-error");
        }
    });

    $('#premios img')
        .click(function () {
            id = $("#chave-premios").text();

            tipo = $("#tipo-premios");
            nome = $("#nome-premios");
            contexto = $("#contexto-premios");
            montante = $("#montante-premios");
            link = $("#link-premios");

            tipo.val($("#premios #td_premios_tipo_" + id).text().trim());
            nome.val($("#premios #td_premios_nome_" + id).text().trim());
            contexto.val($("#premios #td_premios_contexto_" + id).text());
            montante.val($("#premios #td_premios_montante_" + id).text().split('')[0]);
            link.val($("#premios #td_premios_link_" + id).text());

            tips = $(".validateTips-premios");

            $("#dialog-form-premios").dialog("open");
        });

    //Sociedades Cientificas
    $("#dialog-form-sc").dialog({
        autoOpen: false,
        width: 550,
        length: 500,
        modal: true,
        buttons: {
            Confirmar: function () {

                var bValid = true;

                allFields.removeClass("ui-state-error");

                bValid = bValid && checkName(tipo);
                bValid = bValid && checkName(nome);
                bValid = bValid && checkName(datainicio);
                bValid = bValid && checkName(datafim);
                bValid = bValid && checkName(cargo);
                bValid = bValid && checkName(link);

                if (bValid) {
                    $("#sc #td_sc_tipo_" + id).replaceWith("<td id='td_sc_tipo_" + id + "'>" + tipo.val() + "</td>");
                    $("#sc #td_sc_datainicio_" + id).replaceWith("<td id='td_sc_datainicio_" + id + "'>" + datainicio.val() + "</td>");
                    $("#sc #td_sc_datafim_" + id).replaceWith("<td id='td_sc_datafim_" + id + "'>" + datafim.val() + "</td>");
                    $("#sc #td_sc_cargo_" + id).replaceWith("<td id='td_sc_cargo_" + id + "'>" + cargo.val() + "</td>");
                    $("#sc #td_sc_link_" + id).replaceWith("<td id='td_sc_link_" + id + "'>" + link.val() + " </td>");
                    $("#sc #td_sc_nome_" + id).replaceWith("<td id='td_sc_nome_" + id + "'>" + nome.val() + " </td>");

                    saveSCInBD(id, tipo.val(), datainicio.val(), datafim.val(), cargo.val(), link.val(), nome.val(), $('#login').text(), $('#dep').text(), '');
                    $(this).dialog("close");
                }
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        },
        close: function () {
            allFields.val("").removeClass("ui-state-error");
        }
    });

    $('#sc img')
        .click(function () {
            id = $("#chave-sc").text();

            tipo = $("#tipo-sc");
            datainicio = $("#datainicio-sc");
            datafim = $("#datafim-sc");
            cargo = $("#cargo-sc");
            link = $("#link-sc");
            nome = $("#nome-sc");

            tipo.val($("#sc #td_sc_tipo_" + id).text().trim());
            datainicio.val($("#sc #td_sc_datainicio_" + id).text().trim());
            datafim.val($("#sc #td_sc_datafim_" + id).text());
            cargo.val($("#sc #td_sc_cargo_" + id).text());
            link.val($("#sc #td_sc_link_" + id).text());
            nome.val($("#sc #td_sc_nome_" + id).text());

            tips = $(".validateTips-sc");

            $("#dialog-form-sc").dialog("open");
        });

    //Redes
    $("#dialog-form-redes").dialog({
        autoOpen: false,
        width: 550,
        length: 500,
        modal: true,
        buttons: {
            Confirmar: function () {
                var bValid = true;

                allFields.removeClass("ui-state-error");

                bValid = bValid && checkName(tipo);
                bValid = bValid && checkName(nome);
                bValid = bValid && checkName(datainicio);
                bValid = bValid && checkName(datafim);
                bValid = bValid && checkName(link);

                if (bValid) {
                    $("#redes #td_redes_tipo_" + id).replaceWith("<td id='td_redes_tipo_" + id + "'>" + tipo.val() + "</td>");
                    $("#redes #td_redes_datainicio_" + id).replaceWith("<td id='td_redes_datainicio_" + id + "'>" + datainicio.val() + "</td>");
                    $("#redes #td_redes_datafim_" + id).replaceWith("<td id='td_redes_datafim_" + id + "'>" + datafim.val() + "</td>");
                    $("#redes #td_redes_link_" + id).replaceWith("<td id='td_redes_link_" + id + "'>" + link.val() + " </td>");
                    $("#redes #td_redes_nome_" + id).replaceWith("<td id='td_redes_nome_" + id + "'>" + nome.val() + " </td>");

                    saveRedesInBD(id, tipo.val(), datainicio.val(), datafim.val(), link.val(), nome.val(), $('#login').text(), $('#dep').text(), '');
                    $(this).dialog("close");
                }
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        },
        close: function () {
            allFields.val("").removeClass("ui-state-error");
        }
    });

    $('#redes img')
        .click(function () {
            id = $("#chave-redes").text();

            nome = $("#nome-redes");
            tipo = $("#tipo-redes");
            datainicio = $("#datainicio-redes");
            datafim = $("#datafim-redes");
            link = $("#link-redes");

            tipo.val($("#redes #td_redes_tipo_" + id).text().trim());
            datainicio.val($("#redes #td_redes_datainicio_" + id).text().trim());
            datafim.val($("#redes #td_redes_datafim_" + id).text());
            link.val($("#redes #td_redes_link_" + id).text());
            nome.val($("#redes #td_redes_nome_" + id).text());

            tips = $(".validateTips-redes");

            $("#dialog-form-redes").dialog("open");
        });

    //Missñ¥³ 
    $("#dialog-form-missoes").dialog({
        autoOpen: false,
        width: 550,
        length: 500,
        modal: true,
        buttons: {
            Confirmar: function () {

                var bValid = true;

                allFields.removeClass("ui-state-error");

                bValid = bValid && checkName(datainicio);
                bValid = bValid && checkName(datafim);
                bValid = bValid && checkName(motivacao);
                bValid = bValid && checkName(instituicao);
                bValid = bValid && checkName(pais);
                bValid = bValid && checkName(ambtese);

                if (bValid) {
                    $("#missoes #td_missoes_dataini_" + id).replaceWith("<td id='td_missoes_dataini_" + id + "'>" + datainicio.val() + "</td>");
                    $("#missoes #td_missoes_datafim_" + id).replaceWith("<td id='td_missoes_datafim_" + id + "'>" + datafim.val() + "</td>");
                    $("#missoes #td_missoes_motivacao_" + id).replaceWith("<td id='td_missoes_motivacao_" + id + "'>" + motivacao.val() + "</td>");
                    $("#missoes #td_missoes_instituicao_" + id).replaceWith("<td id='td_missoes_instituicao_" + id + "'>" + instituicao.val() + " </td>");
                    $("#missoes #td_missoes_pais_" + id).replaceWith("<td id='td_missoes_pais_" + id + "'>" + pais.val() + " </td>");
                    $("#missoes #td_missoes_ambtese_" + id).replaceWith("<td id='td_missoes_ambtese_" + id + "'>" + ambtese.val() + " </td>");

                    saveMissoesInBD(id, datainicio.val(), datafim.val(), motivacao.val(), instituicao.val(), pais.val(), ambtese.val(), $('#login').text(), $('#dep').text(), '');
                    $(this).dialog("close");
                }
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        },
        close: function () {
            allFields.val("").removeClass("ui-state-error");
        }
    });

    $('#missoes img')
        .click(function () {
            id = $("#chave-missoes").text();

            datainicio = $("#datainicio-missoes");
            datafim = $("#datafim-missoes");
            motivacao = $("#motivacao-missoes");
            instituicao = $("#instituicao-missoes");
            pais = $("#pais-missoes");
            ambtese = $("#ambtese-missoes");

            datainicio.val($("#missoes #td_missoes_dataini_" + id).text().trim());
            datafim.val($("#missoes #td_missoes_datafim_" + id).text());
            motivacao.val($("#missoes #td_missoes_motivacao_" + id).text().trim());
            instituicao.val($("#missoes #td_missoes_instituicao_" + id).text());
            pais.val($("#missoes #td_missoes_pais_" + id).text().trim());
            ambtese.val($("#missoes #td_missoes_ambtese_" + id).text().trim());

            tips = $(".validateTips-missoes");

            $("#dialog-form-missoes").dialog("open");
        });

    //Orientaè¶¥s 
    $("#dialog-form-orientacoes").dialog({
        autoOpen: false,
        width: 550,
        length: 500,
        modal: true,
        buttons: {
            Confirmar: function () {
				var bValid = true;

                allFields.removeClass("ui-state-error");

                bValid = bValid && checkName(tipo);
                bValid = bValid && checkName(tipoori);
                bValid = bValid && checkName(estado);
                bValid = bValid && checkName(titulo);

                if (bValid) {					
					$("#orientacoes #td_orientacoes_tipo_" + id).replaceWith("<td id='td_orientacoes_tipo_" + id + "'>" + tipo.val() + "</td>");
					$("#orientacoes #td_orientacoes_tipoori_" + id).replaceWith("<td id='td_orientacoes_tipoori_" + id + "'>" + tipoori.val() + "</td>");
					$("#orientacoes #td_orientacoes_estado_" + id).replaceWith("<td id='td_orientacoes_estado_" + id + "'>" + estado.val() + "</td>");
					$("#orientacoes #td_morientacoes_titulo_" + id).replaceWith("<td id='td_orientacoes_titulo_" + id + "'>" + titulo.val() + " </td>");

					saveOrientacoesInBD(id, tipo.val(), tipoori.val(), estado.val(), titulo.val(), $('#login').text(), $('#dep').text(), '');
					$(this).dialog("close");
				}
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        },
        close: function () {
            allFields.val("").removeClass("ui-state-error");
        }
    });

    $('#orientacoes img')
        .click(function () {
            id = $("#chave-orientacoes").text();

            tipo = $("#tipo-orientacoes");
            tipoori = $("#tipoori-orientacoes");
            estado = $("#estado-orientacoes");
            titulo = $("#titulo-orientacoes");

            tipo.val($("#orientacoes #td_orientacoes_tipo_" + id).text().trim());
            tipoori.val($("#orientacoes #td_orientacoes_tipoori_" + id).text().trim());
            estado.val($("#orientacoes #td_orientacoes_estado_" + id).text().trim());
            titulo.val($("#orientacoes #td_orientacoes_titulo_" + id).text());

            tips = $(".validateTips-orientacoes");

            $("#dialog-form-orientacoes").dialog("open");
        });

    //Juris 
    $("#dialog-form-juris").dialog({
        autoOpen: false,
        width: 550,
        length: 500,
        modal: true,
        buttons: {
            Confirmar: function () {
			
				var bValid = true;

                allFields.removeClass("ui-state-error");

                bValid = bValid && checkNumMec(agrargfmup);
                bValid = bValid && checkNumMec(agrvogalfmup);
                bValid = bValid && checkNumMec(agrargext);
                bValid = bValid && checkNumMec(agrvogalext);				
				
                bValid = bValid && checkNumMec(doutargfmup);
                bValid = bValid && checkNumMec(doutvogalfmup);
                bValid = bValid && checkNumMec(doutargext);
                bValid = bValid && checkNumMec(doutvogalext);				
				
                bValid = bValid && checkNumMec(mesargfmup);
                bValid = bValid && checkNumMec(mesvogalfmup);
                bValid = bValid && checkNumMec(mesargext);
                bValid = bValid && checkNumMec(mesvogalext);
				
				bValid = bValid && checkNumMec(mesintfmup);
                bValid = bValid && checkNumMec(mesintvogalfmup);
                bValid = bValid && checkNumMec(mesintargext);
                bValid = bValid && checkNumMec(mesintvogalext);

                if (bValid) {
                    $("#juris #td_juris_agrargfmup_" + id).replaceWith("<td id='td_juris_agrargfmup_" + id + "'>" + agrargfmup.val() + "</td>");
					$("#juris #td_juris_agrvogalfmup_" + id).replaceWith("<td id='td_juris_agrvogalfmup_" + id + "'>" + agrvogalfmup.val() + "</td>");
					$("#juris #td_juris_agrargext_" + id).replaceWith("<td id='td_juris_agrargext_" + id + "'>" + agrargext.val() + "</td>");
					$("#juris #td_juris_agrvogalext_" + id).replaceWith("<td id='td_juris_agrvogalext_" + id + "'>" + agrvogalext.val() + " </td>");

					$("#juris #td_juris_doutargfmup_" + id).replaceWith("<td id='td_juris_doutargfmup_" + id + "'>" + doutargfmup.val() + "</td>");
					$("#juris #td_juris_doutvogalfmup_" + id).replaceWith("<td id='td_juris_doutvogalfmup_" + id + "'>" + doutvogalfmup.val() + "</td>");
					$("#juris #td_juris_doutargext_" + id).replaceWith("<td id='td_juris_doutargext_" + id + "'>" + doutargext.val() + "</td>");
					$("#juris #td_juris_doutvogalext_" + id).replaceWith("<td id='td_juris_doutvogalext_" + id + "'>" + doutvogalext.val() + " </td>");

					$("#juris #td_juris_mesargfmup_" + id).replaceWith("<td id='td_juris_mesargfmup_" + id + "'>" + mesargfmup.val() + "</td>");
					$("#juris #td_juris_mesvogalfmup_" + id).replaceWith("<td id='td_juris_mesvogalfmup_" + id + "'>" + mesvogalfmup.val() + "</td>");
					$("#juris #td_juris_mesargext_" + id).replaceWith("<td id='td_juris_mesargext_" + id + "'>" + mesargext.val() + "</td>");
					$("#juris #td_juris_mesvogalext_" + id).replaceWith("<td id='td_juris_mesvogalext_" + id + "'>" + mesvogalext.val() + " </td>");

					$("#juris #td_juris_mesintfmup_" + id).replaceWith("<td id='td_juris_mesintfmup_" + id + "'>" + mesintfmup.val() + "</td>");
					$("#juris #td_juris_mesintvogalfmup_" + id).replaceWith("<td id='td_juris_mesintvogalfmup_" + id + "'>" + mesintvogalfmup.val() + "</td>");
					$("#juris #td_juris_mesintargext_" + id).replaceWith("<td id='td_juris_mesintargext_" + id + "'>" + mesintargext.val() + "</td>");
					$("#juris #td_juris_mesintvogalext_" + id).replaceWith("<td id='td_juris_mesintvogalext_" + id + "'>" + mesintvogalext.val() + " </td>");

					saveJurisInBD(id, agrargfmup.val(), agrvogalfmup.val(), agrargext.val(), agrvogalext.val(), doutargfmup.val(), doutvogalfmup.val(), doutargext.val(), doutvogalext.val(), mesargfmup.val(), mesvogalfmup.val(), mesargext.val(), mesvogalext.val(), mesintfmup.val(), mesintvogalfmup.val(), mesintargext.val(), mesintvogalext.val() , $('#login').text(), $('#dep').text(), '');

					$(this).dialog("close");
				}
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        },
        close: function () {
            allFields.val("").removeClass("ui-state-error");
        }
    });

    $('#juris img')
        .click(function () {
            id = $("#chave-juris").text();

            agrargfmup = $("#agrargfmup-juris");
            agrvogalfmup = $("#agrvogalfmup-juris");
            agrargext = $("#agrargext-juris");
            agrvogalext = $("#agrvogalext-juris");
            doutargfmup = $("#doutargfmup-juris");
            doutvogalfmup = $("#doutvogalfmup-juris");
            doutargext = $("#doutargext-juris");
            doutvogalext = $("#doutvogalext-juris");
            mesargfmup = $("#mesargfmup-juris");
            mesvogalfmup = $("#mesvogalfmup-juris");
            mesargext = $("#mesargext-juris");
            mesvogalext = $("#mesvogalext-juris");
            mesintfmup = $("#mesintfmup-juris");
            mesintvogalfmup = $("#mesintvogalfmup-juris");
            mesintargext = $("#mesintargext-juris");
            mesintvogalext = $("#mesintvogalext-juris");

            agrargfmup.val($("#juris #td_juris_agrargfmup_" + id).text());
            agrvogalfmup.val($("#juris #td_juris_agrvogalfmup_" + id).text());
            agrargext.val($("#juris #td_juris_agrargext_" + id).text());
            agrvogalext.val($("#juris #td_juris_agrvogalext_" + id).text());
            doutargfmup.val($("#juris #td_juris_doutargfmup_" + id).text());
            doutvogalfmup.val($("#juris #td_juris_doutvogalfmup_" + id).text());
            doutargext.val($("#juris #td_juris_doutargext_" + id).text());
            doutvogalext.val($("#juris #td_juris_doutvogalext_" + id).text());
            mesargfmup.val($("#juris #td_juris_mesargfmup_" + id).text());
            mesvogalfmup.val($("#juris #td_juris_mesvogalfmup_" + id).text());
            mesargext.val($("#juris #td_juris_mesargext_" + id).text());
            mesvogalext.val($("#juris #td_juris_mesvogalext_" + id).text());
            mesintfmup.val($("#juris #td_juris_mesintfmup_" + id).text());
            mesintvogalfmup.val($("#juris #td_juris_mesintvogalfmup_" + id).text());
            mesintargext.val($("#juris #td_juris_mesintargext_" + id).text());
            mesintvogalext.val($("#juris #td_juris_mesintvogalext_" + id).text());

            tips = $(".validateTips-juris");

            $("#dialog-form-juris").dialog("open");
        });

    //Outras Actividades 
    $("#dialog-form-outact").dialog({
        autoOpen: false,
        width: 550,
        length: 500,
        modal: true,
        buttons: {
            Confirmar: function () {
				var bValid = true;

                allFields.removeClass("ui-state-error");

                bValid = bValid && checkNumMec(texapoio);
                bValid = bValid && checkNumMec(livdidaticos);
                bValid = bValid && checkNumMec(livdivulgacao);
                bValid = bValid && checkNumMec(app);
                bValid = bValid && checkNumMec(webp);

                if (bValid) {		
					
					$("#outact #td_outact_texapoio_" + id).replaceWith("<td id='td_outact_texapoio_" + id + "'>" + texapoio.val() + "</td>");
					$("#outact #td_outact_livdidaticos_" + id).replaceWith("<td id='td_outact_livdidaticos_" + id + "'>" + livdidaticos.val() + "</td>");
					$("#outact #td_outact_livdivulgacao_" + id).replaceWith("<td id='td_outact_livdivulgacao_" + id + "'>" + livdivulgacao.val() + "</td>");
					$("#outact #td_outact_app_" + id).replaceWith("<td id='td_outact_app_" + id + "'>" + app.val() + " </td>");
					$("#outact #td_outact_webp_" + id).replaceWith("<td id='td_outact_webp_" + id + "'>" + webp.val() + "</td>");

					saveOutActInBD(id, texapoio.val(), livdidaticos.val(), livdivulgacao.val(), app.val(), webp.val(), $('#login').text(), $('#dep').text(), '');

					$(this).dialog("close");
				}
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        },
        close: function () {
            allFields.val("").removeClass("ui-state-error");
        }
    });

    $('#outact img')
        .click(function () {
            id = $("#chave-outact").text();

            texapoio = $("#texapoio-outact");
            livdidaticos = $("#livdidaticos-outact");
            livdivulgacao = $("#livdivulgacao-outact");
            app = $("#app-outact");
            webp = $("#webp-outact");

            texapoio.val($("#outact #td_outact_texapoio_" + id).text());
            livdidaticos.val($("#outact #td_outact_livdidaticos_" + id).text());
            livdivulgacao.val($("#outact #td_outact_livdivulgacao_" + id).text());
            app.val($("#outact #td_outact_app_" + id).text());
            webp.val($("#outact #td_outact_webp_" + id).text());

            tips = $(".validateTips-outact");

            $("#dialog-form-outact").dialog("open");
        });

    //Aè¤¯ de Divulgacao
    $("#dialog-form-acaodivulgacao").dialog({
        autoOpen: false,
        width: 550,
        length: 500,
        modal: true,
        buttons: {
            Confirmar: function () {
				var bValid = true;

                allFields.removeClass("ui-state-error");

                bValid = bValid && checkName(titulo);
                bValid = bValid && checkName(data);
                bValid = bValid && checkName(local);
                bValid = bValid && checkNumMec(npart);

                if (bValid) {					
					$("#acaodivulgacao #td_acaodivulgacao_titulo_" + id).replaceWith("<td id='td_acaodivulgacao_titulo_" + id + "'>" + titulo.val() + "</td>");
					$("#acaodivulgacao #td_acaodivulgacao_data_" + id).replaceWith("<td id='td_acaodivulgacao_data_" + id + "'>" + data.val() + "</td>");
					$("#acaodivulgacao #td_acaodivulgacao_local_" + id).replaceWith("<td id='td_acaodivulgacao_local_" + id + "'>" + local.val() + "</td>");
					$("#acaodivulgacao #td_acaodivulgacao_npart_" + id).replaceWith("<td id='td_acaodivulgacao_npart_" + id + "'>" + npart.val() + " </td>");

					saveAcaoDivulgacaoInBD(id, titulo.val(), data.val(), local.val(), npart.val(), $('#login').text(), $('#dep').text(), '');

					$(this).dialog("close");
				}
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        },
        close: function () {
            allFields.val("").removeClass("ui-state-error");
        }
    });

    $('#acaodivulgacao img')
        .click(function () {
            id = $("#chave-acaodivulgacao").text();

            titulo = $("#titulo-acaodivulgacao");
            data = $("#data-acaodivulgacao");
            local = $("#local-acaodivulgacao");
            npart = $("#npart-acaodivulgacao");

            titulo.val($("#acaodivulgacao #td_acaodivulgacao_titulo_" + id).text());
            data.val($("#acaodivulgacao #td_acaodivulgacao_data_" + id).text());
            local.val($("#acaodivulgacao #td_acaodivulgacao_local_" + id).text());
            npart.val($("#acaodivulgacao #td_acaodivulgacao_npart_" + id).text());

            tips = $(".validateTips-acaodivulgacao");

            $("#dialog-form-acaodivulgacao").dialog("open");
        });

    //Produtos
    $("#dialog-form-produtos").dialog({
        autoOpen: false,
        width: 550,
        length: 500,
        modal: true,
        buttons: {
            Confirmar: function () {
				var bValid = true;
			
                bValid = bValid && checkName(tipo);
                bValid = bValid && checkNumMec(preco);

                if (bValid) {

					$("#produtos #td_produtos_tipo_" + id).replaceWith("<td id='td_produtos_tipo_" + id + "'>" + tipo.val() + "</td>");
					$("#produtos #td_produtos_preco_" + id).replaceWith("<td id='td_produtos_preco_" + id + "'>" + preco.val() + " </td>");

					saveProdutoInBD(id, tipo.val(), preco.val(), $('#login').text(), $('#dep').text(), '');

					$(this).dialog("close");
				}
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        },
        close: function () {
            allFields.val("").removeClass("ui-state-error");
        }
    });

    $('#produtos img')
        .click(function () {
            id = $("#chave-produtos").text();

            tipo = $("#tipo-produtos");
            preco = $("#preco-produtos");

            tipo.val($("#produtos #td_produtos_tipo_" + id).text());
            preco.val($("#produtos #td_produtos_preco_" + id).text().split('')[0].trim());

            tips = $(".validateTips-produtos");

            $("#dialog-form-produtos").dialog("open");
        });

    $("#dialog-form-obj").dialog({
        autoOpen: false,
        width: 550,
        length: 500,
        modal: true,
        buttons: {
            Confirmar: function () {
                texto = $("#texto-obj").val();
                login = $("#login").text();
                dep = $("#dep").text();
                saveObsInBD(id, login, texto, tabela, dep);
                $(this).dialog("close");
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        },
        close: function () {
            allFields.val("").removeClass("ui-state-error");
        }
    });

    $("#dialog-confirm-inv").dialog({
        autoOpen: false,
        resizable: false,
        height: 200,
        modal: true,
        buttons: {
            "Confirmar": function () {
                login = $("#login").text();
                deleteInvestigadorInBD($('#chave-inv').text(), login);	
                $(this).dialog("close");
            },
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });

    $("#dialog-confirm-hab").dialog({
        autoOpen: false,
        resizable: false,
        height: 200,
        modal: true,
        buttons: {
            "Confirmar": function () {
                login = $("#login").text();
                deleteHabilitacaoInBD($('#chave-hab').text(), login);
                $(this).dialog("close");
            },
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });

    $("#dialog-confirm-agr").dialog({
        autoOpen: false,
        resizable: false,
        height: 200,
        modal: true,
        buttons: {
            "Confirmar": function () {
                login = $("#login").text();
                deleteAgregacaoInBD($('#chave-agr').text(), login);	
                $(this).dialog("close");
            },
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });

    $("#dialog-confirm-exp-pro-doc").dialog({
        autoOpen: false,
        resizable: false,
        height: 200,
        modal: true,
        buttons: {
            "Confirmar": function () {
                login = $("#login").text();
                deleteCaracterizacaoInBD($('#chave-exp-pro-doc').text(), login);
                $(this).dialog("close");
            },
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });

    $("#dialog-confirm-exp-pro-pdoc").dialog({
        autoOpen: false,
        resizable: false,
        height: 200,
        modal: true,
        buttons: {
            "Confirmar": function () {
                login = $("#login").text();
                deleteCaracterizacaoInBD($('#chave-exp-pro-pdoc').text(), login);	
                $(this).dialog("close");
            },
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });

    $("#dialog-confirm-dircurso").dialog({
        autoOpen: false,
        resizable: false,
        height: 200,
        modal: true,
        buttons: {
            "Confirmar": function () {
                login = $("#login").text();
                deleteDirCursoInBD($('#chave-dircurso').text(), login);	
                $(this).dialog("close");
            },
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });

    $("#dialog-confirm-colIntAct").dialog({
        autoOpen: false,
        resizable: false,
        height: 200,
        modal: true,
        buttons: {
            "Confirmar": function () {
                login = $("#login").text();
                deleteColIntActInBD($('#chave-colIntAct').text(), login);	
                $(this).dialog("close");
            },
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });

    $("#dialog-confirm-colExtAct").dialog({
        autoOpen: false,
        resizable: false,
        height: 200,
        modal: true,
        buttons: {
            "Confirmar": function () {
                login = $("#login").text();
                deleteColExtActInBD($('#chave-colExtAct').text(), login);	
                $(this).dialog("close");
            },
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });

    $("#dialog-confirm").dialog({
        autoOpen: false,
        resizable: false,
        height: 200,
        modal: true,
        buttons: {
            "Confirmar": function () {
                login = $("#login").text();				
                deleteProjecto($('#chave-projectos').text(), login);	
				loadDataPRJ(1);
                $(this).dialog("close");
            },
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });

    $("#dialog-confirm-pub").dialog({
        autoOpen: false,
        resizable: false,
        height: 200,
        modal: true,
        buttons: {
            "Confirmar": function () {
                login = $("#login").text();
				tabela = $('#tabela').text();
                deletePublicacao($('#chave-publicacoes').text(), login, tabela);
				
				loadDataJISI(1); 
				loadDataJAISI(1);		 
				loadDataPRPISI(1);	
				loadDataCPISI(1);	
				loadDataOAISI(1);
				loadDataLISI(1);
				loadDataCLISI(1);
				loadDataSISI(1);
				loadDataJNISI(1);
				loadDataPRPNISI(1); 
				loadDataSNISI(1);
				loadDataLNISI(1);
				loadDataCLNISI(1); 
				loadDataCPNISI(1); 
				loadDataPISI(1);
				loadDataAISI(1);
				
                $(this).dialog("close");
            },
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });

    $("#dialog-confirm-cnf").dialog({
        autoOpen: false,
        resizable: false,
        height: 200,
        modal: true,
        buttons: {
            "Confirmar": function () {
                login = $("#login").text();
                deleteConferencia($('#chave-conf').text(), login);
				loadDataCNF(1);
                $(this).dialog("close");
            },
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });

    $("#dialog-confirm-apr").dialog({
        autoOpen: false,
        resizable: false,
        height: 200,
        modal: true,
        buttons: {
            "Confirmar": function () {
                login = $("#login").text();
                deleteApresentacao($('#chave-apresentacoes').text(), login);
                $(this).dialog("close");
            },
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });

    $("#dialog-confirm-acaoDiv").dialog({
        autoOpen: false,
        resizable: false,
        height: 200,
        modal: true,
        buttons: {
            "Confirmar": function () {
                login = $("#login").text();				
                deleteAcaoDivulgacao($('#chave-acaodivulgacao').text(), login);
				loadDataAcaoDiv(1);
                $(this).dialog("close");
            },
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });
	
    $("#dialog-confirm-produto").dialog({
        autoOpen: false,
        resizable: false,
        height: 200,
        modal: true,
        buttons: {
            "Confirmar": function () {
                login = $("#login").text();
                deleteProduto($('#chave-produtos').text(), login);
                $(this).dialog("close");
            },
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });
	

    $("#dialog-confirm-premio").dialog({
        autoOpen: false,
        resizable: false,
        height: 200,
        modal: true,
        buttons: {
            "Confirmar": function () {
                login = $("#login").text();
                deletePremio($('#chave-premios').text(), login);
				loadDataPremios(1);
                $(this).dialog("close");
            },
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });

    $("#dialog-confirm-arbRevista").dialog({
        autoOpen: false,
        resizable: false,
        height: 200,
        modal: true,
        buttons: {
            "Confirmar": function () {
                login = $("#login").text();
                deleteArbRevista($('#chave-arb-revistas').text(), login);		 
				loadDataArbRevistas(1); 
                $(this).dialog("close");
            },
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });

    $("#dialog-confirm-uniInv").dialog({
        autoOpen: false,
        resizable: false,
        height: 200,
        modal: true,
        buttons: {
            "Confirmar": function () {
                login = $("#login").text();
                deleteUniInvInBD($('#chave-uniInv').text(), login);
				loadDataUniInv(1);
                $(this).dialog("close");
            },
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });

    $("#dialog-confirm-sc").dialog({
        autoOpen: false,
        resizable: false,
        height: 200,
        modal: true,
        buttons: {
            "Confirmar": function () {
                login = $("#login").text();
                deleteSC($('#chave-sc').text(), login);
                $(this).dialog("close");
            },
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });

    $("#dialog-confirm-rede").dialog({
        autoOpen: false,
        resizable: false,
        height: 200,
        modal: true,
        buttons: {
            "Confirmar": function () {
                login = $("#login").text();
                deleteRede($('#chave-redes').text(), login);
                $(this).dialog("close");
            },
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });

    $("#dialog-confirm-missao").dialog({
        autoOpen: false,
        resizable: false,
        height: 200,
        modal: true,
        buttons: {
            "Confirmar": function () {
                login = $("#login").text();
                deleteMissao($('#chave-missoes').text(), login);
                $(this).dialog("close");
            },
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });
	
    $("#dialog-confirm-orientacao").dialog({
        autoOpen: false,
        resizable: false,
        height: 200,
        modal: true,
        buttons: {
            "Confirmar": function () {
                login = $("#login").text();
                deleteOrientacao($('#chave-orientacoes').text(), login);
                $(this).dialog("close");
            },
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });
	
	$("#dialog-insert-investigador").dialog({
        autoOpen: false,
        resizable: false,
         width: 550,
        length: 500,
        modal: true,
        buttons: {
            "Confirmar": function () {
				var nome = $("#nome-ins-inv"),
				nummec = $("#nummec-ins-inv"),
				datanasc = $("#datanasc-ins-inv");
		
                var bValid = true;
				
				var findPurpose = searchUser(nome.val());
	
				if(findPurpose != null) {
					var bool = confirm("Já existe um investigador com esse nome, quer inserir na mesma ?");
					if (!bool) {						
						$(this).dialog("close");
						bValid = false;
					}
				} 

                allFields.removeClass("ui-state-error");

                bValid = bValid && checkName(nome);
                bValid = bValid && checkNumMec(nummec);

                if (bValid) {
                    $("#users tbody #user" + id + " #td" + id + "_nome").replaceWith("<td id='td" + id + "_nome'>" + nome.val() + "</td>");
                    $("#users tbody #user" + id + " #td" + id + "_nummec").replaceWith("<td id='td" + id + "_nummec'>" + nummec.val() + "</td>");
                    $("#users tbody #user" + id + " #td" + id + "_datanasc").replaceWith("<td id='td" + id + "_datanasc'>" + datanasc.val() + "</td>");

                    insertInvestigadorInBD(nome.val(), nummec.val(), datanasc.val(), $('#login').text(), $('#dep').text());
                    $(this).dialog("close");
                }
            },
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });	
	
	
	
	$("#dialog-insert-agregacao").dialog({
        autoOpen: false,
        resizable: false,
        width: 550,
        length: 500,
        modal: true,
        buttons: {
            "Confirmar": function () {		
				unanimidade = $("#unanimidade-ins-agr");
				
                $("#agreg #td_unanimidade_agr_" + id).replaceWith("<td id='td_unanimidade_agr_" + id + "'>" + unanimidade.val() + "</td>");

                insertAgregacaoInBD(10000, unanimidade.val(), $('#login').text(), $('#dep').text());
                $(this).dialog("close");            
            },
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });
	
	$("#dialog-insert-caracDoc").dialog({
        autoOpen: false,
        resizable: false,
        width: 550,
        length: 500,
        modal: true,
        buttons: {
            "Confirmar": function () {		
				tipo = $("#tipo-ins-doc");
				percentagem = $("#perc-ins-doc");
				                
				login = $('#login').text();
				insertCaracterizacaoDocentesInBD(10000, tipo.val(), percentagem.val(), login, $('#dep').text());          

                $(this).dialog("close");          
            },
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });
	
	$("#dialog-insert-caracPosDoc").dialog({
        autoOpen: false,
        resizable: false,
        width: 550,
        length: 500,
        modal: true,
        buttons: {
            "Confirmar": function () {		
				bolsa = $("#bolsa-ins-pdoc");
				percentagem = $("#per-ins-pdoc");
								                
				login = $('#login').text();
				insertCaracterizacaoPosDocInBD(10000, bolsa.val(), percentagem.val(), login, $('#dep').text());          

                $(this).dialog("close");          
            },
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });
	
	$("#dialog-insert-dirCurso").dialog({
        autoOpen: false,
        resizable: false,
        width: 550,
        length: 500,
        modal: true,
        buttons: {
            "Confirmar": function () {		
				datainicio = $("#dataini-ins-dircurso");
				datafim = $("#datafim-ins-dircurso");
				curso = $("#curso-ins-dircurso");
				grau = $("#grau-ins-dircurso");
				
				insertDirCursoInBD(10000, datainicio.val(), datafim.val(), curso.val(), grau.val(), $('#login').text(), $('#dep').text());
				$(this).dialog("close");
				},
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });
	
	$("#dialog-insert-colIntAct").dialog({
        autoOpen: false,
        resizable: false,
        width: 550,
        length: 500,
        modal: true,
        buttons: {
            "Confirmar": function () {	
				nomecient = $("#nomecient-ins-colIntAct");
				departamento = $("#dep-ins-colIntAct");
				
                var bValid = true;
                allFields.removeClass("ui-state-error");
                bValid = bValid && checkName(nomecient);
                if (bValid) { 
					
                    insertColIntActInBD(10000, nomecient.val(), departamento.val(), $('#login').text(), $('#dep').text());
                    $(this).dialog("close");
                } 
			},
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });
	
	$("#dialog-insert-colExtAct").dialog({
        autoOpen: false,
        resizable: false,
        width: 550,
        length: 500,
        modal: true,
        buttons: {
            "Confirmar": function () {	
				nomecient = $("#nomecient-ins-colExtAct");
				instituicao = $("#inst-ins-colExtAct").val();
				pais = $("#pais-ins-colExtAct");
				
				var bValid = true;
                allFields.removeClass("ui-state-error");
                bValid = bValid && checkName(nomecient);

                if (bValid) {
                    insertColExtActInBD(10000, nomecient.val(), instituicao, pais.val(), $('#login').text(), $('#dep').text());
                    $(this).dialog("close"); 
				}
			},
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });
	
	$("#dialog-insert-uniInv").dialog({
        autoOpen: false,
        resizable: false,
        width: 800,
        length: 1000,
        modal: true,
        buttons: {
            "Confirmar": function () {	
				unidade = $("#uni-ins-uniInv option:selected").text();
				avaliacao = $("#ava-ins-uniInv").val();
				percentagem = $("#perc-ins-uniInv");
				responsavel = $("#resp-ins-uniInv");
				
				insertUniInvInBD(10000, unidade, avaliacao, percentagem.val(), responsavel.val(), $('#login').text(), $('#dep').text());					
				$(this).dialog("close"); 
                
			},
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });
	
	$("#dialog-insert-projectos").dialog({
        autoOpen: false,
        resizable: false,
        width: 500,
        length: 500,
        modal: true,
        buttons: {
            "Confirmar": function () {	
				tipo = $("#tipo-ins-projectos");
				entidade = $("#entidade-ins-projectos");
				inst = $("#inst-ins-projectos");
				monsol = $("#monsol-ins-projectos");
				monapr = $("#monapr-ins-projectos");
				monfmup = $("#monfmup-ins-projectos");
				resp = $("#resp-ins-projectos");
				codigo = $("#codigo-ins-projectos");
				titulo = $("#titulo-ins-projectos");
				dataini = $("#dataini-ins-projectos");
				datafim = $("#datafim-ins-projectos");
				link = $("#link-ins-projectos");
				estado = $("#estado-ins-projectos");			
                login = $("#login").text();	
                dep = $("#dep").text();
				
                insertProjectoInBD(10000, tipo.val().trim(), entidade.val().trim(), inst.val().trim(), monsol.val().trim(), monapr.val().trim(), monfmup.val().trim(), resp.val().trim(), codigo.val().trim(), titulo.val().trim(), dataini.val().trim(), datafim.val().trim(), link.val().trim(), estado.val().trim(), login, dep);
				$(this).dialog("close"); 
			},
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });
	
	$("#dialog-insert-conferencia").dialog({
        autoOpen: false,
        resizable: false,
        width: 500,
        length: 500,
        modal: true,
        buttons: {
            "Confirmar": function () {				
				ambito = $("#ambito-ins-conferencias");
				dataini = $("#dataini-ins-conferencias");
				datafim = $("#datafim-ins-conferencias");
				titulo = $("#titulo-ins-conferencias");
				local = $("#local-ins-conferencias");
				npart = $("#npart-ins-conferencias");
				link = $("#link-ins-conferencias");
				
				var bValid = true;

                allFields.removeClass("ui-state-error");

                bValid = bValid && checkName(ambito);
                bValid = bValid && checkName(dataini);
                bValid = bValid && checkName(datafim);
                bValid = bValid && checkName(titulo);
                bValid = bValid && checkName(local);
                bValid = bValid && checkName(npart);
                bValid = bValid && checkName(link);

                if (bValid) {                    
                    insertConferenciaInBD(10000, ambito.val(), dataini.val(), datafim.val(), titulo.val(),
										  local.val(), npart.val(), link.val(), $('#login').text(), $('#dep').text());
                    $(this).dialog("close");
                }
				$(this).dialog("close"); 
			},
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });
	
	$("#dialog-insert-apresentacao").dialog({
        autoOpen: false,
        resizable: false,
        width: 500,
        length: 500,
        modal: true,
        buttons: {
            "Confirmar": function () {						
				iconfconv = $("#iconfconv-ins-apresentacoes");
				iconfpart = $("#iconfpart-ins-apresentacoes");
				isem = $("#isem-ins-apresentacoes");
				nconfconv = $("#nconfconv-ins-apresentacoes");
				nconfpart = $("#nconfpart-ins-apresentacoes");
				nsem = $("#nsem-ins-apresentacoes");
				
				var bValid = true;

                allFields.removeClass("ui-state-error");

                bValid = bValid && checkName(iconfconv);
                bValid = bValid && checkName(iconfpart);
                bValid = bValid && checkName(isem);
                bValid = bValid && checkName(nconfconv);
                bValid = bValid && checkName(nconfpart);
                bValid = bValid && checkName(nsem);

                if (bValid) {
                   insertApresentacaoInBD(10000, iconfconv.val(), iconfpart.val(), isem.val(), nconfconv.val(), nconfpart.val(), nsem.val(), $('#login').text(), $('#dep').text());
                    $(this).dialog("close");
				}
			},
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });
	
	$("#dialog-insert-arbRevista").dialog({
        autoOpen: false,
        resizable: false,
        width: 600,
        length: 500,
		show: {
			effect: "blind",
			duration: 1000
		},
		hide: {
			effect: "explode",
			duration: 1000
		},
        modal: true,
        buttons: {
            "Confirmar": function () {					
				tipo = $("#tipo-ins-arbrevistas");
				issn = $("#issn-ins-arbrevistas");
				titulo = $("#titulo-ins-arbrevistas");
				link = $("#link-ins-arbrevistas");
				
				var bValid = true;

                allFields.removeClass("ui-state-error");

                bValid = bValid && checkName(tipo);
                bValid = bValid && checkName(issn);
                bValid = bValid && checkName(titulo);
                bValid = bValid && checkName(link);

                if (bValid) {
					insertArbRevistaInBD(10000, tipo.val(), issn.val(), titulo.val(), link.val(), $('#login').text(), $('#dep').text());
                    $(this).dialog("close");
                }
			},
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });
	
	$("#dialog-insert-premio").dialog({
        autoOpen: false,
        resizable: false,
        width: 600,
        length: 500,
		show: {
			effect: "blind",
			duration: 1000
		},
		hide: {
			effect: "explode",
			duration: 1000
		},
        modal: true,
        buttons: {
            "Confirmar": function () {				
				tipo = $("#tipo-ins-premios");
				nome = $("#nome-ins-premios");
				contexto = $("#contexto-ins-premios");
				montante = $("#montante-ins-premios");
				link = $("#link-ins-premios");
				
                var bValid = true;

                allFields.removeClass("ui-state-error");

                bValid = bValid && checkName(tipo);
                bValid = bValid && checkName(nome);
                bValid = bValid && checkName(contexto);
                bValid = bValid && checkName(montante);
                bValid = bValid && checkName(link);

                if (bValid) {
                    insertPremioInBD(10000, tipo.val(), nome.val(), contexto.val(), montante.val(), link.val(), $('#login').text(), $('#dep').text());
                    $(this).dialog("close");
                } 
			},
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });
	
	$("#dialog-insert-socCientifica").dialog({
        autoOpen: false,
        resizable: false,
        width: 600,
        length: 500,
		show: {
			effect: "blind",
			duration: 1000
		},
		hide: {
			effect: "explode",
			duration: 1000
		},
        modal: true,
        buttons: {
            "Confirmar": function () {			
				tipo = $("#tipo-ins-sc");
				datainicio = $("#datainicio-ins-sc");
				datafim = $("#datafim-ins-sc");
				cargo = $("#cargo-ins-sc");
				link = $("#link-ins-sc");
				nome = $("#nome-ins-sc");
                var bValid = true;

                allFields.removeClass("ui-state-error");

                bValid = bValid && checkName(tipo);
                bValid = bValid && checkName(nome);
                bValid = bValid && checkName(datainicio);
                bValid = bValid && checkName(datafim);
                bValid = bValid && checkName(cargo);
                bValid = bValid && checkName(link);

                if (bValid) {                    
                    insertSCInBD(10000, tipo.val(), datainicio.val(), datafim.val(), cargo.val(), link.val(), nome.val(), $('#login').text(), $('#dep').text());
                    $(this).dialog("close");
                } 
			},
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });		
	
	$("#dialog-insert-rede").dialog({
        autoOpen: false,
        resizable: false,
        width: 600,
        length: 500,
		show: {
			effect: "blind",
			duration: 1000
		},
		hide: {
			effect: "explode",
			duration: 1000
		},
        modal: true,
        buttons: {
            "Confirmar": function () {			
				nome = $("#nome-ins-redes");
				tipo = $("#tipo-ins-redes");
				datainicio = $("#datainicio-ins-redes");
				datafim = $("#datafim-ins-redes");
				link = $("#link-ins-redes");
				var bValid = true;

                allFields.removeClass("ui-state-error");

                bValid = bValid && checkName(tipo);
                bValid = bValid && checkName(nome);
                bValid = bValid && checkName(datainicio);
                bValid = bValid && checkName(datafim);
                bValid = bValid && checkName(link);

                if (bValid) {                    
                    insertRedesInBD(10000, tipo.val(), datainicio.val(), datafim.val(), link.val(), nome.val(), $('#login').text(), $('#dep').text());
                    $(this).dialog("close");
                }  
			},
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });		
	
	$("#dialog-insert-missao").dialog({
        autoOpen: false,
        resizable: false,
        width: 600,
        length: 500,
		show: {
			effect: "blind",
			duration: 1000
		},
		hide: {
			effect: "explode",
			duration: 1000
		},
        modal: true,
        buttons: {
            "Confirmar": function () {			
				datainicio = $("#datainicio-ins-missoes");
				datafim = $("#datafim-ins-missoes");
				motivacao = $("#motivacao-ins-missoes");
				instituicao = $("#instituicao-ins-missoes");
				pais = $("#pais-ins-missoes");
				ambtese = $("#ambtese-ins-missoes");
				
                var bValid = true;

                allFields.removeClass("ui-state-error");

                bValid = bValid && checkName(datainicio);
                bValid = bValid && checkName(datafim);
                bValid = bValid && checkName(motivacao);
                bValid = bValid && checkName(instituicao);
                bValid = bValid && checkName(pais);
                bValid = bValid && checkName(ambtese);

                if (bValid) {
                    insertMissoesInBD(10000, datainicio.val(), datafim.val(), motivacao.val(), instituicao.val(), pais.val(), ambtese.val(), $('#login').text(), $('#dep').text());
                    $(this).dialog("close");
                }
			},
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });		
	
	$("#dialog-insert-orientacao").dialog({
        autoOpen: false,
        resizable: false,
        width: 600,
        length: 500,
		show: {
			effect: "blind",
			duration: 1000
		},
		hide: {
			effect: "explode",
			duration: 1000
		},
        modal: true,
        buttons: {
            "Confirmar": function () {					
				tipo = $("#tipo-ins-orientacoes");
				tipoori = $("#tipoori-ins-orientacoes");
				estado = $("#estado-ins-orientacoes");
				titulo = $("#titulo-ins-orientacoes");			
				var bValid = true;

                allFields.removeClass("ui-state-error");

                bValid = bValid && checkName(tipo);
                bValid = bValid && checkName(tipoori);
                bValid = bValid && checkName(estado);
                bValid = bValid && checkName(titulo);

                if (bValid) {                    
					insertOrientacoesInBD(10000, tipo.val(), tipoori.val(), estado.val(), titulo.val(), $('#login').text(), $('#dep').text());
					$(this).dialog("close");
				}
			},
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });		
	
	$("#dialog-insert-juri").dialog({
        autoOpen: false,
        resizable: false,
        width: 600,
        length: 500,
		show: {
			effect: "blind",
			duration: 1000
		},
		hide: {
			effect: "explode",
			duration: 1000
		},
        modal: true,
        buttons: {
            "Confirmar": function () {				
				agrargfmup = $("#agrargfmup-ins-juris");
				agrvogalfmup = $("#agrvogalfmup-ins-juris");
				agrargext = $("#agrargext-ins-juris");
				agrvogalext = $("#agrvogalext-ins-juris");
				doutargfmup = $("#doutargfmup-ins-juris");
				doutvogalfmup = $("#doutvogalfmup-ins-juris");
				doutargext = $("#doutargext-ins-juris");
				doutvogalext = $("#doutvogalext-ins-juris");
				mesargfmup = $("#mesargfmup-ins-juris");
				mesvogalfmup = $("#mesvogalfmup-ins-juris");
				mesargext = $("#mesargext-ins-juris");
				mesvogalext = $("#mesvogalext-ins-juris");
				mesintfmup = $("#mesintfmup-ins-juris");
				mesintvogalfmup = $("#mesintvogalfmup-ins-juris");
				mesintargext = $("#mesintargext-ins-juris");
				mesintvogalext = $("#mesintvogalext-ins-juris");
				
				var bValid = true;

                allFields.removeClass("ui-state-error");

                bValid = bValid && checkNumMec(agrargfmup);
                bValid = bValid && checkNumMec(agrvogalfmup);
                bValid = bValid && checkNumMec(agrargext);
                bValid = bValid && checkNumMec(agrvogalext);				
				
                bValid = bValid && checkNumMec(doutargfmup);
                bValid = bValid && checkNumMec(doutvogalfmup);
                bValid = bValid && checkNumMec(doutargext);
                bValid = bValid && checkNumMec(doutvogalext);				
				
                bValid = bValid && checkNumMec(mesargfmup);
                bValid = bValid && checkNumMec(mesvogalfmup);
                bValid = bValid && checkNumMec(mesargext);
                bValid = bValid && checkNumMec(mesvogalext);
				
				bValid = bValid && checkNumMec(mesintfmup);
                bValid = bValid && checkNumMec(mesintvogalfmup);
                bValid = bValid && checkNumMec(mesintargext);
                bValid = bValid && checkNumMec(mesintvogalext);

                if (bValid) {
					insertJurisInBD(10000, agrargfmup.val(), agrvogalfmup.val(), agrargext.val(), agrvogalext.val(), doutargfmup.val(), doutvogalfmup.val(), doutargext.val(), doutvogalext.val(), mesargfmup.val(), mesvogalfmup.val(), mesargext.val(), mesvogalext.val(), mesintfmup.val(), mesintvogalfmup.val(), mesintargext.val(), mesintvogalext.val() , $('#login').text(), $('#dep').text());
					$(this).dialog("close");
				}
			},
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });	
	
	$("#dialog-insert-outAct").dialog({
        autoOpen: false,
        resizable: false,
        width: 600,
        length: 500,
		show: {
			effect: "blind",
			duration: 1000
		},
		hide: {
			effect: "explode",
			duration: 1000
		},
        modal: true,
        buttons: {
            "Confirmar": function () {				
				texapoio = $("#texapoio-ins-outact");
				livdidaticos = $("#livdidaticos-ins-outact");
				livdivulgacao = $("#livdivulgacao-ins-outact");
				app = $("#app-ins-outact");
				webp = $("#webp-ins-outact");
				var bValid = true;

                allFields.removeClass("ui-state-error");

                bValid = bValid && checkNumMec(texapoio);
                bValid = bValid && checkNumMec(livdidaticos);
                bValid = bValid && checkNumMec(livdivulgacao);
                bValid = bValid && checkNumMec(app);
                bValid = bValid && checkNumMec(webp);

                if (bValid) {
					insertOutActInBD(10000, texapoio.val(), livdidaticos.val(), livdivulgacao.val(), app.val(), webp.val(), $('#login').text(), $('#dep').text());
					$(this).dialog("close");
				}  
			},
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });	
	
	$("#dialog-insert-acaoDiv").dialog({
        autoOpen: false,
        resizable: false,
        width: 600,
        length: 500,
		show: {
			effect: "blind",
			duration: 1000
		},
		hide: {
			effect: "explode",
			duration: 1000
		},
        modal: true,
        buttons: {
            "Confirmar": function () {						
				titulo = $("#titulo-ins-acaodivulgacao");
				data = $("#data-ins-acaodivulgacao");
				local = $("#local-ins-acaodivulgacao");
				npart = $("#npart-ins-acaodivulgacao");
				var bValid = true;

                allFields.removeClass("ui-state-error");

                bValid = bValid && checkName(titulo);
                bValid = bValid && checkName(data);
                bValid = bValid && checkName(local);
                bValid = bValid && checkNumMec(npart);

                if (bValid) {
					insertAcaoDivulgacaoInBD(10000, titulo.val(), data.val(), local.val(), npart.val(), $('#login').text(), $('#dep').text());
					$(this).dialog("close");
				}
			},
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });	
	
	$("#dialog-insert-produto").dialog({
        autoOpen: false,
        resizable: false,
        width: 600,
        length: 500,
		show: {
			effect: "blind",
			duration: 1000
		},
		hide: {
			effect: "explode",
			duration: 1000
		},
        modal: true,
        buttons: {
            "Confirmar": function () {					
				tipo = $("#tipo-ins-produtos");
				preco = $("#preco-ins-produtos");
				var bValid = true;
			
                bValid = bValid && checkName(tipo);
                bValid = bValid && checkNumMec(preco);

                if (bValid) {
					insertProdutoInBD(10000, tipo.val(), preco.val(), $('#login').text(), $('#dep').text());
					$(this).dialog("close");
				} 
			},
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });
	
	$("#dialog-insert-pubArt").dialog({
        autoOpen: false,
        resizable: false,
        width: 600,
        length: 500,		
        modal: true,
        buttons: {
            "Confirmar": function () {				
				issn = $("#issn-ins-pubArt");
				revista = $("#revista-ins-pubArt");
				titulo = $("#titulo-ins-pubArt");
				autores = $("#autores-ins-pubArt");
				volume = $("#volume-ins-pubArt");
				issue = $("#issue-ins-pubArt");
				prim = $("#prim-ins-pubArt");
				ult = $("#ult-ins-pubArt");
				citacoes = $("#citacoes-ins-pubArt");
				estado = $("#estado-ins-pubArt");
				
				var bValid = true;

                allFields.removeClass("ui-state-error");

                bValid = bValid && checkName(issn);
                bValid = bValid && checkName(revista);
                bValid = bValid && checkName(titulo);
                bValid = bValid && checkName(autores);				
                bValid = bValid && checkNumMec(volume);				
                bValid = bValid && checkNumMec(issue);				
                bValid = bValid && checkNumMec(prim);				
                bValid = bValid && checkNumMec(ult);
				
                if (bValid) {					
					insertPubArtInBD(10000, issn.val(), revista.val(), titulo.val(), autores.val(), volume.val(), issue.val(), prim.val(), ult.val(), citacoes.val(), estado.val(), $('#login').text(), $('#dep').text(), tipopublicacao,descr);
					$(this).dialog("close");
				} 
			},
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });
			
	$("#dialog-insert-pubConf").dialog({
        autoOpen: false,
        resizable: false,
        width: 600,
        length: 500,		
        modal: true,
        buttons: {
            "Confirmar": function () {				
				isbn = $("#isbn-ins-pubConf");
				nomepub = $("#nomepub-ins-pubConf");				
				editores = $("#editores-ins-pubConf");
				colecao = $("#colecao-ins-pubConf");
				volume = $("#volume-ins-pubConf");				
				titulo = $("#titulo-ins-pubConf");
				autores = $("#autores-ins-pubConf");
				prim = $("#prim-ins-pubConf");
				ult = $("#ult-ins-pubConf");
				estado = $("#estado-ins-pubConf");
				
				var bValid = true;

                allFields.removeClass("ui-state-error");
				
                if (bValid) {					
					insertPubConfInBD(10000, isbn.val(), nomepub.val(), editores.val(), colecao.val(), volume.val(), titulo.val(), autores.val(), prim.val(), ult.val(), estado.val(), $('#login').text(), $('#dep').text(), tipopublicacao, descr);
					$(this).dialog("close");
				} 
			},
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });
	
	$("#dialog-insert-pubLiv").dialog({
        autoOpen: false,
        resizable: false,
        width: 600,
        length: 500,		
        modal: true,
        buttons: {
            "Confirmar": function () {				
				nomepub = $("#nomepub-ins-pubLiv");
				issn = $("#issn-ins-pubLiv");				
				isbn =	$("#isbn-ins-pubLiv");					
				titulo = $("#titulo-ins-pubLiv");
				autores = $("#autores-ins-pubLiv");
				volume = $("#volume-ins-pubLiv");
				issue = $("#issue-ins-pubLiv");
				ar = $("#ar-ins-pubLiv");				
				colecao = $("#colecao-ins-pubLiv");				
				prim = $("#prim-ins-pubLiv");
				ult = $("#ult-ins-pubLiv");				
				citacoes = $("#citacoes-ins-pubLiv");
				conftitle = $("#conftitle-ins-pubLiv");											
				language = $("#language-ins-pubLiv");
				editor = $("#editor-ins-pubLiv");					
				estado = $("#estado-ins-pubLiv");	
				
				var bValid = true;

                allFields.removeClass("ui-state-error");
				
                if (bValid) {					
					insertPubLivInBD(10000, nomepub.val(), issn.val(), isbn.val(), titulo.val(), autores.val(), volume.val(), issue.val(), ar.val(), colecao.val(), prim.val(), ult.val(), citacoes.val(), conftitle.val(), language.val(), editor.val(), estado.val(), $('#login').text(), $('#dep').text(), tipopublicacao, descr);
					$(this).dialog("close");
				} 
			},
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });
	
	$("#dialog-insert-pubManualPatentes").dialog({
        autoOpen: false,
        resizable: false,
        width: 600,
        length: 500,		
        modal: true,
        buttons: {
            "Confirmar": function () {						
				npatente = $("#npatente-ins-pubManualPatentes");
				ipc = $("#ipc-ins-pubManualPatentes");
				titulo = $("#titulo-ins-pubManualPatentes");
				autores = $("#autores-ins-pubManualPatentes");
				data = $("#data-ins-pubManualPatentes");
				link = $("#link-ins-pubManualPatentes");
				estado = $("#estado-ins-pubManualPatentes");
				
                var bValid = true;

                allFields.removeClass("ui-state-error");

                bValid = bValid && checkName(npatente);
                bValid = bValid && checkName(ipc);
                bValid = bValid && checkName(titulo);
                bValid = bValid && checkName(autores);
				bValid = bValid && checkName(data);
                bValid = bValid && checkName(link);				

                if (bValid) {		
                    insertPubManPatentesInBD(10000, npatente.val(), ipc.val(), titulo.val(), autores.val(), data.val(), link.val(), estado.val(), $('#login').text(), $('#dep').text());
                    $(this).dialog("close"); 
				}
					
			},
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });
	
	$("#dialog-insert-pubManualLiv").dialog({
        autoOpen: false,
        resizable: false,
        width: 600,
        length: 500,		
        modal: true,
        buttons: {
            "Confirmar": function () {					
				titulo = $("#titulo-ins-pubManualLiv");
				autores = $("#autores-ins-pubManualLiv");
				editor = $("#editor-ins-pubManualLiv");		
				editora = $("#editora-ins-pubManualLiv");										
				link = $("#link-ins-pubManualLiv");			
				estado = $("#estado-ins-pubManualLiv");	
				
                var bValid = true;

                if (bValid) {		
                    insertPubManLivInBD(10000, titulo.val(), autores.val(), editor.val(), editora.val(), link.val(), estado.val(), $('#login').text(), $('#dep').text(), descr);
                    $(this).dialog("close");  
				}
					
			},
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });
	
	$("#dialog-insert-pubManualCLiv").dialog({
        autoOpen: false,
        resizable: false,
        width: 600,
        length: 500,		
        modal: true,
        buttons: {
            "Confirmar": function () {							
				isbn =	$("#isbn-ins-pubManualCLiv");		
				nomepub = $("#nomepub-ins-pubManualCLiv");	
				editora = $("#editora-ins-pubManualCLiv");	
				editor = $("#editor-ins-pubManualCLiv");					
				titulo = $("#titulo-ins-pubManualCLiv");
				autores = $("#autores-ins-pubManualCLiv");
				prim = $("#prim-ins-pubManualCLiv");
				ult = $("#ult-ins-pubManualCLiv");										
				link = $("#link-ins-pubManualCLiv");			
				estado = $("#estado-ins-pubManualCLiv");	
				
                var bValid = true;	

                if (bValid) {		
                    insertPubManCLivInBD(10000, isbn.val(), nomepub.val(), editora.val(), editor.val(), titulo.val(), autores.val(), prim.val(), ult.val(), link.val(), estado.val(), $('#login').text(), $('#dep').text(), descr);
                    $(this).dialog("close"); 
				}					
			},
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });
	
	$("#dialog-confirm-juri").dialog({
        autoOpen: false,
        resizable: false,
        height: 150,
        modal: true,
        buttons: {
            "Confirmar": function () {
                login = $("#login").text();
                deleteJuri($('#chave-juris').text(), login);
                $(this).dialog("close");
            },
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });
	
	$("#dialog-confirm-outact").dialog({
        autoOpen: false,
        resizable: false,
        height: 170,
        modal: true,
        buttons: {
            "Confirmar": function () {
                login = $("#login").text();
                deleteOutAct($('#chave-outact').text(), login);
                $(this).dialog("close");
            },
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });
	
    $("#dialog-confirm-acao").dialog({
        autoOpen: false,
        resizable: false,
        height: 180,
        modal: true,
        buttons: {
            "Confirmar": function () {
                validateAcao($('#chave-acao').text());
                $('#clicaAcao').click();
                $(this).dialog("close");
            },
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });

    $("#dialog-remove-acao").dialog({
        autoOpen: false,
        resizable: false,
        height: 180,
        modal: true,
        buttons: {
            "Confirmar": function () {
                removeAcao($('#chave-acao').text());
                $('#clicaAcao').click();
                $(this).dialog("close");
            },
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });
	
	 $("#dialog-apaga-acao").dialog({
        autoOpen: false,
        resizable: false,
        height: 180,
        modal: true,
        buttons: {
            "Confirmar": function () {
                deleteAcao(id,tab, $("#login").text());
                $(this).dialog("close");
            },
            "Cancelar": function () {
                $(this).dialog("close");
            }
        }
    });
	
    $("#dialog-confirm-repetidos").dialog({
        autoOpen: false,
        resizable: false,
        height: 200,
        width: 400,
        modal: true,
        buttons: {
            "OK": function () {
				orig = '';
				rep = [];
                $(this).dialog("close");
            }
        }
    });

    $("#dialog-showinfo-acao").dialog({
        autoOpen: false,
        resizable: false,
        height: 200,
        width: 400,
        modal: true,
        buttons: {
            "OK": function () {
                $(this).dialog("close");
            }
        }
    });

});

function setObservacaoProjectos() {
    tabela = 14;
    id = $("#chave-projectos").text();
    $('#texto-obj').val('');
    $("#dialog-form-obj").dialog("open");
}

function setObservacaoInvestigadores() {
    id = $("#chave-inv").text();
    $('#texto-obj').val('');
    tabela = 1;
    $("#dialog-form-obj").dialog("open");
}

function setObservacaoHabilitacoes() {
    id = $("#chave-hab").text();
    $('#texto-obj').val('');
    tabela = 2;
    $("#dialog-form-obj").dialog("open");
}

function setObservacaoAgregacao() {
    id = $("#chave-agr").text();
    $('#texto-obj').val('');
    tabela = 3;
    $("#dialog-form-obj").dialog("open");
}

function setObservacaoExpProDoc() {
    id = $("#chave-exp-pro-doc").text();
    $('#texto-obj').val('');
    tabela = 4;
    $("#dialog-form-obj").dialog("open");
}

function setObservacaoExpProPosDoc() {
    id = $("#chave-exp-pro-pdoc").text();
    $('#texto-obj').val('');
    tabela = 27;
    $("#dialog-form-obj").dialog("open");
}

function setObservacaoDirCurso() {
    id = $("#chave-dircurso").text();
    $('#texto-obj').val('');
    tabela = 5;
    $("#dialog-form-obj").dialog("open");
}

function setObservacaoColIntAct() {
    id = $("#chave-colIntAct").text();
    $('#texto-obj').val('');
    tabela = 6;
    $("#dialog-form-obj").dialog("open");
}

function setObservacaoColExtAct() {
    id = $("#chave-colExtAct").text();
    $('#texto-obj').val('');
    tabela = 7;
    $("#dialog-form-obj").dialog("open");
}

function setObservacaoUniInv() {
    id = $("#chave-uniInv").text();
    $('#texto-obj').val('');
    tabela = 8;
    $("#dialog-form-obj").dialog("open");
}

function setObservacaoConf() {
    id = $("#chave-conf").text();
    $('#texto-obj').val('');
    tabela = 15;
    $("#dialog-form-obj").dialog("open");
}

function setObservacaoApresentacao() {
    id = $("#chave-apresentacoes").text();
    $('#texto-obj').val('');
    tabela = 16;
    $("#dialog-form-obj").dialog("open");
}

function setObservacaoArbRevista() {
    id = $("#chave-arb-revistas").text();
    $('#texto-obj').val('');
    tabela = 17;
    $("#dialog-form-obj").dialog("open");
}

function setObservacaoPremio() {
    id = $("#chave-premios").text();
    $('#texto-obj').val('');
    tabela = 18;
    $("#dialog-form-obj").dialog("open");
}

function setObservacaoSC() {
    id = $("#chave-sc").text();
    $('#texto-obj').val('');
    tabela = 19;
    $("#dialog-form-obj").dialog("open");
}

function setObservacaoRede() {
    id = $("#chave-redes").text();
    $('#texto-obj').val('');
    tabela = 20;
    $("#dialog-form-obj").dialog("open");
}

function setObservacaoMissao() {
    id = $("#chave-missoes").text();
    $('#texto-obj').val('');
    tabela = 21;
    $("#dialog-form-obj").dialog("open");
}

function setObservacaoOrientacao() {
    id = $("#chave-orientacoes").text();
    $('#texto-obj').val('');
    tabela = 22;
    $("#dialog-form-obj").dialog("open");
}

function setObservacaoJuri() {
    id = $("#chave-juris").text();
    $('#texto-obj').val('');
    tabela = 23;
    $("#dialog-form-obj").dialog("open");
}

function setObservacaoOutAct() {
    id = $("#chave-outact").text();
    $('#texto-obj').val('');
    tabela = 24;
    $("#dialog-form-obj").dialog("open");
}

function setObservacaoAcaoDiv() {
    id = $("#chave-acaodivulgacao").text();
    $('#texto-obj').val('');
    tabela = 25;
    $("#dialog-form-obj").dialog("open");
}

function setObservacaoProduto() {
    id = $("#chave-produtos").text();
    $('#texto-obj').val('');
    tabela = 26;
    $("#dialog-form-obj").dialog("open");
}

function setObservacaoPublicacaoISI() {
    id = $("#chave-publicacao").text();
    $('#texto-obj').val('');
    tabela = 9;
    $("#dialog-form-obj").dialog("open");
}

function setObservacaoPublicacaoPUBMED() {
	id = $("#chave-publicacao").text();
    $('#texto-obj').val('');
    tabela = 10;
    $("#dialog-form-obj").dialog("open");
}

function setObservacaoPubManPat() {
	id = $("#chave-pubManPatentes").text();
    $('#texto-obj').val('');
    tabela = 11;
    $("#dialog-form-obj").dialog("open");
}

function setObservacaoPubManIntArt() {
	id = $("#chave-pubIntMan-Art").text();
    $('#texto-obj').val('');
    tabela = 12;
    $("#dialog-form-obj").dialog("open");
}

function setObservacaoPubManNacArt() {
	id = $("#chave-pubNacMan-Art").text();
    $('#texto-obj').val('');
    tabela = 13;
    $("#dialog-form-obj").dialog("open");
}

function setObservacaoPubManNacRev() {
	id = $("#chave-pubNacMan-Rev").text();
    $('#texto-obj').val('');
    tabela = 13;
    $("#dialog-form-obj").dialog("open");
}

function setObservacaoPubManNacOth() {
	id = $("#chave-pubNacMan-Oth").text();
    $('#texto-obj').val('');
    tabela = 13;
    $("#dialog-form-obj").dialog("open");
}

function setObservacaoPubManNacLiv() {
	id = $("#chave-pubNacMan-Liv").text();
    $('#texto-obj').val('');
    tabela = 13;
    $("#dialog-form-obj").dialog("open");
}

function setObservacaoPubManNacCLiv() {
	id = $("#chave-pubNacMan-CLiv").text();
    $('#texto-obj').val('');
    tabela = 13;
    $("#dialog-form-obj").dialog("open");
}

function setObservacaoPubManIntSum() {
	id = $("#chave-pubIntMan-Sum").text();
    $('#texto-obj').val('');
    tabela = 12;
    $("#dialog-form-obj").dialog("open");
}

function setObservacaoPubManNacSum() {
	id = $("#chave-pubNacMan-Sum").text();
    $('#texto-obj').val('');
    tabela = 13;
    $("#dialog-form-obj").dialog("open");
}

function setObservacaoPubManIntPRP() {
	id = $("#chave-pubIntMan-PRP").text();
    $('#texto-obj').val('');
    tabela = 12;
    $("#dialog-form-obj").dialog("open");
}

function setObservacaoPubManIntCP() {
	id = $("#chave-pubIntMan-Con").text();
    $('#texto-obj').val('');
    tabela = 12;
    $("#dialog-form-obj").dialog("open");
}

function setObservacaoPubManNacCP() {
	id = $("#chave-pubNacMan-Con").text();
    $('#texto-obj').val('');
    tabela = 13;
    $("#dialog-form-obj").dialog("open");
}

function setObservacaoPubManIntOth() {
	id = $("#chave-pubIntMan-Oth").text();
    $('#texto-obj').val('');
    tabela = 12;
    $("#dialog-form-obj").dialog("open");
}

function setObservacaoPubManIntLiv() {
	id = $("#chave-pubIntMan-Liv").text();
    $('#texto-obj').val('');
    tabela = 12;
    $("#dialog-form-obj").dialog("open");
}

function setObservacaoPubManIntCLiv() {
	id = $("#chave-pubIntMan-CLiv").text();
    $('#texto-obj').val('');
    tabela = 12;
    $("#dialog-form-obj").dialog("open");
}

function apagarInvestigador() {
    $("#dialog-confirm-inv").dialog("open");
}

function apagarHabilitacao() {
    $("#dialog-confirm-hab").dialog("open");
}

function apagarAgregacao() {
    $("#dialog-confirm-agr").dialog("open");
}

function apagarCaracterizacaoDocente() {
    $("#dialog-confirm-exp-pro-doc").dialog("open");
}

function apagarCaracterizacaoPosDoc() {
    $("#dialog-confirm-exp-pro-pdoc").dialog("open");
}

function apagarDirCurso() {
    $("#dialog-confirm-dircurso").dialog("open");
}

function apagarColActInt() {
    $("#dialog-confirm-colIntAct").dialog("open");
}

function apagarColActExt() {
    $("#dialog-confirm-colExtAct").dialog("open");
}

function apagarProjecto() {
    $("#dialog-confirm").dialog("open");
}

function apagarPublicacao() {
    $("#dialog-confirm-pub").dialog("open");
}

function apagarConferencia() {
    $("#dialog-confirm-cnf").dialog("open");
}

function apagarApresentacao() {
    $("#dialog-confirm-apr").dialog("open");
}

function apagarAcaoDivulgacao() {
    $("#dialog-confirm-acaoDiv").dialog("open");
}

function apagarProduto() {
    $("#dialog-confirm-produto").dialog("open");
}

function apagarPremio() {
    $("#dialog-confirm-premio").dialog("open");
}

function apagarArbRevista() {
    $("#dialog-confirm-arbRevista").dialog("open");
}

function apagarUniInv() {
    $("#dialog-confirm-uniInv").dialog("open");
}

function apagarSC() {
    $("#dialog-confirm-sc").dialog("open");
}

function apagarRede() {
    $("#dialog-confirm-rede").dialog("open");
}

function apagarMissao() {
    $("#dialog-confirm-missao").dialog("open");
}

function apagarOrientacao() {
    $("#dialog-confirm-orientacao").dialog("open");
}

function insertNewInvestigador() {	
	var data = '';
	var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
	
	xmlhttp.onreadystatechange=function()
	{
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			data = xmlhttp.responseText;
		}
	}
	
    var params = "a=98";
    xmlhttp.open("POST", "../../functions/saveBD.php", false);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
	
	var dataParsed = data.split(",");
	
	for(var i = 0; i<dataParsed.length; i++) {
		content[i] = { label: dataParsed[i], category: dataParsed[i+1] };
	}	
	
    $("#dialog-insert-investigador").dialog("open");
	$( "#nome-ins-inv" ).catcomplete({
	  delay: 0,
	  source: content
	});
	
}

function searchUser(name) {
	for (var i = 0, len = content.length; i < len; i++) {
		if (content[i].label === name)
			return content[i]; 
	}
	return null; 
}
	
	
function insertNewHabilitacao() {
    $("#dialog-insert-habilitacao").dialog("open");
}

function insertNewAgregacao() {
    $("#dialog-insert-agregacao").dialog("open");
}

function insertNewCaracDoc() {
    $("#dialog-insert-caracDoc").dialog("open");
}

function insertNewCaracPosDoc() {
	$("#dialog-insert-caracPosDoc").dialog("open");
}

function insertNewDirCurso() {
	$("#dialog-insert-dirCurso").dialog("open");
}

function insertNewColAct() {
	$("#dialog-insert-colIntAct").dialog("open");
}

function insertNewColExt() {
	$("#dialog-insert-colExtAct").dialog("open");
}

function insertNewUniInv() {
	$("#dialog-insert-uniInv").dialog("open");
}

function insertNewProjeto() {
	$("#dialog-insert-projectos").dialog("open");
}

function insertNewConferencia() {
	$("#dialog-insert-conferencia").dialog("open");
}

function insertNewApresentacao() {
	$("#dialog-insert-apresentacao").dialog("open");
}

function insertNewArbRevista() {
	$("#dialog-insert-arbRevista").dialog("open");
}

function insertNewPremio() {
	$("#dialog-insert-premio").dialog("open");
}

function insertNewSocCientifica(){
	$("#dialog-insert-socCientifica").dialog("open");
}

function insertNewRede() {
	$("#dialog-insert-rede").dialog("open");
}

function insertNewMissao() {
	$("#dialog-insert-missao").dialog("open");
}

function insertNewOrientacao() {
	$("#dialog-insert-orientacao").dialog("open");
}

function insertNewJuri(){
	$("#dialog-insert-juri").dialog("open");
}

function insertNewOutAct() {
	$("#dialog-insert-outAct").dialog("open");
}

function insertNewAcaoDiv() {
	$("#dialog-insert-acaoDiv").dialog("open");
}

function insertNewProduto() {
	$("#dialog-insert-produto").dialog("open");
}

function insertNewPub(tipopub, descricao) {
	tipopublicacao = tipopub;
	descr = descricao;
	
	if (tipopub == 'L' && (descr == 'INT' || descr == 'MAN')) {
		$("#dialog-insert-pubManualLiv").dialog("open");
	} else if (tipopub == 'CL' && (descr == 'INT' || descr == 'MAN')) {
		$("#dialog-insert-pubManualCLiv").dialog("open");
	} else if (tipopub == 'J' || tipopub == 'PRP' || tipopub == 'JA' || tipopub == 'JN' || tipopub == 'PRPN') {
		$("#dialog-insert-pubArt").dialog("open");
	} else if (tipopub == 'CP' || tipopub == 'OA' || tipopub == 'CPN') {
		$("#dialog-insert-pubConf").dialog("open");
	} else if (tipopub == 'L' || tipopub == 'S' || tipopub == 'SN' || tipopub == 'LN' || tipopub == 'CLN'|| tipopub == 'A' || tipopub == 'P') {
		$("#dialog-insert-pubLiv").dialog("open");
	} else if (tipopub == 'Pat') {
		$("#dialog-insert-pubManualPatentes").dialog("open");
	}  
}

function apagarJuri() {
    $("#dialog-confirm-juri").dialog("open");
}

function apagarOutAct() {
    $("#dialog-confirm-outact").dialog("open");
}

function validaAcao() {
    $("#dialog-confirm-acao").dialog("open");
}

function eliminaAcao() {
    $("#dialog-remove-acao").dialog("open");
}

function apagarAcao(idreg,tabela) {
	id = idreg;
	tab = tabela;
    $("#dialog-apaga-acao").dialog("open");
}

function mostraInfoAcao(autor, data) {
    $("#dialog-showinfo-acao-autor").text("- Autor: " + autor);
    $("#dialog-showinfo-acao-data").text("- Data de Realizacao da Acao: " + data);
    $("#dialog-showinfo-acao").dialog("open");
}

function confirmRepetidos(a) {
    if (a == 1) {
        $("#dialog-confirm-repetidos-txt").text("Duplicados registados no sistema.");
        $("#dialog-confirm-repetidos-img").addClass("ui-icon ui-icon-circle-check");
    } else if (a == 2) {
        $("#dialog-confirm-repetidos-txt").text("Tem de ser seleccionado um original para que a validaè¤¯ de repetidos seja efectuada.");
    }
    $("#dialog-confirm-repetidos").dialog("open");
}

function addRepetido(id) {
    if (jQuery.inArray(id, rep) == -1) {
        rep[rep.length] = id;
        document.getElementById('rep_' + id).checked = true;
    } else {
        for (var i = rep.length - 1; i >= 0; i--) {
            if (rep[i] === id) {
                rep.splice(i, 1);
            }
        }
        if (document.getElementById('orig_' + id).checked) {
            document.getElementById('rep_' + id).checked = false;
            document.getElementById('orig_' + id).checked = false;
            orig = null;
        } else
            document.getElementById('rep_' + id).checked = false;
    }
}

function addOriginal(id) {
    if (orig) {
        if (orig == id) {
            orig = null;
            addRepetido(id);
            for (var i = rep.length - 1; i >= 0; i--) {
                if (rep[i] === id) {
                    rep.splice(i, 1);
                }
            }

        } else {
            alert('Não pode existir mais do que um original por cada selecção de repetidos.');
			document.getElementById('orig_' + id).checked = false;
			rep = [];
			orig = '';
        }
    } else {
        orig = id;
        addRepetido(id);
        for (var i = rep.length - 1; i >= 0; i--) {
            if (rep[i] === id) {
                rep.splice(i, 1);
            }
        }
    }
}

function validaRepetidosProjecto(page) {
    if (orig) {
        //validar um ò®©£o como original
        if (rep.length == 0) { rep[0] = orig; }

        var xmlhttp;
        if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else { // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
		
		xmlhttp.onreadystatechange=function()
		{
			if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{											
				orig = '';
				rep = [];		
				confirmRepetidos(1);
				loadDataPRJ(page); 									
				return false;	
			}
		}

        var params = "a=52&orig=" + orig + '&rep=' + rep;
        xmlhttp.open("POST", "../../functions/saveBD.php", false);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send(params);
    } else {
        confirmRepetidos(2);
    }
}


function validaRepetidosPublicacao(page) {	
    if (orig) {
		
        //validar um ò®©£o como original
        if (rep.length == 0) {
            rep[0] = orig;
        }

        var xmlhttp;
        if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else { // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
		
		xmlhttp.onreadystatechange=function()
		{
			if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{											
				orig = '';
				rep = [];		
				confirmRepetidos(1);									
				return false;	
			}
		}

        var params = "a=57&orig=" + orig + '&rep=' + rep;			
        xmlhttp.open("POST", "../../functions/saveBD.php", false);		
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send(params);	
		
		loadDataJISI(page); 
		loadDataJAISI(page);		 
		loadDataPRPISI(page);	
		loadDataCPISI(page);	
		loadDataOAISI(page);
		loadDataLISI(page);
		loadDataCLISI(page);
		loadDataSISI(page);
		loadDataJNISI(page);
		loadDataPRPNISI(page); 
		loadDataSNISI(page);
		loadDataLNISI(page);
		loadDataCLNISI(page); 
		loadDataCPNISI(page); 
		loadDataPISI(page);
		loadDataAISI(page);							 
		loadDataPAT(page); 
		
		return false;
    } else {
        confirmRepetidos(2);
    }
}

function validaRepetidosConferencia(page) {
    if (orig) {
        //validar um ò®©£o como original
        if (rep.length == 0) {
            rep[0] = orig;
        }

        var xmlhttp;
        if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else { // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
		
		xmlhttp.onreadystatechange=function()
		{
			if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{											
				orig = '';
				rep = [];		
				confirmRepetidos(1);									
				return false;	
			}
		}

        var params = "a=59&orig=" + orig + '&rep=' + rep;
        xmlhttp.open("POST", "../../functions/saveBD.php", false);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send(params);
		
		loadDataCNF(page); 
    } else {
        confirmRepetidos(2);
    }
}

function validaRepetidosAcaoDivulgacao(page) {
    if (orig) {
        //validar um ò®©£o como original
        if (rep.length == 0) {
            rep[0] = orig;
        }

        var xmlhttp;
        if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else { // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
		
		xmlhttp.onreadystatechange=function()
		{
			if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{											
				orig = '';
				rep = [];		
				confirmRepetidos(1);									
				return false;	
			}
		}

        var params = "a=61&orig=" + orig + '&rep=' + rep;
        xmlhttp.open("POST", "../../functions/saveBD.php", false);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send(params);
		         
		loadDataAcaoDiv(page); 		
		
    } else {
        confirmRepetidos(2);
    }
}

function validaRepetidosPremio(page) {
    if (orig) {
        //validar um ò®©£o como original
        if (rep.length == 0) {
            rep[0] = orig;
        }

        var xmlhttp;
        if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else { // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }

		xmlhttp.onreadystatechange=function()
		{
			if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{											
				orig = '';
				rep = [];		
				confirmRepetidos(1);									
				return false;	
			}
		}
			
        var params = "a=63&orig=" + orig + '&rep=' + rep;
        xmlhttp.open("POST", "../../functions/saveBD.php", false);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send(params);
		
		loadDataPremios(page); 
    } else {
        confirmRepetidos(2);
    }
}

function validaRepetidosArbitragensRevistas(page) {
    if (orig) {
        //validar um ò®©£o como original
        if (rep.length == 0) {
            rep[0] = orig;
        }

        var xmlhttp;
        if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else { // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
		
		xmlhttp.onreadystatechange=function()
		{
			if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{											
				orig = '';
				rep = [];		
				confirmRepetidos(1);									
				return false;	
			}
		}

        var params = "a=65&orig=" + orig + '&rep=' + rep;
        xmlhttp.open("POST", "../../functions/saveBD.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send(params);
		
		loadDataArbRevistas(page); 
    } else {
        confirmRepetidos(2);
    }
}

function validaRepetidosUnidadesInvestigacao(page) {
    if (orig) {
        //validar um ò®©£o como original
        if (rep.length == 0) {
            rep[0] = orig;
        }

        var xmlhttp;
        if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else { // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
		
		xmlhttp.onreadystatechange=function()
		{
			if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{											
				orig = '';
				rep = [];		
				confirmRepetidos(1);									
				return false;	
			}
		}

        var params = "a=67&orig=" + orig + '&rep=' + rep;
        xmlhttp.open("POST", "../../functions/saveBD.php", false);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send(params);
		
		loadDataUniInv(page); 
    } else {
        confirmRepetidos(2);
    }
}

function validaTodosRepetidosProjecto() {
    var action = confirm('Está prestes a validar todos os registos repetidos de projetos. Tem a certeza que pretende efectuar esta acão?');

    if (action) {
        var xmlhttp;
        if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else { // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }

        var params = "a=53&ids=" + ids;
        xmlhttp.open("POST", "../../functions/saveBD.php", false);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send(params);
		
		loadDataPRJ(1); 
        confirmRepetidos(1);
    }
}

function validaTodosRepetidosPublicacao() {
    var action = confirm('Está prestes a validar todos os registos repetidos de publicações. Tem a certeza que pretende efectuar esta acão?');

    if (action) {
        var xmlhttp;
        if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else { // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }

        var params = "a=58&ids=" + ids;
        xmlhttp.open("POST", "../../functions/saveBD.php", false);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send(params);
		
        confirmRepetidos(1);
    }
}

function validaTodosRepetidosConferencia() {
    var action = confirm('Está prestes a validar todos os registos repetidos de conferências. Tem a certeza que pretende efectuar esta acão?');

    if (action) {
        var xmlhttp;
        if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else { // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
		
        var params = "a=60&ids=" + ids;
        xmlhttp.open("POST", "../../functions/saveBD.php", false);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send(params);
		loadDataCNF(1); 
        confirmRepetidos(1);
    }
}

function validaTodosRepetidosAcaoDivulgacao() {
    var action = confirm('Está prestes a validar todos os registos repetidos de Ações de Divulgação. Tem a certeza que pretende efectuar esta acão?');

    if (action) {
        var xmlhttp;
        if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else { // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }

        var params = "a=62&ids=" + ids;
        xmlhttp.open("POST", "../../functions/saveBD.php", false);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send(params);
		loadDataAcaoDiv(1); 
        confirmRepetidos(1);
    }
}

function validaTodosRepetidosPremio() {
    var action = confirm('Está prestes a validar todos os registos repetidos de Prémios. Tem a certeza que pretende efectuar esta acão?');

    if (action) {
        var xmlhttp;
        if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else { // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }

        var params = "a=64&ids=" + ids;
        xmlhttp.open("POST", "../../functions/saveBD.php", false);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send(params);
		loadDataPremios(1);
        confirmRepetidos(1);
    }
}

function validaTodosRepetidosArbitragensRevistas() {
    var action = confirm('Está prestes a validar todos os registos repetidos de Arbitragens Revistas. Tem a certeza que pretende efectuar esta acão?');

    if (action) {
        var xmlhttp;
        if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else { // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }

        var params = "a=66&ids=" + ids;
        xmlhttp.open("POST", "../../functions/saveBD.php", false);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send(params);
		loadDataArbRevistas(1); 
        confirmRepetidos(1);
    }
}

function validaTodosRepetidosUnidadesInvestigacao() {
    var action = confirm('Está prestes a validar todos os registos repetidos de Unidades de Investigacao. Tem a certeza que pretende efectuar esta acão?');

    if (action) {
        var xmlhttp;
        if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else { // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
		
        var params = "a=68&ids=" + ids;
        xmlhttp.open("POST", "../../functions/saveBD.php", false);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send(params);
		loadDataUniInv(1);
        confirmRepetidos(1);
    }
}

function isEmpty(str) {
    return (!str || 0 === str.length);
}
